<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LpcDefinition extends Model
{
    public $table = "lpc_definition";

    protected $fillable = ['period_from','period_to','amount_from','amount_to','delay_from','delay_to','waive_days','fix_charges','rate','frequency','created_by','updated_by'];
}
