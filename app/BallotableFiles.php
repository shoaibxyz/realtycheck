<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BallotableFiles extends Model
{
	use SoftDeletes;
    public $table = "ballotable_files";


}
