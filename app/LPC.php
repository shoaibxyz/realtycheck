<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LPC extends Model
{
	use SoftDeletes;
    public $table = "lpc";
    
    protected $fillable = ['lpc_amount'];
}
