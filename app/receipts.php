<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class receipts extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = [
        'receipt_no',
        'registration_no',
        'receiving_mode',
        'received_amount',
        'receiving_date',
        'instrument_no',
        'receiving_type',
        'instrument_date',
        'drawee_bank',
        'drawee_branch',
        'bank_id',
        'deposit_account_no',
        'deposit_date',
        'reference_no',
        'deposit_by',
        'receipt_remarks',
        'discount_type',
        'rebate_amount',
        'created_by',
        'updated_by',
        'pdc_ack',
        'to_bank',
        'from_bank',
        'deposited_in',
        'transaction_cheque_no',
        'is_cancelled',
        'cancel_remarks',
        'cancel_by',
        'cancel_date'
    ]; 




    protected static function boot()
    {
        parent::boot();
        static::updated(function ($receipts) {

            $changes = $receipts->isDirty() ? $receipts->getDirty() : false;
            
            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ( $receipts->getOriginal($attr) == null ) ? "null" : $receipts->getOriginal($attr);

                    if($attr == 'updated_at')
                        continue;

                    activity('Receipts')
                        ->performedOn($receipts)
                        ->withProperties(['registration_no' => $receipts->registration_no, 'event' => 'updated'])
                        ->log("Updated receipts field named $attr from {$getOriginal} to {$receipts->$attr} having this receipt no {$receipts->receipt_no}");
                }
            }
        });

        static::created(function ($receipts) {
            activity('Receipts')
                    ->performedOn($receipts)
                    ->withProperties(['event' => 'created', 'registration_no' => $receipts->registration_no])
                    ->log("Created new receipts having receipt number {$receipts->receipt_no} for amount {$receipts->received_amount}");
        });

        static::deleted(function ($receipts) {

            activity('Receipts')
                    ->performedOn($receipts)
                    ->withProperties(['registration_no' => $receipts->registration_no, 'event' => 'Deleted'])
                    ->log("Deleted receipts having this receipt number number {$receipts->receipt_no}");
        });
    }
    

    public function setReceivingDateAttribute($value)
    {
        return $this->attributes['receiving_date'] = date('Y-m-d', strtotime($value));
    }
    
    public function setDepositDateAttribute($value)
    {
        return $this->attributes['deposit_date'] = date('Y-m-d', strtotime($value));
    }

    public function setInstrumentDateAttribute($value)
    {
        return $this->attributes['instrument_date'] = date('Y-m-d', strtotime($value));
    }

    // public function InstallmentReceipts()
    // {
    //     return $this->belongsTo('App\InstallmentReceipts', 'receipt_no');
    // }
    
    public static function getReceipt($receipt_no = null)
    {
        return receipts::where('receipt_no', $receipt_no)->first();
    }
}
