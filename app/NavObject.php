<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NavObject extends Model
{
    protected $fillable = array('name','is_active','is_deleted','created_by','created_at','updated_by','updated_at');

    public $timestamps = false;

    protected $table = 'nav_objects';
	
	/**
     * Get the role record associated with the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'admin_rights');
    }

    public function inRole($id)
    {
        return $this->roles()->where('role_id', $id)->count();
    }
    public function canCreate($id)
    {        
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_create',1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canPerform($rid,$oid,$act)
    {
        return (\DB::table('admin_rights')->where([['role_id', $rid],['object_id', $oid],['can_'.$act,1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canEdit($id)
    {
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_edit',1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canDelete($id)
    {
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_delete',1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canDeleteP($id)
    {
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_deletep',1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canRestore($id)
    {
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_restore',1]])->count() == 0) ? "" : "checked='checked'";
    }
    public function canAssign($id)
    {
        return (\DB::table('admin_rights')->where([['role_id', $id],['can_assign',1]])->count() == 0) ? "" : "checked='checked'";
    }
}
