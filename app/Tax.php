<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Tax extends Model
{
	use SoftDeletes;
	use LogsActivity;

    public $table = "hr_tax";

    public $dates = ['from_date','to_date','created_at','updated_at','deleted_at']; 

    protected $fillable = ['from_date','to_date','amount_from','amount_to','percentage','tax_amount','created_by','updated_by' , 'agency_id'];

   
    protected static function boot()
    {
        
        parent::boot();
        static::updated(function ($tax) {

            $changes = $tax->isDirty() ? $tax->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at' || $attr == 'updated_by')
                        continue;

                    activity('Tax')
                        ->performedOn($tax)
                        ->log("updated tax definition from {$tax->$attr} to {$tax->getOriginal($attr)}");
                }
            }
        });

        static::created(function ($tax) {
            activity('Tax')
                ->performedOn($tax)
                ->log("created tax definition of amount {$tax->tax_amount} range from {$tax->amount_from} to {$tax->amount_to}");
        });

        static::deleted(function ($tax) {
            activity('Tax')
                ->performedOn($tax)
                ->log("deleted tax definition of amount {$tax->tax_amount} range from {$tax->amount_from} to {$tax->amount_to}");
        });
    }
}
