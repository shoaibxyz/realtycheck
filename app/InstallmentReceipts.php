<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstallmentReceipts extends Model
{
	use SoftDeletes;
    public $table = 'installment_receipts';

    protected $fillable = ['registration_no','receipt_id','receipt_no','installment_id','due_amount','received_amount','rebate_amount','os_amount','receipt_amount','receipt_date','payment_mode','created_at','updated_at','payment_desc','installment_no','due_date'];


    // public function receipts(){
    // 	return $this->hasMany('App\receipts');
    // }
}
