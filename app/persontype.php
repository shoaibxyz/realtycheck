<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class persontype extends Model
{
	public $timestamps = false;
    protected $fillable = ['type'];
    public $table = "person_type";

    public function person()
    {
    	return $this->hasOne('App\User');
    }
}
