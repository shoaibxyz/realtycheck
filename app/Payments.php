<?php

namespace App;


use App\InstallmentReceipts;
use App\memberInstallments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use DB;

class Payments extends Model
{
	use LogsActivity;
	use SoftDeletes;

	protected $fillable = [
				'registration_no',
				'payment_schedule_id',
				'payment_type_id',
				'payment_amount',
				'payment_date',
				'payment_mode',
				'payment_remarks',
				'agency_id',
				'rebate_amount',
				'created_by',
				'updated_by',
				'offer',
				'created_at',
				'updated_at',
				'street_no',
				'plot_no',
				'block_no'
			];

		public $table = 'bookings';
	protected $dates = ['cancel_date','reactive_date'];
	public $timestamps = true;

	protected static function boot()
    {
        parent::boot();
        static::updated(function ($booking) {

            $changes = $booking->isDirty() ? $booking->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                	if($attr == 'updated_at')
                		continue;

                    activity('Booking')
                        ->performedOn($booking)
                        ->withProperties(['event' => 'updated'])
                        ->log("Updated booking $attr from {$booking->getOriginal($attr)} to {$booking->$attr}");
                }
            }
        });

        static::deleted(function ($booking){
        	activity('Booking')
                ->performedOn($booking)
                ->withProperties(['registration_no' => $booking->registration_no, 'event' => 'Deleted'])
                ->log("Deleted booking for user named {$booking->person} and CNIC {$booking->cnic} ");
        });
    }

	public static function getInstallmentReceipts($id)
	{
		return InstallmentReceipts::where('installment_id', $id)->get();
	}
	
	public static function getlabel($id)
	{
		$cancel = $id;
		$word = "";
		if($cancel == 1){
		    
		    $word .= "<span class='label label-danger'> Cancel";
		    $word .= "</span>";
		    
		}
		
		elseif($cancel == 2){
		    
		    $word .= "<span class='label label-success'> Refunded";
		    $word .= "</span>";
		    
		}
		
		else{
		    
		    $word .= "<span class='label label-info'> Merged";
		    $word .= "</span>";
		    
		}
		
		return $word;
	}

	public function memberData()
	{
		return $this->belongsTo('App\members','registration_no');
	}

	public function featureset()
	{
		return $this->belongsToMany('App\featureset','booking_featureset','payment_id','feature_set_id');
	}

	public function members()
	{
		return $this->belongsToMany('App\members','member_payment_plan','payment_id','member_id');
	}

	public function setPaymentDateAttribute($value)
	{
		return $this->attributes['payment_date'] = date('Y-m-d', strtotime(strtr($value, '/', '-')));
	}

	public function setDateOfBirthAttribute($value)
	{
		return $this->attributes['dateofbirth'] = date('Y-m-d', strtotime(strtr($value, '/', '-')));
	}

	public function paymentPlan()
	{
		return $this->belongsTo('App\paymentSchedule','payment_schedule_id');
	}

	public function scopeLast($query)
	{
		return $query->orderBy('created_at', 'desc')->first();
	}

	public function scopeInfo($query, $registration_no)
    {
        return $query->where('registration_no',$registration_no)->get()->last();
    }

    public function scopeActive($query)
    {
    	return $query->where('status', 1)->get();
    }

    public function agency()
    {
    	return $this->belongsTo('App\agency');
    }
    
    public static function countbook($id)
    {
        $count = DB::table('booking_merged')->where('booking_id', $id)->count('booking_id');
  
    	return $count;
    }
    
    public static function getbook($id)
    {
        $var = \App\Payments::join('booking_merged','booking_merged.booking_id','bookings.id')->where('booking_merged.booking_id', $id)
            ->orderBy('bookings.cancel_date','desc')
                   ->get(); 
        
        return $var;
    }
    
    public static function getmergebook($id)
    {
        // $var = \App\Payments::join('booking_merged','booking_merged.booking_id','bookings.id')->where('bookings.registration_no', $id)
        //     // ->orderBy('bookings.cancel_date','desc')
        //     ->select(DB::Raw('bookings.cancel_date', 'bookings.refund_amount', 'booking_merged.merged_into', 'bookings.refund_amount', 'bookings.deduct_amount', 'bookings.cancel_remarks'))
            //  ->select(DB::Raw('bookings.*', 'booking_merged.merged_into'))
                //   ->get(); 
        
        
        $var  = DB::table('booking_merged')->where('booking_id', $id)->get();
        
        return $var;
    }
    
    public static function instDue($from,$to,$agency_id,$plot_nature,$rpt_mode)
    {
        if($rpt_mode == 'month')
        {
            $installsDue = memberInstallments::join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->whereNull('members.deleted_at')
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                /*->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })*/
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->select(DB::Raw('sum(member_installments.due_amount) as due'))
                                ->first();
                                
            //return $from.'='.$to.'='.$agency_id.'='.$plot_nature.'='.$rpt_mode;
            return $installsDue->due;
            
        }
        elseif($rpt_mode == 'agency')
        {
            $installsDue = memberInstallments::join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('agency','agency.id','bookings.agency_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->whereNull('members.deleted_at')
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                /*->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })*/
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->select(DB::Raw('agency.name as agent,sum(member_installments.due_amount) as due'))
                                ->groupBy('agent')
                                ->get();
            //dd($installsDue);
        }
        return $installsDue;
    }
    
    public static function instRec($from,$to,$agency_id,$plot_nature,$rpt_mode)
    {
        if($rpt_mode == 'month')
        {
            $installsRec = memberInstallments::join('installment_receipts','installment_receipts.installment_id','member_installments.id')
                                ->join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('member_installments.installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->whereNull('installment_receipts.deleted_at')
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->where('is_paid',1)->orWhere('member_installments.os_amount','>',0)
                                ->select(DB::Raw('sum(installment_receipts.received_amount) as received'))
                                ->first();
            $sql = "select sum(installment_receipts.received_amount) as received from `installment_receipts` 
                    inner join `member_installments` on `member_installments`.`id` = `installment_receipts`.`installment_id` 
                    inner join `bookings` on `bookings`.`registration_no` = `member_installments`.`registration_no` 
                    inner join `members` on `members`.`registration_no` = `bookings`.`registration_no` 
                    inner join `persons` on `persons`.`id` = `members`.`person_id` 
                    inner join `payment_schedule` on `payment_schedule`.`id` = `bookings`.`payment_schedule_id` 
                    inner join receipts on receipts.id = installment_receipts.receipt_id 
                    where `member_installments`.`installment_date` between '". $from . "' and '". $to . "' and `is_active_user` = 1 
                    and `member_installments`.`deleted_at` is null and `bookings`.`is_cancelled` = 0 and `bookings`.`status` = 1 and `bookings`.`deleted_at` is null 
                    and `members`.`is_current_owner` = 1 and members.deleted_at is NULL  and `persons`.`status` = 1 and `payment_schedule`.`plot_nature` = '". $plot_nature ."' 
                    and `installment_receipts`.`deleted_at` is null and `receipts`.`deleted_at` is null and (`is_paid` = 1 or `member_installments`.`os_amount` > 0)";
            $received = DB::select($sql);
            //dd($sql);
            if(count($received) > 0)
                return $received[0]->received;
            else
                return 0;
            //return $installsRec->received;
        }
        elseif($rpt_mode == 'agency')
        {
            $installsRec = memberInstallments::join('installment_receipts','installment_receipts.installment_id','member_installments.id')
                                ->join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('agency','agency.id','bookings.agency_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('member_installments.installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->whereNull('installment_receipts.deleted_at')
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->where('is_paid',1)->orWhere('member_installments.os_amount','>',0)
                                ->select(DB::Raw('agency.name as agent, sum(installment_receipts.received_amount) as received'))
                                ->groupBy('agent')
                                ->get();
            $wh = '';
            if($agency_id)
                $wh = " and bookings.agency_id=". $agency_id;
            
            $sql = "select sum(installment_receipts.received_amount) as received from `installment_receipts` 
                    inner join `member_installments` on `member_installments`.`id` = `installment_receipts`.`installment_id` 
                    inner join `bookings` on `bookings`.`registration_no` = `member_installments`.`registration_no` 
                    inner join `members` on `members`.`registration_no` = `bookings`.`registration_no` 
                    inner join `persons` on `persons`.`id` = `members`.`person_id` 
                    inner join `payment_schedule` on `payment_schedule`.`id` = `bookings`.`payment_schedule_id` 
                    where `member_installments`.`installment_date` between '". $from . "' and '". $to . "' and `member_installments`.`is_cancelled` = 0 and `is_active_user` = 1 
                    and `member_installments`.`deleted_at` is null and `bookings`.`is_cancelled` = 0 and `bookings`.`status` = 1 and `bookings`.`deleted_at` is null 
                    and `members`.`is_current_owner` = 1 and `persons`.`status` = 1 and `payment_schedule`.`plot_nature` = '". $plot_nature . $wh ."' 
                    and `installment_receipts`.`deleted_at` is null and (`is_paid` = 1 or `member_installments`.`os_amount` > 0)";
            $received = DB::select($sql);
            //dd($received[0]->received);
            if(count($received) > 0)
                return $received[0]->received;
            else
                return 0;                
        }
        //return $installsRec;
    }
}
