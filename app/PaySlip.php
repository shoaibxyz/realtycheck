<?php

namespace App;

use App\Models\HR\Employees;
use App\Models\HR\department;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class PaySlip extends Model
{
    use SoftDeletes;
    use LogsActivity;

    public $table = "hr_payslip";
    protected $fillable = ['department_id','employee_id','month','year','basic_salary','total_deductions','total_allowances','fine','fine_reason','net_salary','status','created_by','updated_by','code','advance_salary','advance_salary_id','tax_amount', 'bonus'];


    protected static function boot()
    {
        parent::boot();
        
        static::updated(function ($payslip) {

            $changes = $payslip->isDirty() ? $payslip->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at' || $attr == 'updated_by')
                        continue;
                    

                    activity('payslip')
                        ->performedOn($payslip)
                        ->log("updated payslip {$attr} from {$payslip->$attr} to {$payslip->getOriginal($attr)}");
                }
            }
        });

        static::created(function ($payslip) {
            activity('payslip')
                ->performedOn($payslip)
                ->log("created a new payslip of employee {$payslip->employee->user->name} of amount {$payslip->basic_salary}");
        });

        static::deleted(function ($payslip) {
            activity('payslip')
                ->performedOn($payslip)
                ->log("deleted payslip of employee {$payslip->employee->user->name} of amount {$payslip->basic_salary }");
        });
    }


    public function employee()
    {
    	return $this->belongsTo(Employees::class, 'employee_id');
    }

    public function department()
    {
    	return $this->belongsTo(department::class, 'department_id');
    }
}
