<?php

namespace App;

use App\User;
use App\members;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class members extends Model
{
    use LogsActivity;
     use SoftDeletes;

    protected $fillable = [
                'registration_no',
    			'nominee_name',
    			'nominee_guardian',
    			'nominee_cnic',
    			'relation_with_member',

    			//'registeration_status',
    			//'dd_pay_order_no',
    			//'total_amount',
    			//'cheque_no',
    			//'bank_name',
    			//'lumpsum_payment',
    			//'down_payment',
    			//'other_payments',
    			'person_id',
                'created_by',
                'updated_by'

    		];

    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($member) {
            $changes = $member->isDirty() ? $member->getDirty() : false;
            //Check if member is member or not

            $memberCheck = members::find($member->id);
            $registration_no = $memberCheck->registration_no;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_at' || $attr == 'is_current_owner')
                        continue;

                    $getOriginal = ( $member->getOriginal($attr) == null ) ? "null" : $member->getOriginal($attr);
                    activity('Member')
                        ->performedOn($member)
                        ->withProperties(['registration_no' => $member->registration_no, 'event' => 'updated'])
                        ->log(":causer.name updated user field named $attr from {$getOriginal} to {$member->$attr}");
                }
            }

        });

        static::created(function($member) {
            $user = User::find($member->person_id);

            activity('Member')
                    ->performedOn($member)
                    ->withProperties(['event' => 'created'])
                    ->log(":causer.name created a new member having this CNIC {$user->cnic} ");
        });

        static::deleted(function($member) {
             activity('Member')
                    ->performedOn($member)
                    ->withProperties(['event' => 'deleted'])
                    ->log(":causer.name deleted member field named $attr from {$getOriginal} to {$getAttr}");
        });
    }

public static function getMemberDetail($user_id)
    {
        //dd($user_id);
        return members::find($user_id);
    }
    public function user()
    {
        return $this->belongsTo('App\User','person_id');
    }

    public function member_files()
    {
        return $this->belongsToMany('App\member_files');
    }

    public function payment()
    {
        return $this->hasMany('App\Payments');
    }

    public function memberRegistration()
    {
        return $this->hasOne('App\Payments');
    }

    public function scopeInfo($query, $registration_no)
    {
        return $query->where('registration_no',$registration_no)->where('is_current_owner', 1)->get()->last();
    }

    public function scopeLast($query)
    {
        return $query->orderBy('created_at', 'desc')->first();
    }
}
