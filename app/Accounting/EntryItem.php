<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class EntryItem extends Model
{
    use SoftDeletes;

    protected $fillable = array('entry_id','ledger_id','amount','dc','reconciliation_date','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_entryitems';

    public static function getLE($lid)
    {
        return EntryItem::where([['ledger_id',$lid],['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }

    public static function getEI($eid)
    {
        return EntryItem::where([['entry_id',$eid],['deleted_at',NULL],['module_id',Session::get('module')]])->get();
    }
}