<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class Tag extends Model
{
    use SoftDeletes;

    protected $fillable = array('title','color','background','module_id','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_tags';

    public static function getAll()
    {
        return Tag::where([['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }
}