<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Entry extends Model
{
    use SoftDeletes;

    protected $fillable = array('module_id','tag_id','entrytype_id','number','date','dr_total','cr_total','narration','status','created_by','created_at','updated_by','updated_at','deleted_at','ent_type');

    public $timestamps = false;

    protected $table = 'acc_entries';

    public function ledger()
    {
        return $this->belongsToMany('App\Accounting\Ledger', 'acc_entryitems');
    }

    public function tag()
    {
        return $this->belongsTo('App\Accounting\Tag');
    }

    public static function entryLedgers($id,$deleted) {

        if($deleted == 0)
            $entryitems = EntryItem::where([['entry_id',$id],['deleted_at',NULL],['module_id',Session::get('module')]])->orderBy('id','Desc')->get();
        else
            $entryitems = EntryItem::withTrashed()->where([['entry_id',$id],['module_id',Session::get('module')]])->orderBy('id','Desc')->get();

        /* Get dr and cr ledger id and count */
        $dr_count = 0;
        $cr_count = 0;
        $dr_ledger_id = '';
        $cr_ledger_id = '';
        if(count($entryitems) > 0) {
            foreach ($entryitems as $entryitem) {
                if ($entryitem->dc == 'D') {
                    $dr_ledger_id = $entryitem->ledger_id;
                    $dr_count++;
                } else {
                    $cr_ledger_id = $entryitem->ledger_id;
                    $cr_count++;
                }
            }
        }

        /* Get ledger name */
        $Ledger = new Ledger();
        $dr_name = $Ledger->getName($dr_ledger_id);
        $Ledger = new Ledger();
        $cr_name = $Ledger->getName($cr_ledger_id);

        if (strlen($dr_name) > 15) {
            $dr_name = substr($dr_name, 0, 15) . '...';
        }
        if (strlen($cr_name) > 15) {
            $cr_name = substr($cr_name, 0, 15) . '...';
        }

        // if more than one ledger on dr / cr then add [+] sign
        if ($dr_count > 1) {
            $dr_name = $dr_name . ' [+]';
        }
        if ($cr_count > 1) {
            $cr_name = $cr_name . ' [+]';
        }

        $ledgerstr = 'Dr ' . $dr_name . ' / ' . 'Cr ' . $cr_name;
        if($dr_count == 0 and $cr_count == 0)
            return '';
        else
            return $ledgerstr;
    }

    public static function saveEntry2($module_id,$receipt_id,$ledger_dr,$ledger_cr,$pay_type,$date,$amount,$remarks)
    {
        $tag = new Tag;
        $tag->title = $receipt_id;
        $tag->color = 'FFFFFF';
        $tag->background = 'FF2220';
        $tag->status = 1;
        $tag->module_id = $module_id;
        $tag->created_by = Auth::user()->id;
        $tag->save();

        $entry =  new Entry();
        $lastentry = Entry::orderBy('number', 'desc')->first();
        $entry->number = ($lastentry != null) ? $lastentry->number + 1 : 1;
        $entry->date = date('Y-m-d', strtotime(strtr($date, '/', '-')));
        $entry->narration = $remarks;
        $entry->tag_id = $tag->id;
        $entry->receipt_id = $receipt_id;
        $entry->ent_type = 1;
        $entry->entrytype_id = $pay_type;
        $entry->module_id = $module_id;
        $entry->dr_total = $amount;
        $entry->cr_total = $amount;
        $entry->status = 1;
        $entry->created_by = Auth::user()->id;
        $entry->created_at = date('Y-m-d H:i:s');
        $entry->updated_by = Auth::user()->id;
        $entry->updated_at = date('Y-m-d H:i:s');
        $entry->save();

        //Debit Entry
        $eitem = new EntryItem();
        $eitem->module_id = $module_id;
        $eitem->entry_id = $entry->id;
        $eitem->ledger_id = $ledger_dr;
        $eitem->dc = 'D';
        $eitem->amount = $amount;
        $eitem->status = 1;
        $eitem->created_by = Auth::user()->id;
        $eitem->created_at = date('Y-m-d H:i:s');
        $eitem->updated_by = Auth::user()->id;
        $eitem->updated_at = date('Y-m-d H:i:s');
        $eitem->save();

        //Credit Entry
        $eitem = new EntryItem();
        $eitem->module_id = $module_id;
        $eitem->entry_id = $entry->id;
        $eitem->ledger_id = $ledger_cr;
        $eitem->dc = 'C';
        $eitem->amount = $amount;
        $eitem->status = 1;
        $eitem->created_by = Auth::user()->id;
        $eitem->created_at = date('Y-m-d H:i:s');
        $eitem->updated_by = Auth::user()->id;
        $eitem->updated_at = date('Y-m-d H:i:s');
        $eitem->save();
    }
}