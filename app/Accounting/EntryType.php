<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class EntryType extends Model
{
    use SoftDeletes;

    protected $fillable = array('module_id','label','name','description','base_type','numbering','prefix','suffix','zero_padding','restriction_bankcash','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_entrytypes';

    public static function getAll()
    {
        return EntryType::where([['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }
}