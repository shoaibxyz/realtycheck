<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = array('parent_id','name','code','affects_gross','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_groups';

    public static function getAll($pid, $order, $orderBy)
    {
        return Group::where([['parent_id',$pid],['status',1],['deleted_at',NULL],['module_id',Session::get('module')]])->orderBy($order,$orderBy)->get();
    }
}