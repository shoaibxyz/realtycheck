<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    protected $fillable = array('label','company','email','address','currency_symbol','fy_start','fy_end','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_modules';
}