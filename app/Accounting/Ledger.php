<?php

namespace App\Accounting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Support\Facades\Auth;

class Ledger extends Model
{
    use SoftDeletes;

    protected $fillable = array('group_id','name','code','op_balance','op_balance_dc','type','reconciliation','notes','status','created_by','created_at','updated_by','updated_at','deleted_at');

    public $timestamps = false;

    protected $table = 'acc_ledgers';

    public function getName($id)
    {
        if($id) {
            $name = '';
            $ledger = Ledger::find($id);
            if($ledger)
                $name = $ledger->name;
            
            return $name;
        }
        else
            return '';
    }

    public static function openingBalance($id, $start_date = null) {

        $op = Ledger::find($id);

        $op_total = 0;
        $op_total_dc = '';
        if($op) {
            if (empty($op->op_balance)) {
                $op_total = 0;
            } else {
                $op_total = $op->op_balance;
            }
            $op_total_dc = $op->op_balance_dc;
        }
        /* If start date is not specified then return here */
        if (is_null($start_date)) {
            return array('dc' => $op_total_dc, 'amount' => $op_total);
        }

        $total = Ledger::getEntryItems($id,'D',$start_date,null,null);
        /*$total = DB::table('acc_entryitems')
            ->leftJoin('acc_entries','acc_entries.id','acc_entryitems.entry_id')
            ->where('acc_entryitems.ledger_id',$id)
            ->where('acc_entryitems.dc','D')
            ->where('acc_entryitems.deleted_at',NULL)
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            ->where('acc_entryitems.status',1)
            ->where('acc_entries.module_id',Session::get('module'))
            ->select(DB::Raw('SUM(acc_entryitems.amount) as total'))
            ->when($start_date, function ($query) use ($start_date) {
                $start_date = date('Y-m-d',strtotime($start_date));
                return $query->where('acc_entries.date','<', $start_date);
            })
            ->get();*/

        if (empty($total->total)) {
            $dr_total = 0;
        } else {
            $dr_total = $total->total;
        }

        /*$total = DB::table('acc_entryitems')
            ->leftJoin('acc_entries','acc_entries.id','acc_entryitems.entry_id')
            ->where('acc_entryitems.ledger_id',$id)
            ->where('acc_entryitems.dc','C')
            ->where('acc_entryitems.deleted_at',NULL)
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            ->where('acc_entryitems.status',1)
            ->where('acc_entries.module_id',Session::get('module'))
            ->select(DB::Raw('SUM(acc_entryitems.amount) as total'))
            ->when($start_date, function ($query) use ($start_date) {
                $start_date = date('Y-m-d',strtotime($start_date));
                return $query->where('acc_entries.date','<', $start_date);
            })
            ->get();*/
        $total = Ledger::getEntryItems($id,'C',$start_date,null,null);

        if (empty($total->total)) {
            $cr_total = 0;
        } else {
            $cr_total = $total->total;
        }

        /* Add opening balance */
        if ($op_total_dc == 'D') {
            $dr_total_final = Helpers::calculate($op_total, $dr_total, '+');
            $cr_total_final = $cr_total;
        } else {
            $dr_total_final = $dr_total;
            $cr_total_final = Helpers::calculate($op_total, $cr_total, '+');
        }

        /* Calculate final opening balance */
        if (Helpers::calculate($dr_total_final, $cr_total_final, '>')) {
            $op_total = Helpers::calculate($dr_total_final, $cr_total_final, '-');
            $op_total_dc = 'D';
        } else if (Helpers::calculate($dr_total_final, $cr_total_final, '==')) {
            $op_total = 0;
            $op_total_dc = $op_total_dc;
        } else {
            $op_total = Helpers::calculate($cr_total_final, $dr_total_final, '-');
            $op_total_dc = 'C';
        }

        return array('dc' => $op_total_dc, 'amount' => $op_total);
    }

    public static function closingBalance($id, $start_date = null, $end_date = null)
    {
        /* Opening balance */
        $op = Ledger::find($id);

        $op_total = 0;
        $op_total_dc = $op->op_balance_dc;
        if (is_null($start_date)) {
            if (empty($op->op_balance)) {
                $op_total = 0;
            } else {
                $op_total = $op->op_balance;
            }
        }

        $dr_total = 0;
        $cr_total = 0;
        $dr_total_dc = 0;
        $cr_total_dc = 0;

        $totalEI = Ledger::getEntryItems($id,'D',$start_date,$end_date,null);

        if (count($totalEI) == 0) {
            $dr_total = 0;
        } else {
            $total = $totalEI[0]->total;
            $dr_total = $total;
        }

        $totalEI = Ledger::getEntryItems($id,'C',$start_date,$end_date,null);

        $total = $totalEI[0]->total;

        if (count($totalEI) == 0) {
            $cr_total = 0;
        } else {
            $total = $totalEI[0]->total;
            $cr_total = $total;
        }

        /* Add opening balance */
        if ($op_total_dc == 'D') {
            $dr_total_dc = Helpers::calculate($op_total, $dr_total, '+');
            $cr_total_dc = $cr_total;
        } else {
            $dr_total_dc = $dr_total;
            $cr_total_dc = Helpers::calculate($op_total, $cr_total, '+');
        }

        /* Calculate and update closing balance */
        $cl = 0;
        $cl_dc = '';
        if (Helpers::calculate($dr_total_dc, $cr_total_dc, '>')) {
            $cl = Helpers::calculate($dr_total_dc, $cr_total_dc, '-');
            $cl_dc = 'D';
        } else if (Helpers::calculate($cr_total_dc, $dr_total_dc, '==')) {
            $cl = 0;
            $cl_dc = $op_total_dc;
        } else {
            $cl = Helpers::calculate($cr_total_dc, $dr_total_dc, '-');
            $cl_dc = 'C';
        }

        return array('dc' => $cl_dc, 'amount' => $cl, 'dr_total' => $dr_total, 'cr_total' => $cr_total);
    }

    public static function reconciliationPending($id, $start_date = null, $end_date = null) {

        $dr_total = 0;
        $cr_total = 0;

        /* Debit total */
        $totalEI = Ledger::getEntryItems($id,'D',$start_date,$end_date,1);

        if (count($totalEI) == 0) {
            $dr_total = 0;
        } else {
            $total = $totalEI[0]->total;
            $dr_total = $total;
        }

        /* Credit total */
        $totalEI = Ledger::getEntryItems($id,'C',$start_date,$end_date,1);

        $total = $totalEI[0]->total;

        if (count($totalEI) == 0) {
            $cr_total = 0;
        } else {
            $total = $totalEI[0]->total;
            $cr_total = $total;
        }

        return array('dr_total' => $dr_total, 'cr_total' => $cr_total);

    }

    public static function getEntryItems($eid, $type, $start_date = null, $end_date = null, $rec_date = null)
    {
        $total = DB::table('acc_entryitems')
            ->leftJoin('acc_entries','acc_entries.id','acc_entryitems.entry_id')
            ->where('acc_entryitems.ledger_id',$eid)
            ->where('acc_entryitems.dc',$type)
            /* ->where('acc_entryitems.deleted_at',NULL) */
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            /* ->where('acc_entryitems.status',1) */
            ->where('acc_entries.module_id',Session::get('module'))

            ->when($start_date, function ($query) use ($start_date) {
                $start_date = date('Y-m-d',strtotime($start_date));
                return $query->where('acc_entries.date','>=', $start_date);
            })

            ->when($end_date, function ($query) use ($end_date) {
                $end_date = date('Y-m-d',strtotime($end_date));
                return $query->where('acc_entries.date','<=', $end_date);
            })

            ->when($rec_date, function ($query) use ($rec_date) {
                return $query->where('acc_entryitems.reconciliation_date',NULL);
            })
            ->select(DB::Raw('SUM(acc_entryitems.amount) as total'))

            ->get();

        return $total;
    }

    public static function getOpeningDiff() {
        $total_op = 0;
        $ledgers = Ledger::getAllLedgers();
        foreach ($ledgers as $row => $ledger)
        {
            if ($ledger->op_balance_dc == 'D')
            {
                $total_op = Helpers::calculate($total_op, $ledger->op_balance, '+');
            } else {
                $total_op = Helpers::calculate($total_op, $ledger->op_balance, '-');
            }
        }

        /* Dr is more ==> $total_op >= 0 ==> balancing figure is Cr */
        if (Helpers::calculate($total_op, 0, '>=')) {
            return array('opdiff_balance_dc' => 'C', 'opdiff_balance' => $total_op);
        } else {
            return array('opdiff_balance_dc' => 'D', 'opdiff_balance' => Helpers::calculate($total_op, 0, 'n'));
        }
    }

    public static function getAllLedgers()
    {
        return Ledger::where([['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }

    public static function getCBLedgers()
    {
        return Ledger::where([['deleted_at',NULL],['type',1],['status',1],['module_id',Session::get('module')]])->get();
    }

    public static function getRecLedgers()
    {
        return Ledger::where([['reconciliation',1],['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }

    public static function getGroupLedgers($gid)
    {
        return Ledger::where([['group_id',$gid],['deleted_at',NULL],['status',1],['module_id',Session::get('module')]])->get();
    }
}