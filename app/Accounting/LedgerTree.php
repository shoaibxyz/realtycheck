<?php
namespace App\Accounting;

use App;

class LedgerTree
{
    var $id = 0;
    var $name = '';
    var $code = '';
    var $status = '';

    var $children_groups = array();
    var $children_ledgers = array();

    var $counter = 0;

    var $current_id = -1;

    var $restriction_bankcash = 1;

    var $default_text = 'Please select...';

    var $Group = null;
    var $Ledger = null;

    /**
     * Initializer
     */
    function LedgerTree()
    {
        return;
    }

    /**
     * Setup which group id to start from
     */
    function build($id)
    {
        //dd($id);
        if ($this->current_id == $id) {
            return;
        }

        if ($id == 0)
        {
            $this->id = NULL;
            $this->name = "None";
        } else {
            //$group = $this->Group->find('first', array('conditions' => array('Group.id' => $id)));
            $group = Group::find($id);
            $this->id = $group->id;
            $this->name = $group->name;
            $this->code = $group->code;
            $this->status = $group->status;
        }
        $this->add_sub_ledgers();
        $this->add_sub_groups();
    }

    /**
     * Find and add subgroups as objects
     */
    function add_sub_groups()
    {
        //$conditions = array('Group.parent_id' => $this->id);

        /* If primary group sort by id else sort by name */
        if ($this->id == NULL) {
            /*$child_group_q = $this->Group->find('all', array(
                'conditions' => $conditions,
                'order' => array('Group.id'),
            ));*/
            $child_group_q = Group::getAll($this->id,'id','asc');
        } else {
            /*$child_group_q = $this->Group->find('all', array(
                'conditions' => $conditions,
                'order' => array('Group.name'),
            ));*/
            $child_group_q = Group::getAll($this->id,'name','asc');
        }

        //dd($child_group_q);

        $counter = 0;
        foreach ($child_group_q as $row)
        {
            /* Create new AccountList object */
            $this->children_groups[$counter] = new LedgerTree();

            /* Initial setup */
            //$this->children_groups[$counter]->Group = &$this->Group;
            $this->children_groups[$counter]->current_id = $this->current_id;

            $this->children_groups[$counter]->build($row->id);

            $counter++;
        }
    }

    /**
     * Find and add subledgers as array items
     */
    function add_sub_ledgers()
    {
        $child_ledger_q = Ledger::getGroupLedgers($this->id);
        $counter = 0;
        foreach ($child_ledger_q as $row)
        {
            $this->children_ledgers[$counter]['id'] = $row->id;
            $this->children_ledgers[$counter]['name'] = $row->name;
            $this->children_ledgers[$counter]['code'] = $row->code;
            $this->children_ledgers[$counter]['type'] = $row->type;
            $this->children_ledgers[$counter]['status'] = $row->status;
            $counter++;
        }
    }

    var $ledgerList = array();

    /* Convert ledger tree to a list */
    public function toList($tree, $c = 0)
    {
        /* Add group name to list */
        if ($tree->id != 0) {
            /* Set the group id to negative value since we want to disable it */
            $this->ledgerList[-$tree->id] = $this->space($c) .
                Helpers::toCodeWithName($tree->code, $tree->name);
        } else {
            $this->ledgerList[0] = $this->default_text;
        }

        /* Add child ledgers */
        if (count($tree->children_ledgers) > 0) {
            $c++;
            foreach ($tree->children_ledgers as $id => $data) {
                $ledger_name = Helpers::toCodeWithName($data['code'], $data['name']);
                /* Add ledgers as per restrictions */
                if ($this->restriction_bankcash == 1 ||
                    $this->restriction_bankcash == 2 ||
                    $this->restriction_bankcash == 3) {
                    /* All ledgers */
                    $this->ledgerList[$data['id']] = $this->space($c) . $ledger_name;
                } else if ($this->restriction_bankcash == 4) {
                    /* Only bank or cash ledgers */
                    if ($data['type'] == 1) {
                        $this->ledgerList[$data['id']] = $this->space($c) . $ledger_name;
                    }

                } else if ($this->restriction_bankcash == 5) {
                    /* Only NON bank or cash ledgers */
                    if ($data['type'] == 0) {
                        $this->ledgerList[$data['id']] = $this->space($c) . $ledger_name;
                    }
                }
            }
            $c--;
        }

        /* Process child groups recursively */
        foreach ($tree->children_groups as $id => $data) {
            $c++;
            $this->toList($data, $c);
            $c--;
        }
    }

    function space($count)
    {
        $str = '';
        for ($i = 1; $i <= $count; $i++) {
            $str .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        return $str;
    }
}