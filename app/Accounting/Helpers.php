<?php
namespace App\Accounting;

use App;

class Helpers
{
    public static function statuses()
    {
        $array = array('Pending' => ['color' => 'warning','id' => 0],'Active' => ['color' => 'success','id' => 1],'Inactive' => ['color' => 'info','id' => 2],'Denied' => ['color' => 'danger','id' => 3]);
        return $array;
    }

    public static function getStatus($id)
    {
        $color = '';
        $status = '';
        foreach(App\Accounting\Helpers::statuses() as $key => $value)
        {
            if($value['id'] == $id) {
                $status = $key;
                $color = $value['color'];
            }
        }
        return array($status,$color);
    }

    public static function calculate_withdc($param1, $param1_dc, $param2, $param2_dc) {
        $result = 0;
        $result_dc = 'D';

        if ($param1_dc == 'D' && $param2_dc == 'D') {
            $result = Helpers::calculate($param1, $param2, '+');
            $result_dc = 'D';
        } else if ($param1_dc == 'C' && $param2_dc == 'C') {
            $result = Helpers::calculate($param1, $param2, '+');
            $result_dc = 'C';
        } else {
            if (Helpers::calculate($param1, $param2, '>')) {
                $result = Helpers::calculate($param1, $param2, '-');
                $result_dc = $param1_dc;
            } else {
                $result = Helpers::calculate($param2, $param1, '-');
                $result_dc = $param2_dc;
            }
        }

        return array('amount' => $result, 'dc' => $result_dc);
    }

    public static function calculate($param1 = 0, $param2 = 0, $op = '') {

        $decimal_places = 2;

        if (extension_loaded('bcmath')) {
            switch ($op)
            {
                case '+':
                    return bcadd($param1, $param2, $decimal_places);
                    break;
                case '-':
                    return bcsub($param1, $param2, $decimal_places);
                    break;
                case '==':
                    if (bccomp($param1, $param2, $decimal_places) == 0) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '!=':
                    if (bccomp($param1, $param2, $decimal_places) == 0) {
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                    break;
                case '<':
                    if (bccomp($param1, $param2, $decimal_places) == -1) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '>':
                    if (bccomp($param1, $param2, $decimal_places) == 1) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '>=':
                    $temp = bccomp($param1, $param2, $decimal_places);
                    if ($temp == 1 || $temp == 0) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case 'n':
                    return bcmul($param1, -1, $decimal_places);
                    break;
                default:
                    die();
                    break;
            }
        } else {
            $result = 0;

            if ($decimal_places == 2) {
                $param1 = $param1 * 100;
                $param2 = $param2 * 100;
            } else if ($decimal_places == 3) {
                $param1 = $param1 * 1000;
                $param2 = $param2 * 1000;
            }

            $param1 = (int)round($param1, 0);
            $param2 = (int)round($param2, 0);
            switch ($op)
            {
                case '+':
                    $result = $param1 + $param2;
                    break;
                case '-':
                    $result = $param1 - $param2;
                    break;
                case '==':
                    if ($param1 == $param2) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '!=':
                    if ($param1 != $param2) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '<':
                    if ($param1 < $param2) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '>':
                    if ($param1 > $param2) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case '>=':
                    if ($param1 >= $param2) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                    break;
                case 'n':
                    $result = -$param1;
                    break;
                default:
                    die();
                    break;
            }

            if ($decimal_places == 2) {
                $result = $result/100;
            } else if ($decimal_places == 3) {
                $result = $result/100;
            }

            return $result;
        }
    }

    public static function print_space($count)
    {
        $html = '';
        for ($i = 1; $i <= $count; $i++) {
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        return $html;
    }

    public static function toCurrency($dc, $amount) {

        $decimal_places = 2;

        if (Helpers::calculate($amount, 0, '==')) {
            return Helpers::curreny_format(number_format(0, $decimal_places, '.', ''));
        }

        if ($dc == 'D') {
            if (Helpers::calculate($amount, 0, '>')) {
                return 'Dr ' . Helpers::curreny_format(number_format($amount, $decimal_places, '.', ''));
            } else {
                return 'Cr ' . Helpers::curreny_format(number_format(Helpers::calculate($amount, 0, 'n'), $decimal_places, '.', ''));
            }
        } else if ($dc == 'C') {
            if (Helpers::calculate($amount, 0, '>')) {
                return 'Cr ' . Helpers::curreny_format(number_format($amount, $decimal_places, '.', ''));
            } else {
                return 'Dr ' . Helpers::curreny_format(number_format(Helpers::calculate($amount, 0, 'n'), $decimal_places, '.', ''));
            }
        } else if ($dc == 'X') {
            /* Dr for positive and Cr for negative value */
            if (Helpers::calculate($amount, 0, '>')) {
                return 'Dr ' . Helpers::curreny_format(number_format($amount, $decimal_places, '.', ''));
            } else {
                return 'Cr ' . Helpers::curreny_format(number_format(Helpers::calculate($amount, 0, 'n'), $decimal_places, '.', ''));
            }
        } else {
            return Helpers::curreny_format(number_format($amount, $decimal_places, '.', ''));
        }
        return __d('webzash', 'ERROR');
    }

    public static function toCodeWithName($code, $name) {
        if (strlen($code) <= 0) {
            return $name;
        } else {
            return '[' . $code . '] ' . $name;
        }
    }

    public static function curreny_format($input) {
        /*switch (Configure::read('Account.currency_format')) {
            case 'none':
                return $input;
            case '##,###.##':
                return _currency_2_3_style($input);
                break;
            case '##,##.##':
                return _currency_2_2_style($input);
                break;
            case "###,###.##":
                return _currency_3_3_style($input);
                break;
            default:
                die("Invalid curreny format selected.");
        }*/
        return $input;
    }
    public static function countDecimal($amount) {
        return strlen(substr(strrchr($amount, "."), 1));
    }

    public static function dateToSql($indate) {
        $unixTimestamp = strtotime($indate . ' 00:00:00');
        if (!$unixTimestamp) {
            return false;
        }
        return $unixTimestamp;
    }

    /**
     * This function converts the SQL datetime value to PHP date and time string
     */
    public static function dateFromSql($sqldate) {
        $unixTimestamp = strtotime($sqldate . ' 00:00:00');
        if (!$unixTimestamp) {
            return false;
        }
        return $unixTimestamp;
    }

    public static function toEntryNumber($number, $entrytype_id) {
        $entrytype = EntryType::find($entrytype_id);
        if ($entrytype->zero_padding > 0) {
            return $entrytype->prefix .
                str_pad($number, $entrytype->zero_padding, '0', '0') .
                $entrytype->suffix;
        }
        else {
            return $entrytype->prefix .
                $number .
                $entrytype->suffix;
        }
    }
}