<?php
namespace App\Accounting;

use Illuminate\Support\Facades\Session;
use App;

class AccountList
{
    var $id = 0;
    var $name = '';
    var $code = '';
    var $status = '';

    var $g_parent_id = 0;		/* Group specific */
    var $g_affects_gross = 0;	/* Group specific */
    var $l_group_id = 0;		/* Ledger specific */
    var $l_type = 0;		/* Ledger specific */
    var $l_reconciliation = 0;	/* Ledger specific */
    var $l_notes = '';		/* Ledger specific */

    var $op_total = 0;
    var $op_total_dc = 'D';
    var $dr_total = 0;
    var $cr_total = 0;
    var $cl_total = 0;
    var $cl_total_dc = 'D';

    var $children_groups = array();
    var $children_ledgers = array();

    var $counter = 0;

    var $only_opening = false;
    var $start_date = null;
    var $end_date = null;
    var $affects_gross = -1;

    var $Group = null;
    var $Ledger = null;

    /**
     * Initializer
     */
    function AccountList()
    {
        return;
    }

    /**
     * Setup which group id to start from
     */
    function start($id)
    {
        //dd(Session::get('module'));
        if ($id == 0)
        {
            $this->id = NULL;
            $this->name = "None";
            //dd($this->id);
        }
        else {
            //$group = $this->Group->find('first', array('conditions' => array('Group.id' => $id)));
            $group = Group::where([['id',$id],['module_id',Session::get('module')]])->get();
            //dd($group);
            $this->id = $group[0]->id;
            $this->name = $group[0]->name;
            $this->code = $group[0]->code;
            $this->status = $group[0]->status;
            $this->g_parent_id = $group[0]->parent_id;
            $this->g_affects_gross = $group[0]->affects_gross;
        }

        $this->op_total = 0;
        $this->op_total_dc = 'D';
        $this->dr_total = 0;
        $this->cr_total = 0;
        $this->cl_total = 0;
        $this->cl_total_dc = 'D';

        //echo $this->affects_gross;
        /* If affects_gross set, add sub-ledgers to only affects_gross == 0 */
        if ($this->affects_gross == 1) {
            /* Skip adding sub-ledgers if affects_gross is set and value == 1 */
        } else if ($this->affects_gross == 0) {
            /* Add sub-ledgers if affects_gross is set and value == 0 */
            $this->add_sub_ledgers();
        } else {
            /* Add sub-ledgers if affects_gross is not set == -1 */
            //dd('aaa');
            $this->add_sub_ledgers();
        }

        $this->add_sub_groups();
    }

    /**
     * Find and add subgroups as objects
     */
    function add_sub_groups()
    {
        //$conditions = array('Group.parent_id' => $this->id);

        $affects_gross = -1;
        /* Check if net or gross restriction is set */
        if ($this->affects_gross == 0) {
            //$conditions['Group.affects_gross'] = 0;
            $affects_gross = 0;
        }
        if ($this->affects_gross == 1) {
            //$conditions['Group.affects_gross'] = 1;
            $affects_gross = 1;
        }
        /* Reset is since its no longer needed below 1st level of sub-groups */
        $this->affects_gross = -1;
        //dd($this->id);
        /* If primary group sort by id else sort by name */
        if ($this->id == NULL) {
            /*$child_group_q = $this->Group->find('all', array(
                'conditions' => $conditions,
                'order' => array('Group.id'),
            ));*/
            if($affects_gross != -1)
                $child_group_q = Group::where([['parent_id',$this->id],['affects_gross',$affects_gross],['module_id',Session::get('module')]])->orderBy('id','asc')->get();
            else
                $child_group_q = Group::where([['parent_id',$this->id],['module_id',Session::get('module')]])->orderBy('id','asc')->get();
        } 
        else {
            /*$child_group_q = $this->Group->find('all', array(
                'conditions' => $conditions,
                'order' => array('Group.name'),
            ));*/
            //dd($affects_gross);
            if($affects_gross != -1)
                $child_group_q = Group::where([['parent_id',$this->id],['affects_gross',$affects_gross],['module_id',Session::get('module')]])->orderBy('name','asc')->get();
            else
                $child_group_q = Group::where([['parent_id',$this->id],['module_id',Session::get('module')]])->orderBy('name','asc')->get();
        }
        //dd($child_group_q);
        $counter = 0;
        foreach ($child_group_q as $row)
        {
            /* Create new AccountList object */
            $this->children_groups[$counter] = new AccountList();

            /* Initial setup */
            //$this->children_groups[$counter]->Group = &$this->Group;
            //$this->children_groups[$counter]->Ledger = &$this->Ledger;
            $this->children_groups[$counter]->only_opening = $this->only_opening;
            $this->children_groups[$counter]->start_date = $this->start_date;
            $this->children_groups[$counter]->end_date = $this->end_date;
            $this->children_groups[$counter]->affects_gross = -1; /* No longer needed in sub groups */

            $this->children_groups[$counter]->start($row->id);
            /* echo '<pre>';
            print_r($this->children_groups);
            echo '</pre>'; */
            /* Calculating opening balance total for all the child groups */
            $temp1 = Helpers::calculate_withdc(
                $this->op_total,
                $this->op_total_dc,
                $this->children_groups[$counter]->op_total,
                $this->children_groups[$counter]->op_total_dc
            );
            //print_r($temp1);
            $this->op_total = $temp1['amount'];
            $this->op_total_dc = $temp1['dc'];

            /* Calculating closing balance total for all the child groups */
            $temp2 = Helpers::calculate_withdc(
                $this->cl_total,
                $this->cl_total_dc,
                $this->children_groups[$counter]->cl_total,
                $this->children_groups[$counter]->cl_total_dc
            );
            $this->cl_total = $temp2['amount'];
            $this->cl_total_dc = $temp2['dc'];

            /* Calculate Dr and Cr total */
            $this->dr_total = Helpers::calculate($this->dr_total, $this->children_groups[$counter]->dr_total, '+');
            $this->cr_total = Helpers::calculate($this->cr_total, $this->children_groups[$counter]->cr_total, '+');

            $counter++;
        }
    }

    /**
     * Find and add subledgers as array items
     */
    function add_sub_ledgers()
    {
        /*$child_ledger_q = $this->Ledger->find('all', array(
            'conditions' => array('Ledger.group_id' => $this->id),
            'order' => array('Ledger.name'),
        ));*/
        //$id = ($this->id == NULL) ?
        $child_ledger_q = Ledger::getGroupLedgers($this->id);
        //dd($child_ledger_q);
        $counter = 0;
        //dd(Session::get('module'));
        foreach ($child_ledger_q as $row)
        {
            $this->children_ledgers[$counter]['id'] = $row->id;
            $this->children_ledgers[$counter]['name'] = $row->name;
            $this->children_ledgers[$counter]['code'] = $row->code;
            $this->children_ledgers[$counter]['status'] = $row->status;
            $this->children_ledgers[$counter]['l_group_id'] = $row->group_id;
            $this->children_ledgers[$counter]['l_type'] = $row->type;
            $this->children_ledgers[$counter]['l_reconciliation'] = $row->reconciliation;
            $this->children_ledgers[$counter]['l_notes'] = $row->notes;

            /* If start date is specified dont use the opening balance since its not applicable */
            if (is_null($this->start_date)) {
                $this->children_ledgers[$counter]['op_total'] = $row->op_balance;
                $this->children_ledgers[$counter]['op_total_dc'] = $row->op_balance_dc;
            } else {
                $this->children_ledgers[$counter]['op_total'] = 0.00;
                $this->children_ledgers[$counter]['op_total_dc'] = $row->op_balance_dc;
            }
            /* echo '<pre>';
            print_r($this->children_ledgers);
            echo '</pre>'; */

            /* Calculating current group opening balance total */
            $temp3 = Helpers::calculate_withdc(
                $this->op_total,
                $this->op_total_dc,
                $this->children_ledgers[$counter]['op_total'],
                $this->children_ledgers[$counter]['op_total_dc']
            );
            $this->op_total = $temp3['amount'];
            $this->op_total_dc = $temp3['dc'];

            if ($this->only_opening == true) {
                /* If calculating only opening balance */
                $this->children_ledgers[$counter]['dr_total'] = 0;
                $this->children_ledgers[$counter]['cr_total'] = 0;

                $this->children_ledgers[$counter]['cl_total'] =
                    $this->children_ledgers[$counter]['op_total'];
                $this->children_ledgers[$counter]['cl_total_dc'] =
                    $this->children_ledgers[$counter]['op_total_dc'];
            } else {
                $cl = Ledger::closingBalance(
                    $row->id,
                    $this->start_date,
                    $this->end_date
                );

                $this->children_ledgers[$counter]['dr_total'] = $cl['dr_total'];
                $this->children_ledgers[$counter]['cr_total'] = $cl['cr_total'];

                $this->children_ledgers[$counter]['cl_total'] = $cl['amount'];
                $this->children_ledgers[$counter]['cl_total_dc'] = $cl['dc'];
            }

            /* Calculating current group closing balance total */
            $temp4 = Helpers::calculate_withdc(
                $this->cl_total,
                $this->cl_total_dc,
                $this->children_ledgers[$counter]['cl_total'],
                $this->children_ledgers[$counter]['cl_total_dc']
            );
            $this->cl_total = $temp4['amount'];
            $this->cl_total_dc = $temp4['dc'];

            /* Calculate Dr and Cr total */
            $this->dr_total = Helpers::calculate($this->dr_total, $this->children_ledgers[$counter]['dr_total'], '+');
            $this->cr_total = Helpers::calculate($this->cr_total, $this->children_ledgers[$counter]['cr_total'], '+');

            $counter++;
        }
    }
}