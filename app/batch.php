<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class batch extends Model
{
	 use LogsActivity;
    use SoftDeletes;

    public $table = "batch";

    protected $fillable = ['batch_no','batch_assign_date','agency_id', 'file_qty','parent_id','rate_type'];

 	protected static function boot()
    {
        parent::boot();
        static::updated(function ($batch) {

            $changes = $batch->isDirty() ? $batch->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    activity('batch')
                        ->performedOn($batch)
                        ->log(":causer.name updated batch $attr from {$batch->getOriginal($attr)} to {$batch->$attr}");
                }
            }

        });

        static::created(function ($batch) {

            activity('batch')
                ->performedOn($batch)
                ->log(":causer.name created batch {$batch->batch_name} having account number {$batch->account}");
        });

        static::deleted(function ($batch) {

            activity('batch')
                ->performedOn($batch)
                ->log(":causer.name deleted batch {$batch->batch_name} having account number {$batch->account}");
        });
    }

 	public function agency()
    {
            return $this->belongsTo('App\agency');
    }

    public function files()
    {
    	return $this->hasMany('App\assignFiles');
    }
}
