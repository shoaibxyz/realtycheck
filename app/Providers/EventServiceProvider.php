<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserRegistered' => [
            'App\Listeners\WelcomeEmail',
        ],
        'App\Events\AdminMemberRegistration' => [
            'App\Listeners\SendWelcomeEmailToMember'
        ],
        'Spatie\Backup\Events\BackupWasSuccessful' => [
            'App\Listeners\BackupWasSuccessfulListener',
        ],
        'Spatie\Backup\Events\BackupHasFailed' => [
            'App\Listeners\BackupHasFailedListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
