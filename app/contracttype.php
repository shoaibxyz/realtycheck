<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contracttype extends Model
{
	public $timestamps = false;
	public $table = 'contract_type';
    protected $fillable = ['type'];

    public function property()
    {
    	return $this->hasOne('App\property');
    }
}
