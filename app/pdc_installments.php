<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pdc_installments extends Model
{
    public $table = "pdc_installments";
    protected $fillable = ['pdc_id','installment_id','installment_amount','payment_date','remarks','is_posted','received_amount','registration_no'];
    public $timestamps = false;

    public function pdc()
    {
    	return $this->belongsTo('App\pdc');
    }

    public function pdc_installment()
    {
        return $this->belongsTo('App\memberInstallments');
    }
    
    public function setPaymentDateAttribute($value)
    {
        return $this->attributes['payment_date'] = date('Y-m-d', strtotime($value));
    }
}
