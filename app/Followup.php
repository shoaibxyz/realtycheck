<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Followup extends Model
{
    public $table = "followup";

    use SoftDeletes;
    use LogsActivity;


    protected $fillable = ['registeration_no','meeting_channel','meeting_status','meeting_detail','meeting_done_by','in_out', 'next_meeting_channel', 'meeting_date','next_meeting_channel', 'next_meeting_agenda','next_meeting_date', 'next_meeting_by'];

    protected static function boot()
    {
    parent::boot();
        static::updated(function ($followup) {

            $changes = $followup->isDirty() ? $followup->getDirty() : false;
            
            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ( $followup->getOriginal($attr) == null ) ? "null" : $followup->getOriginal($attr);
                    $getAttr = ($followup->attr == null ) ? "null" : $followup->$attr;

                    if($attr == 'updated_at')
                        continue;

                    activity('Followup')
                        ->performedOn($followup)
                        ->withProperties(['registration_no' => $followup->registeration_no, 'event' => 'updated'])
                        ->log("Updated followup field named $attr from {$getOriginal} to ". $followup->$attr);
                }
            }
        });

        static::created(function ($followup) {
            activity('Followup')
                    ->performedOn($followup)
                    ->withProperties(['event' => 'created', 'registration_no' => $followup->registeration_no])
                    ->log("Created new <a href='".route('followup.show', $followup->registeration_no)."' style='font-weight: bold; color: #f9a533'>followup</a> ");
        });

        static::deleted(function ($followup) {


            activity('Followup')
                    ->performedOn($followup)
                    ->withProperties(['registration_no' => $followup->registeration_no, 'event' => 'Deleted'])
                    ->log("Deleted followup having registration number {$followup->registeration_no}");
        });
    }

    public function files()
    {
        return $this->hasMany('App\followup_files');
    }

    public function getMeetingDateAttribute($value)
    {
        return date('d-m-Y', strtotime($value));
    }

    public function getNextMeetingDateAttribute($value)
    {
        if($value != null)
           return date('d-m-Y', strtotime($value));

        return "00-00-0000";
    }

    public function getNextMeetingByAttribute($value)
    {
         if($value == null)
            return "";

        $user = User::find($value);
        return $user->name;
    }

    public function getMeetingDoneByAttribute($value)
    {

        $user = User::find($value);
        return $user->name;
    }
}
