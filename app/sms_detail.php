<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sms_detail extends Model
{
    protected $fillable = ['sms_id', 'sent_to'];
    public $table = 'sms_detail';
    public $timestamps = false;


    public function sentTo()
    {
    	return $this->belongsTo('App\SMSModel');
    }
}
