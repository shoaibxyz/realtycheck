<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agents extends Model
{
	public $timestamps = false;
	protected $fillable = ['commission','person_id','agency_id'];

	public function person()
	{
		return $this->belongsTo('App\User');
	}

	public function agency()
	{
		return $this->belongsTo('App\agency','agency_id');
	}
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}