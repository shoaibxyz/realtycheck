<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class accounttypes extends Model
{
     public $table = "account_types";
     protected $fillable = ['account_type','account_description'];
}
