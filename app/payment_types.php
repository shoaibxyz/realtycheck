<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_types extends Model
{
    public $table = "payment_type";

    public function otherPayment()
    {
    	return $this->hasOne('App\otherPayments');
    }
}
