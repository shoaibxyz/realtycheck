<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class featureset extends Model
{
	public $timestamps = false;
	public $table = 'feature_set';
    protected $fillable = ['feature','charges', 'charges_description'];

    public function property()
    {
    	return $this->hasMany('App\property','property_features');
    }

    public function booking()
    {
    	return $this->hasMany('App\Payments');
    }

}
