<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
     protected $fillable = [
                'days_or_months',
                'amount_percentage',
                'installment_amount',
                'payment_schedule_id',
                'installment_no'
            ];

    protected $dates = ["installment_date", 'payment_date'];

    // protected $casts = ['received_amount' => 'integer'];

    public function plan()
    {
        return $this->belongsTo('App\paymentSchedule');
    }

    public function setInstallmentDateAttribute($value)
    {
        $this->attributes['installment_date'] = date('Y-m-d',strtotime($value));
    }

    public function scopePaid($query)
    {
        return $query->where('is_paid',1);
    }

    public function scopeUnpaid($query)
    {
        return $query->where('is_paid',0);
    }
}
