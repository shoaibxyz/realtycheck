<?php

namespace App\Listeners;

use App\Events\AdminMemberRegistration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmailToMember
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  AdminMemberRegistration  $event
     * @return void
     */
    public function handle(AdminMemberRegistration $event)
    {
        Mail::to($event->user)->send(new SendWelcomeEmail($event->user->email_verification_code));
    }
}
