<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Spatie\Backup\Events\BackupWasSuccessful;

class BackupWasSuccessfulListener
{
    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */

    public function handle(BackupWasSuccessful $event)
    {
        ini_set('memory_limit', '-1');
        $path = $event->backupDestination->getNewestBackup()->path();
        $fileName = explode('/', $path);
        \Log::info('Path = '. $path);
        \Log::info("Backup done successfully ". $fileName[1] );
        $store = \Storage::disk('google')->put($fileName[1],file_get_contents(storage_path('app/'.$path)));
        \Log::info("Backup done successfully");

        //Get file size
        $fileSize = $this->formatSizeUnits(filesize(storage_path('app/'.$path)));

        $msg = "Backup was succesfully uploaded in Google Drive. File name: " .$fileName[1];
        $msg .= ". File size is: ". $fileSize;
        
        Mail::raw($msg, function ($message){
            $message->to('admin@lahorecapitalcity.com');
            $message->subject('Backup Successful');
        });
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
