<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Spatie\Backup\Events\BackupHasFailed;

class BackupHasFailedListener
{
    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */

    public function handle(BackupHasFailed $event)
    {
        Log::info("Event => " . $event);
        Mail::raw('Backup failed uploaded', function ($message){
            $message->to('rizwansaleem70@gmail.com');
            $message->subject('Backup Failed');
        });
    }
}
