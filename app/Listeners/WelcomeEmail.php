<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\EmailConfirmation;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class WelcomeEmail
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        \Log::info("Listener called");
         if(Mail::to($event->user)->send(new EmailConfirmation($event->user->email_verification_code))){
            \Log::info("Email sent");
         }else{
            \Log::info("Email not sent");
         }
    }
}
