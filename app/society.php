<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class society extends Model
{
	public $table = 'society';
    protected $fillable = ['name', 'address'];

    public function property()
    {
    	return $this->hasOne('App\property');
    }
}
