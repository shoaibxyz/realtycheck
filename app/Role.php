<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Role extends Model
{

    
     public $table = "roles";
    protected $fillable = ['name','display_name','description'];
	public function User()
	{
		return $this->hasMany('App\User','role_person','person_id','role_id');
	}

	public function scopeExceptAdmin($query)
	{
		return $query->except('name','admin');
    }

    public static function getrights($name) {
        //dd(Auth::user()->roles[0]);
        if (Auth::user()->roles[0]->name === 'admin' or Auth::user()->roles[0]->name === 'Super-Admin') {
            $data = array(
                'can_create' => "display:inline-block",
                'show_create' => "display:inline-block",
                'can_view' => true,
                'show_view' => "display:inline-block",
                'can_edit' => "display:inline-block",
                'show_edit' => "display:inline-block",
                'can_delete' => "display:inline-block",
                'show_delete' => "display:inline-block",
                'can_deletep' => "display:inline-block",
                'show_deletep' => "display:inline-block",
                'can_restore' => "display:inline-block",
                'show_restore' => "display:inline-block",
                'can_print' => "display:inline-block",
                'show_print' => "display:inline-block",
            );
        }
        else {
            $objects = \DB::select('SELECT permission_role_new.* FROM `permissions_new`
                            INNER JOIN `permission_role_new` ON `permission_role_new`.`permission_id` = `permissions_new`.`id`
                            WHERE `permission_role_new`.`role_id` = '. Auth::user()->roles[0]->id .'
                            AND `permissions_new`.`name` = "'.$name.'"');
            if(count($objects) > 0) {
                $object = $objects[0];
                $data = array(
                    'can_create' => ($object->can_create == 0) ? false : true,
                    'show_create' => ($object->can_create == 0) ? "display:none" : "display:inline-block",
                    'can_view' => ($object->can_view == 0) ? false : true,
                    'show_view' => ($object->can_view == 0) ? "display:none" : "display:inline-block",
                    'can_edit' => ($object->can_edit == 0) ? false : true,
                    'show_edit' => ($object->can_edit == 0) ? "display:none" : "display:inline-block",
                    'can_delete' => ($object->can_delete == 0) ? false : true,
                    'show_delete' => ($object->can_delete == 0) ? "display:none" : "display:inline-block",
                    'can_deletep' => ($object->can_deletep == 0) ? false : true,
                    'show_deletep' => ($object->can_deletep == 0) ? "display:none" : "display:inline-block",
                    'can_restore' => ($object->can_restore == 0) ? false : true,
                    'show_restore' => ($object->can_restore == 0) ? "display:none" : "display:inline-block",
                    'can_print' => ($object->can_print == 0) ? false : true,
                    'show_print' => ($object->can_print == 0) ? "display:none" : "display:inline-block",
                );
            }
            else {
                $data = array(
                    'can_create' => false,
                    'show_create' => "display:none",
                    'can_view' => false,
                    'show_view' => "display:none",
                    'can_edit' => false,
                    'show_edit' => "display:none",
                    'can_delete' => false,
                    'show_delete' => "display:none",
                    'can_deletep' => false,
                    'show_deletep' => "display:none",
                    'can_restore' => false,
                    'show_restore' => "display:none",
                    'can_assign' => false,
                    'show_assign' => "display:none",
                );
            }
        }
        return json_decode(json_encode($data));
    }

    public static function getMenuItems()
    {
        //dd(Auth::user()->roles);
        if(Auth::user()->roles[0]->name == 'Super-Admin')
            $objects = \DB::select('SELECT * FROM permissions_new where deleted_at is null');
        else
            $objects = \DB::select('SELECT permissions_new.* FROM `permissions_new`
INNER JOIN `permission_role_new` ON `permissions_new`.`id` = `permission_role_new`.`permission_id`
WHERE `permission_role_new`.`role_id` = '. Auth::user()->roles[0]->id .' AND `permission_role_new`.`can_view` = 1 and permissions_new.deleted_at is null');
        //dd($objects);
        $objs[] = '';
        foreach($objects as $object)
        {
            $objs[] = $object->name;
        }
        return $objs;
    }
}

