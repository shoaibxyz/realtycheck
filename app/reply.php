<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reply extends Model

{
    protected $fillable = ['content','reply_date','reply_subject','created_by','updated_by','task_id'];
    public $table = "reply";
    
     public function user()
    {
        return $this->belongsTo('App\user' , 'created_by');
    }

 }