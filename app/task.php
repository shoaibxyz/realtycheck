<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $fillable = ['subject','message_body','created_by','updated_by','stasus'];
    public $table = 'task';

 public function user()
    {
        return $this->belongsTo('App\user' , 'created_by');
    }
    public function category()
    {
        return $this->belongsTo('App\category' , 'category_id');
    }
 }  
 
 
 