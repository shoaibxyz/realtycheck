<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plan extends Model
{
    protected $fillable=['name','status','installament'];
    public $timestamps = false;
    
}
