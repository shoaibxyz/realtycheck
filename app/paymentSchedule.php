<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class paymentSchedule extends Model
{
    use LogsActivity;

    public $table = "payment_schedule";
    protected $fillable = [
        'payment_code',
        'payment_desc',
        'total_price',
        'booking_percentage',
        'booking_price',
        'plot_nature',
        'payment_frequency',
        'plan_type',
        'plot_size_id',
        'installments',
        'plot_dimensions',
        'semi_anual_percentage',
        'possession_percentage',
        'balloting_percentage',
        'pac_percentage',
        'monthly_percentage',
        'is_rescheduled'
    ];


    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($ps) {

            $changes = $ps->isDirty() ? $ps->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_at')
                        continue;
                    activity('payment plan')
                        ->performedOn($ps)
                        ->log("updated payment plan $attr from {$ps->getOriginal($attr)} to {$ps->$attr}");
                }
            }
        });

        static::created(function ($ps) {
            activity('payment plan')
                ->performedOn($ps)
                ->log("created payment plan having code {$ps->payment_code}");
        });

        static::deleted(function ($ps) {
            activity('payment plan')
                ->performedOn($ps)
                ->log("deleted payment plan having code {$ps->payment_code}");
        });
    }

    public function property()
    {
        return $this->belongsTo('App\property');
    }

    public function installment()
    {
        return $this->hasMany('App\Installment');
    }

    public function plotSize()
    {
        return $this->belongsTo('App\plotSize');
    }

    public function reschedule()
    {
        return $this->hasOne('App\reschedule');
    }

    public function booking()
    {
        return $this->hasOne('App\Payments');
    }

    public function scopeNonRescheduled($query)
    {
        return $query->where('is_rescheduled',0)->get();
    }

    public function otherPayments()
    {
        return $this->hasMany('App\otherPayments');
    }
}
