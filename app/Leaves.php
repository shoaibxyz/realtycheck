<?php

namespace App;

use App\Models\HR\Employees;
use App\LeavesRequests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leaves extends Model
{
	use SoftDeletes;
	
    public $table = "hr_leaves";
    protected $fillable = ['name','leaves' , 'created_by' , 'agency_id'];

    protected static function boot()
    {
        parent::boot();
    }
    public function leaveRequest()
    {
    	return $this->hasOne(LeavesRequests::class);
    }

    public function employee()
    {
    	return $this->belongsTo(Employees::class);
    }

}
