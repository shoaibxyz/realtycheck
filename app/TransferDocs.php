<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDocs extends Model
{
    protected $table = "transfer_docs";

    protected $fillable = ['registration_no', 'member_id','doc_id'];

}
