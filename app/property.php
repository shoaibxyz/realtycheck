<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class property extends Model
{
    use Notifiable;
    
	public $table = 'property';
    protected $guarded = ['_token','feature_set'];

    public function contractType()
    {
    	return $this->belongsTo('App\contracttype');
    }

    public function plotType()
    {
    	return $this->belongsTo('App\plottype');
    }

    public function features()
    {
    	return $this->belongsToMany('App\featureset','property_features','property_id','feature_set_id');
    }

    public function society()
    {
    	return $this->belongsTo('App\society');
    }

    public function unit()
    {
        return $this->belongsTo('App\Units');
    }

    public function propertyFeatures()
    {
        return $this->belongsToMany('App\featureset');
    }

    public function payment_schedule()
    {
        return $this->hasMany("App\paymentSchedule");
    }
}
