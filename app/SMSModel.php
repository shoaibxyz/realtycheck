<?php

namespace App;

use App\User;
use App\members;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class SMSModel extends Model
{
    protected $fillable = ['message', 'event' ,'created_by','updated_by','sent_to','sending_date','sent'];

    public $table = "sms";

    use SoftDeletes;
	use LogsActivity;

	protected static function boot()
    {
        parent::boot();
        static::updated(function ($sms) {

            $changes = $sms->isDirty() ? $sms->getDirty() : false;

            if (count($changes) > 0) {

                foreach ($changes as $attr => $value) {
                    $getOriginal = ( $sms->getOriginal($attr) == null ) ? "null" : $sms->getOriginal($attr);
                    $getAttr = ($sms->attr == null ) ? "null" : $sms->$attr;

                    if($attr == 'updated_at')
                        continue;

                    activity('sms')
                        ->performedOn($sms)
                        ->withProperties(['event' => 'updated'])
                        ->log("Updated sms field named $attr from {$getOriginal} to {$getAttr} having this phone number {$sms->sent_to}");
                }
            }
        });

        static::created(function ($sms) {
            activity('sms')
                    ->performedOn($sms)
                    ->withProperties(['event' => 'created'])
                    ->log("Created new sms '{$sms->message}'");
        });

        static::deleted(function ($sms) {

            $checkMember = User::Active()->Members()->find($sms->id);
            $registration_no = null;
            if($checkMember){
                $member = members::where('person_id',$sms->id)->first();
                $registration_no = $member->registration_no;
            }

            activity('sms')
                    ->performedOn($sms)
                    ->withProperties(['event' => 'Deleted'])
                    ->log("Deleted sms {$sms->message}");
        });
    }

    public function SmsDetail()
    {
    	return $this->hasMany('App\sms_detail','sms_id');
    }
}
