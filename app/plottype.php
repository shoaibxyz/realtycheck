<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plottype extends Model
{
	public $timestamps = false;
	public $table = 'plot_type';
    protected $fillable = ['type'];

    public function property()
    {
    	return $this->hasOne('App\property');
    }
}
