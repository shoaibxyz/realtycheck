<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFingerPrint extends Model
{
    public $table = "user_fingerprint";
}
