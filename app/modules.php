<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modules extends Model
{
    public $timestamps = false;

    protected $fillable = ['name','slug'];

	public function permissions()
    {
        return $this->hasMany('App\permissions');
    }
}
