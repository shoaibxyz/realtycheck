<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permissions  extends Model
{
    public $table = "permissions_new";

    protected $fillable = ['name','display_name','description','module_id'];

    /* public function roles()
    {
    	return $this->hasMany('App\roles');
    } */

    public function module()
    {
    	return $this->belongsTo('App\modules');
    }
}
