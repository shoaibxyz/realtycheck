<?php

namespace App;

use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class plotSize extends Model
{

    use SoftDeletes;
    use LogsActivity;
    // use Encryptable;

    // protected $encryptable = [
    //     'plot_size'
    // ];


    public $table = "plot_size";

    protected $fillable = ['plot_size'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($plotSize) {

            $changes = $plotSize->isDirty() ? $plotSize->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_at')
                        continue;

                    activity('plot size')
                        ->performedOn($plotSize)
                        ->withProperties(['event' => 'updated'])
                        ->log("updated plot Size $attr from {$plotSize->getOriginal($attr)} to {$plotSize->$attr}");
                }
            }
        });

        static::created(function ($plotSize) {
            activity('plot size')
                ->performedOn($plotSize)
                ->withProperties(['event' => 'created'])
                ->log("created plotSize having size {$plotSize->plot_size}");
        });

        static::deleted(function ($plotSize) {
            activity('plot size')
                ->performedOn($plotSize)
                ->withProperties(['event' => 'deleted'])
                ->log("deleted plotSize having size {$plotSize->plot_size}");
        });
    }

    public function Plan()
    {
    	return $this->hasOne('App\paymentSchedule');
    }

    public function assigneFile()
    {
    	return $this->hasOne('App\assignFiles');
    }
}
