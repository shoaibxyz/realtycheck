<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class agency extends Model
{
    use LogsActivity;
    use SoftDeletes;

    public $table = "agency";

    protected static $ignoreChangedAttributes = ['updated_at'];
    protected $fillable                       = ['name', 'address', 'cnic_copy', 'created_by', 'updated_by', 'agency_reg_no', 'person_id', 'updated_at', 'contact_no'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($agency) {

            $changes = $agency->isDirty() ? $agency->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_at')
                        continue;

                    activity('Agency')
                        ->performedOn($agency)
                        ->withProperties(['event' => 'updated'])
                        ->log(":causer.name updated agency named {$agency->name} field $attr from {$agency->getOriginal($attr)} to {$agency->$attr}");
                }
            }

        });

        static::created(function ($agency) {

            activity('Agency')
                ->performedOn($agency)
                ->withProperties(['event' => 'created'])
                ->log("Added new agency named {$agency->name}");
        });

        static::deleted(function ($agency) {

            activity('Agency')
                ->performedOn($agency)
                ->withProperties(['event' => 'deleted'])
                ->log("Deleted agency named {$agency->name}");
        });

    }

    public function agents()
    {
    	return $this->hasOne('App\agents');
    }

     public function principal_officer()
    {
        return $this->hasOne('App\principal_officer' , 'agency_id');
    }

    public function files()
    {
    	return $this->hasMany('App\assignFiles');
    }

     public function bookings()
    {
        return $this->hasOne('App\Payments');
    }

}
