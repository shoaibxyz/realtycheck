<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permissionroles extends Model
{
    public $table = "permission_role_new";

    protected $fillable = ['permission_id','role_id','can_view','can_edit','can_delete','can_create','can_deletep','can_restore','can_print'];
}
