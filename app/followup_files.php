<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followup_files extends Model
{
    public $table = "followup_files";

    public $timestamps = false;

    protected $fillable = ['registeration_no','followup_id','file_name'];

    public function followup()
    {
    	return $this->belongsToMany('App\followup','followup_files');
    }
}
