<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transfers extends Model
{
   protected $table = "transfers";
   protected $fillable = ['transfer_no','reg_no','current_user_id','new_user_id','transfer_date','tranfer_charges','tranfer_by','transfer_agent_id','remarks'];
    protected $dates = ['transfer_date', 'ndc_dl_date'];

	public function members()
    {
        return $this->hasMany('App\members');
    } 

    public function newUser()
    {
    	return $this->belongsTo('App\members');
    }
    public function currentUser()
    {
      return $this->belongsTo('App\members');
    }

    public function transferImages()
    {
      return $this->hasMany('App\TransferImages');
    }

    public function tDocuments()
    {
      return $this->hasMany('App\TransferDocuments');
    }
}





