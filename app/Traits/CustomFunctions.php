<?php
namespace App\Traits;

use App\Models\agency;
use App\Models\agents;
use App\Models\principal_officer;
use Illuminate\Support\Facades\Auth;

trait CustomFunctions{

	public function getUsers()
	{
		if(Auth::user()->hasRole('agency'))
		{
			$agency = agency::where('person_id', Auth::user()->id)->first();

			$principalUsers = principal_officer::where('agency_id',$agency->id)->get();

			$usersArr = [];
			$usersArr[] = Auth::user()->id;

			foreach($principalUsers as $principal)
			{
				$usersArr[] = $principal->person_id;
				$agents = agents::where('principal_id', $principal->id)->get();
				foreach($agents as $agent)
				{
					$usersArr[] = $agent->person_id;
				}
			}

			$agents = agents::where('agency_id', $agency->id)->get();
			foreach($agents as $agent)
			{
				$usersArr[] = $agent->person_id;
			}

			$array = array_unique($usersArr);

			return $array;
		}

		if(Auth::user()->hasRole('principal-officer'))
		{
			$principalUsers = principal_officer::where('person_id',Auth::user()->id)->first();
			$usersArr = [];
			$usersArr[] = $principalUsers->person_id;

			$agents = agents::where('principal_id', $principalUsers->id)->get();
			foreach($agents as $agent)
			{
				$usersArr[] = $agent->person_id;
			}
			return $usersArr;
		}

		if(Auth::user()->hasRole('Agent'))
		{
			return [Auth::user()->id];
		}
	}

	public  function getAgency()
	{
		if(Auth::user()->hasRole('agency'))
		{
			$agency = agency::where('person_id', Auth::user()->id)->first();
			return $agency;
		}

		if(Auth::user()->hasRole('principal-officer'))
		{
			$principalUsers = principal_officer::with(['agency'])->where('person_id',Auth::user()->id)->first();
			return $principalUsers->agency;
		}

		if(Auth::user()->hasRole('Agent'))
		{
			$agent = agents::with(['agency'])->where('person_id', Auth::user()->id)->first();
			return $agent->agency;
		}
	}

	public static function getAgencyStatic()
	{
		if(Auth::user()->hasRole('agency'))
		{
			$agency = agency::where('person_id', Auth::user()->id)->first();
			return $agency;
		}

		if(Auth::user()->hasRole('principal-officer'))
		{
			$principalUsers = principal_officer::with(['agency'])->where('person_id',Auth::user()->id)->first();
			return $principalUsers->agency;
		}

		if(Auth::user()->hasRole('Agent'))
		{
			$agent = agents::with(['agency'])->where('person_id', Auth::user()->id)->first();
			return $agent->agency;
		}
	}


	public function getUserLocation($lat, $lng)
	{
		$key = env('GOOGLE_MAP_API');
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$key";
		
		$data = json_decode(file_get_contents($url));
		
		return $data->results[0]->formatted_address;
	}
}