<?php

/**
 * Trail for dealing SMS using bizSMS.pk API
 */

namespace App\Traits;

use Ixudra\Curl\Facades\Curl;

trait SMS
{
	/**
	 * SendSMS
	 *
	 * @var message type string
	 * @var PhoneNo "Format: 923001234567
	 */
    function SendSMS($message, $PhoneNo)
	{
        $response = Curl::to('http://my.ezeee.pk/sendsms_url.html')
                 ->withData( 
                 	array( 
                		
                 		'Username' => '03554012423',
                 		'Password' => '@03554012423',
                 		'From' => 'CapitalCity',
                 		'To' => $PhoneNo,
                 		'Message' => $message,
                 		'language' => 'English',
                 	) 
                 )
                 ->get();
        
        return $response;
	}


	/**
	 * Sending Push Notification to users
	 *
	 * @var title  type string
	 * @var body type string
	 * @var topic type string (Role to send notification)
	 * @var additional data  type array
	 */
    public static function SendNotification($title = null, $body = null, $topic = 'all',$postedData = null)
	{

		$data = array( 
                'to' => '/topics/'.$topic, 
                'priority' => 'high',
                'restricted_package_name' => '', 
                "notification" => array(
                     "title" => $title,
                    "body" => $body,
                    "sound" => "default",
                    "click_action" => "FCM_PLUGIN_ACTIVITY",
                    "icon" => "fcm_push_icon",
                ),
                "data" => $postedData,
            );

	    $response = Curl::to('https://fcm.googleapis.com/fcm/send')
            ->withData( $data )
            ->withContentType('application/json')
            ->withHeader('Authorization: key='.env('FCM_AUTH_KEY'))
            ->asJson( true )
            ->post();
	}

}