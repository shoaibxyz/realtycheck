<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balloting extends Model
{
    public $table = "balloting";

    protected $fillable = [
    	'registration_no',
    	'preference',
    	'plot_no'
    ];
}
