<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member_files extends Model
{
    public $table = 'member_files';

    public function member()
    {
    	return $this->hasOne('App\members');
    }
}
