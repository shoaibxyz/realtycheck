<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BallotPlot extends Model
{
	protected $fillable = [
		'registration_no',
		'block',
		'plot_no',
		'feature_set',
		'is_assigned',
		'location',
		'street',
		'dimensions'
	];	

	public $timestamps = false;
}
