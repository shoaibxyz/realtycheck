<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FingerPrint extends Model
{
    public $table = 'user_fingerprint';
}
