<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $fillable = ['name','created_by','updated_by'];
    public $table = 'category';
    
    public function task()
    {
        return $this->belongsTo('App\task' , 'category_id');
    }
    
      public function user()
    {
        return $this->belongsTo('App\user' , 'created_by');
    }

}



