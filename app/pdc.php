<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class pdc extends Model
{

    //use SoftDeletes;
    use LogsActivity;
    
	public $table = "pdc";

    protected $fillable = ['instrument_type','instrument_date','receiving_date','amount','reference_no','id_no','deposit_by','mobile','pdc_remarks','created_by','acknowledgement_no','drawee_bank'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($pdc) {

            $changes = $pdc->isDirty() ? $pdc->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at')
                        continue;
                    activity('pdc')
                        ->performedOn($pdc)
                        ->log("updated pdc $attr from {$pdc->getOriginal($attr)} to {$pdc->$attr}");
                }
            }
        });

        static::created(function ($pdc) {
            activity('pdc')
                ->performedOn($pdc)
                ->log("created pdc having acknowledgement no {$pdc->acknowledgement_no}");
        });

        static::deleted(function ($pdc) {
            activity('pdc')
                ->performedOn($pdc)
                ->log("deleted pdc having acknowledgement number {$pdc->acknowledgement_no}");
        });
    }

    public function installments()
    {
        return $this->hasMany('App\pdc_installments');
    }

    public function bank()
    {
        return $this->belongsTo('App\banks');
    }

    public function setReceivingDateAttribute($value)
    {
        return $this->attributes['receiving_date'] = date('Y-m-d', strtotime($value));
    }

    public function setDepositDateAttribute($value)
    {
        return $this->attributes['deposit_date'] = date('Y-m-d', strtotime($value));
    }

    public function setClearReturnDateAttribute($value)
    {
        return $this->attributes['clear_return_date'] = date('Y-m-d', strtotime($value));
    }
}
