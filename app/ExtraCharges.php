<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraCharges extends Model
{
    public $table = "extra_charges";
}
