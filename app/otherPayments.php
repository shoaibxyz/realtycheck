<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class otherPayments extends Model
{
    public $table = "other_payments";

    protected $fillable = [
    			'payment_type_id',
    			'payment_schedule_id',
    			'installment_no',
    			'installment_date_creteria',
    			'installment_date',
    			'days_or_months',
    			'amount_creteria',
    			'amount_percentage',
    			'amount_to_receive',
    ];

    public function setInstallmentDateAttribute($value)
    {
        $this->attributes['installment_date'] = date('Y-m-d',strtotime($value));
    }

    public function paymentSchedule()
    {
        return $this->belongsTo('App\paymentSchedule');
    }

    public function paymentType()
    {
        return $this->belongsTo('App\payment_types');
    }
}
