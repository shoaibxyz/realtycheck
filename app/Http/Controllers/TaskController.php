<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\task;
use App\reply;
use App\category;
use Mail; 
use App\User;
use App\Mail\sendtask;

class TaskController extends Controller
{
    

    Public function index(){

         $task = task::orderBy('id', 'desc')->get();
         $category = category::orderBy('name', 'asc')->get();
         
        //dd($task);
        return view('admin.support.index' , compact('task','category'));
    }

    public function create()
    {
         $category = category::orderBy('name', 'ASC')->get();

        return view ('admin.support.create', compact('category'));
    }

    public function store(Request $request){

        $task = new task();
        
        //dd($request->input());
        

        $task->Subject = $request->input('subject');
        $task->Message_body = $request->input('message_body');
        $task->Created_by = Auth::user()->id;
        $task->Updated_by = Auth::user()->id;
        $task->status = 1;
        $task->category_id = $request->input('category');
           
        $task->save();
        
        $content = task::latest()->first();
        
        $user   = User::where('id' , Auth::user()->id)->first();
        $receiverAddress = [$user, 'talhabilal1@outlook.com' , 'tehreematique92@gmail.com'];
        
        $mail = Mail::to($receiverAddress)->send(new Sendtask($content));
        if($mail){
            dd('yes');
        }else{
            dd('no');
        }
        
        return redirect()->route('task');
    }
     public function changestatus(Request $request)
     
    {
        // dd($request->input());
        $task  = task::find($request->task_id);

        $task->support_status = $request->input('support_status');
        $task->save();

       
        return redirect('admin/task');

    }

        
    Public function reply1($id){

         $task = task::find($id);
         
         $reply = reply::where ('task_id' , $id)->orderBy('id','DESC')->get();
         
         //dd($task);
        
        return view('admin.support.reply' , compact('task' , 'reply'));
    }
    
    
     public function replystore(Request $request){

        $task = task::where('id',$request->input('task_id'))->first();
        //dd($task->id);
        
            $reply = new reply(); 

        $reply->content = $request->input('reply');
        $reply->reply_date = date('Y-m-d');
        $reply->reply_subject =$task->subject  ;
        $reply->created_by = Auth::user()->id;
        $reply->updated_by = Auth::user()->id;
        $reply->task_id = $task->id;
           
        $reply->save();

        return redirect()->back();
    }

         public function status(Request $request)
    {
        // dd($request->input());
        
        $task  = task::find($request->task_id);

        $task->status = $request->input('support_status');
        $task->save();

        return redirect('admin/task');

    }
    
     public function find_task($id)
    {
        $task = task::find($id);

        return response()->json(['task' => $task]);
    }
    
     public function category_task($id)
     {
         $task =task::where('category_id',$id)->get();
         $category = category::orderBy('name', 'ASC')->get();
         
         return view ('admin.support.index', compact('task','category'));
         
     }

       
   }
  

