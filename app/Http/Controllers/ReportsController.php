<?php

namespace App\Http\Controllers;

use App\Followup;
use App\LPC;
use App\Payments;
use App\SMSModel;
use App\User;
use App\agency;
use App\alerts;
use App\assignFiles;
use App\bank;
use App\batch;
use App\memberInstallments;
use App\members;
use App\paymentSchedule;
use App\plotSize;
use App\receipts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Yajra\Datatables\Datatables;
use App\Role;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use DateTime;
use DatePeriod;
use DateInterval;

class ReportsController extends Controller
{
    public function index()
    {

  $rights = Role::getrights('receipt-daily-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
       return view('admin/report/daily-receipt');
    }

    public function viewBankReport()
    {
        $rights = Role::getrights('bank-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin/report/bank');
    }

    public function CheckReceiptReport(Request $request, $startdate = null, $enddate = null)
    {
        $startdate = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $enddate = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));
        
        //dd($startdate , $enddate);

        //$report = receipts::whereNull('deleted_at')->whereBetween('receiving_date', [$startdate, $enddate])->where('is_active',1)->where('booking_cancelled',0)->where('receipt_no', 'NOT LIKE', 'IWR%')->get();
        $report = receipts::select(DB::Raw('SUM(received_amount) as received_amount, receipt_no , created_by, receipt_remarks, registration_no, rebate_amount, receiving_date, receiving_mode, receiving_type'))
        ->whereNull('deleted_at')->whereBetween('receiving_date', [$startdate, $enddate])->where('is_active', 1)->where('booking_cancelled', 0)
        ->groupBy('receipt_no')->get();
        
        /*$member = [];
        foreach ($report as $key => $value) {
            $memberInfo = members::where('registration_no', $value->registration_no)->first();
            if (!is_null($memberInfo)) {
                // echo $value->registration_no."<br>";
                $mybooking = Payments::where('registration_no',$value->registration_no)->where('status',1)->first();
                if (!is_null($mybooking)) {
                    $booking[] = $mybooking;
                    $member[] = $memberInfo;
                }
            }
        }*/

        //dd(count($member));

        // die;
        return view('admin/report/daily-receipt-report', compact('startdate', 'enddate', 'report'));
    }
    public function viewbatchFiles(Request $request)
    {
        $rights = Role::getrights('batch-files-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
            
        // dd("check");
        $agency_batches = [];
        $agencies = agency::whereNull('deleted_at')->orderBy('name','asc')->get();
        
        $batchesArr = batch::distinct()->get(['batch_no']);
        return view('admin.report.batch-report', compact('agencies','batchesArr', 'agency_batches', 'rights'));
    }

    public function checkBatchFiles(Request $request)
    {
        $rights = Role::getrights('batch-files-report');
        $from = $request->input('from_date');
        $from_date = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to_date = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));
        $batch_no = $request->input('batch_no');
        $agency_id = $request->input('agency_id');
        
        $batches = batch::when($from, function($query) use ($from_date, $to_date){
            $query->whereBetween('batch_assign_date',[$from_date, $to_date]);
        })
        ->when($batch_no, function($query) use ($batch_no){
             $query->where('batch_no', 'like','%'.$batch_no.'%');
        })
        ->when($agency_id, function($query) use ($agency_id){
            $query->where('agency_id', $agency_id);
        })
        ->groupBy('agency_id')->groupBy('batch_no')->get();
        // dd($batches);
        
         
         $agency_batches = [];
        if($agency_id)
        {
            $agency_batches = batch::select('batch_no')->where('agency_id', $agency_id)->get();
        }
        
        $files = [];
        $filesCommercial = [];
        $filesResidential = [];
        $filesHousing = [];

        foreach ($batches as $key => $batch) {
            $commercial = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Commercial')->where('batch_id', $batch->id)->whereNull('deleted_at')->whereNull('is_cancelled')->distinct()->get();
            if(count($commercial) > 0)
                $filesCommercial[] = $commercial;

            $residential = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Residential')->where('batch_id', $batch->id)->whereNull('deleted_at')->whereNull('is_cancelled')->distinct()->get();
            if(count($residential) > 0)
                $filesResidential[] = $residential;

            $housing = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Housing')->where('batch_id', $batch->id)->whereNull('deleted_at')->whereNull('is_cancelled')->distinct()->get();
            if(count($housing) > 0)
                $filesHousing[] = $housing;
            
            // $files[] = assignFiles::where('agency_id', $batch->agency->id)->where('batch_id', $batch->id)->distinct()->get();
        }

        $agencies  = agency::all();

        $batchesArr = batch::distinct('batch_no')->groupBy('batch_no')->get();
    
        return view('admin.report.batch-report', compact('batches', 'agencies', 'filesCommercial','filesResidential','filesHousing','batchesArr','agency_batches', 'rights','from_date','to_date'));
    }



    public function bankreport(Request $request, $startdate = null, $enddate = null)
    {
        $startdate = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $enddate   = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));

        $cash   = receipts::whereBetween('receiving_date', array($startdate, $enddate))->where('receiving_mode','cash')->sum('received_amount');

        // $cheque   = receipts::whereBetween('receiving_date', array($startdate, $enddate))->where('receiving_mode','cheque')->sum('received_amount');

        $cheque = DB::table('receipts')
            ->join('bank', 'receipts.bank_id', '=', 'bank.id')
            ->where('receipts.received_amount', '>', 0)
            ->whereBetween('receiving_date', array($startdate, $enddate))
            ->get();

        $banks = bank::all();
        return view('admin/report/bank-report', compact('cash', 'cheque','banks'));
    }


    public function dueAmounts()
    {
        $rights = Role::getrights('due-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return view('admin.report.due-amounts', compact('agencies'));
    }

    public function dueAccumulatedAmounts()
    {
        $rights = Role::getrights('due-report');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        if(!$rights->can_view){
            abort(403);
        }

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return view('admin.report.due-amounts-accumulated', compact('agencies'));
    }


    public function CheckdueAmounts(Request $request)
    {
        $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));
//dd($to);
     
        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        $agency = $request->agency_id;

        if($agency)
        {   
            $bookingArr=[];
            $bookings = Payments::where('agency_id',$agency)->where('status', 1)->where('is_cancelled',0)->get();
            foreach($bookings as $booking)
            {
                $bookingArr[] = $booking->registration_no;
            }

            $installments = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('is_cancelled' , 0)->get();
        }else{

            $installments = memberInstallments::whereBetween('installment_date', [$from, $to])->where('is_paid', 0)->where('is_active_user', 1)->where('is_cancelled' , 0)->get();
        }
        //dd($installments);
        $booking_inst = [];
        foreach ($installments as $key => $installment) {

            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->where('is_cancelled',0)->first();
            // if($installment->registration_no == 'CC-GS2-1421'){
            //     $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->where('is_cancelled',0)->toSql();
            //     // dd($booking);    
            // }
            

            //echo $installment->registration_no;
            if(!is_null($booking)){
            
                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $member[] = $memberInfo; 
                        $person[] = $is_member_active;
                        $bookings[] = $booking;
                        $booking_inst[] = $installment;
                        //$lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        $lpc = LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount, SUM(ifnull(amount_received,0)) as amount_received, SUM(ifnull(inst_waive,0)) lpc_waive, SUM(ifnull(os_amount,0)) - SUM(ifnull(inst_waive,0)) as os_amount, calculation_date'))
                        ->where('registration_no', $booking->registration_no)->first();
                        if($lpc)
                            $latePaymentCharges[] = $lpc;
                    }
                }
            }
        }
    
    
        // dd($bookings);

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();

        // foreach ($bookings as $key => $booking) {
        //             echo $booking->paymentPlan->plotSize;
        //         }        

        return view('admin.report.due-amounts', compact('agencies','installments','member','person','bookings','latePaymentCharges', 'booking_inst'));
    }

    public function CheckdueAccumulatedAmounts(Request $request)
    {
        $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));

        
        
        $duePersons = [];
        $due_amount = [];
        $rec_amount = [];
        $sumArr = [];
        $regArr = [];
        $installments2 = '';

        $agency = $request->agency_id;
        
      
        if($agency)
        {
            $bookings = Payments::where('agency_id',$agency)->where('status', 1)->where('is_cancelled', 0)->get();
            foreach($bookings as $booking)
            {
                $bookingArr[] = $booking->registration_no;
            }

            $installments = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('due_amount','>', 0)->where('is_cancelled', 0)->get();
            $installments2 = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_active_user', 1)->where('due_amount','>', 0)->where('is_cancelled', 0)->get();
        }
        else{

            $installments = memberInstallments::where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('due_amount','>', 0)->where('is_cancelled', 0)->get();
            $installments2 = memberInstallments::where('installment_date', '<=', $to)->where('is_active_user', 1)->where('due_amount','>', 0)->where('is_cancelled', 0)->get();
        }

        foreach ($installments->chunk(10) as $key => $installment) {

            foreach($installment as $inst)
            {
                $sumDueAmount = 0;
                
                $due_amount[$inst->registration_no][] = ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount; 
                // $due_amount[$inst->registration_no]['due_amount'][] = $inst->due_amount;
            }
        }
        
        $i=0;
        $received_sum = 0;
        foreach ($installments2->chunk(10) as $key => $installment) 
        {
            foreach($installment as $inst)
            {
                $received = 0;
                $install_rebate = 0;
                $rebate = 0;
                
                if($inst->is_paid == 1 || ( $inst->is_paid == 0  && $inst->os_amount != 0)  )  
                    $received += ($inst->os_amount > 0) ? $inst->due_amount - $inst->os_amount - $inst->rebat_amount : $inst->amount_received;
                //$install_rebate += $inst->rebat_amount;                      
                
                //$booking      = Payments::where('registration_no', $inst->registration_no)->first();
                
                /*if($booking and $booking->rebate_amount)
                    $rebate = $booking->rebate_amount;
                else if($install_rebate)*/
                    //$rebate = $install_rebate;
                    
                //$net_price = $booking->paymentPlan->total_price - $booking->rebate_amount;
                
                //$received_sum += ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount;
                
                $rec_amount[$inst->registration_no][] = $received;
                
                $i++;
            }
        }

        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        $dueAmounts = [];

        $i = 1;
        foreach ($due_amount as $key => $installment) {

            $booking = Payments::where('registration_no', $key)->where('status', 1)->where('is_cancelled',0)->first();
            
            //echo $installment->registration_no;
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $dueSum = 0;
                        $recSum = 0;
                        
                        foreach ($due_amount[$key] as $key3 => $reg) {
                            
                            $dueSum += $reg;
                        }
                        
                        foreach ($rec_amount[$key] as $key4 => $reg) {
                            
                            $recSum += $reg;
                        }
                        
                        $assignFile = assignFiles::where('registration_no', $booking->registration_no)->first();

                        // $dueAmounts['sum'][$key][] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['sum'] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['member'] = $memberInfo; 
                        $dueAmounts[$assignFile->plot_type][$key]['person'] = $is_member_active;
                        $dueAmounts[$assignFile->plot_type][$key]['bookings'] = $booking;
                        $dueAmounts[$assignFile->plot_type][$key]['inst'] = $installment;
                        $dueAmounts[$assignFile->plot_type][$key]['recSum'] = $recSum;

                        /*$lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        if($lpc)
                            $dueAmounts[$assignFile->plot_type]['latePaymentCharges'] = $lpc->lpc_amount + $lpc->lpc_other_amount;*/
                    }
                }
            }
        }


        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        //dd($dueAmounts);
        // foreach ($bookings as $key => $booking) {
        //             dd($booking->paymentPlan->plotSize) ;
        //         }        

        return view('admin.report.due-amounts-accumulated', compact('dueAmounts','member','person','bookings','latePaymentCharges','agencies'));
    }


    public function CustomerFeedbackReportForm()
    {
             $rights = Role::getrights('due-report');
            if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            if(!$rights->can_view){
            abort(403);
            }

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return view('admin.report.feedback-report', compact('agencies', 'rights'));
    }



    public function CustomerFeedbackReport(Request $request)
    {
        $rights = Role::getrights('c-h-customers-report');
        $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));

        $duePersons = [];
        $due_amount = [];
        $sumArr = [];
        $regArr = [];

        $agency = $request->agency_id;

        if ($agency) {

            $bookings = Payments::leftjoin('assign_files', 'assign_files.registration_no', '=', 'bookings.registration_no')->where('bookings.agency_id', $agency)->where('bookings.status', 1)->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])->get();


$bookingArr=[];
            foreach ($bookings as $booking) {
                $bookingArr[] = $booking->registration_no;
            }

            $reg_no = "'". implode("', '", $bookingArr) ."'";

            // dd($reg_no);

            $query = "select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`installment_date` <= '" . $to . "'
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 0
                and 
                    member_installments.registration_no IN ($reg_no)
            UNION ALL
            select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 1
                and 
                    member_installments.registration_no IN  ($reg_no)
            ";
            // dd($query);

            $installments = DB::select($query);

            // $installments = memberInstallments::leftjoin('assign_files', 'assign_files.registration_no', '=', 'member_installments.registration_no')
            //     ->whereIn('member_installments.registration_no', $bookingArr)
            // // ->where('member_installments.is_paid', 0)
            //     ->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])
            //     // ->where('member_installments.installment_date', '<=', $to)
            //     ->where('member_installments.is_active_user', 1)
            //     ->where('member_installments.payment_desc', '!=', 'Down Payment')
            //     ->get();
        } else {

            // $installments = memberInstallments::rightjoin('assign_files', 'assign_files.registration_no', '=', 'member_installments.registration_no')
            //     ->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])
            //     ->where('member_installments.payment_desc', '!=', 'Down Payment')
            //     ->where('member_installments.is_active_user', 1)
            //     ->where('member_installments.installment_date', '<=', $to)
            //     ->orWhere('member_installments.is_paid', 1);

            $query = "select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`installment_date` <= '". $to ."'
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 0
            UNION ALL
            select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 1
            ";
            
            $installments = DB::select($query);
                // ->toSql();
        }

         //dd($installments);

        // dd(User::getQuery($installments));


        
        foreach ($installments as $key => $inst) {
            
            // foreach ($installment as $inst) {
                $sumDueAmount = 0;
                $status = ($inst->is_paid == 1) ? "paid" : "unpaid";
                if ($inst->is_active_user = 1) {
                    if ($inst->is_paid == 1)
                        $due_amount[$inst->registration_no]['paid'][] = $inst->amount_received;
                    elseif($inst->os_amount > 0){

                        $due_amount[$inst->registration_no]['paid'][] = $inst->due_amount - $inst->os_amount;
                        $due_amount[$inst->registration_no]['unpaid'][] = $inst->os_amount;
                        $due_amount[$inst->registration_no]['partial'][] = 1;
                    }
                    else
                        $due_amount[$inst->registration_no]['unpaid'][] = $inst->due_amount;
                    
                } else {
                    $due_amount[$inst->registration_no]['unpaid'][] = 0;
                }
                
                $due_amount[$inst->registration_no]['due_amount'][] = $inst->due_amount;
            // }
        }
        //dd($due_amount);

        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        $dueAmounts = [];

        $i = 1;
        foreach ($due_amount as $key => $installment) {
            $booking = Payments::with(['agency'])->where('bookings.registration_no', $key)
                ->where('bookings.status', 1)
                ->where('bookings.is_cancelled', 0)
                ->first();
            // Payments::join('assign_files', 'assign_files.registration_no','=','bookings.registration_no')->where('bookings.agency_id',$agency)->where('bookings.status', 1)->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])->get();
            //echo $installment->registration_no;
            if ($booking) {

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                if ($memberInfo) {
                    $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();
                    if ($is_member_active) {
                        $dueSum = 0;
                        $paidDueSum = 0;

                        if (isset($due_amount[$key]['unpaid'])) {
                            foreach ($due_amount[$key]['unpaid'] as $key3 => $reg) {

                                $dueSum += $reg;
                            }
                        }

                        if (isset($due_amount[$key]['paid'])) {
                            foreach ($due_amount[$key]['paid'] as $key4 => $reg2) {

                                $paidDueSum += $reg2;
                            }
                        }

                        $assignFile = assignFiles::where('registration_no', $booking->registration_no)->whereIn('plot_type', ['Commercial', 'Housing'])->first();

                        $followup = Followup::where('registeration_no', $booking->registration_no)->orderBy('id', 'DESC')->limit(1)->first();

                        // $dueAmounts['sum'][$key][] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['sum'] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['paid'] = $paidDueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['member'] = $memberInfo;
                        $dueAmounts[$assignFile->plot_type][$key]['person'] = $is_member_active;
                        $dueAmounts[$assignFile->plot_type][$key]['bookings'] = $booking;
                        $dueAmounts[$assignFile->plot_type][$key]['inst'] = isset($installment['unpaid']) ? $installment['unpaid'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['paid_inst'] = isset($installment['paid']) ? $installment['paid'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['partial'] = isset($installment['partial']) ? $installment['partial'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['followup'] = $followup ? $followup->meeting_detail : "";

                        // dd($dueAmounts);

                        // dd($dueAmounts);

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id', 'DESC')->first();
                        if ($lpc) {
                            $dueAmounts[$assignFile->plot_type]['latePaymentCharges'] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                        }

                    }
                }
            } else {
                $assignFiles = assignFiles::with(['agency'])->where('registration_no', $key)->whereIn('plot_type', ['Commercial', 'Housing'])->first();
                //Open Files
                $dueAmounts[$assignFile->plot_type][$key]['sum'] = 0;
                $dueAmounts[$assignFile->plot_type][$key]['paid'] = 0;
                $dueAmounts[$assignFile->plot_type][$key]['inst'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['paid_inst'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['partial'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['followup'] = "open";
                $dueAmounts[$assignFile->plot_type][$key]['file'] = $assignFiles;
                // dd($dueAmounts);
            }
        }

        // dd($dueAmounts);

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        // dd($dueAmounts['CC-GS2-1820']['bookings']->paymentPlan->plotSize->plot_size);
        // foreach ($bookings as $key => $booking) {
        //             echo $booking->paymentPlan->plotSize;
        //         }

        //dd($dueAmounts);
return view('admin.report.feedback-report', compact('dueAmounts', 'member', 'person', 'bookings', 'latePaymentCharges', 'agencies', 'rights'));
        
    }

    public function overdueAmounts()
    {

           $rights = Role::getrights('over-due-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin.report.overdue-amounts');
    }




      public function CheckOverDueAmounts(Request $request)
    {
        
        $startdate = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $enddate = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));

        $installments   = memberInstallments::whereBetween('installment_date',[$startdate,$enddate])->where('is_cancelled',0)->where('is_paid',0)->where('installment_amount','>',0)->get();
        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            //echo $installment->registration_no;
            if($booking){
                $memberInfo = members::where('registration_no', $installment->registration_no)->where('is_current_owner',1)->first();
                $member[] = $memberInfo; 
                $person[] = User::find($memberInfo->person_id);
            }
        }

        $sdate = $request->input('startdate');
        $edate = $request->input('enddate');
        return view('admin.report.overdue-amounts', compact('installments','member','person'));
    }



    public function bookings()
    {
              $rights = Role::getrights('booking-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
        
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return view('admin.report.bookings', compact('agencies'));
    }

    public function BookingReport(Request $request)
    {
        $startdateInput = $request->input('startdate');
        $enddateInput = $request->input('enddate');

        $startdate = date('Y-m-d', strtotime(strtr($startdateInput, '/', '-')));
        $enddate = date('Y-m-d', strtotime(strtr($enddateInput, '/', '-')));
        $agency = $request->agency_id;
        //dd($startdate);
        $bookings = Payments::with(['agency' , 'paymentPlan'])
        ->leftjoin('assign_files', 'assign_files.registration_no', '=', 'bookings.registration_no')
        ->leftjoin('batch', 'batch.id', '=', 'assign_files.batch_id')

        ->when($agency, function($query) use ($agency){
            $query->where('bookings.agency_id', $agency);
        })
        ->when($startdateInput, function($query) use ($startdate, $enddate){
            $query->whereBetween('bookings.payment_date', [$startdate,$enddate]);
        })
        ->where('bookings.status' , 1)->where('bookings.is_cancelled',0)->whereNull('bookings.deleted_at')->get();

         //dd($bookings);

        $BookingDetail['booking'] = $bookings;
        $member = null;
        $plotsArr = [];
        $agency = [];
        $reg_noArr = [];
        $bookingsArr = [];
        foreach ($bookings as $booking) {

            $agency[] = $booking->agency->name;

            //$plotSize =  $booking->paymentPlan->plotSize->plot_size;
            
            $plotsize =  plotSize::join('payment_schedule','payment_schedule.plot_size_id','plot_size.id')->where('payment_schedule.id',$booking->payment_schedule_id)->whereNull('plot_size.deleted_at')->select(DB::Raw('plot_size.plot_size'))->first();
            $plot_size = '';
            if($plotsize)
                $plot_size = $plotsize->plot_size;
            //dd($plot_size);
            $assignedFile = assignFiles::with('batch')->where('registration_no', $booking->registration_no)->first();
            $reg_noArr[] = $booking->registration_no . " - " . $booking->payment_date . " - "  . $assignedFile->plot_type;
            if($booking->paymentPlan->plot_nature == "Commercial")
                $nature = 'C';

            if($booking->paymentPlan->plot_nature == "Residential")
                $nature = 'R';

            if($booking->paymentPlan->plot_nature == "Housing")
                $nature = 'H';


            $plotsArr[$assignedFile->plot_type][$plot_size][] = $plot_size;

            $bookingsArr[$assignedFile->plot_type][$booking->agency->name][$plot_size][$assignedFile->batch->rate_type][] = 1;

        }

        //dd($plotsArr);

        // dd($bookingsArr);

        $plots = [];
        foreach($plotsArr as $key => $plot)
        {   
            foreach($plot as $key2 => $plo)
                $plots[$key][] = $key2;
        }

        foreach($bookingsArr as $key => $plotType)
        {
            // dd($plots[$key]);
            foreach($plotType as $key3 => $plotsSize)
            {
                foreach($plots[$key] as $key4 => $plot)
                {
                    foreach($plotsSize as $key2 => $plotss)
                    {
                        if($plot !== $key2 && !isset($bookingsArr[$key][$key3][$plot])){   

                            $bookingsArr[$key][$key3][$plot] = [
                                'old' => null
                            ];
                        }
                    }
                }
            }
            // dd($bookingsArr);
        }
        // dd($book);
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return view('admin.report.bookings', compact('bookingsArr','startdate', 'enddate', 'agencies' , 'plotsArr'));
    }

    public function checkPaidInst(Request $request)
    {
               $rights = Role::getrights('paid-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin.report.paid-installments');
    }

    public function viewPaidInst(Request $request)
    {
        $startdate = date('Y-m-d', strtotime(strtr($request->input('startdate') , '/' , '-')));
        $enddate = date('Y-m-d', strtotime(strtr($request->input('enddate') , '/' , '-')));

        $installments = memberInstallments::whereBetween('installment_date', [$startdate,$enddate])->where('is_cancelled',0)->where('is_paid' , 1)->where('is_active_user', 1)->get();
        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];
        $reg = [];

        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            // echo $installment->registration_no."<br>";
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $member[] = $memberInfo; 
                        $person[] = $is_member_active;
                        $bookings[] = $booking;

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        if($lpc)
                            $latePaymentCharges[] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                    }
                }
            }
        }
        // die;
        return view('admin.report.paid-installments', compact('installments','member','person','bookings','latePaymentCharges'));
    }


    public function DailyIncome()
    {
               $rights = Role::getrights('reports');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin.report.daily-income');
    }

    public function CheckDailyIncome(Request $request)
    {
        $payment_date = date('Y-m-d', strtotime(strtr($request->input('payment_date'), '/', '-')));
        $inst = memberInstallments::Paid()->where('payment_date',$payment_date)->get();
        dd($inst);
    }

    public function detailbatch($id)
    {

        $files = []; 
        $files['agency'] = agency::find($id);
        $files['assignedFiles'] = assignFiles::where('agency_id' , $files['agency']->id)->get();
        $member = members::all();
        
        foreach ($files['assignedFiles']  as $f) {
            $member = members::where('registration_no' , $f->registration_no )->first();
            if($member)
            {
                $files['member'][] = $member;
                $files['person'][] = User::find($member->person_id);     
                $files['booking'][] = Payments::where('registration_no', $f->registration_no)->where('status',1)->first();    
            }
        }
        // dd($files);
        return view ('admin.report.batch-detail' , compact('files'));
    }

    public function saleSummary()
    {
               $rights = Role::getrights('sale-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
        
        // \Artisan::call('cache:clear');
        // dd("here");
        // $bookings = Payments::Active();


        // $assignedFiles = assignFiles::all();
        $assignedFiles = assignFiles::join('batch','batch.id','=','assign_files.batch_id')->get();

        $agency          = [];
        $batch           = [];
        $received_amount = [];
        $agencyAdded     = [];
        $received_sumArr = [];
        $agencyData      = [];
        $calcData        = [];
        $i = 1;
        foreach ($assignedFiles as $assignedFile) {

            $booking = Payments::where('registration_no', $assignedFile->registration_no)->first();

            // if (count($booking) > 0) {`
                $calcData[$assignedFile->agency->name]['total'][] = $assignedFile->plotSize->Plan->total_price;
                if (!in_array($assignedFile->agency->name, $agencyAdded)) {
                    $agency[]      = $assignedFile->agency;
                    $agencyAdded[] = $assignedFile->agency->name;

                    $agencyFiles[] = batch::where('agency_id', $assignedFile->agency->id)->get();
                }

                $received_sum = 0;
                $installments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 0)->get();
                foreach ($installments as $key => $installment) {
                    $received_sum += $installment->amount_received;
                }

                $ballotingAmount    = 0;
                $memberInstallments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 1)->get();
                foreach ($memberInstallments as $key => $installment) {
                    if ($installment->payment_desc == "Balloting") {
                        $ballotingAmount += $installment->amount_received;
                    }

                }

                $downPayment        = 0;
                $memberInstallments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 1)->get();
                foreach ($memberInstallments as $key => $installment) {
                    if ($installment->payment_desc == "Down Payment") {
                        $downPayment += $installment->amount_received;
                    }

                }

                $calcData[$assignedFile->agency->name]['installmentReceived'][] = $received_sum;
                $calcData[$assignedFile->agency->name]['booking'][]             = $assignedFile->plotSize->Plan->booking_price;
                $calcData[$assignedFile->agency->name]['Balloting'][]           = $ballotingAmount;
                $calcData[$assignedFile->agency->name]['downPayment'][]         = $downPayment;
            // }else{
            //     $calcData[$assignedFile->agency->name]['test'][]         = $i++;   
            // }
        }


        // dd(usort($calcData, [$this, 'sortByValue']));
        $fileQty = [];

        foreach ($agencyFiles as $files) {
            $agencyFilesTotal = 0;
            $receivedSum      = 0;
            $totalSum         = 0;
            $booking          = 0;
            $balloting        = 0;
            $downPayment      = 0;
            foreach ($files as $file) {

                foreach ($calcData[$file->agency->name]['installmentReceived'] as $value) {
                    $receivedSum += $value;
                }
                foreach ($calcData[$file->agency->name]['total'] as $value) {
                    $totalSum += $value;
                }
                foreach ($calcData[$file->agency->name]['booking'] as $value) {
                    $booking += $value;
                }

                foreach ($calcData[$file->agency->name]['Balloting'] as $value) {
                    $balloting += $value;
                }

                foreach ($calcData[$file->agency->name]['downPayment'] as $value) {
                    $downPayment += $value;
                }
                // $agencyFilesTotal += $file->file_qty;
                $agencyFilesTotal += count($file->files);
            }
            $agencyData[$file->agency->name]['fileQty']             = $agencyFilesTotal;
            $agencyData[$file->agency->name]['installmentReceived'] = $receivedSum;
            $agencyData[$file->agency->name]['totalAmount']         = $totalSum;
            // $agencyData[$file->agency->name]['bookingReceived'] = $booking;
            $agencyData[$file->agency->name]['ballotingReceived'] = $balloting;
            $agencyData[$file->agency->name]['downPayment']       = $downPayment;
            $agencyData[$file->agency->name]['totalReceived']     = $receivedSum + $downPayment + $balloting;
            $agencyData[$file->agency->name]['currReceievable']   = $totalSum - ($receivedSum + $downPayment + $balloting);
            $agencyData[$file->agency->name]['agency']   = $file->agency->name;

        }
        //    $files = 0;
        // foreach ($agencyData as $key => $value) {
        //     $files += $value['fileQty'];
        // }
        // // dd($files);
        // dd($agencyData);

         usort($agencyData, function($a, $b) {
            // dd($a);
            $retval = $b['fileQty'] - $a['fileQty'];
            // if ($retval == 0) {
            //     $retval = $a['suborder'] - $b['suborder'];
            //     if ($retval == 0) {
            //         $retval = $a['details']['subsuborder'] - $b['details']['subsuborder'];
            //     }
            // }
            return $retval;
        });

        

        return view('admin.report.sale-summary', compact('agencyData'));
    }





//the below method is edited by Habib
      public function defaulters(Request $request)
    {
     $rights = Role::getrights('defaulters-report');
    if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

if($request->input('enddate'))
        {
            // $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
            $to   = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));
            $installments = memberInstallments::selectRaw('*, sum(due_amount) as sum')->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->groupBy('registration_no')->get();

            $member             = [];
            $person             = [];
            $bookings           = [];
            $latePaymentCharges = [];
            $lpc                = [];

            foreach ($installments as $key => $installment) {
                $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
                //echo $installment->registration_no;
                if ($booking) {
                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo) {
                        $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();
                        if ($is_member_active) {
                            $member[]   = $memberInfo;
                            $person[]   = $is_member_active;
                            $bookings[] = $booking;

                            $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id', 'DESC')->first();
                            if ($lpc) {
                                $latePaymentCharges[] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                            }

                        }
                    }
                }
            }

            $user = User::find(Auth::user()->id);

            // foreach ($bookings as $key => $booking) {
            //             echo $booking->paymentPlan->plotSize;
            //         }


        return view('admin.report.defaulters', compact('installments', 'member', 'person', 'bookings', 'latePaymentCharges', 'user'));
        }

        return view('admin.report.defaulters');
    }






    public function alert()
    {
                  $rights = Role::getrights('alert-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin.report.alert');
    }

    public function checkAlertReport(Request $request)
    {
        $from_date = date('Y-m-d', strtotime($request->get('fromdate')));
        $to_date   = date('Y-m-d', strtotime($request->get('todate')));

        $alerts = alerts::whereBetween('created_at', [$from_date, $to_date])->get();
        return view('admin.report.alert', compact('alerts'));
    }

    public function transferReport(Request $request, $startdate = null, $enddate = null)
    {
                 $rights = Role::getrights('transfer-report');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
            
            $startdate = $request->input('startdate');
            $enddate = $request->input('enddate');
       
            $to = ($startdate) ? date('Y-m-d', strtotime(strtr($startdate, '/', '-'))) : $startdate;
            $from = ($enddate) ? date('Y-m-d', strtotime(strtr($enddate, '/', '-'))) : $enddate;
            $registration_no   = $request->input('registration_no');

            // $transferReport = transfers::whereBetween('transfer_date', [$from, $to]);
            $transferReport = DB::table('transfers')
                ->when($registration_no, function ($query) use ($registration_no) {
                        return $query->where('reg_no', $registration_no);
                })
                ->when($from, function ($query) use ($from, $to) {
                        return $query->whereBetween('transfer_date', [$from, $to]);
                })
                ->get();

            // dd($transferReport);
            return view('admin/report/transfer', compact('startdate', 'enddate', 'transferReport'));
    }

     public function cancelReport()
    {
        return view('admin.report.cancel-report');
    }

    public function Checkcancelreport(Request $request)
    {
        $from = '';
        $to = '';
        if($request->input('startdate'))
            $from   = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        if($request->input('enddate'))
            $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));
        
        $status = $request->input('status');
        
        if($status < 3)
        {
            $bookings = Payments::where('is_cancelled', $status)
            ->when($from, function($query) use ($from, $to){
                $query->whereBetween('cancel_date', [$from, $to]);
            })
            ->orderBy('cancel_date','desc')
            ->get();
            
            $user = User::find(Auth::user()->id);

            return view('admin.report.cancel-report', compact('bookings' , 'user'))->with('st',$request->input('status'));
            
        }
        else
        {
            /*$booking = Payments::join('booking_merged','booking_merged.booking_id','bookings.id')->where('is_cancelled', $status)
            ->when($from, function($query) use ($from, $to){
                $query->whereBetween('cancel_date', [$from, $to]);
            })->select(DB::Raw('bookings.*', 'booking_merged.merged_into', 'booking_merged.amount'))
            ->orderBy('cancel_date','desc')
            
            ->get();
            
            $bookings = $booking->unique('registration_no');

            $bookings->values()->all();*/
            
            $bookings = Payments::where('is_cancelled', $status)
            ->when($from, function($query) use ($from, $to){
                $query->whereBetween('cancel_date', [$from, $to]);
            })->select(DB::Raw('bookings.*'))
            ->orderBy('cancel_date','desc')
            ->get();
            
            $user = User::find(Auth::user()->id);
            //dd($bookings);
            return view('admin.report.cancel-report', compact('user', 'bookings'))->with('st',$request->input('status'));
            
        }
        // dd($booking);
        
    }
    
    public function mergedReport()
    {
        return view('admin.report.open_merged');
    }

    public function Checkmergedreport(Request $request)
    {
        $from = '';
        $to = '';
        if($request->input('startdate'))
            $from   = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        if($request->input('enddate'))
            $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));
        
        $status = $request->input('status');
        
       //dd($status);
            $files =DB::table('assign_files')-> where('is_cancelled', $status)
            ->when($from, function($query) use ($from, $to){
                $query->whereBetween('cancel_date', [$from, $to]);
            })
            ->orderBy('cancel_date','desc')
            ->get();
            
       
        //dd($files);
        $user = User::find(Auth::user()->id);

        return view('admin.report.open_merged', compact('files' , 'user'));
    }

    public function ActivityLogs(Request $request)
    {
        $from = date('Y-m-d', strtotime($request->input('fromdate')))." 00:00:00";
        $to   = date('Y-m-d', strtotime($request->input('todate')))." 23:59:00";

        $activities = Activity::whereBetween('created_at', [$from, $to])->orderBy('id', 'DESC')->get();
        
        return view('admin.report.activity', compact('activities'));
    }
    
    public function recoveryReport2(Request $request)
    {
        $agencies = agency::all();
        if($request->fromdate)
        {
            $from = date('Y-m-d', strtotime($request->input('fromdate')));
            $to   = date('Y-m-d', strtotime($request->input('todate')));
            $agency_id   = $request->input('agency_id');

            $agencyInfo = "";
            if($agency_id)
            {
                $agencyInfo = agency::find($agency_id);
                $bookingArr = [];
                $bookings = Payments::where('agency_id',$agency_id)->where('status', 1)->get();
                foreach($bookings as $booking)
                {
                    $bookingArr[] = $booking->registration_no;
                }

                $installments = memberInstallments::whereIn('registration_no', $bookingArr)->whereBetween('installment_date',[$from,$to])->where('is_cancelled',0)->where('is_paid', 0)->where('is_active_user', 1)->where('payment_desc', '=', 'Installment')->get();
            }else{

                $installments = memberInstallments::whereBetween('installment_date',[$from,$to])->where('is_cancelled',0)->where('is_paid', 0)->where('is_active_user', 1)->where('payment_desc', '=', 'Installment')->get();
            }



            $member = [];
            $person = [];
            $bookings = [];
            $latePaymentCharges = [];
            $lpc = [];
            $due_amount = [];

            foreach ($installments as $key => $installment) {
                $booking = Payments::with('agency')->where('registration_no', $installment->registration_no)->where('status', 1)->first();

                dd($booking);
                //echo $installment->registration_no;
                if($booking){

                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                    if($memberInfo)
                    {
                        $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                        if($is_member_active)
                        {
                            $month = date('M Y', strtotime($installment->installment_date));
                            $due_amount[$month]['unpaid'][] = $installment->due_amount;
                            
                        }
                    }
                }

                //dd($booking);
            }


            $report = receipts::whereBetween('receiving_date', [$from, $to])->where('is_active',1)->where('receiving_type',1)->where('receipt_no', 'NOT LIKE', 'IWR%')->where('booking_cancelled',0)->get();

            $member = [];
            foreach ($report as $key => $value) {

                if($agency_id)
                {
                    $booking = Payments::where('registration_no', $value->registration_no)->where('status', 1)->where('agency_id', $agency_id)->first();
                    if($booking)
                    {
                        $month = date('M Y', strtotime($value->receiving_date));
                        $due_amount[$month]['paid'][] = ($value->received_amount - $value->rebate_amount);                        
                    }
                }else{
                    $month = date('M Y', strtotime($value->receiving_date));
                    $due_amount[$month]['paid'][] = ($value->received_amount - $value->rebate_amount);
                }
            }


            $recovery = [];
            foreach($due_amount as $key => $months)
            {
                $paid = 0;
                $unpaid = 0;
                foreach($months as $key2 => $amounts)
                {

                    if($key2 == 'paid')
                        foreach($amounts as $amount){
                            $paid += $amount;
                        }

                    else
                        foreach($amounts as $amount){
                            $unpaid += $amount;
                        }
                }
                $recovery[$key]['paid'] = $paid;
                $recovery[$key]['unpaid'] = $unpaid;
            }
            
            // dd($recovery);

            return view('admin.report.recovery', compact('recovery','agencies', 'agencyInfo'));

        }else{
            return view('admin.report.recovery', compact('agencies'));
        }   

    }
    
    public function recoveryReport(Request $request)
    {
        $rights = Role::getrights('recovery-report');
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        $from = date('Y-m-d', strtotime($request->input('fromdate')));
        $to   = date('Y-m-d', strtotime($request->input('todate')));
        $agency_id   = $request->input('agency_id');
        
        $installsDueByMon = $this->instDue($from, $to, $agency_id, 'Residential','month');
        $installsDueByMon_Com = $this->instDue($from, $to, $agency_id, 'Commercial','month');
        $installsDueByMon_Hou = $this->instDue($from, $to, $agency_id, 'Housing','month');
        //$instDueByMon = '';
        $installs = array();
        
        if(count($installsDueByMon) > 0)
        {
            foreach($installsDueByMon as $inst)
            {
                $instDueByMon[$inst->mon] = $inst->due;
            }
            //dd($instDueByMon);
            $installs['Residential']['instDueByMon'] = $instDueByMon;
        }
        if(count($installsDueByMon_Com) > 0)
        {
            foreach($installsDueByMon_Com as $inst)
            {
                $instDueByMon_Com[$inst->mon] = $inst->due;
            }
            //dd($instDueByMon);
        }
        if(count($installsDueByMon_Hou) > 0)
        {
            foreach($installsDueByMon_Hou as $inst)
            {
                $instDueByMon_Hou[$inst->mon] = $inst->due;
            }
            //dd($instDueByMon);
        }
        
        $installsRecByMon = $this->instRec($from, $to, $agency_id, 'Residential','month');
        $installsRecByMon_Com = $this->instRec($from, $to, $agency_id, 'Commercial','month');
        $installsRecByMon_Hou = $this->instRec($from, $to, $agency_id, 'Housing','month');
        //dd($installsRecByMon);
        if(count($installsRecByMon) > 0)
        {
            foreach($installsRecByMon as $inst)
            {
                $instRecByMon[$inst->mon] = $inst->received;
            } 
            //dd($instRecByMon);
            $installs['Residential']['instRecByMon'] = $instRecByMon;
        }
        if(count($installsRecByMon_Com) > 0)
        {
            foreach($installsRecByMon_Com as $inst)
            {
                $instRecByMon_Com[$inst->mon] = $inst->received;
            } 
            //dd($instRecByMon);
        }
        if(count($installsRecByMon_Hou) > 0)
        {
            foreach($installsRecByMon_Hou as $inst)
            {
                $instRecByMon_Hou[$inst->mon] = $inst->received;
            } 
            //dd($instRecByMon);
        }
        
        $installsDueByAgent = $this->instDue($from, $to, $agency_id, 'Residential','agency');
        $installsDueByAgent_Com = $this->instDue($from, $to, $agency_id, 'Commercial','agency');
        $installsDueByAgent_Hou = $this->instDue($from, $to, $agency_id, 'Housing','agency');
        //dd($installsDueByAgent);
        if(count($installsDueByAgent) > 0)
        {
            foreach($installsDueByAgent as $inst)
            {
                $instDueByAgent[$inst->agent] = $inst->due;
            }
            //dd($instDueByAgent);
            $installs['Residential']['instDueByAgent'] = $instDueByAgent;
        }
        if(count($installsDueByAgent_Com) > 0)
        {
            foreach($installsDueByAgent_Com as $inst)
            {
                $instDueByAgent_Com[$inst->agent] = $inst->due;
            }
            //dd($instDueByAgent);
        }
        if(count($installsDueByAgent_Hou) > 0)
        {
            foreach($installsDueByAgent_Hou as $inst)
            {
                $instDueByAgent_Hou[$inst->agent] = $inst->due;
            }
            //dd($instDueByAgent);
        }
        
        $installsRecByAgent = $this->instRec($from, $to, $agency_id, 'Residential','agency');
        $installsRecByAgent_Com = $this->instRec($from, $to, $agency_id, 'Commercial','agency');
        $installsRecByAgent_Hou = $this->instRec($from, $to, $agency_id, 'Housing','agency');
        
        if(count($installsRecByAgent) > 0)
        {
            foreach($installsRecByAgent as $inst)
            {
                $instRecByAgent[$inst->agent] = $inst->received;
            }
            //dd($instRecByAgent);
            $installs['Residential']['instRecByAgent'] = $instRecByAgent;
        }
        if(count($installsRecByAgent_Com) > 0)
        {
            foreach($installsRecByAgent_Com as $inst)
            {
                $instRecByAgent_Com[$inst->agent] = $inst->received;
            }
            //dd($instRecByAgent);
        }
        if(count($installsRecByAgent_Hou) > 0)
        {
            foreach($installsRecByAgent_Hou as $inst)
            {
                $instRecByAgent_Hou[$inst->agent] = $inst->received;
            }
            //dd($instRecByAgent);
        }
        
        return view('admin.report.recovery', compact('rights', 'from','to', 'agency_id','agencies'));
    } 
    
    public function instDue($from,$to,$agency_id,$plot_nature,$rpt_mode)
    {
        if($rpt_mode == 'month')
        {
            $installsDue = memberInstallments::join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->select(DB::Raw('month(installment_date) as mon,sum(member_installments.due_amount) as due'))
                                ->groupBy('mon')
                                ->get();
            
        }
        elseif($rpt_mode == 'agency')
        {
            $installsDue = memberInstallments::join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('agency','agency.id','bookings.agency_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->select(DB::Raw('agency.name as agent,sum(member_installments.due_amount) as due'))
                                ->groupBy('agent')
                                ->get();
            //dd($installsDue);
        }
        return $installsDue;
    }
    
    public function instRec($from,$to,$agency_id,$plot_nature,$rpt_mode)
    {
        if($rpt_mode == 'month')
        {
            $installsRec = memberInstallments::join('installment_receipts','installment_receipts.installment_id','member_installments.id')
                                ->join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->whereNull('installment_receipts.deleted_at')
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->where(function ($query) {
                                    $query->where('is_paid', 1)->orWhere('member_installments.os_amount','>',0);
                                })
                                ->select(DB::Raw('month(installment_date) as mon, sum(installment_receipts.received_amount) as received'))
                                ->groupBy('mon')
                                ->get();
                                //dd($installsRec);
            
        }
        elseif($rpt_mode == 'agency')
        {
            $installsRec = memberInstallments::join('installment_receipts','installment_receipts.installment_id','member_installments.id')
                                ->join('bookings','bookings.registration_no','member_installments.registration_no')
                                ->join('members','members.registration_no','bookings.registration_no')
                                ->join('persons','persons.id','members.person_id')
                                ->join('agency','agency.id','bookings.agency_id')
                                ->join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')
                                ->whereBetween('installment_date',[$from,$to])
                                ->where('member_installments.is_cancelled',0)
                                ->where('is_active_user', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('members.is_current_owner',1)
                                ->where('persons.status',1)
                                ->where('payment_schedule.plot_nature',$plot_nature)
                                ->whereNull('installment_receipts.deleted_at')
                                ->when($plot_nature, function($query) use ($plot_nature){
                                    if($plot_nature == 'Residential')
                                        $query->where('member_installments.payment_desc', 'Installment');
                                })
                                ->when($agency_id, function($query) use ($agency_id){
                                    $query->where('bookings.agency_id', $agency_id);
                                })
                                ->where('is_paid',1)->orWhere('member_installments.os_amount','>',0)
                                ->select(DB::Raw('agency.name as agent, sum(installment_receipts.received_amount) as received'))
                                ->groupBy('agent')
                                ->get();
                            
        }
        return $installsRec;
    }
    
    public function rebateType($rebate_type)
    {
        return view('admin.report.discount_type_report', compact('rebate_type'));
    }
    
    public function InventoryReport(Request $request)
    {
        $rights = Role::getrights('inventory-report');
        $agency = $request->agency_id;
        
        //dd($agency);

        $batches = batch::with(['agency'])
        ->when($agency, function($query) use ($agency){
            $query->where('agency_id', $agency);
        })
        ->get();

        
        $member = null;
        $CommericalArr = [];
        $ResidentialArr = [];
        $HousingArr = [];
        $agencyBatch = [];
        $issuedArr = [];
        $CommericalBatchArr = [];
        $ResidentialBatchArr = [];
        $HousingBatchArr = [];


        $filesCommercial = [];
        $filesResidential = [];
        $filesHousing = [];


        $agencyIssuedFiles = [];
        $agencyBookedFiles = [];


        $agencyRemainingFiles = [];
        $housingOpenBookingsArr = [];

        foreach ($batches as $batch) {

            $commercial = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Commercial')->where('batch_id', $batch->id)->distinct()->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->get();
            if(count($commercial) > 0)
                $filesCommercial[] = $commercial;

            $residential = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Residential')->where('batch_id', $batch->id)->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->distinct()->get();
            if(count($residential) > 0)
                $filesResidential[] = $residential;

            $housing = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Housing')->where('batch_id', $batch->id)->distinct()->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->get();
            if(count($housing) > 0)
                $filesHousing[] = $housing;


            //Residential
            $agencyOldIssuedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->count();

            $agencyNewIssuedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->count();

            $agencyOldBookedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->join('bookings','bookings.registration_no','assign_files.registration_no')->where('bookings.is_cancelled',0)->where('bookings.status',1)->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->count();

            $agencyNewBookedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->join('bookings','bookings.registration_no','assign_files.registration_no')->where('bookings.is_cancelled',0)->where('bookings.status',1)->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')->count();


            //Commercial
            $commercialIssuedFiles = assignFiles::with(['plotSize','agency'])
            ->join('batch', 'batch.id','=','assign_files.batch_id')
            ->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')
            ->AgencyFilesCount($batch->agency->id, 'Commercial')
            ->get();

            $commercialFilesCount = [];
            $commercialBookedFilesCount = [];
            $remainingFilesCount = [];
            $remainingFilesCount = [];

            foreach($commercialIssuedFiles as $key => $commercialFiles)
            {
                    $agencyOldIssuedFiles['Commercial'][$batch->agency->name][$commercialFiles->plotSize->plot_size][$commercialFiles->batch_no] = assignFiles::whereNull('assign_files.deleted_at')->IssuedFiles($commercialFiles->batch_id, $commercialFiles->plot_size_id, 'Commercial');

                    $commercialOpenBookings = Payments::join('assign_files', 'assign_files.registration_no', '=', 'bookings.registration_no')
                        ->join('members', 'members.registration_no', '=', 'bookings.registration_no')
                        ->join('persons', 'persons.id', '=', 'members.person_id')
                        ->where('assign_files.plot_type', 'Commercial')
                        ->where('assign_files.agency_id', $batch->agency_id)
                        ->where('assign_files.plot_size_id', $commercialFiles->plot_size_id)
                        ->where('persons.name', 'like', 'open%')
                        ->where('bookings.is_cancelled',0)
                        ->whereNull('bookings.deleted_at')
                         ->where('bookings.status',1)
                        ->count();

                        // $query = User::getQuery($commercialOpenBookings);

                    $commercialOpenBookingsArr['Commercial'][$batch->agency->name][$commercialFiles->plotSize->plot_size] = $commercialOpenBookings;


                
                if($commercialFiles->is_reserved == 1)
                    $agencyOldBookedFiles['Commercial'][$batch->agency->name][$commercialFiles->plotSize->plot_size][$commercialFiles->batch_no] = assignFiles::whereNull('assign_files.deleted_at')->where('assign_files.batch_id', $commercialFiles->batch_id)->join('bookings','bookings.registration_no','assign_files.registration_no')->where('bookings.is_cancelled',0)->where('bookings.status',1)
                        ->where('assign_files.plot_size_id', $commercialFiles->plot_size_id)
                        ->where('assign_files.is_reserved',1)->whereNull('assign_files.is_cancelled')->count();

                        

                $commercialFilesCount[] = $commercialFiles->id;
                $commercialBookedFilesCount[] = $commercialFiles->id;
                // $remainingFilesCount[] = $commercialFiles->id;
             }


             //Commercial
            $HousingIssuedFiles = assignFiles::with(['plotSize'])
            ->join('batch', 'batch.id','=','assign_files.batch_id')
            ->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')
            ->AgencyFilesCount($batch->agency->id, 'Housing')
            ->get();

            $housingFilesCount = [];
            $housingBookedFilesCount = [];
            $housingRemainingFilesCount = [];
            foreach($HousingIssuedFiles as $HousingFiles)
            {
                // if($HousingFiles->registration_no == "CC-RH-8M-0977")
                //     dd($HousingFiles);
                // if(!in_array($HousingFiles->id ,$housingFilesCount))
                $agencyOldIssuedFiles['Housing'][$batch->agency->name][$HousingFiles->plotSize->plot_size][$HousingFiles->batch_no] = assignFiles::whereNull('assign_files.deleted_at')->IssuedFiles($HousingFiles->batch_id, $HousingFiles->plot_size_id, 'Housing');

                if($HousingFiles->is_reserved == 1)
                    $agencyOldBookedFiles['Housing'][$batch->agency->name][$HousingFiles->plotSize->plot_size][$HousingFiles->batch_no] = $agencyOldBookedFiles['Housing'][$batch->agency->name][$HousingFiles->plotSize->plot_size][$HousingFiles->batch_no] = assignFiles::where('assign_files.batch_id', $HousingFiles->batch_id)->join('bookings','bookings.registration_no','assign_files.registration_no')->where('bookings.is_cancelled',0)->where('bookings.status',1)
                        ->where('assign_files.plot_size_id', $HousingFiles->plot_size_id)
                        ->where('assign_files.is_reserved',1)->whereNull('assign_files.is_cancelled')->count();


                if(!in_array($HousingFiles->id ,$housingRemainingFilesCount) && $HousingFiles->is_reserved == 0)
                    $agencyRemainingFiles['Housing'][$batch->agency->name][$HousingFiles->plotSize->plot_size][$HousingFiles->batch_no] = assignFiles::whereNull('assign_files.deleted_at')->RegisteredFiles($HousingFiles->batch_id, 0);

                $housingFilesCount[] = $HousingFiles->id;
                $housingBookedFilesCount[] = $HousingFiles->id;
                $housingRemainingFilesCount[] = $HousingFiles->id;


                $housingOpenBookings = Payments::join('assign_files', 'assign_files.registration_no', '=', 'bookings.registration_no')
                ->join('members', 'members.registration_no', '=', 'bookings.registration_no')
                ->join('persons', 'persons.id', '=', 'members.person_id')
                ->where('assign_files.plot_type', 'Housing')
                ->where('assign_files.agency_id', $batch->agency_id)
                ->where('assign_files.plot_size_id', $HousingFiles->plot_size_id)
                ->where('persons.name', 'like', 'open%')
                 ->where('bookings.is_cancelled',0)
                 ->whereNull('bookings.deleted_at')
                  ->where('bookings.status',1)
                ->count();

                // $query = User::getQuery($housingOpenBookings);

                $housingOpenBookingsArr['Housing'][$batch->agency->name][$HousingFiles->plotSize->plot_size] = $housingOpenBookings;


             }
        
        }
        // dd($commercialOpenBookingsArr);
        // dd($agencyOldBookedFiles['Housing']);

        //Residential Files
        foreach($filesResidential as $residential)
        {
            foreach($residential as $files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $ResidentialArr)){
                    $agencyBatches = batch::select('batch.parent_id', 'batch.rate_type','batch.file_qty', 'batch.batch_no')
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Residential')
                    ->whereNull('assign_files.is_cancelled')
                    ->whereNull('assign_files.deleted_at')
                    // ->distinct('parent_id')
                    ->get();

                    // dd($agencyBatches);

                    $checkFile = [];
                    foreach($agencyBatches as $abatch)
                    {
                        // if(!in_array($abatch->file_qty, $checkFile))
                            $ResidentialBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;

                        // $checkFile[] = $abatch->file_qty;
                    }
                    $ResidentialArr[] = $files->batch->agency_id;

                }
            }
        }
        // dd($ResidentialBatchArr);
        //Commercial Files
        foreach($filesCommercial as  $Commercial)
        {
            foreach($Commercial as $files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $CommericalArr)){
                    $agencyBatches = batch::select('batch.parent_id', 'batch.rate_type','batch.file_qty', 'batch.batch_no')
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Commercial')
                    ->whereNull('assign_files.is_cancelled')
                    ->get();

                    foreach($agencyBatches as $abatch)
                    {
                        // $commercialOpenBookingsArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $query;
                        $CommericalBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                    }
                    $CommericalArr[] = $files->batch->agency_id;
                }
            }
        }

        //Housing Files
        foreach($filesHousing as  $Housing)
        {
            foreach($Housing as $files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $HousingArr)){
                   $agencyBatches = batch::select('batch.parent_id', 'batch.rate_type','batch.file_qty', 'batch.batch_no')
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Housing')
                    ->whereNull('assign_files.is_cancelled')
                    ->get();

                    foreach($agencyBatches as $abatch)
                    {
                        $HousingBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                    }
                    $HousingArr[] = $files->batch->agency_id;
                }
            }
        }

        $filesArr['Residential'] = $ResidentialBatchArr;
        $filesArr['Commercial'] = $CommericalBatchArr;
        $filesArr['Housing'] = $HousingBatchArr;

        // $assign_files = assignFiles::with(['agency'])->get();
        // foreach($assign_files as $file)
        // {
           
        // }

        $residentialOpenBookings = Payments::join('members', 'members.registration_no', '=', 'bookings.registration_no')
        ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id', '=', 'payment_schedule.plot_size_id')
        ->join('persons', 'persons.id', '=', 'members.person_id')
        ->where('payment_schedule.plot_nature', 'Residential')
        ->where('persons.name', 'like', 'open%')
         ->where('bookings.is_cancelled',0)
        ->whereNull('bookings.deleted_at')
         ->where('bookings.status',1)
        ->count();

        return view('admin.report.inventory', compact(
            'filesArr',
            'agencyOldIssuedFiles',
            'agencyRemainingFiles',
            'agencyNewIssuedFiles',
            'agencyOldBookedFiles',
            'agencyNewBookedFiles',
            'residentialOpenBookings',
            'commercialOpenBookings',
            'commercialOpenBookingsArr',
            'housingOpenBookingsArr',
            'rights'
        ));
    }
    
    public function smsReport(Request $request)
    {
        return view('admin.report.smsReport');

    }

    public function getSMS(Request $request)
    {
        $from = $request->input('from_date');
        $from_date = date('Y-m-d', strtotime($request->input('from_date')))." 00:00:00";
        $to_date = date('Y-m-d', strtotime($request->input('to_date')))." 23:59:00";
        $mobile_no = $request->input('mobile_no');
        $status = $request->input('status');
        $reg_no = $request->input('reg_no');

        $sms = SMSModel::when($from, function($query) use ($from_date, $to_date){
            $query->whereBetween('sms.created_at', [$from_date, $to_date]);
        })
        ->when($mobile_no, function($query) use ($mobile_no){
            $query->where('sent_to','like','%'.$mobile_no.'%');
        })
        ->when($status, function($query) use ($status){
            $query->where('sent',$status);
        })
        ->get();
        
        $smss = [];
        foreach($sms as $sm)
        {
            if(!empty($reg_no))
            {
                //$sql = "select members.registration_no from members inner join persons on persons.id = members.person_id where REPLACE(phone_mobile,'-','') = '". $sm->sent_to ."'";
                $reg = DB::select("select members.registration_no from members inner join persons on persons.id = members.person_id where members.registration_no='". $reg_no ."' and REPLACE(phone_mobile,'-','') = '". $sm->sent_to ."'");
                if(count($reg) > 0)
                {
                    $smss[] = $sm;
                }
            }
            else
            {
                $smss[] = $sm;
            }
        }
        //dd($smss[0]);
        
        return view('admin.report.smsReport', compact('smss'));
    }
    
    public function getMembers()
    {
        $rights = Role::getrights('members-report');
        
        $plotSize = plotSize::get();
        $paymentPlan = paymentSchedule::get();
        $agencies = agency::get();
        
        return view('admin.report.members', compact('plotSize', 'paymentPlan', 'agencies', 'rights'));
    }
    
    public function getLPCs()
    {
        return view('admin.report.lpc');
    }

    public function membersReports(Request $request)
    {
        
        $tmp=0;
        
        //dd($request);
        $plot_type = $request->plot_type;
        $plot_size = $request->plot_size;
        $member_name = $request->member_name;
        $agency = $request->agency;
        $payment_plan = $request->payment_plan;
        $due = $request->due;
        $overdue = $request->overdue;
        $install_total = 0;

        // dd($request->input());
        $bookings = Payments::join('members', 'members.registration_no','=','bookings.registration_no')
        ->join('persons','persons.id','=','members.person_id')
        ->with(['agency', 'paymentPlan'])
        
        ->when($member_name, function($query) use ($member_name){
            $query->where('name', 'LIKE', '%'.$member_name.'%');
        })
        ->when($plot_size, function($query) use ($plot_size){
            $query->join('payment_schedule as ps','ps.id','=','bookings.payment_schedule_id')
            ->where('ps.plot_size_id', $plot_size);
        })
        ->when($agency, function($query) use ($agency){
            $query->where('agency_id', $agency);
        })
        ->when($payment_plan, function($query) use ($payment_plan){
            $query->where('payment_schedule_id', $payment_plan);
        })
        ->when($plot_type, function($query) use ($plot_type){
            $query->join('payment_schedule', 'payment_schedule.id','=','bookings.payment_schedule_id')->where('payment_schedule.plot_nature', 'LIKE', '%'.$plot_type.'%');
        })

        ->where('bookings.status', 1)
        ->where('bookings.is_cancelled', 0)
        ->where('members.is_current_owner',1)
        ->whereNull('bookings.deleted_at')
        ->whereNull('members.deleted_at')
        ->orderBy('bookings.registration_no')
        ->get();
        

        $data = Datatables::of($bookings)
            ->addColumn('payment_plan', function($bookings){
                return $bookings->paymentPlan->payment_code;
               
            })
            ->addColumn('name', function($bookings){
                return $bookings->name;
               
            })
            
            ->addColumn('block_no', function($bookings){
                return $bookings->block_no;
               
            })
            
            ->addColumn('plot_no', function($bookings){
                return $bookings->plot_no;
               
            })
            
            ->addColumn('street_no', function($bookings){
                return $bookings->street_no;
               
            })
            ->addColumn('booking_date', function($bookings){
                return $bookings->created_at->format('d-m-Y');
               
            })
            
            
            ->addColumn('agency_name', function($bookings){
                return $bookings->agency->name;
                
            })
            ->addColumn('plot_size', function($bookings){
                if(!isset($bookings->paymentPlan->plotSize->plot_size)){    return 'not set';
                    }
                    
                return $bookings->paymentPlan->plotSize->plot_size;
               
            })

            // ->addColumn('plot_no', function($bookings){
            //     return "";
            // })
            ->addColumn('total_price', function($bookings){
                return $bookings->paymentPlan->total_price;
            })

            ->addColumn('total_net_price', function($bookings){
                return $bookings->paymentPlan->total_price - $bookings->rebate_amount;
            })

            ->addColumn('total_received', function($bookings){
               
                $received = \App\receipts::select(DB::Raw('sum(received_amount) as amount_received'))
                    ->where('registration_no', $bookings->registration_no)->whereNull('deleted_at')->where('booking_cancelled',0)->where('is_active',1)->first();
                return $received->amount_received;
               
            })

            ->addColumn('total_os_amount', function($bookings){

                $net_price = $bookings->paymentPlan->total_price - $bookings->rebate_amount;

                $received = \App\receipts::select(DB::Raw('sum(received_amount) as amount_received,ifnull(sum(rebate_amount),0) as total_rebate'))
                    ->where('registration_no', $bookings->registration_no)->whereNull('deleted_at')->where('booking_cancelled',0)->where('receiving_type','<=',3)->where('is_active',1)->first();
                
                $total_received = $received->amount_received + $received->total_rebate;
                
                /*$lpc = \App\LPC::select(DB::Raw('round(sum(amount_received),0) as amount_received'))
                    ->where('registration_no', $bookings->registration_no)->first();

                $transfer = \App\transfers::select(DB::Raw('tranfer_charges,is_paid'))
                    ->where('reg_no', $bookings->registration_no)->first();
                    
                $fst = \App\booking_featureset::join('bookings','bookings.id','booking_featureset.payment_id')->select(DB::Raw('round(sum(booking_featureset.amount_received),0) as amount_received'))
                    ->where('bookings.registration_no', $bookings->registration_no)->first();
                    
                if($transfer)
                {
                    if($transfer->is_paid == 1)
                        $total_received = $total_received - $transfer->tranfer_charges;
                }
                
                if($lpc)
                    $total_received = $total_received - $lpc->amount_received;
                    
                if($fst)
                    $total_received = $total_received - $fst->amount_received;*/
                    
                return $net_price - $total_received;
            })

            ->addColumn('booking_price', function($bookings){
                return $bookings->paymentPlan->booking_price;
                
            })

            ->addColumn('installment_due_amount', function($bookings) use ($install_total){
                $due_amount = 0;
                $due_amount = memberInstallments::where([['registration_no',$bookings->registration_no],['is_other_payment',0]])->select(\DB::Raw('sum(due_amount)-sum(rebat_amount) as total_due'))->first();
                $install_total = $due_amount->total_due;
                return $due_amount->total_due;
            })

            ->addColumn('installment_rebate', function($bookings){
                $inst_rebate = 0;
                $installments = memberInstallments::Reg($bookings->registration_no)->OtherPayment()->get();
                foreach ($installments as $key => $installment) {
                    $inst_rebate += $installment->rebat_amount;
                }
                return $inst_rebate;
            })

            ->addColumn('total_installment_paid', function($bookings){
                $insts     = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','Installment']])->whereNull('installment_receipts.deleted_at')->select(DB::Raw('sum(received_amount) as total_paid'))->first();
                
                return $insts->total_paid;
            })

            ->addColumn('total_installment_os', function($bookings) use ($install_total){

                $due_amount = 0;
                $due_amount = memberInstallments::where([['registration_no',$bookings->registration_no],['is_other_payment',0]])->select(\DB::Raw('sum(due_amount)-sum(rebat_amount) as total_due'))->first();
                $inst_due = $due_amount->total_due;
                
                $inst_paid = 0;
                $insts     = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','Installment']])->select(DB::Raw('sum(received_amount) as total_paid'))->first();
                $inst_paid = $insts->total_paid;
                
                $inst_rebate = 0;
                $insts_rebate = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','Installment']])->select(DB::Raw('sum(rebate_amount) as total_rebate'))->first();
                $inst_rebate = $insts_rebate->total_rebate;

                $insts     = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','Installment']])->select(DB::Raw('sum(received_amount) - sum(rebate_amount) as total_paid'))->first();
        		$total_paid_inst = 0;
        		$total_paid_inst = $insts->total_paid;
                
                $amount = $inst_due - $inst_paid;
                return  $amount;
            })            

            ->addColumn('registration_status', function($bookings){
               
                return $bookings->is_cancelled == 1 ? "CANCELLED" : "ACTIVE";
            })

            ->addColumn('other_due_amount', function($bookings){
              
                $received_other     = memberInstallments::Reg($bookings->registration_no)->OtherPayment(1)->get();
                $received_other_sum = 0;
                foreach ($received_other as $key => $inst) {
                    $received_other_sum += $inst->installment_amount;
                }

                return $received_other_sum;
            })
            ->addColumn('other_rebate_amount', function($bookings){
                $inst_rebate = 0;
                $installments = memberInstallments::Reg($bookings->registration_no)->OtherPayment(1)->get();
                foreach ($installments as $key => $installment) {
                    $inst_rebate += $installment->rebat_amount;
                }
                return $inst_rebate;
            })
            ->addColumn('other_recev_amount', function($bookings){
                
                $insts     = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','!=','Installment']])->whereNull('installment_receipts.deleted_at')->select(DB::Raw('sum(received_amount) as total_paid'))->first();
                
                return $insts->total_paid;
            })
            ->addColumn('other_os_amount', function($bookings){
                $other_due     = memberInstallments::Reg($bookings->registration_no)->OtherPayment(1)->get();
                $other_due_sum = 0;
                foreach ($other_due as $key => $inst) {
                    $other_due_sum += $inst->installment_amount;
                }
                $other_paid = 0;
                $insts     = \App\InstallmentReceipts::where([['registration_no',$bookings->registration_no],['payment_desc','!=','Installment']])->select(DB::Raw('sum(received_amount) as total_paid'))->first();
                $other_paid = $insts->total_paid;
                
                return $other_due_sum - $other_paid;
            })
            
            
            ->addColumn('due_installments_count', function($bookings){
                return memberInstallments::InstallmentsData($bookings->registration_no, 0,0,1);
            })
            ->addColumn('od_installments_count', function($bookings){
                return memberInstallments::InstallmentsData($bookings->registration_no, 0,0,0,1);
            })            

            // ->addColumn('block_no', function($bookings){
            //     return "";
            // })

            // ->addColumn('plot_no', function($bookings){
            //     return "";
            // })

            ->addColumn('total_installments', function($bookings){
                return $bookings->paymentPlan->installments;
            })            

            ->addColumn('lpc_due', function($bookings){
                $lpc = \App\LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount'))
                    ->where('registration_no', $bookings->registration_no)->first();
                return $lpc->lpc_amount;
            })

            ->addColumn('lpc_waive', function($bookings){
                $lpc = \App\LPC::select(DB::Raw('SUM(ifnull(inst_waive,0)) as inst_waive'))
                    ->where('registration_no', $bookings->registration_no)->first();
                return ($lpc) ? $lpc->inst_waive : '';
                //return "";
            })

            ->addColumn('lpc_receive', function($bookings){
                $lpc = \App\receipts::select(DB::Raw('sum(received_amount) as amount'))->where('receiving_type' , 5)
                    ->where('registration_no', $bookings->registration_no)->first();
                return $lpc->amount;
                //return "";
            })

            ->addColumn('lpc_os', function($bookings){
                
                  
                $lpc = \App\LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount , SUM(ifnull(inst_waive,0)) as inst_waive , SUM(ifnull(amount_received,0)) as amount_received'))
                    ->where('registration_no', $bookings->registration_no)->first();
                    
                    $amnt_rec = 0;
                    if($lpc->amount_received)
                       $amnt_rec = $lpc->amount_received; 
                    $amount = $lpc->lpc_amount - ($lpc->inst_waive + $amnt_rec);
                    
                    return ($amount > 0) ? $amount : 0 ;
            })

            ->addColumn('total_od', function($bookings){
         
                //$other_received_od_sum = memberInstallments::InstallmentsData($bookings->registration_no, 1,0,0,0,1);

                //$inst_od =  memberInstallments::InstallmentsData($bookings->registration_no, 0,1);
                $due_amount = 0;
                $dueInstallments = memberInstallments::where('registration_no',$bookings->registration_no)->where('is_cancelled', 0)->where('is_paid', 0)->where('is_active_user', 1)->get(); 
                foreach($dueInstallments as $installment)
                {
                    if($installment->installment_date < date('Y-m-d'))
                    {
                        if($installment->os_amount > 0)
                            $due_amount += $installment->os_amount;
                        else
                            $due_amount += $installment->installment_amount;
                    }
                }
                return $due_amount;
            })
            
            ->addColumn('paid_inst_count', function($bookings){
                return memberInstallments::Reg($bookings->registration_no)->Paid()->ActiveUser()->count();
            })

            ->addColumn('unpaid_inst_count', function($bookings){
                return memberInstallments::Reg($bookings->registration_no)->Unpaid()->ActiveUser()->count();  
            })
            
            ->addColumn('curr_inst', function($bookings){
                $inst = '';
                $insts = \DB::select('select installment_amount from member_installments where month(installment_date) = '. date('m') .' and year(installment_date) = '. date('Y') .' and registration_no="'. $bookings->registration_no .'"');
                foreach($insts as $inst)
                    $inst = number_format($inst->installment_amount);
                return $inst;  
            })
            
            ->addColumn('installments_od', function($bookings){
          
                $due_amount = 0;
                $dueInstallments = memberInstallments::where('registration_no',$bookings->registration_no)->where('installment_date', '<=', date('Y-m-d'))->where('is_cancelled', 0)->where('is_paid', 0)->where('due_amount','>', 0)->where('is_active_user', 1)->get(); 
                
                foreach($dueInstallments as $installment)
                {
                    $booking = Payments::where('registration_no', $bookings->registration_no)->where('status', 1)->where('is_cancelled',0)->first();
            
                    //echo $installment->registration_no;
                    if($booking){
        
                        $memberInfo = members::where('registration_no', $bookings->registration_no)->where('is_current_owner',1)->first();
        
                        if($memberInfo)
                        {
                            $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                            if($is_member_active)
                            {
                                $due_amount++;
                            }
                        }
                    }
                    
                }
                return $due_amount;
            })
            ->make(true);
            
          
            
        return $data;
        
        
    }
    
    public function lpcReport(Request $request)
    {
        //dd($request);
        //return $request;
        $registration_no = $request->registration_no;
        /*$amount_from = $request->amount_from;
        $amount_to = $request->amount_to;
        $startdate = date('Y-m-d', strtotime($request->startdate));
        $enddate = date('Y-m-d', strtotime($request->enddate));*/
        $columns = array(
            0 => 'registration_no',
            1 =>'installment_no',
            2 =>'calculation_date',
            3 =>'days',
            4=> 'lpc_amount',
            5=> 'waive_amount',
            6=> 'amount_received',
            7 => 'os_amount',
            8 => 'waive_remarks'
        );
        $sort['col'] = $columns[$request->input('order.0.column')];
        $sort['dir'] = $request->input('order.0.dir');

        $lpcs = LPC::join('member_installments','member_installments.id','lpc.installment_id')
                    ->select(DB::Raw('lpc.*,member_installments.installment_no'))
        
        ->when($registration_no, function($lpcs) use ($registration_no){
            $lpcs->where('lpc.registration_no', $registration_no);
        })
        /*->when($startdate, function($lpcs) use ($startdate, $enddate){
            $lpcs->whereBetween('calculation_date', [$startdate, $enddate]);
        })
        ->when($amount_from, function($lpcs) use ($amount_from,$amount_to){
            $lpcs->where('lpc_amount','>', $amount_from)->where('lpc_amount','<', $amount_to);
        })*/
        ->orderBy($sort['col'], $sort['dir'])
        ->get();
//dd($lpcs);
        $data = Datatables::of($lpcs)
            ->addColumn('registration_no', function($lpcs){
                return $lpcs->registration_no;
            })
            
            ->addColumn('installment_no', function($lpcs){
                return $lpcs->installment_no;
            })
            
            ->addColumn('calculation_date', function($lpcs){
                return $lpcs->calculation_date;
            })
            
            ->addColumn('days', function($lpcs){
                return $lpcs->days;
            })
            
            ->addColumn('lpc_amount', function($lpcs){
                return $lpcs->lpc_amount;
            })
            ->addColumn('waive_amount', function($lpcs){
                return $lpcs->waive_amount;
            })

            ->addColumn('amount_received', function($lpcs){
                return ($lpcs->amount_received) ? $lpcs->amount_received : 0;
            })

            ->addColumn('os_amount', function($lpcs){
                return $lpcs->lpc_amount - $lpcs->amount_received - $lpcs->waive_amount;
            })
            
            ->addColumn('waive_remarks', function($lpcs){
                return $lpcs->waive_remarks;
            })

            ->make(true);
        return $data;
    }
    
    
    public function salaryReport(Request $request)
    {
        return view('admin.report.salary');

    }

    public function getsalary(Request $request)
    {
        $forMonth      = explode('-', $request->forMonth);
        
        

        $month = null;
        $year  = null;
        if ($request->forMonth) {
            $month = $forMonth[0];
            $year  = $forMonth[1];
        }
        
        $payslip = DB::table('hr_payslip')
            ->select(DB::Raw('hr_payslip.*, hr_designations.name as designation_name, persons.name as username'))
            
            ->leftjoin('hr_employee', 'hr_employee.id', '=', 'hr_payslip.employee_id')
            ->leftjoin('hr_designations', 'hr_designations.id', '=', 'hr_employee.designation_id')
            ->leftjoin('persons', 'persons.id', '=', 'hr_employee.person_id')
            
            ->when($month, function ($query) use ($month) {
                return $query->where('hr_payslip.month', $month);
            })
            ->when($year, function ($query) use ($year) {
                return $query->where('hr_payslip.year', $year);
            })
       
            ->orderBy('hr_payslip.id', 'desc')
            ->get();
            
           

        return view('admin.report.salary' , compact('payslip'));
    }
    
    public function agency()
    {
            $rights = Role::getrights('agency-report');
            if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            if(!$rights->can_view){
            abort(403);
            }   
            $Agency = agency::orderBy('name','asc')->get();
            return view('admin.report.agency', compact('Agency'));
    }
    
    public function summaryreport(Request $request)
    {
        // if($request->input('from_date') !== null  && $request->input('to_date') !== null){
        //     $first = $request->input('from_date');
        //     $last = $request->input('to_date');
            
        // }elseif($request->input('from_date')!== null){
        //     $first = $request->input('from_date');
        //     $last_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'desc')->first();
        //     $last = $last_booking->payment_date;
            
        // }elseif($request->input('to_date')!== null){
        //     $first_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'asc')->first();
        //     $first = $first_booking->payment_date;
        //     $last = $request->input('to_date');
            
        // }else{
        //     $first_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'asc')->first();
        //     $first = $first_booking->payment_date;
        //     $last_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'desc')->first();
        //     $last = $last_booking->payment_date;
            
        // }
        $first_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'asc')->first();
        $first = $first_booking->payment_date;
        $last_booking = Payments::select('payment_date')->whereNull('deleted_at')->orderBy('payment_date', 'desc')->first();
        $last = $last_booking->payment_date;
        
        $ts1 = strtotime($first);
        $ts2 = strtotime($last);
        
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);
        
        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        // dd($month2);
        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        
        $plotsize = plotSize::select('plot_size', 'id')->whereNull('deleted_at')->orderBy('plot_size', 'asc')->get();
        
        return view('admin.report.summaryreport', compact('plotsize', 'first', 'last', 'diff', 'booking'));
    }
}