<?php

namespace App\Http\Controllers;
use App\Http\Requests\RoleRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Validator;
use App\Role;

class RolesController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('roles');
         if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
    	    $roles = Role::all();
            return view('admin.roles.index', compact('roles', 'rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function create()
    {
        $rights = Role::getrights('roles');
        
        if(!$rights->can_create){
    	    abort(403);
        }
    	return view('admin.roles.create');
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:roles',
        ])->validate();

    	Role::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Role has been added successfully.');

    	return redirect('admin/roles');
    }

    public function destroy($id)
    {
    	$role= Role::find($id);
        $role->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Role has been deleted successfully.');

    	return redirect('admin/roles');
    }

    public function edit($id)
    {
        $rights = Role::getrights('roles');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $role = Role::find($id);
        return view('admin.roles.edit',compact('role'));
    }

    public function update($id, Request $request)
    {
        $name = $request->input('name');
        $display_name = $request->input('display_name');
        $description = $request->input('description');
        $roles = Role::find($id);

        $roles->name = $name;
        $roles->display_name = $display_name;
        $roles->description  = $description ;
        $roles->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Role has been updated successfully.');

        return redirect('admin/roles');
    }

    /* public function search(Request $request, $name)
    {
      $roles = Role::all();
      $alldata = Role::Where('name', 'LIKE', '%' . $name . '%')->first();
      $name=$request->input('name');
      if(!empty($name))
      {
      }
      $alldata=$alldata->paginate(5);
      return view ('admin.roles.search',compact('roles','alldata'));
    } */
}
