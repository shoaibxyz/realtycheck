<?php

namespace App\Http\Controllers;

use App\Installment;
use App\memberInstallments;
use App\otherPayments;
use App\paymentSchedule;
use App\payment_types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\LPC;

class InstallmentController extends Controller
{
	public function index()
    {
    	$Installment = Installment::all();
    	return view('admin.installments.index', compact('Installment'));
    }

    public function create($id)
    {
    	$plan = paymentSchedule::findorfail($id);
        $payment_type = payment_types::all();
    	return view('admin.installments.create',compact('plan','payment_type'));
    }

    public function store(Request $request)
    {
    	Installment::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Installments plan has been generated successfully.');

    	return redirect('admin/installments');
    }

    public function destroy($id)
    {
    	$Installment = Installment::find($id);
    	$Installment->delete();
    	return redirect('admin/installments');
    }

    public function show()
    {

    }

    public function edit($id)
    {
        $unit = Installment::find($id);
        return view('admin.installments.edit',compact('unit'));
    }

    public function update($id, Request $request)
    {
        $plan = paymentSchedule::find($id);
        // //If total amount is not equal to installment amount
        // if($request->input('total_amount') != $plan->total_price){
        //     // $request->session()->flash('error' , 'total amount not match');
        //     return redirect()->back()->withErrors(['message' => 'Total amount not match with installment amount']);
        // }
        // Search for previously added rows in installment table
        $installment = Installment::where('payment_schedule_id',$id)->count();

        if($installment > 0)
            return redirect()->back()->withErrors(['message' => 'You have already created installment plan']);


        if($request->loop)
        {
            foreach ($request->input('loop') as $key => $value) {
                $data = [
                    'days_or_months' => $request->input('days_or_months')[$key],
                    'amount_percentage' => $request->input('amount_percentage')[$key],
                    'installment_amount' => $request->input('inst')[$key],
                    'installment_no'  => $request->input('loop')[$key],
                    'payment_schedule_id' => $id
                ];
                //Save plan based in installment_id
                Installment::create($data);
            }
        }
        if($request->input('other_payment_type_id') )
        {
            foreach ($request->input('other_payment_type_id') as $key => $value) {
                $other_payment_type_id = $value;
                $data2 = [
                    'payment_type_id' => $value,
                    'installment_no' => $request->input('other_installment_no')[$key],
                    'days_or_months' => $request->input('other_days_or_months')[$key],
                    'amount_to_receive' => $request->input('other_inst')[$key],
                    'label' => $request->input('other_payment_label')[$key],
                    'payment_schedule_id' => $id
                ];
                //Save plan based in installment_id
                otherPayments::create($data2);
            }
        }

        return redirect('admin/payment-schedule');
    }

    public function installmentPlan(Request $request)
    {
        $planId = $request->input('payment_schedule_id');
        
        $installments = Installment::where('payment_schedule_id',$planId)->get();
        //dd($installments);
        return $installments;
    }


    public function InstallmentReceipt(Request $request)
    {
        if( $request->ajax() )
        { 
            $received_amount = 0;
            if($request->amount)
                $received_amount = $request->input('amount');
            $rebate_amount = 0;
            if($request->input('rebate_amount'))
                $rebate_amount = $request->input('rebate_amount');

            $received_amount = $received_amount + $rebate_amount;
            //dd($received_amount);
            // return response()->json([''])

            $inst_amount = $request->input('inst_amount');
            $other_amount = $request->input('other_amount');
            $edit = $request->input('edit');
            $receipt_no = $request->input('receipt_no');
            $registration_no = $request->input('reg_no');
            $receiving_type = ($request->input('receiving_type') == 1) ? 0 : 1;
            
            if($request->input('receiving_type') == 4) //Extra Charges
            {
                $bfs = \App\booking_featureset::join('bookings','bookings.id','booking_featureset.payment_id')->where('bookings.registration_no', $registration_no)->where('booking_featureset.is_paid', 0)->select(DB::Raw('booking_featureset.*'))->get();
                //dd($bfs);
                $loop = 1;
                $total = 0;
                $remaining = 0;
                $data['installments'] = null;



                foreach($bfs as $bf)
                {
                    $inst = $bf->fst_payment;
                    $total = $total + $inst;
                    $data['total'][] = $total + $inst;

                    if( $loop == 1)
                        $remaining = ($bf->os_amount > 0) ? (float)$received_amount - $bf->os_amount : $received_amount - $inst;
                    else{
                        $remaining = ($bf->os_amount > 0) ? (float)$received_amount - $bf->os_amount : $remaining - $inst;
                    }
                    //dd($remaining);
                    //$remaining = ($remaining > 0) ? $remaining : -1 * $remaining;

                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    // $data['remain'][] = $remain;
                    $data['remaining'][] = round($remaining,2);

                    if( $remain >= 0)
                    {
                        $data['installments'][] = $bf;
                    }
                    elseif($remain < 0 ){
                        $data['installments'][] = $bf;
                        break;
                    }

                    $loop++;

                }

                return $data;
            }
            
            if($request->input('receiving_type') == 5) //Late Payment Charges
            {
                $lpcs = LPC::select(DB::Raw('lpc.*,ifnull(lpc_amount,0) + ifnull(lpc_other_amount,0) as lpc_amount, lpc.id as lpc_id, lpc.os_amount as lpc_os_amount, lpc.waive_amount, member_installments.*'))
                ->join('member_installments', 'member_installments.id', '=', 'lpc.installment_id')
                ->where('lpc.registration_no', $registration_no)
                // ->where('registration_no', $registration_no)
                ->where('lpc.is_paid', 0)
                ->get();
                $loop = 1;
                $total = 0;
                $remaining = 0;
                $data['installments'] = null;



                foreach($lpcs as $lpc)
                {
                    $inst = ($lpc->inst_waive) ? $lpc->lpc_amount - $lpc->inst_waive : $lpc->lpc_amount;
                    $total = $total + $inst;
                    $data['total'][] = $total + $inst;

                    if( $loop == 1)
                        $remaining = ($lpc->lpc_os_amount > 0) ? (float)$received_amount - $lpc->lpc_os_amount : $received_amount - $inst;
                    else{
                        $remaining = ($lpc->lpc_os_amount > 0) ? (float)$received_amount - $lpc->lpc_os_amount : $remaining - $inst;
                    }
                    //dd($remaining);
                    //$remaining = ($remaining > 0) ? $remaining : -1 * $remaining;

                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    // $data['remain'][] = $remain;
                    $data['remaining'][] = round($remaining,2);

                    if( $remain >= 0)
                    {
                        $data['installments'][] = $lpc;
                    }
                    elseif($remain < 0 ){
                        $data['installments'][] = $lpc;
                        break;
                    }

                    $loop++;

                }

                return $data;
            }

            if($request->input('receiving_type') == 6) //Transfer Charges
            {
                $transfer = \App\transfers::select(DB::Raw('id,tranfer_charges'))
                ->where([['reg_no', $registration_no],['is_paid','!=',1]])
                ->first();
                $data = $transfer;
            }

            if($edit == 1)
            {
               $installments = memberInstallments::where('registration_no',$registration_no)->where('is_other_payment',$receiving_type)->where('receipt_no', $receipt_no)->where('is_active_user',1)->get();

                $total = 0;
                $remaining = 0;
                $loop = 1;
                $data['installments'] = null;

                foreach($installments as $installment)
                {
                    $inst = $installment->installment_amount;
                    $total = $total + $inst;
                    $data['total'][] = $total + $inst;

                    if( $loop == 1)
                        $remaining = (int)$received_amount - $installment->due_amount;
                    else{
                        $remaining = $remaining - $installment->due_amount;
                    }

                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    // $data['remain'][] = $remain;
                    $data['remaining'][] = $remaining;

                    if( $remain >= 0)
                    {
                        $data['installments'][] = $installment;
                    }
                    elseif($remain < 0 ){
                        $data['installments'][] = $installment;
                        break;
                    }

                    $loop++;

                }
                return $data;
            }

            if($request->input('reg_no') != "" && $request->input('receiving_type') < 4)
            {   
                $both = 0;
                if($request->input('receiving_type') == 1)
                {
                    $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_other_payment',0)->where('is_active_user',1)->get();
                }elseif($request->input('receiving_type') == 2){
                    $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_other_payment',1)->where('is_active_user',1)->get();
                }elseif($request->input('receiving_type') == 3){
                    $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_active_user',1)->get();
                    $both = 1;
                }elseif($request->input('receiving_type') == 4){
                    $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_other_payment',2)->where('is_active_user',1)->get();
                }

                $total = 0;
                $remaining = 0;
                $loop = 1;
                $data['installments'] = null;

                foreach($installments as $installment)
                {
                    $inst = $installment->installment_amount;
                    $total = $total + $inst;
                    $data['total'][] = $total + $inst;

                    if($both == 1)
                    {
                        if( $loop == 1)
                            $remaining = (int)$inst_amount - $installment->due_amount;
                        else{
                            $remaining = $remaining - $installment->due_amount;
                        }
                    }else{
                        if( $loop == 1)
                            $remaining = ($installment->os_amount > 0) ? (int)$received_amount - $installment->os_amount : (int)$received_amount - $installment->due_amount;
                        else{
                            $remaining = $remaining - $installment->due_amount;
                        }
                    }
                   
                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    // $data['remain'][] = $remain;
                    $data['remaining'][] = $remaining;

                    if( $remain >= 0)
                    {
                        $data['installments'][] = $installment;
                    }
                    elseif($remain < 0 ){
                        $data['installments'][] = $installment;
                        break;
                    }

                    $loop++;

                    // if($received_amount >= $total)
                    // {
                    //     $data['installments'][] = $installment;
                    // }
                }
            }
            return $data;
        }
    }
    
    public function InstallmentReceipt2(Request $request)
    {
        if( $request->ajax() )
        { 
            $received_amount = $request->input('amount');
            $rebate_amount = $request->input('rebate_amount');

            $received_amount = ($received_amount == 0) ? $rebate_amount : $received_amount;

            // return response()->json([''])

            $inst_amount = $request->input('inst_amount');
            $other_amount = $request->input('other_amount');
            $edit = $request->input('edit');
            $receipt_no = $request->input('receipt_no');
            $registration_no = $request->input('reg_no');
            $receiving_type = ($request->input('receiving_type') == 1) ? 0 : 1;

            if($request->input('reg_no') != "")
            { 
                $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_other_payment',0)->where('is_active_user',1)->get();

                $total = 0;
                $remaining = 0;
                $loop = 1;
                $data['installments'] = null;

                foreach($installments as $installment)
                {
                    $inst = $installment->installment_amount;
                    $total = $total + $inst;
                    $data['total'][] = $total + $inst;

                    if( $loop == 1)
                        $remaining = ($installment->os_amount > 0) ? (int)$inst_amount - $installment->os_amount : (int)$inst_amount - $installment->due_amount;
                    else{
                        $remaining = $remaining - $installment->due_amount;
                    }
                    
                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    $data['remaining'][] = $remaining;

                    if( $remain >= 0)
                    {
                        $data['installments'][] = $installment;
                    }
                    elseif($remain < 0 ){
                        $data['installments'][] = $installment;
                        break;
                    }

                    $loop++;
                }
                
                $installments = memberInstallments::where('registration_no',$registration_no)->where('is_paid',0)->where('is_other_payment',1)->where('is_active_user',1)->get();

                $total = 0;
                $remaining = 0;
                $loop = 1;
                $data['installments_other'] = null;

                foreach($installments as $installment)
                {
                    $inst = $installment->installment_amount;
                    $total = $total + $inst;
                    $data['total_other'][] = $total + $inst;

                    if( $loop == 1)
                        $remaining = ($installment->os_amount > 0) ? (int)$other_amount - $installment->os_amount : (int)$other_amount - $installment->due_amount;
                    else{
                        $remaining = $remaining - $installment->due_amount;
                    }
                    
                    $remain = ($remaining > 0) ? $remaining : -1;
                    
                    $data['remaining_other'][] = $remaining;

                    if( $remain >= 0)
                    {
                        $data['installments_other'][] = $installment;
                    }
                    elseif($remain < 0 ){
                        $data['installments_other'][] = $installment;
                        break;
                    }

                    $loop++;
                }
            }
            //dd($data);
            return $data;
        }
    }
    

     public function getMemberInstallments(Request $request)
    {
        $installments = memberInstallments::where('registration_no', $request->input('registration_no'))->get();
        return $installments;
    }

    public function getInstallment(Request $request)
    {
        $installments = memberInstallments::where('registration_no', $request->input('registration_no'))->where('installment_no', $request->input('installment_no'))->where('is_paid',0)->first();
        return response()->json(['installment' => $installments, 'data' => $request->input()]);
    }

}
