<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentScheduleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\paymentSchedule;
use App\plotSize;
use App\property;
use App\units;
use App\Role;
use Validator;

class PaymentScheduleController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('payment-schedule');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $payment_schedule = paymentSchedule::join('plot_size','plot_size.id','payment_schedule.plot_size_id')->select(DB::Raw('payment_schedule.*,plot_size.plot_size'))->get();
            return view('admin.payment-schedule.index', compact('payment_schedule','rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function create()
    {
        $rights = Role::getrights('payment-schedule');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $plotSize = plotSize::all();
        $plan     = paymentSchedule::all();
        return view('admin.payment-schedule.create', compact('plotSize', 'plan'));
    }

    public function store(PaymentScheduleRequest $request)
    {
        dd($request->input());
        $plan = paymentSchedule::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Payment plan has been added successfully.');

        return redirect('admin/payment-schedule');
    }

    public function destroy($id)
    {
        $payment_schedule = paymentSchedule::find($id);
        $payment_schedule->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Payment plan has been deleted successfully.');

        return redirect('admin/payment-schedule');
    }

    public function edit($id)
    {
        $rights = Role::getrights('payment-schedule');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $plotSize = plotSize::all();
        $payment_schedule = paymentSchedule::find($id);
        return view('admin.payment-schedule.edit', compact('payment_schedule', 'plotSize'));
    }

    public function update($id, Request $request)
    {
        $payment_schedule = paymentSchedule::find($id);
        $payment_schedule->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Payment plan has been updated successfully.');

        return redirect('admin/payment-schedule');
    }

    public function propertyInfo(Request $request, $property)
    {
        if ($request->ajax()) {
            $proper = property::findorfail($property);
            return $proper;
        } else {
            return $property;
        }
    }

    public function PlanInfo(Request $request)
    {
        $plan = paymentSchedule::find($request->input('payment_schedule_id'));
        return $plan;
    }
}
