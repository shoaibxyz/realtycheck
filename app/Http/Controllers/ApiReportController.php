<?php

namespace App\Http\Controllers;

use App\LPC;
use App\Payments;
use App\User;
use App\agency;
use App\assignFiles;
use App\bank;
use App\batch;
use App\memberInstallments;
use App\members;
use App\plotSize;
use App\receipts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class ApiReportController extends Controller
{
    public function ReceiptReport(Request $request)
    {
    	$startdate = date('Y-m-d', strtotime($request->input('from_date')));
        $enddate   = date('Y-m-d', strtotime($request->input('to_date')));

        $report = receipts::whereBetween('receiving_date', [$startdate, $enddate])->get();
        $data['report'] = $report;
        $member = [];
        foreach ($report as $key => $value) {
            $memberInfo = members::where('registration_no', $value->registration_no)->first();
            if (!is_null($memberInfo)) {
                $data['booking'][] = Payments::where('registration_no',$value->registration_no)->first();
                $data['member'][] = User::find($memberInfo->person_id);
            }
        }
        return response()->json(['report' => $data],200, [], JSON_NUMERIC_CHECK);
    }

    public function DueReport(Request $request)
    {
        $from = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));

        $installments   = memberInstallments::where('installment_date','<=',$to)->where('is_paid',0)->where('is_active_user',1)->get();

        $data['installments'] = $installments;

        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            //echo $installment->registration_no;
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $data['member'][] = $memberInfo; 
                        $data['person'][] = $is_member_active;
                        $data['bookings'][] = $booking;

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        if($lpc)
                            $data['latePaymentCharges'][] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                    }
                }
            }
        }

        return response()->json(['report' => $data],200, [], JSON_NUMERIC_CHECK);
    }

    // Start dueAccumulatedAmounts
    
    public function dueAccumulatedAmounts(Request $request){
        
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return response()->json(['agencies' => $agencies],200, [], JSON_NUMERIC_CHECK);
    }
    
    // End CheckdueAccumulatedAmounts
    
        public function CheckdueAccumulatedAmounts(Request $request){
        
        $from = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));

        
        
        $duePersons = [];
        $due_amount = [];
        $rec_amount = [];
        $sumArr = [];
        $regArr = [];
        $installments2 = '';

        $agency = $request->agency_id;
        
      
        if($agency)
        {
            $bookings = Payments::where('agency_id',$agency)->where('status', 1)->get();
            foreach($bookings as $booking)
            {
                $bookingArr[] = $booking->registration_no;
            }

            $installments = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('due_amount','>', 0)->get();
            $installments2 = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_active_user', 1)->where('due_amount','>', 0)->get();
        }
        else{

            $installments = memberInstallments::where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('due_amount','>', 0)->get();
            $installments2 = memberInstallments::where('installment_date', '<=', $to)->where('is_active_user', 1)->where('due_amount','>', 0)->get();
        }

        foreach ($installments->chunk(10) as $key => $installment) {

            foreach($installment as $inst)
            {
                $sumDueAmount = 0;
                
                $due_amount[$inst->registration_no][] = ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount; 
                // $due_amount[$inst->registration_no]['due_amount'][] = $inst->due_amount;
            }
        }
        
        $i=0;
        $received_sum = 0;
        foreach ($installments2->chunk(10) as $key => $installment) 
        {
            foreach($installment as $inst)
            {
                $received = 0;
                $install_rebate = 0;
                $rebate = 0;
                
                if($inst->is_paid == 1 || ( $inst->is_paid == 0  && $inst->os_amount != 0)  )  
                    $received += ($inst->os_amount > 0) ? $inst->due_amount - $inst->os_amount - $inst->rebat_amount : $inst->amount_received;
                
                $rec_amount[$inst->registration_no][] = $received;
                
                $i++;
            }
        }

        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];
        $plotsize = [];

        $dueAmounts = [];
        $dueAmounts2 = [];

        $i = 1;
        foreach ($due_amount as $key => $installment) {

            $booking = Payments::where('registration_no', $key)->where('status', 1)->where('is_cancelled',0)->first();
            
            // $total1 =  Payments::join('payment_schedule','payment_schedule.id','bookings.payment_schedule_id')->where('payment_schedule.id',$booking->payment_schedule_id)->first();
             
            //  if($total1){
            //      $total = $total1->total_price;
            //  } 
             
             $total = ($booking)?$booking->paymentPlan->total_price:0;
            
            //echo $installment->registration_no;
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $dueSum = 0;
                        $recSum = 0;
                        
                        foreach ($due_amount[$key] as $key3 => $reg) {
                            
                            $dueSum += $reg;
                        }
                        
                        foreach ($rec_amount[$key] as $key4 => $reg) {
                            
                            $recSum += $reg;
                        }
                        
                        $plotsize =  \App\plotSize::join('payment_schedule','payment_schedule.plot_size_id','plot_size.id')->
                            where('payment_schedule.id',$booking->payment_schedule_id)->select(DB::Raw('plot_size.plot_size'))->first();
                        $plot_size = '';
                             if($plotsize){
                                $plot_size = $plotsize->plot_size;
                             }
                        
                        $assignFile = assignFiles::where('registration_no', $booking->registration_no)->first();

                       
                        $dueAmounts[$assignFile->plot_type][$key]['sum'] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['member'] = $memberInfo->reference_no; 
                        $dueAmounts[$assignFile->plot_type][$key]['person_name'] = $is_member_active->name;
                        $dueAmounts[$assignFile->plot_type][$key]['person_mobile'] = $is_member_active->phone_mobile;
                        $dueAmounts[$assignFile->plot_type][$key]['bookings'] = $booking->registration_no;
                        $dueAmounts[$assignFile->plot_type][$key]['inst'] = count($installment);
                        $dueAmounts[$assignFile->plot_type][$key]['recSum'] = $recSum;
                        $dueAmounts[$assignFile->plot_type][$key]['total_amount'] = $total;
                        $dueAmounts[$assignFile->plot_type][$key]['remaining_amount'] = ($total) - ($recSum);
                        $dueAmounts[$assignFile->plot_type][$key]['plot_size'] = $plot_size;

                        
                    }
                }
            }
        }
        
        $dueAmounts2[] = $dueAmounts['Commercial'];

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        $i = 1;
                        $commArr = [];
                        $resArr = [];
                        $hounsingArr = [];
                        $commArr2 = [];
                        $resArr2 = [];
                        $hounsingArr2 = [];
                        $footerTotal = [];
                        $inst_sum = 0;
                        $total_sum = 0;
                        $os_sum = 0;
                        $lpc_sum = 0;
                        $i = 1;
                       
                        
                    foreach($dueAmounts['Commercial'] as $key => $plotType){
                        
                    $commArr[] = $plotType;
                    }
                    
                    foreach($dueAmounts['Residential'] as $key => $plotType){
                        
                    $resArr[] = $plotType;
                    }
                    
                    foreach($dueAmounts['Housing'] as $key => $plotType){
                        
                    $hounsingArr[] = $plotType;
                    }
                    $commArr2[] = $commArr;
                    $resArr2[] = $resArr;
                    $hounsingArr2[] = $hounsingArr;
        return response()->json(['commercial' => $commArr , 'residential' => $resArr , 'housing' => $hounsingArr],200, [], JSON_NUMERIC_CHECK);
        
    
    }
    //End CheckdueAccumulatedAmounts
    
    //Start Recovery Report
    
    public function recovery()
    {
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return response()->json(['agencies' => $agencies],200, [], JSON_NUMERIC_CHECK);
    }
    
    //End Recovery Report
    
      //Start Get Recovery Report
    public function getrecoveryReport(Request $request)
    {
        
       $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        if($request->from_date)
        {
            $from = date('Y-m-d', strtotime($request->input('from_date')));
            $to   = date('Y-m-d', strtotime($request->input('to_date')));
            $agency_id   = $request->input('agency_id');

            $agencyInfo = "";
            if($agency_id)
            {
                $agencyInfo = agency::find($agency_id);
                $bookingArr = [];
                $bookings = Payments::where('agency_id',$agency_id)->where('status', 1)->get();
                foreach($bookings as $booking)
                {
                    $bookingArr[] = $booking->registration_no;
                }

                $installments = memberInstallments::whereIn('registration_no', $bookingArr)->whereBetween('installment_date',[$from,$to])->where('is_cancelled',0)->where('is_paid', 0)->where('is_active_user', 1)->where('payment_desc', '=', 'Installment')->get();
            }else{

                $installments = memberInstallments::whereBetween('installment_date',[$from,$to])->where('is_cancelled',0)->where('is_paid', 0)->where('is_active_user', 1)->where('payment_desc', '=', 'Installment')->get();
            }


            $member = [];
            $person = [];
            $bookings = [];
            $latePaymentCharges = [];
            $lpc = [];
            $due_amount = [];
            $due_amount1 = [];


            $plotTypes = [];
            $agency = [];
            foreach ($installments as $key => $installment) {
                $booking = Payments::with('agency')->where('registration_no', $installment->registration_no)->where('status', 1)->first();
                //echo $installment->registration_no;
                if($booking){

                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();
                    if($memberInfo)
                    {
                        $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();

                        $file = assignFiles::where('registration_no', $memberInfo->registration_no)->first();
                        //$agency = agency::where('id' , $booking->agency_id)->first();
                        if($is_member_active)
                        {
                            $month = date('M Y', strtotime($installment->installment_date));
                            $due_amount[$file->plot_type][$month]['unpaid'][] = $installment->due_amount;
                            $plotTypes[$file->plot_type] = $file->plot_type;
                            $due_amount1[$file->plot_type][$booking->agency->name]['unpaid'][] = $installment->due_amount;
                            $agency[$booking->agency->name] = $booking->agency->name;

                            
                        }
                    }
                }
            }
           //dd($due_amount1);


            $report = receipts::whereBetween('receiving_date', [$from, $to])->where('is_active',1)->where('receiving_type',1)->where('booking_cancelled',0)->get();

            $member = [];
            foreach ($report as $key => $value) {

                $file = assignFiles::where('registration_no', $value->registration_no)->first();
                $booking = Payments::with('agency')->where('registration_no', $value->registration_no)->where('status', 1)->first();

                if($agency_id)
                {
                    
                    $booking = Payments::where('registration_no', $value->registration_no)->where('status', 1)->where('agency_id', $agency_id)->first();
                    if($booking)
                    {
                        $month = date('M Y', strtotime($value->receiving_date));
                        $due_amount[$file->plot_type][$month]['paid'][] = ($value->received_amount - $value->rebate_amount);
                    }
                }else{
                    $month = date('M Y', strtotime($value->receiving_date));
                    $due_amount[$file->plot_type][$month]['paid'][] = ($value->received_amount - $value->rebate_amount);
                    $due_amount1[$file->plot_type][$booking->agency->name]['paid'][] = ($value->received_amount - $value->rebate_amount);
                }
            }

            //dd($due_amount);

            $recovery = [];
            $recovery1 = [];
            foreach($due_amount as $key => $months)
            {
                // dd($months);
                foreach($months as $key2 => $amounts)
                {
                    $paid = 0;
                    $unpaid = 0;
                    $mon = 0;
                
                    // dd($amounts);
                    if(isset($amounts['paid']))
                    {   
                        foreach($amounts['paid'] as $amount){
                            // foreach($amount as $amt)
                                $paid += $amount;
                        }
                    }

                    if(isset($amounts['unpaid']))
                    {
                        foreach($amounts['unpaid'] as $amount){
                            // foreach($amount as $amt)
                                $unpaid += $amount;
                        }
                    }
                    // dd($key);
                    
                    $recovery[$key][$key2]['Month'] = $key2;
                    if(isset($agency_id)){
                    $recovery[$key][$key2]['Agency'] = $agencyInfo->name;
                    }
                    $recovery[$key][$key2]['DueAmount'] = $paid+$unpaid;
                    $recovery[$key][$key2]['Remaining'] = $unpaid;
                    $recovery[$key][$key2]['Received'] = $paid;
                    // $var    =    round($paid / $unpaid, 2);
                    // $var = number_format((float)$var, 2, '.', '');
                    // $recovery[$key][$key2]['temp']  = " " .$var. " ";
                    $var = $paid / ($unpaid + $paid) * 100;
                    $amount = round ($var, 2);
                    $recovery[$key][$key2]['Recovery'] = " ".$amount." ";
                    
                }
            }


            foreach($due_amount1 as $key => $agen)
            {
                // dd($months);
                foreach($agen as $key2 => $amounts)
                {
                    $paid = 0;
                    $unpaid = 0;
                
                    // dd($amounts);
                    if(isset($amounts['paid']))
                    {   
                        foreach($amounts['paid'] as $amount){
                            // foreach($amount as $amt)
                                $paid += $amount;
                        }
                    }

                    if(isset($amounts['unpaid']))
                    {
                        foreach($amounts['unpaid'] as $amount){
                            // foreach($amount as $amt)
                                $unpaid += $amount;
                        }
                    }
                    // dd($key);
                    
                    $recovery1[$key][$key2]['Agency'] = $key2;
                    $recovery1[$key][$key2]['DueAmount'] = $paid+$unpaid;
                    $recovery1[$key][$key2]['Remaining'] = $unpaid;
                    $recovery1[$key][$key2]['Received'] = $paid;
                    // $recovery1[$key][$key2]['temp']  = round($paid / ($unpaid + $paid), 2);
                    // $recovery1[$key][$key2]['Recovery'] = round($paid / ($unpaid + $paid) * 100, 2);
                    
                    $amount = round ($paid / ($unpaid + $paid) * 100, 2);
                    $recovery1[$key][$key2]['Recovery'] = " ".$amount." ";
                }
            }
            
           if(!isset($agency_id)){
                    $i = 1;
                    $resArr = [];
                    $comArr = [];
                    $houArr = [];
                    $resdetailArr = [];
                    $comdetailArr = [];
                    $houdetailArr = [];
                    //dd($recovery);
                    if($recovery['Residential'])
                    {
                        foreach($recovery['Residential'] as $key => $amounts){
                            
                            $resArr[] = $amounts;
                        
                        }
                    }
                    if($recovery['Commercial'])
                    {
                        foreach($recovery['Commercial'] as $key => $amounts){
                            
                            $comArr[] = $amounts;
                        
                        }
                    }
                    if(isset($recovery['Housing']))
                    {
                        foreach($recovery['Housing'] as $key => $amounts){
                            
                            $houArr[] = $amounts;
                        
                        }
                    }
                    
                    if($recovery1['Residential'])
                    {
                        foreach($recovery1['Residential'] as $key => $amounts){
                            
                            $resdetailArr[] = $amounts;
                        
                        }
                    }
                    
                    if($recovery1['Commercial'])
                    {
                        foreach($recovery1['Commercial'] as $key => $amounts){
                            $comdetailArr[] = $amounts;
                        }
                    }
                    
                    if(isset($recovery1['Housing']))
                    {
                        foreach($recovery1['Housing'] as $key => $amounts){
                            $houdetailArr[] = $amounts;
                        }
                    }
                    //dd($due_amount);
                    
                    
                    return response()->json(['resArr' => $resArr, 'comArr' => $comArr, 'houArr' => $houArr, 'resdetailArr' => $resdetailArr, 'comdetailArr' => $comdetailArr, 'houdetailArr' => $houdetailArr],200, [], JSON_NUMERIC_CHECK);
           }else{
                               $i = 1;
                    $resArr = [];
                    $comArr = [];
                    $houArr = [];
                    $resdetailArr = [];
                    $comdetailArr = [];
                    $houdetailArr = [];
                    
                    if(isset($recovery['Residential'])){
                    foreach($recovery['Residential'] as $key => $amounts){
                        
                        $resArr[] = $amounts;
                    
                    }
                    }
                    
                    if(isset($recovery['Commercial'])){
                    foreach($recovery['Commercial'] as $key => $amounts){
                        
                        $comArr[] = $amounts;
                    
                    }
                    }
                    if(isset($recovery['Housing'])){
                    foreach($recovery['Housing'] as $key => $amounts){
                        
                        $houArr[] = $amounts;
                    
                    }
                    }
                    
                    if(isset($recovery1['Residential'])){
                    foreach($recovery1['Residential'] as $key => $amounts){
                        
                        $resdetailArr[] = $amounts;
                    
                    }
                    }
                    
                    if(isset($recovery1['Commercial'])){
                    foreach($recovery1['Commercial'] as $key => $amounts){
                        
                        $comdetailArr[] = $amounts;
                    
                    }
                    }
                    if(isset($recovery1['Housing'])){
                    foreach($recovery1['Housing'] as $key => $amounts){
                        
                        $houdetailArr[] = $amounts;
                    
                    }
                    }
                    //dd($due_amount);
                    
                    
                    return response()->json(['resArr' => $resArr, 'comArr' => $comArr, 'resdetailArr' => $resdetailArr, 'comdetailArr' => $comdetailArr],200, [], JSON_NUMERIC_CHECK);
           }

        }   

    } 
    //End Get Recovery Report
    
    
    //Start Defaulters
    public function defaulters(Request $request)
    {
        if($request->input('to_date'))
        {
            // $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
            $to   = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));
            $installments = memberInstallments::selectRaw('*, sum(due_amount) as sum')->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->groupBy('registration_no')->get();

            $member             = [];
            $person             = [];
            $bookings           = [];
            $latePaymentCharges = [];
            $lpc                = [];

            foreach ($installments as $key => $installment) {
                $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
                //echo $installment->registration_no;
                if ($booking) {
                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo) {
                        $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();
                        if ($is_member_active) {
                            $member[]   = $memberInfo;
                            $person[]   = $is_member_active;
                            $bookings[] = $booking;

                            $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id', 'DESC')->first();
                            if ($lpc) {
                                $latePaymentCharges[] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                            }

                        }
                    }
                }
            }
            $user = User::find(Auth::user()->id);

                $i = 1;
                $defaulters = [];
                $inst_sum = 0;
                $lpc_sum = 0;
                foreach($installments as $key => $inst)
                {
                    $defaulters [$key] ['sr'] = $i;
                    $defaulters [$key] ['Registration_no'] =  $inst->registration_no;
                    $defaulters [$key] ['reference_no'] = $member[$key]->reference_no ?? "";
                    
                    $defaulters [$key] ['plot_size'] = $bookings[$key]->paymentPlan->plotSize->plot_size ?? "";
                    $defaulters [$key] ['member'] = $person[$key]->name ?? "";
                    $defaulters [$key] ['date'] = date('d-m-Y', strtotime($inst->installment_date));
                    
                    $date_1=date_create($inst->installment_date->format('Y-m-d'));
                          $date_2=date_create(date('Y-m-d'));
                          $diff2=date_diff($date_1,$date_2);
                          $days = (int)$diff2->format("%R%a");
                          if(($days > 0)) 
                          $defaulters [$key] ['days'] = $diff2->format("%R%a");
                          else 
                         $defaulters [$key] ['days'] = $diff2->format("%R%a");
                        
                        $defaulters [$key] ['mobile'] = $person[$key]->phone_mobile ?? "";
                        $defaulters [$key] ['amount'] = $inst->sum;
                        $inst_sum += $inst->sum;
                }
                $total = $inst_sum;
                  
            return response()->json(['defaulters' => $defaulters, 'total' => $total],200, [], JSON_NUMERIC_CHECK);
        }
    }
    //End Defaulters
    
    
    //Start dueAmounts
    public function dueAmounts()
    {
        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        
        return response()->json(['agencies' => $agencies],200, [], JSON_NUMERIC_CHECK);
    }

    //End dueAmounts
    
    
    //Start CheckdueAmounts
    public function CheckdueAmounts(Request $request)
    {
        $from = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));
// dd($from);
     
        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        $agency = $request->agency_id;

        if($agency)
        {   
            $bookingArr=[];
            $bookings = Payments::where('agency_id',$agency)->where('status', 1)->where('is_cancelled',0)->get();
            foreach($bookings as $booking)
            {
                $bookingArr[] = $booking->registration_no;
            }

            $installments = memberInstallments::whereIn('registration_no', $bookingArr)->where('installment_date', '<=', $to)->where('is_paid', 0)->where('is_active_user', 1)->where('is_cancelled' , 0)->get();
        }else{

            $installments = memberInstallments::whereBetween('installment_date', [$from, $to])->where('is_paid', 0)->where('is_active_user', 1)->where('is_cancelled' , 0)->get();
        }
        //dd($installments);
        $booking_inst = [];
        foreach ($installments as $key => $installment) {

            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->where('is_cancelled',0)->first();
            // if($installment->registration_no == 'CC-GS2-1421'){
            //     $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->where('is_cancelled',0)->toSql();
            //     // dd($booking);    
            // }
            

            //echo $installment->registration_no;
            if(!is_null($booking)){
            
                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $member[] = $memberInfo; 
                        $person[] = $is_member_active;
                        $bookings[] = $booking;
                        $booking_inst[] = $installment;
                        //$lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        $lpc = LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount, SUM(ifnull(amount_received,0)) as amount_received, SUM(ifnull(inst_waive,0)) lpc_waive, SUM(ifnull(os_amount,0)) - SUM(ifnull(inst_waive,0)) as os_amount, calculation_date'))
                        ->where('registration_no', $booking->registration_no)->first();
                        if($lpc)
                            $latePaymentCharges[] = $lpc;
                    }
                }
            }
        }
    
    
        // dd($bookings);

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();

        // foreach ($bookings as $key => $booking) {
        //             echo $booking->paymentPlan->plotSize;
        //         }   
    //   dd($booking_inst);
                $i = 1;
                $due_amounts = [];
                $inst_sum = 0;
                $lpc_sum = 0;
                foreach($booking_inst as $key => $inst){
                  
                  
                      $due_amounts [$key] ['Sr_No'] = $i;
                      $due_amounts [$key] ['Inst_No'] = $inst->installment_no;
                      $due_amounts [$key] ['Reg_No'] = $inst->registration_no;
                      $due_amounts [$key] ['Ref_No'] = $member[$key]->reference_no;
                      $due_amounts [$key] ['Plot_No'] = ($bookings[$key]->paymentPlan and $bookings[$key]->paymentPlan->plotSize) ? $bookings[$key]->paymentPlan->plotSize->plot_size : "";
                      $due_amounts [$key] ['Member'] = $person[$key]->name;
                      $due_amounts [$key] ['Phone'] = $person[$key]->phone_mobile;
                      $due_amounts [$key] ['Type'] = $inst->payment_desc;
                      $due_amounts [$key] ['Date'] = date('d-m-Y', strtotime($inst->installment_date));
                      
                          $date_1=date_create($inst->installment_date->format('Y-m-d'));
                          $date_2=date_create(date('Y-m-d'));
                          $diff2=date_diff($date_1,$date_2);
                          $days = (int)$diff2->format("%R%a");
                          if(($days > 0)) 
                            $due_amounts [$key] ['Days'] = $diff2->format("%R%a"); 
                          else 
                            $due_amounts [$key] ['Days'] = $diff2->format("%R%a");
                          $lpc = ($inst->lpc_charges != 0) ? $inst->lpc_charges : $inst->lpc_other_amount;
                        
                      $due_amounts [$key] ['Due_Amount'] = ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                      $due_amounts [$key] ['LPC'] = number_format($lpc);
                      
                      $amount = ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                        $due_amounts [$key] ['Total_Amount'] = number_format($lpc + $amount);
                    
                    $inst_sum += $amount;
                    $lpc_sum += $lpc;
                  $i++;
    }
                    $footer_total = [];
               
                    $footer_total ['Total_Due_Amount'] = number_format($inst_sum);
                    $footer_total ['Total_Due_LPC'] = number_format($lpc_sum);
                    $footer_total ['Total_Amount'] = number_format($inst_sum + $lpc_sum);
                 
        return response()->json(['due_amounts' => $due_amounts, 'footer_total' => $footer_total],200, [], JSON_NUMERIC_CHECK);
        
    }
    
    //End CheckdueAmounts
    
    // Start Over Due Report  CheckOverDueAmounts
      public function CheckOverDueAmounts(Request $request)
    {
        
        $startdate = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $enddate = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));

        $installments   = memberInstallments::whereBetween('installment_date',[$startdate,$enddate])->where('is_cancelled',0)->where('is_paid',0)->where('installment_amount','>',0)->get();
        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            //echo $installment->registration_no;
            if($booking){
                $memberInfo = members::where('registration_no', $installment->registration_no)->where('is_current_owner',1)->first();
                $member[] = $memberInfo; 
                $person[] = User::find($memberInfo->person_id);
            }
        }

        $sdate = $request->input('from_date');
        $edate = $request->input('to_date');
        
        
        $i = 1;
                foreach($installments as $key => $inst)
                    {
                      $overdue [$key] ['Sr_No'] =  $i;
                      $overdue [$key] ['Inst_No'] = $inst->installment_no;
                      $overdue [$key] ['Reg_No'] = $member[$key]->registration_no ?? "";
                      $overdue [$key] ['Ref_No'] = $member[$key]->reference_no ?? "";
                      $overdue [$key] ['Member_Name'] = $person[$key]->name ?? "";
                      $overdue [$key] ['Payment_Type'] = $inst->payment_desc;
                      $overdue [$key] ['Due_Date'] = date('d-m-Y', strtotime($inst->installment_date));
                       
                            $date1=date_create($inst->installment_date);
                            $date2=date_create(date('Y-m-d'));
                            $diff=date_diff($date2,$date1);
                          
                      $overdue [$key] ['Overdue_Days'] = $diff->format("%a days");
                      $overdue [$key] ['Due_Amount'] = ($inst->os_amount > 0 ) ? $inst->os_amount : $inst->installment_amount;
                    
                  $i++;
                  }
                  
                  
        return response()->json(['overdue' => $overdue],200, [], JSON_NUMERIC_CHECK);
    }

// End Over Due Report  CheckOverDueAmounts
      
      
    //   Start Paid Installments Report
    
    public function PaidInstallent(Request $request)
    {
        $startdate = date('Y-m-d', strtotime(strtr($request->input('from_date') , '/' , '-')));
        $enddate = date('Y-m-d', strtotime(strtr($request->input('to_date') , '/' , '-')));

        $installments = memberInstallments::whereBetween('installment_date', [$startdate,$enddate])->where('is_cancelled',0)->where('is_paid' , 1)->where('is_active_user', 1)->get();
        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];
        $reg = [];

        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            // echo $installment->registration_no."<br>";
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $member[] = $memberInfo; 
                        $person[] = $is_member_active;
                        $bookings[] = $booking;

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        if($lpc)
                            $latePaymentCharges[] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                    }
                }
            }
        }
        
        
       $i = 1;
       $inst_sum = 0;
       $inst_received = 0;
       
       foreach ($installments as $key => $installment) {
          $receipt = \App\receipts::getReceipt($installment->receipt_no);
       
        $paid [$key] ['Sr_No'] = $i;
        $paid [$key] ['Reg_No'] = $installment->registration_no;
        $paid [$key] ['Member_Name'] = $person[$key]->name ?? "";
        $paid [$key] ['Phone'] = $person[$key]->phone_mobile ?? "";
        $paid [$key] ['Payment_Type'] = $installment->payment_desc;
        $paid [$key] ['Plot_Size'] = $bookings[$key]->paymentPlan->plotSize->plot_size ?? "";
        $paid [$key] ['Installment_Due'] = $installment->installment_amount;
        $paid [$key] ['Amount_Paid'] = $installment->amount_received;
        $paid [$key] ['Dated'] = date('d-m-Y', strtotime($installment->installment_date));
        $i++;
        
        $inst_sum += $installment->installment_amount;
        $inst_received += ($installment->amount_received -$installment->rebate_amount);
      
      } 
        
        $footer_total = [];
        $footer_total [$key] ['Total_Installment_Due'] = number_format($inst_sum);
        $footer_total [$key] ['Total_Amount_Paid'] = number_format($inst_received);
      
        // die;
        return response()->json(['paid' => $paid, 'footer_total' => $footer_total],200, [], JSON_NUMERIC_CHECK);
    }
    
    //   End Paid Installments Report
    
    public function BatchReport(Request $request)
    {
        $from = $request->input('from_date');
        $from_date = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to_date = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));
        $batch_no = $request->input('batch_no');
        $agency_id = $request->input('agency_id');
        
        $batches = batch::when($from, function($query) use ($from_date, $to_date){
            $query->whereBetween('batch_assign_date',[$from_date, $to_date]);
        })
        ->when($batch_no, function($query) use ($batch_no){
             $query->where('batch_no', 'like','%'.$batch_no.'%');
        })
        ->when($agency_id, function($query) use ($agency_id){
            $query->where('agency_id', $agency_id);
        })
        ->groupBy('agency_id')->groupBy('batch_no')->get();


        // $batches = batch::groupBy('agency_id')->groupBy('batch_no')->get();

        $files = [];
        $plots = [];
        $fileAgency = [];
        
        $filesCommercial = [];
        $filesResidential = [];
        $filesHousing = [];
        
        foreach ($batches as $key => $batch) {
            $commercial = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Commercial')->where('batch_id', $batch->id)->whereNull('deleted_at')->distinct()->get();
            if(count($commercial) > 0)
                $filesCommercial[] = $commercial;

            $residential = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Residential')->where('batch_id', $batch->id)->whereNull('deleted_at')->distinct()->get();
            if(count($residential) > 0)
                $filesResidential[] = $residential;

            $housing = assignFiles::where('agency_id', $batch->agency->id)->where('plot_type','Housing')->where('batch_id', $batch->id)->whereNull('deleted_at')->distinct()->get();
            if(count($housing) > 0)
                $filesHousing[] = $housing;
            
            // $files[] = assignFiles::where('agency_id', $batch->agency->id)->where('batch_id', $batch->id)->distinct()->get();
        }
        // foreach ($batches as $key => $batch) {
        //     $files[] = assignFiles::where('agency_id', $batch->agency->id)->where('batch_id', $batch->id)->get();
        // }

        // foreach ($files as $key => $file) {
        //     foreach ($file as $key => $fi) {
        //         $plots[] = plotSize::find($fi->plot_size_id);
        //         $fileAgency[] = agency::find($fi->agency_id);
        //     }
        // }

        $agencies  = agency::all();
        
        
        
        // Start Residential Total Files 
        
        $resTotal = [];
        
        $agency = [];
					$i = 1;
					$batchNo = [];

					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";
					
					$twoRMarla       = 0;
					$threeRMarla       = 0;
					$fourRMarla       = 0;
					$fiveRMarla       = 0;
					$sixRMarla        = 0;
					$eightRMarla      = 0;
					$tenRMarla        = 0;
					$twelveRMarla     = 0;
					$sixteenRMarla    = 0;
					$twentyfourRMarla = 0;
					
					foreach ($filesResidential as $totalResidential => $myfiles) {
						foreach ($myfiles as $key2 => $file) {
							if($file != null)
							{
								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$totalResidential]['TwoMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$totalResidential]['ThreeMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$totalResidential]['FourMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$totalResidential]['FiveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$totalResidential]['SixMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$totalResidential]['EightMarla'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$totalResidential]['TenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$totalResidential]['TwelveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$totalResidential]['SixteenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$totalResidential]['TwentyfourMarla'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}

						}

					$resTotal[$totalResidential]['twoRMarla']	 = (isset($batchNo[$totalResidential]['TwoMarla'])) ? count($batchNo[$totalResidential]['TwoMarla']) : 0;
					$resTotal[$totalResidential]['threeRMarla']    = (isset($batchNo[$totalResidential]['ThreeMarla'])) ? count($batchNo[$totalResidential]['ThreeMarla']) : 0;
					$resTotal[$totalResidential]['fourRMarla']	 = (isset($batchNo[$totalResidential]['FourMarla'])) ? count($batchNo[$totalResidential]['FourMarla']) : 0;
					$resTotal[$totalResidential]['fiveRMarla']	  = (isset($batchNo[$totalResidential]['FiveMarla'])) ? count($batchNo[$totalResidential]['FiveMarla']) : 0;
					$resTotal[$totalResidential]['sixRMarla']	  = (isset($batchNo[$totalResidential]['SixMarla'])) ? count($batchNo[$totalResidential]['SixMarla']) : 0;
					$resTotal[$totalResidential]['eightRMarla']  = (isset($batchNo[$totalResidential]['EightMarla'])) ? count($batchNo[$totalResidential]['EightMarla']) : 0;
					$resTotal[$totalResidential]['tenRMarla']	  = (isset($batchNo[$totalResidential]['TenMarla'])) ? count($batchNo[$totalResidential]['TenMarla']) : 0;
					$resTotal[$totalResidential]['twelveRMarla']  = (isset($batchNo[$totalResidential]['TwelveMarla'])) ? count($batchNo[$totalResidential]['TwelveMarla']) : 0;
					$resTotal[$totalResidential]['sixteenRMarla']  = (isset($batchNo[$totalResidential]['SixteenMarla'])) ? count($batchNo[$totalResidential]['SixteenMarla']) : 0;
					$resTotal[$totalResidential]['twentyfourRMarla'] = (isset($batchNo[$totalResidential]['TwentyfourMarla'])) ? count($batchNo[$totalResidential]['TwentyfourMarla']) : 0;
					
					$resTotal[$totalResidential]['batch_no'] = $batch_no;
					
					$resTotal[$totalResidential]['agency_name'] = $agency_name;
					$resTotal[$totalResidential]['batch_assign_date'] = $batch_assign_date;
						
					}
					
					
		// End Residential Total Files 
		
		
		// Start Residential Booked Files 
		
        		    $resBooked = [];
        
                    $agency = [];
					$batchNo = [];
					$i = 1;
					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";
					
                    
							foreach ($filesResidential as $bookedResidential => $myfiles) {
								foreach ($myfiles as $key2 => $file2) {
									if($file != null)
									{

										$batch_no = $file2->batch->batch_no ;
										$agency_name = $file2->batch->agency->name;
										$agency_id = $file2->batch->agency->id;
										$batch_assign_date = date('d-m-Y', strtotime($file2->batch->batch_assign_date));
									}
								}
							
							
				    $booked = [];
							
								$bookedFiles = \App\assignFiles::batchBookedFiles($file2->batch_id, $request->input('agency_id'), 'Residential');
								foreach($bookedFiles as $key2 =>  $file)
								{
									if($file != null)
									{
										$twoMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '02-M');
										
										if ($twoMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['TwoMarlaBooked'][] = $twoMarlaPlotBookings;
										}

										$threeMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '03-M');

										if ($threeMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['ThreeMarlaBooked'][] = $threeMarlaPlotBookings;
										}

										$fourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '04-M');
										if ($fourMarlaPlotBookings > 0) {
											$batchNo[$bookedResidential]['FourMarlaBooked'][] = $fourMarlaPlotBookings;
										}

										$fiveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '05-M');

										if ($fiveMarlaPlotBookings > 0) {
											$batchNo[$bookedResidential]['FiveMarlaBooked'][] = $fiveMarlaPlotBookings;
										}

										$sixMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '06-M');


										if ($sixMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['SixMarlaBooked'][] = $sixMarlaPlotBookings;
										}

										$eightMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '08-M');


										if ($eightMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['EightMarlaBooked'][] = $eightMarlaPlotBookings;
										}

										$tenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '10-M');

										if ($tenMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['TenMarlaBooked'][] = $tenMarlaPlotBookings;
										}


										$twelveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '12-M');


										if ($twelveMarlaPlotBookings > 0) {
											$batchNo[$bookedResidential]['TwelveMarlaBooked'][] = $twelveMarlaPlotBookings;
										}


										$sixteenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential',  '16-M');


										if ($sixteenMarlaPlotBookings > 0) {
											$batchNo[$bookedResidential]['SixteenMarlaBooked'][] = $sixteenMarlaPlotBookings;
										}

										$twentyfourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Residential', '24-M');

										if ($twentyfourMarlaPlotBookings > 0 ) {
											$batchNo[$bookedResidential]['TwentyfourMarlaBooked'][] = $twentyfourMarlaPlotBookings;
										}
									}


								$resBooked[$bookedResidential]['twoRMarla']  = (isset($batchNo[$bookedResidential]['TwoMarlaBooked'])) ? $batchNo[$bookedResidential]['TwoMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['threeRMarla']  = (isset($batchNo[$bookedResidential]['ThreeMarlaBooked'])) ? $batchNo[$bookedResidential]['ThreeMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['fourRMarla']  = (isset($batchNo[$bookedResidential]['FourMarlaBooked'])) ? $batchNo[$bookedResidential]['FourMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['fiveRMarla']  = (isset($batchNo[$bookedResidential]['FiveMarlaBooked'])) ? $batchNo[$bookedResidential]['FiveMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['sixRMarla']  = (isset($batchNo[$bookedResidential]['SixMarlaBooked'])) ? $batchNo[$bookedResidential]['SixMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['eightRMarla']  = (isset($batchNo[$bookedResidential]['EightMarlaBooked'])) ? $batchNo[$bookedResidential]['EightMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['tenRMarla'] = (isset($batchNo[$bookedResidential]['TenMarlaBooked'])) ? $batchNo[$bookedResidential]['TenMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['twelveRMarla'] = (isset($batchNo[$bookedResidential]['TwelveMarlaBooked'])) ? $batchNo[$bookedResidential]['TwelveMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['sixteenRMarla'] = (isset($batchNo[$bookedResidential]['SixteenMarlaBooked'])) ? $batchNo[$bookedResidential]['SixteenMarlaBooked'][0] : 0;
								$resBooked[$bookedResidential]['twentyfourRMarla'] = (isset($batchNo[$bookedResidential]['TwentyfourMarlaBooked'])) ? $batchNo[$bookedResidential]['TwentyfourMarlaBooked'][0] : 0;
								
								$resBooked[$bookedResidential]['batch_no'] = $batch_no;
					
					            $resBooked[$bookedResidential]['agency_name'] = $agency_name;
					            
					            $resBooked[$bookedResidential]['batch_assign_date'] = $batch_assign_date;
								}
							}
					
        // End Residential Booked Files 					
        
        
        // Start Residential Remaining Files 					
        
					$remaining = [];
					$resRemaining = [];
                    $agency = [];
					$batchNo = [];
					$i = 1;
					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";
					
					foreach ($filesResidential as $totalRemaining => $myfiles) {
						foreach ($myfiles as $key2 => $file) {

							if($file != null)
							{

								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$totalRemaining]['TwoMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwoMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$totalRemaining]['ThreeMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['ThreeMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$totalRemaining]['FourMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['FourMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$totalRemaining]['FiveMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['FiveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$totalRemaining]['SixMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['SixMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$totalRemaining]['EightMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['EightMarlaBooked'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$totalRemaining]['TenMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$totalRemaining]['TwelveMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwelveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$totalRemaining]['SixteenMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['SixteenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$totalRemaining]['TwentyfourMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwentyfourMarlaBooked'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}
						}

						$resRemaining[$totalRemaining]['twoRMarla']  = ((isset($batchNo[$totalRemaining]['TwoMarla'])) ? count($batchNo[$totalRemaining]['TwoMarla']) : 0) - ((isset($batchNo[$totalRemaining]['TwoMarlaBooked'])) ? count($batchNo[$totalRemaining]['TwoMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['threeRMarla']   = ((isset($batchNo[$totalRemaining]['ThreeMarla'])) ? count($batchNo[$totalRemaining]['ThreeMarla']) : 0) - ((isset($batchNo[$totalRemaining]['ThreeMarlaBooked'])) ? count($batchNo[$totalRemaining]['ThreeMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['fourRMarla']    = ((isset($batchNo[$totalRemaining]['FourMarla'])) ? count($batchNo[$totalRemaining]['FourMarla']) : 0) - ((isset($batchNo[$totalRemaining]['FourMarlaBooked'])) ? count($batchNo[$totalRemaining]['FourMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['fiveRMarla']    = ((isset($batchNo[$totalRemaining]['FiveMarla'])) ? count($batchNo[$totalRemaining]['FiveMarla']) : 0) - ((isset($batchNo[$totalRemaining]['FiveMarlaBooked'])) ? count($batchNo[$totalRemaining]['FiveMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['sixRMarla']    = ((isset($batchNo[$totalRemaining]['SixMarla'])) ? count($batchNo[$totalRemaining]['SixMarla']) : 0) - ((isset($batchNo[$totalRemaining]['SixMarlaBooked'])) ? count($batchNo[$totalRemaining]['SixMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['eightRMarla']    = ((isset($batchNo[$totalRemaining]['EightMarla'])) ? count($batchNo[$totalRemaining]['EightMarla']) : 0) - ((isset($batchNo[$totalRemaining]['EightMarlaBooked'])) ? count($batchNo[$totalRemaining]['EightMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['tenRMarla']   = ((isset($batchNo[$totalRemaining]['TenMarla'])) ? count($batchNo[$totalRemaining]['TenMarla']) : 0) - ((isset($batchNo[$totalRemaining]['TenMarlaBooked'])) ? count($batchNo[$totalRemaining]['TenMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['twelveRMarla']   = ((isset($batchNo[$totalRemaining]['TwelveMarla'])) ? count($batchNo[$totalRemaining]['TwelveMarla']) : 0) - ((isset($batchNo[$totalRemaining]['TwelveMarlaBooked'])) ? count($batchNo[$totalRemaining]['TwelveMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['sixteenRMarla']   = ((isset($batchNo[$totalRemaining]['SixteenMarla'])) ? count($batchNo[$totalRemaining]['SixteenMarla']) : 0) - ((isset($batchNo[$totalRemaining]['SixteenMarlaBooked'])) ? count($batchNo[$totalRemaining]['SixteenMarlaBooked']) : 0);
						$resRemaining[$totalRemaining]['twentyfourRMarla']   = ((isset($batchNo[$totalRemaining]['TwentyfourMarla'])) ? count($batchNo[$totalRemaining]['TwentyfourMarla']) : 0) - ((isset($batchNo[$totalRemaining]['TwentyfourMarlaBooked'])) ? count($batchNo[$totalRemaining]['TwentyfourMarlaBooked']) : 0);

						$resRemaining[$totalRemaining]['batch_no'] = $batch_no;
					
                        $resRemaining[$totalRemaining]['agency_name'] = $agency_name;
                        
                        $resRemaining[$totalRemaining]['batch_assign_date'] = $batch_assign_date;
					}
        // End Residential Remaining Files 	
        
        // Start Commercial Total Files 	
        $agency = [];
        $comTotal = [];
					$i=1;
					$batchNo = [];

					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";

					foreach ($filesCommercial as $totalCommercial => $myfiles) {
						foreach ($myfiles as $key2 => $file) {
							if($file != null)
							{
								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$totalCommercial]['TwoMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$totalCommercial]['ThreeMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$totalCommercial]['FouCMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$totalCommercial]['FiveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$totalCommercial]['SixMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$totalCommercial]['EightMarla'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$totalCommercial]['TenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$totalCommercial]['TwelveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$totalCommercial]['SixteenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$totalCommercial]['TwentyfouCMarla'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}

						}

						
						$comTotal[$totalCommercial]['twoRMarla']	 = (isset($batchNo[$totalCommercial]['TwoMarla'])) ? count($batchNo[$totalCommercial]['TwoMarla']) : 0;
    					$comTotal[$totalCommercial]['threeRMarla']    = (isset($batchNo[$totalCommercial]['ThreeMarla'])) ? count($batchNo[$totalCommercial]['ThreeMarla']) : 0;
    					$comTotal[$totalCommercial]['fourRMarla']	 = (isset($batchNo[$totalCommercial]['FourMarla'])) ? count($batchNo[$totalCommercial]['FourMarla']) : 0;
    					$comTotal[$totalCommercial]['fiveRMarla']	  = (isset($batchNo[$totalCommercial]['FiveMarla'])) ? count($batchNo[$totalCommercial]['FiveMarla']) : 0;
    					$comTotal[$totalCommercial]['sixRMarla']	  = (isset($batchNo[$totalCommercial]['SixMarla'])) ? count($batchNo[$totalCommercial]['SixMarla']) : 0;
    					$comTotal[$totalCommercial]['eightRMarla']  = (isset($batchNo[$totalCommercial]['EightMarla'])) ? count($batchNo[$totalCommercial]['EightMarla']) : 0;
    					$comTotal[$totalCommercial]['tenRMarla']	  = (isset($batchNo[$totalCommercial]['TenMarla'])) ? count($batchNo[$totalCommercial]['TenMarla']) : 0;
    					$comTotal[$totalCommercial]['twelveRMarla']  = (isset($batchNo[$totalCommercial]['TwelveMarla'])) ? count($batchNo[$totalCommercial]['TwelveMarla']) : 0;
    					$comTotal[$totalCommercial]['sixteenRMarla']  = (isset($batchNo[$totalCommercial]['SixteenMarla'])) ? count($batchNo[$totalCommercial]['SixteenMarla']) : 0;
    					$comTotal[$totalCommercial]['twentyfourRMarla'] = (isset($batchNo[$totalCommercial]['TwentyfourMarla'])) ? count($batchNo[$totalCommercial]['TwentyfourMarla']) : 0;
    					
    					$comTotal[$totalCommercial]['batch_no'] = $batch_no;
    					
    					$comTotal[$totalCommercial]['agency_name'] = $agency_name;
    					
    					$comTotal[$totalCommercial]['batch_assign_date'] = $batch_assign_date;
					}
        // End Commercial Total Files 	
        
        // Start Commercial Booked Files
        
        $comBooked = [];
        	$agency = [];
        	$batchNo = [];
					$i = 1;
					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";
                    
							foreach ($filesCommercial as $bookedCommercial => $myfiles) {
								foreach ($myfiles as $key2 => $file2) {
									if($file != null)
									{

										$batch_no = $file2->batch->batch_no ;
										$agency_name = $file2->batch->agency->name;
										$agency_id = $file2->batch->agency->id;
										$batch_assign_date = date('d-m-Y', strtotime($file2->batch->batch_assign_date));
									}
								}
					
							$booked = [];
							
							
								$bookedFiles = \App\assignFiles::batchBookedFiles($file2->batch_id, $request->input('agency_id'), 'Commercial');
								foreach($bookedFiles as $key2 =>  $file)
								{
									if($file != null)
									{
										$twoMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '02-M');
										
										if ($twoMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['TwoMarlaBooked'][] = $twoMarlaPlotBookings;
										}

										$threeMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '03-M');

										if ($threeMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['ThreeMarlaBooked'][] = $threeMarlaPlotBookings;
										}

										$fourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '04-M');
										if ($fourMarlaPlotBookings > 0) {
											$batchNo[$bookedCommercial]['FourMarlaBooked'][] = $fourMarlaPlotBookings;
										}

										$fiveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '05-M');

										if ($fiveMarlaPlotBookings > 0) {
											$batchNo[$bookedCommercial]['FiveMarlaBooked'][] = $fiveMarlaPlotBookings;
										}

										$sixMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '06-M');


										if ($sixMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['SixMarlaBooked'][] = $sixMarlaPlotBookings;
										}

										$eightMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '08-M');


										if ($eightMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['EightMarlaBooked'][] = $eightMarlaPlotBookings;
										}

										$tenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '10-M');

										if ($tenMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['TenMarlaBooked'][] = $tenMarlaPlotBookings;
										}


										$twelveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '12-M');


										if ($twelveMarlaPlotBookings > 0) {
											$batchNo[$bookedCommercial]['TwelveMarlaBooked'][] = $twelveMarlaPlotBookings;
										}


										$sixteenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial',  '16-M');


										if ($sixteenMarlaPlotBookings > 0) {
											$batchNo[$bookedCommercial]['SixteenMarlaBooked'][] = $sixteenMarlaPlotBookings;
										}

										$twentyfourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '24-M');

										if ($twentyfourMarlaPlotBookings > 0 ) {
											$batchNo[$bookedCommercial]['TwentyfourMarlaBooked'][] = $twentyfourMarlaPlotBookings;
										}
									}


								$comBooked[$bookedCommercial]['twoRMarla']  = (isset($batchNo[$bookedCommercial]['TwoMarlaBooked'])) ? $batchNo[$bookedCommercial]['TwoMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['threeRMarla']  = (isset($batchNo[$bookedCommercial]['ThreeMarlaBooked'])) ? $batchNo[$bookedCommercial]['ThreeMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['fourRMarla']  = (isset($batchNo[$bookedCommercial]['FourMarlaBooked'])) ? $batchNo[$bookedCommercial]['FourMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['fiveRMarla']  = (isset($batchNo[$bookedCommercial]['FiveMarlaBooked'])) ? $batchNo[$bookedCommercial]['FiveMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['sixRMarla']  = (isset($batchNo[$bookedCommercial]['SixMarlaBooked'])) ? $batchNo[$bookedCommercial]['SixMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['eightRMarla']  = (isset($batchNo[$bookedCommercial]['EightMarlaBooked'])) ? $batchNo[$bookedCommercial]['EightMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['tenRMarla'] = (isset($batchNo[$bookedCommercial]['TenMarlaBooked'])) ? $batchNo[$bookedCommercial]['TenMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['twelveRMarla'] = (isset($batchNo[$bookedCommercial]['TwelveMarlaBooked'])) ? $batchNo[$bookedCommercial]['TwelveMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['sixteenRMarla'] = (isset($batchNo[$bookedCommercial]['SixteenMarlaBooked'])) ? $batchNo[$bookedCommercial]['SixteenMarlaBooked'][0] : 0;
								$comBooked[$bookedCommercial]['twentyfourRMarla'] = (isset($batchNo[$bookedCommercial]['TwentyfourMarlaBooked'])) ? $batchNo[$bookedCommercial]['TwentyfourMarlaBooked'][0] : 0;
								
								$comBooked[$bookedCommercial]['batch_no'] = $batch_no;
					
					            $comBooked[$bookedCommercial]['agency_name'] = $agency_name;
					            
					            $comBooked[$bookedCommercial]['batch_assign_date'] = $batch_assign_date;
								}
							    
							}
        // End Commercial Booked Files 	
        
        // End Commercial Remaining Files 	
        
        $comRemaining = [];
        $agency = [];
					$i = 1;
					$remaining = [];
					$agency_name = "";
					$batch_no = "";
					$batch_assign_date = "";
					foreach ($filesCommercial as $totalRemaining => $myfiles) {
						foreach ($myfiles as $key2 => $file) {

							if($file != null)
							{

								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$totalRemaining]['TwoMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwoMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$totalRemaining]['ThreeMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['ThreeMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$totalRemaining]['FouCMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['FouCMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$totalRemaining]['FiveMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['FiveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$totalRemaining]['SixMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['SixMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$totalRemaining]['EightMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['EightMarlaBooked'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$totalRemaining]['TenMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$totalRemaining]['TwelveMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwelveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$totalRemaining]['SixteenMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['SixteenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$totalRemaining]['TwentyfouCMarla'][] = 1;
									if($file->is_reserved == 1)
										$batchNo[$totalRemaining]['TwentyfouCMarlaBooked'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}
						}

						$comRemaining[$totalRemaining]['twoRMarla']  = ((isset($remaining[$totalRemaining]['TwoMarla'])) ? count($remaining[$totalRemaining]['TwoMarla']) : 0) - ((isset($remaining[$totalRemaining]['TwoMarlaBooked'])) ? count($remaining[$totalRemaining]['TwoMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['threeRMarla']   = ((isset($remaining[$totalRemaining]['ThreeMarla'])) ? count($remaining[$totalRemaining]['ThreeMarla']) : 0) - ((isset($remaining[$totalRemaining]['ThreeMarlaBooked'])) ? count($remaining[$totalRemaining]['ThreeMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['fourRMarla']    = ((isset($remaining[$totalRemaining]['FourMarla'])) ? count($remaining[$totalRemaining]['FourMarla']) : 0) - ((isset($remaining[$totalRemaining]['FourMarlaBooked'])) ? count($remaining[$totalRemaining]['FourMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['fiveRMarla']    = ((isset($remaining[$totalRemaining]['FiveMarla'])) ? count($remaining[$totalRemaining]['FiveMarla']) : 0) - ((isset($remaining[$totalRemaining]['FiveMarlaBooked'])) ? count($remaining[$totalRemaining]['FiveMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['sixRMarla']    = ((isset($remaining[$totalRemaining]['SixMarla'])) ? count($remaining[$totalRemaining]['SixMarla']) : 0) - ((isset($remaining[$totalRemaining]['SixMarlaBooked'])) ? count($remaining[$totalRemaining]['SixMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['eightRMarla']    = ((isset($remaining[$totalRemaining]['EightMarla'])) ? count($remaining[$totalRemaining]['EightMarla']) : 0) - ((isset($remaining[$totalRemaining]['EightMarlaBooked'])) ? count($remaining[$totalRemaining]['EightMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['tenRMarla']   = ((isset($remaining[$totalRemaining]['TenMarla'])) ? count($remaining[$totalRemaining]['TenMarla']) : 0) - ((isset($remaining[$totalRemaining]['TenMarlaBooked'])) ? count($remaining[$totalRemaining]['TenMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['twelveRMarla']   = ((isset($remaining[$totalRemaining]['TwelveMarla'])) ? count($remaining[$totalRemaining]['TwelveMarla']) : 0) - ((isset($remaining[$totalRemaining]['TwelveMarlaBooked'])) ? count($remaining[$totalRemaining]['TwelveMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['sixteenRMarla']   = ((isset($remaining[$totalRemaining]['SixteenMarla'])) ? count($remaining[$totalRemaining]['SixteenMarla']) : 0) - ((isset($remaining[$totalRemaining]['SixteenMarlaBooked'])) ? count($remaining[$totalRemaining]['SixteenMarlaBooked']) : 0);
						$comRemaining[$totalRemaining]['twentyfourRMarla']   = ((isset($remaining[$totalRemaining]['TwentyfourMarla'])) ? count($remaining[$totalRemaining]['TwentyfourMarla']) : 0) - ((isset($remaining[$totalRemaining]['TwentyfourMarlaBooked'])) ? count($remaining[$totalRemaining]['TwentyfourMarlaBooked']) : 0);
						
						$comRemaining[$totalRemaining]['batch_no'] = $batch_no;
			
			            $comRemaining[$totalRemaining]['agency_name'] = $agency_name;
			            
			            $comRemaining[$totalRemaining]['batch_assign_date'] = $batch_assign_date;
					}
					
					//End Remaining Commercial Files
					
					//Start Total Housing Files
					
					$houTotal = [];
                    $agency = [];
					$i = 1;
					$agency_name = "";
					$batch_no = "";
					$batch_assign_date = "";
					
					foreach ($filesHousing as $totalHousing => $myfiles) {
						foreach ($myfiles as $key2 => $file) {
							if($file != null)
							{
								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$totalHousing]['TwoMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$totalHousing]['ThreeMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$totalHousing]['FouHMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$totalHousing]['FiveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$totalHousing]['SixMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$totalHousing]['EightMarla'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$totalHousing]['TenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$totalHousing]['TwelveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$totalHousing]['SixteenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$totalHousing]['TwentyfouHMarla'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}

						}

					$houTotal[$totalHousing]['twoRMarla']	 = (isset($batchNo[$totalHousing]['TwoMarla'])) ? count($batchNo[$totalHousing]['TwoMarla']) : 0;
					$houTotal[$totalHousing]['threeRMarla']    = (isset($batchNo[$totalHousing]['ThreeMarla'])) ? count($batchNo[$totalHousing]['ThreeMarla']) : 0;
					$houTotal[$totalHousing]['fourRMarla']	 = (isset($batchNo[$totalHousing]['FourMarla'])) ? count($batchNo[$totalHousing]['FourMarla']) : 0;
					$houTotal[$totalHousing]['fiveRMarla']	  = (isset($batchNo[$totalHousing]['FiveMarla'])) ? count($batchNo[$totalHousing]['FiveMarla']) : 0;
					$houTotal[$totalHousing]['sixRMarla']	  = (isset($batchNo[$totalHousing]['SixMarla'])) ? count($batchNo[$totalHousing]['SixMarla']) : 0;
					$houTotal[$totalHousing]['eightRMarla']  = (isset($batchNo[$totalHousing]['EightMarla'])) ? count($batchNo[$totalHousing]['EightMarla']) : 0;
					$houTotal[$totalHousing]['tenRMarla']	  = (isset($batchNo[$totalHousing]['TenMarla'])) ? count($batchNo[$totalHousing]['TenMarla']) : 0;
					$houTotal[$totalHousing]['twelveRMarla']  = (isset($batchNo[$totalHousing]['TwelveMarla'])) ? count($batchNo[$totalHousing]['TwelveMarla']) : 0;
					$houTotal[$totalHousing]['sixteenRMarla']  = (isset($batchNo[$totalHousing]['SixteenMarla'])) ? count($batchNo[$totalHousing]['SixteenMarla']) : 0;
					$houTotal[$totalHousing]['twentyfourRMarla'] = (isset($batchNo[$totalHousing]['TwentyfourMarla'])) ? count($batchNo[$totalHousing]['TwentyfourMarla']) : 0;
					
					$houTotal[$totalHousing]['batch_no'] = $batch_no;
					
					$houTotal[$totalHousing]['agency_name'] = $agency_name;
					$houTotal[$totalHousing]['batch_assign_date'] = $batch_assign_date;
						
						}
					
					//End Total Housing Files
					
					//Start Booked Housing Files
					$houBooked = [];
					$agency = [];
					$i=1;
					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";



							foreach ($filesHousing as $bookedHousing => $myfiles) {
								foreach ($myfiles as $key2 => $file2) {
									if($file != null)
									{

										$batch_no = $file2->batch->batch_no ;
										$agency_name = $file2->batch->agency->name;
										$agency_id = $file2->batch->agency->id;
										$batch_assign_date = date('d-m-Y', strtotime($file2->batch->batch_assign_date));
									}
								}
					

							$booked = [];
							
								$bookedFiles = \App\assignFiles::batchBookedFiles($file2->batch_id, $request->input('agency_id'), 'Housing');
								foreach($bookedFiles as $key2 =>  $file)
								{
									if($file != null)
									{
										$twoMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '02-M');
										
										if ($twoMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['TwoMarlaBooked'][] = $twoMarlaPlotBookings;
										}

										$threeMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '03-M');

										if ($threeMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['ThreeMarlaBooked'][] = $threeMarlaPlotBookings;
										}

										$fourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '04-M');
										if ($fourMarlaPlotBookings > 0) {
											$booked[$bookedHousing]['FourMarlaBooked'][] = $fourMarlaPlotBookings;
										}

										$fiveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '05-M');

										if ($fiveMarlaPlotBookings > 0) {
											$booked[$bookedHousing]['FiveMarlaBooked'][] = $fiveMarlaPlotBookings;
										}

										$sixMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '06-M');


										if ($sixMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['SixMarlaBooked'][] = $sixMarlaPlotBookings;
										}

										$eightMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '08-M');


										if ($eightMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['EightMarlaBooked'][] = $eightMarlaPlotBookings;
										}

										$tenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '10-M');

										if ($tenMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['TenMarlaBooked'][] = $tenMarlaPlotBookings;
										}


										$twelveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '12-M');


										if ($twelveMarlaPlotBookings > 0) {
											$booked[$bookedHousing]['TwelveMarlaBooked'][] = $twelveMarlaPlotBookings;
										}


										$sixteenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing',  '16-M');


										if ($sixteenMarlaPlotBookings > 0) {
											$booked[$bookedHousing]['SixteenMarlaBooked'][] = $sixteenMarlaPlotBookings;
										}

										$twentyfourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Housing', '24-M');

										if ($twentyfourMarlaPlotBookings > 0 ) {
											$booked[$bookedHousing]['TwentyfourMarlaBooked'][] = $twentyfourMarlaPlotBookings;
										}
									}

                                $houBooked[$bookedHousing]['twoRMarla']  = (isset($batchNo[$bookedHousing]['TwoMarlaBooked'])) ? $batchNo[$bookedHousing]['TwoMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['threeRMarla']  = (isset($batchNo[$bookedHousing]['ThreeMarlaBooked'])) ? $batchNo[$bookedHousing]['ThreeMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['fourRMarla']  = (isset($batchNo[$bookedHousing]['FourMarlaBooked'])) ? $batchNo[$bookedHousing]['FourMarlaBooked'][0] : 0;
							    $houBooked[$bookedHousing]['fiveRMarla']  = (isset($batchNo[$bookedHousing]['FiveMarlaBooked'])) ? $batchNo[$bookedHousing]['FiveMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['sixRMarla']  = (isset($batchNo[$bookedHousing]['SixMarlaBooked'])) ? $batchNo[$bookedHousing]['SixMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['eightRMarla']  = (isset($batchNo[$bookedHousing]['EightMarlaBooked'])) ? $batchNo[$bookedHousing]['EightMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['tenRMarla'] = (isset($batchNo[$bookedHousing]['TenMarlaBooked'])) ? $batchNo[$bookedHousing]['TenMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['twelveRMarla'] = (isset($batchNo[$bookedHousing]['TwelveMarlaBooked'])) ? $batchNo[$bookedHousing]['TwelveMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['sixteenRMarla'] = (isset($batchNo[$bookedHousing]['SixteenMarlaBooked'])) ? $batchNo[$bookedHousing]['SixteenMarlaBooked'][0] : 0;
								$houBooked[$bookedHousing]['twentyfourRMarla'] = (isset($batchNo[$bookedHousing]['TwentyfourMarlaBooked'])) ? $batchNo[$bookedHousing]['TwentyfourMarlaBooked'][0] : 0;
								
								$houBooked[$bookedHousing]['batch_no'] = $batch_no;
					
					            $houBooked[$bookedHousing]['agency_name'] = $agency_name;
					            
					            $houBooked[$bookedHousing]['batch_assign_date'] = $batch_assign_date;
								}}
					//End Booked Housing Files
					
					//Start Remaining Housing Files
					$agency = [];
					$i = 1;
					$houRemaining = [];
					$agency_name = "";
					$batch_no = "";
					$batch_assign_date = "";
					foreach ($filesHousing as $housingRemaining => $myfiles) {
						foreach ($myfiles as $key2 => $file) {

							if($file != null)
							{

								if ($file->plotSize->plot_size == '02-M') {
									$remaining[$housingRemaining]['TwoMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['TwoMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$remaining[$housingRemaining]['ThreeMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['ThreeMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$remaining[$housingRemaining]['FouHMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['FouHMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$remaining[$housingRemaining]['FiveMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['FiveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$remaining[$housingRemaining]['SixMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['SixMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$remaining[$housingRemaining]['EightMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['EightMarlaBooked'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$remaining[$housingRemaining]['TenMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['TenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$remaining[$housingRemaining]['TwelveMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['TwelveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$remaining[$housingRemaining]['SixteenMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['SixteenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$remaining[$housingRemaining]['TwentyfouHMarla'][] = 1;
									if($file->is_reserved == 1)
										$remaining[$housingRemaining]['TwentyfouHMarlaBooked'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}
						}

						$houRemaining[$housingRemaining]['twoRMarla']  = ((isset($remaining[$housingRemaining]['TwoMarla'])) ? count($remaining[$housingRemaining]['TwoMarla']) : 0) - ((isset($remaining[$housingRemaining]['TwoMarlaBooked'])) ? count($remaining[$housingRemaining]['TwoMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['threeRMarla']   = ((isset($remaining[$housingRemaining]['ThreeMarla'])) ? count($remaining[$housingRemaining]['ThreeMarla']) : 0) - ((isset($remaining[$housingRemaining]['ThreeMarlaBooked'])) ? count($remaining[$housingRemaining]['ThreeMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['fourRMarla']    = ((isset($remaining[$housingRemaining]['FourMarla'])) ? count($remaining[$housingRemaining]['FourMarla']) : 0) - ((isset($remaining[$housingRemaining]['FourMarlaBooked'])) ? count($remaining[$housingRemaining]['FourMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['fiveRMarla']    = ((isset($remaining[$housingRemaining]['FiveMarla'])) ? count($remaining[$housingRemaining]['FiveMarla']) : 0) - ((isset($remaining[$housingRemaining]['FiveMarlaBooked'])) ? count($remaining[$housingRemaining]['FiveMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['sixRMarla']    = ((isset($remaining[$housingRemaining]['SixMarla'])) ? count($remaining[$housingRemaining]['SixMarla']) : 0) - ((isset($remaining[$housingRemaining]['SixMarlaBooked'])) ? count($remaining[$housingRemaining]['SixMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['eightRMarla']    = ((isset($remaining[$housingRemaining]['EightMarla'])) ? count($remaining[$housingRemaining]['EightMarla']) : 0) - ((isset($remaining[$housingRemaining]['EightMarlaBooked'])) ? count($remaining[$housingRemaining]['EightMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['tenRMarla']   = ((isset($remaining[$housingRemaining]['TenMarla'])) ? count($remaining[$housingRemaining]['TenMarla']) : 0) - ((isset($remaining[$housingRemaining]['TenMarlaBooked'])) ? count($remaining[$housingRemaining]['TenMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['twelveRMarla']   = ((isset($remaining[$housingRemaining]['TwelveMarla'])) ? count($remaining[$housingRemaining]['TwelveMarla']) : 0) - ((isset($remaining[$housingRemaining]['TwelveMarlaBooked'])) ? count($remaining[$housingRemaining]['TwelveMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['sixteenRMarla']   = ((isset($remaining[$housingRemaining]['SixteenMarla'])) ? count($remaining[$housingRemaining]['SixteenMarla']) : 0) - ((isset($remaining[$housingRemaining]['SixteenMarlaBooked'])) ? count($remaining[$housingRemaining]['SixteenMarlaBooked']) : 0);
						$houRemaining[$housingRemaining]['twentyfourRMarla']   = ((isset($remaining[$housingRemaining]['TwentyfourMarla'])) ? count($remaining[$housingRemaining]['TwentyfourMarla']) : 0) - ((isset($remaining[$housingRemaining]['TwentyfourMarlaBooked'])) ? count($remaining[$housingRemaining]['TwentyfourMarlaBooked']) : 0);
						
						$houRemaining[$housingRemaining]['batch_no'] = $batch_no;
			
			            $houRemaining[$housingRemaining]['agency_name'] = $agency_name;
			            
			            $houRemaining[$housingRemaining]['batch_assign_date'] = $batch_assign_date;
					}
					//End Remaining Housing Files
        return response()->json(['agencies' => $agencies, 'batches' => $batches,'resTotal' => $resTotal, 'resBooked' => $resBooked, 'resRemaining' => $resRemaining, 'comTotal' => $comTotal, 'comBooked' => $comBooked, 'comRemaining' => $comRemaining, 'houTotal' => $houTotal, 'houBooked' => $houBooked, 'houRemaining' => $houRemaining],200, [], JSON_NUMERIC_CHECK);
    }


    public function BankReport(Request $request)
    {
        $startdate = date('Y-m-d', strtotime($request->input('from_date')));
        $enddate   = date('Y-m-d', strtotime($request->input('to_date')));

        $data['cash']   = receipts::whereBetween('receiving_date', array($startdate, $enddate))->where('receiving_mode','cash')->sum('received_amount');

        // $cheque   = receipts::whereBetween('receiving_date', array($startdate, $enddate))->where('receiving_mode','cheque')->sum('received_amount');

        $data['cheque'] = DB::table('receipts')
            ->join('bank', 'receipts.bank_id', '=', 'bank.id')
            ->where('receipts.received_amount', '>', 0)
            ->whereBetween('receiving_date', array($startdate, $enddate))
            ->get();

        $data['banks'] = bank::all();

        return response()->json(['report' => $data],200, [], JSON_NUMERIC_CHECK);
    }


    public function BookingReport(Request $request)
    {
        $startdate = $request->input('from_date');
        $enddate = $request->input('to_date');

        $bookings = Payments::whereBetween('created_at', [$enddate,$startdate])->where('status' , 1)->get();

        // return response()->json(['report' => $bookings],200, [], JSON_NUMERIC_CHECK);

        $BookingDetail['booking'] = $bookings;
        $member = null;
        foreach ($bookings as $booking) {

            $BookingDetail['paymentPlan'][] = $booking->paymentPlan;

            $BookingDetail['PlotSize'][] = $booking->paymentPlan->plotSize;

            $members = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->get();

            foreach ($members as $member) {
                $person                    = User::find($member->person_id);
                $BookingDetail['person'][] = $person;
            }

            if (!is_null($member)) {
                $BookingDetail['member'][] = $member;
            }

            $received_sum = 0;
            $installments = memberInstallments::where('registration_no', $booking->registration_no)->where('is_paid', 1)->get();
            foreach ($installments as $key => $installment) {
                $received_sum+=$installment->amount_received;
            }

            $BookingDetail['inst'][] = $received_sum;

        }
        return response()->json(['report' => $BookingDetail],200, [], JSON_NUMERIC_CHECK);
    }


    public function PaidInstReport(Request $request)
    {
        $startdate = $request->input('from_date');
        $enddate = $request->input('to_date');

        $installments = memberInstallments::whereBetween('payment_date', [$startdate,$enddate])->where('is_paid' , 1)->where('is_active_user', 1)->get();
        $data['installments'] = $installments;


        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        foreach ($installments as $key => $installment) {
            $booking = Payments::where('registration_no', $installment->registration_no)->where('status', 1)->first();
            if($booking){

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner',1)->first();

                if($memberInfo)
                {
                    $is_member_active = User::where('id',$memberInfo->person_id)->where('status',1)->first();
                    if($is_member_active)
                    {
                        $data['member'][] = $memberInfo; 
                        $data['person'][] = $is_member_active;
                        $data['bookings'][] = $booking;
                        $data['plotSize'][] = $booking->paymentPlan->plotSize;

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id','DESC')->first();
                        if($lpc)
                            $data['latePaymentCharges'][] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                    }
                }
            }
        }
        return response()->json(['report' => $data],200, [], JSON_NUMERIC_CHECK);
    }
    
    function inventory_res(Request $request)
    {
        $agency = $request->agency_id;
        
        //dd($agency);

        $batches = batch::with(['agency'])
        ->when($agency, function($query) use ($agency){
            $query->where('agency_id', $agency);
        })
        ->get();

        
        $member = null;
        $ResidentialArr = [];
        $agencyBatch = [];
        $issuedArr = [];
        $ResidentialBatchArr = [];

        $filesResidential = [];

        $agencyIssuedFiles = [];
        $agencyBookedFiles = [];

        $agencyRemainingFiles = [];

        foreach ($batches as $batch) {

            $residential = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Residential')->where('batch_id', $batch->id)->distinct()->whereNull('assign_files.deleted_at')->get();
            if(count($residential) > 0)
                $filesResidential[] = $residential;

            //Residential
            $agencyOldIssuedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewIssuedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();

            $agencyOldBookedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewBookedFiles['Residential'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Residential')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();
        }
        
        //Residential Files
        foreach($filesResidential as $residential)
        {
            foreach($residential as $key=>$files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $ResidentialArr)){
                    $agencyBatches = batch::select(DB::Raw('batch.parent_id,batch.rate_type,batch_no,batch.file_qty'))
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Residential')
                    // ->distinct('parent_id')
                    ->get();

                    $checkFile = [];
                    //$ResidentialBatchArr['agencies'][$key]['name']= $files->batch->agency->name;
                    $i=0;
                    // $ResidentialBatchArr[$i]=array('agency'=>$files->batch->agency->name); 
                    foreach($agencyBatches as $k=>$abatch)
                    {
                        //dd($abatch);
                        $ResidentialBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                        // $ResidentialBatchArr[$i]['data'][]= [
                        //         'batch'=>$abatch->parent_id,
                        //         'rate_type'=>$abatch->rate_type,
                        //         'qty'=>$abatch->file_qty
                        //     ];
                        //$ResidentialBatchArr['agencies'][$key]['batch_no']=$abatch->parent_id;
                        //$ResidentialBatchArr['agencies'][$key]['type']= $abatch->rate_type;
                        //$ResidentialBatchArr['agencies'][$key]['qty']= $abatch->file_qty;
                        //[$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                    }
                    // dd($ResidentialBatchArr);
                    $i++;
                    $ResidentialArr[] = $files->batch->agency_id;
                }
            }
        }

        $filesArr = $ResidentialBatchArr;
        
        // dd($filesArr);
        
        $residentialOpenBookings = Payments::join('members', 'members.registration_no', '=', 'bookings.registration_no')
        ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id', '=', 'payment_schedule.plot_size_id')
        ->join('persons', 'persons.id', '=', 'members.person_id')
        ->where('payment_schedule.plot_nature', 'Residential')
        ->where('persons.name', 'like', 'open%')
        ->count();


        return response()->json(['filesArr' => $filesArr, 'agencyOldIssuedFiles' => $agencyOldIssuedFiles, 'agencyRemainingFiles' => $agencyRemainingFiles, 'agencyNewIssuedFiles' => $agencyNewIssuedFiles, 'agencyOldBookedFiles' => $agencyOldBookedFiles, 'agencyNewBookedFiles' => $agencyNewBookedFiles, 'residentialOpenBookings' => $residentialOpenBookings],200, [], JSON_NUMERIC_CHECK);
    }

    
    
    function inventory_com(Request $request)
    {
        $agency = $request->agency_id;
        
        //dd($agency);

        $batches = batch::with(['agency'])
        ->when($agency, function($query) use ($agency){
            $query->where('agency_id', $agency);
        })
        ->get();

        
        $member = null;
        $CommercialArr = [];
        $agencyBatch = [];
        $issuedArr = [];
        $CommercialBatchArr = [];

        $filesCommercial  = [];

        $agencyIssuedFiles = [];
        $agencyBookedFiles = [];

        $agencyRemainingFiles = [];

        foreach ($batches as $batch) {

            $commercial = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Commercial')->where('batch_id', $batch->id)->distinct()->whereNull('assign_files.deleted_at')->get();
            if(count($commercial) > 0)
                $filesCommercial[] = $commercial;

            //Commercial
            $agencyOldIssuedFiles['Commercial'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Commercial')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewIssuedFiles['Commercial'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Commercial')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();

            $agencyOldBookedFiles['Commercial'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Commercial')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewBookedFiles['Commercial'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Commercial')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();
        }
        
        //Commercial Files
        foreach($filesCommercial as $commercial)
        {
            foreach($commercial as $key=>$files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $CommercialArr)){
                    $agencyBatches = batch::select(DB::Raw('batch.parent_id,batch.rate_type,batch_no,batch.file_qty'))
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Commercial')
                    // ->distinct('parent_id')
                    ->get();

                    $checkFile = [];
                    //$CommercialBatchArr['agencies'][$key]['name']= $files->batch->agency->name;
                    $i=0;
                    // $CommercialBatchArr[$i]=array('agency'=>$files->batch->agency->name); 
                    foreach($agencyBatches as $k=>$abatch)
                    {
                        //dd($abatch);
                        $CommercialBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                        // $CommercialBatchArr[$i]['data'][]= [
                        //         'batch'=>$abatch->parent_id,
                        //         'rate_type'=>$abatch->rate_type,
                        //         'qty'=>$abatch->file_qty
                        //     ];
                        //$CommercialBatchArr['agencies'][$key]['batch_no']=$abatch->parent_id;
                        //$CommercialBatchArr['agencies'][$key]['type']= $abatch->rate_type;
                        //$CommercialBatchArr['agencies'][$key]['qty']= $abatch->file_qty;
                        //[$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                    }
                    // dd($CommercialBatchArr);
                    $i++;
                    $CommercialArr[] = $files->batch->agency_id;
                }
            }
        }

        $filesArr = $CommercialBatchArr;
        
        // dd($filesArr);
        
        $commercialOpenBookings = Payments::join('members', 'members.registration_no', '=', 'bookings.registration_no')
        ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id', '=', 'payment_schedule.plot_size_id')
        ->join('persons', 'persons.id', '=', 'members.person_id')
        ->where('payment_schedule.plot_nature', 'Commercial')
        ->where('persons.name', 'like', 'open%')
        ->count();


        return response()->json(['filesArr' => $filesArr, 'agencyOldIssuedFiles' => $agencyOldIssuedFiles, 'agencyRemainingFiles' => $agencyRemainingFiles, 'agencyNewIssuedFiles' => $agencyNewIssuedFiles, 'agencyOldBookedFiles' => $agencyOldBookedFiles, 'agencyNewBookedFiles' => $agencyNewBookedFiles, 'commercialOpenBookings' => $commercialOpenBookings],200, [], JSON_NUMERIC_CHECK);
    }
    
    
    function inventory_hous(Request $request)
    {
        $agency = $request->agency_id;
        
        //dd($agency);

        $batches = batch::with(['agency'])
        ->when($agency, function($query) use ($agency){
            $query->where('agency_id', $agency);
        })
        ->get();

        
        $member = null;
        $HousingArr = [];
        $agencyBatch = [];
        $issuedArr = [];
        $HousingBatchArr = [];

        $filesHousing  = [];

        $agencyIssuedFiles = [];
        $agencyBookedFiles = [];

        $agencyRemainingFiles = [];

        foreach ($batches as $batch) {

            $housing = assignFiles::with(['batch','agency'])->where('agency_id', $batch->agency->id)->where('plot_type','Housing')->where('batch_id', $batch->id)->distinct()->whereNull('assign_files.deleted_at')->get();
            if(count($housing) > 0)
                $filesHousing[] = $housing;

            //Housing
            $agencyOldIssuedFiles['Housing'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Housing')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewIssuedFiles['Housing'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->AgencyFilesCount($batch->agency->id, 'Housing')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();

            $agencyOldBookedFiles['Housing'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Housing')->where('batch.rate_type','old')->whereNull('assign_files.deleted_at')->count();

            $agencyNewBookedFiles['Housing'][$batch->agency->name][$batch->rate_type] = assignFiles::join('batch', 'batch.id','=','assign_files.batch_id')->where('is_reserved',1)->AgencyFilesCount($batch->agency->id, 'Housing')->where('batch.rate_type','new')->whereNull('assign_files.deleted_at')->count();
        }
        
        //Housing Files
        foreach($filesHousing as $housing)
        {
            foreach($housing as $key=>$files)
            {
                //Check agency batches
                if(!in_array($files->batch->agency_id, $HousingArr)){
                    $agencyBatches = batch::select(DB::Raw('batch.parent_id,batch.rate_type,batch_no,batch.file_qty'))
                    ->join('assign_files','assign_files.batch_id','=','batch.id')
                    ->where('batch.agency_id', $files->batch->agency_id)
                    ->where('assign_files.plot_type', 'Housing')
                    // ->distinct('parent_id')
                    ->get();

                    $checkFile = [];
                    //$HousingBatchArr['agencies'][$key]['name']= $files->batch->agency->name;
                    $i=0;
                    // $HousingBatchArr[$i]=array('agency'=>$files->batch->agency->name); 
                    foreach($agencyBatches as $k=>$abatch)
                    {
                        //dd($abatch);
                        $HousingBatchArr[$files->batch->agency->name][$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                        // $HousingBatchArr[$i]['data'][]= [
                        //         'batch'=>$abatch->parent_id,
                        //         'rate_type'=>$abatch->rate_type,
                        //         'qty'=>$abatch->file_qty
                        //     ];
                        //$HousingBatchArr['agencies'][$key]['batch_no']=$abatch->parent_id;
                        //$HousingBatchArr['agencies'][$key]['type']= $abatch->rate_type;
                        //$HousingBatchArr['agencies'][$key]['qty']= $abatch->file_qty;
                        //[$abatch->parent_id][$abatch->rate_type][$abatch->batch_no] = $abatch->file_qty;
                    }
                    // dd($HousingBatchArr);
                    $i++;
                    $HousingArr[] = $files->batch->agency_id;
                }
            }
        }

        $filesArr = $HousingBatchArr;
        
        // dd($filesArr);
        
        $housingOpenBookings = Payments::join('members', 'members.registration_no', '=', 'bookings.registration_no')
        ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id', '=', 'payment_schedule.plot_size_id')
        ->join('persons', 'persons.id', '=', 'members.person_id')
        ->where('payment_schedule.plot_nature', 'Housing')
        ->where('persons.name', 'like', 'open%')
        ->count();


        return response()->json(['filesArr' => $filesArr, 'agencyOldIssuedFiles' => $agencyOldIssuedFiles, 'agencyRemainingFiles' => $agencyRemainingFiles, 'agencyNewIssuedFiles' => $agencyNewIssuedFiles, 'agencyOldBookedFiles' => $agencyOldBookedFiles, 'agencyNewBookedFiles' => $agencyNewBookedFiles, 'housingOpenBookings' => $housingOpenBookings],200, [], JSON_NUMERIC_CHECK);
    }
    
    // Start Sale Summary Report
    
    
    public function saleSummary()
    {
  
        // \Artisan::call('cache:clear');
        // dd("here");
        // $bookings = Payments::Active();


        // $assignedFiles = assignFiles::all();
        $assignedFiles = assignFiles::join('batch','batch.id','=','assign_files.batch_id')->get();

        $agency          = [];
        $batch           = [];
        $received_amount = [];
        $agencyAdded     = [];
        $received_sumArr = [];
        $agencyData      = [];
        $calcData        = [];
        $i = 1;
        foreach ($assignedFiles as $assignedFile) {

            $booking = Payments::where('registration_no', $assignedFile->registration_no)->first();

            // if (count($booking) > 0) {`
                $calcData[$assignedFile->agency->name]['total'][] = $assignedFile->plotSize->Plan->total_price;
                if (!in_array($assignedFile->agency->name, $agencyAdded)) {
                    $agency[]      = $assignedFile->agency;
                    $agencyAdded[] = $assignedFile->agency->name;

                    $agencyFiles[] = batch::where('agency_id', $assignedFile->agency->id)->get();
                }

                $received_sum = 0;
                $installments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 0)->get();
                foreach ($installments as $key => $installment) {
                    $received_sum += $installment->amount_received;
                }

                $ballotingAmount    = 0;
                $memberInstallments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 1)->get();
                foreach ($memberInstallments as $key => $installment) {
                    if ($installment->payment_desc == "Balloting") {
                        $ballotingAmount += $installment->amount_received;
                    }

                }

                $downPayment        = 0;
                $memberInstallments = memberInstallments::where('registration_no', $assignedFile->registration_no)->where('is_cancelled',0)->where('is_paid', 1)->where('is_other_payment', 1)->get();
                foreach ($memberInstallments as $key => $installment) {
                    if ($installment->payment_desc == "Down Payment") {
                        $downPayment += $installment->amount_received;
                    }

                }

                $calcData[$assignedFile->agency->name]['installmentReceived'][] = $received_sum;
                $calcData[$assignedFile->agency->name]['booking'][]             = $assignedFile->plotSize->Plan->booking_price;
                $calcData[$assignedFile->agency->name]['Balloting'][]           = $ballotingAmount;
                $calcData[$assignedFile->agency->name]['downPayment'][]         = $downPayment;
            // }else{
            //     $calcData[$assignedFile->agency->name]['test'][]         = $i++;   
            // }
        }


        // dd(usort($calcData, [$this, 'sortByValue']));
        $fileQty = [];

        foreach ($agencyFiles as $files) {
            $agencyFilesTotal = 0;
            $receivedSum      = 0;
            $totalSum         = 0;
            $booking          = 0;
            $balloting        = 0;
            $downPayment      = 0;
            foreach ($files as $file) {

                foreach ($calcData[$file->agency->name]['installmentReceived'] as $value) {
                    $receivedSum += $value;
                }
                foreach ($calcData[$file->agency->name]['total'] as $value) {
                    $totalSum += $value;
                }
                foreach ($calcData[$file->agency->name]['booking'] as $value) {
                    $booking += $value;
                }

                foreach ($calcData[$file->agency->name]['Balloting'] as $value) {
                    $balloting += $value;
                }

                foreach ($calcData[$file->agency->name]['downPayment'] as $value) {
                    $downPayment += $value;
                }
                // $agencyFilesTotal += $file->file_qty;
                $agencyFilesTotal += count($file->files);
            }
            $agencyData[$file->agency->name]['fileQty']             = $agencyFilesTotal;
            $agencyData[$file->agency->name]['installmentReceived'] = $receivedSum;
            $agencyData[$file->agency->name]['totalAmount']         = $totalSum;
            // $agencyData[$file->agency->name]['bookingReceived'] = $booking;
            $agencyData[$file->agency->name]['ballotingReceived'] = $balloting;
            $agencyData[$file->agency->name]['downPayment']       = $downPayment;
            $agencyData[$file->agency->name]['totalReceived']     = $receivedSum + $downPayment + $balloting;
            $agencyData[$file->agency->name]['currReceievable']   = $totalSum - ($receivedSum + $downPayment + $balloting);
            $agencyData[$file->agency->name]['agency']   = $file->agency->name;

        }
        //    $files = 0;
        // foreach ($agencyData as $key => $value) {
        //     $files += $value['fileQty'];
        // }
        // // dd($files);
        // dd($agencyData);

         usort($agencyData, function($a, $b) {
            // dd($a);
            $retval = $b['fileQty'] - $a['fileQty'];
            // if ($retval == 0) {
            //     $retval = $a['suborder'] - $b['suborder'];
            //     if ($retval == 0) {
            //         $retval = $a['details']['subsuborder'] - $b['details']['subsuborder'];
            //     }
            // }
            return $retval;
        });


        return response()->json(['agencyData' => $agencyData], 200, [], JSON_NUMERIC_CHECK);
    }

    
    // End Sale Summary Report
    
    // Start Feedback Report
    
    public function customerFeedback(Request $request)
    {
        
        $from = date('Y-m-d', strtotime(strtr($request->input('startdate'), '/', '-')));
        $to = date('Y-m-d', strtotime(strtr($request->input('enddate'), '/', '-')));

        $duePersons = [];
        $due_amount = [];
        $sumArr = [];
        $regArr = [];

        $agency = $request->agency_id;

        if ($agency) {

            $bookings = Payments::leftjoin('assign_files', 'assign_files.registration_no', '=', 'bookings.registration_no')->where('bookings.agency_id', $agency)->where('bookings.status', 1)->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])->get();


$bookingArr=[];
            foreach ($bookings as $booking) {
                $bookingArr[] = $booking->registration_no;
            }

            $reg_no = "'". implode("', '", $bookingArr) ."'";

            // dd($reg_no);

            $query = "select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`installment_date` <= '" . $to . "'
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 0
                and 
                    member_installments.registration_no IN ($reg_no)
            UNION ALL
            select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 1
                and 
                    member_installments.registration_no IN  ($reg_no)
            ";
            // dd($query);

            $installments = DB::select($query);

            // $installments = memberInstallments::leftjoin('assign_files', 'assign_files.registration_no', '=', 'member_installments.registration_no')
            //     ->whereIn('member_installments.registration_no', $bookingArr)
            // // ->where('member_installments.is_paid', 0)
            //     ->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])
            //     // ->where('member_installments.installment_date', '<=', $to)
            //     ->where('member_installments.is_active_user', 1)
            //     ->where('member_installments.payment_desc', '!=', 'Down Payment')
            //     ->get();
        } else {

            // $installments = memberInstallments::rightjoin('assign_files', 'assign_files.registration_no', '=', 'member_installments.registration_no')
            //     ->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])
            //     ->where('member_installments.payment_desc', '!=', 'Down Payment')
            //     ->where('member_installments.is_active_user', 1)
            //     ->where('member_installments.installment_date', '<=', $to)
            //     ->orWhere('member_installments.is_paid', 1);

            $query = "select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`installment_date` <= '". $to ."'
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 0
            UNION ALL
            select * 
                from 
                    `member_installments` 
                right join 
                    `assign_files` 
                on 
                    `assign_files`.`registration_no` = `member_installments`.`registration_no` 
                where 
                    `assign_files`.`plot_type` in ('Commercial', 'Housing') 
                and 
                    `member_installments`.`payment_desc` != 'Down Payment' 
                and 
                    `member_installments`.`is_active_user` = 1 
                and 
                    `member_installments`.`deleted_at` is null 
                and 
                    member_installments.is_paid = 1
            ";
            
            $installments = DB::select($query);
                // ->toSql();
        }

         //dd($installments);

        // dd(User::getQuery($installments));


        
        foreach ($installments as $key => $inst) {
            
            // foreach ($installment as $inst) {
                $sumDueAmount = 0;
                $status = ($inst->is_paid == 1) ? "paid" : "unpaid";
                if ($inst->is_active_user = 1) {
                    if ($inst->is_paid == 1)
                        $due_amount[$inst->registration_no]['paid'][] = $inst->amount_received;
                    elseif($inst->os_amount > 0){

                        $due_amount[$inst->registration_no]['paid'][] = $inst->due_amount - $inst->os_amount;
                        $due_amount[$inst->registration_no]['unpaid'][] = $inst->os_amount;
                        $due_amount[$inst->registration_no]['partial'][] = 1;
                    }
                    else
                        $due_amount[$inst->registration_no]['unpaid'][] = $inst->due_amount;
                    
                } else {
                    $due_amount[$inst->registration_no]['unpaid'][] = 0;
                }
                
                $due_amount[$inst->registration_no]['due_amount'][] = $inst->due_amount;
            // }
        }
        //dd($due_amount);

        $member = [];
        $person = [];
        $bookings = [];
        $latePaymentCharges = [];
        $lpc = [];

        $dueAmounts = [];

        $i = 1;
        foreach ($due_amount as $key => $installment) {
            $booking = Payments::with(['agency'])->where('bookings.registration_no', $key)
                ->where('bookings.status', 1)
                ->where('bookings.is_cancelled', 0)
                ->first();
            // Payments::join('assign_files', 'assign_files.registration_no','=','bookings.registration_no')->where('bookings.agency_id',$agency)->where('bookings.status', 1)->whereIn('assign_files.plot_type', ['Commercial', 'Housing'])->get();
            //echo $installment->registration_no;
            if ($booking) {

                $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                if ($memberInfo) {
                    $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();
                    if ($is_member_active) {
                        $dueSum = 0;
                        $paidDueSum = 0;

                        if (isset($due_amount[$key]['unpaid'])) {
                            foreach ($due_amount[$key]['unpaid'] as $key3 => $reg) {

                                $dueSum += $reg;
                            }
                        }

                        if (isset($due_amount[$key]['paid'])) {
                            foreach ($due_amount[$key]['paid'] as $key4 => $reg2) {

                                $paidDueSum += $reg2;
                            }
                        }

                        $assignFile = assignFiles::where('registration_no', $booking->registration_no)->whereIn('plot_type', ['Commercial', 'Housing'])->first();

                        $followup = Followup::where('registeration_no', $booking->registration_no)->orderBy('id', 'DESC')->limit(1)->first();

                        // $dueAmounts['sum'][$key][] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['sum'] = $dueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['paid'] = $paidDueSum;
                        $dueAmounts[$assignFile->plot_type][$key]['member'] = $memberInfo;
                        $dueAmounts[$assignFile->plot_type][$key]['person'] = $is_member_active;
                        $dueAmounts[$assignFile->plot_type][$key]['bookings'] = $booking;
                        $dueAmounts[$assignFile->plot_type][$key]['inst'] = isset($installment['unpaid']) ? $installment['unpaid'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['paid_inst'] = isset($installment['paid']) ? $installment['paid'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['partial'] = isset($installment['partial']) ? $installment['partial'] : [];
                        $dueAmounts[$assignFile->plot_type][$key]['followup'] = $followup ? $followup->meeting_detail : "";

                        // dd($dueAmounts);

                        // dd($dueAmounts);

                        $lpc = LPC::where('registration_no', $booking->registration_no)->orderBy('id', 'DESC')->first();
                        if ($lpc) {
                            $dueAmounts[$assignFile->plot_type]['latePaymentCharges'] = $lpc->lpc_amount + $lpc->lpc_other_amount;
                        }

                    }
                }
            } else {
                $assignFiles = assignFiles::with(['agency'])->where('registration_no', $key)->whereIn('plot_type', ['Commercial', 'Housing'])->first();
                //Open Files
                $dueAmounts[$assignFile->plot_type][$key]['sum'] = 0;
                $dueAmounts[$assignFile->plot_type][$key]['paid'] = 0;
                $dueAmounts[$assignFile->plot_type][$key]['inst'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['paid_inst'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['partial'] = [];
                $dueAmounts[$assignFile->plot_type][$key]['followup'] = "open";
                $dueAmounts[$assignFile->plot_type][$key]['file'] = $assignFiles;
                // dd($dueAmounts);
            }
        }

        // dd($dueAmounts);

        $agencies = agency::whereNull('deleted_at')->orderBy('name')->get();
        // dd($dueAmounts['CC-GS2-1820']['bookings']->paymentPlan->plotSize->plot_size);
        // foreach ($bookings as $key => $booking) {
        //             echo $booking->paymentPlan->plotSize;
        //         }

        //dd($dueAmounts);
        // return view('admin.report.feedback-report', compact('dueAmounts', 'member', 'person', 'bookings', 'latePaymentCharges', 'agencies', 'rights'));
        return response()->json(['bookings' => $bookings], 200, [], JSON_NUMERIC_CHECK);
    }
    
    
    // End Feedback Report
}
