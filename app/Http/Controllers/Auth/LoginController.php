<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\HR\Employees;
use App\User;
use App\Role;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/member';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
	{
		return 'username';
	}

    public function logout(Request $request)
    {
        activity('Authentication')
            ->withProperties(['event' => 'logout'])
            ->log(":causer.name logout successfully");

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
        Session::put('module', 41);
        activity('Authentication')
            ->withProperties(['event' => 'login'])
            ->log($request->input('username')." login successfully");


        // if($user->email_verified != 1)
        // {
        //     Auth::logout();
        //     return redirect()->back()->withInput()->with('message', 'Please confirm your Email');
        // }

        // if($user->is_approved != 1)
        // {
        //     Auth::logout();
        //     return redirect()->back()->withInput()->with('message', 'Admin has not approved your account');
        // }

        // if($user->status != 1)
        // {
        //     Auth::logout();
        //     return redirect()->back()->withInput()->with('message', 'Your account is deleted.');
        // }
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

         activity('Authentication')
            ->withProperties(['event' => 'login'])
            ->log(":causer.name logged in successfully");


        if(Auth::user()->roles[0]->name == 'Admin' || Auth::user()->roles[0]->name == 'Super-Admin')
            return redirect()->intended('admin/dashboard');
        
        else
        return redirect()->intended('member');
        
            
        return redirect()->intended('/');
    }
}
