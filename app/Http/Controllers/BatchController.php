<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\agency;
use App\batch;
use App\Role;
use Validator;
use Illuminate\Validation\Rule;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $rights = Role::getrights('batch');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $batches = batch::whereNull('deleted_at')->get();
            return view('admin.batch.index', compact('batches','rights'));
        }
        else
            abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $rights = Role::getrights('batch');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $agencies = agency::all();

        $batches = batch::where('parent_id',0)->get();

        return view('admin.batch.create', compact('agencies', 'batches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'batch_assign_date' => 'required',
            'file_qty' => 'required|numeric',
            'agency_id' => 'required'
        ]);
        
        

        $batch = $request->input();
        $aid = $request->input('agency_id');
        
        Validator::make($request->all(), [
            'batch_no' => Rule::unique('batch')->where(function ($query) use ($aid) {
                return $query->where('agency_id', $aid)->whereNull('deleted_at');
            }),
        ])->validate();

        // $batch['parent_id'] = ( $request->parent_id == null ) ? 0 : $request->parent_id;

        $batch['batch_assign_date'] = date('Y-m-d', strtotime(strtr($request->input('batch_assign_date'), '/', '-')));

        if(strpos($request->batch_no,'NR'))
            $batch['rate_type'] = "new";
        else
            $batch['rate_type'] = "old";

        if (preg_match('/\blob\b/',$request->batch_no))
        {
            $batch['parent_id'] = 'LOB';
        }
        elseif(preg_match('/\beb\b/' , $request->batch_no))
        {
            $batch['parent_id'] = "EB";
        }else{

            $explode = explode('-', $request->batch_no);
            if((int)$explode[0] != 0){

                if($explode[0] == 'C1' || $explode[0] == 'H1')
                {
                    $batch['parent_id'] = 01;
                }else{
                    $batch['parent_id'] = (int)$explode[0];
                }
            }
        }

        batch::create($batch);

        Session::flash('status', 'success');
        Session::flash('message', 'Batch has been added successfully.');

        return redirect()->route('batch.index');
    }


    public function getBatch(Request $request)
    {
        $batches = batch::where('agency_id', $request->input('agency_id'))->get();
        $options = "<option value=''>Select Batch</option>";

        $id = $request->id ? $request->id : 0;

        foreach ($batches as $key => $batch) {
            if($id == 1)
                $options .= "<option value='".$batch->id."'>".$batch->batch_no."</option>";
            else
                $options .= "<option value='".$batch->batch_no."'>".$batch->batch_no."</option>";
        }

        return $options;
    }

    public function parentBatch(Request $request)
    {
        $batches = batch::where('agency_id', $request->input('agency_id'))->where('parent_id',0)->get();
        $options = "<option value=''>Select Batch</option>";

        foreach ($batches as $key => $batch) {
            $options .= "<option value='".$batch->id."'>".$batch->batch_no."</option>";
        }

        return $options;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $rights = Role::getrights('batch');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $batch = batch::find($id);
        $agencies = agency::all();

        return view('admin.batch.edit', compact('batch','agencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $batchRecord = batch::find($id);

        $batch = $request->input();

        $batch['batch_assign_date'] = date('Y-m-d', strtotime(strtr($request->input('batch_assign_date'), '/', '-')));

        $batchRecord->update($batch);

        Session::flash('status', 'success');
        Session::flash('message', 'Batch has been updated successfully.');

        return redirect()->route('batch.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $batch = batch::find($id);
        $batch->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Batch has been deleted successfully.');

        return redirect()->route('batch.index');
    }
}
