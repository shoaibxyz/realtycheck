<?php

namespace App\Http\Controllers;

use App\InstallmentReceipts;
use App\User;
use App\bank;
use App\memberInstallments;
use App\members;
use App\payment_types;
use App\pdc;
use App\pdc_installments;
use App\receipts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Illuminate\Support\Facades\Session;

class PDCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}
        $pdc = pdc::all();
        return view ('admin.pdc.index' , compact('pdc', 'rights'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        $banks = bank::all();
        $payment_types = payment_types::all();
        return view ('admin.pdc.create',compact('banks', 'payment_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check for the acknowldegement number
        $instrument = explode('/',$request->input('instrument_date'));
        $instrument_date = $instrument[2].'-'.$instrument[1].'-'.$instrument[0];
        
        $ack_no = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);
        
        $ack = pdc::where("acknowledgement_no",$ack_no)->first();
        if($ack != null)
            $ack_number = $ack_no;
        else
            $ack_number = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);

        $input = $request->input();
        $input['created_by'] = Auth::user()->id;
        $input['receiving_date'] =  date('Y-m-d', strtotime(strtr($input['received_date'], '/', '-')));        
        $input['instrument_date'] = $instrument_date;
        $input['acknowledgement_no'] = $ack_number;
        $pdc = pdc::create($input);


        foreach ($request->input('member_installment_id') as $key => $value) {

            $pdc_inst = pdc_installments::create([                
                'registration_no' => $request->input('registration_no')[$key],
                'installment_amount' => $request->input('os_amount')[$key],
                'received_amount' => $request->input('received_amount')[$key],
                'remarks' => $request->input('remarks')[$key],
                'payment_date' => $input['received_date'],
                'pdc_id' => $pdc->id,
                'installment_id' => $value
            ]);
        }

        $message = "PDC added";
        return redirect("admin/pdc")->with(['message' => $message]);
    }

    public function sendclear(Request $request)
    {
       $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        $banks = bank::all();

        if($request->input('instrument_date'))
        {
            $instrument_date = date('Y-m-d', strtotime(strtr($request->input('instrument_date'), '/', '-')));
            $pdcs = pdc::where("instrument_date", $instrument_date)->where('deposit_pending',0)->get();
        }
        elseif($request->input('instrument_no')){
            $pdcs = pdc::where("reference_no", $request->input('instrument_no'))->where('deposit_pending',0)->get();
        }
        else
            $pdcs = pdc::where("instrument_date", date('Y-m-d'))->where('deposit_pending',0)->get();
        
        return view('admin.pdc.send',compact('pdcs','banks'));
    }

    public function sendClearence(Request $request)
    {
       $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        foreach ($request->input('pdc_id') as $key => $value) {
           
            $pdc = pdc::find($value);
            $deposit_date = explode('/',$request->input('deposit_date')[$key]);
            $pdc->deposit_date = $deposit_date[2]."-".$deposit_date[1]."-".$deposit_date[0];
            $pdc->drawee_bank = $request->input('drawee_bank')[$key];
            $pdc->deposit_bank = $request->input('deposit_bank')[$key];
            $pdc->deposit_acc = $request->input('deposit_acc')[$key];
            $pdc->deposit_pending = $request->input('deposit_pending'.$value);
            $pdc->save();
        }

        return redirect()->back()->with(['message' => "Success"]);
    }

    public function destroy($id)
    {
        $pdc = pdc::find($id);

        foreach ($pdc->installments as $installments) {
            $installments->delete();
        }
        $pdc->delete();

        return redirect()->back()->with(['message' => "Success"]);

    }

    public function markclear(Request $request)
    {
       $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}


        $banks = bank::all();

        if($request->input('instrument_date') && $request->input('deposit_date'))
        {
            $instrument_date = date('Y-m-d', strtotime(strtr($request->input('instrument_date'), '/', '-')));
            $deposit_date = date('Y-m-d', strtotime(strtr($request->input('deposit_date'), '/', '-')));
            $pdcs = pdc::where("instrument_date", $instrument_date)->where('deposit_date',$deposit_date)->where('deposit_pending',1)->where('is_clear_return',0)->get();
        }
        elseif($request->input('instrument_date'))
        {
            $instrument_date = date('Y-m-d', strtotime(strtr($request->input('instrument_date'), '/', '-')));
            $pdcs = pdc::where("instrument_date", $instrument_date)->where('deposit_pending',1)->where('is_clear_return',0)->get();
        }
        elseif($request->input('deposit_date'))
        {
            $deposit_date = date('Y-m-d', strtotime(strtr($request->input('deposit_date'), '/', '-')));
            $pdcs = pdc::where("deposit_date", $deposit_date)->where('deposit_pending',1)->where('is_clear_return',0)->get();
        }
        elseif($request->input('instrument_no')){
            $pdcs = pdc::where("reference_no", $request->input('instrument_no'))->where('deposit_pending',1)->where('is_clear_return',0)->get();
        }
        else
            $pdcs = pdc::where('deposit_pending',1)->where('is_clear_return',0)->get();

        
        return view('admin.pdc.markclear',compact('pdcs','banks'));
    }

    public function markClearence(Request $request)
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        $i = 1;
        foreach ($request->input('pdc_id') as $key => $value) {
            $pdc = pdc::find($value);

            $clearReturnDate = explode('/',$request->input('clear_return_date')[$key]);

            if(!is_null($pdc))
            {
                $pdc->is_clear_return = $request->input('is_clear_return')[$key];
                $pdc->clear_return_date = $clearReturnDate[2]."-".$clearReturnDate[1]."-".$clearReturnDate[0];
                $pdc->save();
            }
            $i++;
        }

        return redirect()->back()->with(['message' => "Success"]);
    }

    public function verify()
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        $pdcs = pdc::where('deposit_pending',1)->where('is_clear_return',1)->where('clear_posted',0)->get();
        return view('admin.pdc.verify',compact('pdcs'));
    }

    public function postVerify(Request $request)
    {
        if(!Auth::user()->can('can_create_pdc'))
            abort(403);
        foreach ($request->input('pdc_id') as $key => $pdc_id) {
            
            $pdc = pdc::find($pdc_id);
            $pdc->clear_posted = $request->input('clear_posted')[$key];
            $pdc->save();
        }
        return redirect("admin/pdc/verify")->with(['message' => 'Pdc successfully posted']);
    }

    
    public function printAck($id)
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}

        $ack1=pdc::find($id);
        $member = members::where('registration_no',$ack1->registration_no)->first();

        return view('admin.pdc.acknowledgement' , compact('ack1' , 'member'));
    }

    public function postReceipt(Request $request, $id)
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}
        $pdcs = pdc::find($id);

        $member = members::where('registration_no',$pdcs->registration_no)->first();
        $user = User::where('id',$member->person_id)->first();
        $banks = bank::all();

        $pdc_inst = pdc_installments::where('pdc_id',$pdcs->id)->where('is_posted',0)->get();

        return view('admin.pdc.postReceipt',compact('pdcs','banks','user','pdc_inst'));
    }

    public function Receiptposting(Request $request)
    {
        $rights = Role::getrights('pdc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
 abort(403);
}


        $pdc = pdc::find($request->input('pdc_installment_id'));

        $i = 1;
        $receiptAdded = [];
        $receiptData = [];

        foreach ($request->input('installment_id') as $key => $value) {

            $inst = memberInstallments::find($value);
            $inst->amount_received = $request->input('installment_amount')[$key];
            $inst->receipt_no = $request->input('receipt_no')[$key];

            $pdc_installment = pdc_installments::where('installment_id', $value)->first();

            $results = [];
            foreach ($pdc->installments as $installment1) {
                $results[$installment1->registration_no][] = $installment1->received_amount;
            }

            $regSum = [];
            foreach ($results as $key2 => $result) {
                $sum = 0;
                foreach ($result as $r) {

                    $sum += $r;
                }
                $regSum[$key2] = $sum;
            }

            if($inst->due_amount < $pdc_installment->received_amount)
            {
                $is_paid = 0;
                $os_amount = $inst->due_amount - $pdc_installment->received_amount;
            }else{
                $is_paid = 1;
                $os_amount = 0;
            }

            $inst->is_paid = $is_paid;
            $inst->os_amount = $os_amount;
            $inst->save();

            // $sum[$pdc_installment->registration_no] = $pdc_installment->receiving_amount;


            if(!in_array($pdc_installment->registration_no, $receiptAdded))
            {
                //Post in receipts
                $receipt = new receipts();
                $receipt->receipt_no = $request->input('receipt_no')[$key];
                $receipt->registration_no =  $pdc_installment->registration_no;
                $receipt->receiving_date = $pdc->clear_return_date; //Date on which cheque is cleared
                $receipt->receiving_mode = $pdc->instrument_type;
                // $receipt->received_amount = $pdc_installment->received_amount;
                $receipt->reference_no = $pdc->reference_no;
                $receipt->instrument_date = $pdc->instrument_date;
                $receipt->drawee_bank = $pdc->drawee_bank;
                $receipt->bank_id = $pdc->drawee_bank;
                $receipt->deposit_account_no = $pdc->deposit_acc;
                $receipt->deposit_date = $pdc->deposit_date;
                $receipt->receipt_remarks = $pdc->pdc_remarks;
                $receipt->pdc_ack = $pdc_installment->id;
                $receipt->pdc_id = $pdc->id;
                $receipt->save();
                $receiptAdded[] = $pdc_installment->registration_no;
                $receiptData[] = $receipt;
            }
        }

        $j =1;
        foreach ($receiptData as $key => $receipt) {
            
            $receipt->received_amount = $regSum[$receipt->registration_no];
            $receipt->save();
            
            $pdc_installment = pdc_installments::find($receipt->pdc_ack);
            $installment = memberInstallments::find($pdc_installment->installment_id);

            if($installment->due_amount < $pdc_installment->received_amount)
            {
                $is_paid = 0;
                $os_amount = $installment->due_amount - $pdc_installment->received_amount;
            }else{
                $is_paid = 1;
                $os_amount = 0;
            }


            $pre_instal_receipts = InstallmentReceipts::where('registration_no',$request->input('registration_no'))->where('os_amount','!=',0)->limit(1)->orderBy('id','desc')->first();

            if($is_paid && $j == 1 && $pre_instal_receipts){
                $os_amount = $pre_instal_receipts->os_amount;
                $pre_instal_receipts->os_amount = 0;
                $pre_instal_receipts->save();
            }

            // echo $is_paid;
            // dd($os_amount);
            // dd($pre_instal_receipts);

            $receipt->due_amount = $installment->due_amount;
            $receipt->receipt_id = $receipt->id;
            $receipt->installment_id = $installment->id;

            $receipt->receipt_amount = $regSum[$receipt->registration_no];

            if($is_paid ==1)
                $receipt->received_amount = $receipt->received_amount;
            
            $receipt->payment_mode = $pdc->instrument_type;
            $receipt->os_amount = $installment->os_amount;
            $receipt->rebate_amount = $installment->rebate_amount;
            $receipt->receipt_date = $installment->payment_date;
            $receipt->payment_desc = $installment->payment_desc;
            $receipt->installment_no = $installment->installment_no;
            $receipt->due_date = $installment->installment_date;
            $receipt->registration_no = strtoupper($installment->registration_no);

            $inst_receipts_added = InstallmentReceipts::create($receipt->toArray());
            $j++;
        }

        foreach ($pdc->installments as $inst) {
            $inst->is_posted = 1;
            $inst->save();
        }
        

        return redirect("admin/pdc/verify")->with(['message' => 'Pdc successfully posted']);
    }
}
