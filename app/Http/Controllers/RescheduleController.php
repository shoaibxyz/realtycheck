<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReschedulePaymentPlan;
use App\Installment;
use App\Payments;
use App\agents;
use App\featureset;
use App\members;
use App\otherPayments;
use App\paymentSchedule;
use App\payment_types;
use App\reschedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Illuminate\Support\Facades\Session;

class RescheduleController extends Controller
{
    public function index()
    {

    $rights = Role::getrights('reschedule');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}


        $reschedules = reschedule::all();
        return view('admin.reschedule.index', compact('reschedules', 'rights'));
    }

    public function create()
    {
          $rights = Role::getrights('reschedule');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        $paymentSchedule = paymentSchedule::all();
        $featureset = featureset::all();
        $agents = agents::all();
        $paymentTypes = payment_types::all();

        return view('admin.reschedule.create', compact('paymentSchedule','featureset','agents','paymentTypes'));
    }

    public function store(ReschedulePaymentPlan $request)
    {
        $info = Payments::Info($request->input('registration'));
        $plan = $info->paymentPlan;

        $TotalInstallments = 0;
        foreach ($request->input('installment_amount') as $key => $value) {
                $TotalInstallments+=$value;
        }

        if((int)$TotalInstallments != $request->input('total_amount_plan'))
            return redirect()->back()->withErrors(['message' => 'Total amount not match with installment amount']);

        $data = [
            'plot_size_id' => $plan->plot_size_id,
            'plot_nature' => $plan->plot_nature,
            'plot_dimensions' => $plan->plot_dimensions,
            'pac_percentage' => $plan->pac_percentage,
            'booking_percentage' => $plan->booking_percentage,
            'booking_price' => $plan->booking_price,
            'balloting_percentage' => $plan->balloting_percentage,
            'possession_percentage' => $plan->possession_percentage,
            'semi_anual_percentage' => $plan->semi_anual_percentage,
            'monthly_percentage' => $plan->monthly_percentage,
            'payment_code' => $request->input('payment_code'),
            'payment_desc' => $request->input('payment_desc'),
            'total_installments' => $request->input('total_installments'),
            'payment_frequency' => $request->input('payment_frequency'),
            'total_price' => $request->input('total_amount_plan'),
            'installments' => $request->input('total_installments'),
            'is_rescheduled' => 1
        ];


        $paymentPlan = paymentSchedule::create($data);


        //Create installment Plan
        foreach ($request->input('installment_date') as $key => $value) {
            $data = [
                'installment_date' => $value,
                'installment_amount' => $request->input('installment_amount')[$key], 
                'installment_no' => $request->input('installment_no')[$key], 
                'rebat_amount' => $request->input('rebat_amount')[$key], 
                'amount_received' => $request->input('amount_received')[$key], 
                'remarks' => $request->input('installment_remarks')[$key],
                'payment_schedule_id' => $paymentPlan->id
            ];
            //Save plan based in installment_id
            Installment::create($data); 
        }

        //Fetch old other payments of current payment plan and save them with new plan id
        $otherPayments = otherPayments::where('payment_schedule_id', $plan->id)->get();

        foreach ($otherPayments as $otherPayment) {
            $otherData = [
                    'installment_no' => $otherPayment->installment_no,
                    'installment_date_creteria' => $otherPayment->installment_date_creteria,
                    'installment_date' => $otherPayment->installment_date,
                    'days_or_months' => $otherPayment->days_or_months,
                    'amount_creteria' => $otherPayment->amount_creteria,
                    'amount_percentage' => $otherPayment->amount_percentage,
                    'amount_to_receive' => $otherPayment->amount_to_receive,
                    'include_in_total' => $otherPayment->include_in_total,
                    'payment_type_id' => $otherPayment->payment_type_id,
                    'payment_schedule_id' => $paymentPlan->id,
            ];
            otherPayments::create($otherData); 
        }

        $reschedule = [
            'current_payment_plan' => $plan->id,
            'new_payment_plan' => $paymentPlan->id,
            'registration_no' => $request->input('registration'),
            'reschedule_date' => $request->input('reschedule_date'),
            'remarks' => $request->input('remarks'),
        ]; 

        reschedule::create($reschedule);

        //Update the member payment schedule id
        $info->payment_schedule_id = $paymentPlan->id;
        $info->save();




        $message = "Plan has been rescheduled.";
        return redirect('admin/reschedule')->with(['message' => $message, 'status' => 1]);
    }

    public function destroy($id)
    {
          $rights = Role::getrights('reschedule');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_delete){
abort(403);
}

        $payment_schedule = reschedule::find($id);
        $payment_schedule->delete();
        return redirect('admin/reschedule');
    }

    public function edit($id)
    {
               $rights = Role::getrights('reschedule');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_edit){
abort(403);
}

        $units            = units::all();
        $payment_schedule = reschedule::find($id);
        $plotSize = plotSize::all();
        return view('admin.reschedule.edit', compact('payment_schedule', 'units','plotSize'));
    }

    public function update($id, Request $request)
    {
        $payment_schedule = reschedule::find($id);
        $payment_schedule->update($request->input());

        return redirect('admin/reschedule');
    }

    public function propertyInfo(Request $request, $property)
    {
        if ($request->ajax()) {
            $proper = property::findorfail($property);
            return $proper;
        } else {
            return $property;
        }
    }

    public function PlanInfo(Request $request)
    {
        if( $request->ajax() )
        {
           $plan = paymentSchedule::findorfail($request->input('payment_schedule_id'));
            return $plan;
        } 
    }
}
