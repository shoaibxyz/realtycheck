<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\category;


class CategoryController extends Controller
{
    

    public function index(){

         $category = category::all();
         
        
        return view('admin.category.index' , compact('category'));
    }

    public function create()
    {
        
        return view ('admin.category.create');
        
    }

    public function store(Request $request){

        $category = new category();
        
        //dd($request->input());
        

        $category->name = $request->input('name');
        $category->Created_by = Auth::user()->id;
        $category->Updated_by = Auth::user()->id;
      
        
           
        $category->save();

        return redirect('admin/category');
    }
      
    //   public function store(Request $request){

       // $category = new category();

       // $category->name = $request->input('name');

      //  $category->save();

       // return redirect('admin/category');
   // }
    
     public function edit(Request $request , $id){

        $category = category::where('id' , $id)->first();

       

       return view ('admin.category.edit' , compact('category'));
    }


    public function update(Request $request , $id){

        $category = category::where('id' , $id)->first();

        $category->name = $request->input('name');

        
        $category->save();

        return redirect('admin/category');

        
    }

    public function destroy($id){

        $category = category::where('id' , $id)->first();

        
        $category->delete();

        return redirect('admin/category');
    
    
}

}

