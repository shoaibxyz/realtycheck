<?php

namespace App\Http\Controllers;

use App\featureset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Role;
use Validator;

class FeatureSetController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('features');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $featureset = featureset::all();
            return view('admin.featureset.index', compact('featureset','rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function create()
    {
        return view('admin.featureset.create');
    }



    public function store(Request $request)
    {
        //authorization code is added by Habib
$rights = Role::getrights('features');
if($rights->can_create){

        Validator::make($request->all(), [
            'feature' => 'unique:feature_set',
        ])->validate();

        featureset::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Feature has been added successfully.');

        return redirect('admin/features');
    }
    else{
        return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    }

    public function destroy($id)
    {
        $rights = Role::getrights('features');
        if($rights->can_delete){

        $featureset = featureset::find($id);
        $featureset->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Feature has been deleted successfully.');

        return redirect('admin/features');
  }
   else{
        return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

}


    


    public function edit($id)
    {
        $featureset = featureset::find($id);
        return view('admin.featureset.edit',compact('featureset'));
    }



    public function update($id, Request $request)
    {
        $rights = Role::getrights('features');
       if($rights->can_edit){

        $featureset = featureset::find($id);
        $featureset->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Feature has been updated successfully.');

        return redirect('admin/features');
    }
     else{
        return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }


}


}

