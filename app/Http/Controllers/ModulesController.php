<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Role;
use Validator;
use App\modules;

class ModulesController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('modules');
        if(!$rights->can_view){
    	    abort(403);
        }
        $modules = modules::all();
        return view('admin.modules.index', compact('modules', 'rights'));
    }

    public function create()
    {
        $rights = Role::getrights('modules');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.modules.create');
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:modules',
        ])->validate();

        modules::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been added successfully.');

        return redirect()->route('module.index');
    }

    public function destroy($id)
    {
        $modules= modules::find($id);
        $modules->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been deleted successfully.');
        return redirect()->route('module.index');
    }

    public function edit($id)
    {
        $rights = Role::getrights('modules');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $modules = modules::find($id);
        return view('admin.modules.edit',compact('modules'));
    }

    public function update($id, Request $request)
    {
        /* Validator::make($request->all(), [
            'name' => 'unique:modules',
        ])->validate();
 */
        $modules = modules::find($id);

        $modules->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been updated successfully.');

        return redirect()->route('module.index');
    }

    public function search(Request $request, $name)
    {
      $modules = modules::all();
      $alldata = modules::Where('name', 'LIKE', '%' . $name . '%')->first();
      $name=$request->input('name');
      if(!empty($name))
      {
      }
      $alldata=$alldata->paginate(5);
      return view ('admin.modules.search',compact('modules','alldata'));
    }
}
