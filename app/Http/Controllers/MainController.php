<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\PaymentScheduleRequest;
use App\InstallmentReceipts;
use App\LPC;
use App\Payments;
use App\SMSModel;
use App\SMSTemplates;
use App\Traits\SMS;
use App\User;
use App\agency;
use App\agents;
use App\alerts;
use App\assignFiles;
use App\featureset;
use App\memberInstallments;
use App\members;
use App\otherPayments;
use App\paymentSchedule;
use App\payment_types;
use App\pdc;
use App\plotSize;
use App\receipts;
use App\sms_detail;
use App\transfers;
use Carbon\Carbon;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class MainController extends Controller
{

}
