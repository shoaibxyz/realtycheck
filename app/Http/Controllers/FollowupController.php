<?php

namespace App\Http\Controllers;

use App\Followup;
use App\Http\Requests\uploadRequest;
use App\User;
use App\Payments;
use App\followup_files;
use App\members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Role;

class FollowupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('followup');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
        $followup = Followup::all();
        return view('admin.followup.index', compact('followup'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function getFollowup()
    {
         $followup = Followup::all();
         return Datatables::of($followup)
                ->addColumn('edit', function($followup){
                    return '<a href="'.route('followup.edit', $followup->id ).'" class="btn btn-xs btn-info"> Edit</a>';
                })
                ->addColumn('delete', function($followup){
                     return '<form method="post" action="'.route('followup.destroy', $followup->id).'">'.csrf_field()."<input name='_method' type='hidden' value='DELETE'> <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'></form>";
                })
                ->editColumn('registeration_no', function($followup){
                    return $followup->registeration_no;
                })
                ->rawColumns(['edit','delete'])
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('followup');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view){
         $ccd = User::CCD();
        return view('admin.followup.add-followup', compact('ccd'));
    }
    else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(uploadRequest $request)
    {   
        $input = $request->input();

        $input['meeting_date'] = date('Y-m-d', strtotime(strtr($input['meeting_date'], '/', '-')));
        $input['created_by'] = Auth::user()->id;
        if($input['meeting_status'] == 0)
            $input['next_meeting_date']   = date('Y-m-d', strtotime(strtr($input['next_meeting_date'], '/', '-')));

        $followup = Followup::create($input);

        foreach ($request->files as $files) {
            if(is_array($files))
            {
                foreach ($files as $key => $file) {
                    $upload_path = public_path() . '/followup';
                    $filename    = Auth::user()->id .'-'. uniqid() . '-' . $file->getClientOriginalName();
                    $file->move($upload_path, $filename);

                    $followup_files = followup_files::create([
                                            'followup_id' => $followup->id,
                                            'file_name' => $filename,
                                            'registeration_no' => $input['registeration_no']
                                        ]);
                }
            }
        }

        
            if(Auth::user()->Roles->first()->name == 'principal-officer'){
         return redirect('admin/generate_lead')->with(['message' => 'Followup created']);
            }

            if(Auth::user()->Roles->first()->name == 'agency'){
        return redirect('admin/generate_lead')->with(['message' => 'Followup created']);
            }

            if(Auth::user()->Roles->first()->name == 'agent'){
        return redirect('admin/generate_lead')->with(['message' => 'Followup created']);
            }

        return redirect('admin/followup/'.$input['registeration_no'])->with(['message' => 'Followup created', 'status' => 1]);

    }
    
     public  function Search(Request $request)
    {
        $registration_no = $request->registration_no;
        $html = $this->show($request, $registration_no, 1);

        return response()->json(['html' => $html]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $registeration_no = null, $isAjaxRedner = 0)
    {
        // if(!Auth::user()->can('can_view_followup'))
        //     abort(403);

       
       //dd($registeration_no);
        $followup = Followup::where('registeration_no',$registeration_no)->orderBy('id', 'DESC')->get();
        $ccd = User::CCD();
        
         $bookingList = Payments::select('bookings.registration_no')
        ->join('members', 'members.registration_no','=', 'bookings.registration_no')
        ->join('persons', 'members.person_id','=', 'persons.id')
        ->join('payment_schedule', 'payment_schedule.id','=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id','=', 'payment_schedule.plot_size_id')
        ->where('bookings.status',1)
        ->where('bookings.is_cancelled',0)
        ->get();

        if($isAjaxRedner == 1)
        {
            return view('admin.followup._add-followup', compact('ccd','followup','registeration_no'))->render();
        }
        
        return view('admin.followup.add-followup', compact('ccd','followup','registeration_no','bookingList'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('followup');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            
if($rights->can_edit){
        $followup = Followup::find($id);
        $files = followup_files::where('registeration_no',$followup->registeration_no)->get();
        $ccd = User::CCD();
        return view('admin.followup.edit-followup', compact('followup', 'ccd','files'));
}
else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    
}


    public function deleteAttachment($id)
    {

        $followup_file = followup_files::find($id);
        
        unlink(public_path('followup/'.$followup_file->file_name));

        $followup_id = $followup_file->followup_id;
        
        $followup_file->delete();

        return redirect('admin/followup/'.$followup_id.'/edit')->with(['message' => 'file deleted successfully', 'status' => 1]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $followup = Followup::find($id);

        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        $input['meeting_date'] = date('Y-m-d', strtotime(strtr($input['meeting_date'], '/', '-')));
        $input['created_by'] = Auth::user()->id;
        if($input['meeting_status'] == 0)
            $input['next_meeting_date']   = date('Y-m-d', strtotime(strtr($input['next_meeting_date'], '/', '-')));


        $followup->update($input);

        foreach ($request->files as $files) {
            if(is_array($files))
            {
                foreach ($files as $key => $file) {
                    $upload_path = public_path() . '/followup';
                    $filename    = Auth::user()->id .'-'. uniqid() . '-' . $file->getClientOriginalName();
                    $file->move($upload_path, $filename);

                    $followup_files = followup_files::create([
                                            'followup_id' => $followup->id,
                                            'file_name' => $filename,
                                            'registeration_no' => $input['registeration_no']
                                        ]);
                }
            }
        }

        return redirect('admin/followup/'.$id.'/edit')->with(['message' => 'Followup updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $rights = Role::getrights('followup');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
         $followup = Followup::find($id);
        $followup->delete();
        return redirect()->route('followup.index');
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function dueMeetings()
    {
        $ccd = User::CCD();
        return view('admin.followup.due_meetings',compact('ccd'));
    }

    public function getDueMeetings(Request $request)
    {
        $from_date = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-')));
        $to_date   = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-')));

        $followup = Followup::whereBetween('next_meeting_date',[$from_date, $to_date])->where('next_meeting_by',$request->input('user'))->where('meeting_status', 0)->groupBy('followup.registeration_no')->orderBy('id', 'DESC')->get();

    
        $ccd = User::CCD();
        return view('admin.followup.due_meetings',compact('ccd','followup'));
    }

    public function viewAttachment($id)
    {
         $followup = Followup::find($id);
         return view('admin.followup.view-attachments', compact('followup'));
    }
}
