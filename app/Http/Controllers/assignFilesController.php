<?php

namespace App\Http\Controllers;

use App\Http\Requests\assignFilesRequest;
use App\Payments;
use App\agency;
use App\assignFiles;
use App\batch;
use App\plotSize;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class assignFilesController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('assign-file');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            return view('admin.assignFiles.index',compact('rights'));
        }
        else
    	    abort(403);
    }

    public function getFiles(Request $request)
    {
        $search = $request->squery;
        
        $from = '';
        $to = '';
        if($request->startdate)
            $from   = date('Y-m-d', strtotime(strtr($request->startdate, '/', '-')));
        if($request->enddate)
            $to = date('Y-m-d', strtotime(strtr($request->enddate, '/', '-')));
            
        $assignFiles = assignFiles::select(DB::raw('assign_files.*, agency.name as agency_name, plot_size.plot_size as plot_size,batch.batch_no as batch_no, batch.batch_assign_date as batch_assign_date'))
        ->join('agency','agency.id','=','assign_files.agency_id')
        ->join('plot_size','plot_size.id','=','assign_files.plot_size_id')
        ->join('batch','batch.id','=','assign_files.batch_id')
         ->when($from, function($query) use ($from, $to){
                $query->whereBetween('batch.batch_assign_date', [$from, $to]);
            })
        // ->when($search, function ($query) use ($search) {
        //     $query->where(function($q) use ($search){

        //         $q->where('plot_size.plot_size','LIKE', '%'.$search.'%');
        //         $q->orWhere('agency.name', 'like', '%'.$search.'%');
        //         $q->orWhere('registration_no', 'like', '%'.$search.'%');
        //         $q->orWhere('plot_type', 'like', '%'.$search.'%');
        //         $q->orWhere('batch.batch_no', 'like', '%'.$search.'%');
        //         $q->orWhere('batch.batch_assign_date', 'like', '%'.$search.'%');
        //         if(strtolower($search) == "yes" || strtolower($search) == "no"){
        //             $search = $search == "yes" ? 1 : 0;
        //             $q->orWhere('is_reserved', 'like', '%'.$search.'%');
        //         }
        //         return $q;
        //     });
        // })
        ->whereNull('assign_files.deleted_at')
        ->where('is_cancelled',0)->orWhere('is_cancelled',null)
        ->orderBy('assign_files.id','DESC')->get();

        // dd($assignFiles);
         return Datatables::of($assignFiles)
         ->editColumn('id', function($assignFiles){
             return $assignFiles->id;
            })
            ->addColumn('plot_size', function($assignFiles){
                return $assignFiles->plot_size;
            })
            ->editColumn('plot_type', function($assignFiles){
                return $assignFiles->plot_type;
            })
            ->addColumn('registration_no', function($assignFiles){
                return $assignFiles->registration_no;
            })
            ->addColumn('agency', function($assignFiles){
                return $assignFiles->agency_name;
            })
            ->addColumn('batch_no', function($assignFiles){
                return $assignFiles->batch_no;
            })
            ->addColumn('batch_date', function($assignFiles){
                return ($assignFiles->batch_assign_date != null) ? date('d-m-Y', strtotime($assignFiles->batch_assign_date)) : "";
            })
            ->editColumn('is_reserved', function($assignFiles){
                $booking = \App\Payments::where('registration_no',$assignFiles->registration_no)->first();
                if($assignFiles->is_reserved == 0)
                    return "No";
                else
                {
                    if($booking->is_cancelled == 0)
                        return "Yes";
                    elseif($booking->is_cancelled == 1)
                        return "Cancelled";
                    elseif($booking->is_cancelled == 2)
                        return "Refunded";
                    elseif($booking->is_cancelled == 3)
                        return "Merged";
                }
            })
            ->addColumn('block', function($assignFiles){

                    $block = ($assignFiles->is_blocked == 1) ? "btn-danger" : "btn-success";
                    $blockText = ($assignFiles->is_blocked == 1) ? "Block" : "Active";
                    $block_reverse = $assignFiles->is_blocked == 1 ? 0 : 1;

                    return '<a href="'.route('assign-file.block', [ $assignFiles->registration_no, $block_reverse ] ).'" class="btn btn-xs '.$block.'">'.$blockText.'</a>';
            })
            /*->addColumn('edit', function($assignFiles){
                $rights = Role::getrights('assign-file');
                $se = $rights->show_edit;
                return '<a href="'.route('assign-file.edit',$assignFiles->id).'" class="btn btn-xs btn-primary" style="'.$se.'"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })*/
            ->addColumn('actions', function($assignFiles){
                $rights = Role::getrights('assign-file');
                $se = $rights->show_edit;
                $de = $rights->show_delete;
                return '<a href="'.route('assign-file.edit',$assignFiles->id).'" class="btn btn-xs btn-primary" style="'.$se.'; float:left;"><i class="fa fa-edit"></i></a>
                        <form method="post" action="'.route('assign-file.destroy', $assignFiles->id).'" style="'.$de.'">'.csrf_field()."<input name='_method' type='hidden' value='DELETE'> <button type='submit' class='btn btn-danger btn-xs DeleteBtn'><i class='fa fa-trash-o'></i></button></form>";
            })
            ->rawColumns(['actions','block'])
            ->make(true);
    }

    public function index2()
    {
        $assignFiles = assignFiles::all();
        return view('admin.assignFiles.index', compact('assignFiles'));
    }

    public function create()
    {
        $rights = Role::getrights('assign-file');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $agencies = agency::all();
        $plotSize = plotSize::all();
        $batches = batch::all();
        return view('admin.assignFiles.create',compact('agencies','plotSize', 'batches'));
    }

    public function store(assignFilesRequest $request)
    {
        $files = $request->input();
        $files['created_by'] = Auth::user()->id;
        assignFiles::create($files);

        Session::flash('status', 'success');
        Session::flash('message', 'Files have been assigned successfully.');

        return redirect('admin/assign-file');
    }

    public function destroy($id)
    {
        $assignFiles = assignFiles::find($id);
        $assignFiles->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Assign file has been deleted successfully.');

        return redirect('admin/assign-file');
    }

    public function edit($id)
    {
        $rights = Role::getrights('assign-file');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $assignFiles = assignFiles::find($id);
        $agencies = agency::all();
        $plotSize = plotSize::all();
        $batches = batch::all();

        $agencyBatch = batch::where('agency_id', $assignFiles->agency->id)->get();

        return view('admin.assignFiles.edit',compact('assignFiles','agencies','plotSize','batches', 'agencyBatch'));
    }

    public function update($id, Request $request)
    {
        $input = $request->input();
        $input['batch_assign_date'] = date('Y-m-d', strtotime(strtr($request->input('batch_assign_date'), '/', '-')));
        $input['updated_by'] = Auth::user()->id;

        $assignFiles = assignFiles::find($id);

        $assignFiles->update($input);

        Session::flash('status', 'success');
        Session::flash('message', 'Assign file has been updated successfully.');

        return redirect('admin/assign-file');
    }

    public function block($registration_no , $block)
    {
        $assignFiles = assignFiles::where('registration_no', $registration_no)->first();
        $assignFiles->is_blocked = $block;
        $assignFiles->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Status changed successfully.');

        return redirect('admin/assign-file');
    }
}
