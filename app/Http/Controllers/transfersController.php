<?php

namespace App\Http\Controllers;

use App\Helpers;
use App\LPC;
use App\Payments;
use App\TransferCharges;
use App\TransferDocs;
use App\TransferImages;
use App\User;
use App\Role;
use App\members;
use App\transfers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use DB;


class transfersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transfers = transfers::all();
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        return view('admin.transfers.index', compact('transfers','rights'));
    }

    public function init()
    {
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_print){
    	    abort(403);
        }
        $docs = Helpers::TransferDocuments();
        $rights = Role::getrights('transfer-application');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if(!$rights->can_print){
            abort(403);
        }
        return view('admin.transfers.init', compact('docs'));   
    }

    public function ndc($id = null)
    {
        $rights = Role::getrights('ndc');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        if($id)
        {
            $transfer = transfers::find($id);
            
            $data['transfer'] = $transfer;
            $data['currOwner'] = members::find($transfer->current_user_id);
            $data['currOwner']['user'] = members::find($transfer->current_user_id)->user;
            $data['newOwner'] = members::find($transfer->new_user_id);
            $data['newOwner']['user'] = members::find($transfer->new_user_id)->user;

            $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
            $data['plot'] = $booking->paymentPlan->plotSize;

            return view('admin.transfers.ndc1', compact('data'));       
        }

        return view('admin.transfers.ndc', compact('rights'));   
    }

     

    public function postInit(Request $request)
    {
        if(!$request->transfer_docs)
            return redirect()->back()->with(['status' => 0, 'message' => 'Please select some documents.']);
        $member = members::where('registration_no', $request->input('registration_no'))->where('is_current_owner',1)->first();
        $booking = Payments::where('registration_no', $request->input('registration_no'))->first();

        $data['member'] = $member;
        $data['user'] = $member->user;
        $data['booking'] = $booking;

        foreach($request->transfer_docs as $doc)
        {
            $transferDocs = TransferDocs::create([
                'registration_no' => $request->input('registration_no'),
                'member_id' => $member->id,
                'doc_id' => $doc,
            ]);
        }
        
        $booking = Payments::Active()->where('registration_no', $request->input('registration_no'))->first();

        $tCharges = TransferCharges::where('plot_size_id', $booking->paymentPlan->plotSize->id)->first();

        if(is_null($tCharges))
        {
            $request->session()->flash('status', 0);
            $request->session()->flash('message', 'Transfer Charges are not defined for this plot size.  Please define transfer charnges to proceed.');
            return redirect()->route('transfer.create')->withInput();
        }

        // $check = transfers::where('reg_no', $request->input('registration_no'))->where('is_ndc_approved',0)->first();

        
        // if($check){
        //     $request->session()->flash('status', 0);
        //     $request->session()->flash('message', 'This transfer is not yet completed! You cannot download form again');
        //     return redirect()->route('transfer.create')->withInput();
        // }

        $checkUser = User::where('cnic', $request->input('cnic1'))->first();

        if($checkUser)
            $person = $checkUser;
        else{
            $user = new User();
            
             if (Input::hasfile('buyer_photo')) {
                $image       = Input::file('buyer_photo');
                $upload_path = public_path() . '/images';
                $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
                $image->move($upload_path, $filename);
                $path            = $upload_path . $filename;
                $user['picture'] = $filename;
            }

            $user->name              = $request->input('name1');
            $user->gurdian_name      = $request->input('guardian_name1');
            $user->guardian_type      = $request->input('guardian_type1');
            $user->cnic              = $request->input('cnic1');
            $user->nationality              = $request->input('nationality1');
            $user->current_address              = $request->input('address11');
            $user->permanent_address              = $request->input('address21');
            $user->country_id              = $request->input('country_id1');
            $user->city_id              = $request->input('city_id1');
            $user->phone_office           = $request->input('phone_office1');
            $user->phone_res           = $request->input('phone_rec1');
            $user->phone_mobile           = $request->input('phone_mobile1');
            $user->	email           = $request->input('email1');
            $user->is_approved       = 1;
            $user->created_by = Auth::user()->id;
            $user->save();
            $person = $user;
        }

        //$member = User::where('id',  $request->input('person_id'))->last();
        
        $membr = members::where('registration_no', $request->input('registration_no'))->where('is_current_owner',1)->first();

        $member    = $person->id;
        $newmember = members::create([
            'registration_no' => $request->input('registration_no'),
            'reference_no' => $membr->reference_no,
            'person_id'       => $member,
            'created_by'       => Auth::user()->id,
            'nominee_name'       => $request->input('nominee_name'),
            'nominee_guardian'       => $request->input('nominee_guardian'),
            'nominee_cnic'       => $request->input('nominee_cnic'),
            'relation_with_member'       => $request->input('relation_with_member'),
            // 'is_current_owner' => 1
        ]);

        $transfer = transfers::create([
            'reg_no'          => $request->input('registration_no'),
            'current_user_id' => $membr->id,
            'new_user_id'     => $newmember->id,
            'transfer_date'   => date('Y-m-d', strtotime(strtr($request->input('transfer_date'), '/', '-'))),
            'tranfer_charges' => $tCharges->charges,
            'tranfer_by'      => Auth::user()->id,
            'remarks'         => $request->input('remarks'),
        ]);

        $transfer->transfer_no = str_pad($transfer->id, 4, 0, STR_PAD_LEFT);
        $transfer->save();
        
        $person->Roles()->attach(1);
        
        Session::flash('status', 'success');
        Session::flash('message', 'Transfer application has been submitted successfully.');
        //return Redirect::to('admin/transfer');
        return redirect('admin/transfer');
        /*return view('admin.transfers.applicationForm', $data);
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.transfers.applicationForm', $data);
        return $pdf->stream($member->registration_no . '-transfer.pdf');*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        /*$member = members::all('registration_no');
        $user      = User::all();
        $transfers = transfers::all();
        return view('admin.transfers.create', compact('member', 'user', 'transfers'));
        */
        
        return view('admin.transfers.create');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transfer = transfers::where('transfer_no',$request->transfer_no)->first();
        
        if($request->file('documents'))
        {
            foreach ($request->file('documents') as $document) 
            {
                $transferDocument = new TransferImages;
                $upload_path = storage_path() . '/app/public/transfer/documents';
                $filename    = time() . uniqid() . '-' . $document->getClientOriginalName();
                $document->move($upload_path, $filename);
                $path            = $upload_path . $filename;
                $transferDocument->document = $filename;
                $transferDocument->transfer_id = $transfer->id;
                $transferDocument->is_document = 1;        
                $transferDocument->created_by = Auth::user()->id;
                $transferDocument->save();
            }
        }

        if($request->input('picture'))
        {
            foreach ($request->input('picture') as $picture) {
                $transferImage = new TransferImages;
                $transferImage->transfer_id = $transfer->id;    
                $transferImage->picture = $picture;    
                $transferImage->is_document = 0;
                $transferImage->created_by = Auth::user()->id;
                $transferImage->save();            
            }
        }
        
        
        if (Input::hasfile('seller_photo')) {
            mkdir("./public/images/ndc/" . $transfer->current_user_id);
            $image       = Input::file('seller_photo');
            $upload_path = public_path() . '/images/ndc/'. $transfer->current_user_id;
            
           
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path              = $upload_path . $filename;
            
            $transfer->current_user_pic = $filename;
            $transfer->save();
        }
        
        if (Input::hasfile('buyer_photo')) {
            mkdir("./public/images/ndc/" . $transfer->new_user_id);
            $image       = Input::file('buyer_photo');
            $upload_path = public_path() . '/images/ndc/'. $transfer->new_user_id;
            
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path              = $upload_path . $filename;
            
            $transfer->new_user_pic = $filename;
            $transfer->save();
        }

        $message = "Documents attached successfully";
        return redirect('admin/transfer')->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transfer = transfers::find($id);
        $data['currOwner'] = members::find($transfer->current_user_id);
        $data['currOwner']['user'] = members::find($transfer->current_user_id)->user;
        $data['newOwner'] = members::find($transfer->new_user_id);
        $data['newOwner']['user'] = members::find($transfer->new_user_id)->user;

        $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
        $data['paymentPlan'] = $booking->paymentPlan;
        $data['booking'] = $booking;
        $data['transfer'] = $transfer;

        $transferImages = TransferImages::where('transfer_id', $transfer->id)->get();

        // dd($transferImages);

        return view('admin.transfers.show', compact('data','transferImages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $transfer = transfers::find($id);

        $currOwner = members::find($transfer->current_user_id);
        $currUser = members::find($transfer->current_user_id)->user;
        $newOwner = members::find($transfer->new_user_id);
        $newUser = members::find($transfer->new_user_id)->user;

        $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
        $paymentPlan = $booking->paymentPlan;

        return view('admin.transfers.edit', compact('transfer', 'currOwner', 'currUser', 'newUser', 'newOwner','paymentPlan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($request->input('user_id'));

        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path            = $upload_path . $filename;
            $user['picture'] = $filename;
        }
        
        if (Input::hasfile('cnic_pic')) {
            $image       = Input::file('cnic_pic');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path            = $upload_path . $filename;
            $user['cnic_pic'] = $filename;
        }
        
        if (Input::hasfile('affidavit_address')) {
            $image       = Input::file('affidavit_address');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path            = $upload_path . $filename;
            $user['affidavit_address'] = $filename;
        }

        $user->name              = $request->input('name1');
        $user->username          = $request->input('username');
        $user->guardian_type     = $request->input('guardian_type1');
        $user->gurdian_name      = $request->input('guardian_name1');
        $user->nationality       = $request->input('nationality1');
        $user->cnic              = $request->input('cnic1');
        $user->passport          = $request->input('passport1');
        $user->ref_pk            = $request->input('ref_pk1');
        $user->email             = $request->input('email1');
        $user->phone_mobile      = $request->input('phone_mobile1');
        $user->mobile2           = $request->input('mobile1');
        $user->dateofbirth       = date('Y-m-d', strtotime(strtr($request->input('dob1'), '/', '-')));
        $user->current_address   = $request->input('current_address1');
        $user->permanent_address = $request->input('address21');
        $user->country_id        = $request->input('country_id1');
        $user->city_id           = $request->input('city_id1');
        $user->phone_office      = $request->input('phone_office1');
        $user->phone_res         = $request->input('phone_rec1');
        $user->password          = 123456;
        $user->updated_by = Auth::user()->id;

        $user->save();

        //$member = User::where('id',  $request->input('person_id'))->last();
        
        $member = members::where('registration_no', $request->input('registration_no'))->where('person_id', $user->id)->first();
        $member->registration_no = $request->input('registration_no');
        $member->reference_no = $request->input('reference_no');
        $member->nominee_name = $request->input('nominee_name');
        $member->nominee_guardian = $request->input('nominee_guardian');
        $member->nominee_cnic = $request->input('nominee_cnic');
        $member->relation_with_member = $request->input('relation_with_member');
        $member->updated_by = Auth::user()->id;
        $member->save();

        $request->session()->flash('status',1);
        $request->session()->flash('message',"Transfer detail updated");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function findname(Request $request)
    {
        if ($request->ajax()) {

            if ($request->input('registration_no') != "") {
                $booking = Payments::where('registration_no', $request->input('registration_no'))->first();
                if (!is_null($booking)) {
                    $member = members::where('registration_no', $request->input('registration_no'))->where('is_current_owner', 1)->first();
                    $data   = [
                        'person'  => $member->user,
                        'member'  => $member,
                        'payment' => $booking,
                        'plot' => $booking->paymentPlan->plotSize,
                        'error'   => 0,
                    ];
                } else {
                    $data = [
                        'error' => 1,
                    ];
                }

                if ($request->input('page') && $data['error'] == 0) {
                    $receipts = LPC::where('registration_no', $request->input('registration_no'))->where('is_finalized', 1)->get();

                    if ($receipts != null) {
                        $sum       = 0;
                        $other_sum = 0;

                        foreach ($receipts as $key => $receipt) {
                            $sum += $receipt->lpc_amount;
                            $other_sum += $receipt->lpc_other_amount;
                        }

                        $data['sum']     = $sum;
                        $data['other']   = $other_sum;
                        $data['total']   = $sum + $other_sum;
                        $data['receipt'] = $receipts;
                    }
                }

                return $data;
            }
        }
    }

    public function captureImage($id)
    {
        $transfer_id = $id;
        $TransferImages = TransferImages::where('transfer_id', $transfer_id)->get();
        return view('admin.transfers.captureImage', compact('transfer_id','TransferImages'));
    }

    public function saveImages(Request $request)
    {
        foreach ($request->input('picture') as $picture) {
            $transfer = new TransferImages;
            $transfer->transfer_id = $request->input('transfer_id');    
            $transfer->picture = $picture;    
            $transfer->created_by = Auth::user()->id;
            $transfer->save();
        
        }

        $request->session()->flash("status", 1);
        $request->session()->flash("message", "Images Updated. Please download Transfer Sheet");
        return redirect()->route('transfer.index');
    }

    public function transferSheetDownload(Request $request)
    {
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_print){
    	    abort(403);
        }
        $transfer = transfers::find($request->input('transfer_id'));
        $transfer->is_ts_dl = 1;
        $transfer->ts_dl_date = date('Y-m-d');
        $transfer->save();

        $member = members::where('registration_no', $transfer->reg_no)->where('is_current_owner',1)->first();
        $booking = Payments::where('registration_no', $transfer->reg_no)->first();
        
         $data['member'] = $member;
        $data['user'] = $member->user;
        $data['booking'] = $booking;
        $data['transfer'] = $transfer;
        $data['newMember']= members::find($transfer->new_user_id);
        $data['newUser'] = members::find($transfer->new_user_id)->user;

        return view('admin.transfers.transfer_sheet', $data);

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.transfers.transfer_sheet', $data);
        return $pdf->stream($transfer->registration_no . '-transfer.pdf');
    }

    public function downloadNdc(Request $request)
    {
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_print){
    	    abort(403);
        }
        $transfer = transfers::where('transfer_no', $request->input('transfer_no'))->first();
        // if($transfer->is_ndc_dl){
        //     $request->session()->flash('status', 0);
        //     $request->session()->flash('message', "NDC is already downloaded");
        //     return redirect()->back();
        // }

        $transfer->ndc_dl_date = date('Y-m-d');
        $transfer->is_ndc_dl = 1;
        $transfer->is_ok = 1;
        $transfer->save();
        $data['transfer'] = $transfer;

        $data['currMember'] = members::find($transfer->current_user_id);
        $data['currUser'] = members::find($transfer->current_user_id)->user;
        $data['newMember']= members::find($transfer->new_user_id);
        $data['newUser'] = members::find($transfer->new_user_id)->user;
        
        $data['cur_thumb'] = DB::table('member_fps')->where('member_id' ,$transfer->current_user_id)->first() ;
        $data['new_thumb'] = DB::table('member_fps')->where('member_id' ,$transfer->new_user_id)->first() ;

        // dd($data['currMember']->reference_no);
        
        $booking = Payments::where('registration_no', $transfer->reg_no)->first();
        $data['booking'] = $booking;

        $member = members::where('registration_no', $transfer->reg_no)->where('is_current_owner',1)->first();
        $data['member'] = $member;
        $data['user'] = $member->user;
        $data['currMember']->is_current_owner = 0;
        $data['currMember']->save();

        $data['newMember']->is_current_owner = 1;
        $data['newMember']->reference_no = $data['currMember']->reference_no;
        // $data['newMember']->membership_no = $data['currUser']->membership_no;
        $data['newMember']->save();
        
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.transfers.ndc_pdf', $data)->setPaper('legal', 'potrait');;
        return $pdf->stream($transfer->reg_no . '-NDC.pdf');
    }

    public function checkTransfer(Request $request)
    {
        if($request->ajax())
        {
            $transfer = transfers::where('transfer_no', $request->input('transfer_no'))->first();
            if($transfer)
            {
                $data['currOwner'] = members::find($transfer->current_user_id);
                $data['currOwner']['user'] = members::find($transfer->current_user_id)->user;
                $data['newOwner'] = members::find($transfer->new_user_id);
                $data['newOwner']['user'] = members::find($transfer->new_user_id)->user;

                $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
                $data['plot'] = $booking->paymentPlan->plotSize;

                return response()->json(['data' => $data, 'status' => 200]);
            }
            return response()->json(['status' => 201, 'message' => 'No record found for this transfer']);
        }
    }

    public function acknowledgement($id)
    {
       $rights = Role::getrights('transfer');
        
        if(!$rights->can_print){
    	    abort(403);
        }
        $transfer = transfers::find($id);
        $data['transfer'] = $transfer;
        $data['currOwner'] = members::find($transfer->current_user_id);
        $data['currOwner']['user'] = members::find($transfer->current_user_id)->user;
        $data['newOwner'] = members::find($transfer->new_user_id);
        $data['newOwner']['user'] = members::find($transfer->new_user_id)->user;

        $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
        $data['plan'] = $booking->paymentPlan;
        $data['booking'] = $booking;

        $data['docs'] = TransferDocs::where('registration_no', $booking->registration_no)->where('member_id', $transfer->current_user_id)->get();

        return view('admin.transfers.acknowledgement', compact('data'));
    }
    
    public function sellerdetail($id)
    {
        $transfer = transfers::find($id);
        
        $new_member = members::find($transfer->new_user_id);
        $new_user = User::find($new_member->person_id);
        
        
        return view('admin.transfers.sellerdetail', compact('transfer','new_user'));
    }



    public function affidavit($id)
    {
        $rights = Role::getrights('transfer');
        
        if(!$rights->can_print){
    	    abort(403);
        }
        $transfer = transfers::find($id);
        $data['transfer'] = $transfer;
        $data['currOwner'] = members::find($transfer->current_user_id);
        $data['currOwner']['user'] = members::find($transfer->current_user_id)->user;
        $data['newOwner'] = members::find($transfer->new_user_id);
        $data['newOwner']['user'] = members::find($transfer->new_user_id)->user;

        $booking = Payments::where('registration_no', $transfer->reg_no)->where('status',1)->first();
        $data['plan'] = $booking->paymentPlan;
        $data['booking'] = $booking;

        $data['docs'] = TransferDocs::where('registration_no', $booking->registration_no)->where('member_id', $transfer->current_user_id)->get();

        return view('admin.transfers.affidavit', compact('data'));
    }
}
