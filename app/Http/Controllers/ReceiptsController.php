<?php

namespace App\Http\Controllers;

use App\Accounting\Entry;
use App\Accounting\EntryItem;
use App\Accounting\Tag;
use App\Http\Requests\receiptRequest;
use App\InstallmentReceipts;
use App\Payments;
use App\SMSModel;
use App\Traits\SMS;
use App\bank;
use App\memberInstallments;
use App\members;
use App\pdc;
use App\receipts;
use App\sms_detail;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use NumberToWords\NumberToWords;
use App\Role;
use Illuminate\Support\Facades\Session;
use App\LPC;
use App\LpcReceipt;
use DB;

class ReceiptsController extends Controller
{
    use SMS;

    public function index()
    {
        $rights = Role::getrights('receipts');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if(!$rights->can_view){
    	    abort(403);
        } 
        $receipts = receipts::whereNull('deleted_at')->orderBy('id', 'desc')->whereNull('deleted_at')->get();
            return view('admin.receipts.index', compact('receipts','rights'));
        
    }

    public function getReceipts(Request $request)
    {
        if($request->discount_type == null)
            $receipts = receipts::whereNull('deleted_at')->orderBy('id', 'DESC')->where('receipt_no', 'NOT LIKE', 'IWR%')->where('is_active',1)->get();
        else
            $receipts = receipts::whereNull('deleted_at')->where('discount_type', $request->discount_type)->orderBy('id', 'DESC')->where('is_active',1)->where('receipt_no', 'NOT LIKE', 'IWR%')->get();


        return Datatables::of($receipts)
            ->addColumn('actions', function ($receipts) {
                $bgColor = ($receipts->is_cancelled == 0) ? "bg-info" : "bg-danger";
                
                // $html =  '<a  href="' . route('receipt.printReceipts', $receipts->id) . '" class="btn btn-xs ' . $bgColor . '"><i class="glyphicon glyphicon-edit"></i> Print</a>';

                $html = '<a href="' . route('receipt.edit', $receipts->id) . '" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>';

                $html .= '<form method="post" style="display: inline" action="' . route('receipt.destroy', $receipts->id) . '">' . csrf_field() . "<input name='_method' type='hidden' value='DELETE'> <button type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'><i class='fa fa-trash-o'></i></button></form>";

                return $html;
            })
            ->editColumn('registration_no', function ($receipts) {
                // return $receipts->registration_no;
                return "<a href='".route('booking.ledger', $receipts->registration_no)."' target='_blank'><u>".$receipts->registration_no."</u></a>";
            })

            ->editColumn('receipt_no', function ($receipts) {
                return $receipts->receipt_no;
            })
            ->editColumn('amount_paid', function ($receipts) {
                return $receipts->receipt_amount;
            })
            ->editColumn('rebate_amount', function ($receipts) {
                return $receipts->rebate_amount;
            })

            ->editColumn('discount_type', function ($receipts) {
                return $receipts->discount_type;
            })

            ->editColumn('receiving_type', function ($receipts) {
                return ($receipts->receiving_type == 1) ? "Inst" : "Other";
            })

            ->editColumn('receiving_mode', function ($receipts) {
                return $receipts->receiving_mode ;
            })

            ->editColumn('receiving_date', function ($receipts) {
                return date('d-m-Y', strtotime($receipts->receiving_date));
            })
            ->rawColumns(['actions','registration_no'])
            ->make(true);
    }

    public function printReceipts($id)
    {
        
        $rights = Role::getrights('receipts');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if(!$rights->can_print){
            abort(403);
        }
        
        $receipt = receipts::whereNull('deleted_at')->find($id);

        // create the number to words "manager" class
        $numberToWords = new NumberToWords();

        // build a new number transformer using the RFC 3066 language identifier
        $numberTransformer = $numberToWords->getNumberTransformer('en');

        $received_amount = $numberTransformer->toWords($receipt->received_amount); // outputs "five thousand one hundred twenty"

        $member = members::where('registration_no', $receipt->registration_no)->first();
        //dd($receipt);
        return view('admin.receipts.print', compact('receipt', 'member', 'received_amount'));

    }

    public function cancel(Request $request)
    {
        $receipt = receipts::whereNull('deleted_at')->find($request->input('receipt_id'));

        if ($receipt->is_cancelled == 1) {
            return response()->json(['result' => false, 'message' => 'This receipt is already cancelled'], 200);
        }

        $receipt->is_cancelled   = 1;
        $receipt->cancel_by      = Auth::user()->id;
        $receipt->cancel_date    = date('Y-m-d');
        $receipt->cancel_remarks = $request->input('remarks');
        $receipt->save();

        $installment_receipt = InstallmentReceipts::where('receipt_id', $receipt->id)->get();
        foreach ($installment_receipt as $ireceipt) {
            $ireceipt->delete();
        }

        $member_installments = memberInstallments::where('receipt_no', $receipt->receipt_no)->get();
        foreach ($member_installments as $mInstallments) {
            $mInstallments->receipt_no      = null;
            $mInstallments->receipt_amount  = null;
            $mInstallments->is_paid         = 0;
            $mInstallments->amount_received = 0;
            $mInstallments->payment_date    = null;
            $mInstallments->payment_mode    = null;
            $mInstallments->os_amount       = null;

            $mInstallments->save();
        }
        return response()->json(['result' => true, 'message' => 'Receipt Cancelled successfully'], 200);
    }

    public function getCancelDetail(Request $request)
    {
        $receipt = receipts::whereNull('deleted_at')->find($request->input('receipt_id'));
        return response()->json(['result' => $receipt], 200);
    }

    public function create()
    {
        $rights = Role::getrights('receipts');
        if(!Session::get('objects'))
        Session::put('objects', Role::getMenuItems());
        if(!$rights->can_create){
            abort(403);
        }
        $banks = bank::all();

        return view('admin.receipts.create', compact('banks'));
    }

 
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'receipt_no' => Rule::unique('receipts')->where(function ($query) use($request) {
                    return $query->whereNull('deleted_at');
            }),
            'receiving_mode' => 'required',
            'registration_no' => 'required'
        ])->validate();


      //   dd($pre_instal_receipts);
        // die;
        $a = [];
        $db_os_amount = [];
        $i = 1;
        
        DB::beginTransaction();

        try {
            if ($request->receiving_type == 3) 
                $this->saveBoth($request);
            elseif ($request->receiving_type == 4) {
                $i            = 1;
                $fstArr       = [];
    
                $amount_received = ($request->input('receiving_amount') == 0) ? $request->input('rebate_amount') : $request->input('receiving_amount');
    
                $input = $request->input();
                $input['created_by']     = Auth::user()->id;
                $input['receiving_mode'] = $request->input('receiving_mode');
                $input['receipt_remarks'] = "Plot Preference";
                $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
    
                // $input
                $receiptAdded = receipts::create($input);
                
                foreach ($request->input('fst_id') as $key => $value) {
    
                    if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                        $is_paid   = 1;
                        $os_amount = 0;
                    } else {
                        //$paid_amount = $paid_amount[$key];
                        $os_amount = abs($amount_received[$key]);
                        $is_paid   = 0;
                    }
    
                    $bf = \App\booking_featureset::find($value);
    
                    $bf->is_paid           = $is_paid;
                    $db_os_amount[$bf->id] = $bf->os_amount;
    
                    $bf->os_amount = $os_amount;
                    
                    if ($is_paid == 1) {
                        $bf->amount_received += ($request->input('installment_amount')[$key] != 0) ? $request->input('installment_amount')[$key] : $request->rebate_amount;
                    } else {
                        $bf->amount_received += $request->input('installment_amount')[$key] - $os_amount;
                    }
    
                    $bf->save();
    
                    $i++;
                    
                    $bfReceipt = new \App\FstReceipt();
                    $bfReceipt->featureset_id = $bf->id;
                    $bfReceipt->receipt_id = $receiptAdded->id;
                    $bfReceipt->save();
                }
    
                //$receiptAdded->lpcReceipts()->sync($lpcArr);
            }
            elseif ($request->receiving_type == 5) {
                $i            = 1;
                $lpcArr       = [];
    
                $amount_received = ($request->input('receiving_amount') == 0) ? $request->input('rebate_amount') : $request->input('receiving_amount');
    
                $input = $request->input();
                $input['created_by']     = Auth::user()->id;
                $input['receiving_mode'] = $request->input('receiving_mode');
                $input['receipt_remarks'] = "LPC";
                $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
    
                // $input
                $receiptAdded = receipts::create($input);
                
                foreach ($request->input('lpc_id') as $key => $value) {
    
                    if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                        $is_paid   = 1;
                        $os_amount = 0;
                    } else {
                        //$paid_amount = $paid_amount[$key];
                        $os_amount = abs($amount_received[$key]);
                        $is_paid   = 0;
                    }
    
                    // $pre_inst = memberInstallments::where('registration_no' , 'cc-gs1-1205')->where('os_amount','!=',0)->limit(1)->orderBy('id','desc')->first();
                    $lpc = LPC::find($value);
    
                    $lpc->is_paid           = $is_paid;
                    $db_os_amount[$lpc->id] = $lpc->os_amount;
    
                    $lpc->os_amount = $os_amount;
                    $waive = ($lpc->inst_waive) ? $lpc->inst_waive : 0;
                    // $lpc->total_received = $total_received;
                    if ($is_paid == 1) {
                        $lpc->amount_received += ($request->input('installment_amount')[$key] != 0) ? $request->input('installment_amount')[$key] - $waive : $request->rebate_amount;
                    } else {
                        $lpc->amount_received += $request->input('installment_amount')[$key] - $waive - $os_amount;
                    }
    
                    // dd($lpc);
    
                    // $lpc->receipt_no = $request->input('receipt_no');
                    $lpc->remarks    = $request->input('remarks')[$key];
    
    
                    // dd($lpc);
                    $lpc->save();
    
                    $lpcArr[] = $lpc->id;
                    // if($key == 0)
                    // dd($installment);
                    $i++;
                    //Attaching late payment charges with receipts (Many to many relationship)
                    $lpcReceipt = new \App\LpcReceipt();
                    $lpcReceipt->lpc_id = $lpc->id;
                    $lpcReceipt->receipt_id = $receiptAdded->id;
                    $lpcReceipt->save();
                }
    
                //$receiptAdded->lpcReceipts()->sync($lpcArr);
            }
            elseif ($request->receiving_type == 6) {
                $amount_received = ($request->input('receiving_amount') == 0) ? $request->input('rebate_amount') : $request->input('receiving_amount');
    
                $input = $request->input();
                $input['created_by']     = Auth::user()->id;
                $input['receiving_mode'] = $request->input('receiving_mode');
                $input['receipt_remarks'] = "Transfer";
                $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
    
                $receiptAdded = receipts::create($input);
                
                $transfer = \App\transfers::find($request->transfer_id);
                $transfer->is_paid = 1;
                $transfer->save();
                
                DB::insert('insert into transfer_receipts(transfer_id, receipt_id) values('. $request->transfer_id .','. $receiptAdded->id .')');
            }
            else
            {
                $amount_received = ($request->input('receiving_amount') == 0) ? $request->input('rebate_amount') : $request->input('receiving_amount');
        
                foreach ($request->input('member_installment_id') as $key => $value) {
        
                    if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                        $is_paid   = 1;
                        $os_amount = 0;
                    } else {
                        $os_amount = abs($amount_received[$key]);
                        $is_paid   = 0;
                    }
        
                    // $pre_inst = memberInstallments::where('registration_no' , 'cc-gs1-1205')->where('os_amount','!=',0)->limit(1)->orderBy('id','desc')->first();
        
                    $installment            = memberInstallments::find($value);
        
                    $installment->is_paid   = $is_paid;
                    $db_os_amount[$installment->id] = $installment->os_amount;
        
                    $installment->os_amount = $os_amount;
        
        
                    // $installment->total_received = $total_received;
                    if ($is_paid == 1) {
                        $installment->amount_received = ($request->input('installment_amount')[$key] != 0) ?  $request->input('installment_amount')[$key] : $request->rebate_amount;
                    } else {
                        $installment->amount_received = $request->input('due_amount')[$key] - $os_amount;
                    }
        
                    $installment->receipt_no = $request->input('receipt_no');
                    $installment->remarks    = $request->input('remarks')[$key];
        
                    if ($i == 1) {
                        $installment->receipt_amount = ($request->received_amount) ? $request->received_amount : 0;
        
                        $installment->payment_date = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
        
                        $installment->payment_mode = $request->input('receiving_mode');
                        if($request->rebate_amount)
                        {
                            $installment->rebate_amount = $request->rebate_amount;
                        }
                    } else {
                        $installment->receipt_amount = null;
                    }
                    $installment->save();
                    // if($key == 0)
                        // dd($installment);
                    $i++;
                }
        
                $input = $request->input();
        
                // incase user select option 'both' then he will write amount of inst and other payment 
                if ($input['received_amount'] == null) {
                    $input['received_amount'] = $input['inst_amount'] + $input['other_amount'];
                }
        
                $input['created_by']     = Auth::user()->id;
                $input['receiving_mode'] = $request->input('receiving_mode');
                $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
        
                $booking  = Payments::where('registration_no', $request->input('registration_no'))->first();
                $receipts = receipts::whereNull('deleted_at')->where('registration_no', $request->input('registration_no'))->get();
        
                $add = 0;
                if ($receipts != null) {
                    foreach ($receipts as $key => $receipt) {
                        $add = ($receipt->rebate_amount != 0) ? 0 : 1;
                    }
                }
        
                if ($booking->rebate_amount != 0 && $add == 1) {
                    $input['rebate_amount'] = $booking->rebate_amount;
                }
        
                if($request->rebate_amount)
                {
                    $input['rebate_amount'] = $request->rebate_amount;
                    $input['discount_type'] = $request->discount_type;
                }
                
                $receiptAdded = receipts::create($input);
                $receiptInput = [];
                $j = 1;
        
                foreach ($request->input('member_installment_id') as $key => $value) {
        
                    if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                        $is_paid   = 1;
                        $os_amount = 0;
                    } else {
                        $os_amount = abs($amount_received[$key]);
                        $is_paid   = 0;
                    }
        
                    $installment = memberInstallments::find($value);
        
                    $pre_instal_receipts = InstallmentReceipts::where('registration_no', $request->input('registration_no'))->where('os_amount', '!=', 0)->where('installment_id', $value)->get();
        
                    // dd($pre_instal_receipts);
        
                    if (count($pre_instal_receipts) > 0) {
                        foreach($pre_instal_receipts as $pre_receipts)
                        {
                            // $os_amount                  = $pre_receipts->os_amount;
                            $pre_receipts->os_amount = 0;
                            $pre_receipts->save();
                        }
                    }
        
                    // echo $is_paid;
                    $receiptInput['due_amount']     = $installment->due_amount;
                    $receiptInput['receipt_id']     = $receiptAdded->id;
                    $receiptInput['installment_id'] = $installment->id;
        
                    if ($key == 0) {
                        $receiptInput['receipt_amount'] = ($request->received_amount) ? $request->received_amount : 0;
                        $receiptInput['receipt_date']   = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
                        $receiptInput['payment_mode']   = $request->receiving_mode;
                        if($request->rebate_amount)
                        {
                            $receiptInput['rebate_amount'] = $request->rebate_amount;
                        }
                    }
                    // dd($os_amount);
                    if($is_paid == 1){
                        if(count($pre_instal_receipts) > 0 && $os_amount  == 0)
                            $receiptInput['received_amount'] = $db_os_amount[$installment->id];
                        else
                            $receiptInput['received_amount'] = $request->input('due_amount')[$key];
                    }
                    else{
                        $receiptInput['os_amount'] = abs($amount_received[$key]);
                        $receiptInput['received_amount'] = $request->input('due_amount')[$key] - $os_amount;
                    }
        
                    // dd($receiptInput);
                    // if($key == 1)
                    //     dd($receiptInput);
                    
                    $j++;

                    $receiptInput['receipt_no']   = $request->receipt_no;
                    $receiptInput['registration_no']   = $request->registration_no;
                    $receiptInput['rebate_amount']  = $installment->rebate_amount;
                    $receiptInput['payment_desc']   = $installment->payment_desc;
                    $receiptInput['installment_no'] = $installment->installment_no;
                    $receiptInput['due_date']       = $installment->installment_date;
        
                    $inst_receipts_added = InstallmentReceipts::create($receiptInput);
        
                    $receiptInput = [];
                }
            
                /*$pay_type = ($request->receiving_mode == 'cash') ? 68 : 67;
                Entry::saveEntry2(41,$request->receipt_no,23,24,$pay_type,$request->input('receiving_date'),$request->received_amount,$request->receipt_remarks);*/
        
                $sms_template = "Dear Customer,
        
                Thank you for depositing payment. Your payment has been updated against registration number: " . $request->input('registration_no') . "
                
                Regards,
                Capital City, PMC
                0300-0604762
                0300-0605762
                0423-5761650-2";
        
                $person = members::where('registration_no', $request->input('registration_no'))->first()->user;
        
                $phone  = str_replace("-", "", $person->phone_mobile);

                //$mobile = substr($phone, 0, 1) == 0 ? '92' . substr($phone, 1) : $phone;
                
                //dd($mobile);
        
                $res = $this->SendSMS($sms_template, $phone);

                /*$status = 0;
                if(strpos($res,'Message Sent Successfully',0) !== false)
                {*/
                    $status = 1;
                    \Log::info("receipt sms at " . $phone);
                /*}*/
                $sms = SMSModel::create([
                    'message'    => $sms_template,
                    'event'      => 'Payment Deposit',
                    'sent_to'      => $phone,
                    'sent'      => $status,
                    'created_by' => Auth::user()->id,
                ]);
        
        
                sms_detail::create([
                    'sent_to' => $request->input('registration_no'),
                    'sms_id' => $sms->id,
                ]);
            }
            
            DB::commit();
            $message = "New receipt added";
            Session::flash('status', 'success');
            return redirect()->route('receipt.index')->with(['message' => $message]);
        }
        catch (\Exception $e) {
            DB::rollBack();
            $message = "Some error occured! please try again later.";
            \Log::info($e->getMessage());
            return redirect()->back()->with(['status' => 0, 'message' => $message]);
        }
    }
    
    
    public function saveBoth(Request $request)
    {
        $input = $request->input();

        $input['received_amount'] = $input['inst_amount'] + $input['other_amount'];

        $input['created_by']     = Auth::user()->id;
        $input['receiving_mode'] = $request->input('receiving_mode');
        $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));

        /*$booking  = Payments::where('registration_no', $request->input('registration_no'))->first();*/
        $receipts = receipts::where('registration_no', $request->input('registration_no'))->get();

        /*$add = 0;
        if ($receipts != null) {
            foreach ($receipts as $key => $receipt) {
                $add = ($receipt->rebate_amount != 0) ? 0 : 1;
            }
        }

        if ($booking->rebate_amount != 0 && $add == 1) {
            $input['rebate_amount'] = $booking->rebate_amount;
        }*/

        $input['rebate_amount'] = $request->rebate_amount;
        $input['discount_type'] = $request->discount_type;
        $input['clear_date'] = date('Y-m-d',strtotime($request->clear_date));
        
        $receiptAdded = receipts::create($input);
        
        $a = [];
        $db_os_amount2 = [];
        $i = 1;
        
        $amount_received = $request->input('receiving_amount2');

        foreach ($request->input('member_installment_id2') as $key => $value) {

            if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($amount_received[$key]);
                $is_paid   = 0;
            }

            $installment            = memberInstallments::find($value);

            $installment->is_paid   = $is_paid;
            $db_os_amount2[$installment->id] = $installment->os_amount;

            $installment->os_amount = $os_amount;

            if ($is_paid == 1) {
                $installment->amount_received = ($request->input('installment_amount2')[$key] != 0) ?  $request->input('installment_amount2')[$key] : $request->rebate_amount;
            } else {
                $installment->amount_received = $request->input('due_amount2')[$key] - $os_amount;
            }

            $installment->receipt_no = $request->input('receipt_no');
            $installment->remarks    = $request->input('remarks2')[$key];

            if ($i == 1) {
                $amount = ($request->received_amount == 0) ? $request->rebate_amount : $request->received_amount;
                $installment->receipt_amount = ($amount == null) ? $request->input('other_amount') : $amount;

                $installment->payment_date = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));

                $installment->payment_mode = $request->input('receiving_mode');
            } else {
                $installment->receipt_amount = null;
            }
            $installment->save();
            $i++;
        }
        
        $receiptInput = [];
        $j = 1;

        $amount_received = $request->input('receiving_amount2');

        foreach ($request->input('member_installment_id2') as $key => $value) {

            if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($amount_received[$key]);
                $is_paid   = 0;
            }

            $installment = memberInstallments::find($value);

            $pre_instal_receipts = InstallmentReceipts::where('registration_no', $request->input('registration_no'))->where('os_amount', '!=', 0)->where('installment_id', $value)->get();

            // dd($pre_instal_receipts);

            if (count($pre_instal_receipts) > 0) {
                foreach($pre_instal_receipts as $pre_receipts)
                {
                    $pre_receipts->os_amount = 0;
                    $pre_receipts->save();
                }
            }

            // echo $is_paid;
            $receiptInput['due_amount']     = $installment->due_amount;
            $receiptInput['receipt_id']     = $receiptAdded->id;
            $receiptInput['installment_id'] = $installment->id;

            if ($key == 0) {
                $receiptInput['receipt_amount'] = $request->other_amount;
                $receiptInput['receipt_date']   = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
                $receiptInput['payment_mode']   = $request->receiving_mode;
                // $receiptInput['receipt_amount'] = $installment->receipt_amount;
            }
            // dd($os_amount);
            if($is_paid == 1){
                if(count($pre_instal_receipts) > 0 && $os_amount  == 0)
                    $receiptInput['received_amount'] = $db_os_amount2[$installment->id];
                else
                    $receiptInput['received_amount'] = $request->input('due_amount2')[$key];
            }
            else{
                $receiptInput['os_amount'] = abs($amount_received[$key]);
                $receiptInput['received_amount'] = $request->input('due_amount2')[$key] - $os_amount;
            }
            
            $j++;

            $receiptInput['receipt_no']   = $request->receipt_no;
            $receiptInput['registration_no']   = $request->registration_no;
            $receiptInput['rebate_amount']  = $installment->rebate_amount;
            $receiptInput['payment_desc']   = $installment->payment_desc;
            $receiptInput['installment_no'] = $installment->installment_no;
            $receiptInput['due_date']       = $installment->installment_date;
            //$receiptInput['rebate_amount'] = $request->rebate_amount;

            $inst_receipts_added = InstallmentReceipts::create($receiptInput);

            $receiptInput = [];
        }
        
        $a = [];
        $db_os_amount = [];
        $i = 1;
        
        $amount_received = $request->input('receiving_amount');

        foreach ($request->input('member_installment_id') as $key => $value) {

            if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($amount_received[$key]);
                $is_paid   = 0;
            }

            $installment            = memberInstallments::find($value);

            $installment->is_paid   = $is_paid;
            $db_os_amount[$installment->id] = $installment->os_amount;

            $installment->os_amount = $os_amount;

            if ($is_paid == 1) {
                $installment->amount_received = ($request->input('installment_amount')[$key] != 0) ?  $request->input('installment_amount')[$key] : $request->rebate_amount;
            } else {
                $installment->amount_received = $request->input('due_amount')[$key] - $os_amount;
            }

            $installment->receipt_no = $request->input('receipt_no');
            $installment->remarks    = $request->input('remarks')[$key];

            if ($i == 1) {
                $amount = ($request->received_amount == 0) ? $request->rebate_amount : $request->received_amount;
                $installment->receipt_amount = ($amount == null) ? $request->input('inst_amount') : $amount;

                $installment->payment_date = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));

                $installment->payment_mode = $request->input('receiving_mode');
            } else {
                $installment->receipt_amount = null;
            }
            $installment->save();
            $i++;
        }
        
        $receiptInput = [];
        $j = 1;

        foreach ($request->input('member_installment_id') as $key => $value) {

            if ($amount_received[$key] >= 0 || $amount_received[$key] == -4 || $amount_received[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($amount_received[$key]);
                $is_paid   = 0;
            }

            $installment = memberInstallments::find($value);

            $pre_instal_receipts = InstallmentReceipts::where('registration_no', $request->input('registration_no'))->where('os_amount', '!=', 0)->where('installment_id', $value)->get();

            // dd($pre_instal_receipts);

            if (count($pre_instal_receipts) > 0) {
                foreach($pre_instal_receipts as $pre_receipts)
                {
                    // $os_amount                  = $pre_receipts->os_amount;
                    $pre_receipts->os_amount = 0;
                    $pre_receipts->save();
                }
            }

            // echo $is_paid;
            $receiptInput['due_amount']     = $installment->due_amount;
            $receiptInput['receipt_id']     = $receiptAdded->id;
            $receiptInput['installment_id'] = $installment->id;

            if ($key == 0) {
                $receiptInput['receipt_amount'] = $request->inst_amount;
                $receiptInput['receipt_date']   = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
                $receiptInput['payment_mode']   = $request->receiving_mode;
                // $receiptInput['receipt_amount'] = $installment->receipt_amount;
            }
            // dd($os_amount);
            if($is_paid == 1){
                if(count($pre_instal_receipts) > 0 && $os_amount  == 0)
                    $receiptInput['received_amount'] = $db_os_amount[$installment->id];
                else
                    $receiptInput['received_amount'] = $request->input('due_amount')[$key];
            }
            else{
                $receiptInput['os_amount'] = abs($amount_received[$key]);
                $receiptInput['received_amount'] = $request->input('due_amount')[$key] - $os_amount;
            }
            
            $j++;

            $receiptInput['receipt_no']   = $request->receipt_no;
            $receiptInput['registration_no']   = $request->registration_no;
            $receiptInput['rebate_amount']  = $installment->rebate_amount;
            $receiptInput['payment_desc']   = $installment->payment_desc;
            $receiptInput['installment_no'] = $installment->installment_no;
            $receiptInput['due_date']       = $installment->installment_date;
            //$receiptInput['rebate_amount'] = $request->rebate_amount;

            $inst_receipts_added = InstallmentReceipts::create($receiptInput);

            $receiptInput = [];
        }
    }

    public function destroy($id)
    {
        $receipts = receipts::whereNull('deleted_at')->find($id);
        $installment_no = null;
        $firstInst = null;
        $receiptData = [];
        
        $installment_receipt = InstallmentReceipts::where('receipt_no', $receipts->receipt_no)->get();

        $i = 0;
        foreach ($installment_receipt as $key => $ireceipt) {

            if($key == 0){
                $firstInst = $ireceipt;
            }

            $receiptData[] = $ireceipt;

            $lastRecipt = $ireceipt;
            $ireceipt->delete();
            
            $i++;
        }  

        //         //This receipt is paid against multiple installments
        // if($lastRecipt->installment_no != $firstInst->installment_no)
        // {
               
        // }
        $member_installments = memberInstallments::where('receipt_no', $receipts->receipt_no)->get();
        foreach ($member_installments as $key2 => $mInstallments) 
        {
            // $amountReceived = $mInstallments->amount_received;
            $mInstallments->receipt_no      = null;
            $mInstallments->receipt_amount  = 0;
            $mInstallments->is_paid         = 0;
            $mInstallments->amount_received = 0;
            $mInstallments->payment_date    = null;
            $mInstallments->payment_mode    = null;
            $mInstallments->os_amount       = 0;
            $mInstallments->save();
        }



        $memberLastInstallment = memberInstallments::find($firstInst->installment_id);
        if($memberLastInstallment)
        {
            if($firstInst->os_amount != 0 && $firstInst->os_amount != null)
            {
                $os_amount = $firstInst->os_amount + $firstInst->received_amount;
            }else{
                $os_amount = $firstInst->received_amount;
            }


            $memberLastInstallment->amount_received = $firstInst->due_amount - $os_amount;
            $memberLastInstallment->os_amount = $os_amount;
            $memberLastInstallment->receipt_no = $firstInst->receipt_no;
            $memberLastInstallment->receipt_amount = $firstInst->due_amount - $os_amount;
            $memberLastInstallment->payment_mode = $firstInst->payment_mode;
            $memberLastInstallment->payment_date = $firstInst->payment_date;
            $memberLastInstallment->os_amount = $os_amount;
            $memberLastInstallment->is_paid = 0;
            $memberLastInstallment->save(); 
        }

        $lastInstallmentReceipt = InstallmentReceipts::where('registration_no', $receipts->registration_no)->where('installment_no', $firstInst->installment_no)->limit(1)->orderBy('id','DESC')->first();

        if($lastInstallmentReceipt)
        {
            $lastInstallmentReceipt->os_amount = $os_amount;
            $lastInstallmentReceipt->save();
        }


        //Check if PDC created for this receipt or not
        if (!is_null($receipts->pdc_ack)) {
            $pdc = pdc::find($receipts->pdc_id);
            if ($pdc) {
                foreach ($pdc->installments as $installments) {
                    $installments->delete();
                }
                $pdc->delete();
            }
        }

        $receipts->delete();
        return redirect()->route('receipt.index')->with(['message' => 'Receipt Deleted']);
    }

    public function edit($id)
    {
$rights = Role::getrights('receipts');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_edit){
    abort(403);
}

        $receipts = receipts::whereNull('deleted_at')->find($id);
        // dd($receipts->user());
        $banks        = bank::all();
        $installments = memberInstallments::where('receipt_no', $receipts->receipt_no)->where('is_active_user', 1)->where('registration_no', $receipts->registration_no)->get();
        return view('admin.receipts.edit', compact('receipts', 'installments', 'banks'));
    }





    public function update($id, Request $request)
    {
        $i = 1;
        $j = 1;

        
        $input                   = $request->input();
        $input['updated_by']     = Auth::user()->id;
        $input['receiving_mode'] = $request->input('receiving_mode');
        // $input['receiving_date'] = date('Y-m-d');

        $a = [];
        $db_os_amount = [];
        $i = 1;
        foreach ($request->input('member_installment_id') as $key => $value) {

            if ($request->input('receiving_amount')[$key] >= 0 || $request->input('receiving_amount')[$key] == -4 || $request->input('receiving_amount')[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($request->input('receiving_amount')[$key]);
                $is_paid   = 0;
            }
            // $pre_inst = memberInstallments::where('registration_no' , 'cc-gs1-1205')->where('os_amount','!=',0)->limit(1)->orderBy('id','desc')->first();

            $installment            = memberInstallments::find($value);

            $installment->is_paid   = $is_paid;
            $db_os_amount[$installment->id] = $installment->os_amount;

            $installment->os_amount = $os_amount;


            // $installment->total_received = $total_received;
            if ($is_paid == 1) {
                $installment->amount_received = $request->input('installment_amount')[$key];
            } else {
                $installment->amount_received = $request->input('due_amount')[$key] - $os_amount;
            }

            $installment->receipt_no = $request->input('receipt_no');
            $installment->remarks    = $request->input('remarks')[$key];

            if ($i == 1) {
                $installment->receipt_amount = ($request->input('received_amount') == null) ? ($request->input('inst_amount') + $request->input('other_amount')) : $request->input('received_amount');

                $installment->payment_date = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));

                $installment->payment_mode = $request->input('receiving_mode');
            } else {
                $installment->receipt_amount = null;
            }
            $installment->save();
            // if($key == 0)
                // dd($installment);
            $i++;
        }

        // incase user select option 'both' then he will write amount of inst and other payment 
        if ($input['received_amount'] == null) {
            $input['received_amount'] = $input['inst_amount'] + $input['other_amount'];
        }

        $input['receiving_mode'] = $request->input('receiving_mode');
        $input['receiving_date'] = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));

        $booking  = Payments::where('registration_no', $request->input('registration_no'))->first();
        $receipts = receipts::whereNull('deleted_at')->where('registration_no', $request->input('registration_no'))->get();

        $add = 0;
        if ($receipts != null) {
            foreach ($receipts as $key => $receipt) {
                $add = ($receipt->rebate_amount != 0) ? 0 : 1;
            }
        }

        if ($booking->rebate_amount != 0 && $add == 1) {
            $input['rebate_amount'] = $booking->rebate_amount;
        }
            
        $receiptAdded = receipts::whereNull('deleted_at')->find($id);
        $receiptInput = [];
        $j = 1;

        foreach ($request->input('member_installment_id') as $key => $value) {

            if ($request->input('receiving_amount')[$key] >= 0 || $request->input('receiving_amount')[$key] == -4 || $request->input('receiving_amount')[$key] == -2) {
                $is_paid   = 1;
                $os_amount = 0;
            } else {
                $os_amount = abs($request->input('receiving_amount')[$key]);
                $is_paid   = 0;
            }

            $installment = memberInstallments::find($value);

            $inst_receipt = InstallmentReceipts::where('receipt_no', $request->receipt_no)->get();
            foreach($inst_receipt as $rec)
            {
                $rec->forceDelete();
            }

            // echo $is_paid;
            $receiptInput['due_amount']     = $installment->due_amount;
            $receiptInput['receipt_id']     = $receiptAdded->id;
            $receiptInput['installment_id'] = $installment->id;

            if ($key == 0) {
                $receiptInput['receipt_amount'] = $request->received_amount;
                $receiptInput['receipt_date']   = date('Y-m-d', strtotime(strtr($request->input('receiving_date'), '/', '-')));
                $receiptInput['payment_mode']   = $request->receiving_mode;
                // $receiptInput['receipt_amount'] = $installment->receipt_amount;
            }
            // dd($os_amount);




            $pre_instal_receipts = memberInstallments::where('registration_no' , 'cc-gs1-1205')->where('os_amount','!=',0)->limit(1)->orderBy('id','desc')->get();

            if($is_paid == 1){
                if(count($pre_instal_receipts) > 0 && $os_amount  == 0)
                    $receiptInput['received_amount'] = $db_os_amount[$installment->id];
                else
                    $receiptInput['received_amount'] = $request->input('due_amount')[$key];
            }
            else{
                $receiptInput['os_amount'] = abs($request->input('receiving_amount')[$key]);
                $receiptInput['received_amount'] = $request->input('due_amount')[$key] - $os_amount;
            }

            // dd($receiptInput);
            // if($key == 1)
            //     dd($receiptInput);
            
            $j++;


            // // if ($is_paid == 1) {
            // //     $receiptInput['received_amount'] = $request->received_amount;
            // // } else {

            // $receiptInput['os_amount']       = abs($request->input('receiving_amount')[$key]);
            // $receiptInput['received_amount'] = $request->received_amount;

            // }

            // $installment->amount_received += $receiptInput['received_amount'];
            // $installment->save();

            $receiptInput['receipt_no']   = $request->receipt_no;
            $receiptInput['registration_no']   = $request->registration_no;
            $receiptInput['rebate_amount']  = $installment->rebate_amount;
            $receiptInput['payment_desc']   = $installment->payment_desc;
            $receiptInput['installment_no'] = $installment->installment_no;
            $receiptInput['due_date']       = $installment->installment_date;

            $inst_receipts_added = InstallmentReceipts::create($receiptInput);

            $receiptInput = [];
        }

        $receiptAdded->update($input);

        $message = "Receipt updated";
        return redirect()->route('receipt.edit', $id)->with(['message' => $message]);

    }
}
