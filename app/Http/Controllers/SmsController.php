<?php

namespace App\Http\Controllers;

use App\agency;
use App\assignFiles;
use App\InstallmentReceipts;
use App\memberInstallments;
use App\members;
use App\Payments;
use App\plotSize;
use App\principal_officer;
use App\receipts;
use App\SMSModel;
use App\SMSTemplates;
use App\sms_detail;
use App\Traits\SMS;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Role;
use App\LPC;
use DB;
use Illuminate\Support\Facades\Session;

class SmsController extends Controller
{

    use SMS;

    public function testsms()
    {
        
        $response = $this->SendSMS("Good bye", '03014012423');
        /*$response = Curl::to('http://my.ezeee.pk/sendsms_url.html')
                 ->withData( 
                 	array( 
                		
                 		'Username' => '03544012423',
                 		'Password' => '@03544012423',
                 		'From' => 'ROYALENCLAV',
                 		'To' => '03014012423',
                 		'Message' => "How are you",
                 	) 
                 )
                 ->get();*/
         echo $response;
    }
    
    public function index()
    {
        $rights = Role::getrights('sms');
        if(!Session::get('objects'))
        Session::put('objects', Role::getMenuItems());
        if(!$rights->can_view){
            abort(403);
        }
        return view('admin.sms.index',compact('rights'));
    }

    public function getSMS()
    {
        $sms  = SMSModel::orderBy('id', 'DESC')->get();
        
        ini_set('memory_limit', '1024M');
        $data = Datatables::of($sms)
            ->addColumn('check', function ($sms) {
                return '<a class="btn btn-primary check" data-toggle="modal" href="#sentTo" data-smsId="' . $sms->id . '">Check</a>';
            })
            ->editColumn('event', function ($sms) {
                return $sms->event;
            })
            ->editColumn('sending_date', function ($sms) {
                return ($sms->sending_date) ? date('d-m-Y H:i', strtotime($sms->sending_date)) : "";
            })
            ->editColumn('sent', function ($sms) {
                
                return ($sms->sent) ? "<span class='label label-lg label-success'>Sent</span>" : "<span class='label label-warning'>Pending</span>";
            })

            ->rawColumns(['check', 'sent'])
            ->make(true);
            
           
        return $data;
    }

    public function create()
    {
        $rights = Role::getrights('sms');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());

if(!$rights->can_create){
abort(403);
}

        // app('debugbar')->disable();

        $plotSize  = plotSize::all();
        $agency    = agency::all();
        $employees = User::Employees();
        $members   = Payments::Active();

        $principal_officer = principal_officer::all();

        $dueInstallments = memberInstallments::where('installment_date', '<=', date('Y-m-d'))->where('is_paid', 0)->where('is_active_user', 1)->get();

         $duePersons = [];

        // foreach ($dueInstallments as $key => $installment) {
        //     $booking = Payments::with('memberData')->where('registration_no', $installment->registration_no)->where('status', 1)->first();
        //     // dd($booking);
        //     if ($booking) {

        //         $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

        //         if ($memberInfo) {
        //             $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();
        //             if ($is_member_active) {
        //                 $duePersons[] = $is_member_active;
        //             }
        //         }
        //     }
        // }
        // dd($duePersons);
        // $overDueInstallments   = memberInstallments::where('installment_date','<=',Carbon::now()->addMonths(2))->where('is_paid',0)->where('is_active_user',1)->get();

        $overDuePersons = [];

        // foreach ($overDueInstallments as $key => $installment2) {
        //     $booking2 = Payments::where('registration_no', $installment2->registration_no)->where('status', 1)->first();
        //     if($booking2){

        //         $memberInfo2 = members::where('registration_no', $booking2->registration_no)->where('is_current_owner',1)->first();

        //         if($memberInfo2)
        //         {
        //             $is_member_active2 = User::where('id',$memberInfo2->person_id)->where('status',1)->first();
        //             if($is_member_active2)
        //             {
        //                 $overDuePersons[] = $is_member_active2;
        //             }
        //         }
        //     }
        // }
        // dd($overDuePersons);
        // $duePersons = count($duePersons);
        // dd($duePersons);
        // $overDuePersons = count($overDuePersons);

        $sms_templates = SMSTemplates::all();

        return view('admin.sms.create', compact('plotSize', 'agency', 'employees', 'members', 'duePersons', 'overDuePersons', 'principal_officer'));
    }

    public function show($id)
    {
        return redirect()->route('sms.index')->with(['message' => 'SMS are scheduled']);
    }

    public function store(Request $request)
    {
        $members                  = $request->input('members');
        $plotTypeCommercial       = $request->input('plotTypeCommercial');
        $plotTypeHousing          = $request->input('plotTypeHousing');
        $members                  = $request->input('members');
        $custom_mobile_number     = $request->input('custom_mobile_number');
        $plotSize                 = $request->input('plotSize');
        $agency                   = $request->input('agency');
        $principalOfficers        = $request->input('principalOfficers');
        $allMembers               = $request->input('allMembers');
        $allEmployees             = $request->input('allEmployees');
        $allLPCs                  = $request->input('allLPCs');
        $message                  = $request->input('message');
        $dueInstallmentsInput     = $request->input('dueInstallments');
        $overDueInstallmentsInput = $request->input('overDueInstallments');
        $users_phone              = [];
        $sms_detail               = [];
        $users_name             = [];
        
        // if ($plotTypeHousing) {
        //     $user = User::Members();
        //     foreach ($user as $key => $u) {
        //         if ($u->phone_mobile != null && !in_array($u->phone_mobile, $users_phone)) {

        //             $member = members::join('bookings', 'bookings.registration_no', '=', 'members.registration_no')
        //                 ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
        //                 ->where('members.person_id', $u->id)
        //                 ->where('members.is_current_owner', 1)
        //                 ->where('payment_schedule.plot_nature', 'Housing')
        //                 ->first();

        //             if ($member) {
        //                 if (strpos($u->phone_mobile, '-')) {
        //                     $users_phone[] = str_replace("-", "", $u->phone_mobile);
        //                 } else {
        //                     $users_phone[] = $u->phone_mobile;
        //                 }
        //                 $sms_detail['sent_to'][] = $member->registration_no;
        //                 $sms_detail['message'][] = $request->message;
        //             }
        //         }
        //     }
        // }

        if ($plotTypeCommercial) {

            if (!$request->target_date) {
                $request->session()->flash('message', "Please choose target date");
                $request->session()->flash('status', 0);
                return redirect()->route('sms.create');
            }

            $ploType = ($plotTypeHousing == Null) ? "Commerical" : "Housing";

            //Getting only commerical plots
            $assignFiles = assignFiles::where('plot_type', 'Commercial')->pluck('registration_no')->toArray();
            // abort(200, "Working on it");
            $dueInstallments = memberInstallments::where('installment_date', '<=', $request->target_date)->where('is_paid', 0)->where('is_cancelled', 0)->where('is_active_user', 1)->where('installment_amount','>',0)->whereIn('registration_no', $assignFiles)->get();
    

            // dd($dueInstallments);

            $duePersons = [];
            $due_amount = [];
            $sumArr     = [];
            $regArr     = [];

            foreach ($dueInstallments->chunk(10) as $key => $installment) {
                foreach ($installment as $inst) {
                    //Check receipt amounts and due amount
                    $inst_receipt = InstallmentReceipts::where('receipt_no', $inst->receipt_no)->where('os_amount', '!=', 0)->get();
                    $amount       = 0;
                    foreach ($inst_receipt as $receipt) {
                        $amount += ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                        // $amount += ($inst->due_amount - $receipt->received_amount);
                    }
                    $due_amount[$inst->registration_no][] = $inst->due_amount;
                    $os_amount[$inst->registration_no][]  = $amount;
                    // if($inst->registration_no == "CC-GS2-0809")
                    //     break;
                }

            }

            $i = 1;
            foreach ($due_amount as $key => $inst) {

                $dueSum = 0;
                foreach ($due_amount[$key] as $key3 => $reg) {
                    $dueSum += ($os_amount[$key][$key3] != 0) ? $os_amount[$key][$key3] : $reg;
                }
                $sumArr[$key] = $dueSum;

                $booking = Payments::where('registration_no', $key)->where('status', 1)->first();

                if ($booking) {

                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo) {

                        $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();

                        if ($is_member_active && !in_array($is_member_active->phone_mobile, $users_phone)) {
                            // $userPhone = $is_member_active->phone_mobile;
                            // if (strpos($is_member_active->phone_mobile, '-')) {
                            $userPhone = str_replace("-", "", $is_member_active->phone_mobile);
                            // }
                            array_push($users_phone, $userPhone);
                            array_push($users_name, $is_member_active->name);
                            $message                 = str_replace("[REG_NO]", $booking->registration_no, $request->message);
                            $final_msg               = str_replace("[AMOUNT]", number_format($sumArr[$key]), $message);
                            $sms_detail['message'][] = $final_msg;
                            $sms_detail['sent_to'][] = $booking->registration_no;
                            // $sms_detail['mobile'][] = "Due Installment - " . $booking->registration_no;
                        }
                    }
                }
            }

            // $user = User::Members();
            // foreach ($user as $key => $u) {
            //     if ($u->phone_mobile != null && !in_array($u->phone_mobile, $users_phone)) {

            //         $member = members::join('bookings', 'bookings.registration_no', '=', 'members.registration_no')
            //         ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
            //         ->where('members.person_id', $u->id)
            //         ->where('members.is_current_owner',1)
            //         ->where('payment_schedule.plot_nature', 'Commercial')
            //         ->first();

            //         if($member)
            //         {
            //             if (strpos($u->phone_mobile, '-')) {
            //                 $users_phone[] = str_replace("-", "", $u->phone_mobile);
            //             } else {
            //                 $users_phone[] = $u->phone_mobile;
            //             }
            //             $sms_detail['sent_to'][] = $member->registration_no;
            //             $sms_detail['message'][] = $request->message;
            //         }
            //     }
            // }
        }
        
        if ($plotTypeHousing) {

            if (!$request->target_date) {
                $request->session()->flash('message', "Please choose target date");
                $request->session()->flash('status', 0);
                return redirect()->route('sms.create');
            }

            $ploType = ($plotTypeHousing == Null) ? "Commerical" : "Housing";

            //Getting only commerical plots
            $assignFiles = assignFiles::where('plot_type', 'Housing')->pluck('registration_no')->toArray();
            // abort(200, "Working on it");
            $dueInstallments = memberInstallments::where('installment_date', '<=', $request->target_date)->where('is_paid', 0)->where('installment_amount','>',0)->where('is_cancelled', 0)->where('is_active_user', 1)->whereIn('registration_no', $assignFiles)->get();
            // dd($dueInstallments );

            // dd($dueInstallments);

            $duePersons = [];
            $due_amount = [];
            $sumArr     = [];
            $regArr     = [];

            foreach ($dueInstallments->chunk(10) as $key => $installment) {
                foreach ($installment as $inst) {
                    //Check receipt amounts and due amount
                    $inst_receipt = InstallmentReceipts::where('receipt_no', $inst->receipt_no)->where('os_amount', '!=', 0)->get();
                    $amount       = 0;
                    foreach ($inst_receipt as $receipt) {
                        $amount += ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                        // $amount += ($inst->due_amount - $receipt->received_amount);
                    }
                    $due_amount[$inst->registration_no][] = $inst->due_amount;
                    $os_amount[$inst->registration_no][]  = $amount;
                    // if($inst->registration_no == "CC-GS2-0809")
                    //     break;
                }

            }

            $i = 1;
            foreach ($due_amount as $key => $inst) {

                $dueSum = 0;
                foreach ($due_amount[$key] as $key3 => $reg) {
                    $dueSum += ($os_amount[$key][$key3] != 0) ? $os_amount[$key][$key3] : $reg;
                }
                $sumArr[$key] = $dueSum;

                $booking = Payments::where('registration_no', $key)->where('status', 1)->first();

                if ($booking) {

                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo) {

                        $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();

                        if ($is_member_active && !in_array($is_member_active->phone_mobile, $users_phone)) {
                            // $userPhone = $is_member_active->phone_mobile;
                            // if (strpos($is_member_active->phone_mobile, '-')) {
                            $userPhone = str_replace("-", "", $is_member_active->phone_mobile);
                            // }
                            array_push($users_phone, $userPhone);
                            array_push($users_name, $is_member_active->name);
                            $message                 = str_replace("[REG_NO]", $booking->registration_no, $request->message);
                            $final_msg               = str_replace("[AMOUNT]", number_format($sumArr[$key]), $message);
                            $sms_detail['message'][] = $final_msg;
                            $sms_detail['sent_to'][] = $booking->registration_no;
                            // $sms_detail['mobile'][] = "Due Installment - " . $booking->registration_no;
                        }
                    }
                }
            }

            // $user = User::Members();
            // foreach ($user as $key => $u) {
            //     if ($u->phone_mobile != null && !in_array($u->phone_mobile, $users_phone)) {

            //         $member = members::join('bookings', 'bookings.registration_no', '=', 'members.registration_no')
            //         ->join('payment_schedule', 'payment_schedule.id', '=', 'bookings.payment_schedule_id')
            //         ->where('members.person_id', $u->id)
            //         ->where('members.is_current_owner',1)
            //         ->where('payment_schedule.plot_nature', 'Commercial')
            //         ->first();

            //         if($member)
            //         {
            //             if (strpos($u->phone_mobile, '-')) {
            //                 $users_phone[] = str_replace("-", "", $u->phone_mobile);
            //             } else {
            //                 $users_phone[] = $u->phone_mobile;
            //             }
            //             $sms_detail['sent_to'][] = $member->registration_no;
            //             $sms_detail['message'][] = $request->message;
            //         }
            //     }
            // }
        }
        
        

        // if($members == null || $plotSize == null || $agency == null || $allMembers == null || $allEmployees == null || $dueInstallmentsInput == null || $overDueInstallmentsInput == null)

        if ($custom_mobile_number) {
            $sms_detail['sent_to'][] = $custom_mobile_number;
            $users_phone[]           = $custom_mobile_number;
            $sms_detail['message'][] = $request->message;
        }

        if ($plotSize) {
            foreach ($plotSize as $key => $value) {
                if ($value) {
                    $plot = plotSize::find($value);
                    if ($plot->Plan) {
                        $bookings = payments::where('payment_schedule_id', $plot->Plan->id)->where('status', 1)->get();
                        foreach ($bookings as $key => $booking) {
                            $member = members::Info($booking->registration_no);
                            $user   = User::find($member->person_id);
                            if ($user->phone_mobile != null && !in_array($user->phone_mobile, $users_phone)) {

                                if (strpos($user->phone_mobile, '-')) {
                                    $users_phone[] = str_replace("-", "", $user->phone_mobile);
                                    
                                } else {
                                    $users_phone[] = $user->phone_mobile;
                                    
                                }

                            }
                        }
                        $users_name[] = $user->name;
                        $sms_detail['sent_to'][] = $plot->plot_size;
                        $sms_detail['message'][] = $request->message;
                    }
                }
            }
        }

        if ($dueInstallmentsInput) {
            if (!$request->target_date) {
                $request->session()->flash('message', "Please choose target date");
                $request->session()->flash('status', 0);
                return redirect()->route('sms.create');
            }
            // abort(200, "Working on it");
            $dueInstallments = memberInstallments::where('installment_date', '<=', $request->target_date)->where('is_paid', 0)->where('is_cancelled', 0)->where('installment_amount','>',0)->where('is_active_user', 1)->get();

            $duePersons = [];
            $due_amount = [];
            $sumArr     = [];
            $regArr     = [];

            foreach ($dueInstallments->chunk(10) as $key => $installment) {
                foreach ($installment as $inst) {
                    //Check receipt amounts and due amount
                    $inst_receipt = InstallmentReceipts::where('receipt_no', $inst->receipt_no)->where('os_amount', '!=', 0)->get();
                    $amount       = 0;
                    foreach ($inst_receipt as $receipt) {
                        $amount += ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                        // $amount += ($inst->due_amount - $receipt->received_amount);
                    }
                    $due_amount[$inst->registration_no][] = $inst->due_amount;
                    $os_amount[$inst->registration_no][]  = $amount;
                    // if($inst->registration_no == "CC-GS2-0809")
                    //     break;
                }

            }

            $i = 1;
            foreach ($due_amount as $key => $inst) {

                $dueSum = 0;
                foreach ($due_amount[$key] as $key3 => $reg) {
                    $dueSum += ($os_amount[$key][$key3] != 0) ? $os_amount[$key][$key3] : $reg;
                }
                $sumArr[$key] = $dueSum;

                $booking = Payments::where('registration_no', $key)->where('status', 1)->first();

                if ($booking) {

                    $memberInfo = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo) {

                        $is_member_active = User::where('id', $memberInfo->person_id)->where('status', 1)->first();

                        if ($is_member_active && !in_array($is_member_active->phone_mobile, $users_phone)) {
                            // $userPhone = $is_member_active->phone_mobile;
                            // if (strpos($is_member_active->phone_mobile, '-')) {
                            $userPhone = str_replace("-", "", $is_member_active->phone_mobile);
                            // }
                            array_push($users_phone, $userPhone);
                            array_push($users_name, $is_member_active->name);
                            $message                 = str_replace("[REG_NO]", $booking->registration_no, $request->message);
                            $final_msg               = str_replace("[AMOUNT]", number_format($sumArr[$key]), $message);
                            $sms_detail['message'][] = $final_msg;
                            $sms_detail['sent_to'][] = $booking->registration_no;
                            // $sms_detail['mobile'][] = "Due Installment - " . $booking->registration_no;
                        }
                    }
                }
            }
        }

        if ($overDueInstallmentsInput) {
            $overDueInstallments = memberInstallments::where('installment_date', '<=', Carbon::now()->addMonths(2))->where('is_paid', 0)->where('installment_amount','>',0)->where('is_cancelled', 0)->where('is_active_user', 1)->get();

            $overDuePersons = [];

            foreach ($overDueInstallments as $key => $installment2) {
                $booking2 = Payments::where('registration_no', $installment2->registration_no)->where('status', 1)->first();
                //echo $installment->registration_no;
                if ($booking2) {

                    $memberInfo2 = members::where('registration_no', $booking2->registration_no)->where('is_current_owner', 1)->first();

                    if ($memberInfo2) {
                        $is_member_active2 = User::where('id', $memberInfo2->person_id)->where('status', 1)->first();
                        if ($is_member_active2 && !in_array($is_member_active2->phone_mobile, $users_phone)) {
                            if (strpos($is_member_active2->phone_mobile, '-')) {
                                $users_phone[] = str_replace("-", "", $is_member_active2->phone_mobile);
                                $users_name[] = $is_member_active2->name;
                            } else {
                                $users_phone[] = $is_member_active2->phone_mobile;
                                $users_name[] = $is_member_active2->name;
                            }

                            $sms_detail['sent_to'][] = "OverDue Installment - " . $booking2->registration_no;
                        }
                    }
                }
            }
        }

        if ($members) {
            foreach ($members as $booking_id) {
                $bookings = payments::where('id', $booking_id)->where('status', 1)->get();
                foreach ($bookings as $key => $booking) {
                    $member = members::Info($booking->registration_no);
                    $user   = User::find($member->person_id);
                    if ($user->phone_mobile != null && !in_array($user->phone_mobile, $users_phone)) {
                        if (strpos($user->phone_mobile, '-')) {
                            $users_phone[] = str_replace("-", "", $user->phone_mobile);
                            $users_name[] = $user->name;
                        } else {
                            $users_phone[] = $user->phone_mobile;
                            $users_name[] = $user->name;
                        }

                        $msg = $request->message;

                        if (strpos($request->message, "[AMOUNT]")) {

                            $dueInstallments = memberInstallments::where('installment_date', '<=', $request->target_date)->where('is_paid', 0)->where('installment_amount','>',0)->where('is_cancelled', 0)->where('registration_no', $booking->registration_no)->where('is_active_user', 1)->get();

                            $duePersons = [];
                            $due_amount = [];
                            $sumArr     = [];
                            $regArr     = [];

                            foreach ($dueInstallments as $key => $inst) {
                                //Check receipt amounts and due amount
                                $inst_receipt = InstallmentReceipts::where('receipt_no', $inst->receipt_no)->where('os_amount', '!=', 0)->get();
                                $amount       = 0;

                                foreach ($inst_receipt as $receipt) {
                                    $amount += ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount;
                                }
                                $due_amount[$inst->registration_no][] = $inst->due_amount;
                                $os_amount[$inst->registration_no][]  = $amount;
                                // if($inst->registration_no == "CC-GS2-0809")
                                //     break;
                            }
                            $i = 1;
                            foreach ($due_amount as $key => $inst) {

                                $dueSum = 0;
                                foreach ($due_amount[$key] as $key3 => $reg) {
                                    $dueSum += ($os_amount[$key][$key3] != 0) ? $os_amount[$key][$key3] : $reg;
                                }
                                $sumArr[$key] = $dueSum;

                                $msg = str_replace("[AMOUNT]", number_format($sumArr[$key]), $request->message);
                                // $sms_detail['message'][] = $final_msg;
                            }
                        }

                        $message                 = str_replace("[REG_NO]", $booking->registration_no, $msg);
                        $sms_detail['message'][] = $message;
                        $sms_detail['sent_to'][] = $member->registration_no;
                    }
                }
            }
        }

        // if($members)
        // {
        //     foreach ($members as $booking_id) {

        //         $bookings =  payments::where('id', $booking_id)->where('status', 1)->get();
        //         foreach ($bookings as $key => $booking) {
        //             $member = members::Info($booking->registration_no);
        //             $user = User::find($member->person_id);
        //             if($user->phone_mobile != null && !in_array($user->phone_mobile, $users_phone)){
        //                 if(strpos($user->phone_mobile, '-'))
        //                     $users_phone[] = str_replace("-","",$user->phone_mobile);
        //                 else
        //                     $users_phone[] = $user->phone_mobile;

        //                 $sms_detail['sent_to'][] = $member->registration_no;
        //             }
        //         }
        //     }
        // }

        if ($allMembers) {
            $user = User::Members();
            foreach ($user as $key => $u) {
                if ($u->phone_mobile != null && !in_array($u->phone_mobile, $users_phone)) {
                    if (strpos($u->phone_mobile, '-')) {
                        $users_phone[] = str_replace("-", "", $u->phone_mobile);
                        $users_name[] = $u->name;
                    } else {
                        $users_phone[] = $u->phone_mobile;
                        $users_name[] = $u->name;
                    }

                    $member = members::where('person_id', $u->id)->where('is_current_owner', 1)->first();
                    if ($member) {
                        $sms_detail['sent_to'][] = $member->registration_no;
                        $sms_detail['message'][] = $request->message;
                    }
                }
            }
        }

        if ($allEmployees) {
            $user = User::Employees();
            foreach ($user as $key => $u) {
                if ($u->phone_mobile != null && !in_array($u->phone_mobile, $users_phone)) {
                    if (strpos($u->phone_mobile, '-')) {
                        $phone         = str_replace("-", "", $u->phone_mobile);
                        $users_phone[] = $phone;
                    } else {
                        $phone         = $u->phone_mobile;
                        $users_phone[] = $u->phone_mobile;
                        
                    }
                    
                    $users_name[] = $u->name;

                    $sms_detail['sent_to'][] = $u->name . " - employee";
                    $sms_detail['message'][] = $request->message;
                }
            }
        }
        
        if ($allLPCs) {
            $lpcs = LPC::join('members','members.registration_no','lpc.registration_no')->join('bookings','bookings.registration_no','members.registration_no')->join('persons','persons.id','members.person_id')->select(DB::Raw('lpc.registration_no, persons.phone_mobile,persons.name, sum(lpc_amount - IFNULL(amount_received,0) - IFNULL(inst_waive,0)) as total_lpc'))->where('members.is_current_owner', 1)->where('persons.status', 1)->where('bookings.is_cancelled', 0)->groupBy('lpc.registration_no')->groupBy('persons.phone_mobile')->get();
            foreach ($lpcs as $lpc)
            {
                $msg = str_replace("[AMOUNT]", number_format($lpc->total_lpc), $request->message);
                $message                 = str_replace("[REG_NO]", $lpc->registration_no, $msg);
                        $sms_detail['message'][] = $message;
                        $sms_detail['sent_to'][] = $lpc->registration_no;
                        $users_phone[] = str_replace("-", "", $lpc->phone_mobile);
                        $users_name[] = $lpc->name;
            }
            //dd($lpcs);
        }
        //dd($sms_detail);

        if ($principalOfficers) {
            foreach ($principalOfficers as $key => $po) {
                $user = principal_officer::find($po);
                if ($user->cell != null && !in_array($user->cell, $users_phone)) {
                    if (strpos($user->cell, '-')) {
                        if (strpos($user->cell, ',')) {
                            $cell          = explode(',', $user->cell);
                            $users_phone[] = str_replace("-", "", $cell[0]);
                        } else {
                            $users_phone[] = str_replace("-", "", $user->cell);
                        }

                    } else {
                        $users_phone[] = $user->cell;
                    }

                }
                
                $users_name[] = $user->name;
                $sms_detail['sent_to'][] = $user->name . '- Principal Officer';
                $sms_detail['message'][] = $request->message;
            }

        }

        //Select all members that belongs to a speicifc agency
        if ($agency) {
            foreach ($agency as $key => $agenc) {
                if ($agenc) {
                    $agencies = agency::find($agenc);

                    //Get the pricipal officers of the agency
                    foreach ($agencies->principal_officer as $key => $principal_officer) {
                        if ($principal_officer->cell != null) {
                            //remove dash - from the number
                            $users_phone['phone'] = str_replace("-", "", $principal_officer->cell);
                            $users_name['name'] = $principal_officer->name;
                        }
                    }

                    $sms_detail['sent_to'][] = "Pricipal Officers";
                    $sms_detail['message'][] = $request->message;
                }
            }
        }

        if (count($users_phone) == 0) {
            $request->session()->flash('message', "No Phone Number Found");
            $request->session()->flash('status', 0);

            return redirect()->route('sms.index');
        }

        $mobileNo = [];

        //dd($users_name);
        $sendNow     = $request->input('sendnow');
        $sendingDate = $request->input('sending_date');
        $message     = $request->input('message');

        if(isset($sms_detail['message']))
        {
            $smss = DB::select('select * from sms_temp');
            if(count($smss) == 0)
            {
                foreach($sms_detail['message'] as $key2 => $message2)
                {
                    $mobile = $users_phone[$key2];
                    DB::insert('insert into sms_temp(registration_no,mobile,message) values("'. $sms_detail['sent_to'][$key2] .'","'. $mobile .'","'. $message2 .'")');
                }
            }
        }
        return view('admin.sms.preview', compact('sms_detail', 'users_phone', 'sendNow', 'sendingDate', 'dueInstallmentsInput', 'message','users_name'));

    }

    public function send(Request $request)
    {
        //dd($request);
        $msgArr = [];
        $sent   = 1;
        $smss = DB::select('select * from sms_temp');
        foreach($smss as $sm)
        {
            $sms = SMSModel::create([
                'message'      => $sm->message,
                'event'        => "Reminder",
                'created_by'   => Auth::user()->id,
                'sent_to'      => $sm->mobile,
                'sending_date' => $request->input('sendingDate'),
                'sent'         => $sent,
            ]);
            
            $sent_to = '';
            if(isset($sm->mobile))
                $sent_to = $sm->mobile;
            else
                $sent_to = $sm->registration_no;
                
            sms_detail::create([
                'sms_id'  => $sms->id,
                'sent_to' => $sent_to,
            ]);
        }
        DB::delete('DELETE FROM sms_temp');
        /*foreach ($request->mobile as $key => $phone) {

            $mobiles = explode(',',$phone);
            foreach($mobiles as $mobile)
            {
                $sms = SMSModel::create([
                    'message'      => $request->message[$key],
                    'event'        => "Reminder",
                    'created_by'   => Auth::user()->id,
                    'sent_to'      => $mobile,
                    'sending_date' => $request->input('sendingDate'),
                    'sent'         => $sent,
                ]);
                
                $sent_to = '';
                if(count($mobiles) > 1)
                    $sent_to = $mobile;
                else
                    $sent_to = $request->sent_to[$key];
                    
                sms_detail::create([
                    'sms_id'  => $sms->id,
                    'sent_to' => $sent_to,
                ]);
            }

            // $this->SendSMS($request->input('message'), $phone);
        }*/

        return redirect()->route('sms.index')->with(['msg' => 'Sms are queued successfully.', 'status' => 1]);
    }
    
    public function downloadcsv(Request $request){
        
        $CsvData=array('First Name=Last Name=Email=Mobile=Message');
        
         dd($request->name);
        foreach($request->mobile as $key => $phone){
            
            $mobiles = explode(',',$phone);
            
            foreach($mobiles as $mobile)
            {
          
            if(isset($request->name[$key])){
                $fname = $request->name[$key];
                $message = $request->message[$key];
            }
            else{
                $fname = 'Capital';
                $message = '';
            }
            
            $lname = 'City';
            $email = 'admin@lahorecapitalcity.com';
            $mobile = $mobile;
            
            
            
            $CsvData[]=$fname.'='.$lname.'='.$email.'='.$mobile.'='.$message;
        
    }
    
    
    
        }

        $filename=date('Y-m-d').".csv";
        $file_path=base_path().'/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
          fputcsv($file,explode('=',$exp_data));
        }
        fclose($file);          
 
        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
        
    }

    public function checkSentTo(Request $request)
    {
        $sms = SMSModel::find($request->input('sms_id'));

        return ['smsDetail' => $sms->SmsDetail, 'sms' => $sms];
    }

    public function smstemplates(Request $request)
    {
        if ($request->ajax()) {
            $smstemplate = SMSTemplates::all();
            return $smstemplate;
        }
    }

    public function getmessage(Request $request)
    {
        if ($request->ajax()) {
            $smstemplate = SMSTemplates::find($request->input('id'));
            return $smstemplate;
        }
    }

    public function welcomeSMS()
    {
        $members = Payments::Active();
        return view('admin.sms.welcome', compact('members'));
    }

    public function welcomeSMSPost(Request $request)
    {
        if ($request->member) {
            $users_phone = "";
            $booking     = payments::where('id', $request->member)->where('status', 1)->first();
            $member      = members::Info($booking->registration_no);
            $user        = User::find($member->person_id);
            if ($user->phone_mobile != null) {
                if (strpos($user->phone_mobile, '-')) {
                    $users_phone[] = str_replace("-", "", $user->phone_mobile);
                } else {
                    $users_phone[] = $user->phone_mobile;
                }

                $sms_detail['sent_to'][] = $member->registration_no;
            }

            $sendNow              = $request->input('sendnow');
            $sendingDate          = $request->input('sending_date');
            $dueInstallmentsInput = null;

            $receipt = receipts::where('registration_no', $booking->registration_no)->latest()->first();
            $message = str_replace("[AMOUNT]", number_format($receipt->received_amount), $request->input('message'));

            $finalMessage = str_replace("[REG_NO]", $booking->registration_no, $message);

            $sms_detail['message'][] = $finalMessage;

            return view('admin.sms.preview', compact('sendNow', 'sms_detail', 'sendingDate', 'users_phone', 'dueInstallmentsInput'));
        } else {
            return redirect()->back()->with(['status' => 0, 'message' => 'Please select member to send sms.']);
        }

        dd($request->input());
    }
    
    
    // public function t_sms(){
        
    //     $response = Curl::to('http://api.bizsms.pk/api-send-branded-sms.aspx')
    //     ->withData( 
    //     	array( 
    //     		'username' => 'capitalcity@bizsms.pk',
    //     		'pass' => 'cap1taty679',
    //     		'text' => "SMS Api",
    //     		'masking' => 'CapitalCity',
    //     		'destinationnum' => '923114344208',
    //     		'language' => 'English',
    //     	) 
    //     )
    //     ->get();
        
    //     return $response;
    // }

}
