<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\PaymentScheduleRequest;
use App\InstallmentReceipts;
use App\LPC;
use App\Payments;
use App\SMSModel;
use App\SMSTemplates;
use App\Traits\SMS;
use App\User;
use App\agency;
use App\agents;
use App\alerts;
use App\assignFiles;
use App\featureset;
use App\memberInstallments;
use App\members;
use App\otherPayments;
use App\paymentSchedule;
use App\payment_types;
use App\pdc;
use App\plotSize;
use App\receipts;
use App\sms_detail;
use App\transfers;
use Carbon\Carbon;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Facades\Storage;

class PaymentsController extends Controller
{
    use SMS;

    public function index()
    {
        $rights = Role::getrights('bookings');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            //return view('admin.payments.index', compact('BookingDetail','rights'));
            return view('admin.payments.index', compact('rights'));
        }
    }

    public function getBookings(Request $request)
    {
        // $sortby = $request->input('order.0.column');
        // $sortDir = $request->input('order.0.dir');

        $search = $request->squery;
        $booking = Payments::with([
            'paymentPlan',
            'agency'
        ])
        ->join('members', 'members.registration_no','=', 'bookings.registration_no')
        ->join('persons', 'members.person_id','=', 'persons.id')
        ->join('payment_schedule', 'payment_schedule.id','=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id','=', 'payment_schedule.plot_size_id')

        ->when($search, function($query) use ($search) {
            $query->where(function($q) use ($search){
                $q->orWhere('members.registration_no', 'like', '%'.$search.'%');
                $q->orWhere('members.reference_no', 'like', '%'.$search.'%');
                $q->orWhere('payment_schedule.payment_code', 'like', '%'.$search.'%');
                $q->orWhere('plot_size.plot_size', 'like', '%'.$search.'%');
                $q->orWhere('persons.name', 'like', '%'.$search.'%');
                $q->orWhere('bookings.created_at', 'like', '%'.$search.'%');
            });
        })
        ->where('bookings.status',1)
        ->where('members.is_current_owner',1)
        ->whereNull('bookings.deleted_at')
        //->where('bookings.is_cancelled',0)
        // ->orderBy($sortby, $sortDir)
        ->get();

        $data = Datatables::of($booking)
            ->addColumn('actions', function($booking){

                $html = "";
                $l_rights = Role::getrights('ledger');
                $f_rights = Role::getrights('followup');
                $i_rights = Role::getrights('inquiry');
                $b_rights = Role::getrights('bookings');
                 $c_rights = Role::getrights('CANCEL');
                if($l_rights->can_view)
                    $html .= '<a href="'.route('booking.ledger', $booking->registration_no).'" class="btn btn-xs btn-warning">Ledger</a>';

                if($b_rights->can_edit)
                    $html .= '<a href="'.route('booking.edit', $booking->registration_no).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if($b_rights->can_delete)
                    $html .= '<form method="post" style="display: inline" action="'.route('booking.destroy', $booking->registration_no).'">'.csrf_field()."<input name='_method' type='hidden' value='DELETE'> <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'></form>";

                if($f_rights->can_create)
                    $html .= '<a href="'.route('followup.show', $booking->registration_no).'" class="btn btn-xs btn-warning">Followup</a>';

                if($i_rights->can_create)
                    $html .= '<a href="'.route('member.inquiry', $booking->registration_no).'" class="btn btn-xs btn-warning">Inquiry</a>';
                    
                     if($c_rights->can_create)
                    $html .= '<a href="'.route('booking.cancel', $booking->registration_no).'" class="btn btn-xs btn-warning">CRM</a>';

                    $html .= '<a href="'.route('booking.welcomeLetter').'" class="btn btn-xs btn-warning">Welcome Ltr</a>';
                    
                return $html;
            })
            ->editColumn('date', function($booking){
                return date('d-m-Y', strtotime($booking->payment_date));
            })
            ->addColumn('plan', function($booking){
                return $booking->paymentPlan->payment_code;
            })
            ->addColumn('plot_size', function($booking){
                return $booking->plot_size;
            })

            ->addColumn('reference_no', function($booking){
                 /* $member = members::with(['user'])->where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->last();

                if (!is_null($member)) { */
                    return $booking->reference_no;
                //}
            })

            ->addColumn('name', function($booking){
                
                    
                if($booking->is_cancelled != 0){
                    $label = Payments::getlabel($booking->is_cancelled);
                    return $booking->name . $label;
                }
                else{
                    return $booking->name;
                }
            })

            ->addColumn('paid', function($booking){
                $received_sum = 0;
                $installments = memberInstallments::where('registration_no', $booking->registration_no)->get();
                foreach ($installments as $key => $installment) {
                        $received_sum += ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $installment->amount_received;
                }
                return $received_sum;
            })

            ->addColumn('total', function($booking){
                return $booking->paymentPlan->total_price;
            })
            // ->editColumn('batch_no', function($BookingDetail){
            //     return $BookingDetail->batch->batch_no;
            // })
            // ->editColumn('batch_date', function($BookingDetail){
            //     return ($BookingDetail->batch != null) ? date('d-m-Y', strtotime($BookingDetail->batch->batch_assign_date)) : "";
            // })
            // ->editColumn('is_reserved', function($BookingDetail){
            //     return ($BookingDetail->is_reserved == 1) ? "Yes" : "No";
            // })
            ->rawColumns(['actions','name'])
            ->setRowId(function ($booking) {
                    if($booking->is_cancelled != 0){
                    $label = \App\Helpers::booking_status($booking->is_cancelled);
                    return $label;
                }
                else{
                    return "not-label";
                }
                })
            ->make(true);
            return $data;
    }

    public function index2()
    {
        $booking = Payments::Active();

        $BookingDetail['booking'] = $booking;

        foreach ($booking as $bookings) {


            $BookingDetail['paymentPlan'][] = $bookings->paymentPlan;

            $BookingDetail['PlotSize'][] = $bookings->paymentPlan->plotSize;

            $members = members::where('registration_no', $bookings->registration_no)->where('is_current_owner', 1)->get();

            foreach ($members as $member) {
                $person = User::find($member->person_id);
                $BookingDetail['person'][] = $person;
            }

            if(!is_null($member))
                $BookingDetail['member'][] = $member;

            $received_sum = 0;
            $installments = memberInstallments::where('registration_no', $bookings->registration_no)->where('is_paid', 1)->get();
            foreach ($installments as $key => $installment) {
                $received_sum+=$installment->amount_received;
            }

            $BookingDetail['inst'][] = $received_sum;

        }

    	return view('admin.payments.index',compact('BookingDetail'));
    }
    
    public function crm($reg_no)
    {
        //$reg_no = $request->registration_no;
        return view('admin.payments.cancel', compact('reg_no'));
    }
    
    public function cancelBooking(Request $request, $reg_no)
    {
        $booking = Payments::where('registration_no',$reg_no)->first();
        $booking->is_cancelled = $request->action;
        $booking->cancel_date = date('Y-m-d',strtotime(strtr($request->input('cancel_date'), '/', '-')));
        $booking->cancel_remarks = $request->remarks;
        $msg = "Booking has been cancelled successfully.";
        if($request->action == 2 || $request->action == 3)
        {
            $booking->deduct_amount = $request->deduction;
            if($request->action == 2)
            {
                $booking->refund_amount = $request->amount;
                $msg = "Booking has been refunded successfully.";
            }
            else
            {
                $booking->merge_amount = $request->amount;
                
                for($i=0; $i < count($request->merge_into); $i++)
                {
                    DB::insert('insert into booking_merged(booking_id, merged_into, amount) values('. $booking->id .',"'. $request->merge_into[$i] .'",'. $request->merge_amount[$i] .')');
                }
                $msg = "Booking has been merged successfully.";
            }
        }
        $booking->update();
        
        DB::update('update member_installments set is_cancelled = '. $request->action .' where registration_no="'. $reg_no .'"');
        DB::update('update receipts set booking_cancelled = '. $request->action .' where registration_no="'. $reg_no .'"');
        
        Session::flash('status', 'success');
        Session::flash('message', $msg);

        return redirect('admin/booking');
    }
    

    public function create()
    {
        $rights = Role::getrights('bookings');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $paymentSchedule = paymentSchedule::all();
        $featureset = featureset::all();
        $agents = User::Agents();
        $agencies = agency::all();

        return view('admin.payments.create',compact('featureset','paymentSchedule','agents','agencies'));
    }

    public function store(BookingRequest $request)
    {
        $registration_no = $request->input('registration_no');
        $file = assignFiles::where('registration_no', $registration_no)->first();

        if($file == null){
            return redirect('admin/booking/')->with(['status' => 0, 'message' => "File ". $registration_no." does not exist."]);
        }

        if($file->is_blocked == 1)
        {
            return redirect()->back()->with(['status' => 0, 'message' => "File is blocked"]);
        }

        DB::beginTransaction();

        try {
            $payment1 = Payments::where('registration_no' , $registration_no)->first();
    
            if($payment1){
                if($payment1->is_cancelled == 1){
    
                    $payment1->is_cancelled = 0;
                    $payment1->cancel_date = null;
                    $payment1->cancel_by = null;
                    $payment1->cancel_remarks = null;
                    $payment1->save();
    
                    $member = members::where('registration_no' , $payment1->registration_no)->first();
                    $member->is_current_owner = 0;
                    $member->save();
    
                    $memberInst = memberInstallments::where('registration_no', $payment1->registration_no)->get();
                    foreach ($memberInst as $key => $inst) {
                        $inst->is_cancelled = 1;
                        $inst->save();
                    }
                }
            }
    
            $input = $request->input();
            //dd($input);
            $input['created_by']  = Auth::user()->id;
            $input['payment_date']  = $request->payment_date;
            
            $payment         = Payments::create($input);
            $payment->save();
            
            // dd($payment);
            
            $featureset = $request->input('feature_set_id') ? $request->input('feature_set_id') : [];
            $total_f = 0;
            for($i=0;$i<count($featureset);$i++)
            {
                $feat_id = $featureset[$i];
                $feat = \App\featureset::find($feat_id);
                
                $ps = \App\paymentSchedule::find($request->payment_schedule_id);
                $total_f = ($ps->total_price * $feat->charges) / 100;
                
                $bf = new \App\booking_featureset();
                $bf->payment_id = $payment->id;
                $bf->feature_set_id = $feat_id;
                $bf->fst_payment = $total_f;
                $bf->is_paid = 0;
                $bf->save();
            }
    
            $file->is_reserved = 1;
            $file->save();
    
            //Get other payments and add in member installments
            $otherPayments = otherPayments::where('payment_schedule_id', $payment->payment_schedule_id)->where('payment_type_id', 4)->get();
            if($otherPayments)
            {
                foreach ($otherPayments as $otherPayment) {
                    $data1 = [
                        'installment_no'      => 1,
                        'installment_amount'  => $otherPayment->amount_to_receive,
                        'due_amount'          => $otherPayment->amount_to_receive,
                        'amount_received'     => 0,
                        'registration_no'     => $registration_no,
                        'installment_date'    => $payment->payment_date,
                        'payment_schedule_id' => $request->input('payment_schedule_id'),
                        'payment_desc'        => $otherPayment->paymentType->payment_type,
                        'is_other_payment'    => 1,
                    ];
                    memberInstallments::create($data1);
                }
            }
    
            if($request->input('installment_no'))
            {
                foreach ($request->input('installment_no') as $key => $value) {
                    $data = [
                        'installment_no'      => $value,
                        'installment_date'    => date('Y-m-d', strtotime($request->input('installment_date')[$key])),
                        'amount_percentage'   => $request->input('percentage')[$key],
                        'installment_amount'  => $request->input('installment_amount')[$key],
                        'due_amount'          => $request->input('installment_amount')[$key],
                        'rebat_amount'        => $request->input('rebat_amount')[$key],
                        'amount_received'     => $request->input('amount_received')[$key],
                        'remarks'             => $request->input('installment_remarks')[$key],
                        'registration_no'     => $registration_no,
                        'payment_schedule_id' => $request->input('payment_schedule_id'),
                        'payment_desc'        => 'Installment',
                    ];
                    memberInstallments::create($data);
                }
            }
            
            $otherPayments = otherPayments::where('payment_schedule_id', $payment->payment_schedule_id)->where('amount_to_receive','>',0)->where('payment_type_id', 5)->get();
    
            if($otherPayments)
            {
                $value = isset($value) ?: 1;
                foreach ($otherPayments as $otherPayment) {
                    $installment_no = ++$value;
                    $data1 = [
                        'installment_no'      => $otherPayment->installment_no,
                        'installment_amount'  => $otherPayment->amount_to_receive,
                        'due_amount'          => $otherPayment->amount_to_receive,
                        'amount_received'     => 0,
                        'registration_no'     => $registration_no,
                        'installment_date'    => date("Y-m-d", strtotime("+3 month", strtotime($payment->payment_date))),
                        'payment_schedule_id' => $request->input('payment_schedule_id'),
                        'payment_desc'        => $otherPayment->paymentType->payment_type,
                        'is_other_payment'    => 1,
                    ];
                    memberInstallments::create($data1);
                }
            }
    
            //Get other payments and add in member installments
            $otherPayments = otherPayments::where('payment_schedule_id', $payment->payment_schedule_id)->where('amount_to_receive','>',0)->where('payment_type_id', 1)->get();
    
            if($otherPayments and count($otherPayments) > 0)
            {
                $inst = memberInstallments::where([['registration_no', $registration_no],['is_other_payment',0]])->orderBy('id', 'desc')->first();
    
                $time = strtotime($inst->installment_date);
                $balloting_date = date("Y-m-d", strtotime("+1 month", $time));
            
                $value = isset($value) ?: 1;
                foreach ($otherPayments as $otherPayment) {
                    $installment_no = ++$value;
                    $data1 = [
                        'installment_no'      => $otherPayment->installment_no,
                        'installment_amount'  => $otherPayment->amount_to_receive,
                        'due_amount'          => $otherPayment->amount_to_receive,
                        'amount_received'     => 0,
                        'registration_no'     => $registration_no,
                        'installment_date'    => $balloting_date,
                        'payment_schedule_id' => $request->input('payment_schedule_id'),
                        'payment_desc'        => $otherPayment->paymentType->payment_type,
                        'is_other_payment'    => 1,
                    ];
                    memberInstallments::create($data1);
                }
            }
    
    
            $person = User::where('cnic',  $request->input('cnic'))->first();
    
            // if ( !is_null($person->member->registration_no))
            // {
            //     //Create a new member
            //     $new_member = new members();
            //     $new_member->person_id = $person->id;
            //     $new_member->registration_no = $registration_no;
            //     $new_member->reference_no = $request->input('reference_no');
            //     $new_member->is_current_owner = 1;
            //     $new_member->save();
            // }else{
            //     //Allot registration number to member
            //     $person->member->registration_no = $registration_no;
            //     $person->member->reference_no = $request->input('reference_no');
            //     $person->member->is_current_owner = 1;
            //     $person->member->save();
            // }
            
            //dd($person);
    
            if (!is_null($person->member->registration_no)) {
                //Create a new member
                $new_member                   = new members();
                $new_member->person_id        = $person->id;
                $new_member->registration_no  = $registration_no;
                $new_member->reference_no     = $request->input('reference_no');
                $new_member->nominee_name     = $request->input('nominee_name');
                $new_member->nominee_guardian     = $request->input('nominee_guardian');
                $new_member->nominee_cnic     = $request->input('nominee_cnic');
                $new_member->relation_with_member     = $request->input('relation_with_member');
    
                $new_member->is_current_owner = 1;
                $new_member->save();
            } else {
                //Allot registration number to member
                //dd($person);
                $person->member->registration_no  = $registration_no;
                $person->member->reference_no     = $request->input('reference_no');
                // $new_member->nominee_name     = $request->input('nominee_name');
                // $new_member->nominee_guardian     = $request->input('nominee_guardian');
                // $new_member->nominee_cnic     = $request->input('nominee_cnic');
                // $new_member->relation_with_member     = $request->input('relation_with_member');
                $person->member->is_current_owner = 1;
                $person->member->save();
            }
    
            /*$featureset = $request->input('feature_set_id') ? $request->input('feature_set_id') : [];
            $payment->featureset()->attach($featureset);*/
    
            //Check if this is booking has some offers
            if($request->input('offer') == 'launch_offer')
            {
                //Get first two installments of member
                $memberInstallments = memberInstallments::where('registration_no', $payment->registration_no)->whereIn('installment_no', [1,2])->where('payment_desc', 'Installment')->get();
    
                $payment->offer = 'Launch Offer';
                $payment->save();
    
                foreach($memberInstallments as $installment)
                {
                    //Check of existing IWR receipt
                    $receipts = receipts::where('receipt_no','like','IWR%')->orderBy('id','DESC')->limit(1)->first();
                    if($receipts)
                    {
                        $iwr = substr($receipts->receipt_no, 3);
                        $receipt_no = 'IWR'.++$iwr;
                    }else{
                        $receipt_no = 'IWR1';
                    }
    
                    //Create new receipt
                    $newReceipt = new receipts();
    
                    $newReceipt->receipt_no = $receipt_no;
                    $newReceipt->registration_no = $payment->registration_no;
                    $newReceipt->receiving_date = date('Y-m-d');
                    $newReceipt->receiving_mode = 'CASH';
                    $newReceipt->receiving_type = 2;
                    $newReceipt->received_amount = $installment->due_amount;
                    $newReceipt->receipt_remarks = 'Installment';
                    $newReceipt->save();
    
    
                    $installmentReceipt = new InstallmentReceipts();
                    $installmentReceipt->registration_no = $payment->registration_no;
                    $installmentReceipt->receipt_id = $newReceipt->id;
                    $installmentReceipt->installment_id = $installment->id;
                    $installmentReceipt->due_amount = $installment->due_amount;
                    $installmentReceipt->received_amount = $installment->due_amount;
                    $installmentReceipt->os_amount = $installment->os_amount;
                    $installmentReceipt->receipt_no = $newReceipt->receipt_no;
                    $installmentReceipt->receipt_amount = $installment->due_amount;
                    $installmentReceipt->payment_mode = 'CASH';
                    $installmentReceipt->payment_desc   = $installment->payment_desc;
                    $installmentReceipt->installment_no = $installment->installment_no;
                    $installmentReceipt->due_date = $installment->installment_date;
                    $installmentReceipt->save();
    
    
                    //updating memberInstallment table for specified installments.
                    $installment->receipt_no = $newReceipt->receipt_no;
                    $installment->amount_received = $installment->due_amount;
                    $installment->receipt_amount = $installment->due_amount;
                    $installment->payment_mode = 'CASH';
                    $installment->payment_date = date('Y-m-d');
                    $installment->is_paid = 1;
                    $installment->save();
    
                }
            }
    
    
            //Check if this is booking has some offers
            if($request->input('offer') == 'waive_dp')
            {
                //Get first two installments of member
                $installment = memberInstallments::where('registration_no', $payment->registration_no)->where('installment_no', 1)->where('payment_desc', 'Down Payment')->first();
    
                $payment->offer = 'Waive Down Payment';
                $payment->save();
    
                //Check of existing IWR receipt
                $receipts = receipts::where('receipt_no','like','IWR%')->orderBy('id','DESC')->limit(1)->first();
                if($receipts)
                {
                    $iwr = substr($receipts->receipt_no, 3);
                    $receipt_no = 'IWR'.++$iwr;
                }else{
                    $receipt_no = 'IWR1';
                }
    
                //Create new receipt
                $newReceipt = new receipts();
    
                $newReceipt->receipt_no = $receipt_no;
                $newReceipt->registration_no = $payment->registration_no;
                $newReceipt->receiving_date = date('Y-m-d');
                $newReceipt->receiving_mode = 'CASH';
                $newReceipt->receiving_type = 1;
                $newReceipt->received_amount = $installment->due_amount;
                $newReceipt->receipt_remarks = 'Installment';
                $newReceipt->save();
    
    
                $installmentReceipt = new InstallmentReceipts();
                $installmentReceipt->registration_no = $payment->registration_no;
                $installmentReceipt->receipt_id = $newReceipt->id;
                $installmentReceipt->installment_id = $installment->id;
                $installmentReceipt->due_amount = $installment->due_amount;
                $installmentReceipt->received_amount = $installment->due_amount;
                $installmentReceipt->os_amount = $installment->os_amount;
                $installmentReceipt->receipt_no = $newReceipt->receipt_no;
                $installmentReceipt->receipt_amount = $installment->due_amount;
                $installmentReceipt->payment_mode = 'CASH';
                $installmentReceipt->payment_desc   = $installment->payment_desc;
                $installmentReceipt->installment_no = $installment->installment_no;
                $installmentReceipt->due_date = $installment->installment_date;
                $installmentReceipt->receipt_date = $installment->installment_date;
                $installmentReceipt->save();
    
    
                //updating memberInstallment table for specified installments.
                $installment->receipt_no = $newReceipt->receipt_no;
                $installment->amount_received = $installment->due_amount;
                $installment->receipt_amount = $installment->due_amount;
                $installment->payment_mode = 'CASH';
                $installment->payment_date = date('Y-m-d');
                $installment->is_paid = 1;
                $installment->save();
            }
    
    
            activity('Booking')->performedOn($payment)
                    ->withProperties(['registration_no' => $registration_no, 'event' => 'created'])
                    ->log("Created new booking for user named <a href='".route('member.inquiry', $registration_no)."' style='font-weight: bold; color: #f9a533'> {$person->name} </a> and CNIC {$person->cnic}");
    
    
            $sms_template = SMSTemplates::where('name', 'Welcome Message')->first();
    
            $phone = str_replace("-","",$person->phone_mobile);
            
            //$mobile = substr($phone,0,1) == 0 ? '92'.substr($phone,1) : $phone;
            
            $mobile = $phone;
    
            $res = $this->SendSMS($sms_template->body, $mobile);
        	$message = "Success! New Booking Added";
    
            /*$status = 0;
            if(strpos($res,'Message Sent Successfully',0) !== false)
            {*/
                $status = 1;
                \Log::info("Member Registration SMS at " . $mobile);
            //}
            
            $sms = SMSModel::create([
                'message'    => $sms_template->body,
                'event'      => "Member Registration",
                'sent_to'      => $mobile,
                'sent'      => $status,
                'created_by' => Auth::user()->id,
            ]);
    
            sms_detail::create([
                'sent_to' => $registration_no,
                'sms_id' => $sms->id,
            ]);
            
            DB::commit();
    
            Session::flash('status', 'success');
            Session::flash('message', 'Booking has been added successfully.');
    
            return redirect('admin/booking/')->with(['status' => 1]);
        }
        catch (\Exception $e) {
            DB::rollBack();
            $message = "Some error occured! please try again later.";
            \Log::info($e->getMessage());
            return redirect()->back()->with(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    public function ledgerDownload($booking_id)
    {
        $booking      = Payments::find($booking_id);
        $member       = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();
        $person       = User::find($member->person_id);
        $installments = memberInstallments::where('registration_no', $booking->registration_no)->get();

        $data['person']       = $person;
        $data['booking']      = $booking;
        $data['member']       = $member;
        $data['installments'] = $installments;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.payments.ledger-pdf', $data);
        return $pdf->stream($booking->registration_no . '.pdf');
    }

    public function destroy($id)
    {
    	// $payment_schedule = Payments::find($id);
     //    $payment_schedule->status = 0;
     //    $payment_schedule->save();
    	// return redirect('admin/booking');



        
        $payment_schedule         = Payments::where('registration_no', $id)->first();
        $payment_schedule->status = 0;
        $members_booking = members::where('registration_no',$payment_schedule->registration_no)->where('is_current_owner',1)->first();

        // dd($payment_schedule->registration_no);
        $unique_id = uniqid();


        $assignFiles = assignFiles::where('registration_no', $members_booking->registration_no)->first();
        if($assignFiles)
        {
            $assignFiles->is_reserved = 0;
            $assignFiles->save();
        }

        $receipts = receipts::where('registration_no', $members_booking->registration_no)->get();
        foreach ($receipts as $key => $receipt) {
            $receipt->is_active = 0;
            $receipt->save();
            $receipt->delete();
        }


        $installment = memberInstallments::where('registration_no', $members_booking->registration_no)->get();
        foreach ($installment as $key => $inst) {
            $inst->registration_no = $members_booking->registration_no. "-".$unique_id;
            $inst->registration_no = $members_booking->registration_no;
            $inst->is_active_user = 0;
            $inst->save();
            $inst->delete();
        }

        $lpc = LPC::where('registration_no',$members_booking->registration_no)->get();
        if($lpc)
        {
            foreach ($lpc as $key => $l) {
                $l->registration_no = $members_booking->registration_no. "-".$unique_id;
                // $l->registration_no = $members_booking->registration_no;
                $l->save();
                $l->delete();
            }
        }

        // $pdcs = pdc::where('registration_no',$members_booking->registration_no)->get();
        // if($pdcs)
        // {
        //     foreach ($pdcs as $key => $pdc) {
        //         $pdc->registration_no = $members_booking->registration_no. "-".$unique_id;
        //         $pdc->save();
        //     }
        // }

        // $members_booking->registration_no = $members_booking->registration_no;
        // $members_booking->reference_no = $members_booking->reference_no;

        $members_booking->registration_no = $members_booking->registration_no. "-".$unique_id;
        $members_booking->reference_no = $members_booking->reference_no. "-".$unique_id;
        $members_booking->save();


        // $payment_schedule->registration_no = $payment_schedule->registration_no;
        $payment_schedule->registration_no = $payment_schedule->registration_no."-".$unique_id;
        $payment_schedule->save();

        $person = User::find($members_booking->person_id);
        $person->status = 0;
        $person->save();
        $person->delete();

        $payment_schedule->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Booking has been deleted successfully.');

        return redirect('admin/booking');
    }
    public function show($id)
    {
        abort("404");
    }

    public function edit($id)
    {
        if (strpos($id, '-') !== false) {
            $booking      = Payments::where('registration_no', $id)->firstorfail();
            }else{
            $booking      = Payments::find($id);
        }
        $BookingDetail['booking'] = $booking;
        $BookingDetail['paymentPlan'] = $booking->paymentPlan;
        $BookingDetail['installmentPlan'] = memberInstallments::where('payment_schedule_id', $booking->payment_schedule_id)->where('registration_no', $booking->registration_no)->get();

        $BookingDetail['PlotSize'] = $booking->paymentPlan->plotSize;

        $members = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();

        $person = User::find($members->person_id);




        $BookingDetail['person'] = $person;

        if (!is_null($members)) {
            $BookingDetail['member'] = $members;
        }




        $featureset      = featureset::all();
        $agents          = User::all();
        $agencies        = agency::all();
        $paymentTypes    = payment_types::all();
        $paymentTypes    = payment_types::all();
        $paymentSchedule = paymentSchedule::all();

        $installments = memberInstallments::where('registration_no', $booking->registration_no)->get();
        // dd($BookingDetail);
        return view('admin.payments.edit', compact('BookingDetail', 'featureset', 'agents', 'agencies', 'paymentTypes', 'paymentSchedule', 'installments'));
    }



    public function update($id, PaymentScheduleRequest $request)
    {
        $payment_schedule = paymentSchedule::find($id);

        $payment_schedule->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Booking has been updated successfully.');

        return redirect('admin/payment-schedule');
    }



    public function updateBookingDetail($id, Request $request)
    {
        $payment             = Payments::find($id);
        $input               = $request->input();
        
        $input['updated_by'] = Auth::user()->id;

        $person = User::where('cnic', $request->input('cnic'))->first();

        $person->member->registration_no = $request->input('registration_no');
        $person->member->reference_no    = $request->input('reference_no');
        $person->member->updated_by      = Auth::user()->id;
        $person->member->save();

        $installments = memberInstallments::where('registration_no', $payment->registration_no)->get();

        foreach ($installments as $key => $installment) {
            $installment->registration_no = $request->input('registration_no');
            $installment->save();
        }

        $payment->update($input);

        $payment->featureset()->sync($request->input('feature_set_id'));

        Session::flash('status', 'success');
        Session::flash('message', 'Booking has been updated successfully.');

        return redirect('admin/booking/' . $payment->registration_no . '/edit')->with(['status' => 1]);

    }

    public function propertyInfo(Request $request, $property)
    {
        if( $request->ajax() )
        {
            $proper = property::findorfail($property);
            return $proper;
        }
        else
            return $property;
    }

    public function memberInfo($id)
    {
        $member1 = members::find($id);
        //members with same person id
        $all_members = members::where('person_id', $member1->person_id)->where('is_current_owner', 1)->get();
        $BookingDetail[] = NULL;
        foreach ($all_members as $index => $member) {

            if( $member->registration_no != "")
            {
                $person = User::find($member->person_id);
                $BookingDetail['person'][] = $person;

                $payments = Payments::where('registration_no', $member->registration_no)->first();

                if(!is_null($payments))
                {
                    $BookingDetail['booking'][] = $payments;
                    $BookingDetail['paymentPlan'][] = $payments->paymentPlan;
                    $BookingDetail['PlotSize'][] = $payments->paymentPlan->plotSize;
                }
            }

            // foreach ($payments as $payment) {
            //     $BookingDetail['booking'][] = $payments;
            //     $BookingDetail['paymentPlan'][] = $payment->paymentPlan;
            //     $BookingDetail['PlotSize'][] = $payment->paymentPlan->plotSize;
            // }
        }

        return view('admin.payments.membersBooking',compact('BookingDetail'));
    }

    public function specialPlan(Request $request)
    {dd($request);
    }

    public  function ledgerSearch(Request $request)
    {
        $registration_no = $request->registration_no;
        $html = $this->ledger($registration_no, 1);

        $lpc = LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount, SUM(ifnull(amount_received,0)) as amount_received, SUM(ifnull(os_amount,0)) as os_amount, calculation_date'))
        ->where('registration_no', $registration_no)->first();
        
        $cancel = Payments::where('registration_no' , $registration_no)->first();
        if($cancel->is_cancelled != 0){
        $word = \App\Helpers::booking_status($cancel->is_cancelled);
        
        return response()->json(['html' => $html, 'lpc' => $lpc , 'word' => $word]);
        }
        
        else{
            return response()->json(['html' => $html, 'lpc' => $lpc]);
            
        }
    }

    public function ledger($booking_id, $isAjaxSearch = 0)
    {
        //The following two lines are commented by Habib and new code to check permission is added
         //if(!Auth::user()->getPermission('can_view_ledger'))
           // abort(403);

        $rights = Role::getrights('ledger');
        if($rights->can_view){
    
            if (strpos($booking_id, '-') !== false) {
                $booking      = Payments::where('registration_no', $booking_id)->firstorfail();
            }else{
                $booking      = Payments::find($booking_id);
            }
            
            if($booking->is_cancelled != 0){
                $word = \App\Helpers::booking_status($booking->is_cancelled);
            }
    
            $member = members::where('registration_no',$booking->registration_no)->where('is_current_owner', 1)->first();
            // dd($member);
            $person = DB::table('persons')->where('id' , $member->person_id)->first();
    
            $installments = memberInstallments::where('registration_no',$booking->registration_no)->where('is_cancelled', 0)->where('installment_amount','>',0)->orderBy('installment_no','asc')->orderBy('is_other_payment','desc')->get(); 
            
            $preferences = 0; 
            $preferences = \App\booking_featureset::where('payment_id',$booking->id)->select(DB::Raw('SUM(ifnull(fst_payment,0)) as fst_payment, SUM(ifnull(amount_received,0)) as amount_received, SUM(ifnull(os_amount,0)) as os_amount'))->first(); 
            
            $lpc = LPC::select(DB::Raw('SUM(ifnull(lpc_amount,0)) + SUM(ifnull(lpc_other_amount,0)) as lpc_amount, SUM(ifnull(amount_received,0)) as amount_received, SUM(ifnull(inst_waive,0)) lpc_waive, SUM(ifnull(os_amount,0)) - SUM(ifnull(inst_waive,0)) as os_amount'))
                        ->where('registration_no', $booking->registration_no)->first();
                        
            $lpcs = receipts::select(DB::Raw('receipts.receipt_no,receipts.receiving_date,receipts.receiving_mode,receipts.received_amount'))
                        ->where([['receipts.registration_no', $booking->registration_no],['receiving_type',5]])->orderBy('receipts.id','asc')->get();
            
            if($isAjaxSearch == 1)
            {
                return view('admin.payments._ledger', compact('booking', 'member', 'person','installments', 'bookingList','preferences', 'lpc'))->render();
            }
            
            $bookingList = Payments::select('bookings.registration_no')
            ->join('members', 'members.registration_no','=', 'bookings.registration_no')
            ->join('persons', 'members.person_id','=', 'persons.id')
            ->join('payment_schedule', 'payment_schedule.id','=', 'bookings.payment_schedule_id')
            ->join('plot_size', 'plot_size.id','=', 'payment_schedule.plot_size_id')
            ->where('bookings.status',1)
            //->where('bookings.is_cancelled',0)
            ->get();
            
            $transfer = \App\transfers::select(DB::Raw('tranfer_charges,receipt_no,receiving_date,received_amount,receiving_mode'))
                    ->leftJoin('transfer_receipts', 'transfer_receipts.transfer_id', 'transfers.id')
                    ->leftJoin('receipts', 'receipts.id', 'transfer_receipts.receipt_id')
                    ->where('transfers.reg_no', $booking->registration_no)
                    ->first();
    
            return view('admin.payments.ledger', compact('booking', 'member', 'person','installments', 'bookingList','preferences', 'lpcs', 'lpc','word', 'transfer', 'rights'));
        }
        else { return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE')); }
    }
    
    public function getFstDetail(Request $request)
    {
        if($request->ajax())
        {
            $preferences = \App\booking_featureset::join('feature_set','feature_set.id','booking_featureset.feature_set_id')
                                                ->join('featureset_receipts','featureset_receipts.featureset_id','booking_featureset.id')
                                                ->join('receipts','receipts.id','featureset_receipts.receipt_id')
                                                ->where('payment_id',$request->booking_id)
                                                ->select(DB::Raw('feature_set.feature,receipts.receipt_no,receipts.receiving_date,ifnull(fst_payment,0) as fst_payment, ifnull(amount_received,0) as amount_received, ifnull(os_amount,0) as os_amount'))
                                                ->get(); 
            return $preferences;
        }
    }

    public function generateBooking($id, Request $request)
    {
        $rights = Role::getrights('booking');

        $member = User::findorfail($id);

        $paymentSchedule = paymentSchedule::all();
        $featureset = featureset::all();
        $agents = User::Agents();
        $agencies = agency::all();
        return view('admin.payments.generateBooking',compact('member','featureset','paymentSchedule','agents','agencies','rights'));
    }

    public function editbooking($person_id)
    {
        if(!Auth::user()->getPermission('can_edit_booking'))
            abort(403);

        $person = members::find($person_id);
        $user   = User::where('id', $person_id)->first();
        return view('admin.payments.bookingedit', compact('user'));
    }

    public function updatebooking($id, Request $request)
    {

        $person = User::find($id);
        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path              = $upload_path . $filename;
            $person['picture'] = $filename;
        }
        if (Input::hasfile('cnic_pic')) {
            $image       = Input::file('cnic_pic');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $path               = $upload_path . $filename;
            $person['cnic_pic'] = $filename;
        }
        $person->save();

        return redirect('admin/booking');

    }

    /*
    public function updateBalloting()
    {
        $bookings = Payments::Active();

        $i = 1;
        foreach ($bookings as $key => $booking) {

            $inst = memberInstallments::where('registration_no', $booking->registration_no)->orderBy('id', 'desc')->first();

            if(!empty($inst))
            {
                $time = strtotime(date('Y-m-d', strtotime($inst->installment_date)));

                $balloting_date = date("Y-m-d", strtotime("+1 month", $time));

                $balloting = memberInstallments::where('registration_no', $booking->registration_no)->where('payment_desc', 'Balloting')->first();

                $balloting->installment_date = $balloting_date;

                $balloting->save();
           }
        }
        echo "Done";
    }
    */


     public function cancel($id, $registration_no = null)
    {

        if(!Auth::user()->getPermission('can_cancel_booking'))
            abort(403);
        $registration_no = \Request::get('registration_no');
        if($registration_no)
        {
            $member= members::where('registration_no' , $registration_no)->where('is_current_owner', 1)->first();
            $user =  User::where('id' , $member->person_id)->first();

            $os_inst = memberInstallments::where('registration_no', $registration_no)->where('installment_date', '<=', date('Y-m-d'))->where('is_paid','0')->get();

            $lpc = LPC::where('registration_no', $registration_no)->where('is_finalized',1)->get();

            $booking = Payments::where('registration_no', $registration_no)->where('status', 1)->first();

            $received_inst = memberInstallments::where('registration_no', $registration_no)->where('is_paid','1')->where('is_other_payment', 0)->get();
            $received_inst_sum = 0;
            foreach ($received_inst as $key => $inst) {
                $received_inst_sum += $inst->installment_amount;
            }

            $received_other = memberInstallments::where('registration_no', $registration_no)->where('is_paid','1')->where('is_other_payment', 1)->get();
            $received_other_sum = 0;
            foreach ($received_other as $key => $inst) {
                $received_other_sum += $inst->installment_amount;
            }

            // $pdc = pdc::where('registration_no',$registration_no)->where('clear_posted', 0)->get();
            // $pdc_sum = 0;
            // foreach ($pdc as $key => $p) {
            //     $pdc_sum += $p->amount;
            // }

            $alerts = alerts::where('registration_no', $registration_no)->get();

            $transfers = transfers::where('reg_no',$registration_no)->get();

            $file = assignFiles::where('registration_no', $registration_no)->first();


            if($id == 0)
                return view('admin.payments.cancel', compact('registration_no','member' , 'user', 'os_inst','lpc','same_reg', 'booking','received_inst_sum', 'received_other_sum', 'pdc_sum','alerts', 'transfers','file'));
            else
                return view('admin.payments.reactive', compact('registration_no','member' , 'user', 'os_inst','lpc','same_reg', 'booking','received_inst_sum', 'received_other_sum', 'pdc_sum','alerts', 'transfers','file'));
        }else{
            if($id == 0)
                return view('admin.payments.cancel',compact('registration_no'));
            else
                return view('admin.payments.reactive',compact('registration_no'));
        }
    }

    public function postCancel(Request $request, $registration_no)
    {
        $booking = Payments::Active()->where('registration_no', strtoupper($registration_no))->where('is_cancelled',0)->first();
        if($booking)
        {
            $booking->cancel_date = date('Y-m-d', strtotime(strtr($request->input('cancel_date'), '/', '-')));
            $booking->cancel_by = Auth::user()->id;
            $booking->is_cancelled = 1;
            $booking->cancel_remarks = $request->input('cancel_remarks');

            $booking->save();

            $request->session()->flash('message', "File has been cancelled");
            $request->session()->flash('status', 1);
        }else{
            $request->session()->flash('message', "This file is already cancelled");
            $request->session()->flash('status', 0);
        }

        $assign = assignFiles::where('registration_no', strtoupper($registration_no))->first();
        $assign->is_reserved = 0;
        $assign->save();

        return redirect()->route('booking.cancel-reactive','0?registration_no='.$registration_no);
    }

    public function reactive()
    {
        if(!Auth::user()->getPermission('can_reactive_booking'))
            abort(403);

        return view('admin.payments.reactive');
    }

    public function postReactive(Request $request, $registration_no)
    {
        $booking = Payments::Active()->where('registration_no', $registration_no)->where('is_cancelled',1)->first();
        if($booking)
        {
            $booking->reactive_date = date('Y-m-d', strtotime(strtr($request->input('reactive_date'), '/', '-')));
            $booking->reactive_by = Auth::user()->id;
            $booking->is_cancelled = 0;
            $booking->reactive_remarks = $request->input('reactive_remarks');

            $booking->save();
            $request->session()->flash('message', "File has been reactive");
            $request->session()->flash('status', 1);
        }else{
            $request->session()->flash('message', "This booking is not canclled yet.");
            $request->session()->flash('status', 0);
        }

        $assign = assignFiles::where('registration_no', $registration_no)->first();
        $assign->is_reserved = 1;
        $assign->save();

        return redirect()->route('booking.cancel-reactive','1?registration_no='.$registration_no);
    }


    public function welcomeLetter()
    {
        return view('admin.payments.welcomeLetter');
    }

    public function updatePlans()
    {
        if(!Auth::user()->can('can_update_payment_plan'))
            abort(403);

        return view('admin.payments.updatePlan');
    }

    public function storeUpdatedPlans(Request $request)
    {
        $this->validate($request, [
            'registration_no' => 'required',
            'starting_month' => 'required',
            'starting_year' => 'required',
        ]);

        // dd($request->input());
        $booking = payments::where('registration_no', $request->registration_no)->where('status', 1)->where('is_cancelled', 0)->first();
        if($booking)
        {
            // dd("here");
             //Start updating payment dates
            $memberInstallments = memberInstallments::where('registration_no', $request->registration_no)->get();
            // dd($memberInstallments);
            foreach ($memberInstallments as $key => $installment) {
                if($installment->installment_no == 1 && $installment->payment_desc == 'Installment')
                {
                    $day = 10;
                    // $month = $installment->payment_date->month;
                    $month = $request->starting_month;
                    $year = $request->starting_year;
                    // $date = Carbon::createFromDate(2017, 5, 17, 'Asia/Karachi');
                    $date = Carbon::createFromDate($year, $month, $day, 'Asia/Karachi');
                    break;
                }
            }

            // dd($date);

            $i = 1;
            $balloting_date = "";
            foreach ($memberInstallments as $key => $installment) {

                if($installment->payment_desc != 'Down Payment')
                {
                    if($installment->payment_desc == 'Balloting')
                    {
                        $balloting_date = $installment;
                        continue;
                    }
                    // dd($installment);
                    $installment_receipts = \App\Payments::getInstallmentReceipts($installment->id);
                    // dd($installment_receipts);
                    if($installment_receipts)
                    {
                        foreach($installment_receipts as $receipt)
                        {
                            $receipt->due_date = $date;
                            $receipt->save();

                        }
                    }

                    $installment->installment_date = $date;
                    $installment->save();

                    // if($installment_receipts[$key])
                    $date = $date->addMonth(1);

                }
            }

            $balloting_date->installment_date = $date;
            $balloting_date->save();
            // End updating payment dates
            $message = "Payment plan for ". $request->registration_no . " has been updated.";
            $status = 1;
        }else{
            $message = "Registration number does not exsit";
            $status = 0;
        }

        return redirect()->route('booking.updatePlans')->with(['message' => $message, 'status' => $status]);
    }

    public function attachPreference()
    {

        return view('admin.payments.attach-preference');
    }

    public function storeAttachPreference(Request $request)
    {
        // dd($request->input());
        $booking = Payments::where('registration_no', $request->registration_no)->first();
        $featureset = featureset::find($request->featureset_id);

        $memberInstallment = memberInstallments::create([
            'installment_no'      => $request->installment_no,
            'installment_amount'  => $request->due_amount,
            'due_amount'          => $request->due_amount,
            'amount_received'     => 0,
            'registration_no'     => $request->registration_no,
            'installment_date'    => date('Y-m-d', strtotime(strtr($request->input('installment_date'), '/', '-'))),
            'payment_schedule_id' => $booking->payment_schedule_id,
            'payment_desc'        => $featureset->feature,
            'is_other_payment'    => 2,
        ]);

        return redirect()->back()->with(['message' => 'Preference is attached with the ledger.', 'status' => 1]);

    }

    public function getPreferenceAmount(Request $request)
    {
        $featurest = featureset::find($request->featureset_id);
        $booking = Payments::with(['paymentPlan'])->where('registration_no', $request->registration_no)->first();

        $total_amount = $booking->paymentPlan->total_price;

        $getPercentage = $featurest->charges / 100 * $total_amount;

        return response()->json(['due_amount' => $getPercentage, 'status' => 200]);
    }
}
