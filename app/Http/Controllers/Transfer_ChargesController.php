<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\TransferCharges;
use App\paymentSchedule;
use App\plotSize;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Transfer_ChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('transfer-charges');
        
        if(!$rights->can_view){
    	    abort(403);
        }
		$TransferCharges = TransferCharges::all();
        return view('admin.transfer_charges.index',compact('TransferCharges','rights'));
    }

  
	public function create()
	{
		//$paymentSchedule = paymentSchedule::all();
		
		$rights = Role::getrights('transfer-charges');
        
        if(!$rights->can_create){
    	    abort(403);
        }
		/*$paymentSchedule = paymentSchedule::join('plot_size','plot_size.id','=','payment_schedule.plot_size_id')
		->whereNull('payment_schedule.deleted_at')->whereNull('plot_size.deleted_at')->get();*/
		
		$paymentSchedule = plotSize::whereNull('deleted_at')->get();

		return view('admin.transfer_charges.create',compact('paymentSchedule'));
	}  
	
	public function store(Request $request)
	{
		$this->validate($request, [
		    'charges' => 'required|numeric',
		    'plot_size_id' => 'required|numeric'
		]);

		$input = $request->input();
		$input['created_by'] = Auth::user()->id;

		$tCharges = TransferCharges::create($input);
		return redirect()->route('transfer_charges.index');
	}

	public function edit($id)
	{
	    $rights = Role::getrights('transfer-charges');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
		$transferCharges = TransferCharges::find($id);
		$paymentSchedule = plotSize::whereNull('deleted_at')->get();
		return view('admin.transfer_charges.edit', compact('transferCharges', 'paymentSchedule'));
	}
            

    public function update(Request $request, $id)
	{
		$this->validate($request, [
		    'charges' => 'required|numeric',
		    'plot_size_id' => 'required|numeric'
		]);

		$input = $request->input();
		$input['updated_by'] = Auth::user()->id;

		$tCharges = TransferCharges::find($id);

		$tCharges->update($input);

		return redirect()->route('transfer_charges.index');
	}
    

    public function destroy($id)
    {
    	$tCharges = TransferCharges::find($id);
    	$tCharges->deleted_by = Auth::user()->id;
    	$tCharges->save();
    	$tCharges->delete();

    	return redirect()->route('transfer_charges.index');
    }
}
