<?php

namespace App\Http\Controllers;

use App\alerts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $input = $request->input();
            $input['created_by'] = Auth::user()->id;
            alerts::create($input);
            $alerts = alerts::where('registration_no', $input['registration_no'])->get();
            $html = view('admin.alerts.render', compact('alerts'))->render();
            return response()->json( array('success' => true, 'html'=>$html) );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }

    public function delete($id)
    {
        $alert = alerts::find($id);
        $reg = $alert->registration_no;
        $alert->delete();
        return redirect('admin/member/inquiry/'.$reg)->with(['message' => 'Alert removed', 'status', 1]);   
    }

    public function checkAlerts(Request $request)
    {
        if($request->ajax())
        {
            $status = 0;
            $alerts = alerts::where('registration_no',$request->input('registration_no'))->orderBy('id','DESC')->whereNull('deleted_at')->get();
            if($alerts)
                $status = 1;    
            return ['status' => $status, 'alert' => $alerts];
        }
    }
}
