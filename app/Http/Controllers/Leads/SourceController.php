<?php

namespace App\Http\Controllers\Leads;

use App\Http\Controllers\Controller;
use App\Models\Leads\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\Agency\agency;
use App\Models\Agency\agents;
use App\Models\Agency\principal_officer;
use App\Traits\CustomFunctions;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Role;
use App\User;

class SourceController extends Controller
{
use CustomFunctions;
    // use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        
        $rights = Role::getrights('sources');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            

        if($rights->can_view){
             
            $lsources = Source::whereNull('deleted_at')->get();
           
            return view('admin.leads.sources.index', compact('lsources', 'rights'));

        }else {
             return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $rights = Role::getrights('sources');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            

        if($rights->can_view){
             return view('admin.leads.sources.create');
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:sources',
        ])->validate();
        
        $input = $request->input();
      
        $input['created_by'] = Auth::user()->id;

        // $input['agency_id'] = $this->getAgency()->id;
        Source::create($input);
        
        return redirect()->route('leads.sources')->with(['message' => 'Source Added Successfully', 'status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('sources');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            

        if($rights->can_view){
             $lead = Source::findorfail($id);
        return view('admin.leads.sources.edit', compact('lead'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $source = Source::findorfail($id);

        Validator::make($request->all(), [
            'name' => Rule::unique('sources')->where(function ($query) use($id) {
                return $query->whereNull('deleted_at')->where('id','!=',$id);
            }),
        ])->validate();

        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        $source->update($input);
        
        return redirect()->route('leads.sources')->with(['message' => 'Source updated succesfully.', 'status' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rights = Role::getrights('sources');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            

        if($rights->can_view){
            
            $lead = Source::findorfail($id);

            $lead->delete();
    
            return redirect()->route('leads.sources')->with(['message' => 'Source deleted successfully', 'status' => 'success']);
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        

        
    }
}
