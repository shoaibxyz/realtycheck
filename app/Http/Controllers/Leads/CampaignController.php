<?php

namespace App\Http\Controllers\Leads;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Leads\Source;
use App\Models\Leads\Campaign;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Traits\CustomFunctions;
use Mail;
use App\Role;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;
use Validator;

class CampaignController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('campaign');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
        
            $campaign = Campaign::whereNull('deleted_at')->get();
            //dd($campaign);
            return view('admin.leads.campaign.index', compact('campaign', 'rights'));
        }
        else
    	    abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('campaign');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

      if($rights->can_view)
        {
        
            $lsources  = Source::whereNull('deleted_at')->get();
            return view('admin.leads.campaign.create', compact('lsources', 'rights'));
        }else
    	    abort(403);
    	    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:campaigns',
        ])->validate();
        
        $input = $request->input();
        $input['created_by'] = Auth::user()->id;
        Campaign::create($input);
        
        Session::flash('status', 'success');
        Session::flash('message', 'Campaign has been added successfully.');

        return redirect()->route('leads.campaign')->with(['message' => 'Campaign Added Successfully', 'status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $rights = Role::getrights('campaign');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            
        if ($rights->can_edit) {

            $lsources = Source::whereNull('deleted_at')->get();
            $campaign = Campaign::findorfail($id);
            return view('admin.leads.campaign.edit', compact('lsources' , 'campaign'));
        }else{
             abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campaign = Campaign::findorfail($id);
        
        Validator::make($request->all(), [
            'name' => Rule::unique('campaigns')->where(function ($query) use($id) {
                return $query->whereNull('deleted_at')->where('id','!=',$id);
            }),
        ])->validate();
       
        $input = $request->input();
        
        $campaign->update($input);
        
        Session::flash('status', 'success');
        Session::flash('message', 'Campaign has been updated successfully.');

        return redirect()->route('leads.campaign')->with(['message' => 'Campaign updated succesfully.', 'status' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $campaign = Campaign::findorfail($id);

        $campaign->delete();
        
        Session::flash('status', 'success');
        Session::flash('message', 'Campaign has been deleted successfully.');

        return redirect()->route('leads.campaign')->with(['message' => 'Campaign deleted successfully', 'status' => 'success']);
    }
    
    public function findcampaign(Request $request)
    {
        $campaign = campaign::where('source_id', $request->input('id'))->where('agency_id' , $this->getAgency()->id)->get();

        $html = "<select class='form-control select2' id='campaign' name='campaign'>";
        $html .= "<option value=''>Select Campaign</option>";
        foreach($campaign as $state)
        {
            $html .= "<option value='".$state->id."'>".$state->name."</option>";
        }
        $html .= "</select>";

        return response()->json(['campaign' => $html]);
    }
}
