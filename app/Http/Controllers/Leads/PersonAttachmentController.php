<?php

namespace App\Http\Controllers\Leads;

use App\Http\Controllers\Controller;
use App\Http\Requests\uploadAttachmentRequest;
use App\Models\Leads\Client;
use App\Models\Leads\UserAttachment;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PersonAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        
        $rights = Role::getrights('customer_attachment');
      
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            
            $customer = User::find($id);
            return view('admin.leads.user-attachments.create', compact('customer'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(count($request->files) > 0)
        {
            foreach ($request->files as $files) {
                foreach ($files as $key => $file) {
                    $filename    = Auth::user()->id .'-'. uniqid() . '-' . $file->getClientOriginalName();
                    $store = Storage::putFileAs('leads/attachments', $request->file('attachment')[$key],$filename);
                    UserAttachment::create([
                        'person_id' => $request->person_id,
                        'attachment' => $filename,
                        'created_by' => Auth::user()->id,
                        'title' => $request->title,
                        'notes' => $request->notes,
                    ]);
                }
            }
        }
        else
        {
            UserAttachment::create([
                'person_id' => $request->person_id,
                'created_by' => Auth::user()->id,
                'title' => $request->title,
                'notes' => $request->notes,
            ]);
        }    

        $client = Client::where('person_id', $request->person_id)->first();
        // UserAttachment::create($input);

        return redirect()->route('clients.show', $client->id)->with(['message' => 'Attachment Added', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\Auth::user()->can(('can_edit_customer_attachment')))
            abort(403);

        $attachment = UserAttachment::with(['user'])->find($id);

        return view('admin.leads.user-attachments.edit', compact('attachment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = UserAttachment::find($id);

        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        
        if (Input::hasfile('logo')) {
            $image       = Input::file('logo');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $input['logo'] = $filename;
        }

        $project->update($input);

        return redirect()->route('user-attachment.index')->with(['message' => 'Project Updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attachment = UserAttachment::find($id);

        $personId = $attachment->person_id;
        $attachment->delete();
        
        $client = Client::where('person_id', $personId)->first();
        return redirect()->route('clients.show', $client->id)->with(['message' => 'Attachment Deleted']);
    }
}
