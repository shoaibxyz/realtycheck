<?php

namespace App\Http\Controllers\Leads;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use App\Jobs\LeadsEmailJob;
use App\Mail\LeadsEmail;
use App\agents;
use App\principal_officer;
use App\Models\Country;
use App\Models\Helpers;
use App\Models\Leads\Campaign;
use App\Models\Leads\Client;
use App\Models\Leads\lead;
use App\Models\Leads\LeadHistory;
use App\Models\Leads\LeadReminder;
use App\Models\Leads\LeadStatus;
use App\Models\Leads\PropertySubtypes;
use App\Models\Leads\PropertyType;
use App\Models\Leads\Source;
use App\Models\Leads\projects;
use App\User;
use App\Traits\CustomFunctions;
use App\Traits\SMS;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use App\Role;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\HR\Employees;
use Spatie\Activitylog\Traits\LogsActivity;
use File;

class LeadsController extends Controller
{

    use SMS;
    use CustomFunctions;
    use LogsActivity;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $rights = Role::getrights('Lead');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            $canAssign  = Auth::user()->can('can_assign_lead');
            $leadStatus = DB::table('lead_status')->whereNull('deleted_at')->get();
            $lead = DB::table('leads')->whereNull('deleted_at')->get();
            $principal = principal_officer::all();
            $sale_officers = Employees::whereNull('deleted_at')->get();
            $projects = projects::where('status', 1)->whereNull('deleted_at')->get();
            $assign_to_html = "<select name='next_meeting_by' class='form-control assignTo' id='assignTo' style='width: 200px;'>";
            $assign_to_html .= "<option value=''>Select One</option>";
    
            foreach ($sale_officers as $officer) {
                if($officer->user){
                    $selected = ($officer->user->id == Auth::user()->id) ? "selected" : "";
                    $assign_to_html .= "<option value='" . $officer->user->id . "' " . $selected . ">" . $officer->user->name . " - agent </option>";
                }
            }
    
            $assign_to_html .= "</select>";
            
             $routeName = (\Request::route()->getName() != "leads.index") ? true : false;

            if($routeName)
                return view('admin.leads.dueLeads', compact('canAssign', 'leadStatus', 'assign_to_html', 'rights', 'lead'));
    
            return view('admin.leads.index', compact('canAssign', 'leadStatus', 'assign_to_html', 'rights', 'lead'));
            
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }

    public function closeddashboard()
    {
        return view('admin.leads.closeddashboard');
    }

    public function dashboard()
    {
        $rights = Role::getrights('dashboard');
        //dd(Session::get('objects'));
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
              return view('admin.leads.dashboard');
        }
        else
        {
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }

    public function getLeads(Request $request)
    {
        $rights = Role::getrights('Lead');
        
        $search = $request->input('query');
        $lead_id = $request->input('lead_id');
        $status = $request->input('status');
        $mobile_no = $request->input('moblie_no');
        $leadtype = $request->input('leadtype');
        
        $length = $request->input('length');
        $start = $request->input('start');

        $routeName = \Request::route()->getName();
        
           $leads = DB::table('leads')
                
                ->join('persons', 'persons.id', '=', 'leads.person_id')
                
                ->leftJoin('lead_status', 'lead_status.id', '=', 'leads.lead_status_id')
                ->leftJoin('lead_campaigns', 'lead_campaigns.lead_id', '=', 'leads.id')
                ->leftJoin('campaigns', 'campaigns.id', '=', 'lead_campaigns.campaign_id')
                
                ->when($mobile_no, function($q) use ($mobile_no){
                    $q->where('persons.phone_mobile', 'like', '%' . $mobile_no . '%');
                })
                
                 ->when($search, function($q) use ($search){
                      //  $q->where('persons.name', $search);
                    $q->where('persons.name', 'like', '%' . $search . '%');
                 })

                ->when($lead_id, function($q) use ($lead_id){
                   // $q->where('lead_id', $lead_id);
                    $q->where('leads.id', 'like', '%' . $lead_id . '%');
                })

                ->when($status, function($q) use ($status){
                    $q->where('leads.lead_status_id', $status);
                })
                /*->where('lead_status.name','=','New')*/
                ->whereNull('leads.deleted_at')->orderBy('leads.id', 'DESC')
                ->select(DB::Raw('leads.budget,leads.comments,leads.interest, persons.id, persons.phone_mobile, persons.name as user_name, persons.client_type as clientType, lead_status.name as status_name,  leads.id as lead_id,leads.created_at as created_by, leads.assigned_to ,leads.assigned_date, campaigns.name as campaign_name'))
                ->get();
                
    
        if ($request->android == 1) {
            $assignedToArr = [];
            foreach ($leads as $lead) {
                $assignedTo      = $lead->assigned_to;
                $assignedToArr[] = ($assignedTo != null) ? User::getUserInfo($assignedTo) : null;
            }
            return response()->json(['leads' => $leads, 'assignedTo' => $assignedToArr]);
        }
        
        //dd($leads);

        $data = Datatables::of($leads)

            ->addColumn('actions', function ($leads) {

                $canView   = Auth::user()->can(('can_view_leads'));
                $canDelete = Auth::user()->can(('can_delete_leads'));
                $canEdit   = Auth::user()->can(('can_edit_leads'));
                $canAssign = Auth::user()->can(('can_assign_lead'));
                $rights = Role::getrights('Lead');
                
                
                $html = "";
                if ($rights->can_view) {
                    $html .= '<a class="btn btn-xs btn-primary viewLead" data-toggle="modal" href="#viewLeadModel" title="View" data-leadid=' . $leads->lead_id . '><i class="fa fa-eye"></i></a>';
                }else{
                    $html = $canView;
                }

                if ($rights->can_edit) {
                    $html .= '<a href="' . route('leads.edit', $leads->lead_id) . '" class="btn btn-xs btn-warning edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                }

                if ($rights->can_delete) {
                    $html .= '<form method="post" style="display: inline" action="' . route('leads.destroy', $leads->lead_id) . '">' . csrf_field() . "<input name='_method' type='hidden' value='DELETE'> <button type='submit' class='btn btn-danger btn-xs DeleteBtn delete' data-toggle='tooltip' title='Delete'> <i class='fa fa-times'></i></button></form>";
                }
                if (Role::getrights('lead_history')->can_view) {
                    $html .= '<a class="btn btn-xs btn-info viewLeadHistory" data-toggle="modal" href="#viewLeadHistory" data-leadid=' . $leads->lead_id . '>History</a>';
                }

                if (Role::getrights('leadstatus')->can_view) {
                    $html .= '<a class="btn btn-xs btn-default viewFU" data-toggle="modal" href="#updateLeadModel" data-leadid=' . $leads->lead_id . '>Followup</a>';
                }

                return $html;
            })

            ->editColumn('id', function($leads){
                return $leads->lead_id;
            })

            ->addColumn('status', function ($leads) {
                
                    return $leads->status_name;
                
            })

            ->editColumn('assigned_date', function ($leads) {
                if (!$leads->assigned_date) {
                    return "";
                }

                return date('d-m-Y', strtotime($leads->assigned_date));
            })
            ->addColumn('name', function ($leads) {
                // return $leads->status->name;
                return $leads->user_name;
            })
            ->addColumn('phone_mobile', function ($leads) {
                return $leads->phone_mobile;
            })

            ->editColumn('interest', function ($leads) {
                 
                   return $leads->interest;
                
                
            })

            ->editColumn('checkbox', function ($leads) {
                return '<input type="checkbox" name="selector[]" class="dt-checkboxes" value='.$leads->lead_id.'>' ;
            })

            ->editColumn('campaign_name', function ($leads) {
                
                    return $leads->campaign_name;
                
            })

            ->addColumn('assigned_to', function ($leads) {
                if (!$leads->assigned_to) {
                    return "";
                }

                $user = User::find($leads->assigned_to);
                if($user){
                return ($leads->assigned_to != "") ? "<a href='#assignedLeads' class='assignedLeads' id='lead_" . $leads->lead_id . "' data-userid='" . $user->id . "' data-toggle='modal'>" . $user->name . "</a>" : "";
            }else{
                return "";
            }
            })

            ->addColumn('query', function ($leads) {
                return $leads->comments;
            })

             ->rawColumns(['actions', 'assign', 'assigned_to', 'checkbox'])
            ->make(true);
            
            //dd($data);

        return $data;
    }

    public function leadStatus($status = null)
    {
        $rights = Role::getrights('leadstatus');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            
            $leads = lead::with(['User', 'project', 'status', 'sources', 'propertyType', 'PropertySubtypes', 'assignedBy', 'assignedTo'])->whereNull('deleted_at')->where('lead_status_id', $status)->get();
            $leadStatus = DB::table('lead_status')->where('id', $status)->whereNull('deleted_at')->get();
         
            if (\Request::input('android') == 1) {
                return response()->json(['leads' => $leads, 'leadStatus' => $leadStatus]);
            }
    
            return view('admin.leads.leadStatus', compact('leads', 'leadStatus', 'rights'));
            
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }   
    }
    
    public function details(){
        
        $persons                    = User::all();
        $employees                  = Employees::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('admin.leads.details', compact('persons', 'employees'));
    }
    
    public function persondetails($id){
        
        // $leads = Lead::where('assigned_to', $id)->whereNull('deleted_at')->get();
        $leads = lead::with(['User', 'project', 'status', 'sources', 'propertyType', 'PropertySubtypes', 'History'])->where('leads.assigned_to', $id)->get();
 
        return view('admin.leads.persondetails', compact('leads'));
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('Lead');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            $countries     = Country::all();
            $propertyTypes = PropertyType::all();
            $LeadSource = Source::whereNull('deleted_at')->get();
            $campaigns = Campaign ::whereNull('deleted_at')->get();
            $projects   = projects::whereNull('deleted_at')->get();
       
            return view('admin.leads.create', compact('rights', 'countries', 'propertyTypes', 'LeadSource','campaigns', 'projects'));
        }
        else
        {
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
    }

    public function getPropertySubType(Request $request)
    {
        $stypes = PropertySubtypes::where('property_type_id', $request->input('property_type_id'))->get();

        $html = "";
        $html .= "<option value=''>Select Property Sub Type</option>";
        foreach ($stypes as $type) {
            $html .= "<option value='" . $type->id . "'>" . $type->name . "</option>";
        }

        if ($request->input('android') == 1) {
            return response()->json(['stypes' => $stypes]);
        }

        return response()->json(['html' => $html]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
   
    public function store(Request $request)
    {
        if (!$request->customer_id) {
            $user                    = new User();
           
            $user->name              = $request->input('name');
            $user->username          = $request->input('name');
            $user->email             = $request->input('email');
            $user->phone_res         = $request->input('phone');
            $user->phone_mobile      = $request->input('mobile');
            $user->current_address   = $request->input('address');
            $user->permanent_address = $request->input('address');
            $user->zip_code          = $request->input('zip_code');
            $user->whatsapp_number   = $request->input('whatsapp_number');
            $user->country_id        = $request->input('country_id');
            $user->password          = 123456;
            $user->created_by        = Auth::user()->id;
            $user->save();

            //13 = Customer
            $user->Roles()->attach(13);

            $person = $user->id;

            $client                = new Client();
            $client->person_id     = $user->id;
            $client->client_type = $request->client_type;
            $client->client_rating = $request->client_rating;
            $client->created_by    = Auth::user()->id;
            $client->save();

        } else {
            $person = $request->customer_id;
        }

        $input = $request->input();
        
        
        if (!$request->android) {
            $input['created_by'] = Auth::user()->id;
        } else {
            $input['created_by'] = JWTAuth::toUser($request->input('token'))->id;
        }

        $input['person_id']      = $person;
        $input['interest']       = $request->interest;
        $input['assigned_to'] = Auth::user()->id;
        $input['assigned_date'] = date('Y-m-d');
        $input['assigned_by'] = Auth::user()->id;
        $input['lead_status_id'] = LeadStatus::where('name', 'New')->first()->id;
        

$input['visit_promise_date']=date('Y-m-d',strtotime($request->visit_promise_date));

$input['whatsapp_sent_on']=date('Y-m-d',strtotime($request->whatsapp_sent_on));


        $lead = lead::create($input);
        
       $lead->campaigns()->attach($request->input('campaign_id'));
        
        // $lead->sources()->attach($request->input('source_id'));

        // $receiver = User::CCD()->pluck('email')->toArray();
        // $content  = Auth::user()->name . " has created a new lead";

        // $job = (new LeadsEmailJob($receiver, $content, $lead->id, 'updated'))->onQueue('lead');
        // dispatch($job);

        // $this->SendNotification(env('NOFICATIONS_TITLE'), "New lead generated", 'call-center');

        if ($request->android == 1) {
            return response()->json(['message' => 'Lead added successfully.', 'status' => 1]);
        }


        return redirect()->route('leads.index')->with(['message' => 'Lead Generated', 'status' => 'success']);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $lead = lead::with(['User', 'project', 'status', 'sources', 'propertyType', 'PropertySubtypes', 'assignedBy', 'assignedTo'])->findorfail($id);
      
        $leadHistory = [];
        $history     = LeadHistory::with(['User', 'status', 'meetingBy'])->where('lead_id', $id)->orderBy('id', 'DESC')->get();
        if (count($history) > 0) {
            $leadHistory = $history;
        }
       
        $leadSource = Source::where('id', $lead->source_id)->first();

        $next_meeting_By = [];

        foreach ($leadHistory as $lf) {
            $next_meeting_by   = $lf->next_meeting_by;
            $next_meeting_By[] = ($next_meeting_by != null) ? User::getUserInfo($next_meeting_by) : "";
        }
        
        
        return response()->json(['lead' => $lead, 'history' => $leadHistory  , 'next_meeting_By' => $next_meeting_By, 'leadSource' => $leadSource ]);
    }
    
    public function view($id)
    {
        $lead = lead::with(['User', 'project', 'status', 'sources', 'campaigns', 'propertyType', 'PropertySubtypes', 'assignedBy', 'assignedTo'])->findorfail($id);
        
        $leadSource = Source::where('id', $lead->source_id)->first();
        $classify = '';
        $pp = '';
        $ptype = '';
        $beds = '';
        $unit = '';
        
        $country = \App\Models\Country::find($lead->User->country_id);
        
        if($country == null){
            $country == '';
        }else{
            $country = $country->name;
        }
        $client_type = '';
        foreach(\App\Helpers::Classification() as $key => $classification)
        {
            if($key == $lead->classification)
                $classify = $classification;
        }
        if($classify = "Select Classification"){
            $classify = '';
        }
        foreach(\App\Helpers::Purpose() as $key => $purpose)
        {
            if($key == $lead->purpose)
                $pp = $purpose;
        }
        if($pp = "Select Purpose"){
            $pp = '';
        }
        foreach(\App\Helpers::Beds() as $key => $bed)
        {
            if($key == $lead->beds)
                $beds = $bed;
        }
        if($beds = "Select Beds"){
            $beds = '';
        }
        foreach(\App\Helpers::LandUnits() as $key => $unit)
        {
            if($key == $lead->land_unit)
                $unit = $unit;
        }
        $source = Source::find($lead->source_id);
        $client = Client::where('person_id',$lead->person_id)->first();
        foreach(\App\Helpers::ClinetTypes() as $key => $client_type)
        {
            if($client->client_type and $key == $client->client_type)
                $client_type = $client_type;
        }
        $camps = '';
        foreach($lead->campaigns as $camp)
            $camps .= $camp->name . ', ';
        $camps = substr($camps,0,strlen($camps)-2);
        //dd($lead->project);
        // dd($lead->propertyType->name);
        if($lead->property_type_id == null){
            $propertyType_name = '';
        }else{
            $propertyType_name  = $lead->propertyType->name;
        }
        if($lead->property_subtype_id == null){
            $propertySubType_name = '';
        }else{
            $propertySubType_name  = $lead->PropertySubtypes->name;
        }
        if($lead->source_id == null){
            $source_name = '';
        }else{
            $source_name  = $source->name;
        }
        if($lead->source_id == null){
            $source_name = '';
        }else{
            $source_name  = $source->name;
        }
        if($lead->project_id == null){
            $project_name = '';
        }else{
            $project_name  = $lead->project->name;
        }
        if($lead->land_min_area == null){
            $land_min_area = '';
        }else{
            $land_min_area  = $lead->land_min_area;
        }
        if($lead->land_max_area == null){
            $land_max_area = '';
        }else{
            $land_max_area  = $lead->land_max_area;
        }
        if($lead->land_max_area == null && $lead->land_min_area == null){
            $unit = '';
        }
        $str = '<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Lead Info</h2>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Classification</label>
                        <div class="col-sm-7 val"> '. $classify .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Purpose</label>
                        <div class="col-sm-7 val">'. $pp .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Property Type</label>
                        <div class="col-sm-7 val">'. $propertyType_name .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Property Sub Type</label>
                        <div class="col-sm-7 val">'. $propertySubType_name .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Source</label>
                        <div class="col-sm-7 val">'. $source_name .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Campaign</label>
                        <div class="col-sm-7 val">'. $camps .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Projects</label>
                        <div class="col-sm-7 val">'. $project_name .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Beds</label>
                        <div class="col-sm-7 val">'. $beds .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Land</label>
                        <div class="col-sm-7 val">'. $land_min_area . ' - ' . $land_max_area . ' ' . $unit .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Interest</label>
                        <div class="col-sm-7 val">'. $lead->interest .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Visit Promise Date</label>
                        <div class="col-sm-7 val">'. date('d/m/y',strtotime($lead->visit_promise_date)) .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Budget</label>
                        <div class="col-sm-7 val">'. $lead->budget .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Whatsapp Sent On</label>
                        <div class="col-sm-7 val">'. date('d/m/y',strtotime($lead->whatsapp_sent_on)) .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Customer Feedback</label>
                        <div class="col-sm-7 val">'. $lead->customer_feedback_after_visit .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Comments</label>
                        <div class="col-sm-7 val">'. $lead->comments .'</div>
                    </div>
                    <div class="col-sm-6">
                        
                    </div>
                </div>
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Customer Info</h2>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Name</label>
                        <div class="col-sm-7 val">'. $lead->User->name .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Email</label>
                        <div class="col-sm-7 val">'. $lead->User->email .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Phone</label>
                        <div class="col-sm-7 val">'. $lead->User->phone_res .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Mobile</label>
                        <div class="col-sm-7 val">'. $lead->User->phone_mobile .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Whatsapp Number</label>
                        <div class="col-sm-7 val">'. $lead->User->whatsapp_number .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">CNIC</label>
                        <div class="col-sm-7 val">'. $lead->User->cnic .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Address</label>
                        <div class="col-sm-7 val" style="line-height:1em;">'. $lead->User->permanent_address .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Zip Code</label>
                        <div class="col-sm-7 val">'. $lead->User->zip_code .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Country</label>
                        <div class="col-sm-7 val">'. $country .'</div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Client Type</label>
                        <div class="col-sm-7 val">'. $client_type .'</div>
                    </div>
                </div>
                <div class="row form-horizontal" style="margin-bottom:2px;">
                    <div class="col-sm-6">
                        <label class="col-sm-5 control-label">Client Rating</label>
                        <div class="col-sm-7 val">'. $client->client_rating .'</div>
                    </div>
                    <div class="col-sm-6">
                        
                    </div>
                </div>
                ';    

        return response()->json($str);
    }

    public function showUserLeads($user_id)
    {
        $leads = lead::with(['User', 'project', 'status', 'sources', 'propertyType', 'PropertySubtypes'])->where('leads.assigned_to', $user_id)->get();
        $user  = User::find($user_id);
        return response()->json(['leads' => $leads, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lead = lead::findorfail($id);
    
        $countries     = Country::all();
        $propertyTypes = PropertyType::all();

        $propertySubTypes = PropertySubtypes::where('property_type_id', $lead->property_type_id)->get();
 
            $LeadSource = Source::whereNull('deleted_at')->get();
            $projects   = projects::whereNull('deleted_at')->get();
            $campaign   = Campaign::whereNull('deleted_at')->get();
    

        $client = Client::where('person_id', $lead->person_id)->first();

        return view('admin.leads.edit', compact('lead', 'countries', 'propertyTypes', 'LeadSource', 'projects', 'propertySubTypes', 'campaign', 'client'));
    }

    public function getHelper($key = null)
    {

        if ($key) {
            $classifications = Helpers::Classification($key);
        } else {
            $classifications = Helpers::Classification();
        }

        dd($classifications);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lead = lead::find($id);

        $lead->user->name              = $request->input('name');
        $lead->user->username          = $request->input('name');
        $lead->user->email             = $request->input('email');
        $lead->user->phone_res         = $request->input('phone');
        $lead->user->phone_mobile      = $request->input('mobile');
        $lead->user->current_address   = $request->input('address');
        $lead->user->permanent_address = $request->input('address');
        $lead->user->zip_code          = $request->input('zip_code');
        $lead->user->country_id        = $request->input('country_id');
        $lead->user->password          = 123456;
        $lead->user->save();

        $input = $request->input();
   
        if (!$request->android) {
            $input['updated_by'] = Auth::user()->id;
        } else {
            $input['updated_by'] = JWTAuth::toUser($request->input('token'))->id;
        }

        $input['whatsapp_sent_on'] = date('Y-m-d', strtotime(strtr($request->whatsapp_sent_on, '/', '-')));
        $input['visit_promise_date'] = date('Y-m-d', strtotime(strtr($request->visit_promise_date, '/', '-')));
        // dd($input);
        $lead->update($input);

        $client = Client::where('person_id', $lead->user->id)->first();
        if ($client) {
            $client->client_rating   = $request->client_rating;
            $client->local_interest  = $request->local_interest;
            $client->future_interest = $request->future_interest;
            $client->client_type     = $request->client_type;
            $client->save();
        }
        
        //if($request->input('campaign' != NULL)){
        $campaign = $request->input('campaign') ? $request->input('campaign') : [];
        $lead->campaigns()->detach();
        $lead->campaigns()->attach($campaign);
//}
        return redirect()->route('leads.index')->with(['message' => 'Lead Information updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lead             = lead::findorfail($id);
        $lead->delete();

        return redirect()->route('leads.index')->with(['message' => 'Lead deleted successfully']);
    }

    public function deletedLeads()
    {
        
        $rights = Role::getrights('deletedlead');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
      /*  if($rights->can_view){
            $leads = lead::where('deleted_at', '!=', "NULL")->get();
        } */
        
         if($rights->can_delete){
            $leads = lead::onlyTrashed()->get();
            return view('admin.leads.deleted', compact('leads', 'rights'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
            
    
       // return view('admin.leads.deleted', compact('leads', 'rights'));
    }

    public function restoreLeads(Request $request, $id)
    {
        //$lead = lead::where('deleted_at', '!=', "NULL")->find($id);
        
        $lead = lead::onlyTrashed()->find($id);
        $lead->restore();

        return redirect()->route('lead.deleted');
    }

    public function search()
    {
       $rights = Role::getrights('Lead');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            
        $countries     = Country::all();
        $propertyTypes = PropertyType::all();
        $LeadStatus    = DB::table('leads')->whereNull('deleted_at')->get();
       
        $LeadSource = Source::whereNull('deleted_at')->get();
        $projects   = projects::whereNull('deleted_at')->get();


        return view('admin.leads.search', compact('countries', 'propertyTypes', 'LeadSource', 'projects', 'LeadStatus', 'rights'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    public function searchLead(Request $request)
    {
        //Lead information
        $rights = Role::getrights('Lead');
        $classification      = $request->classification;
        $purpose             = $request->purpose;
        $property_type_id    = $request->property_type_id;
        $property_subtype_id = $request->property_subtype_id;
        $beds                = $request->beds;
        $lead_id             = $request->lead_id;
        $budget              = $request->budget;
        $lead_status_id      = $request->lead_status_id;
        $project_id          = $request->project_id;
        $lead_from           = date('Y-m-d', strtotime(strtr($request->lead_from, '/', '-'))) . ' 00:00';
        $lead_to             = date('Y-m-d', strtotime(strtr($request->lead_to, '/', '-'))) . ' 23:59';

        if (($request->lead_from != null && $request->lead_to == null) || ($request->lead_from == null && $request->lead_to != null)) {
            return redirect()->back()->with(['message' => 'Please select lead from and to date', 'status' => 0]);
        }

        //dd($lead_status_id );

        //Client information
        $name            = $request->name;
        $phone           = $request->phone;
        $email           = $request->email;
        $mobile          = $request->mobile;
        $address         = $request->address;
        $zip_code        = $request->zip_code;
        $future_interest = ($request->future_interest) ? 1 : 0;
        $local_interest  = ($request->local_interest) ? 1 : 0;
       

        $leads = DB::table('leads')->select(DB::Raw('persons.id, persons.name, persons.client_type, lead_status.name as status_name, projects.name as project_name, leads.id as lead_id,leads.created_at, leads.assigned_to, leads.budget,persons.phone_mobile,leads.assigned_date, leads.assigned_to, leads.created_at as lead_created_at', 'leads.lead_status_id'))
            ->leftjoin('persons', 'persons.id', '=', 'leads.person_id')
            ->leftjoin('lead_status', 'lead_status.id', '=', 'leads.lead_status_id')
            ->leftjoin('lead_sources', 'lead_sources.lead_id', '=', 'leads.id')
            ->leftjoin('projects', 'projects.id', '=', 'leads.project_id')
            ->when($classification, function ($query) use ($classification) {
                return $query->where('leads.classification', $classification);
            })
            ->when($purpose, function ($query) use ($purpose) {
                return $query->where('purpose', $purpose);
            })
            ->when($property_type_id, function ($query) use ($property_type_id) {
                return $query->where('property_type_id', $property_type_id);
            })
            ->when($property_subtype_id, function ($query) use ($property_subtype_id) {
                return $query->where('property_subtype_id', $property_subtype_id);
            })
            ->when($beds, function ($query) use ($beds) {
                return $query->where('beds', $beds);
            })
            ->when($lead_id, function ($query) use ($lead_id) {
                return $query->where('lead_id', $lead_id);
            })
            ->when($project_id, function ($query) use ($project_id) {
                return $query->where('project_id', $project_id);
            })
            ->when($lead_status_id, function ($query) use ($lead_status_id) {
                return $query->where('lead_status_id', $lead_status_id);
            })
           
            ->when($budget, function ($query) use ($budget) {
                $checkBudget = '';
                foreach (\App\Helpers::Budget() as $key => $budg) {
                    if ($key == $budget) {
                        $checkBudget = explode('to', str_replace(" ", "", $budg));
                        break;
                    }
                }
                $minBudget = $checkBudget[0];
                if (isset($checkBudget[1])) {
                    $maxBudget = $checkBudget[1];
                    return $query->whereBetween('budget', [$minBudget, $maxBudget]);
                } else {
                    return $query->where('budget', '<=', $minBudget);
                }
            })
            ->when($lead_from, function ($query) use ($lead_from, $lead_to) {
                if ($lead_from != "1970-01-01 00:00") {
                    return $query->whereBetween('leads.created_at', [$lead_from, $lead_to]);
                }

            })

        // Clients
            ->when($name, function ($query) use ($name) {
                $query->where('persons.name','like', '%'.$name.'%' );
            })

            ->when($email, function ($query) use ($email) {
                $query->where('persons.email', $email);
            })

            ->when($phone, function ($query) use ($phone) {
                $query->where('persons.phone_res', $phone);
            })

            ->when($mobile, function ($query) use ($mobile) {

                $query->where('persons.phone_mobile', $mobile);
            })

            ->when($address, function ($query) use ($address) {
                $query->where('persons.permanent_address', $address);
                $query->orWhere('persons.current_address', $address);
            })

            ->when($zip_code, function ($query) use ($zip_code) {
                $query->where('persons.zip_code', $zip_code);
            })

            ->when($future_interest, function ($query) use ($future_interest) {
                $query->where('persons.future_interest', $future_interest);
            })

            ->when($local_interest, function ($query) use ($local_interest) {
                $query->where('persons.local_interest', $local_interest);
            })

         
            ->whereNull('leads.deleted_at')->get();

        //dd($leads);

        if ($request->android == 1) {
            return response()->json(['input' => $request->input(), 'leads' => $leads]);
        }

        return view('admin.leads.search-result', compact('leads', 'rights'));
        // dd($leads);
    }

    public function updateLead(Request $request)
    {
        if($request->ajax())
        {
            // dd($request);
            $leads = [];
            
            $mail="";
            $ids = explode(',', $request->comment);
            // dd($ids);
            foreach ($ids as $lead_id) {
                if($lead_id != ""){
                    $lead= lead::find((int)$lead_id);
                    $lead->assigned_to   = $request->next_meeting_by;
                    $lead->assigned_by   = Auth::user()->id;
                    $lead->assigned_date = date('Y-m-d');
                    $lead->save();
                }
            }
            return response()->json(['mail' => $mail]);
        }
    }

    public function changeStatus(Request $request)
    { 
        
        $lead  = lead::find($request->lead_id);
        $input = $request->input();
        
        $input['meeting_date']   = date('Y-m-d', strtotime(strtr($input['meeting_date'], '/', '-')));
        $input['lead_status_id'] = $request->lead_status;
        $input['created_by']     = Auth::user()->id;

        if ($input['meeting_status'] == 0) {
            $input['next_meeting_date'] = date('Y-m-d', strtotime(strtr($input['next_meeting_date'], '/', '-')));
        }

        //$input['agency_id'] = $this->getAgency()->id;
        
        $leadHistory = LeadHistory::create($input);
//dd($lead);

        $lead->lead_status_id = $request->lead_status;
        $lead->save();
        
        $leadStatus = LeadStatus::find($request->lead_status);
        

        /*$receiver       = principal_officer::with(['user'])->where('agency_id', $this->getAgency()->id)->get();
        $receiverEmails = [];
        foreach ($receiver as $r) {
            $receiverEmails[] = $r->user->email;
        }
        $content = Auth::user()->name . " has updated lead " . $request->lead_id . " status";
        array_push($receiverEmails, Auth::user()->email); */

        // $job = (new LeadsEmailJob($receiverEmails, $content, $lead->id, 'updated', $leadHistory))->onQueue('lead');
        // dispatch($job);

        // $this->SendNotification(env('NOFICATIONS_TITLE'), "Lead $lead->id status changed", 'call-center');
        // $this->SendNotification(env('NOFICATIONS_TITLE'), "Lead $lead->id status changed", 'Admin');

        return response()->json(['response' => true, 'history' => $leadHistory, 'leadStatus' => $leadStatus]);
    }

    public function activityReport(Request $request)
    {
        $leadStatus = LeadStatus::all();
        return view('admin.leads.reports.activity', compact('leadStatus'));
    }

    public function activityReportResult(Request $request)
    {
        $startDate = date('Y-m-d', strtotime($request->startdate)) . ' 00:00:00';
        $endDate   = date('Y-m-d', strtotime($request->enddate)) . ' 23:59:00';

        $leadStatus = LeadStatus::all();

        $leadStatusCount = [];

           $leads = lead::with(['status', 'user'])->whereBetween('created_at', [$startDate, $endDate])->get();

            foreach ($leadStatus as $status) {
                $countLeads = lead::whereBetween('created_at', [$startDate, $endDate])->where('lead_status_id', $status->id)->count();
                if ($countLeads > 0) {
                    $leadStatusCount[$status->id] = $countLeads;
                }
            }

        if ($request->android == 1) {
            return response()->json(['leads' => $leads, 'leadStatus' => $leadStatus, 'leadStatusCount' => $leadStatusCount]);
        }

        return view('admin.leads.reports.activity', compact('leadStatus', 'leads', 'leadStatusCount'));
    }
    public function import()
    {
        // $user = User::select(DB::Raw('persons.*, agents.id as agent_id'))
        //             ->join('agents','agents.person_id','=','persons.id')
        //             ->where('persons.username', 'like' ,'muneeb%')
        //             ->where('agents.agency_id', $this->getAgency()->id)
        //             ->first();
        // dd($user);
        return view('admin.leads.import');
    }

    public function ImportData(Request $request)
    {
        // dd($request->file('ImportData'));
        // $this->validate($request, [
        //     'ImportData' => 'mimes:xls,csv,xlsx|required',
        // ]);

    if($request->hasFile('ImportData'))
    {
        $extension = File::extension($request->ImportData->getClientOriginalName());
            if ($extension == "csv") 
            {
                $file = $request->file('ImportData');

                $fileParts = explode('.',$file->getClientOriginalName());
                $imagename = md5($fileParts[0]).'.'.$fileParts[1];
                $path = $request->file('ImportData')->storeAs(
                    'public', $imagename
                );
                $file = storage_path('app/public/'.$imagename);
                $results = $this->csvToArray($file);
                // Getting all results
                // $results = $reader->get();
                // dd($results);
                $block_descArr = [];
                $customer      = [];
                // $agency        = $this->getAgency()->id;
                foreach ($results as $key => $result['items']) {
                    
                    if ($result['items']['customer_name'] != "") {

                    $name  = $result['items']['customer_name'];
                    $email = $result['items']['email_adress'];
                    $phone = $result['items']['phone_no'];
                    $query = $result['items']['customer_query'];
                    $city  = $result['items']['city'];
                    if($result['items']['lead_date'] != ""){
                        $date  = date('y-m-d', strtotime($result['items']['lead_date']));
                    }else{
                        $date = date('y-m-d');
                    }
                    
                    $followup = $result['items']['follow_up_1'];
                    
                    //Check if user already exist or not
                   
                  // dd($name,$email,$phone,$query,$city,$date);
                   
                    if ($email != "" || $phone != "") {
                        $user = User::where('email', $email)
                            ->where('phone_mobile' , $phone)
                            ->count();
                            
                        // activity('Client Generation')
                        //     ->withProperties(['event' => 'updated'])
                        //   ->log(":causer.email already exist");
                            // if($phone == '23219414347'){
                            //     dd('ge');
                            // }
                            if ($user == 0) {
                            $userArr['name']            = $name;
                            $userArr['username']        = $name;
                            $userArr['email']           = $email;
                            $userArr['whatsapp_number'] = $phone;
                            $userArr['phone_mobile']    = $phone;
                            $userArr['password']        = 123456;
                            $userArr['created_by']      = Auth::user()->id;
                            $userArr['city_id']      = $city;
        
                            $user = User::Create($userArr);
                            
                    //13 = Customer
                    $user->Roles()->attach(13);

                    $clientArr['person_id']  = $user->id;
                    // $clientArr['agency_id']  = $this->getAgency()->id;
                    $clientArr['created_by'] = Auth::user()->id;
                    $client                  = Client::Create($clientArr);

                    //Adding lead
                    $lead                                = new lead();
                    $lead->comments                      = $query;
                    $lead->person_id                     = $user->id;
                    $lead->created_by                    = Auth::user()->id;
                    $lead->lead_date                     = date('y-m-d', strtotime($date));
                    $lead->interest                      = "";
                    $lead->lead_status_id                = 1;
                    $lead->customer_feedback_after_visit = "";
                    $lead->whatsapp_sent_on              = "";
                    $lead->visit_promise_date            = "";
                    // $lead->agency_id                     = $this->getAgency()->id;
                    $lead->assigned_to   = Auth::user()->id;
                    $lead->assigned_by   = Auth::user()->id;
                    $lead->assigned_date = date('y-m-d', strtotime($date));
                    $lead->save();
                    
                    //followup LeadHistory

                    if($followup != ""){
                        
                        $leadhistoryArr['lead_id']      = $lead->id;
                    $leadhistoryArr['lead_status_id'] = $lead->lead_status_id ?$lead->lead_status_id:"" ;
                    // $leadhistoryArr['agency_id'] = $this->getAgency()->id;
                    $leadhistoryArr['meeting_date'] = date('Y-m-d', strtotime($lead->lead_date. ' + 1 days'));
                    $leadhistoryArr['meeting_detail'] = $followup;
                    $leadhistoryArr['meeting_done_by'] = Auth::user()->id;

                    $leadhistory                 = LeadHistory::create($leadhistoryArr);
                    }
                    
                    
                            }
                    
                    }
                  
                    
                    }
                    
                   
                }
                
                //dd(count($results));
                 //dd($block_descArr);
                $request->session()->flash('status', 1);
                $request->session()->flash('message', "File has been imported successfully...");
            }else{
                $request->session()->flash('status', 0);
                $request->session()->flash('message', "File did not import. Please try again...");
            }
        }else{
            $request->session()->flash('status', 0);
            $request->session()->flash('message', "Please import a CSV file...");
        }

        
        return redirect()->route('lead.import');
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
    
    public function dueMeetings(Request $request){
            
            $rights = Role::getrights('Duemeetings');
        
            if(!Session::get('objects'))
                Session::put('objects', Role::getMenuItems());
            
            if($rights->can_view){
                $agents = agents::with(['user'])->get();
                $principal = principal_officer::with(['user'])->get();
                $sale_officers = Employees::whereNull('deleted_at')->get(); 
                $leadStatus = LeadStatus::whereNull('deleted_at')->get();

        
                $assign_to_html = "<select name='next_meeting_by' class='form-control assignTo' style='width: 200px;'>";
                $assign_to_html .= "<option value=''></option>";
                
                foreach ($sale_officers as $officer) {
                    if($officer->user){
                        $selected = ($officer->user->id == Auth::user()->id) ? "selected" : "";
                        $assign_to_html .= "<option value='" . $officer->user->id . "' " . $selected . ">" . $officer->user->name . " - agent </option>";
                    }
                }
                
                $assign_to_html .= "</select>";
                
                return view('admin.leads.due_meetings',compact('rights', 'agents', 'principal' , 'leadStatus' , 'assign_to_html' , 'sale_officers'));
            }else{
                 return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));        
            }
        }

    public function getDueMeetings(Request $request)
    {
        $rights = Role::getrights('Duemeetings');
        $hidden_id=$request->input('user');
        
        $from_date = date('Y-m-d', strtotime(strtr($request->input('from_date'), '/', '-'))). ' 00:00';
        $to_date   = date('Y-m-d', strtotime(strtr($request->input('to_date'), '/', '-'))) . ' 23:59';
        
        $followup = leadHistory::whereBetween('next_meeting_date',[$from_date, $to_date])->where('next_meeting_by',$request->input('user'))->where('meeting_status', 0)->whereRaw('id IN (select MAX(id) FROM lead_history GROUP BY lead_id)')->orderBy('id', 'DESC')->get();
         
        $input = $request->input();
        $user = $request->input('user');


        $agents = Agents::get();
        $principal = principal_officer::get();
        $sale_officers = Employees::whereNull('deleted_at')->get();


        $leadStatus = LeadStatus::whereNull('deleted_at')->get();
        
        $assign_to_html = "<select name='next_meeting_by' class='form-control assignTo' style='width: 200px;'>";
        $assign_to_html .= "<option value=''></option>";

        foreach ($sale_officers as $officer) {
            if($officer->user){
            $selected = ($officer->user->id == Auth::user()->id) ? "selected" : "";
            $assign_to_html .= "<option value='" . $officer->user->id . "' " . $selected . ">" . $officer->user->name . " - agent </option>";
        }
}
        $assign_to_html .= "</select>";
        // return redirect()->back()->withInput()->with(['ccd' => $ccd, 'followup' => $followup]);
        //dd($test);
        return view('admin.leads.due_meetings',compact('rights', 'agents','principal','followup', 'input' , 'leadStatus' , 'assign_to_html' , 'sale_officers' , 'user','hidden_id'));
    }
}
