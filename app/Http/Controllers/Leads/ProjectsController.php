<?php

namespace App\Http\Controllers\Leads;

use App\Http\Controllers\Controller;
use App\Models\Leads\projects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Role;
use App\Traits\CustomFunctions;
use App\agency;
use App\Models\Agency\agents;
use App\Models\Agency\principal_officer;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;

class ProjectsController extends Controller
{
     use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('projects');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){
            $projects = projects::where('status', 1)->whereNull('deleted_at')->get();
            return view('admin.leads.projects.index', compact('projects', 'rights'));        
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
     
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           $rights = Role::getrights('projects');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_create){

            $agencies = agency::get();
            return view('admin.leads.projects.create',compact('agencies'));
        } else {
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:projects',
        ])->validate();
        
        $input = $request->input();
        $input['created_by'] = Auth::user()->id;
        // $input['agency_id'] = $aid;

        if (Input::hasfile('logo')) {
            $image    = Input::file('logo');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);
            $input['logo'] = $filename;
        }
        

        projects::create($input);
        
        Session::flash('status', 'success');
        Session::flash('message', 'Project has been added successfully.');

        return redirect()->route('leads.projects')->with(['message' => 'Project Added', 'status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('projects');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_create){
            
            $project = projects::find($id);
            $agencies = agency::get();
            return view('admin.leads.projects.edit',compact('agencies', 'project'));
        } else {
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $project = projects::find($id);
        
        Validator::make($request->all(), [
            'name' => Rule::unique('projects')->where(function ($query) use($id) {
                return $query->whereNull('deleted_at')->where('id','!=',$id);
            }),
            'logo' => 'mimes:jpeg,png,gif |max:4096',
        ])->validate();

        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        if (Input::hasfile('logo')) {
            $image    = Input::file('logo');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);
            $input['logo'] = $filename;
        }
        
        $project->update($input);
        
        Session::flash('status', 'success');
        Session::flash('message', 'Project has been updated successfully.');

        return redirect()->route('leads.projects')->with(['message' => 'Project Updated', 'status' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = projects::find($id);

        $project->delete();
        
        Session::flash('status', 'success');
        Session::flash('message', 'Project has been deleted successfully.');
        
        return redirect()->route('leads.projects')->with(['message' => 'Project Deleted', 'status' => 'success']);
    }
}
