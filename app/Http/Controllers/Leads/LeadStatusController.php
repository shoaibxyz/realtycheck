<?php

namespace App\Http\Controllers\Admin\Leads;

use App\Http\Controllers\Controller;
use App\Models\Leads\LeadStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Traits\CustomFunctions;
use App\Models\Agency\agency;
use App\Models\Agency\agents;
use App\Models\Agency\principal_officer;
use Illuminate\Validation\Rule;
use Validator;
use App\Role;

class LeadStatusController extends Controller
{

    use CustomFunctions;
    /**
     * Display a listing of the restatu.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\Auth::user()->can(('can_view_lead_status')))
            abort(403);

        $canEdit = Auth::user()->can('can_edit_lead_status');
        $canDelete = Auth::user()->can('can_delete_lead_status');
        
        

        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Super-Admin')) {

       $lstatus = LeadStatus::whereNull('deleted_at')->get();

        return view('admin.lead-status.index', compact('lstatus', 'canEdit', 'canDelete'));

    }

    else {

         
            $lstatus = LeadStatus::whereNull('deleted_at')->where('agency_id' , $this->getAgency()->id)->get();
            // dd($posts);
           
        return view('admin.lead-status.index', compact('lstatus', 'canEdit', 'canDelete'));

    }

    
    }


    /**
     * Show the form for creating a new restatu.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(!\Auth::user()->can(('can_create_lead_status'))
        //     abort(403);

        return view('admin.lead-status.create');
    }

    /**
     * Store a newly created restatu in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $aid = $this->getAgency()->id;
        Validator::make($request->all(), [
            'name' => Rule::unique('lead_status')->where(function ($query) use($aid) {
                return $query->where('agency_id', $aid)->whereNull('deleted_at');
            }),
        ])->validate();
        $input = $request->input();
        $input['created_by'] = Auth::user()->id;

        $input['agency_id'] = $this->getAgency()->id;
   


        LeadStatus::create($input);

        return redirect()->route('lead-status.index')->with(['message' => 'Status Added Successfully', 'status' => 1]);
    }

    /**
     * Display the specified restatu.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified restatu.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // if(!\Auth::user()->can(('can_edit_lead_status'))
        //     abort(403);
        $lead = LeadStatus::findorfail($id);
        return view('admin.lead-status.edit', compact('lead'));
    }

    /**
     * Update the specified restatu in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lead = LeadStatus::findorfail($id);
        
        $aid = $this->getAgency()->id;
        Validator::make($request->all(), [
            'name' => Rule::unique('lead_status')->where(function ($query) use($aid) {
                return $query->where('agency_id' , '!=' ,$aid)->whereNull('deleted_at');
            }),
        ])->validate();

        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        $lead->update($input);

        return redirect()->route('lead-status.index')->with(['message' => 'Status updated succesfully.', 'status' => 1]);
    }

    /**
     * Remove the specified restatu from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if(!\Auth::user()->can(('can_delete_lead_status'))
        //     abort(403);

        $lead = LeadStatus::findorfail($id);

        $lead->delete();

        return redirect()->route('lead-status.index')->with(['message' => 'Status deleted successfully', 'status' => 1]);
    }
}
