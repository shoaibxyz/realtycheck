<?php

namespace App\Http\Controllers\Leads;

use App\Http\Controllers\Controller;
use App\Jobs\LeadReminderEmailJob;
use App\Mail\LeadReminderEmail;
use App\Models\Leads\LeadReminder;
use App\Models\Leads\lead;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Illuminate\Support\Facades\Session;
use App\Role;
use DB;

class LeadReminderController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('reminder');
         if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $reminders = DB::table('lead_reminder')->whereNull('deleted_at')->get();
           
              return view('admin.leads.reminders.index', compact('reminders', 'rights'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $rights = Role::getrights('reminder');
         if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $leads = lead::with(['User', 'project', 'status', 'sources', 'propertyType', 'PropertySubtypes', 'assignedBy', 'assignedTo'])->whereNull('deleted_at')->get();
     
            $leadsArr = [];
            
            foreach($leads as $lead)
            {
                $leadsArr[$lead->id] = "Lead Id => ". $lead->id;
            }
            
            return view('admin.leads.reminders.create', compact('leadsArr'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $lead = lead::find($request->lead_id);
        $input['created_by'] = Auth::user()->id;

        $leadReminder = LeadReminder::create($input);

        // $job = (new LeadReminderEmailJob(Auth::user()->email, $lead->id, $leadReminder->id))->onQueue('leadReminder');
        // dispatch($job);
        return redirect()->route('reminder.index')->with(['message' => 'Lead Reminder Saved.', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reminder = LeadReminder::findorfail($id);

        $leads = lead::with(['User','propertyType','PropertySubtypes','status'])->whereNull('deleted_at')->get();
        $leadsArr = [];

        foreach($leads as $lead)
        {
            $leadsArr[$lead->id] = $lead->id . ' -> ' . $lead->propertyType['name'] .' -> '. $lead->PropertySubtypes['name'] . ' -> '. $lead->status['name'];
        }
        return view('admin.leads.reminders.edit', compact('reminder', 'leadsArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->android == 1){
            $user = JWTAuth::toUser($request->input('token'));

            $reminder = LeadReminder::find($request->reminder_id);
            $input = $request->input();
            $input['updated_by'] = $user->id;
            $reminder->update($input);

            return response()->json(['reminder' => $reminder]);
        }

        $reminder = LeadReminder::find($id);
        $input = $request->input();
        $input['updated_by'] = Auth::user()->id;

        $reminder->update($input);

        return redirect()->route('reminder.index')->with(['message' => 'Reminder Updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reminder = LeadReminder::find($id);
        $reminder->delete();

        return redirect()->route('reminder.index')->with(['message' => 'Reminder Deleted', 'status' => 1]);
    }

    public function complete(Request $request, $id)
    {
        $reminder = LeadReminder::find($id);
        $reminder->is_completed = 1;
        $reminder->completed_by = Auth::user()->id;
        $reminder->completed_date = date('Y-m-d H:i:s');

        $reminder->save();

        if($request->android == 1)
            return response()->json(['reminder' => $reminder]);

        return redirect()->route('reminder.index')->with(['message' => 'Reminder Completed', 'status' => 1]);
    }
}
