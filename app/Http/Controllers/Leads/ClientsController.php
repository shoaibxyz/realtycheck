<?php

namespace App\Http\Controllers\Leads;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Leads\Client;
use App\Models\Leads\LeadStatus;
use App\Models\Leads\PropertySubtypes;
use App\Models\Booking\members;
use App\Models\Leads\PropertyType;
use App\Models\Leads\Source;
use App\Models\Leads\UserAttachment;
use App\User;
use App\Role;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\Leads\lead;
use App\Models\Leads\projects;

class ClientsController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('clients');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_view){

            $clients = Client::whereNull('deleted_at')->get();
       

        if (\Request::input('android') == 1) {
            return response()->json(['clients' => $clients]);
        }

        return view('admin.leads.clients.index', compact('clients', 'rights'));
            
            }else{
        
                return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
                
            }
     


    }

    public function getLeads()
    {
        $leads = User::all();
        return response()->json(['leads' => $leads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('admin.leads.clients.create', compact('countries'));
    }

    public function getPropertySubType(Request $request)
    {
        $stypes = PropertySubtypes::where('property_type_id', $request->input('property_type_id'))->get();

        $html = "";
        $html .= "<option value=''>Select Property Sub Type</option>";
        foreach ($stypes as $type) {
            $html .= "<option value='" . $type->id . "'>" . $type->name . "</option>";
        }

        if ($request->input('android') == 1) {
            return response()->json(['stypes' => $stypes]);
        }

        return response()->json(['html' => $html]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user                    = new User();
        $user->name              = $request->input('name');
        $user->username          = $request->input('name');
        $user->email             = $request->input('email');
        $user->phone_res         = $request->input('phone');
        $user->phone_mobile      = $request->input('mobile');
        $user->current_address   = $request->input('address');
        $user->permanent_address = $request->input('address');
        $user->whatsapp_number      = $request->input('whatsapp_number');
        $user->zip_code          = $request->input('zip_code');
        $user->country_id        = $request->input('country_id');
        $user->cnic        = $request->input('cnic');
        $user->password          = 123456;
        $user->created_by        = Auth::user()->id;
        $user->save();

        //13 = Customer
        $user->Roles()->attach(13);

        $client            = new Client();
        $client->person_id = $user->id;
        $client->client_rating = $request->client_rating;
        $client->client_type = $request->client_type;
        $client->created_by        = Auth::user()->id;
        $client->save();

        if (\Request::input('android') == 1) {
            return response()->json(['status' => "1", "message" => 'Client added']);
        }

        return redirect()->route('clients.index')->with(['message' => 'Client Generated']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
        $client = Client::with(['user'])->findorfail($id);
        $notes  = UserAttachment::distinct('title')->where('person_id', $client->user->id)->get();

        $leads = DB::table('leads')->select(DB::Raw('persons.id, persons.name, persons.client_type, lead_status.name as status_name, projects.name as project_name, leads.id as lead_id,leads.created_at'))
            ->leftjoin('persons', 'persons.id', '=', 'leads.person_id')
            ->leftjoin('lead_status', 'lead_status.id', '=', 'leads.lead_status_id')
            ->leftjoin('projects', 'projects.id', '=', 'leads.project_id')
            ->where('leads.person_id', $client->user->id)
            ->where('leads.deleted_at', null)->get();

        if (\Request::input('android') == 1) {
            return response()->json(['client' => $client, 'notes' => $notes, 'leads' => $leads]);
        }
        
        //dd($leads);

        return view('admin.leads.clients.show', compact('client', 'notes', 'leads'));
    }

    public function getClientInfo(Request $request)
    {
        if ($request->input('search_in')) {
            if ($request->search_in == "all") {
                $clients = DB::table('persons')
                    ->select(DB::Raw('persons.*, persons.id as person_id, clients.*'))
                    ->join('clients', 'clients.person_id', 'persons.id')
                    ->whereNull('persons.deleted_at')
                    ->get();
                //dd($clients);    
                
                // $user = User::join()where('cnic',$request->input('cnic'))->first();
                if ($clients === null) {
                    return json_encode(['error' => 1]);
                }

                // $member = Client::where('person_id', $clients->person_id)->first();
//dd($clients);
                $data = ['clients' => $clients];
                    
                return $data;
                // $clients = User::whereNull('deleted_at')->Customer();
            } else {
                $clients = DB::table('persons')
                    ->select(DB::Raw('persons.*, persons.id as person_id, clients.*'))
                    ->join('clients', 'clients.person_id', 'persons.id')
                    ->where('clients.agency_id', $this->getAgency()->id)
                    ->whereNull('persons.deleted_at')
                    ->where($request->search_in, 'LIKE', "%" . $request->search . "%")
                    ->first();
                $data = ['clients' => $clients];
                    
                return $data;
                // $clients = User::where($request->search_in, 'LIKE', "%" . $request->search . "%")->whereNull('deleted_at')->Customer();
            }
        } else {
            $clients = User::whereNull('deleted_at')->Customer();
        }
        
        return response()->json($clients);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client    = Client::with(['user'])->findorfail($id);
        $countries = Country::all();
        return view('admin.leads.clients.edit', compact('client', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::with(['user'])->find($id);
        // dd($request->input());
        $client->user->name              = $request->input('name');
        $client->user->username          = $request->input('name');
        $client->user->email             = $request->input('email');
        $client->user->phone_res         = $request->input('phone');
        $client->user->phone_mobile      = $request->input('mobile');
        $client->user->current_address   = $request->input('address');
        $client->user->permanent_address = $request->input('address');
        $client->user->whatsapp_number = $request->input('whatsapp_number');
        $client->user->zip_code          = $request->input('zip_code');
        $client->user->country_id        = $request->input('country_id');
        $client->user->save();

        $client->client_rating = $request->client_rating;
        $client->local_interest = $request->local_interest;
        $client->future_interest = $request->future_interest;
        $client->client_type = $request->client_type;
        $client->save();


        if (\Request::input('android') == 1) {
            return response()->json(['client' => $client]);
        }

        return redirect()->route('clients.index')->with(['message' => 'Client Information updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $client             = Client::findorfail($id);
        $leads = lead::where('person_id',$client->person_id)->get();
            
        //dd($leads);
        foreach($leads as $l){
            $l->delete();
        }
        
        
        $client->delete();

        return redirect()->route('clients.index')->with(['message' => 'Client deleted successfully']);
    }

    public function deletedClients()
    {
        $rights = Role::getrights('clients');
        
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
        
        if($rights->can_delete){
            $clients = Client::onlyTrashed()->get();
            return view('admin.leads.clients.deleted', compact('clients', 'rights'));
        }else{
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
        }
        
    }

    public function restoreLeads(Request $request, $id)
    {

        $client = Client::onlyTrashed()->find($id);
        $client->restore();

        $client->user->Roles()->attach(13);

        return redirect()->route('clients.deleted')->with(['message' => 'Client Restored']);
    }

    public function search()
    {
        $countries     = Country::all();
        $propertyTypes = PropertyType::all();
        $LeadSource    = Source::all();
        $projects      = projects::all();
        $LeadStatus    = LeadStatus::all();

        return view('admin.leads.clients.search', compact('countries', 'propertyTypes', 'LeadSource', 'projects', 'LeadStatus'));
    }

    public function searchLead(Request $request)
    {

        //Client information
        $id              = $request->client_id;
        $name            = $request->name;
        $phone           = $request->phone;
        $email           = $request->email;
        $mobile          = $request->mobile;
        $address         = $request->address;
        $zip_code        = $request->zip_code;
        $client_type     = $request->client_type;
        $client_rating   = $request->client_rating;
        $future_interest = ($request->future_interest) ? 1 : 0;
        $local_interest  = ($request->local_interest) ? 1 : 0;

        

            $clients = DB::table('persons')->select(DB::Raw('persons.*'))
                ->join('role_person', 'role_person.person_id', '=', 'persons.id')
                ->join('roles', 'roles.id', '=', 'role_person.role_id')
                ->where('roles.name', 'Customer')

                ->when($id, function ($query) use ($id) {
                    return $query->where('persons.id', $id);
                })
                ->when($name, function ($query) use ($name) {
                    $query->where('persons.name', 'LIKE', '%' . $name . '%');
                })

                ->when($email, function ($query) use ($email) {
                    $query->where('persons.email', 'LIKE', '%' . $email . '%');
                })

                ->when($phone, function ($query) use ($phone) {
                    $query->where('persons.phone_res', 'LIKE', '%' . $phone . '%');
                })

                ->when($mobile, function ($query) use ($mobile) {

                    $query->where('persons.phone_mobile', 'LIKE', '%' . $mobile . '%');
                })

                ->when($address, function ($query) use ($address) {
                    $query->where('persons.permanent_address', 'LIKE', '%' . $address . '%');
                    $query->orWhere('persons.current_address', 'LIKE', '%' . $address . '%');
                })

                ->when($client_rating, function ($query) use ($client_rating) {
                    $query->where('persons.client_rating', $client_rating);
                })

                ->when($client_type, function ($query) use ($client_type) {
                    $query->where('persons.client_type', $client_type);
                })

                ->when($zip_code, function ($query) use ($zip_code) {
                    $query->where('persons.zip_code', 'LIKE', '%' . $zip_code . '%');
                })
                ->when($future_interest, function ($query) use ($future_interest) {
                    $query->where('persons.future_interest', $future_interest);
                })
                ->when($local_interest, function ($query) use ($local_interest) {
                    $query->where('persons.local_interest', $local_interest);
                })
                ->get();

        
       
        if ($request->android == 1) {
            return response()->json(['clients' => $clients]);
        }

        // dd($clients);
        return view('admin.leads.clients.search-result', compact('clients'));
        // dd($leads);
    }
}
