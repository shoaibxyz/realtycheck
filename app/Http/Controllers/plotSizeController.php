<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\plotSize;
use App\Role;
use Yajra\Datatables\Datatables;
use Validator;

class plotSizeController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('plotsize');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            return view('admin.plotsize.index',compact('rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function getPlots()
    {
        $plotSize = plotSize::whereNull('deleted_at')->get();
        return Datatables::of($plotSize)
        ->addColumn('edit', function($plotSize){
            $rights = Role::getrights('plotsize');
            $se = $rights->show_edit;
            return '<a href="'.route('plotsize.edit',$plotSize->id).'" class="btn btn-xs btn-info" style="'.$se.'"> <i class="fa fa-edit"></i></a>';
        })
        ->addColumn('delete', function($plotSize){
            $rights = Role::getrights('plotsize');
            $de = $rights->show_delete;
            return '<form method="post" action="'.route('plotsize.destroy', $plotSize->id).'" style="'.$de.'">'.csrf_field()."<input name='_method' type='hidden' value='DELETE'>  <button class='btn btn-danger btn-xs' type='submit' value='' title='delete'><i class='fa fa-trash-o'></i></button></form>";
        })
        // ->editColumn('plot_size', function($plotSize){
        //     return $this->is_base64($plotSize->plot_size) ? \Crypt::decrypt($plotSize->plot_size) : "";
        // })
        ->rawColumns(['edit','delete'])
        ->make(true);
    }

     function is_base64($s)
    {
          return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s);
    }

    public function create()
    {
        $rights = Role::getrights('plotsize');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.plotsize.create');
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'plot_size' => 'unique:plot_size',
        ])->validate();

        plotSize::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Plot Size has been added successfully.');

        return redirect('admin/plotsize');
    }

    public function destroy($id)
    {
        $plotsize = plotSize::find($id);
        $plotsize->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Plot Size has been deleted successfully.');

        return redirect('admin/plotsize');
    }

    public function edit($id)
    {
        $rights = Role::getrights('plotsize');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $plotsize = plotSize::find($id);
        return view('admin.plotsize.edit',compact('plotsize'));
    }

    public function update($id, Request $request)
    {
        $name = $request->input('plot_size');
        $plotsize = plotSize::find($id);
        
        //if plot_size is actually changed
        if($plotsize->plot_size!=$name){
        Validator::make($request->all(), [
            'plot_size' => 'unique:plot_size',
        ])->validate();
        }
        
        

        $plotsize->plot_size = $name;
        $plotsize->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Plot Size has been updated successfully.');

        return redirect('admin/plotsize');
    }
}
