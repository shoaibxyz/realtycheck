<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\MemberValidation;
use App\Http\Requests\UpdatePasswordRequest;
use App\Mail\SendWelcomeEmail;
use App\Role;
use App\User;
use App\Models\HR\Employees;
use App\members;
use App\paymentSchedule;
use App\Payments;
use App\assignFiles;
use App\memberInstallments;
use GuzzleHttp\Client;
use Yajra\Datatables\Datatables;
use Mail;
use Spatie\Activitylog\Models\Activity;
use DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PersonController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('members');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        $rights = Role::getrights('members');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            return view('admin.users.index', compact('rights'));
        }
    }

    public function getPersons()
    {
        /*$persons = User::join('role_person','role_person.person_id','persons.id')
                        ->join('roles','roles.id','role_person.role_id')
                        ->select(DB::Raw('persons.*'))
                        ->where('roles.name','Member')
                        ->where('status',1)
                        ->get();*/

        $persons = User::join('members','members.person_id','persons.id')->where('persons.status',1)->whereHas('roles', function($query){
            $query->where('name', '=', \Config::get('constants.roles.member'));
        })->select(DB::Raw('members.id as member_id, registration_no,persons.*'))->get();
        //dd($persons);
        return Datatables::of($persons)
            ->editColumn('member_id', function ($person) {
                return $person->member_id;
            })
            ->editColumn('reg_no', function ($person) {
                return $person->registration_no;
            })
            ->editColumn('name', function ($person) {
                return $person->name;
            })
            ->editColumn('guardian', function ($person) {
                return $person->gurdian_name;
            })
            /* ->editColumn('email', function ($person) {
                return $person->email;
            }) */
            ->editColumn('phone', function ($person) {
                return $person->phone_mobile;
            })
            /* ->editColumn('registration_no', function ($person) {
                return $person->member->registration_no or "";
            }) */
            ->editColumn('cnic', function ($person) {
                return $person->cnic;
            })
            ->editColumn('date', function ($person) {
                return date('d-m-Y',strtotime($person->created_at));
            })
            
            ->addColumn('edit', function ($person) {
                $rights = Role::getrights('users');
                $se = $rights->show_edit;
                return '<a href="' . route('users.edit', $person->id) . '" class="btn btn-xs btn-info" style="'. $se .'"> Edit</a>';
            })
            ->addColumn('delete', function ($person) {
                $rights = Role::getrights('users');
                $de = $rights->show_edit;
                return '<form method="post" action="' . route('users.destroy', $person->id) . '" style="'. $de .'">' . csrf_field() . "<input name='_method' type='hidden' value='DELETE'> <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'></form>";
            })
            ->rawColumns(['edit', 'delete'])
            ->make(true);
    }

    public function create()
    {
        $rights = Role::getrights('members');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.users.create');
    }

    public function show()
    {

    }

    public function store(MemberValidation $request)
    {
       //person creation
        $user = new User();

        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('public', $image,$filename,'private');
            $destinationPath = storage_path('/app/public');
            $thumbImg = \App\Helpers::createImage($image,$destinationPath,$filename,290,385);
            $user['picture'] = $filename;
        }

        if (Input::hasfile('cnic_pic')) {
            $image       = Input::file('cnic_pic');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('public', $image,$filename,'private');
            $destinationPath = storage_path('/app/public');
            $thumbImg = \App\Helpers::createImage($image,$destinationPath,$filename,280,180);
            $user['cnic_pic'] = $filename;
        }

        $password = str_random();

        $user->username          = $request->input('username');
        $user->name              = $request->input('name');
        $user->guardian_type     = $request->input('guardian_type');
        $user->gurdian_name      = $request->input('guardian_name');
        $user->nationality       = $request->input('nationality');
        $user->cnic              = $request->input('cnic');
        $user->email             = $request->input('email');
        $user->phone_mobile      = $request->input('phone_mobile');
        $user->current_address   = $request->input('current_address');
        $user->permanent_address = $request->input('permanent_address');
        $user->country_id        = $request->input('country_id');
        $user->city_id           = $request->input('city_id');
        $user->phone_office      = $request->input('phone_office');
        $user->phone_res         = $request->input('phone_rec');
        $user->passport          = $request->input('passport');
        $user->dateofbirth       = $request->input('dob');
        $user->ref_pk            =   $request->input('ref_pk');
        $user->email_verified    = 1;
        $user->is_approved       = 1;
        $user->password          = $password;
        $user->remember_token    = str_random(60);
        // $user->person_type       = 1;

        $user->save();

       //member creation
        $member = new members();

        $member->nominee_name  = $request->input('nominee_name');
        $member->nominee_guardian= $request->input('nominee_guardian');
        $member->nominee_cnic = $request->input('nominee_cnic');
        $member->relation_with_member = $request->input('relation_with_member');

        $member->person_id = $user->id;

        $member->save();

        $user->Roles()->attach(1);

        // Mail::to($user)->send(new SendWelcomeEmail($user, $password));

        // $message = "New persons added";
        // return redirect('admin/users')->with(['message' => $message]);

        return redirect('admin/booking/generate/'.$user->id)->with(['member' => $member]);
    }

    public function destroy($id)
    {
        $persons = User::find($id);
        $persons->status = 0;
        $persons->cnic = $persons->cnic.'-'.uniqid();
        $persons->username = $persons->username.'-'.uniqid();
        $persons->save();

        $persons->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Member has been deleted successfully.');

        return redirect('admin/users');
    }

    public function edit($id)
    {
        $rights = Role::getrights('members');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $persons = User::find($id);
        return view('admin.users.edit', compact('persons'));
    }

    public function updateo($id, Request $request)
    {
    dd('update function');
        $persons = User::find($id);
        $persons->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Member has been updated successfully.');

        return redirect('admin/users');
    }

    public function update1($id, Request $request)
    {

        $persons = User::find($id);
        $persons->email= $request->input('email');
        $persons->permanent_address= $request->input('permanent_address');
        $persons->current_address= $request->input('current_address');
        $persons->save();
        return redirect('admin/users');
    }

    public function approveStatus1($id, $status)
    {
        $user              = User::find($id);
        $status            = ($status == 0) ? 1 : 0;
        $user->is_approved = $status;
        $user->save();
        return redirect('admin/users');
    }

    public function approveStatus($id, $status)
    {
        $user              = User::find($id);
        $status            = ($status == 0) ? 1 : 0;
        $user->is_approved = $status;
        $user->save();
         Mail::send('welcome', ['user' => $user], function ($m) use ($user) {
            // $m->from('tehreematique92@gmail.com', 'Realty Check');
            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
        return redirect('admin/users' );
    }

    public function Search($reg_no)
    {
    }

    public function showprofile()
    {

        $profile = User::find(Auth::user()->id);
        return view('admin.member')->with(['profile' => $profile]); 
    }
    public function editprofile($id)
    {

        $profile = User::find($id);
        return view('admin.profileedit' , compact('profile'));
    //     if($profile->is_filled==0){
    //     return view('editprofile' , compact('profile'));
    // }
    // else{

    //       return redirect('member');

    // }
    }

    public function storeprofile($id, Request $request)
    {
         //$person = members::find($id);

        //$profile->update($request->input());
        //$profile->is_filled=1;
        //$profile->save();
        $person=User::find( $id);

        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('app/public/', $image,$filename,'private');

            $person['picture'] = $filename;
        }

        // if (Input::hasfile('picture')) {
        //     $image       = Input::file('picture');
        //     $upload_path = public_path() . '/images';
        //     $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
        //     $image->move($upload_path, $filename);
        //     $path            = $upload_path . $filename;
        //     $person['picture'] = $filename;
        // }
        $person->save();

        return redirect('member');



    }

    public function postContact(Request $request)
    {
        $input = Input::all();

        $data = array(
            'title' => 'Member Request',
            'message' => $request->input('message'),
        );


        Mail::send('email.Welcome', $data, function ($message) use ($input) {
            $message->from(Auth::user()->email, Auth::user()->name);

            $message->to(\Config::get('mail.from.address'), \Config::get('mail.from.name'));

            if(Input::has('resume'))
            {
                 $message->attach($input['resume']->getRealPath(), array(
                    'as'   => 'resume.' . $input['resume']->getClientOriginalExtension(),
                    'mime' => $input['resume']->getMimeType())
                );
            }
        });

        return redirect('contact-admin')->with(['message' => 'Your email has been sent!']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $rights = Role::getrights('dashboard');
        //dd(Session::get('objects'));
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $members = User::join('members','members.person_id','persons.id')->where('persons.status',1)->whereHas('roles', function($query){
            $query->where('name', '=', \Config::get('constants.roles.member'));
        })->count();
            $agents = User::Agents()->count();
            //dd($agents);
            $paymentPlans = paymentSchedule::all()->count();
            $bookings_count = Payments::join('members','members.registration_no','bookings.registration_no')->where('status',1)->where('members.is_current_owner',1)->whereNull('bookings.deleted_at')->count();
            $cancelbookings_count = Payments::where('status',1)->where('is_cancelled',1)->whereNull('deleted_at')->count();
            $refbookings_count = Payments::where('status',1)->where('is_cancelled',2)->whereNull('deleted_at')->count();
            $merbookings_count = Payments::where('status',1)->where('is_cancelled',3)->whereNull('deleted_at')->count();
            $rembookings_count = Payments::join('members','members.registration_no','bookings.registration_no')->where('status',1)->where('is_cancelled',0)->where('members.is_current_owner',1)->whereNull('bookings.deleted_at')->whereNull('members.deleted_at')->count();
            
            $bookings = Payments::where('status',1)->where('is_cancelled',0)->whereNull('deleted_at')->get();
            $cancelled_bookings = Payments::where('status',1)->where('is_cancelled',1)->count();
            
            $totalFiles = assignFiles::whereNull('deleted_at')->count();
            $cancelFiles = assignFiles::where('is_cancelled',1)->count();
            $refFiles = assignFiles::where('is_cancelled',2)->count();
            $merFiles = assignFiles::where('is_cancelled',3)->count();
            $remFiles = assignFiles::whereNull('deleted_at')->where('is_cancelled',0)->orWhere('is_cancelled',null)->count();
            //dd($totalFiles);
            $memberInstallments = memberInstallments::where('installment_no',1)->where('payment_desc', 'Installment')->where('is_paid',1)->count();

            if(count($bookings)!=0){
            $installmentReceived = (int)round($memberInstallments/ count($bookings) * 100);
}
else{
    $installmentReceived=0;
}

            // dd($memberInstallments);

            $client = new Client();
            $response = $client->get('http://api.bizsms.pk/api-balance-enquiry.aspx?username='.env('BIZ_USERNAME').'&pass='.env('BIZ_PASSWORD'));

            $balance_inquiry = "";
            if ($response->getBody()) {
                $balance_inquiry = $response->getBody();
            }

            $response2 = $client->get('http://api.bizsms.pk/api-expiry-enquiry.aspx?username='.env('BIZ_USERNAME').'&pass='.env('BIZ_PASSWORD'));

            $package_expiry = "";
            if ($response2->getBody()) {
                $package_expiry = $response2->getBody();
            }

            $activites = Activity::where('causer_id', Auth::user()->id)->limit(5)->orderBy('id', 'DESC')->get();
            // dd($activites);

            return view('admin.dashboard',compact('rights','members','agents','paymentPlans','bookings','bookings_count','cancelbookings_count','refbookings_count','merbookings_count','rembookings_count','cancelled_bookings','activites', 'totalFiles','cancelFiles','refFiles','merFiles','remFiles','installmentReceived', 'memberInstallments', 'package_expiry', 'balance_inquiry'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }
    
    public function dashboardGraph(Request $request)
    {
        // $bookings = Payments::where('status',1)->get();
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        $bookings = DB::table('bookings')->where('status',1)->where('is_cancelled',0)
                    ->when($startDate, function ($query) use ($startDate,$endDate) {
                         return $query->whereBetween('payment_date', [$startDate,$endDate]);
                    })->get();

        $count = 0;
        $arr = []; 
        $bookingGraph = []; 
        $months = [1,2,3,4,5,6,7,8,9,10,11,12];
        $bookingDates = [];
        foreach ($bookings as $key => $booking) {
         foreach ($months as $key => $month) {
           if( date('m', strtotime($booking->payment_date)) == $month )
            {
               $arr[date('Y-m', strtotime($booking->payment_date))][] = [strtotime(date('Y-m', strtotime($booking->payment_date))), 1];
            }
         }              
        }
        
        foreach ($arr as $key => $value) {
            $bookingGraph[] = count($value);
        }
        $arr2 = [];
        $arr3 = [];
        $arr4 = [];
        $check = [];

        // $report = receipts::all();
        $report = DB::table('receipts') 
                    ->whereNull('deleted_at')
                    ->when($startDate, function($query) use ($startDate, $endDate){
                        return $query->whereBetween('receiving_date', [$startDate,$endDate]);
                    })->where('is_active',1)->where('booking_cancelled',0)->get();

        $check = [];
        foreach ($report as $key => $receipt) {
            $memberInfo = members::where('registration_no', $receipt->registration_no)->first();
            if (!is_null($memberInfo)) {
                // echo $value->registration_no."<br>";
                $mybooking = Payments::where('registration_no',$receipt->registration_no)->where('status',1)->first();

                if (!is_null($mybooking)) {
                    // $arr2[date('Y-m', strtotime($receipt->receiving_date))][] = $receipt->received_amount;
                    
                    // $installment = memberInstallments::where('receipt_no', $receipt->receipt_no)->first();
                    if($receipt->receiving_type == 1)
                        $arr2[date('Y-m', strtotime($receipt->receiving_date))][] = $receipt->received_amount - $receipt->rebate_amount;
                    elseif($receipt->receiving_type == 2)
                        $arr3[date('Y-m', strtotime($receipt->receiving_date))][] = $receipt->received_amount - $receipt->rebate_amount;
                    elseif($receipt->receiving_type == 3){
                        $arr4[date('Y-m', strtotime($receipt->receiving_date))][] = $receipt->received_amount - $receipt->rebate_amount;
                    }
                }
            }
        }
        // dd($check);



        $monthlyIncome = [];
        $income = 0;
        foreach ($arr2 as $key => $value) {
            foreach ($value as $key2 => $val) {
                // dd($val);
                $income += $val; 
            }
            $monthlyIncome['time'][] = date('M Y',strtotime($key));
            $monthlyIncome['amount'][] = $income;
            $income = 0;
        }
        

        $monthlyIncomeBooking = [];
        $incomeBooking = 0;
        foreach ($arr3 as $key => $value) {
            foreach ($value as $key2 => $val) {
                // dd($val);
                $incomeBooking += $val; 
            }
            $monthlyIncomeBooking['time'][] = date('M Y',strtotime($key));
            $monthlyIncomeBooking['amount'][] = $incomeBooking;
            $incomeBooking = 0;
        }

        $ballotingReceived = [];
        $incomeBalloting = 0;
        foreach ($arr4 as $key => $value) {
            foreach ($value as $key2 => $val) {
                $incomeBalloting += $val; 
            }
            $ballotingReceived['time'][] = date('M Y',strtotime($key));
            $ballotingReceived['amount'][] = $incomeBalloting;
            $incomeBalloting = 0;
        }

        return response()->json(['installments_received' => $monthlyIncome, 'bookings' => $bookingGraph, 'booking_income' => $monthlyIncomeBooking,'ballotingReceived' => $ballotingReceived]);
    }

    public function setting()
    {
        return view('admin.setting.index');
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::find(Auth::user()->id);

        if( Hash::check($request->input('old_password'), $user->password) )
        {
            $user->password = $request->input('password');
            $user->save();
            return redirect('admin/dashboard')->with(['message' => 'Password updated', 'status' => 1]);
        }else{
            return redirect('admin/dashboard')->with(['message' => 'Current Password is wrong', 'status' => 0]);
        }
    }



 public function update($id, Request $request)
    {
    dd('update function');
}


    public function updateUserDetail($UserId, $bookingId, Request $request)
    {
        $persons = User::find($UserId);

        $input = $request->input();
        $input['dateofbirth'] = date('Y-m-d', strtotime(strtr($request->input('dateofbirth'), '/', '-')));

        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('app/public/', $image,$filename,'private');
            $destinationPath = storage_path('/app/public');
            $thumbImg = \App\Helpers::createImage($image,$destinationPath,$filename,290,385);
            $input['picture'] = $filename;
        }

        if (Input::hasfile('cnic_pic')) {
            $image       = Input::file('cnic_pic');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('app/public/', $image,$filename,'private');
            $destinationPath = storage_path('/app/public');
            $thumbImg = \App\Helpers::createImage($image,$destinationPath,$filename,280,180);
            $input['cnic_pic'] = $filename;
        }

        $persons->update($input);

        //Getting registration number
        $booking = Payments::find($bookingId);
        $members = members::where('registration_no', $booking->registration_no)->first();
        $members->nominee_name = $request->input('nominee_name');
        $members->nominee_cnic = $request->input('nominee_cnic');
        $members->nominee_guardian = $request->input('nominee_guardian');
        $members->relation_with_member = $request->input('relation_with_member');
        $members->save();

      //  return redirect('admin/booking/'.$bookingId.'/edit');
        return redirect('admin/booking/'.$booking->registration_no.'/edit');

    }

    public function mail()
    {
        $user = Auth::user()->toArray();
        Mail::send('email.Welcome', $user, function($message) use ($user) {
            $message->to('rizwansaleem70@gmail.com');
            $message->subject('Mailgun Testing');
        });
        print_r(error_get_last());
        dd('Mail Send Successfully');
    }

    public function getCity(Request $request)
    {
        $cities = City::where('state_id', $request->input('state'))->get();

        $html = "<select class='form-control select2' name='city'>";
        $html .= "<option value=''>Select City</option>";
        foreach($cities as $city)
        {
            $html .= "<option value='".$city->id."'>".$city->name."</option>";
        }

        $html .= "</select>";

        return response()->json(['cities' => $html]);
    }

    public function olduser($id)
    {
        $user = User::find($id);
        
        $member = members::where('person_id' , $id)->first();

        

        
        return response()->json(['user' => $user, 'member' => $member]);

    }
    
    public function getuser(){
        
        $user = DB::table('persons')->leftjoin('role_person' , 'role_person.person_id' , 'persons.id')->leftjoin('roles' , 'roles.id' , 'role_person.role_id')->where('roles.id' , '!=' , 1)->select('persons.name' , 'roles.name as role' , 'persons.password' , 'persons.id')->get();
        
        
        
        return view('admin.users.user-index' , compact('user'));
    }
    
    public function editgetuser($id){
        
        $user = User::find($id);
        
        
        
        return view('admin.users.change-pass' , compact('user'));
    }
    
    public function updategetuser($id,Request $request){
        
        $user = User::find($id);
        
        $user->password = $request->input('password');
        $user->save();
        
        
        return redirect('admin/user/getuser');
    }

}
