<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\modules;
use App\permissions;
use App\Role;
use Validator;
use DB;

class PermissionsController extends Controller
{
     public function index()
    {
        $rights = Role::getrights('permissions');
         if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
    	    $permissions = permissions::join('modules','modules.id','permissions_new.module_id')->select(DB::Raw('permissions_new.*, modules.name as module'))->orderBy('permissions_new.name','Asc')->get();
            return view('admin.permissions.index', compact('permissions','rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function create()
    {
        $rights = Role::getrights('permissions');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $modules = modules::orderBy('name','Asc')->get();
    	
        //commented by habib
        //return view('admin.permissions.create', compact('permissions','modules'));
        return view('admin.permissions.create', compact(('modules')));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:permissions',
        ])->validate();

    	permissions::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Permission has been added successfully.');

    	return redirect('admin/permissions');
    }

    public function destroy($id)
    {
    	$permission = permissions::find($id);
        $permission->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Permission has been deleted successfully.');

    	return redirect('admin/permissions');
    }

    public function edit($id)
    {
        $rights = Role::getrights('permissions');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $permission = permissions::find($id);
        $modules = modules::orderBy('name','Asc')->get();
        return view('admin.permissions.edit',compact('permission','modules'));
    }

    public function update($id, Request $request)
    {
        $permission = permissions::find($id);
        $permission->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Permission has been updated successfully.');

        return redirect('admin/permissions');
    }
}
