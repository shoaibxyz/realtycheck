<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use App\Accounting\Tag;
use App\Accounting\Entry;
use App\Accounting\EntryType;
use App\Accounting\EntryItem;
use DB;
use Carbon;
use Validator;
use Auth;
use App\Role;

class EntriesController extends Controller
{
    public function index($tid = null)
    {
        $rights = Role::getrights('entries');
        ini_set('memory_limit','-1');
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $etypes = EntryType::getAll();
            return view('admin.accounting.entries',array('tid'=>$tid, 'rights'=>$rights))->with('etypes',$etypes);
        }
    }

    public function index2($tid = null)
    {
        $rights = Role::getrights('entries');
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $etypes = EntryType::getAll();
            return view('admin.accounting.entries',array('tid'=>$tid, 'rights'=>$rights))->with('etypes',$etypes);
        }
    }

    public function deleted_entries()
    {
        return view('admin.accounting.deleted-entries');
    }

    public function getAllEntries(Request $request)
    {
        $etype = $request->etype;
        $tid = $request->tid;
        $columns = array(
            0 => 'date',
            1 => 'number',
            2 => 'ledger',
            3 => 'entrytype_id',
            4 => 'tag_id',
            5 => 'dr_total',
            6 => 'cr_total',
            7 => 'status',
            8 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_entries')
            ->leftJoin('acc_tags','acc_tags.id','acc_entries.tag_id')
            ->join('acc_entrytypes','acc_entrytypes.id','acc_entries.entrytype_id')
            ->select(DB::Raw('acc_entries.*,acc_entries.id eid, acc_entrytypes.name etname, acc_entrytypes.prefix, acc_entrytypes.suffix, acc_entrytypes.zero_padding,acc_tags.id tid,acc_tags.title tname, acc_tags.color, acc_tags.background'))
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.module_id',Session::get('module'))

            ->when($etype, function ($query) use ($etype) {
                return $query->where('acc_entries.entrytype_id', $etype);
            })

            ->when($tid, function ($query) use ($tid) {
                return $query->where('acc_tags.id', $tid);
            })

            ->orderBy($sort['col'], $sort['dir'])

            ->get();

        $data = Datatables::of($query)

            ->addColumn('date', function($query){
                return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('number', function($query){
                $zeros = '';
                if($query->zero_padding > 0)
                {
                    for($i=0;$i<$query->zero_padding;$i++)
                    {
                        $zeros .= '0';
                    }
                }
                return $query->prefix . $zeros . $query->number . $query->suffix;
            })

            ->addColumn('ledger', function($query){
                return Entry::entryLedgers($query->eid,0);
            })

            ->addColumn('etname', function($query){
                return $query->etname;
            })

            ->addColumn('tag_id', function($query){
                if($query->tid)
                    $tag = '<span class="acc_tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->tid) . '" style="color:#'. $query->color .' !important;">'. $query->tname .'</a></span>';
                else
                    $tag = '';
                return $tag;
            })

            ->addColumn('dr_total', function($query){
                return Helpers::toCurrency('D',$query->dr_total);
            })

            ->addColumn('cr_total', function($query){
                return Helpers::toCurrency('C',$query->cr_total);
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                $rights = Role::getrights('entries');
                if($rights->show_view)
                    $cs = "display:inline-block";
                else
                    $cs = 'display:none';
                if($rights->show_view)
                    $sv = "display:'inline-block'";
                else
                    $sv = 'display:none';
                if($rights->show_edit)
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if($rights->show_delete)
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <button type='button' class='btn btn-default btn-xs' style='$cs' onclick=changeStatus(". $query->eid .",". $query->status .")><i class='fa fa-hand-o-left'></i>&nbsp;Change Status</button>&nbsp;
                            <a type='button' class='btn btn-success btn-xs view' data-toggle='tooltip' title='View' style='$sv' href='". url('/admin/accounting/view-entry/'.$query->eid) . "'><i class='fa fa-eye'></i></a>&nbsp;
                            <a type='button' class='btn btn-info btn-xs edit' data-toggle='tooltip' title='Edit' style='$se' href='". url('/admin/accounting/edit-entry/'.$query->eid) . "'><i class='fa fa-pencil'></i></a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete delete' data-toggle='tooltip' title='Delete' style='$de' href='". url('/admin/accounting/delete-entry/'.$query->eid) . "'><i class='fa fa-times'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('ledger','tag_id','status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function getAllDelEntries(Request $request)
    {
        $columns = array(
            0 => 'date',
            1 => 'number',
            2 => 'ledger',
            3 => 'entrytype_id',
            4 => 'tag_id',
            5 => 'dr_total',
            6 => 'cr_total',
            7 => 'status',
            8 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_entries')
            ->leftJoin('acc_tags','acc_tags.id','acc_entries.tag_id')
            ->join('acc_entrytypes','acc_entrytypes.id','acc_entries.entrytype_id')
            ->select(DB::Raw('acc_entries.*,acc_entries.id eid, acc_entrytypes.name etname, acc_entrytypes.prefix, acc_entrytypes.suffix, acc_entrytypes.zero_padding,acc_tags.id tid,acc_tags.title tname, acc_tags.color, acc_tags.background'))
            ->where('acc_entries.deleted_at','!=',NULL)
            ->where('acc_entries.module_id',Session::get('module'))

            ->orderBy($sortby, $sort['dir'])

            ->get();

        $data = Datatables::of($query)

            ->addColumn('date', function($query){
                return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('number', function($query){
                $zeros = '';
                if($query->zero_padding > 0)
                {
                    for($i=0;$i<$query->zero_padding;$i++)
                    {
                        $zeros .= '0';
                    }
                }
                return $query->prefix . $zeros . $query->number . $query->suffix;
            })

            ->addColumn('ledger', function($query){
                return Entry::entryLedgers($query->eid,1);
            })

            ->addColumn('etname', function($query){
                return $query->etname;
            })

            ->addColumn('tag_id', function($query){
                if($query->tid)
                    $tag = '<span class="acc_tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->tid) . '" style="color:#'. $query->color .' !important;">'. $query->tname .'</a></span>';
                else
                    $tag = '';
                return $tag;
            })

            ->addColumn('dr_total', function($query){
                return Helpers::toCurrency('D',$query->dr_total);
            })

            ->addColumn('cr_total', function($query){
                return Helpers::toCurrency('C',$query->cr_total);
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_restore_entry'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete restore' data-toggle='tooltip' title='Restore' style='$de' href='". url('/admin/accounting/restore-entry/'.$query->eid) . "'><i class='fa fa-undo'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('ledger','tag_id','status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function view($id)
    {
        $entry = Entry::find($id);
        $entryitems = DB::table('acc_entryitems')
                        ->join('acc_ledgers','acc_ledgers.id','acc_entryitems.ledger_id')
                        ->join('acc_entries','acc_entries.id','acc_entryitems.entry_id')
                        ->where('acc_entryitems.entry_id',$id)
                        ->where('acc_entries.deleted_at',NULL)
                        ->where('acc_entryitems.deleted_at',NULL)
                        ->where('acc_entries.status',1)
                        ->where('acc_entryitems.status',1)
                        ->where('acc_entries.module_id',Session::get('module'))
                        ->select(DB::Raw('acc_entryitems.*,acc_ledgers.name lname,acc_entries.dr_total,acc_entries.cr_total'))
                        ->get();
        $curEntryitems = array();
        foreach ($entryitems as $row => $data) {
            if ($data->dc == 'D') {
                $curEntryitems[$row] = array(
                    'dc' => $data->dc,
                    'ledger_id' => $data->ledger_id,
                    'ledger_name' => $data->lname,
                    'dr_amount' => $data->amount,
                    'cr_amount' => '',
                );
            } else {
                $curEntryitems[$row] = array(
                    'dc' => $data->dc,
                    'ledger_id' => $data->ledger_id,
                    'ledger_name' => $data->lname,
                    'dr_amount' => '',
                    'cr_amount' => $data->amount,
                );
            }
        }
        $tag = $entry->tag;
        //dd($entry->tag);
        if($entry->tag != NULL)
            $tag = '<span class="acc_tag" style="color:#'. $tag->color .' !important; background-color:#'. $tag->background .' !important;"><a href="'. url('/admin.accounting/entries/'.$tag->id) . '" style="color:#'. $tag->color .' !important;">'. $tag->title .'</a></span>';

        return view('admin.accounting.view-entry',array('entry'=>$entry,'tag'=>$tag))->with('entryitems',$curEntryitems);
    }

    public function add($id)
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $entrytype = App\Accounting\EntryType::find($id);

            $tags = Tag::getAll();

            /* Ledger selection */
            $ledgers = new App\Accounting\LedgerTree();
            $ledgers->current_id = -1;
            $ledgers->restriction_bankcash = $entrytype->restriction_bankcash;
            $ledgers->build(0);
            //dd($ledgers);
            $ledgers->toList($ledgers, -1);
            $ledgers_disabled = array();
            foreach ($ledgers->ledgerList as $row => $data) {
                if ($row < 0) {
                    $ledgers_disabled[] = $row;
                }
            }

            $curEntryitems = array();
            if ($entrytype->restriction_bankcash == 3) {
                /* Special case if atleast one Bank or Cash on credit side (3) then 1st item is Cr */
                $curEntryitems[0] = array('dc' => 'C');
                $curEntryitems[1] = array('dc' => 'D');
            } else {
                /* Otherwise 1st item is Dr */
                $curEntryitems[0] = array('dc' => 'D');
                $curEntryitems[1] = array('dc' => 'C');
            }
            $curEntryitems[2] = array('dc' => 'D');
            $curEntryitems[3] = array('dc' => 'D');
            $curEntryitems[4] = array('dc' => 'D');

            return view('admin.accounting.add-entry', array('ledger_options' => $ledgers->ledgerList, 'ledgers_disabled' => $ledgers_disabled, 'curEntryitems' => $curEntryitems, 'tags' => $tags))->with('entrytype', $entrytype);
        }
    }

    public function edit($id)
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $entry = Entry::find($id);
            $entrytype = App\Accounting\EntryType::find($entry->entrytype_id);
            $tags = Tag::getAll();

            /* Ledger selection */
            $ledgers = new App\Accounting\LedgerTree();
            $ledgers->current_id = -1;
            $ledgers->restriction_bankcash = $entrytype->restriction_bankcash;
            $ledgers->build(0);
            //dd($ledgers);
            $ledgers->toList($ledgers, -1);
            $ledgers_disabled = array();
            foreach ($ledgers->ledgerList as $row => $data) {
                if ($row < 0) {
                    $ledgers_disabled[] = $row;
                }
            }

            $curEntryitemsData = EntryItem::getEI($id);
            //dd($curEntryitemsData);
            $curEntryitems = array();
            foreach ($curEntryitemsData as $row => $data) {
                if ($data->dc == 'D') {
                    $curEntryitems[$row] = array(
                        'dc' => $data->dc,
                        'ledger_id' => $data->ledger_id,
                        'dr_amount' => $data->amount,
                        'cr_amount' => '',
                    );
                } else {
                    $curEntryitems[$row] = array(
                        'dc' => $data->dc,
                        'ledger_id' => $data->ledger_id,
                        'dr_amount' => '',
                        'cr_amount' => $data->amount,
                    );
                }
            }
            $curEntryitems[] = array('dc' => 'D');
            $curEntryitems[] = array('dc' => 'D');
            $curEntryitems[] = array('dc' => 'D');

            return view('admin.accounting.edit-entry', array('ledger_options' => $ledgers->ledgerList, 'ledgers_disabled' => $ledgers_disabled, 'curEntryitems' => $curEntryitems,'tags' => $tags, 'entry' => $entry))->with('entrytype', $entrytype);
        }
    }

    public function save(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        /*$entry = Entry::where([['number',$request->number],['module_id',Session::get('module')]])->get();
        if(count($entry) > 0)
            $validator->errors()->add($request->number,'field', 'The number has already been taken.');*/

        if ($validator->fails())
            return back()->withErrors($validator->errors()->all());

        //dd($request);

        /* Check ledger restriction */
        $dc_valid = false;
        //print_r($this->request->data['Entryitem']);
        $entryitems = $request->entryitems;
        foreach ($entryitems as $row => $entryitem) {
            //dd($entryitem['ledger_id']);
            if ($entryitem['ledger_id'] <= 0) {
                continue;
            }
            $ledger = App\Accounting\Ledger::find($entryitem['ledger_id']);
            //print_r($ledger);
            if (!$ledger) {
                $validator->errors()->add($ledger,'field', 'Invalid ledger selected.');
                return back()->withErrors($validator->errors()->all());
            }

            if ($request->restriction_bankcash == 4) {
                if ($ledger->type != 1) {
                    $validator->errors()->add($ledger->type,'Only bank or cash ledgers are allowed for this entry type.');
                    return back()->withErrors($validator->errors()->all());
                }
            }
            if ($request->restriction_bankcash == 5) {
                if ($ledger->type == 1) {
                    $validator->errors()->add($ledger->type,'Bank or cash ledgers are not allowed for this entry type.');
                    return back()->withErrors($validator->errors()->all());
                }
            }

            if ($entryitem['dc'] == 'D') {
                if ($request->restriction_bankcash == 2) {
                    if ($ledger->type == 1) {
                        $dc_valid = true;
                    }
                }
            } else if ($entryitem['dc'] == 'C') {
                if ($request->restriction_bankcash == 3) {
                    if ($ledger->type == 1) {
                        $dc_valid = true;
                    }
                }
            }
        }
        //echo $dc_valid;
        if ($request->restriction_bankcash == 2) {
            if (!$dc_valid) {
                $validator->errors()->add($dc_valid,'Atleast one bank or cash ledger has to be on debit side for this entry type.');
                return back()->withErrors($validator->errors()->all());
            }
        }
        if ($request->restriction_bankcash == 3) {
            if (!$dc_valid) {
                $validator->errors()->add($dc_valid,'Atleast one bank or cash ledger has to be on credit side for this entry type.');
                return back()->withErrors($validator->errors()->all());
            }
        }

        $dr_total = 0;
        $cr_total = 0;

        /* Check equality of debit and credit total */
        foreach ($entryitems as $row => $entryitem) {
            if ($entryitem['ledger_id'] <= 0) {
                continue;
            }

            if ($entryitem['dc'] == 'D') {
                if ($entryitem['dr_amount'] <= 0) {
                    $validator->errors()->add($entryitem['dr_amount'],'Invalid amount specified. Amount cannot be negative or zero.');
                    return back()->withErrors($validator->errors()->all());
                }
                if (Helpers::countDecimal($entryitem['dr_amount']) > 2) {
                    $validator->errors()->add($entryitem['dr_amount'],'Invalid amount specified. Maximum 2 decimal places allowed.');
                    return back()->withErrors($validator->errors()->all());
                }
                $dr_total = Helpers::calculate($dr_total, $entryitem['dr_amount'], '+');
            } else if ($entryitem['dc'] == 'C') {
                if ($entryitem['cr_amount'] <= 0) {
                    $validator->errors()->add($entryitem['cr_amount'],'Invalid amount specified. Amount cannot be negative or zero.');
                    return back()->withErrors($validator->errors()->all());
                }
                if (Helpers::countDecimal($entryitem['cr_amount']) > 2) {
                    $validator->errors()->add($entryitem['cr_amount'],'Invalid amount specified. Maximum 2 decimal places allowed.');
                    return back()->withErrors($validator->errors()->all());
                }
                $cr_total = Helpers::calculate($cr_total, $entryitem['cr_amount'], '+');
            } else {
                $validator->errors()->add('','Invalid Dr/Cr option selected.');
                return back()->withErrors($validator->errors()->all());
            }
        }
        if (Helpers::calculate($dr_total, $cr_total, '!=')) {
            $validator->errors()->add('','Debit and Credit total do not match.');
            return back()->withErrors($validator->errors()->all());
        }

        $entry =  new Entry();
        $lastentry = Entry::orderBy('number', 'desc')->first();
        $entry->number = $lastentry->number + 1;
        $entry->date = date('Y-m-d', strtotime($request->date));
        $entry->narration = $request->narration;
        $entry->tag_id = $request->tag;
        $entry->entrytype_id = $id;
        $entry->module_id = Session::get('module');
        $entry->dr_total = $dr_total;
        $entry->cr_total = $cr_total;
        $entry->status = 1;
        $entry->created_by = Auth::user()->id;
        $entry->created_at = date('Y-m-d H:i:s');
        $entry->updated_by = Auth::user()->id;
        $entry->updated_at = date('Y-m-d H:i:s');
        $entry->save();

        foreach ($entryitems as $row => $entryitem) {
            $amount = 0;
            if($entryitem['ledger_id'] != 0) {
                if ($entryitem['dc'] == 'D' and isset($entryitem['dr_amount']))
                    $amount = $entryitem['dr_amount'];
                elseif ($entryitem['dc'] == 'C' and isset($entryitem['cr_amount']))
                    $amount = $entryitem['cr_amount'];

                $eitem = new App\Accounting\EntryItem();
                $eitem->module_id = Session::get('module');
                $eitem->entry_id = $entry->id;
                $eitem->ledger_id = $entryitem['ledger_id'];
                $eitem->dc = $entryitem['dc'];
                $eitem->amount = $amount;
                $eitem->status = 0;
                $eitem->created_by = Auth::user()->id;
                $eitem->created_at = date('Y-m-d H:i:s');
                $eitem->updated_by = Auth::user()->id;
                $eitem->updated_at = date('Y-m-d H:i:s');
                $eitem->save();
            }
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Entry has been created successfully.');
        return Redirect::to('/admin/accounting/entries');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails())
            return back()->withErrors($validator->errors()->all());

        /* Check ledger restriction */
        $dc_valid = false;
        $entryitems = $request->entryitems;
        foreach ($entryitems as $row => $entryitem) {
            //dd($entryitem['ledger_id']);
            if ($entryitem['ledger_id'] <= 0) {
                continue;
            }
            $ledger = App\Accounting\Ledger::find($entryitem['ledger_id']);
            //print_r($ledger);
            if (!$ledger) {
                $validator->errors()->add($ledger,'field', 'Invalid ledger selected.');
                return back()->withErrors($validator->errors()->all());
            }

            if ($request->restriction_bankcash == 4) {
                if ($ledger->type != 1) {
                    $validator->errors()->add($ledger->type,'Only bank or cash ledgers are allowed for this entry type.');
                    return back()->withErrors($validator->errors()->all());
                }
            }
            if ($request->restriction_bankcash == 5) {
                if ($ledger->type == 1) {
                    $validator->errors()->add($ledger->type,'Bank or cash ledgers are not allowed for this entry type.');
                    return back()->withErrors($validator->errors()->all());
                }
            }

            if ($entryitem['dc'] == 'D') {
                if ($request->restriction_bankcash == 2) {
                    if ($ledger->type == 1) {
                        $dc_valid = true;
                    }
                }
            } else if ($entryitem['dc'] == 'C') {
                if ($request->restriction_bankcash == 3) {
                    if ($ledger->type == 1) {
                        $dc_valid = true;
                    }
                }
            }
        }
        //echo $dc_valid;
        if ($request->restriction_bankcash == 2) {
            if (!$dc_valid) {
                $validator->errors()->add($dc_valid,'Atleast one bank or cash ledger has to be on debit side for this entry type.');
                return back()->withErrors($validator->errors()->all());
            }
        }
        if ($request->restriction_bankcash == 3) {
            if (!$dc_valid) {
                $validator->errors()->add($dc_valid,'Atleast one bank or cash ledger has to be on credit side for this entry type.');
                return back()->withErrors($validator->errors()->all());
            }
        }

        $dr_total = 0;
        $cr_total = 0;

        /* Check equality of debit and credit total */
        foreach ($entryitems as $row => $entryitem) {
            if ($entryitem['ledger_id'] <= 0) {
                continue;
            }

            if ($entryitem['dc'] == 'D') {
                if ($entryitem['dr_amount'] <= 0) {
                    $validator->errors()->add($entryitem['dr_amount'],'Invalid amount specified. Amount cannot be negative or zero.');
                    return back()->withErrors($validator->errors()->all());
                }
                if (Helpers::countDecimal($entryitem['dr_amount']) > 2) {
                    $validator->errors()->add($entryitem['dr_amount'],'Invalid amount specified. Maximum 2 decimal places allowed.');
                    return back()->withErrors($validator->errors()->all());
                }
                $dr_total = Helpers::calculate($dr_total, $entryitem['dr_amount'], '+');
            } else if ($entryitem['dc'] == 'C') {
                if ($entryitem['cr_amount'] <= 0) {
                    $validator->errors()->add($entryitem['cr_amount'],'Invalid amount specified. Amount cannot be negative or zero.');
                    return back()->withErrors($validator->errors()->all());
                }
                if (Helpers::countDecimal($entryitem['cr_amount']) > 2) {
                    $validator->errors()->add($entryitem['cr_amount'],'Invalid amount specified. Maximum 2 decimal places allowed.');
                    return back()->withErrors($validator->errors()->all());
                }
                $cr_total = Helpers::calculate($cr_total, $entryitem['cr_amount'], '+');
            } else {
                $validator->errors()->add('','Invalid Dr/Cr option selected.');
                return back()->withErrors($validator->errors()->all());
            }
        }
        if (Helpers::calculate($dr_total, $cr_total, '!=')) {
            $validator->errors()->add('','Debit and Credit total do not match.');
            return back()->withErrors($validator->errors()->all());
        }

        $entry =  Entry::find($id);
        /*$entry->number = $request->number;*/
        $entry->date = date('Y-m-d', strtotime($request->date));
        $entry->narration = $request->narration;
        $entry->tag_id = $request->tag;
        $entry->entrytype_id = $request->entrytype_id;
        $entry->module_id = Session::get('module');
        $entry->dr_total = $dr_total;
        $entry->cr_total = $cr_total;
        $entry->status = $request->status;
        $entry->updated_by = Auth::user()->id;
        $entry->updated_at = date('Y-m-d H:i:s');
        $entry->save();

        DB::table('acc_entryitems')->where('entry_id', '=', $id)->delete();

        foreach ($entryitems as $row => $entryitem) {
            $amount = 0;
            if($entryitem['ledger_id'] != 0) {
                if ($entryitem['dc'] == 'D' and isset($entryitem['dr_amount']))
                    $amount = $entryitem['dr_amount'];
                elseif ($entryitem['dc'] == 'C' and isset($entryitem['cr_amount']))
                    $amount = $entryitem['cr_amount'];

                $eitem = new App\Accounting\EntryItem();
                $eitem->module_id = Session::get('module');
                $eitem->entry_id = $entry->id;
                $eitem->ledger_id = $entryitem['ledger_id'];
                $eitem->dc = $entryitem['dc'];
                $eitem->amount = $amount;
                /*$eitem->status = $request->status;*/
                $eitem->updated_by = Auth::user()->id;
                $eitem->updated_at = date('Y-m-d H:i:s');
                $eitem->save();
            }
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Entry has been created successfully.');
        return Redirect::to('/admin/accounting/entries');
    }

    public function updpstatus(Request $request)
    {
        //\DB::insert('update provider_packages set status='. $request->status .' where id = ' . $request->id);
        DB::table('acc_entries')
            ->where('id', $request->id)
            ->update(['status' => $request->status]);

        DB::table('acc_entryitems')
            ->where('entry_id', $request->id)
            ->update(['status' => $request->status]);
    }

    public function delete($id)
    {
        $eitems = EntryItem::getEI($id);
        if(count($eitems) > 0)
        {
            foreach($eitems as $eitem)
            {
                $item = EntryItem::find($eitem);
                $item->delete();
            }
        }
        $entry = Entry::find($id);
        $entry->delete();
        //DB::table('acc_entryitems')->where('entry_id', '=', $id)->delete();
        //DB::table('acc_entries')->where('id', '=', $id)->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Entry has been deleted successfully.');
        return Redirect::to('/admin/accounting/entries');
    }

    public function restore($id)
    {
        $entryitem = EntryItem::withTrashed()->where('entry_id',$id)->restore();
        $entry = Entry::withTrashed()->find($id)->restore();

        Session::flash('status', 'success');
        Session::flash('message', 'Entry has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-entries');
    }

    public function addrow(Request $request) {
        /* Ledger selection */
        $ledgers = new App\Accounting\LedgerTree();
        $ledgers->current_id = -1;
        $ledgers->restriction_bankcash = $request->restriction_bankcash;
        $ledgers->build(0);
        $ledgers->toList($ledgers, -1);
        $ledgers_disabled = array();

        $ledger_options = $ledgers->ledgerList;
        
        // Generate a random id to use in below form array
        $i = time() + rand  (0, time()) + rand  (0, time()) + rand  (0, time());
        $row = (int)$request->row_id + 1;

        $newrow = '<tr class="ajax-add">
                    <td><div class="form-group-entryitem">
                        <select name="dc_'. $row .'" class="form-control dc-dropdown" id="dc_'. $row .'">
                            <option value="D">Dr</option>
                            <option value="C">Cr</option>
                        </select></div>
                    </td>
                    <td><div class="form-group-entryitem">
                        <select name="ledger_'. $row  .'" id="ledger_'. $row  .'" class="form-control ledger-dropdown">';
                            foreach($ledger_options as $key => $val) {
                                $newrow .= '<option value="' . $key . '">' . $val . '</option>';
                            }
        $newrow .= '</select></div>
                    </td>
                    <td><div class="form-group-entryitem">
                        <input name="dr_'. $row .'" class="form-control dr-item" type="text" id="dr_'. $row .'" disabled="">
                        </div>
                    </td>
                    <td><div class="form-group-entryitem">
                        <input name="cr_'. $row .'" class="form-control cr-item" type="text" id="cr_'. $row .'" disabled="">
                        </div>
                    </td>
                    <td style="text-align: center;">
                        <a class="addrow" data-id="'. $row .'"><i class="glyphicon glyphicon-plus"></i> Add</a>
                        <span class="link-pad"></span>
                        <a class="deleterow"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    </td>
                    <td class="ledger-balance"><div></div></td>
                </tr>';

        return response()->json($newrow, 200);
    }
}