<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\EntryType;
use App\Role;
use DB;
use Carbon;
use Validator;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('acc-dashboard');
        if(!$rights->can_view){
    	    abort(403);
        }
        //print_r(URL::previous());
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $module = App\Accounting\Module::find(Session::get('module'));
            //dd($module);
            $ledgers = App\Accounting\Ledger::getCBLedgers();
            $ledgersCB = array();
            foreach ($ledgers as $ledger) {
                $ledgersCB[] = array(
                    'name' => Helpers::toCodeWithName($ledger->code, $ledger->name),
                    'balance' => App\Accounting\Ledger::closingBalance($ledger->id),
                );
            }
            /* Account summary */
            $assets = new App\Accounting\AccountList();
            $assets->only_opening = false;
            $assets->start_date = null;
            $assets->end_date = null;
            $assets->affects_gross = -1;
            $group = App\Accounting\Group::where([['name','Assets'],['module_id',session('module')]])->get();
            $assets->start($group[0]->id);
            //dd($assets);

            $liabilities = new App\Accounting\AccountList();
            $liabilities->only_opening = false;
            $liabilities->start_date = null;
            $liabilities->end_date = null;
            $liabilities->affects_gross = -1;
            $group = App\Accounting\Group::where([['name','Liabilities and Owners Equity'],['module_id',session('module')]])->get();
            $liabilities->start($group[0]->id);

            $income = new App\Accounting\AccountList();
            $income->only_opening = false;
            $income->start_date = null;
            $income->end_date = null;
            $income->affects_gross = -1;
            $group = App\Accounting\Group::where([['name','Incomes'],['module_id',session('module')]])->get();
            $income->start($group[0]->id);

            $expense = new App\Accounting\AccountList();
            $expense->only_opening = false;
            $expense->start_date = null;
            $expense->end_date = null;
            $expense->affects_gross = -1;
            $group = App\Accounting\Group::where([['name','Expenses'],['module_id',session('module')]])->get();
            $expense->start($group[0]->id);

            $accsummary = array(
                'assets_total_dc' => $assets->cl_total_dc,
                'assets_total' => $assets->cl_total,
                'liabilities_total_dc' => $liabilities->cl_total_dc,
                'liabilities_total' => $liabilities->cl_total,
                'income_total_dc' => $income->cl_total_dc,
                'income_total' => $income->cl_total,
                'expense_total_dc' => $expense->cl_total_dc,
                'expense_total' => $expense->cl_total,
            );
            return view('admin.accounting.acc-dashboard',array('ledgers' => $ledgersCB, 'accsummary' => $accsummary, 'module' => $module));
        }
    }
}