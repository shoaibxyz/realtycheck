<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use App\Accounting\Tag;
use App\Accounting\Entry;
use DB;
use Carbon;
use Validator;
use Auth;

class SearchController extends Controller
{
    public function index()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {

            $entrytypes = App\Accounting\EntryType::getAll();

            $tags = Tag::getAll();

            /* Ledger selection */
            $ledgers = new App\Accounting\LedgerTree();
            $ledgers->current_id = -1;
            $ledgers->restriction_bankcash = 1;
            $ledgers->default_text = '(ALL)';
            $ledgers->build(0);
            //dd($ledgers);
            $ledgers->toList($ledgers, -1);
            $ledgers_disabled = array();
            foreach ($ledgers->ledgerList as $row => $data) {
                if ($row < 0) {
                    $ledgers_disabled[] = $row;
                }
            }
            return view('admin.accounting.acc-search',array('tags'=>$tags,'ledgers'=>$ledgers->ledgerList,'ledgers_disabled'=>$ledgers_disabled))->with('entrytypes',$entrytypes);
        }
    }

    public function showsearch_results(Request $request)
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $ledgers = '';
            if ($request->ledgers)
                $ledgers = implode(',', $request->ledgers);

            $entrytypes = '';
            if ($request->entrytypes)
                $entrytypes = implode(',', $request->entrytypes);
            $tags = '';
            if ($request->tags)
                $tags = implode(',', $request->tags);
            return view('admin.accounting.search_results', array('ledgers' => $ledgers, 'entrytypes' => $entrytypes, 'tags' => $tags))->with('req', $request);
        }
    }

    public function getSearchResults(Request $request)
    {
        $ledgers = '';
        if($request->ledgers)
            $ledgers = $request->ledgers;

        $entrytypes = '';
        if($request->entrytypes)
            $entrytypes = $request->entrytypes;

        $tags = '';
        if($request->tags)
            $ledgers = $request->tags;

        $number = $request->number;
        $number2 = $request->number2;
        $amount = $request->amount;
        $amount2 = $request->amount2;
        $number_restrict = $request->number_restrict;
        $drcr_restrict = $request->drcr_restrict;
        $amount_restrict = $request->amount_restrict;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $narration = $request->narration;

        $query = DB::table('acc_entries')
            ->join('acc_entryitems','acc_entryitems.entry_id','acc_entries.id')
            ->join('acc_entrytypes','acc_entrytypes.id','acc_entries.entrytype_id')
            ->leftJoin('acc_tags','acc_tags.id','acc_entries.tag_id')
            ->select(DB::Raw('acc_entries.*,acc_entryitems.id aid,acc_entryitems.entry_id,acc_entryitems.ledger_id,acc_entryitems.amount,acc_entryitems.dc,
                            acc_entryitems.reconciliation_date,acc_entrytypes.name as etname, acc_tags.title as tname, acc_tags.color,acc_tags.background'))
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            /*->where('acc_entryitems.deleted_at',NULL)
            ->where('acc_entryitems.status',1)
            ->where('acc_entrytypes.deleted_at',NULL)
            ->where('acc_entrytypes.status',1)*/
            ->where('acc_entries.module_id',Session::get('module'))
            ->distinct()
            ->orderBy('acc_entries.date', 'asc')

            ->when($ledgers, function ($query) use ($ledgers) {
                return $query->where('acc_entryitems.ledger_id', 'like', "%{$ledgers}%");
            })

            ->when($entrytypes, function ($query) use ($entrytypes) {
                return $query->where('acc_entries.entrytype_id', 'like', "%{$entrytypes}%");
            })

            ->when($tags, function ($query) use ($tags) {
                return $query->where('acc_entries.tag_id', 'like', "%{$tags}%");
            })

            ->when($number, function ($query) use ($number, $number2, $number_restrict) {
                if (!empty($number)) {
                    if ($number_restrict == 1) {
                        /* Equal to */
                        return $query->where('acc_entries.number', $number);
                    } else if ($number_restrict == 2) {
                        /* Less than or equal to */
                        return $query->where('acc_entries.number', '<=', $number);
                    } else if ($number_restrict == 3) {
                        /* Greater than or equal to */
                        return $query->where('acc_entries.number', '>=', $number);
                    } else if ($number_restrict == 4) {
                        /* In between */
                        if (!empty($number1) and !empty($number2)) {
                            return $query->whereBetween('acc_entries.number', [$number,$number2]);
                        }
                    }
                }
            })

            ->when($drcr_restrict, function ($query) use ($drcr_restrict) {
                return $query->where('acc_entryitems.dc', $drcr_restrict);
            })

            ->when($amount, function ($query) use ($amount, $amount2, $amount_restrict) {
                if (!empty($amount)) {
                    if ($amount_restrict == 1) {
                        /* Equal to */
                        return $query->where('acc_entryitems.amount', $amount);
                    } else if ($amount_restrict == 2) {
                        /* Less than or equal to */
                        return $query->where('acc_entryitems.amount', '<=', $amount);
                    } else if ($amount_restrict == 3) {
                        /* Greater than or equal to */
                        return $query->where('acc_entryitems.amount', '>=', $amount);
                    } else if ($amount_restrict == 4) {
                        /* In between */
                        if (!empty($amount1) and !empty($amount2)) {
                            return $query->whereBetween('acc_entryitems.amount', [$amount,$amount2]);
                        }
                    }
                }
            })

            ->when($from_date, function ($query) use ($from_date) {
                $fromdate = date('Y-m-d',strtotime($from_date));
                return $query->where('acc_entries.date','>=', $fromdate);
            })

            ->when($to_date, function ($query) use ($to_date) {
                $todate = date('Y-m-d',strtotime($to_date));
                return $query->where('acc_entries.date','<=', $todate);
            })

            ->when($narration, function ($query) use ($narration) {
                return $query->where('acc_entries.narration', 'like', "%{$narration}%");
            })

            ->get();

        $data = Datatables::of($query)

            ->addColumn('date', function($query){
                return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('number', function($query){
                return Helpers::toEntryNumber($query->number, $query->entrytype_id);
                //return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('ledger', function($query){
                return Entry::entryLedgers($query->id,0);
            })

            ->addColumn('etname', function($query){
                return $query->etname;
            })

            ->addColumn('tag_id', function($query){
                if($query->tag_id)
                    $tag = '<span class="tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->tag_id) . '" style="color:#'. $query->color .' !important;">'. $query->tname .'</a></span>';
                else
                    $tag = '';
                return $tag;
            })

            ->addColumn('dr_total', function($query){
                return Helpers::toCurrency('D',$query->amount);
            })

            ->addColumn('cr_total', function($query){
                return Helpers::toCurrency('C',$query->amount);
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_view_entry'))
                    $sv = "display:'inline-block'";
                else
                    $sv = 'display:none';
                if(App\User::getPermission('can_edit_entry'))
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if(App\User::getPermission('can_delete_entry'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-success btn-xs' style='$sv' href='". url('/admin/accounting/view-entry/'.$query->id) . "'><i class='fa fa-hand-o-left'></i>&nbsp;View</a>&nbsp;
                            <a type='button' class='btn btn-default btn-xs' style='$se' href='". url('/admin/accounting/edit-entry/'.$query->id) . "'><i class='fa fa-edit'></i>&nbsp;Edit</a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete' style='$de' href='". url('/admin/accounting/delete-entry/'.$query->id) . "'><i class='fa fa-trash-o'></i>&nbsp;Delete</a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('ledger','tag_id','status','btn_actions'))

            ->make(true);

        return $data;
    }
}