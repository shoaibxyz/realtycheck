<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Role;
use DB;
use Carbon;
use Validator;
use Auth;
use App\Accounting\Tag;

class TagsController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('tags');
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else
            return view('admin.accounting.tags', compact('rights'));
    }

    public function deleted_tags()
    {
        return view('admin.accounting.deleted-tags');
    }

    public function getAllTags(Request $request)
    {
        $columns = array(
            0 => 'title',
            1 => 'tag',
            2 => 'status',
            3 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_tags')
            ->select(DB::Raw('*'))
            ->where('deleted_at',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('title', function($query){
                return $query->title;
            })

            ->addColumn('tag', function($query){
                $tag = '<span class="acc_tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->id) . '" style="color:#'. $query->color .' !important;">'. $query->title .'</a></span>';
                return $tag;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                $rights = Role::getrights('tags');
                if($rights->show_edit)
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if($rights->show_delete)
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-default btn-xs edit' data-toggle='tooltip' title='Edit' style='$se' href='". url('/admin/accounting/edit-tag/'.$query->id) . "'><i class='fa fa-pencil'></i></a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete delete' data-toggle='tooltip' title='Delete' style='$de' href='". url('/admin/accounting/delete-tag/'.$query->id) . "'><i class='fa fa-times'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('tag','status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function getAllDelTags(Request $request)
    {
        $columns = array(
            0 => 'title',
            1 => 'status',
            2 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_tags')
            ->select(DB::Raw('*'))
            ->where('deleted_at','!=',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('title', function($query){
                return $query->title;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_restore_tag'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete restore' data-toggle='tooltip' title='Restore' style='$de' href='". url('/admin/accounting/restore-tag/'.$query->id) . "'><i class='fa fa-undo'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function add()
    {
        return view('admin.accounting.add-tag');
    }

    public function edit(Request $request, $id)
    {
        $tag = Tag::find($id);

        return view('admin.accounting.edit-tag',array('tag' => $tag));
    }

    public function save(Request $request)
    {
        $title = $request->title;
        $mid = Session::get('module');
        Validator::make($request->all(), [
            'title' => Rule::unique('acc_tags')->where(function ($query) use($title,$mid) {
                return $query->where('module_id', $mid);
            }),
        ])->validate();

        $tag = new Tag();
        $tag->title = $request->title;
        $tag->color = $request->color;
        $tag->background = $request->background;
        $tag->module_id = Session::get('module');
        $tag->status = $request->status;
        $tag->created_by = Auth::user()->id;
        $tag->created_at = date('Y-m-d H:i:s');
        $tag->updated_by = Auth::user()->id;
        $tag->updated_at = date('Y-m-d H:i:s');
        $tag->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Tag has been added successfully.');
        return Redirect::to('/admin/accounting/tags');
    }

    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->title = $request->title;
        $tag->color = $request->color;
        $tag->background = $request->background;
        $tag->status = $request->status;
        $tag->updated_by = Auth::user()->id;
        $tag->updated_at = date('Y-m-d H:i:s');
        $tag->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Tag has been updated successfully.');
        return Redirect::to('/admin/accounting/tags');
    }

    public function delete($id)
    {
        $childs = App\Accounting\Entry::where('tag_id',$id)->count();
        if ($childs > 0) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Tag cannot be deleted since one or more entries are still using it.']);
        }

        $tag = Tag::find($id);

        $tag->updated_by = Auth::user()->id;
        $tag->updated_at = date('Y-m-d H:i:s');
        $tag->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Tag has been deleted successfully.');
        return Redirect::to('/admin/accounting/tags');
    }

    public function restore($id)
    {
        $tag = Tag::withTrashed()->find($id);
        if($tag) {
            $tag->updated_by = Auth::user()->id;
            $tag->updated_at = date('Y-m-d H:i:s');
            $tag->restore();
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Tag has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-tags');
    }
}