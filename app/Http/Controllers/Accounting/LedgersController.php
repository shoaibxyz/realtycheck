<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use DB;
use Carbon;
use Validator;
use Auth;
use App\Accounting\Group;
use App\Accounting\Ledger;
use App\Accounting\GroupTree;

class LedgersController extends Controller
{
    public function index()
    {
        $parentGroups = new GroupTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        // dd($parentGroups);
        $parentGroups->toList($parentGroups, -1);
        // dd($parentGroups->groupList);
        return view('admin.accounting.add-ledger',array('parents' => $parentGroups->groupList));
    }

    public function deleted_ledgers()
    {
        return view('admin.accounting.deleted-ledgers');
    }

    public function getAllDelLedgers(Request $request)
    {
        $columns = array(
            0 => 'name',
            1 => 'status',
            2 => 'deleted_at',
            3 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_ledgers')
            ->select(DB::Raw('*'))
            ->where('deleted_at','!=',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('name', function($query){
                return $query->name;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->addColumn('deleted_at', function($query){
                return \Carbon\Carbon::parse($query->deleted_at)->format('d/m/Y');
            })

            ->editColumn('btn_actions', function($query){
                if(App\Models\User::getPermission('can_restore_ledger'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete restore' data-toggle='tooltip' title='Restore' style='$de' href='". url('/admin/accounting/restore-ledger/'.$query->id) . "'><i class='fa fa-undo'></i></a>
                            </div>";
                return $actions;
            })
            //->orderColumn('title')
            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function edit(Request $request, $id)
    {
        $parentGroups = new GroupTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        //dd($parentGroups);
        $parentGroups->toList($parentGroups, -1);
        //dd($parentGroups->groupList);
        $ledger = Ledger::find($id);

        return view('admin.accounting.edit-ledger',array('parents' => $parentGroups->groupList, 'ledger' => $ledger));
    }

    public function save(Request $request)
    {
        $mid = Session::get('module');
        Validator::make($request->all(), [
            'name' => Rule::unique('acc_ledgers')->where(function ($query) use($mid) {
                return $query->where('module_id', $mid);
            }),
        ])->validate();

        $op_balance = 0;
        /* Count number of decimal places */
        if(empty($request->op_balance) || $request->op_balance == 0)
            $op_balance = 0;
        elseif (Helpers::countDecimal($request->op_balance) > 2) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Invalid amount specified. Maximum 2 decimal places allowed.']);
        }
        else
            $op_balance = $request->op_balance;

        $ledger = new Ledger();
        $ledger->group_id = $request->group;
        $ledger->module_id = Session::get('module');
        $ledger->op_balance = $op_balance;
        $ledger->op_balance_dc = $request->op_balance_dc;
        $ledger->name = $request->name;
        $ledger->code = $request->code;
        $ledger->type = ($request->type == 'on') ? 1 : 0;
        $ledger->reconciliation = ($request->reconciliation == 'on') ? 1 : 0;
        $ledger->notes = $request->notes;
        $ledger->status = 1;
        $ledger->created_by = Auth::user()->id;
        $ledger->created_at = date('Y-m-d H:i:s');
        $ledger->updated_by = Auth::user()->id;
        $ledger->updated_at = date('Y-m-d H:i:s');
        $ledger->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Ledger has been added successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function update(Request $request, $id)
    {
        $op_balance = 0;
        /* Count number of decimal places */
        if(empty($request->op_balance) || $request->op_balance == 0)
            $op_balance = 0;
        elseif (Helpers::countDecimal($request->op_balance) > 2)
            return redirect()->back()->withInput()->withErrors(['message' => 'Invalid amount specified. Maximum 2 decimal places allowed.']);
        else
            $op_balance = $request->op_balance;

        $ledger = Ledger::find($id);
        $ledger->group_id = $request->group;
        $ledger->op_balance = $op_balance;
        $ledger->op_balance_dc = $request->op_balance_dc;
        $ledger->name = $request->name;
        $ledger->code = $request->code;
        $ledger->type = ($request->ledger_type == 'on') ? 1 : 0;
        $ledger->reconciliation = ($request->reconciliation == 'on') ? 1 : 0;
        $ledger->notes = $request->notes;
        /* $ledger->status = $request->status; */
        $ledger->updated_by = Auth::user()->id;
        $ledger->updated_at = date('Y-m-d H:i:s');
        $ledger->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Ledger has been updated successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function delete($id)
    {
        /* Check if any entry item using this ledger still exists */
        $entries = count(App\Accounting\EntryItem::getLE($id));
        //dd($entries);
        if ($entries > 0) {
            Session::flash('status', 'error');
            Session::flash('message', 'Account ledger cannot not be deleted since it has one or more entries still present.');
            return redirect('/admin/accounting/accounts')->with('message', 'Account ledger cannot not be deleted since it has one or more entries still present.');
            //return back()->with('message' , 'Account ledger cannot not be deleted since it has one or more entries still present.');
        }

        $ledger = Ledger::find($id);

        $ledger->updated_by = Auth::user()->id;
        $ledger->updated_at = date('Y-m-d H:i:s');
        $ledger->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Ledger has been deleted successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function restore($id)
    {
        $ledger = Ledger::withTrashed()->find($id);
        if($ledger) {
            $ledger->updated_by = Auth::user()->id;
            $ledger->updated_at = date('Y-m-d H:i:s');
            $ledger->restore();
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Ledger has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-ledgers');
    }

    public function cl(Request $request)
    {
        /* Read ledger id from url get request */
        $id = (int)$request->id;

        /* Check if ledger exists */
        $ledger = Ledger::find($request->id);
        if (!$ledger) {
            return response()->json(['cl' => array('dc' => '', 'amount' => '')], 200);
        }

        $cl = Ledger::closingBalance($id);

        $status = 'ok';

        /* If its a cash or bank account and closing balance is Cr then negative balance */
        if ($ledger->type == 1) {
            if ($cl['dc'] == 'C') {
                $status = 'neg';
            }
        }

        /* Return closing balance */
        return response()->json(['cl' => array(
            'dc' => $cl['dc'],
            'amount' => $cl['amount'],
            'status' => $status,
        )], 200);
    }
}