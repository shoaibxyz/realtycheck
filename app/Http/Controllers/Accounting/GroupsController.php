<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use App\Role;
use DB;
use Carbon;
use Validator;
use Auth;
use App\Accounting\Group;
use App\Accounting\Ledger;
use App\Accounting\GroupTree;

class GroupsController extends Controller
{
    /*public function __construct()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin.accounting/select-module');
    }*/

    public function index()
    {
        $rights = Role::getrights('group');
        if(!$rights->can_create){
    	    abort(403);
        }
        $parentGroups = new GroupTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        //dd($parentGroups->groupList);
        return view('admin.accounting.add-group',array('parents' => $parentGroups->groupList));
    }

    public function deleted_groups()
    {
        return view('admin.accounting.deleted-groups');
    }

    public function getAllDelGroups(Request $request)
    {
        $columns = array(
            0 => 'name',
            1 => 'status',
            2 => 'deleted_at',
            3 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_groups')
            ->select(DB::Raw('*'))
            ->where('deleted_at','!=',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('name', function($query){
                return $query->name;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->addColumn('deleted_at', function($query){
                return \Carbon\Carbon::parse($query->deleted_at)->format('d/m/Y');
            })

            ->editColumn('btn_actions', function($query){
                $rights = Role::getrights('group');
                if($rights->show_restore)
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete' style='$de' href='". url('/admin/accounting/restore-group/'.$query->id) . "'><i class='fa fa-trash-o'></i>&nbsp;Restore</a>
                            </div>";
                return $actions;
            })
            //->orderColumn('title')
            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function edit(Request $request, $id)
    {
        $rights = Role::getrights('group');
        if(!$rights->can_edit){
    	    abort(403);
        }
        $parentGroups = new GroupTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        //dd($parentGroups);
        $parentGroups->toList($parentGroups, -1);
        //dd($parentGroups->groupList);
        $group = Group::find($id);

        return view('admin.accounting.edit-group',array('parents' => $parentGroups->groupList, 'group' => $group));
    }

    public function save(Request $request)
    {
        $mid = Session::get('module');
        Validator::make($request->all(), [
            'name' => Rule::unique('acc_groups')->where(function ($query) use($mid) {
                return $query->where('module_id', $mid);
            }),
        ])->validate();

        $group = new Group();
        $group->name = $request->name;
        $group->code = $request->code;
        $group->module_id = Session::get('module');
        $group->parent_id = $request->parent;
        $group->affects_gross = $request->rdo_affects;
        $group->status = $request->status;
        $group->created_by = Auth::user()->id;
        $group->created_at = date('Y-m-d H:i:s');
        $group->updated_by = Auth::user()->id;
        $group->updated_at = date('Y-m-d H:i:s');
        $group->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Group has been added successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function update(Request $request, $id)
    {
        if ($id <= 4) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Cannot edit basic account groups.']);
        }

        $group = Group::find($id);
        /* Check if group and parent group are not same */
        if ($id == $group->parent_id) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Account group and parent group cannot be same.']);
        }

        $group->name = $request->name;
        $group->code = $request->code;
        $group->parent_id = $request->parent;
        $group->affects_gross = $request->rdo_affects;
        $group->status = $request->status;
        $group->created_by = Auth::user()->id;
        $group->created_at = date('Y-m-d H:i:s');
        $group->updated_by = Auth::user()->id;
        $group->updated_at = date('Y-m-d H:i:s');
        $group->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Group has been updated successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function delete($id)
    {
        if ($id <= 4) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Cannot delete basic account groups.']);
        }

        /* Check if any child groups exists */
        $childs = Group::getAll($id,'id','asc');
        if (count($childs) > 0) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Account group cannot be deleted since it has one or more child group accounts still present.']);
        }

        $childs = Ledger::getGroupLedgers($id);
        if (count($childs) > 0) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Account group cannot not be deleted since it has one or more child ledger accounts still present.']);
        }

        $group = Group::find($id);

        $group->updated_by = Auth::user()->id;
        $group->updated_at = date('Y-m-d H:i:s');
        $group->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Account Group has been deleted successfully.');
        return Redirect::to('/admin/accounting/accounts');
    }

    public function restore($id)
    {
        $group = Group::withTrashed()->find($id);
        if($group) {
            $group->updated_by = Auth::user()->id;
            $group->updated_at = date('Y-m-d H:i:s');
            $group->restore();
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Group has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-groups');
    }
}