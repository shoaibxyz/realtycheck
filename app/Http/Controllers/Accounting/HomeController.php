<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Carbon;
use Validator;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        //Auth::user()->hasRole('agency');

        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else
            return view('admin.accounting.acc-dashboard');
    }
}
