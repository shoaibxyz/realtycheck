<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use App\Accounting\Tag;
use App\Accounting\Entry;
use App\Accounting\Module;
use DB;
use Carbon;
use Validator;
use Auth;

class ReportsController extends Controller
{

    public function __construct()
    {
        $subtitle = '';
        $opening_title = '';
        $closing_title = '';
        $reconcile_title = '';
        $ledger_id = '';
        $start_date = '';
        $end_date = '';
        $current_op = '';
    }

    public function balance_sheet()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $bsheet = $this->getBS();
            return view('admin.accounting.balance_sheet',array('subtitle' => $this->subtitle,'req' => ''))->with('bsheet',$bsheet);
        }
    }

    public function showbalance_sheet(Request $request)
    {
        //dd($request);
        $bsheet = $this->getBS($request->opening,$request->start_date,$request->end_date);
        return view('admin.accounting.balance_sheet',array('subtitle' => $this->subtitle,'req' => $request))->with('bsheet',$bsheet);
    }

    public function getBS($only_opening = '', $startdate = null, $enddate = null)
    {
        echo $only_opening;
        if(empty($only_opening) and $startdate == null and $enddate == null)
        {
            $this->subtitle = 'Closing Balance Sheet as on ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');
        }
        else
        {
            if(!empty($only_opening))
            {
                $this->subtitle = 'Opening Balance Sheet as on ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y');
            }
            else
            {
                if(!empty($startdate) and !empty($enddate)) {
                    $this->subtitle = 'Balance Sheet from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse($enddate)->format('d-M-Y');
                }
                elseif(!empty($startdate))
                {
                    $this->subtitle = 'Balance Sheet from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');
                }
                elseif(!empty($enddate))
                {
                    $this->subtitle = 'Balance Sheet from ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse($enddate)->format('d-M-Y');
                }
            }
        }

        /**********************************************************************/
        /*********************** BALANCESHEET CALCULATIONS ********************/
        /**********************************************************************/

        /* Liabilities */
        $liabilities = new AccountList();
        $liabilities->only_opening = ($only_opening == '') ? false : true;
        $liabilities->start_date = $startdate;
        $liabilities->end_date = $enddate;
        $liabilities->affects_gross = -1;
        $group = App\Accounting\Group::where([['name','Liabilities and Owners Equity'],['module_id',session('module')]])->get();
        //dd($group[0]->id);
        $liabilities->start($group[0]->id);

        $bsheet['liabilities'] = $liabilities;

        $bsheet['liabilities_total'] = 0;
        if ($liabilities->cl_total_dc == 'C') {
            $bsheet['liabilities_total'] = $liabilities->cl_total;
        } else {
            $bsheet['liabilities_total'] = Helpers::calculate($liabilities->cl_total, 0, 'n');
        }

        /* Assets */
        $assets = new AccountList();
        $assets->only_opening = ($only_opening == '') ? false : true;
        $assets->start_date = $startdate;
        $assets->end_date = $enddate;
        $assets->affects_gross = -1;
        $group = App\Accounting\Group::where([['name','Assets'],['module_id',session('module')]])->get();
        //dd($group[0]->id);
        $assets->start($group[0]->id);

        $bsheet['assets'] = $assets;

        $bsheet['assets_total'] = 0;
        if ($assets->cl_total_dc == 'D') {
            $bsheet['assets_total'] = $assets->cl_total;
        } else {
            $bsheet['assets_total'] = Helpers::calculate($assets->cl_total, 0, 'n');
        }

        /* Profit and loss calculations */
        $income = new AccountList();
        $income->only_opening = ($only_opening == '') ? false : true;
        $income->start_date = $startdate;
        $income->end_date = $enddate;
        $income->affects_gross = -1;
        $group = App\Accounting\Group::where([['name','Incomes'],['module_id',session('module')]])->get();
        //dd($group[0]->id);
        $income->start($group[0]->id);

        $expense = new AccountList();
        $expense->only_opening = ($only_opening == '') ? false : true;
        $expense->start_date = $startdate;
        $expense->end_date = $enddate;
        $expense->affects_gross = -1;
        $group = App\Accounting\Group::where([['name','Expenses'],['module_id',session('module')]])->get();
        //dd($group[0]->id);
        $expense->start($group[0]->id);
        //dd($expense);

        if ($income->cl_total_dc == 'C') {
            $income_total = $income->cl_total;
        } else {
            $income_total = Helpers::calculate($income->cl_total, 0, 'n');
        }
        if ($expense->cl_total_dc == 'D') {
            $expense_total = $expense->cl_total;
        } else {
            $expense_total = Helpers::calculate($expense->cl_total, 0, 'n');
        }

        $bsheet['pandl'] = Helpers::calculate($income_total, $expense_total, '-');

        /* Difference in opening balance */
        $bsheet['opdiff'] = App\Accounting\Ledger::getOpeningDiff();
        if (Helpers::calculate($bsheet['opdiff']['opdiff_balance'], 0, '==')) {
            $bsheet['is_opdiff'] = false;
        } else {
            $bsheet['is_opdiff'] = true;
        }

        /**** Final balancesheet total ****/
        $bsheet['final_liabilities_total'] = $bsheet['liabilities_total'];
        $bsheet['final_assets_total'] = $bsheet['assets_total'];

        /* If net profit add to liabilities, if net loss add to assets */
        if (Helpers::calculate($bsheet['pandl'], 0, '>=')) {
            $bsheet['final_liabilities_total'] = Helpers::calculate(
                $bsheet['final_liabilities_total'],
                $bsheet['pandl'], '+');
        } else {
            $positive_pandl = Helpers::calculate($bsheet['pandl'], 0, 'n');
            $bsheet['final_assets_total'] = Helpers::calculate(
                $bsheet['final_assets_total'],
                $positive_pandl, '+');
        }

        /**
         * If difference in opening balance is Dr then subtract from
         * assets else subtract from liabilities
         */
        if ($bsheet['is_opdiff']) {
            if ($bsheet['opdiff']['opdiff_balance_dc'] == 'D') {
                $bsheet['final_assets_total'] = Helpers::calculate(
                    $bsheet['final_assets_total'],
                    $bsheet['opdiff']['opdiff_balance'], '+');
            } else {
                $bsheet['final_liabilities_total'] = Helpers::calculate(
                    $bsheet['final_liabilities_total'],
                    $bsheet['opdiff']['opdiff_balance'], '+');
            }
        }
        return $bsheet;
    }

    public function profit_loss()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $pl = $this->getPL();
            return view('admin.accounting.profit_loss',array('subtitle' => $this->subtitle,'req' => ''))->with('profitloss',$pl);
        }
    }

    public function showprofit_loss(Request $request)
    {
        //dd($request);
        $pl = $this->getPL($request->opening,$request->start_date,$request->end_date);
        return view('admin.accounting.profit_loss',array('subtitle' => $this->subtitle,'req' => $request))->with('profitloss',$pl);
    }

    public function getPL($only_opening = '', $startdate = null, $enddate = null)
    {
        if(empty($only_opening) and $startdate == null and $enddate == null)
        {
            $this->subtitle = 'Closing Trading and Profit & Loss Statement as on ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');
        }
        else
        {
            if(!empty($only_opening))
            {
                $this->subtitle = 'Opening Trading and Profit & Loss Statement as on ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y');
            }
            else
            {
                if(!empty($startdate) and !empty($enddate)) {
                    $this->subtitle = 'Trading and Profit & Loss Statement from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse($enddate)->format('d-M-Y');
                }
                elseif(!empty($startdate))
                {
                    $this->subtitle = 'Trading and Profit & Loss Statement from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');
                }
                elseif(!empty($enddate))
                {
                    $this->subtitle = 'Trading and Profit & Loss Statement from ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y') . ' to ' .
                        \Carbon\Carbon::parse($enddate)->format('d-M-Y');
                }
            }
        }

        /**********************************************************************/
        /*********************** GROSS CALCULATIONS ***************************/
        /**********************************************************************/

        /* Gross P/L : Expenses */
        $gross_expenses = new AccountList();
        $gross_expenses->only_opening = ($only_opening == '') ? false : true;
        $gross_expenses->start_date = $startdate;
        $gross_expenses->end_date = $enddate;
        $gross_expenses->affects_gross = 1;
        $group = App\Accounting\Group::where([['name','Expenses'],['module_id',session('module')]])->get();
        //dd($group[0]->id);
        $gross_expenses->start($group[0]->id);
//dd($gross_expenses);
        $pandl['gross_expenses'] = $gross_expenses;

        $pandl['gross_expense_total'] = 0;
        if ($gross_expenses->cl_total_dc == 'D') {
            $pandl['gross_expense_total'] = $gross_expenses->cl_total;
        } else {
            $pandl['gross_expense_total'] = Helpers::calculate($gross_expenses->cl_total, 0, 'n');
        }

        /* Gross P/L : Incomes */
        $gross_incomes = new AccountList();
        $gross_incomes->only_opening = ($only_opening == '') ? false : true;
        $gross_incomes->start_date = $startdate;
        $gross_incomes->end_date = $enddate;
        $gross_incomes->affects_gross = 1;
        $group = App\Accounting\Group::where([['name','Incomes'],['module_id',session('module')]])->get();
        $gross_incomes->start($group[0]->id);

        $pandl['gross_incomes'] = $gross_incomes;

        $pandl['gross_income_total'] = 0;
        if ($gross_incomes->cl_total_dc == 'C') {
            $pandl['gross_income_total'] = $gross_incomes->cl_total;
        } else {
            $pandl['gross_income_total'] = Helpers::calculate($gross_incomes->cl_total, 0, 'n');
        }

        /* Calculating Gross P/L */
        $pandl['gross_pl'] = Helpers::calculate($pandl['gross_income_total'], $pandl['gross_expense_total'], '-');

        /**********************************************************************/
        /************************* NET CALCULATIONS ***************************/
        /**********************************************************************/

        /* Net P/L : Expenses */
        $net_expenses = new AccountList();
        $net_expenses->only_opening = ($only_opening == '') ? false : true;
        $net_expenses->start_date = $startdate;
        $net_expenses->end_date = $enddate;
        $net_expenses->affects_gross = 0;
        $group = App\Accounting\Group::where([['name','Expenses'],['module_id',session('module')]])->get();
        $net_expenses->start($group[0]->id);

        $pandl['net_expenses'] = $net_expenses;

        $pandl['net_expense_total'] = 0;
        if ($net_expenses->cl_total_dc == 'D') {
            $pandl['net_expense_total'] = $net_expenses->cl_total;
        } else {
            $pandl['net_expense_total'] = Helpers::calculate($net_expenses->cl_total, 0, 'n');
        }

        /* Net P/L : Incomes */
        $net_incomes = new AccountList();
        $net_incomes->only_opening = ($only_opening == '') ? false : true;
        $net_incomes->start_date = $startdate;
        $net_incomes->end_date = $enddate;
        $net_incomes->affects_gross = 0;
        $group = App\Accounting\Group::where([['name','Incomes'],['module_id',session('module')]])->get();
        $net_incomes->start($group[0]->id);

        $pandl['net_incomes'] = $net_incomes;

        $pandl['net_income_total'] = 0;
        if ($net_incomes->cl_total_dc == 'C') {
            $pandl['net_income_total'] = $net_incomes->cl_total;
        } else {
            $pandl['net_income_total'] = Helpers::calculate($net_incomes->cl_total, 0, 'n');
        }

        /* Calculating Net P/L */
        $pandl['net_pl'] = Helpers::calculate($pandl['net_income_total'], $pandl['net_expense_total'], '-');
        $pandl['net_pl'] = Helpers::calculate($pandl['net_pl'], $pandl['gross_pl'], '+');
        
        return $pandl;
    }

    public function trial_balance()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $bsheet = $this->getBS();
            $this->subtitle = 'Trial Balance from ' . \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y') . ' to ' .
                \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');

            $accountlist = new AccountList();
            $accountlist->only_opening = false;
            $accountlist->start_date = null;
            $accountlist->end_date = null;
            $accountlist->affects_gross = -1;

            $accountlist->start(0);

            return view('admin.accounting.trial_balance',array('subtitle' => $this->subtitle))->with('accountlist',$accountlist);
        }
    }

    public function ledger_statement()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            /* Create list of ledgers to pass to view */
            $ledgers = new App\Accounting\LedgerTree();
            $ledgers->current_id = -1;
            $ledgers->restriction_bankcash = 1;
            $ledgers->build(0);
            $ledgers->toList($ledgers, -1);
            $ledgers_disabled = array();
            foreach ($ledgers->ledgerList as $row => $data) {
                if ($row < 0) {
                    $ledgers_disabled[] = $row;
                }
            }
            return view('admin.accounting.ledger_statement',array('ledgers_disabled' => $ledgers_disabled, 'req' => '', 'showStatement' => ''))->with('ledgers',$ledgers->ledgerList);
        }
    }

    public function showledger_statement(Request $request, $id = null)
    {
        $ledgers = new App\Accounting\LedgerTree();
        $ledgers->current_id = -1;
        $ledgers->restriction_bankcash = 1;
        $ledgers->build(0);
        $ledgers->toList($ledgers, -1);

        $startdate = $request->start_date;
        $enddate = $request->end_date;

        $st_date = \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y');
        $en_date = \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');

        if(!$id)
            $ledger = App\Accounting\Ledger::find($request->ledgers);
        else {
            $ledger = App\Accounting\Ledger::find($id);
            $request->ledgers = $id;
        }

        $ledger_name = Helpers::toCodeWithName($ledger->code, $ledger->name);

        if(empty($startdate) and empty($enddate))
        {
            $this->subtitle = 'Ledger statement for ' . $ledger_name . ' from ' . $st_date . ' to ' . $en_date;
        }
        else
        {
            if(!empty($startdate) and !empty($enddate)) {
                $this->subtitle = 'Ledger statement for ' . $ledger_name . ' from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
            }
            elseif(!empty($startdate) and empty($enddate))
            {
                $this->subtitle = 'Ledger statement for ' . $ledger_name . ' from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . $en_date;
            }
            elseif(!empty($enddate) and empty($startdate))
            {
                $this->subtitle = 'Ledger statement for ' . $ledger_name . ' from ' . $st_date . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
            }
        }

        /* Opening and closing titles */
        if (is_null($startdate)) {
            $this->opening_title = 'Opening balance as on ' . $st_date;
        } else {
            $this->opening_title = 'Opening balance as on ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y');
        }
        if (is_null($enddate)) {
            $this->closing_title = 'Closing balance as on ' . $en_date;
        } else {
            $this->closing_title = 'Closing balance as on ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
        }

        /* Calculating opening balance */
        $op = App\Accounting\Ledger::openingBalance($ledger->id, $startdate);

        /* Calculating closing balance */
        $cl = App\Accounting\Ledger::closingBalance($ledger->id, $startdate, $enddate);

        return view('admin.accounting.ledger_statement',array('req' => $request, 'showStatement' => '1', 'subtitle' => $this->subtitle, 'opening_title' => $this->opening_title, 'closing_title' => $this->closing_title, 'op' => $op, 'cl' => $cl, 'ledger' => $ledger))->with('ledgers',$ledgers->ledgerList);
    }

    public function getAllLedgerStatements(Request $request)
    {
        $ledger_id = $request->ledger_id;
        $this->ledger_id = $ledger_id;
        $start_date = $request->start_date;
        $this->start_date = $start_date;
        $end_date = $request->end_date;
        $this->end_date = $end_date;

        $query = DB::table('acc_entries')
            ->leftJoin('acc_tags','acc_tags.id','acc_entries.tag_id')
            ->join('acc_entrytypes','acc_entrytypes.id','acc_entries.entrytype_id')
            ->join('acc_entryitems','acc_entryitems.entry_id','acc_entries.id')
            ->select(DB::Raw('acc_entries.*,acc_entries.id eid,acc_entryitems.amount,acc_entryitems.dc, acc_entrytypes.name etname, acc_entrytypes.prefix, acc_entrytypes.suffix, acc_entrytypes.zero_padding,acc_tags.id tid,acc_tags.title tname, acc_tags.color, acc_tags.background'))
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            /* ->where('acc_entryitems.deleted_at',NULL)
            ->where('acc_entryitems.status',1)
            ->where('acc_entrytypes.deleted_at',NULL)
            ->where('acc_entrytypes.status',1) */
            ->where('acc_entries.module_id',Session::get('module'))

            ->when($ledger_id, function ($query) use ($ledger_id) {
                return $query->where('acc_entryitems.ledger_id', $ledger_id);
            })

            ->when($start_date, function ($query) use ($start_date) {
                return $query->where('acc_entries.date','>=', date('Y-m-d',strtotime($start_date)));
            })

            ->when($end_date, function ($query) use ($end_date) {
                return $query->where('acc_entries.date','<=', date('Y-m-d',strtotime($end_date)));
            })

            ->orderBy('acc_entryitems.id', 'asc')

            ->get();

        $data = Datatables::of($query)

            ->addColumn('date', function($query){
                return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('number', function($query){
                $zeros = '';
                if($query->zero_padding > 0)
                {
                    for($i=0;$i<$query->zero_padding;$i++)
                    {
                        $zeros .= '0';
                    }
                }
                return $query->prefix . $zeros . $query->number . $query->suffix;
            })

            ->addColumn('ledger', function($query){
                return Entry::entryLedgers($query->eid,0);
            })

            ->addColumn('etname', function($query){
                return $query->etname;
            })

            ->addColumn('tag_id', function($query){
                if($query->tid)
                    $tag = '<span class="acc_tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->tid) . '" style="color:#'. $query->color .' !important;">'. $query->tname .'</a></span>';
                else
                    $tag = '';
                return $tag;
            })

            ->addColumn('dr_total', function($query){
                return ($query->dc == 'D') ? Helpers::toCurrency('D',$query->amount) : "";
            })

            ->addColumn('cr_total', function($query){
                return ($query->dc == 'C') ? Helpers::toCurrency('C',$query->amount) : "";
            })

            ->editColumn('balance', function($query){
                if(empty($this->current_op)) {
                    $op = App\Accounting\Ledger::openingBalance($this->ledger_id, $this->start_date);
                    $this->current_op = $op;
                }
                $entry_balance = Helpers::calculate_withdc(
                    $this->current_op['amount'], $this->current_op['dc'],
                    $query->amount, $query->dc
                );
                $this->current_op = $entry_balance;
                $ledger = App\Accounting\Ledger::find($this->ledger_id);
                if($ledger->type == 1 and $this->current_op['dc'] == 'C' and $this->current_op['amount'] != '0')
                    return '<span class="error_entry">' . Helpers::toCurrency($this->current_op['dc'], $this->current_op['amount']) . '</span>';
                else
                    return Helpers::toCurrency($this->current_op['dc'], $this->current_op['amount']);
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_view_entry'))
                    $sv = "display:'inline-block'";
                else
                    $sv = 'display:none';
                if(App\User::getPermission('can_edit_entry'))
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if(App\User::getPermission('can_delete_entry'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-success btn-xs' style='$sv' href='". url('/admin/accounting/view-entry/'.$query->eid) . "'><i class='fa fa-hand-o-left'></i>&nbsp;View</a>&nbsp;
                            <a type='button' class='btn btn-default btn-xs' style='$se' href='". url('/admin/accounting/edit-entry/'.$query->eid) . "'><i class='fa fa-edit'></i>&nbsp;Edit</a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete' style='$de' href='". url('/admin/accounting/delete-entry/'.$query->eid) . "'><i class='fa fa-trash-o'></i>&nbsp;Delete</a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('ledger','tag_id','balance','btn_actions'))

            ->make(true);

        return $data;
    }

    public function ledger_reconcile()
    {
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            /* Create list of ledgers to pass to view */
            $ledgers = App\Accounting\Ledger::getRecLedgers();
            return view('admin.accounting.ledger_reconcile',array('req' => '', 'showStatement' => ''))->with('ledgers',$ledgers);
        }
    }

    public function showledger_reconcile(Request $request)
    {
        $startdate = $request->start_date;
        $enddate = $request->end_date;

        $st_date = \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_start)->format('d-M-Y');
        $en_date = \Carbon\Carbon::parse(Module::find(Session::get('module'))->fy_end)->format('d-M-Y');

        $ledgers = App\Accounting\Ledger::getRecLedgers();

        $ledger = App\Accounting\Ledger::find($request->ledgers);

        $ledger_name = Helpers::toCodeWithName($ledger->code, $ledger->name);

        if(empty($startdate) and empty($enddate))
        {
            $this->subtitle = 'Reconciliation report for ' . $ledger_name . ' from ' . $st_date . ' to ' . $en_date;
        }
        else
        {
            if(!empty($startdate) and !empty($enddate)) {
                $this->subtitle = 'Reconciliation report for ' . $ledger_name . ' from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
            }
            elseif(!empty($startdate) and empty($enddate))
            {
                $this->subtitle = 'Reconciliation report for ' . $ledger_name . ' from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . $en_date;
            }
            elseif(!empty($enddate) and empty($startdate))
            {
                $this->subtitle = 'Reconciliation report for ' . $ledger_name . ' from ' . $st_date . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
            }
        }

        /* Opening and closing titles */
        if (is_null($startdate)) {
            $this->opening_title = 'Opening balance as on ' . $st_date;
        } else {
            $this->opening_title = 'Opening balance as on ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y');
        }
        if (is_null($enddate)) {
            $this->closing_title = 'Closing balance as on ' . $en_date;
        } else {
            $this->closing_title = 'Closing balance as on ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
        }

        /* Reconciliation pending title */
        if (is_null($startdate) && is_null($enddate))
            $this->reconcile_title = 'Reconciliation pending from ' . $st_date . ' to ' . $en_date;
        elseif (!is_null($startdate) and !is_null($enddate))
            $this->reconcile_title = 'Reconciliation pending from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
        elseif (is_null($startdate) and !is_null($enddate))
            $this->reconcile_title = 'Reconciliation pending from ' . $st_date . ' to ' . \Carbon\Carbon::parse($enddate)->format('d-M-Y');
        elseif (!is_null($startdate) and is_null($enddate))
            $this->reconcile_title = 'Reconciliation pending from ' . \Carbon\Carbon::parse($startdate)->format('d-M-Y') . ' to ' . $en_date;

        /* Calculating opening balance */
        $op = App\Accounting\Ledger::openingBalance($ledger->id, $startdate);

        /* Calculating closing balance */
        $cl = App\Accounting\Ledger::closingBalance($ledger->id, null, $enddate);

        /* Calculating reconciliation pending balance */
        $rp = App\Accounting\Ledger::reconciliationPending($ledger->id, $startdate, $enddate);

        return view('admin.accounting.ledger_reconcile',array('req' => $request, 'showStatement' => '1', 'subtitle' => $this->subtitle, 'opening_title' => $this->opening_title, 'closing_title' => $this->closing_title, 'reconcile_title' => $this->reconcile_title, 'op' => $op, 'cl' => $cl, 'rp' => $rp, 'ledger' => $ledger))->with('ledgers',$ledgers);
    }

    public function getAllLedgerReconciles(Request $request)
    {
        $ledger_id = $request->ledger_id;
        $this->ledger_id = $ledger_id;
        $start_date = $request->start_date;
        $this->start_date = $start_date;
        $end_date = $request->end_date;
        $this->end_date = $end_date;

        $query = DB::table('acc_entries')
            ->leftJoin('acc_tags','acc_tags.id','acc_entries.tag_id')
            ->join('acc_entrytypes','acc_entrytypes.id','acc_entries.entrytype_id')
            ->join('acc_entryitems','acc_entryitems.entry_id','acc_entries.id')
            ->select(DB::Raw('acc_entries.*,acc_entries.id eid,acc_entryitems.id eiid,acc_entryitems.amount,acc_entryitems.dc, acc_entryitems.reconciliation_date, acc_entrytypes.name etname, acc_entrytypes.prefix, acc_entrytypes.suffix, acc_entrytypes.zero_padding,acc_tags.id tid, acc_tags.color, acc_tags.background'))
            ->where('acc_entries.deleted_at',NULL)
            ->where('acc_entries.status',1)
            /* ->where('acc_entryitems.deleted_at',NULL)
            ->where('acc_entryitems.status',1)
            ->where('acc_entrytypes.deleted_at',NULL)
            ->where('acc_entrytypes.status',1) */
            ->where('acc_entries.module_id',Session::get('module'))

            ->when($ledger_id, function ($query) use ($ledger_id) {
                return $query->where('acc_entryitems.ledger_id', $ledger_id);
            })

            ->when($start_date, function ($query) use ($start_date) {
                return $query->where('acc_entries.date','>=', date('Y-m-d',strtotime($start_date)));
            })

            ->when($end_date, function ($query) use ($end_date) {
                return $query->where('acc_entries.date','<=', date('Y-m-d',strtotime($end_date)));
            })

            ->orderBy('acc_entries.date', 'desc')

            ->get();

        $data = Datatables::of($query)

            ->addColumn('date', function($query){
                return \Carbon\Carbon::parse($query->date)->format('d/m/Y');
            })

            ->addColumn('number', function($query){
                $zeros = '';
                if($query->zero_padding > 0)
                {
                    for($i=0;$i<$query->zero_padding;$i++)
                    {
                        $zeros .= '0';
                    }
                }
                return $query->prefix . $zeros . $query->number . $query->suffix;
            })

            ->addColumn('ledger', function($query){
                return Entry::entryLedgers($query->eid,0);
            })

            ->addColumn('etname', function($query){
                return $query->etname;
            })

            ->addColumn('tag_id', function($query){
                if($query->tid)
                    $tag = '<span class="acc_tag" style="color:#'. $query->color .' !important; background-color:#'. $query->background .' !important;"><a href="'. url('/admin/accounting/entries/'.$query->tid) . '" style="color:#'. $query->color .' !important;">Tag'. $query->tid .'</a></span>';
                else
                    $tag = '';
                return $tag;
            })

            ->addColumn('dr_total', function($query){
                return ($query->dc == 'D') ? Helpers::toCurrency('D',$query->amount) : "";
            })

            ->addColumn('cr_total', function($query){
                return ($query->dc == 'C') ? Helpers::toCurrency('C',$query->amount) : "";
            })

            ->editColumn('reconsile_date', function($query){
                $recDate = ($query->reconciliation_date) ? $query->reconciliation_date : "";
                return '<input type="hidden" name="entryids[]" id="entryid'. $query->eiid .'" value="'. $query->eiid .'"><input name="reconsile_date[]" class="form-control reconsile_date datepicker" value="'. $recDate .'" type="text" id="reconsile_date_'. $query->eiid .'">';
            })

            ->rawcolumns(array('ledger','tag_id','reconsile_date'))

            ->make(true);

        return $data;
    }

    function ledger_reconcile2(Request $request)
    {
        for($i=0;$i<count($request->entryids);$i++)
        {
            $eiid = $request->entryids[$i];
            if($request->reconsile_date[$i])
            {
                $eItem = App\Accounting\EntryItem::find($eiid);
                $eItem->reconciliation_date = date('Y-m-d',strtotime($request->reconsile_date[$i]));
                $eItem->updated_by = Auth::user()->id;
                $eItem->updated_at = date('Y-m-d H:i:s');
                $eItem->save();
            }
        }
        return back();
    }
}