<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\AccountList;
use DB;
use App\Role;
use Carbon;
use Validator;
use Auth;
use App\Accounting\Group;
use App\Accounting\Ledger;

class AccountsController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('accounts');
        if(!$rights->can_view){
    	    abort(403);
        }
        $group_rights = Role::getrights('group');
         $ledger_rights = Role::getrights('ledger');
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else {
            $accountlist = new AccountList();
            /*$accountlist->Group = &Group::all();
            $accountlist->Ledger = &Ledger::all();*/
            $accountlist->only_opening = false;
            $accountlist->start_date = null;
            $accountlist->end_date = null;
            $accountlist->affects_gross = -1;
            $accountlist->start(0);
            //dd($accountlist);

            $opdiff = Ledger::getOpeningDiff();

            //dd($opdiff);
            return view('admin.accounting.accounts', array('opdiff' => $opdiff, 'group_rights' => $group_rights, 'ledger_rights' => $ledger_rights))->with('accountlist', $accountlist);
        }
    }

    public function getAllAccounts(Request $request)
    {
        $columns = array(
            0 => 'account',
            1 => 'type',
            2 => 'opbalance',
            3 => 'clbalance',
            4 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_modules')
            ->select(DB::Raw('*'))
            ->where('deleted_at','=',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('label', function($query){
                return $query->label;
            })

            ->addColumn('company', function($query){
                return $query->company;
            })

            ->addColumn('email', function($query){
                return $query->email;
            })

            ->addColumn('fy_start', function($query){
                return \Carbon\Carbon::parse($query->fy_start)->format('d/m/Y');
            })

            ->addColumn('fy_end', function($query){
                return \Carbon\Carbon::parse($query->fy_end)->format('d/m/Y');
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                $rights = Role::getrights('module');
                if($rights->show_edit)
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
               $rights = Role::getrights('module');
                if($rights->show_delete)
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-default btn-xs edit' data-toggle='tooltip' title='Edit' style='$se' href='". url('/admin/accounting/edit-module/'.$query->id) . "'><i class='fa fa-pencil'></i></a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete delete' data-toggle='tooltip' title='Delete' style='$de' href='". url('/admin/accounting/delete-module/'.$query->id) . "'><i class='fa fa-times'></i></a>
                            </div>";
                return $actions;
            })
            //->orderColumn('title')
            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }
}