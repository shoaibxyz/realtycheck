<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Carbon;
use Validator;
use Auth;
use App\Traits\CustomFunctions;

class ModulesController extends Controller
{
    use CustomFunctions;
    public function __construct()
    {
        $ref = '';
    }

    public function index()
    {
        //echo 'Role='. Auth::user()->agency->id;
        return view('admin.accounting.modules');
    }

    public function deleted_modules()
    {
        return view('admin.accounting.deleted-modules');
    }

    public function getAllModules(Request $request)
    {        
        $columns = array(
            0 => 'label',
            1 => 'company',
            2 => 'email',
            3 => 'fy_start',
            4 => 'fy_end',
            5 => 'status',
            6 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        //dd($this->getAgency()->person_id);
        $query = DB::table('acc_modules')
            ->select(DB::Raw('acc_modules.*'))
            ->where('acc_modules.deleted_at','=',NULL)            
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('label', function($query){
                return $query->label;
            })

            ->addColumn('company', function($query){
                return $query->company;
            })

            ->addColumn('email', function($query){
                return $query->email;
            })

            ->addColumn('fy_start', function($query){
                return \Carbon\Carbon::parse($query->fy_start)->format('d-M-Y');
            })

            ->addColumn('fy_end', function($query){
                return \Carbon\Carbon::parse($query->fy_end)->format('d-M-Y');
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_edit_module'))
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if(App\User::getPermission('can_delete_module'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-default btn-xs edit' data-toggle='tooltip' title='Edit' style='$se' href='". url('/admin/accounting/edit-module/'.$query->id) . "'><i class='fa fa-pencil'></i></a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete delete' data-toggle='tooltip' title='Delete' style='$de' href='". url('/admin/accounting/delete-module/'.$query->id) . "'><i class='fa fa-times'></i></a>
                            </div>";
                return $actions;
            })
            //->orderColumn('title')
            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function getAllDelModules(Request $request)
    {
        $columns = array(
            0 => 'label',
            1 => 'company',
            2 => 'email',
            3 => 'fy_start',
            4 => 'fy_end',
            5 => 'status',
            6 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_modules')
            ->select(DB::Raw('*'))
            ->where('deleted_at','!=',NULL)            
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('label', function($query){
                return $query->label;
            })

            ->addColumn('company', function($query){
                return $query->company;
            })

            ->addColumn('email', function($query){
                return $query->email;
            })

            ->addColumn('fy_start', function($query){
                return \Carbon\Carbon::parse($query->fy_start)->format('d/m/Y');
            })

            ->addColumn('fy_end', function($query){
                return \Carbon\Carbon::parse($query->fy_end)->format('d/m/Y');
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_restore_module'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete restore' data-toggle='tooltip' title='Restore' style='$de' href='". url('/admin/accounting/restore-module/'.$query->id) . "'><i class='fa fa-undo'></i></a>
                            </div>";
                return $actions;
            })
            //->orderColumn('title')
            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function create_module()
    {
        return view('admin.accounting.create-module');
    }

    public function select_module()
    {
        $mods = App\Accounting\Module::where('deleted_at',NULL)->where('status',1)->get();
        return view('admin.accounting.select-module')->with('modules',$mods);
    }

    public function set_module(Request $request)
    {
        Session::put('module', $request->module);
        //print_r(Session::get('ref_url'));
        if(Session::get('ref_url'))
            return Redirect::to(Session::get('ref_url'));
        return Redirect::to('/admin/accounting/acc-dashboard');
    }

    public function edit_module($id)
    {
        $module = App\Accounting\Module::find($id);
        return view('admin.accounting.edit-module')->with('module',$module);
    }

    public function select()
    {
        return view('admin.accounting.select-module');
    }

    public function save(Request $request)
    {
        $uid = Auth::user()->id;
        $email = $request->input('email');
        $messages = [
            'fy_start.date' => 'Financial year start is not a valid date.',
            'fy_end.date' => 'Financial year end is not a valid date.',
        ];
        Validator::make($request->all(), [
            'label' => Rule::unique('acc_modules')->where(function ($query) use($uid) {
                return $query->where('created_by', $uid);
            }),
            'email' => Rule::unique('acc_modules')->where(function ($query) use($uid) {
                return $query->where('created_by', $uid);
            }),
            'fy_start' => 'date',
            'fy_end' => 'date',
        ],$messages)->validate();

        $module = new App\Accounting\Module();

        $module->label = $request->input('label');
        $module->company = $request->input('company');
        $module->email = $request->input('email');
        $module->address = $request->input('address');
        $module->currency_symbol = 'PKR';
        $module->fy_start = date('Y-m-d', strtotime($request->input('fy_start')));
        $module->fy_end = date('Y-m-d', strtotime($request->input('fy_end')));
        $module->status = $request->input('status');
        $module->created_by = Auth::user()->id;
        $module->created_at = date('Y-m-d H:i:s');
        $module->updated_by = Auth::user()->id;
        $module->updated_at = date('Y-m-d H:i:s');
        $module->save();


        $fields['Assets'][0]['name'] = 'Current Assets';
        $fields['Assets'][0]['parent_id'] = NULL;
        $fields['Assets'][0]['affects_gross'] = 0;
        $fields['Assets'][1]['name'] = 'Fixed Assets';
        $fields['Assets'][1]['parent_id'] = NULL;
        $fields['Assets'][1]['affects_gross'] = 0;
        $fields['Assets'][2]['name'] = 'Investments';
        $fields['Assets'][2]['parent_id'] = NULL;
        $fields['Assets'][2]['affects_gross'] = 0;
        $fields['Liabilities and Owners Equity'][0]['name'] = 'Capital Account';
        $fields['Liabilities and Owners Equity'][0]['parent_id'] = NULL;
        $fields['Liabilities and Owners Equity'][0]['affects_gross'] = 0;
        $fields['Liabilities and Owners Equity'][1]['name'] = 'Current Liabilities';
        $fields['Liabilities and Owners Equity'][1]['parent_id'] = NULL;
        $fields['Liabilities and Owners Equity'][1]['affects_gross'] = 0;
        $fields['Liabilities and Owners Equity'][2]['name'] = 'Loans (Liabilities)';
        $fields['Liabilities and Owners Equity'][2]['parent_id'] = NULL;
        $fields['Liabilities and Owners Equity'][2]['affects_gross'] = 0;
        $fields['Incomes'][0]['name'] = 'Direct Incomes';
        $fields['Incomes'][0]['parent_id'] = NULL;
        $fields['Incomes'][0]['affects_gross'] = 0;
        $fields['Incomes'][1]['name'] = 'Indirect Incomes';
        $fields['Incomes'][1]['parent_id'] = NULL;
        $fields['Incomes'][1]['affects_gross'] = 0;
        $fields['Incomes'][2]['name'] = 'Sales';
        $fields['Incomes'][2]['parent_id'] = NULL;
        $fields['Incomes'][2]['affects_gross'] = 0;
        $fields['Expenses'][0]['name'] = 'Direct Expenses';
        $fields['Expenses'][0]['parent_id'] = NULL;
        $fields['Expenses'][0]['affects_gross'] = 0;
        $fields['Expenses'][1]['name'] = 'Indirect Expenses';
        $fields['Expenses'][1]['parent_id'] = NULL;
        $fields['Expenses'][1]['affects_gross'] = 0;
        $fields['Expenses'][2]['name'] = 'Purchases';
        $fields['Expenses'][2]['parent_id'] = NULL;
        $fields['Expenses'][2]['affects_gross'] = 0;

        foreach($fields as $key=>$value)
        {
            $gid = $this->insertGroup($key,NULL,0,$module->id);
            foreach($value as $k => $v)
            {
                $gid1 = $this->insertGroup($v['name'],$gid,0,$module->id);
            }
        }

        $this->insertET('Receipt','Received in Bank account or Cash account',2,$module->id);
        $this->insertET('Payment','Payment made from Bank account or Cash account',3,$module->id);
        $this->insertET('Contra','Transfer between Bank account and Cash account',4,$module->id);
        $this->insertET('Journal','Transfer between Non Bank account and Cash account',5,$module->id);

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been added successfully.');
        return Redirect::to('/admin/accounting/all-modules');
    }

    public function insertGroup($name,$pid,$afg,$mid)
    {
        $group = new App\Accounting\Group();
        $group->name = $name;
        $group->parent_id = $pid;
        $group->code = NULL;
        if($name=='Direct Incomes' || $name=='Direct Expenses' || $name=='Sales' || $name=='Purchases')
            $group->affects_gross = 1;
        else
            $group->affects_gross = 0;
        $group->module_id = $mid;
        $group->status = 1;
        $group->created_by = Auth::user()->id;
        $group->created_at = date('Y-m-d H:i:s');
        $group->updated_by = Auth::user()->id;
        $group->updated_at = date('Y-m-d H:i:s');
        $group->save();
        return $group->id;
    }

    public function insertET($label,$desc,$restriction_bankcash,$mid)
    {
        $etype = new App\Accounting\EntryType();
        $etype->label = $label;
        $etype->name = strtolower($label);
        $etype->description = $desc;
        $etype->base_type = 1;
        $etype->numbering = 1;
        $etype->prefix = '';
        $etype->suffix = '';
        $etype->zero_padding = 0;
        $etype->restriction_bankcash = $restriction_bankcash;
        $etype->module_id = $mid;
        $etype->status = 1;
        $etype->created_by = Auth::user()->id;
        $etype->created_at = date('Y-m-d H:i:s');
        $etype->updated_by = Auth::user()->id;
        $etype->updated_at = date('Y-m-d H:i:s');
        $etype->save();
    }

    public function update(Request $request, $id)
    {
        $module = App\Accounting\Module::find($request->id);

        $module->label = $request->input('label');
        $module->company = $request->input('company');
        $module->email = $request->input('email');
        $module->address = $request->input('address');
        $module->currency_symbol = 'PKR';
        $module->fy_start = date('Y-m-d', strtotime($request->input('fy_start')));
        $module->fy_end = date('Y-m-d', strtotime($request->input('fy_end')));
        $module->status = $request->input('status');
        $module->updated_by = Auth::user()->id;
        $module->updated_at = date('Y-m-d H:i:s');
        $module->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been updated successfully.');
        return Redirect::to('/admin/accounting/all-modules');
    }

    public function delete($id)
    {
        $module = App\Accounting\Module::find($id);

        $module->updated_by = Auth::user()->id;
        $module->updated_at = date('Y-m-d H:i:s');
        $module->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Module has been deleted successfully.');
        return Redirect::to('/admin/accounting/all-modules');
    }

    public function restore($id)
    {
        $module = App\Accounting\Module::withTrashed()->find($id);
        if($module) {
            $module->updated_by = Auth::user()->id;
            $module->updated_at = date('Y-m-d H:i:s');
            $module->restore();
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Module has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-modules');
    }
}
