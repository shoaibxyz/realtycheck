<?php

namespace App\Http\Controllers\Accounting;

use App;
use App\Accounting\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Accounting\EntryType;
use App\Role;
use DB;
use Carbon;
use Validator;
use Auth;

class EtypesController extends Controller
{
    public function index()
    {
        
        $rights = Role::getrights('entry-types');
        if(!$rights->can_view){
    	    abort(403);
        }
        if(Session::get('module') == null)
            return Redirect::to('admin/accounting/select-module');
        else
            return view('admin.accounting.entrytypes', compact('rights'));
    }

    public function deleted_entrytypes()
    {
        return view('admin.accounting.deleted-entrytypes');
    }

    public function getAllEtypes(Request $request)
    {
        
        $columns = array(
            0 => 'id',
            1 => 'label',
            2 => 'name',
            3 => 'prefix',
            4 => 'suffix',
            5 => 'zero_padding',
            6 => 'status',
            7 => 'btn_actions',
        );

        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_entrytypes')
            ->select(DB::Raw('*'))
            ->where('deleted_at',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sort['col'], $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('label', function($query){
                return $query->label;
            })

            ->addColumn('name', function($query){
                return $query->name;
            })

            ->addColumn('prefix', function($query){
                return $query->prefix;
            })

            ->addColumn('suffix', function($query){
                return $query->suffix;
            })

            ->addColumn('zero_padding', function($query){
                return $query->zero_padding;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                $rights = Role::getrights('entry-types');
                if($rights->can_edit)
                    $se = "display:'inline-block'";
                else
                    $se = 'display:none';
                if($rights->can_delete)
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a type='button' class='btn btn-info btn-xs edit' data-toggle='tooltip' title='Edit' style='$se' href='". url('/admin/accounting/edit-entrytype/'.$query->id) . "'><i class='fa fa-pencil'></i></a>&nbsp;
                            <a class='btn btn-danger btn-xs btn-delete delete' data-toggle='tooltip' title='Delete' style='$de' href='". url('/admin/accounting/delete-entrytype/'.$query->id) . "'><i class='fa fa-times'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function getAllDelEtypes(Request $request)
    {
        $columns = array(
            0 => 'label',
            1 => 'name',
            2 => 'status',
            3 => 'btn_actions',
        );
        $sortby = $columns[$request->input('order.0.column')];
        $sort['col'] = $sortby;
        $sort['dir'] = $request->input('order.0.dir');

        $query = DB::table('acc_entrytypes')
            ->select(DB::Raw('*'))
            ->where('deleted_at','!=',NULL)
            ->where('module_id',Session::get('module'))
            ->orderBy($sortby, $sort['dir'])
            ->get();

        $data = Datatables::of($query)

            ->addColumn('label', function($query){
                return $query->label;
            })

            ->addColumn('name', function($query){
                return $query->name;
            })

            ->editColumn('status', function($query){
                $status = Helpers::getStatus($query->status);
                $status = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                return $status;
            })

            ->editColumn('btn_actions', function($query){
                if(App\User::getPermission('can_restore_entrytype'))
                    $de = "display:'inline-block'";
                else
                    $de = 'display:none';

                $actions = "<div class='action-btns'>
                            <a class='btn btn-danger btn-xs btn-delete restore' data-toggle='tooltip' title='Restore' style='$de' href='". url('/admin/accounting/restore-entrytype/'.$query->id) . "'><i class='fa fa-undo'></i></a>
                            </div>";
                return $actions;
            })

            ->rawcolumns(array('status','btn_actions'))

            ->make(true);

        return $data;
    }

    public function add()
    {
        $rights = Role::getrights('entry-types');
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.accounting.add-entrytype');
    }

    public function edit(Request $request, $id)
    {
        $rights = Role::getrights('entry-types');
        if(!$rights->can_edit){
    	    abort(403);
        }
        $entrytype = EntryType::find($id);

        return view('admin.accounting.edit-entrytype',array('entrytype' => $entrytype));
    }

    public function save(Request $request)
    {
        Validator::make($request->all(), [
            'label' => 'unique:acc_entrytypes',
            'name' => 'unique:acc_entrytypes',
        ])->validate();

        $entrytype = new EntryType();
        $entrytype->label = $request->label;
        $entrytype->name = $request->name;
        $entrytype->prefix = $request->prefix;
        $entrytype->suffix = $request->suffix;
        $entrytype->description = $request->description;
        $entrytype->numbering = $request->numbering;
        $entrytype->zero_padding = (!empty($request->zero_padding)) ? $request->zero_padding : 0;
        $entrytype->restriction_bankcash = $request->restriction;
        $entrytype->base_type = 1;
        $entrytype->module_id = Session::get('module');
        $entrytype->status = $request->status;
        $entrytype->created_by = Auth::user()->id;
        $entrytype->created_at = date('Y-m-d H:i:s');
        $entrytype->updated_by = Auth::user()->id;
        $entrytype->updated_at = date('Y-m-d H:i:s');
        $entrytype->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Entry type has been added successfully.');
        return Redirect::to('/admin/accounting/entrytypes');
    }

    public function update(Request $request, $id)
    {
        $entrytype = EntryType::find($id);
        $entrytype->label = $request->label;
        $entrytype->name = $request->name;
        $entrytype->prefix = $request->prefix;
        $entrytype->suffix = $request->suffix;
        $entrytype->description = $request->description;
        $entrytype->numbering = $request->numbering;
        $entrytype->zero_padding = (!empty($request->zero_padding)) ? $request->zero_padding : 0;
        $entrytype->restriction_bankcash = $request->restriction;
        $entrytype->base_type = 1;
        $entrytype->status = $request->status;
        $entrytype->updated_by = Auth::user()->id;
        $entrytype->updated_at = date('Y-m-d H:i:s');
        $entrytype->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Entry type has been updated successfully.');
        return Redirect::to('/admin/accounting/entrytypes');
    }

    public function delete($id)
    {
        $childs = App\Accounting\Entry::where([['entrytype_id',$id],['module_id',Session::get('module')],['status',1],['deleted_at',NULL]])->count();
        if ($childs > 0) {
            return redirect()->back()->withInput()->withErrors(['message' => 'Entry type cannot be deleted since one or more entries are still using it.']);
        }

        $entrytype = EntryType::find($id);

        $entrytype->updated_by = Auth::user()->id;
        $entrytype->updated_at = date('Y-m-d H:i:s');
        $entrytype->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Entry type has been deleted successfully.');
        return Redirect::to('/admin/accounting/entrytypes');
    }

    public function restore($id)
    {
        $entrytype = EntryType::withTrashed()->find($id);
        if($entrytype) {
            $entrytype->updated_by = Auth::user()->id;
            $entrytype->updated_at = date('Y-m-d H:i:s');
            $entrytype->restore();
        }
        Session::flash('status', 'success');
        Session::flash('message', 'Entry type has been restored successfully.');
        return Redirect::to('/admin/accounting/deleted-entrytypes');
    }
}