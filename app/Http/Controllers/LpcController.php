<?php

namespace App\Http\Controllers;

use App\Http\Requests\LpcRequest;
use App\LPC;
use App\LpcDefinition;
use App\LpcInstallment;
use App\Payments;
use App\User;
use App\memberInstallments;
use App\members;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Illuminate\Support\Facades\Session;

class LpcController extends Controller
{

    public function index()
    {

       $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        return view('admin.lpc.index', compact('rights'));
    }

    public function lpcDefinition()
    {
        $rights = Role::getrights('define-lpc');
        
        if(!$rights->can_view){
    	    abort(403);
        }
         $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_view){
abort(403);
}

        $lpcDef = LpcDefinition::first();
        return view('admin.lpc.define-charges', compact('lpcDef'));
    }

    public function create()
    {

    }

    public function show()
    {

    }

    public function lpcDefinitionStore(Request $request)
    {
        $period_from = date('Y-m-d', strtotime(strtr($request->input('period_from'), '/', '-')));
        $period_to   = date('Y-m-d', strtotime(strtr($request->input('period_to'), '/', '-')));
        $rate_date   = date('Y-m-d', strtotime(strtr($request->input('rate_date'), '/', '-')));

        $check = LpcDefinition::first();

        if (is_null($check)) {
            $input                = $request->input();
            $input['period_from'] = $period_from;
            $input['rate_date']   = $rate_date;
            $input['period_to']   = $period_to;
            $input['created_by']  = Auth::user()->id;

            $lpc_def = LpcDefinition::create($input);
        } else {

            $check2                = $request->input();
            $check2['updated_by']  = Auth::user()->id;
            $check2['rate_date']   = $rate_date;
            $check2['period_from'] = $period_from;
            $check2['period_to']   = $period_to;
            $check->update($check2);
        }

        $message = "LPC Definition saved";
        return redirect('admin/lpc-definition')->with(['message' => $message, 'status' => 1]);
    }

    public function lpcCalculate(Request $request)
    {
        $rights = Role::getrights('lpc');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());
            
        if(!$rights->can_view){
            abort(403);
        }
        $LpcDefinition = LpcDefinition::first();

        //Get the bookings 
        // $bookingsData = Payments::whereBetween('payment_date', [$LpcDefinition->period_from, $LpcDefinition->period_to])->where('bookings.status', 1)->where('bookings.is_cancelled', 0)->pluck('registration_no');
        // $chargeDate   = date('Y-m-d', strtotime($request->input('charge_date')));
        $installments = memberInstallments::select(DB::Raw('id,registration_no,installment_no,installment_date,installment_amount'))->where('is_paid', 0)->where('is_active_user', 1)->where('installment_date','<', date('Y-m-d'))->where('installment_amount','>',0)->get();
        //$installments = memberInstallments::where('registration_no', 'CC-GS2-1820')->where('installment_date','<', date('Y-m-d'))->orderBy('installment_no','asc')->get();
        //dd($installments);

        $lpcInstArr  = [];
        $lpcOtherArr = [];

        $chargesSum      = 0;
        $otherChargesSum = 0;
        $itr             = 0;
        $reg_Arr         = [];

        $i = 1;
        foreach ($installments as $key => $installment) {

            /* $member = members::join('bookings', 'bookings.registration_no', '=', 'members.registration_no')
            ->join('persons', 'persons.id', '=', 'members.person_id')
            ->where('persons.status', 1)
            ->where('members.registration_no', $installment->registration_no)
            ->where('members.is_full_waived', 0)->where('members.is_current_owner', 1)->whereNull('bookings.deleted_at')->where('bookings.status',1)->first();
            if ($member != null) { */

                /* $person = User::find($member->person_id); */
                
                $lpc = LPC::where([['registration_no',$installment->registration_no],['installment_id',$installment->id]])->first();

                /*$d1 = '';
                $d2 = '';
                $days = 0;
                $days2 = 0;*/
                if(!$lpc)
                {
                    $date1 = date_create(date('Y-m-d', strtotime($installment->installment_date)));
                    /*$d1  =date('Y-m-d', strtotime($installment->installment_date));
                    if(date('m', strtotime($installment->installment_date)) == 4)
                        $days = 20;
                    if(date('m', strtotime($installment->installment_date)) == 5)
                        $days2 = 21;*/
                }
                else
                {
                    $date1 = date_create(date('Y-m-d', strtotime($lpc->calculation_date)));
                    //$d1 = date('Y-m-d', strtotime($lpc->calculation_date));
                }

                //$date2   = date_create($LpcDefinition->period_to);
                $date2   = date_create(date('Y-m-d'));
                $d2   = date('Y-m-d');
                $diff    = date_diff($date1, $date2);
                $os_days = $diff->format("%a");

                if(!$lpc)
                    $waive_days = $os_days - $LpcDefinition->waive_days;
                else
                    $waive_days = $os_days;

                if ($waive_days > 0) {

                    $itr++;

                    $rate = 0;
                    /*$month = 0;
                    $year = date('Y',strtotime($installment->installment_date));
                    if($year == 2017)
                         $rate = 6.39;
                    elseif($year == 2018)
                    {
                        $month = date('m',strtotime($installment->installment_date));
                        if($month >= 1 and $month < 7)
                            $rate = 7.45;
                        else if($month >= 7 and $month <= 12)
                            $rate = 8.23;
                    }
                    else*/
                    $k_rate = $LpcDefinition->rate;
                    $calc_rate = $installment->installment_amount * ($LpcDefinition->rate/100);
                    $rate = round($calc_rate / 30,2);
                    
                    /*$StartDate = @strtotime($d1);
                    $StopDate = @strtotime($d2);
                    $times = $this->echoDate( $StartDate, $StopDate );
                    //dd($d2);
                    foreach($times as $time)
                    {
                        $month = date('m',$time);
                        $year = date('Y',$time);
                        
                        if($year == 2020 and $month == 4)
                            //$wav += 30;
                            $waive_days = ($days > 0) ? $waive_days - $days : $waive_days - 30;
                        if($year == 2020 and $month == 5)
                            //$wav += 31;
                            $waive_days = ($days2 > 0) ? $waive_days - $days2 : $waive_days - 31;
                    }
                    $chargesSum = ($waive_days > 0) ? $rate * $waive_days : 0;*/
                    $chargesSum = $rate;
                    $lpcInstArr[$installment->registration_no][$installment->installment_no]['os_days'] = $waive_days;
                    
                    /*if ($installment->is_other_payment == 0) {*/
                        $lpcInstArr[$installment->registration_no][$installment->installment_no]['charges'][] = $chargesSum;
                    /*} else {
                        $lpcInstArr[$installment->registration_no][$installment->installment_no]['otherChargesSum'][] = $chargesSum;
                    }*/

                    $lpcInstArr[$installment->registration_no][$installment->installment_no]['rate']         = /* $person->name */  $rate;
                    $lpcInstArr[$installment->registration_no][$installment->installment_no]['booking_date']         = $installment->installment_date;
                    $lpcInstArr[$installment->registration_no][$installment->installment_no]['id'][]           = $installment->id;
                    $lpcInstArr[$installment->registration_no][$installment->installment_no]['installment_no'] = $installment->installment_no;

                    if (!in_array($installment->registration_no, $reg_Arr)) {
                        $reg_Arr[]       = $installment->registration_no;
                        $chargesSum      = 0;
                        $otherChargesSum = 0;
                    }
                }
            //}
        }

        foreach ($lpcInstArr as $key => $value) {
            foreach ($value as $key2 => $val) {
                $chargesSum      = 0;
                $otherChargesSum = 0;

                if (isset($val['charges'])) {
                    foreach ($val['charges'] as $k => $v) {
                        $chargesSum += $v;
                    }
                }

                /*if (isset($val['otherChargesSum'])) {
                    foreach ($val['otherChargesSum'] as $j => $q) {
                        $otherChargesSum += $q;
                    }
                }*/

                $lpcInstArr[$key][$key2]['chargesSum']      = $chargesSum;
                /*$lpcInstArr[$key][$key2]['otherChargesSum'] = $otherChargesSum;*/
            }
        }
        // dd($lpcInstArr);

        // echo $chargesSum."<br>";
        if ($itr > 0) {
            return view('admin.lpc.index', compact('lpcInstArr', 'rights'));
        } else {
            $message = "For this date no new record found";
            return redirect('admin/lpc/lpc-calculate')->with(['message' => $message, 'status' => 0]);
        }
    }
    
    public function echoDate( $start, $end )
    {
        $current = $start;
        $ret = array();

        while( $current<$end ){
            
            $next = @date('Y-M-01', $current) . "+1 month";
            $ret[] = $current;
            $current = @strtotime($next);
        }

        return $ret;
    }

    public function store(Request $request)
    {
        //dd('hellow');
        $LpcDefinition = LpcDefinition::first();

        $chargeDate   = date('Y-m-d', strtotime($request->input('charge_date')));
        $installments = memberInstallments::select(DB::Raw('id,registration_no,installment_no,installment_date,installment_amount'))->where('is_paid', 0)->where('is_active_user', 1)->where('installment_date','<', date('Y-m-d'))->where('installment_amount','>',0)->get();
        //$installments = memberInstallments::where('registration_no', 'CC-GS2-1820')->where('installment_date','<', date('Y-m-d'))->orderBy('installment_no','asc')->get();
 
        //dd($installments);
        
        $lpcInstArr  = [];
        $lpcOtherArr = [];
        $installmentNoArr = [];

        $chargesSum      = 0;
        $otherChargesSum = 0;
        $i               = 1;

        $user_id = Auth::user()->id;

        $reg_Arr = [];
        if ($installments != null) {

            foreach ($installments as $key => $installment) {
               /* $member = members::join('bookings', 'bookings.registration_no', '=', 'members.registration_no')
                ->join('persons', 'persons.id', '=', 'members.person_id')
                ->where('persons.status', 1)
                ->where('members.registration_no', $installment->registration_no)
                ->where('members.is_full_waived', 0)->where('members.is_current_owner', 1)->whereNull('bookings.deleted_at')->where('bookings.status',1)->first();
    
                if($member)
                { */
                    /* $person = User::find($member->person_id); */
                    /*$d1 = '';
                    $d2 = '';
                    $days = 0;
                    $days2 = 0;*/
                    
                    $lpc = LPC::where([['registration_no',$installment->registration_no],['installment_id',$installment->id]])->first();
    
                    if(!$lpc)
                    {
                        $date1 = date_create(date('Y-m-d', strtotime($installment->installment_date)));
                        /*$d1  =date('Y-m-d', strtotime($installment->installment_date));
                        if(date('m', strtotime($installment->installment_date)) == 4)
                            $days = 20;
                        if(date('m', strtotime($installment->installment_date)) == 5)
                            $days2 = 21;*/
                    }
                    else
                    {
                        $date1 = date_create(date('Y-m-d', strtotime($lpc->calculation_date)));
                        //$d1 = date('Y-m-d', strtotime($lpc->calculation_date));
                    }
                    //$date2   = date_create($LpcDefinition->period_to);
                    $date2   = date_create(date('Y-m-d'));
                    //$d2   = date('Y-m-d');
                    $diff    = date_diff($date1, $date2);
                    $os_days = $diff->format("%a");
    
                    if(!$lpc)
                        $waive_days = $os_days - $LpcDefinition->waive_days;
                    else
                        $waive_days = $os_days;
    
                    if($waive_days > 0)
                    {
                        $rate = 0;
                        /*$month = 0;
                        $year = date('Y',strtotime($installment->installment_date));
                        if($year == 2017)
                            $rate = 6.39;
                        elseif($year == 2018)
                        {
                            $month = date('m',strtotime($installment->installment_date));
                            if($month >= 1 and $month < 7)
                                $rate = 7.45;
                            else if($month >= 7 and $month <= 12)
                                $rate = 8.23;
                        }
                        else*/
                        $k_rate = $LpcDefinition->rate;
                        $calc_rate = $installment->installment_amount * ($LpcDefinition->rate/100);
                        $rate = round($calc_rate / 30,2);

                        $chargesSum = $rate * $waive_days;
                        
                        $wav = 0;
                        
                        /*$StartDate = @strtotime($d1);
                        $StopDate = @strtotime($d2);
                        $times = $this->echoDate( $StartDate, $StopDate );
                        //dd($d2);
                        foreach($times as $time)
                        {
                            $month = date('m',$time);
                            $year = date('Y',$time);
                            
                            if($year == 2020 and $month == 4)
                                $wav = $wav + (($days > 0) ? $days : 30);
                                //$waive_days = $waive_days - 30;
                            if($year == 2020 and $month == 5)
                                $wav = $wav + (($days2 > 0) ? $days2 : 31);
                                //$waive_days = $waive_days - 31;
                            //print($installment->registration_no. '-' .$installment->installment_no. '-' .$d1 . '-' . $d2 . '<br>');
                        }
                            
                        $wav = $rate * $wav;*/
                        
                        if($lpc)
                        {
                            /*if($installment->is_other_payment == 0)
                            {*/
                            $lpc->lpc_amount       += round($chargesSum,2);
                            /*}
                            else{
                                $lpc->lpc_other_amount += $chargesSum;
                            }*/
                            /*$lpc->inst_waive       += $wav;
                            $lpc->waive_remarks       = ($wav > 0) ? "Corona Relief" : "";*/
                            $lpc->kibor_rate       = $LpcDefinition->rate;
                            $lpc->kibor_rate_date  = $date2;
                            $lpc->calculation_date = $date2;
                            $lpc->days += $waive_days;
                            $lpc->created_by       = $user_id;
                            $lpc->save();
                        }
                        else
                        {
                            $lpc                   = new LPC();
        
                            /*if($installment->is_other_payment == 0)
                            {*/
                                $lpc->lpc_amount       = round($chargesSum,2);
                            /*}else{
                                $lpc->lpc_other_amount = $chargesSum;
                            }*/
                            /*$lpc->inst_waive       = $wav;
                            $lpc->waive_remarks       = ($wav > 0) ? "Corona Relief" : "";*/
        
                            $lpc->registration_no  = $installment->registration_no;
                            $lpc->installment_id  = $installment->id;
                            $lpc->kibor_rate       = $rate;
                            $lpc->kibor_rate_date  = $date2;
                            $lpc->calculation_date = $date2;
                            $lpc->days = $waive_days;
                            $lpc->created_by       = $user_id;
                            $lpc->save();
                        }
                    }
                //}
            }
            
            return redirect("admin/lpc/lpc-calculate")->with(['message', 'Saved! Now continue to finalize', 'status' => 1]);
        } 
        else {
            return redirect("admin/lpc/lpc-calculate")->with(['message', 'No data found']);
        }
    }

    public function PostWaive(Request $request)
    {
              $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        $waive_date    = date('Y-m-d', strtotime(strtr($request->input('waive_date'), '/', '-')));
        $waive_charges = $request->input('waive_charges');
        $waive_remarks = $request->input('waive_remarks');

        if ($waive_charges != 0 && $waive_charges != "") {

            foreach ($request->input('lpc_id') as $key => $value) 
            {
                if($request->waive_amount[$key] > 0)
                {
                    $lpc                = LPC::find($value);
                    $lpc->waive_date    = $waive_date;
                    $lpc->inst_waive  = $request->waive_amount[$key];
                    $lpc->waive_amount  = $waive_charges;
                    $lpc->waive_remarks = $waive_remarks;
                    if($request->lpc_amount[$key] == $request->waive_amount[$key])
                        $lpc->is_paid = 1;
                    $lpc->save();
                }
            }

            return redirect("admin/lpc/waive")->with(['message' => "Waive has been added to the given charges.", "status" => 1]);
        }

        return redirect("admin/lpc/waive")->with(['message' => "You didn't choose waive amount", "status" => 0]);

    }

    public function destroy($id)
    {
    }

    public function edit($id)
    {

    }

    public function update($id, Request $request)
    {

    }

    public function finalize()
    {

              $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        $lpc = LPC::where('is_finalized', 0)->get();
        foreach ($lpc as $key => $lpcs) {

            $inst[$lpcs->registration_no]['inst'] = memberInstallments::where('registration_no', $lpcs->registration_no)->where('lpc_finalize', 1)->get();

            if ($inst != null) {
                $inst[$lpcs->registration_no]['lpc_amt']       = $lpcs->lpc_amount;
                $inst[$lpcs->registration_no]['lpc_other_amt'] = $lpcs->lpc_other_amount;

                $member = members::where('registration_no', $lpcs->registration_no)->where('is_current_owner', 1)->first();

                $person = User::find($member->person_id);
                if ($person) {
                    $inst[$lpcs->registration_no]['member'] = User::find($member->person_id);
                }

                $inst[$lpcs->registration_no]['id'] = $lpcs->id;
            }
        }

        return view("admin.lpc.finalize", compact('inst'));
    }

    public function postFinalize(Request $request)
    {
        foreach ($request->input('lpc_id') as $value) {
            $lpc = LPC::find($value);

            $inst = memberInstallments::where('registration_no', $lpc->registration_no)->where('lpc_finalize', 0)->where('os_days', '!=', 0)->get();

            foreach ($inst as $key => $insts) {
                $insts->lpc_finalize = 1;
                $insts->save();
            }

            $lpc->is_finalized = 1;
            $lpc->save();
        }

        return redirect("admin/lpc/finalize")->with(['message' => "LPC posted to ledger", 'status' => 1]);
    }

    public function cancelLpc()
    {
              $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        return view("admin.lpc.cancel");
    }

    public function waive()
    {
              $rights = Role::getrights('waive-charges');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        return view('admin.lpc.waive-charges');
    }

    public function waiveRegisteration()
    {
              $rights = Role::getrights('lpc');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if(!$rights->can_create){
abort(403);
}

        return view('admin.lpc.waive-charges-registeration');
    }

    public function postWaiveRegisteration(Request $request)
    {
        $waive_date             = date('Y-m-d', strtotime(strtr($request->input('waive_date'), '/', '-')));
        $registration_no        = $request->input('registration_no');
        $member                 = members::where('registration_no', $registration_no)->where('is_current_owner', 1)->first();
        $member->is_full_waived = 1;
        $member->waive_remarks  = $request->input('waive_remarks');
        $member->waive_date     = $waive_date;
        $member->save();

        return redirect()->route('lpc.waiveRegisteration')->with(['message' => 'Information saved.', 'status' => 1]);

    }

    public function LpcCharges(Request $request)
    {
        if($request->ajax())
        {
            $registration_no       = $request->registration_no;
            $member = members::where('registration_no', $registration_no)->where('is_current_owner', 1)->first();
            $person           = $member->user;
            $lpcs           = LPC::join('member_installments','member_installments.id','lpc.installment_id')
                    ->select(DB::Raw('lpc.*,member_installments.installment_no'))->where('lpc.registration_no', $registration_no)->get();
            return ['lpcs' => $lpcs, 'person' => $person];
        }
    }

    public function cancelwaive()
    {

        return view('admin.lpc.cancelwaive');
    }

    public function cancelwaive1(Request $request)
    {
        $waive_no = $request->input('waive_no');
        $reg_no   = $request->input('registration_no');
        if ($request->ajax()) {

            $data['installaments'] = DB::table('member_installments')->join('lpc', 'lpc.registration_no', '=', 'member_installments.registration_no')->where('lpc.id', $waive_no)->where('is_other_payment', 0)->where('lpc_charges', '!=', 0)->get();

            $data['otherPayments'] = DB::table('member_installments')->join('lpc', 'lpc.registration_no', '=', 'member_installments.registration_no')->where('lpc.id', $waive_no)->where('is_other_payment', 1)->where('lpc_charges', '!=', 0)->get();
            return $data;

        }
    }

    public function waivepost(Request $request)
    {

        $waive_no = $request->input('waive_no');
        $reg_no   = $request->input('registration_no');
        $plan     = 0;

        $installaments = DB::table('member_installments')->join('lpc', 'lpc.registration_no', 'member_installments.registration_no')->where('lpc.id', $waive_no)->where('member_installments.lpc_charges', '!=', 0)->update(array('member_installments.lpc_charges' => $plan));

        return redirect('admin/lpc/cancelwaive')->with(['message' => "Record has been updated"]);

    }

    public function getLPCs(Request $request)
    {
        $registration_no = $request->registration_no;

        $lpcs           = LPC::join('member_installments','member_installments.id','lpc.installment_id')
                    ->select(DB::Raw('lpc.*,member_installments.installment_no'))->where('lpc.registration_no', $registration_no)->get();
        $total_waive = 0;
        foreach($lpcs as $lpc)
            $total_waive += $lpc->waive_amount;

        return response()->json(['lpcs' => $lpcs, 'total_waive' => $total_waive]);
    }

    public function lpcByRegistration(Request $request)
    {
        if($request->ajax())
        {
            $lpc = LPC::select(
                DB::Raw(
                    'lpc.lpc_amount as lpc_amount, 
                    lpc.lpc_other_amount as lpc_other_amount, 
                    ifnull(lpc.amount_received,0) as amount_received,
                    member_installments.installment_no,
                    lpc.days, ifnull(inst_waive,0) as inst_waive, waive_remarks,receipts.receipt_no, receipts.receiving_date
                '))
            ->join('member_installments', 'member_installments.id', '=', 'lpc.installment_id')
            ->leftJoin('lpc_receipts', 'lpc_receipts.lpc_id', 'lpc.id')
            ->leftJoin('receipts', 'receipts.id', 'lpc_receipts.receipt_id')
            ->where('lpc.registration_no', $request->registration_no)->groupBy('installment_id')->get();

            return $lpc;
        }
    }
}
