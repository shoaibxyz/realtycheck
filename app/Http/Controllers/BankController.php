<?php

namespace App\Http\Controllers;

use App\bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Role;
use Validator;

class BankController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('bank');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $banks = bank::whereNull('deleted_at')->get();
            return view('admin.bank.index', compact('banks','rights'));
        }
        else
    	    abort(403);
    }

    public function create()
    {
        $rights = Role::getrights('bank');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $bank = bank::all();
        return view('admin.bank.create');
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'feature' => 'bank:name',
        ])->validate();

        bank::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Bank has been added successfully.');

        return redirect('admin/bank');
    }

    public function destroy($id)
    {
        $bank = bank::find($id);
        $bank->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Bank has been deleted successfully.');

        return redirect('admin/bank');
    }

    public function edit($id)
    {
        $rights = Role::getrights('bank');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $bank = bank::find($id);
        return view('admin.bank.edit',compact('bank'));
    }

    public function update($id, Request $request)
    {
        $bank = bank::find($id);
        $bank->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Bank has been updated successfully.');

        return redirect('admin/bank');
    }
}
