<?php

namespace App\Http\Controllers\HR;

use App\Accounting\Entry;
use App\Accounting\EntryItem;
use App\Accounting\Tag;
use App\Http\Controllers\Controller;
use App\Jobs\PaySlipEmailJob;
use App\AdvanceSalary;
use App\Models\HR\department;
use App\Models\HR\Designation;
use App\Models\HR\EmployeeAllownceDeduction;
use App\Models\HR\Employees;
use App\Leaves;
use App\LeavesRequests;
use App\PaySlip;
use App\Tax;
use App\User;
use App\Role;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DateTime;

class PaySlipController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('Generate-Payslip');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        
        $departments = department::all();
       
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }


        return view('admin.hr.payslip.index', compact('departmentsArr', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('Generate-Payslip');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $departments = department::all();
       
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        return view('admin.hr.payslip.create', compact('departments', 'departmentsArr'));
    }

    public function checkPayslip(Request $request)
    {
        $department_id = $request->department_id;
        $forMonth      = explode('-', $request->forMonth);

        $month = null;
        $year  = null;
        if ($request->forMonth) {
            $month = $forMonth[0];
            $year  = $forMonth[1];
        }

        $payslip = DB::table('hr_payslip')
            ->select(DB::Raw('hr_payslip.*, hr_department.name as department_name, persons.name as username'))
            ->join('hr_department', 'hr_department.id', '=', 'hr_payslip.department_id')
            ->join('hr_employee', 'hr_employee.id', '=', 'hr_payslip.employee_id')
            ->join('persons', 'persons.id', '=', 'hr_employee.person_id')
            ->when($department_id, function ($query) use ($department_id) {
                return $query->where('hr_payslip.department_id', $department_id);
            })
            ->when($month, function ($query) use ($month) {
                return $query->where('hr_payslip.month', $month);
            })
            ->when($year, function ($query) use ($year) {
                return $query->where('hr_payslip.year', $year);
            })
        // ->where('hr_payslip.agency_id' , $this->getAgency()->id)
            ->orderBy('hr_payslip.id', 'desc')->get();

        $departments = department::all();
       
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        // $checkPaySlip = PaySlip::where('department_id', $department_id)->where('month', $month)->where('year', $year)->get();

        return view('admin.hr.payslip.index', compact('payslip', 'departments', 'departmentsArr', 'department_id', 'month', 'year','agency'));
    }

    public function advanceSalary()
    {
        return view('admin.hr.payslip.advance-salary');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $department_id = $request->department_id;
        $employee_id   = $request->employee_id;
        $month         = $request->month;
        $year          = $request->year;

        

        $checkPaySlip = PaySlip::where('department_id', $department_id)->where('employee_id', $employee_id)->where('month', $month)->where('year', $year)->count();

        if ($checkPaySlip == 0) {
            $input               = $request->input();
            $input['created_by'] = Auth::user()->id;
            $input['code']       = str_random(6);
            $input['bonus'] = $request->input('bonus');

            //Check if advance salary exist
            if ($request->advance_salary) {
                $input['advance_salary']    = $request->advance_salary;
                $input['advance_salary_id'] = $request->advance_salary_id;
            }

            $payslip = PaySlip::create($input);
            $message = "Payslip created";
            $status  = 1;

            
            $employee = Employees::find($request->employee_id);

            $taxes         = Tax::all();
            $incomeTax     = 0;
            $taxPercentage = 0;
            foreach ($taxes as $tax) {
                $salary = $employee->salary * 12;

                if ($salary > $tax->amount_from && $salary <= $tax->amount_to) {
                    $incomeTax     = $tax->tax_amount;
                    $taxPercentage = round((($salary - $tax->amount_from) * $tax->percentage) / 100); //
                    break;
                }
            }

            $allownceDeduction = EmployeeAllownceDeduction::where('employee_id', $payslip->employee_id)->get();

            $admin    = User::where('username', 'admin')->first();
            $receiver = [$employee->user->email, $admin->email];

            // $job = (new PaySlipEmailJob($receiver, $employee->user, $payslip, $employee, $allownceDeduction, $incomeTax, $taxPercentage, $checkAdvanceSalary))->onQueue('payslip');
            // dispatch($job);

            //Adding new tag
            $tag             = new Tag();
            $tag->title      = "emp_" . $employee->id;
            $tag->background = "FF0000";
            $tag->color      = "FFFFFF";
            $tag->module_id  = 39;
            $tag->status     = 1;
            $tag->created_by = Auth::user()->id;
            $tag->created_at = date('Y-m-d H:i:s');
            $tag->updated_by = Auth::user()->id;
            $tag->updated_at = date('Y-m-d H:i:s');
            $tag->save();

            $entry               = new Entry();
            $lastentry           = Entry::orderBy('number', 'desc')->first();
            $entry->number       = ($lastentry != null) ? $lastentry->number + 1 : 1;
            $entry->date         = $year . '-' . $month. '- 01';
            $entry->narration    = $request->receipt_remarks;
            $entry->tag_id       = $tag->id;
            $entry->receipt_id   = $payslip->id;
            $entry->ent_type     = 2;
            $entry->entrytype_id = ($request->receiving_mode == 'cash') ? 68 : 67;
            $entry->module_id    = 41;
            $entry->dr_total     = $request->net_salary;
            $entry->cr_total     = $request->net_salary;
            $entry->status       = 1;
            $entry->created_by   = Auth::user()->id;
            $entry->created_at   = date('Y-m-d H:i:s');
            $entry->updated_by   = Auth::user()->id;
            $entry->updated_at   = date('Y-m-d H:i:s');
            $entry->save();

            //Debit Entry
            $eitem             = new EntryItem();
            $eitem->module_id  = 41;
            $eitem->entry_id   = $entry->id;
            $eitem->ledger_id  = 25;
            $eitem->dc         = 'D';
            $eitem->amount     = $request->net_salary;
            $eitem->status     = 0;
            $eitem->created_by = Auth::user()->id;
            $eitem->created_at = date('Y-m-d H:i:s');
            $eitem->updated_by = Auth::user()->id;
            $eitem->updated_at = date('Y-m-d H:i:s');
            $eitem->save();

            //Credit Entry
            $eitem             = new EntryItem();
            $eitem->module_id  = 41;
            $eitem->entry_id   = $entry->id;
            $eitem->ledger_id  = 23;
            $eitem->dc         = 'C';
            $eitem->amount     = $request->net_salary;
            $eitem->status     = 0;
            $eitem->created_by = Auth::user()->id;
            $eitem->created_at = date('Y-m-d H:i:s');
            $eitem->updated_by = Auth::user()->id;
            $eitem->updated_at = date('Y-m-d H:i:s');
            $eitem->save();

        } else {
            $message = "This slip is already created.";
            $status  = 0;
        }

        return redirect()->route('payslip.create')->with(['message' => $message, 'status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        $payslip  = PaySlip::with(['department'])->find($id);
        $employee = Employees::find($payslip->employee_id);
        $designation = Designation::where('id' , $employee->designation_id)->first();

        $EmployeeAllownceDeduction = EmployeeAllownceDeduction::where('employee_id', $payslip->employee_id)->get();
        
        $s_d =cal_days_in_month(CAL_GREGORIAN,$payslip->month,$payslip->year);
        
        $yearly_salary = PaySlip::where('employee_id' , $employee->id)
       ->where('year' , $payslip->year)
       ->sum('net_salary');
     
       $leaves = LeavesRequests::where('employee_id' , $employee->id)
       ->where('status',1)
       ->where((DB::raw('MONTH(leave_start_date)')) , $payslip->month)
       ->where((DB::raw('MONTH(leave_end_date)')) , $payslip->month)
       ->sum('no_days');
        
        $s_days = $s_d - $leaves;
     
        $data = [
            'payslip'           => $payslip,
            'employee'          => $employee,
            'user'              => $employee->user,
            'allownceDeduction' => $EmployeeAllownceDeduction,
            'designation'     => $designation,
            's_days'      => $s_days,
            'yearly_salary' => $yearly_salary
            
        ];
        return response()->json([$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatePayslip(Request $request)
    {

        $department = department::find($request->department_id);
        $employee   = Employees::find($request->employee_id);
        $forMonth   = explode('-', $request->forMonth);

        $month = $forMonth[0];
        $year  = $forMonth[1];
        // if($month . '-'. $year != date('m-Y'))
        //     return redirect()->route('payslip.create')->with(['message' => 'Payslip cannot be generated in advance or previous dates.', 'status' => 0]);

        $checkAdvanceSalary = AdvanceSalary::where('employee_id', $request->employee_id)->where('deduct_month', $month)->where('deduct_year', $year)->where('is_approved', 1)->groupBy('employee_id')->sum('amount');

        $departments        = department::all();
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $depart) {
            $departmentsArr[$depart->id] = $depart->name;
        }

        $employeeList = [];
        $employees    = Employees::where('department_id', $department->id)->get();
        foreach ($employees as $emp) {
            $employeeList[$emp->id] = $emp->user->name;
        }

        $allownceDeduction = EmployeeAllownceDeduction::where('employee_id', $employee->id)->get();

        $taxes         = Tax::all();
        $incomeTax     = 0;
        $taxPercentage = 0;
        foreach ($taxes as $tax) {
            $salary = $employee->salary * 12;

            if ($salary > $tax->amount_from && $salary <= $tax->amount_to) {
                $incomeTax     = $tax->tax_amount;
                $taxPercentage = round((($salary - $tax->amount_from) * $tax->percentage) / 100); //
                break;
            }
        }

        $leaves = Leaves::whereNull('deleted_at')->get();

        $leaveRequests = LeavesRequests::where('leave_start_date', 'LIKE', \Carbon\Carbon::now()->subMonth()->format('Y-m') . '%')->where('employee_id', $employee->id)->whereNull('deleted_at')->get();

        $date = $request->forMonth;
        return view('admin.hr.payslip.make-payment', compact('department', 'employee', 'month', 'year', 'allownceDeduction', 'departmentsArr', 'employeeList', 'checkAdvanceSalary', 'incomeTax', 'taxPercentage', 'leaves', 'leaveRequests', 'date'));
    }
}
