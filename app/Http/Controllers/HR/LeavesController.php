<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Models\HR\department;
use App\Leaves;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Role;

class LeavesController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (!\Auth::user()->can(('can_view_leave'))) {
        //     abort(403);
        // }

        // $can_edit   = Auth::user()->can('can_edit_leave');
        // $can_delete = Auth::user()->can('can_delete_leave');

         //$leaves = Leaves::whereNull('deleted_at')->orderBy('id', 'desc')->first();
        $rights = Role::getrights('leave');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        $leaves = DB::table('hr_leaves')->get();
        
        return view('admin.hr.leaves.index', compact('leaves'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if (!\Auth::user()->can(('can_create_leave'))) {
        //     abort(403);
        // }
$rights = Role::getrights('leave');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $departments = department::all();
        
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        return view('admin.hr.leaves.create', compact('departmentsArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input               = $request->input();
        $input['created_by'] = Auth::user()->id;

        $leave = Leaves::create($input);

        return redirect()->route('leave.index')->with(['message' => 'Leave Added Successfully', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('leave');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        // if (!\Auth::user()->can(('can_edit_leave'))) {
        //     abort(403);
        // }

        $leave = Leaves::findorfail($id);

        return view('admin.hr.leaves.edit', compact('leave'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $leave = Leaves::findorfail($id);

        $input = $request->input();
        $leave->update($input);

        return redirect()->route('leave.index')->with(['message' => 'Leave Request Updated', 'status' => 1]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (!\Auth::user()->can(('can_delete_leave'))) {
        //     abort(403);
        // }

        $leave = Leaves::findorfail($id);
        $leave->delete();
        return redirect()->route('leave.index')->with(['message' => 'Leave Deleted Successfully', 'status' => 1]);
    }
}
