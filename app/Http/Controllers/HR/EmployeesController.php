<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Models\Access\roles;
use App\Models\City;
use App\Models\Country;
use App\Models\HR\department;
use App\Models\HR\Designation;
use App\Models\HR\EmployeeAllownceDeduction;
use App\Models\HR\Employees;
use App\Models\State;
use App\Traits\CustomFunctions;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class EmployeesController extends Controller
{

    use CustomFunctions;

    public function index()
    {
        
        
        $rights = Role::getrights('employee');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        
        $persons                    = User::whereNull('deleted_at')->get();
        $employees                  = Employees::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
       
        return view('admin.hr.employees.index' , compact('persons','employees'));

    }
    
    public function getemployee(Request $request){

        //dd($request);
         
         $rights = Role::getrights('employee');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        
        $persons                    = User::all();
        $employees                  = Employees::orderBy('id', 'DESC')->whereNull('deleted_at')->get();


        
        $data = Datatables::of($employees)

            ->addColumn('actions', function ($employees) {
                $html = "";
                $html .= "<div class='text-center'>";
                

                 
                    $html .= '<a class="btn btn-xs btn-primary viewEmployee" data-toggle="modal" href="#viewEmoloyeeModel" title="View" data-employeeid=' . $employees->user->id . '><i class="fa fa-eye"></i></a>';
                    
                    

                    $html .= '<a href="' . route('employee.edit', $employees->user->id) . '" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                

                
                    $html .= '<form method="post" style="display: inline" action="' . route('employee.destroy', $employees->user->id) . '">' . csrf_field() . "<input name='_method' type='hidden' value='DELETE'> <button type='submit' class='btn btn-danger btn-xs DeleteBtn delete' data-toggle='tooltip' title='Delete'> <i class='fa fa-times'></i></button>
                            </form>";
                            
                    if($employees->resume != ""){
                        
                          $html .= '<a href="' . route('employee.download', $employee->id) . '" class="btn btn-xs btn-info edit" target="_blank">Download Resume</a>';
                    }
              
                $html .= "</div>";
                return $html;
            })

            
            ->editColumn('picture', function ($employees) {

                
                $html = '';             
                $html .= '<img src="' . env('STORAGE_PATH_Listing') . $employees->user->picture  .'" class="img-responsive" style="width:40px;height:30px;">'; 
                return $html;
            })

            ->editColumn('name', function ($employees) {
                return $employees->user->name;
            })

            ->editColumn('email', function ($employees) {
                return $employees->user->email;
            })

            ->editColumn('phone', function ($employees) {


                return $employees->user->phone_mobile;
            })

            ->editColumn('status', function ($employees) {

                
                $html = '';
                
                    $html .= '<a href="" class="btn btn-success btn-xs">Approve</a>'; 

                return $html;
            })

            
          ->rawColumns(['actions' , 'picture' , 'status'])
            ->make(true);
        return $data;
    }


    public function hrdashboard()
    {
        return view('admin.hr.hrdashboard');

    }

    public function create()
    {
        $rights = Role::getrights('employee');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        // if (!\Auth::user()->can(('can_create_employee'))) {
        //     abort(403);
        // }

        $persons   = User::all();
        $employees = User::Employees();
       
        $departments  = department::whereNull('deleted_at')->get();
        $designations = Designation::whereNull('deleted_at')->get();
   
        $countries = Country::all();

        return view('admin.hr.employees.create', compact('employees', 'persons', 'roles', 'countries', 'designations', 'departments'));
    }

    public function show($id)
    {
        // if (!\Auth::user()->can(('can_view_employee'))) {
        //     abort(403);
        // }

        $employee = Employees::with(['user', 'designation', 'department'])->where('person_id', $id)->firstorFail();
        // dd($employee);
        $allownceDeduction = EmployeeAllownceDeduction::where('employee_id', $employee->id)->get();
        return response()->json(['employee' => $employee, 'allownceDeduction' => $allownceDeduction]);
    }

    public function store(EmployeeRequest $request)
    {

        $person = new User();

        if (Input::hasfile('picture')) {
            $image    = Input::file('picture');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);

            $person['picture'] = $filename;
        }

        if (Input::hasfile('cnic_pic')) {
            $image    = Input::file('cnic_pic');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename, 'private');
            $person['cnic_pic'] = $filename;
        }

        $person->name              = $request->input('name');
        $person->password          = 123456;
        $person->username          = $request->input('name');
        $person->guardian_type     = $request->input('guardian_type');
        $person->gurdian_name      = $request->input('gurdian_name');
        $person->nationality       = $request->input('nationality');
        $person->cnic              = $request->input('cnic');
        $person->email             = $request->input('email');
        $person->phone_mobile      = $request->input('phone_mobile');
        $person->current_address   = $request->input('current_address');
        $person->permanent_address = $request->input('permanent_address');
        $person->country_id        = $request->input('country_id');
        $person->state_id          = $request->input('state');
        $person->city_id           = $request->input('city');
        $person->phone_office      = $request->input('phone_office');
        $person->phone_res         = $request->input('phone_rec');
        $person->passport          = $request->input('passport');
        $person->dateofbirth       = date('Y-m-d', strtotime(strtr($request->input('dob'), '/', '-')));
        $person->ref_pk            = $request->input('ref_pk');
        $person->is_approved       = 1;
        $person->email_verified    = 1;
        $person->save();

        $persons = $person->id;

        $resume = "";
        if (Input::hasfile('resume')) {
            $image    = Input::file('resume');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public/resume', $image, $filename);
            $resume = $filename;
        }

        $employee = Employees::create([
            'person_id'        => $person->id,
            'department_id'    => $request->input('department_id'),
            'designation_id'   => $request->input('designation_id'),
            'ntn_number'       => $request->input('ntn_number'),
            'last_company'     => $request->input('last_company'),
            'joining_date'     => date('Y-m-d', strtotime(strtr($request->input('joining_date'), '/', '-'))),
            'leaving_date'     => $request->input('leaving_date') ? date('Y-m-d', strtotime(strtr($request->input('leaving_date'), '/', '-'))) : Null,
            'total_experience' => $request->input('total_experience'),
            'salary'           => $request->input('salary'),
            'commission'       => $request->input('commission'),
            'resume'           => $resume,
            'payment_type'       => $request->input('payment_type'),
            'acc_number'       => $request->input('acc_number'),
            'appointment_status'       => $request->input('appointment_status'),
            'is_restricted'           => $request->input('is_restricted'),
            'start_time'           => $request->input('start_time'),
            'end_time'           => $request->input('end_time'),
            'created_by'       => Auth::user()->id,
        ]);

        // $role = roles::where('name', 'Employee')->first();
        $person->Roles()->attach(2);

        if ($request->input('deduction_label')) {
            foreach ($request->input('deduction_label') as $key => $deduction) {
                $deduction_amount = $request->input('deduction_amount')[$key];
                if ($deduction_amount != 0 && $deduction_amount != "") {
                    EmployeeAllownceDeduction::create([
                        'label'       => $deduction,
                        'amount'      => $deduction_amount,
                        'employee_id' => $employee->id,
                        'type'        => 0,
                        'created_by'  => Auth::user()->id,
                    ]);
                }
            }
        }

        if ($request->input('allownce_label')) {
            foreach ($request->input('allownce_label') as $key => $allownce) {
                $allownce_amount = $request->input('allownce_amount')[$key];

                if ($allownce_amount != 0 && $allownce_amount != "") {
                    EmployeeAllownceDeduction::create([
                        'label'       => $allownce,
                        'amount'      => $allownce_amount,
                        'employee_id' => $employee->id,
                        'type'        => 1,
                        'created_by'  => Auth::user()->id,
                    ]);
                }

            }
        }

        // Mail::to($person)->send(new SendWelcomeEmail($person, $password));

        return redirect()->route('employee.index');
    }

    public function destroy($id)
    {
        // if (!\Auth::user()->can(('can_delete_employee'))) {
        //     abort(403);
        // }

        $person           = User::find($id);
        $person->status   = 0;
        $person->cnic     = $person->cnic . '-' . uniqid();
        $person->username = $person->username . '-' . uniqid();
        $person->save();

        $employee             = Employees::where('person_id', $id)->first();
        $employee->deleted_at = date('Y-m-d H:i:s');
        $employee->save();

        return redirect()->route('employee.index');
    }

    public function edit($id)
    {
        // if (!\Auth::user()->can(('Edit-Employee'))) {
        //     abort(403);
        // }

        $rights = Role::getrights('employee');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        
        $employee  = Employees::where('person_id', $id)->firstOrFail();
        $countries = Country::all();
        $states    = State::where('country_id', $employee->user->country_id)->get();
        $cities    = City::where('state_id', $employee->user->state_id)->get();
        $departments  = department::whereNull('deleted_at')->get();
        $designations = Designation::whereNull('deleted_at')->get();

        $EmployeeAllownceDeduction = EmployeeAllownceDeduction::where('employee_id', $employee->id)->get();

        // dd($employee->user->Roles[0]->name);
        return view('admin.hr.employees.edit', compact('employee', 'countries', 'states', 'cities', 'departments', 'designations', 'EmployeeAllownceDeduction'));
    }

    public function AgentInfo(Request $request)
    {
        if ($request->ajax()) {
            $employees = Employees::where('person_id', $request->input('agent_id'))->first();
            return $employees;
        }
    }

    public function update($id, Request $request)
    {
        
        // dd(Input::hasfile('resume'));
        $employee = Employees::find($id);
        $input    = $request->input();
        $resume   = "";
        if (Input::hasfile('resume')) {
            $image    = Input::file('resume');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);
            $input['resume'] = $filename;
        }

        $input['joining_date'] = date('Y-m-d', strtotime(strtr($request->input('joining_date'), '/', '-')));
        $input['leaving_date'] = date('Y-m-d', strtotime(strtr($request->input('leaving_date'), '/', '-')));
        
        // if($request->input('leaving_date') == NULL){
        //     $input['is_restricted'] = 0;
        // }else{
        //     $input['is_restricted'] = 1;
        // }
        
        $employee->update($input);

        if (Input::hasfile('picture')) {
            $image    = Input::file('picture');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);

            $input['picture'] = $filename;
        }

        if (Input::hasfile('cnic_pic')) {
            $image    = Input::file('cnic_pic');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);
            $input['cnic_pic'] = $filename;
        }

        $input['dateofbirth'] = date('Y-m-d', strtotime(strtr($request->input('dateofbirth'), '/', '-')));
        

        $employee->user->update($input);

        EmployeeAllownceDeduction::where('employee_id', $employee->id)->forcedelete();
        if ($request->input('deduction_label')) {
            foreach ($request->input('deduction_label') as $key => $deduction) {
                $deduction_amount = $request->input('deduction_amount')[$key];
                if ($deduction_amount != 0 && $deduction_amount != "") {
                    EmployeeAllownceDeduction::create([
                        'label'       => $deduction,
                        'amount'      => $deduction_amount,
                        'employee_id' => $employee->id,
                        'type'        => 0,
                        'created_by'  => Auth::user()->id,
                    ]);
                }
            }
        }

        if ($request->input('allownce_label')) {

            foreach ($request->input('allownce_label') as $key => $allownce) {
                $allownce_amount = $request->input('allownce_amount')[$key];

                if ($allownce_amount != 0 && $allownce_amount != "") {
                    EmployeeAllownceDeduction::create([
                        'label'       => $allownce,
                        'amount'      => $allownce_amount,
                        'employee_id' => $employee->id,
                        'type'        => 1,
                        'created_by'  => Auth::user()->id,
                    ]);
                }

            }
        }

        return redirect()->route('employee.index')->with(['message' => 'Employee Updated', 'status' => 1]);
    }

    public function approveStatus($id, $status)
    {
        $user              = User::find($id);
        $status            = ($status == 0) ? 1 : 0;
        $user->is_approved = $status;
        $user->save();
        return redirect()->route('employee.index');
    }

    public function getEmployeeByDepartment(Request $request)
    {
        if ($request->ajax()) {
            $employees = Employees::with(['user'])->where('department_id', $request->input('department_id'))->whereNull('deleted_at')->get();
            $html      = "<select class='form-control' name='employee_id'>";
            $html .= "<option value=''>Select Employee</option>";
            foreach ($employees as $employee) {
                $html .= "<option value='" . $employee->id . "'>" . $employee->user->name . " (" . $employee->designation->name . ")</option>";
            }

            $html .= "</select>";

            return response()->json(['employees' => $html]);
        }
    }
    
    public function downloadImage($imageId){
   $employee = Employees::where('id', $imageId)->firstOrFail();
   $path = storage_path(). '/app/public/resume/'. $employee->resume;
   return response()->download($path, $employee
            ->original_filename, ['Content-Type' => $employee->mime]);
    }
}
