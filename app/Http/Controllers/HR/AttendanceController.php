<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Attendance;
use App\Models\HR\department;
use App\Models\HR\Designation;
use App\Models\HR\Employees;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    use CustomFunctions;

    public function index()
    {
        $departments = department::all();
     
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        return view('admin.hr.attendance.index', compact('departmentsArr'));
    }

    public function create()
    {

        $employees = Employees::all();
        return view('admin.hr.attendance.create', compact('employees'));
    }

    public function store(Request $request)
    {
        $attendance               = new Attendance();
        $attendance->employee_id  = $request->employee;
        $attendance->clock_in_out = date('Y-m-d H:i:s');
        $attendance->type         = $request->attendanceStatus;
        $attendance->created_by   = Auth::user()->id;

        $attendance->save();

        return response()->json(['attendance' => $attendance, 'status' => 1]);
    }

    public function storeLocation(Request $request)
    {
        $attendance = Attendance::find($request->attendance_id);

        $attendance->latitude = $request->lat;
        $attendance->longitude = $request->lng;
        $attendance->formatted_address = $this->getUserLocation($request->lat, $request->lng);
        $attendance->save();

        return response()->json(['location' => true]);
    }

    public function addRemarks(Request $request)
    {
        $attendance          = Attendance::find($request->attendance_id);
        $attendance->remarks = $request->remarks;
        $attendance->save();

        return response()->json(['attendance' => $attendance, 'status' => 1]);
    }

    public function history(Request $request)
    {
        $department = $request->department_id;
        $employee   = $request->employee_id;
        $from       = ($request->from != "") ? date('Y-m-d', strtotime($request->from)) . " 00:00:00" : null;
        $to         = ($request->from != "") ? date('Y-m-d', strtotime($request->to)) . " 23:59:00" : null;

        $attendance = Attendance::with(['employee'])->where('employee_id', $employee)->whereBetween('clock_in_out', [$from, $to])->get();

        // $attendance = DB::table('hr_attendance')->select(DB::Raw('hr_attendance.*, hr_employee.*, persons.*'))
        //                 ->join('hr_employee','hr_employee.id','=','hr_attendance.employee_id')
        //                 ->join('hr_department','hr_department.id','=','hr_employee.department_id')
        //                 ->join('hr_designations','hr_designations.id','=','hr_employee.designation_id')
        //                 ->join('persons','hr_employee.person_id','=','persons.id')
        //                 ->where('hr_employee.department_id', $department)
        //                 ->where('hr_attendance.employee_id', $employee)
        //                 ->whereBetween('hr_attendance.clock_in_out',[$from,$to])->get();

        $departments        = department::all();
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }
        // dd($attendance);

        return view('admin.hr.attendance.index', compact('attendance', 'departmentsArr'));
    }
}
