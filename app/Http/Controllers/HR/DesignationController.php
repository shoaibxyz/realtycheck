<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Models\HR\Designation;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Role;
use Validator;

class DesignationController extends Controller
{

    use CustomFunctions;
    public function index()
    {
        $rights = Role::getrights('designation');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        $designations = Designation::whereNull('deleted_at')->get();
        return view('admin.hr.designations.index', compact('designations'));
    }

    public function create()
    {
        $rights = Role::getrights('designation');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.hr.designations.create');
    }

    public function store(Request $request)
    {
        $input = $request->input();

        $input['created_by'] = Auth::user()->id;
        Designation::create($input);
        $message = "New designation added";
        return redirect()->route('designation.index')->with(['message' => $message, 'status' => 1]);
    }

    public function destroy($id)
    {
        $designation = Designation::find($id);
        $designation->delete();
        return redirect()->route('designation.index');
    }

    public function edit($id)
    {
        $rights = Role::getrights('designation');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $designation = Designation::find($id);
        return view('admin.hr.designations.edit', compact('designation'));
    }

    public function update($id, Request $request)
    {
        $designation      = Designation::find($id);
        $designation->name = $request->input('name');
        $designation->save();

        $request->session()->flash("message", "designation saved");
        $request->session()->flash("status", 1);

        return redirect()->route('designation.index');
    }
}
