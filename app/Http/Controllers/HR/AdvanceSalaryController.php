<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\AdvanceSalary;
use App\Models\HR\department;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdvanceSalaryController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $advanceSalary = AdvanceSalary::whereNull('deleted_at')->get();
        
        // $advanceSalary = DB::table('hr_advance_salary as s')
        // ->leftjoin('hr_employee', 'hr_employee.id', '=', 's.employee_id')
        // ->leftjoin('persons','persons.id','hr_employee.person_id')
        // ->leftjoin('hr_department','hr_department.id','hr_employee.department_id')
        // ->select('persons.name as name','hr_department.name as department','s.amount','s.deduct_month','s.deduct_year','s.created_at','s.is_approved','s.id')
        // ->get();
         
        return view('admin.hr.advance-salary.index', compact('advanceSalary'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = department::all();
        $departmentsArr  = [];
        $departmentsArr['']  = 'Select Department';
        foreach($departments as $department)
        {
            $departmentsArr[$department->id] = $department->name;
        }
        return view('admin.hr.advance-salary.create', compact('departments', 'departmentsArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input               = $request->input();
        $input['created_by'] = Auth::user()->id;
        $advanceSalary       = AdvanceSalary::create($input);
        return redirect()->route('advance-salary.index')->with(['message' => 'Advance Salary Saved', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function checkAdvanceSalary(Request $request)
    {
        $forMonth = explode('-', $request->forMonth);

        $month = $forMonth[0];
        $year  = $forMonth[1];

        if ($month == null && $year == null) {
            return redirect()->route('advance-salary.index');
        }

        $advanceSalary = AdvanceSalary::where('deduct_year', $year)->where('deduct_month', $month)->whereNull('deleted_at')->get();
        // $advanceSalary = DB::table('hr_advance_salary')
        //                 ->select(DB::Raw('hr_advance_salary.*, hr_department.name as department_name, persons.name as username'))
        //                 ->join('hr_employee','hr_employee.id','=','hr_advance_salary.employee_id')
        //                 ->join('hr_department','hr_department.id','=','hr_employee.department_id')
        //                 ->join('persons','persons.id','=','hr_employee.person_id')
        //                 ->when($month, function ($query) use ($month) {
        //                     return $query->where('hr_advance_salary.deduct_month', $month);
        //                 })
        //                 ->when($year, function ($query) use ($year) {
        //                     return $query->where('hr_advance_salary.deduct_year', $year);
        //                 })->
        //                 orderBy('hr_advance_salary.id','desc')->get();

        return view('admin.hr.advance-salary.index', compact('advanceSalary', 'month', 'year'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advanceSalary = AdvanceSalary::findorfail($id);

        $advanceSalary->is_approved = ($request->status_val == 1) ? 0 : 1;
        $advanceSalary->save();

        return redirect()->route('advance-salary.index')->with(['message' => 'Advance Salary status updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advanceSalary = AdvanceSalary::findorfail($id);
        $advanceSalary->delete();

        return redirect()->route('advance-salary.index')->with(['message' => 'Advance Salary deleted', 'status' => 1]);
    }
}
