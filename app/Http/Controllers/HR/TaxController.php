<?php

namespace App\Http\Controllers\HR;
use App\Tax;
use App\Role;
use App\Http\Controllers\Controller;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\DB;
class TaxController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('tax');
        
        if(!$rights->can_view){
    	    abort(403);
        }
         $taxes = Tax::whereNull('deleted_at')->get();
        //$taxes = DB::table('hr_tax')->get();
        return view('admin.hr.tax.index', compact('taxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rights = Role::getrights('tax');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.hr.tax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'amount_from' => 'required|integer',
            'amount_to'   => 'required|integer',
            'tax_amount'  => 'required|integer',
            'percentage'  => 'required',
            'from_date'   => 'required',
            'to_date'     => 'required',
        ]);

        if ($request->amount_from >= $request->amount_to) {
            return redirect()->back()->withInput()->with(['message' => '`Amount From` must be less than `Amount To`']);
        }

        $input               = $request->input();
        $input['from_date']  = date('Y-m-d', strtotime(strtr($request->from_date, '/', '-')));
        $input['to_date']    = date('Y-m-d', strtotime(strtr($request->to_date, '/', '-')));
        $input['created_by'] = Auth::user()->id;

        $tax = Tax::create($input);

        return redirect()->back()->withInput()->with(['message' => 'Tax definition saved.', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rights = Role::getrights('tax');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $tax = Tax::findorfail($id);
        return view('admin.hr.tax.edit', compact('tax'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount_from' => 'required|integer',
            'amount_to'   => 'required|integer',
            'tax_amount'  => 'required|integer',
            'percentage'  => 'required',
            'from_date'   => 'required',
            'to_date'     => 'required',
        ]);

        if ($request->amount_from >= $request->amount_to) {
            return redirect()->back()->withInput()->with(['message' => '`Amount From` must be less than `Amount To`']);
        }

        $input               = $request->input();
        $input['from_date']  = date('Y-m-d', strtotime(strtr($request->from_date, '/', '-')));
        $input['to_date']    = date('Y-m-d', strtotime(strtr($request->to_date, '/', '-')));
        $input['updated_by'] = Auth::user()->id;

        $tax = Tax::findorfail($id);
        $tax->update($input);

        return redirect()->route('tax.index')->with(['message' => 'Tax definition updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tax = Tax::findorfail($id);
        $tax->delete();

        return redirect()->route('tax.index')->with(['message' => 'Tax definition deleted', 'status' => 1]);
    }
    
    public function viewsms()
    {
        $response = Curl::to('http://my.ezeee.pk/sendsms_url.html')
        ->withData( 
        	array( 
        		'Username' => '03554012423',
        		'Password' => '@03554012423',
        		'From' => 'CapitalCity',
        		'To' => '03014012423',
        		'Message' => 'Testing',
        		'language' => 'English',
        	) 
        )
        ->get();
        
        
        return $response;
    }
    
    
}
