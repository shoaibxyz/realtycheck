<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Models\HR\department;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use App\Role;

class DepartmentController extends Controller
{
    use CustomFunctions;
    public function index()
    {  $rights = Role::getrights('department');
        if(!$rights->can_view){
    	    abort(403);
        }

        $department = department::whereNull('deleted_at')->get();
        return view('admin.hr.department.index', compact('department'));

    }

    public function create()
    {
        $rights = Role::getrights('department');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        $department = department::all();
        return view('admin.hr.department.create');
    }

    public function store(Request $request)
    {
        $input = $request->input();

        $input['created_by'] = Auth::user()->id;
        department::create($input);
        $message = "New Department added";
        return redirect()->route('department.index')->with(['message' => $message, 'status' => 1]);
    }

    public function destroy($id)
    {
        $department = department::find($id);
        $department->delete();
        return redirect()->route('department.index');
    }

    public function edit($id)
    {
        $rights = Role::getrights('department');
        if(!$rights->can_edit){
    	    abort(403);
        }
        $department = department::find($id);
        return view('admin.hr.department.edit', compact('department'));
    }

    public function update($id, Request $request)
    {
        $department      = department::find($id);
        $department->name =  $request->input('name');
        $department->save();

        $request->session()->flash("message", "Department saved");
        $request->session()->flash("status", 1);

        return redirect()->route('department.index');
    }
}
