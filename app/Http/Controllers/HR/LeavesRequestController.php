<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
//use App\Jobs\LeaveEmailJob;
use App\Models\HR\department;
use App\Models\HR\Employees;
use App\Leaves;
use App\LeavesRequests;
use App\User;
use App\Traits\CustomFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Role;
use DateTime;

class LeavesRequestController extends Controller
{
    use CustomFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rights = Role::getrights('leave-type');
        
        if(!$rights->can_view){
    	    abort(403);
        }
        // if (!\Auth::user()->can(('can_view_leave'))) {
        //     abort(403);
        // }

        // $can_edit   = Auth::user()->can('can_edit_leave');
        // $can_delete = Auth::user()->can('can_delete_leave');

        $departments = department::all();

        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

       
       $leaves = LeavesRequests::with(['employee'])->whereNull('deleted_at')->orderBy('id', 'desc')->get();
        
            // $leaves = DB::table('hr_leaves_requests as a')
            // ->leftjoin('hr_leaves', 'hr_leaves.id', '=', 'a.leave_type_id')
            // ->leftjoin('hr_employee', 'hr_employee.id', '=', 'a.employee_id')
            // ->leftjoin('persons','persons.id','hr_employee.person_id')
            // ->leftjoin('hr_department','hr_department.id','hr_employee.department_id')
            // ->select('persons.name as emp_name','hr_department.name as dep_name', 'hr_leaves.name as leave_type', 'a.leave_start_date', 'a.leave_end_date', 'a.reason', 'a.status', 'a.attachment','a.id')
            // ->get();
        // }
            //   dd($leaves->dep_name);

        return view('admin.hr.leaves-requests.index', compact('leaves', 'departmentsArr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if (!\Auth::user()->can(('can_create_leave'))) {
        //     abort(403);
        // }
        $rights = Role::getrights('leave-type');
        
        if(!$rights->can_create){
    	    abort(403);
        }

        $departments = department::all();
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }
        $leaveTypes = Leaves::all();

        return view('admin.hr.leaves-requests.create', compact('departmentsArr', 'leaveTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $input['created_by']       = Auth::user()->id;
        $input['leave_start_date'] = date('Y-m-d', strtotime(strtr($request->input('leave_start_date'), '/', '-')));
        $input['leave_end_date']   = date('Y-m-d', strtotime(strtr($request->input('leave_end_date'), '/', '-')));
        
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $input['leave_end_date']);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $input['leave_start_date']);
        $input['no_days']  = $to->diffInDays($from);

        if (Input::hasfile('attachment')) {
            $image    = Input::file('attachment');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);

            $input['attachment'] = $filename;
        }

        $user = Employees::find($request->employee_id)->user;

        $leave    = LeavesRequests::create($input);
        $admin    = User::where('username', 'admin')->first();
        $receiver = [$user->email, $admin->email];

        //$job = (new LeaveEmailJob($receiver, $user, $leave, 'new'))->onQueue('leaveMail');
        //dispatch($job);
        // Mail::to($admin)->send(new LeaveEmail($user, $leave, 'new'));

        return redirect()->route('leave-request.index')->with(['message' => 'Leave Added Successfully', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // if (!\Auth::user()->can(('can_edit_leave'))) {
        //     abort(403);
        // }

        $rights = Role::getrights('leave-type');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        
        $leave              = LeavesRequests::findorfail($id);
        $departments        = department::all();
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        $employees = Employees::where('department_id', $leave->employee->department->id)->get();

        $employeesArr     = [];
        $employeesArr[''] = 'Select Employees';
        foreach ($employees as $employee) {
            $employeesArr[$employee->id] = $employee->user->name;
        }

        $leaveTypes = Leaves::all();

        return view('admin.hr.leaves-requests.edit', compact('leave', 'departmentsArr', 'employees', 'employeesArr', 'leaveTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $leave = LeavesRequests::findorfail($id);

        $input                     = $request->input();
        $input['updated_by']       = Auth::user()->id;
        $input['leave_start_date'] = date('Y-m-d', strtotime(strtr($request->input('leave_start_date'), '/', '-')));
        $input['leave_end_date']   = date('Y-m-d', strtotime(strtr($request->input('leave_end_date'), '/', '-')));
        
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $input['leave_end_date']);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $input['leave_start_date']);
        $input['no_days']  = $to->diffInDays($from);

        if (Input::hasfile('attachment')) {
            $image    = Input::file('attachment');
            $filename = Auth::user()->id . '-' . uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image, $filename);

            $input['attachment'] = $filename;
        }

        $leave->update($input);

        return redirect()->route('leave-request.index')->with(['message' => 'Leave Request Updated', 'status' => 1]);

    }

    public function changeStatus($leave_id, $status)
    {
        $leave         = LeavesRequests::findorfail($leave_id);
        $leave->status = $status;
        $leave->save();

        $user     = Employees::find($leave->employee_id)->user;
        $admin    = User::where('username', 'admin')->first();
        $receiver = [$user->email, $admin->email];

        //$job = (new LeaveEmailJob($receiver, $user, $leave, 'update'))->onQueue('leaveMail');
        //dispatch($job);

        return redirect()->route('leave-request.index')->with(['message' => 'Leave status updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (!\Auth::user()->can(('can_delete_leave'))) {
        //     abort(403);
        // }

        $leave = LeavesRequests::findorfail($id);
        $leave->delete();

        // $user = Employees::find($leave->employee_id)->user;
        // // dd($emails);
        // Mail::to($user)->send(new LeaveEmail($user, $leave, 'delete'));

        // $admin = User::where('username', 'admin')->first();
        // Mail::to($admin)->send(new LeaveEmail($user, $leave, 'delete'));

        return redirect()->route('leave-request.index')->with(['message' => 'Leave Deleted Successfully', 'status' => 1]);
    }

    public function checkRequest(Request $request)
    {
        // $can_edit   = Auth::user()->can('can_edit_leave');
        // $can_delete = Auth::user()->can('can_delete_leave');

        $departments        = department::all();
        $departmentsArr     = [];
        $departmentsArr[''] = 'Select Department';
        foreach ($departments as $department) {
            $departmentsArr[$department->id] = $department->name;
        }

        $from     = date('Y-m-d', strtotime(strtr($request->input('from'), '/', '-')));
        $to       = date('Y-m-d', strtotime(strtr($request->input('to'), '/', '-')));
        $employee = $request->employee_id;

        // if ($can_edit) {
            // $leaves = LeavesRequests::whereNull('deleted_at')->where('employee_id', $employee)->where('leave_start_date', '>=', $from)->OrWhere('leave_end_date', '<=', $to)->orderBy('id', 'desc')->get();
        // } else {
            $leaves = LeavesRequests::whereNull('deleted_at')->where('leave_start_date', '>=', $from)->where('leave_end_date', '<', $to)->where('created_by', Auth::user()->id)->orderBy('id', 'desc')->where('employee_id', $employee)->get();
        // }
// $leaves = DB::table('hr_leaves_requests as a')
//             ->leftjoin('hr_leaves', 'hr_leaves.id', '=', 'a.leave_type_id')
//             ->leftjoin('hr_employee', 'hr_employee.id', '=', 'a.employee_id')
//             ->leftjoin('persons','persons.id','hr_employee.person_id')
//             ->leftjoin('hr_department','hr_department.id','hr_employee.department_id')
//             ->select('persons.name as emp_name','hr_department.name as dep_name', 'hr_leaves.name as leave_type', 'a.leave_start_date', 'a.leave_end_date', 'a.reason', 'a.status', 'a.attachment','a.id')
//             ->where('a.leave_start_date', '>=', $from)->where('leave_end_date', '<', $to)->get();
            
        // dd($leaves);
        return view('admin.hr.leaves-requests.index', compact('leaves','departmentsArr', 'from', 'to'));
        // dd($leaves);
    }
}
