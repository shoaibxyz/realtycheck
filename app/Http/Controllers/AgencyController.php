<?php

namespace App\Http\Controllers;

use App\agency;
use App\agents;
use App\Payments;
use App\assignFiles;
use App\principal_officer;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;

class AgencyController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('agency');
         if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
    	    $Agency = agency::orderBy('name','asc')->get();
            return view('admin.agency.index', compact('Agency','rights'));
        }
        else
            return view('admin.unauthorize')->with('exception', env('UNAUTHORIZE'));
    }

    public function getAgencyByRegistrationNo(Request $request)
    {
        $assignFile = assignFiles::where('registration_no', $request->input('reg'))->where('is_blocked',0)->first();
        if($assignFile){
            if($request->is_preference)
            {
                $booking = Payments::where('registration_no', $request->reg)->first();
                $featureset = $booking->featureset;

                $featuresetArr = [];

                $html = "<select name='featureset_id' class='form-control' id='featureset_id'>";
                $html .= "<option value=''>Select Preference</option>";
                foreach($featureset as $feature)
                {
                    $html .= "<option value='".$feature->id."'>".$feature->feature."</option>";
                }
                $html .= "</select>";
                return response()->json(['status' => 200, 'featureset' => $html]);
            }
            return response()->json(['result' => $assignFile->agency, 'status' => 200]);
        }

        return response()->json(['status' => 401]);
    }

    public function getPrincipalOfficers()
    {
        $Agency = agency::all();
        return view('admin.agency.pricipal-officers',compact('Agency'));
    }

    public function create()
    {
        $rights = Role::getrights('agency');
        
        if(!$rights->can_create){
    	    abort(403);
        }
        return view('admin.agency.create');
    }

    public function show()
    {

    }

    public function storePrincipalOfficers(Request $request)
    {
        $po = $request->input();

        if (Input::hasfile('cnic')) {
            $image       = Input::file('cnic');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            Storage::putFileAs('public', $image,$filename,'private');
            $po['cnic'] = $filename;
        }

        $po['created_by'] = Auth::user()->id;

        principalOfficers::create($po);
        $message = "New Principal Officer added";
        return redirect('admin/principal-officer')->with(['message' => $message]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'unique:agency',
        ])->validate();

        $agency = $request->input();

        if (Input::hasfile('cnic_copy')) {
            $image       = Input::file('cnic_copy');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('storage\app\agency', $image,$filename,'private');
            $thumbImg = $image->storeAs('agency',$filename);
            $agency['cnic_copy'] = $filename;
        }

        $agency['created_by'] = Auth::user()->id;

        agency::create($agency);

    	Session::flash('status', 'success');
        Session::flash('message', 'Agency has been added successfully.');

    	return redirect('admin/agency');
    }

    public function destroy($id)
    {
        $rights = Role::getrights('agency');
        
        if(!$rights->can_delete){
    	    abort(403);
        }
    	$Agency = agency::find($id);
        $Agency->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Agency has been deleted successfully.');

    	return redirect('admin/agency');
    }

    public function edit($id)
    {
        $rights = Role::getrights('agency');
        
        if(!$rights->can_edit){
    	    abort(403);
        }
        $agency = agency::find($id);
        return view('admin.agency.edit',compact('agency'));
    }

    public function update($id, Request $request)
    {
        $request = $request->input();
        $Agency = agency::find($id);

        if (Input::hasfile('cnic_copy')) {
            $image       = Input::file('cnic_copy');
            $filename    = Auth::user()->id .'-'. uniqid() . '-' . $image->getClientOriginalName();
            //Storage::putFileAs('public', $image,$filename,'private');

            if($Agency->cnic_copy)
                Storage::delete('agency/' . $Agency->cnic_copy);
            $thumbImg = $image->storeAs('agency', $filename);
            $Agency['cnic_copy'] = $filename;
        }

        $Agency->update($request);

        Session::flash('status', 'success');
        Session::flash('message', 'Agency has been updated successfully.');

        return redirect('admin/agency');
    }

    public function viewagents($id)
    {
        $agency=agency::find($id);
        $principal=principal_officer::where('agency_id' , $id)->get();
        $agents=agents::where('agency_id' , $id)->get();

        return view('admin.agency.viewagent' , compact('agency' , 'principal' , 'agents'));
    }
}
