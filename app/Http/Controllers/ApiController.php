<?php

namespace App\Http\Controllers;

use App\Installment;
use App\Mail\SendWelcomeEmail;
use App\Payments;
use App\Post;
use App\Traits\SMS;
use App\User;
use App\WpUser;
use App\agency;
use App\agents;
use App\assignFiles;
use App\batch;
use App\featureset;
use App\lead;
use App\memberInstallments;
use App\members;
use App\otherPayments;
use App\paymentSchedule;
use App\plotSize;
use App\principal_officer;
use App\property_booking;
use Illuminate\Auth\Passwords\sendResetLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Ixudra\Curl\Facades\Curl;
use JWTAuth;

use App\receipts;

class ApiController extends Controller
{
    use SMS;

    public function index()
    {
        return response()->json(['result' => true], 200, [], JSON_NUMERIC_CHECK);
    }

    public function register(Request $request)
    {
        if ($request->input('from_mobile') == true) {
            return response()->json(['status' => 'MEMBER_ADDED'], 200, [], JSON_NUMERIC_CHECK);
        }
    }

    public function LeadUsers()
    {
        $users = User::all();
        return response()->json(['users' => $users]);
    }

    public function allAgents()
    {
        $agents = User::Agents();
        return response()->json(['agents' => $agents]);
    }

    public function propertyList()
    {
        $posts = Post::type('property')->get();
        foreach ($posts as $post) {
            $postArr['post'][] = $post;
            $postArr['user'][] = WpUser::find($post->author_id);
        }
        return response()->json(['posts' => $postArr]);
    }

    public function principalOfficer(Request $request)
    {
        $user = JWTAuth::toUser($request->input('token'));

        $agency = agency::where('person_id', $user->id)->pluck('id');

        $principal = principal_officer::where('agency_id', $agency)->pluck('person_id');

        $persons = [];
        foreach ($principal as $p) {

            $user = User::where('status', 1)->where('id', $p)->find($p);

            if ($user) {
                $persons[] = $user;
            }

        }

        return response()->json(['users' => $persons]);
    }

    public function checkCnic(Request $request)
    {
        $user = User::where('cnic', $request->input('cnic'))->count();
        if ($user > 0) {
            return response()->json(['status' => 0, 'msg' => 'CNIC already exist']);
        }
        return response()->json(['status' => 1, 'msg' => 'CNIC is unique']);
    }

    public function checkUsername(Request $request)
    {
        $user = User::where('username', $request->input('username'))->count();
        if ($user > 0) {
            return response()->json(['status' => 0, 'msg' => 'Username already exist']);
        }
        return response()->json(['status' => 1, 'msg' => 'Username is unique']);
    }

    public function forgetPassword(Request $request)
    {
        $user = User::where('username', $request->input('username'))->first();
        if ($user) {
            Password::sendResetLink(['username' => $request->input('username')]);
            return response()->json(['status' => 1, 'msg' => 'Please check your email to reset your password.']);
        }
        return response()->json(['status' => 0, 'msg' => 'Username is invalid']);
    }

    public function login(Request $request)
    {
        $input = $request->all();

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json(['message' => 'email or password is wrong.', 'status' => 'INVALID_CREDENTIALS'], 200, [], JSON_NUMERIC_CHECK);
        }

        $user = JWTAuth::toUser($token);

        if ($user->email_verified != 1) {
            return response()->json(['message' => 'Please confirm your email', 'status' => 'EMAIL_CONFIRMATION_PENDING'], 200, [], JSON_NUMERIC_CHECK);
        }

        if ($user->is_approved != 1) {
            return response()->json(['message' => 'Admin has not approved your account', 'status' => 'ADMIN_APPROVAL_PENDING'], 200, [], JSON_NUMERIC_CHECK);
        }

        if ($user->status != 1) {
            return response()->json(['message' => 'Your account is deleted.', 'status' => 'ACCOUNT_DELETED'], 200, [], JSON_NUMERIC_CHECK);
        }

        $memberInfo = members::where('person_id', $user->id)->where('is_current_owner', 1)->get();
        foreach ($memberInfo as $key => $member) {
            // $booking =
        }

        return response()->json(['token' => $token, 'user' => $user, 'role' => $user->Roles], 200, [], JSON_NUMERIC_CHECK);
    }

    public function UserStats(Request $request)
    {
        $user = JWTAuth::toUser($request->input('token'));

        $stats['members']       = User::Members()->count();
        $stats['agents']        = User::Agents()->count();
        $stats['paymentPlans']  = paymentSchedule::all()->count();
        $stats['bookings']      = Payments::where('status', 1)->count();
        $stats['graphBookings'] = Payments::where('status', 1)->get();

        return response()->json(['stats' => $stats, 'user' => $user], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getAgencyFiles(Request $request)
    {
        $principal = principal_officer::where('person_id', $request->input('person_id'))->first();
        $agencies  = $principal->agency;

        $files = assignFiles::where('agency_id', $principal->agency->id)->get();

        $plotSize = [];
        $batch    = [];

        foreach ($files as $key => $file) {
            $plotSize[] = plotSize::find($file->plot_size_id);
            $batch[]    = batch::find($file->batch_id);
        }

        return response()->json(['stats' => 200, 'agencies' => $principal, 'files' => $files, 'batch' => $batch, 'plotSize' => $plotSize], 200, [], JSON_NUMERIC_CHECK);

    }

    public function ledgerDownload(Request $request)
    {
        $booking      = Payments::find($request->input('booking_id'));
        $member       = members::where('registration_no', $booking->registration_no)->where('is_current_owner', 1)->first();
        $person       = User::find($member->person_id);
        $installments = memberInstallments::where('registration_no', $booking->registration_no)->get();

        $data['person']       = $person;
        $data['booking']      = $booking;
        $data['member']       = $member;
        $data['installments'] = $installments;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.payments.ledger-pdf', $data);
        $pdf->save('public/pdf/' . $booking->registration_no . '.pdf');

        return response()->json(['path' => URL::to('public/pdf/' . $booking->registration_no . '.pdf')], 200, [], JSON_NUMERIC_CHECK);
    }

    public function get_user_details(Request $request)
    {
        $input = $request->all();

        $user = JWTAuth::toUser($input['token']);

        return response()->json(['user' => $user], 200, [], JSON_NUMERIC_CHECK);
    }

    public function uploadUserPicture(Request $request)
    {
        $user = User::find($request->input('user_id'));

        if (Input::hasfile('picture')) {
            $image       = Input::file('picture');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $user->picture = $filename;
        }

        $user->save();

        return response()->json(['picture' => Input::hasfile('picture')], 200, [], JSON_NUMERIC_CHECK);
    }

    public function uploadCnicPicture(Request $request)
    {
        $user = User::find($request->input('user_id'));

        if (Input::hasfile('cnicPic')) {
            $image       = Input::file('cnicPic');
            $upload_path = public_path() . '/images';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $user->cnic_pic = $filename;
        }

        $user->save();

        return response()->json(['cnicPic' => Input::hasfile('cnicPic')], 200, [], JSON_NUMERIC_CHECK);
    }

    public function uploadAffidavitForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'affidavit_form' => 'required|mimes:jpg,jpeg,png,pdf,docx',
        ]);

        if ($validator->fails()) {
            return response()->json(['validate' => $validator->errors()]);
        }

        // $a = $this->validate($request, [
        //     'affidavit_form' => 'required|mimes:docx',
        // ]);

        // return response()->json(['validate' => $a],200, [], JSON_NUMERIC_CHECK);

        $user = User::find($request->input('user_id'));

        if (Input::hasfile('affidavit_form')) {
            $image       = Input::file('affidavit_form');
            $upload_path = public_path() . '/affidavit-form';
            $filename    = time() . uniqid() . '-' . $image->getClientOriginalName();
            $image->move($upload_path, $filename);
            $user->affidavit_address = $filename;
        }

        $user->save();

        return response()->json(['affidavit_form' => Input::hasfile('affidavit_form')], 200, [], JSON_NUMERIC_CHECK);
    }

    public function addNewMember(Request $request)
    {
        //person creation
        $user = new User();

        $password = str_random();

        $user->username          = $request->input('username');
        $user->name              = $request->input('name');
        $user->guardian_type     = $request->input('guardian_type');
        $user->gurdian_name      = $request->input('guardian_name');
        $user->nationality       = $request->input('nationality');
        $user->cnic              = $request->input('cnic');
        $user->email             = $request->input('email');
        $user->phone_mobile      = $request->input('phone_mobile');
        $user->current_address   = $request->input('current_address');
        $user->permanent_address = $request->input('permanent_address');
        $user->country_id        = $request->input('country_id');
        $user->city_id           = $request->input('city_id');
        $user->phone_office      = $request->input('phone_office');
        $user->phone_res         = $request->input('phone_rec');
        $user->passport          = $request->input('passport');
        $user->dateofbirth       = $request->input('dateofbirth');
        $user->ref_pk            = $request->input('ref_pk');
        $user->email_verified    = 1;
        $user->is_approved       = 1;
        $user->password          = $password;
        $user->remember_token    = str_random(60);
        // $user->person_type       = 1;

        $user->save();

        //member creation
        $member = new members();

        $member->nominee_name = $request->input('nominee_name');

        $member->nominee_guardian = $request->input('nominee_guardian');

        $member->nominee_cnic         = $request->input('nominee_cnic');
        $member->relation_with_member = $request->input('relation_with_member');

        $member->person_id = $user->id;

        $member->save();

        $user->Roles()->attach(1);

        $paymentSchedule = paymentSchedule::all();
        $featureset      = featureset::all();
        $agencies        = agency::all();

        return response()->json([
            'member'          => $member,
            'paymentSchedule' => $paymentSchedule,
            'featureset'      => $featureset,
            'agencies'        => $agencies,
            'status'          => 'MEMBER_ADDED',
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    public function AddBooking(Request $request)
    {
        $registration_no = $request->input('registration_no');
        $file            = assignFiles::where('registration_no', $registration_no)->first();

        if ($file == null) {

        }

        $payment = Payments::create($request->input());
        $payment->save();

        $file->is_reserved = 1;
        $file->save();

        //Get other payments and add in member installments
        $otherPayments = otherPayments::where('payment_schedule_id', $payment->payment_schedule_id)->where('payment_type_id', 4)->get();

        foreach ($otherPayments as $otherPayment) {
            $data1 = [
                'installment_no'      => 1,
                'installment_amount'  => $otherPayment->amount_to_receive,
                'due_amount'          => $otherPayment->amount_to_receive,
                'amount_received'     => 0,
                'registration_no'     => $registration_no,
                'installment_date'    => date('Y-m-d'),
                'payment_schedule_id' => $request->input('payment_schedule_id'),
                'payment_desc'        => $otherPayment->paymentType->payment_type,
                'is_other_payment'    => 1,
            ];
            memberInstallments::create($data1);
        }

        foreach ($request->input('installment_no') as $key => $value) {
            $data = [
                'installment_no'      => $value,
                'installment_date'    => date('Y-m-d', strtotime($request->input('installment_date')[$key])),
                'amount_percentage'   => $request->input('percentage')[$key],
                'installment_amount'  => $request->input('installment_amount')[$key],
                'due_amount'          => $request->input('installment_amount')[$key],
                'rebat_amount'        => $request->input('rebat_amount')[$key],
                'amount_received'     => $request->input('amount_received')[$key],
                'remarks'             => $request->input('installment_remarks')[$key],
                'registration_no'     => $registration_no,
                'payment_schedule_id' => $request->input('payment_schedule_id'),
                'payment_desc'        => 'Installment',
            ];
            memberInstallments::create($data);
        }

        $inst = memberInstallments::where('registration_no', $registration_no)->orderBy('id', 'desc')->first();

        $time           = strtotime($inst->installment_date);
        $balloting_date = date("Y-m-d", strtotime("+1 month", $time));

        //Get other payments and add in member installments
        $otherPayments = otherPayments::where('payment_schedule_id', $payment->payment_schedule_id)->where('payment_type_id', 1)->get();

        foreach ($otherPayments as $otherPayment) {
            $data1 = [
                'installment_no'      => 37,
                'installment_amount'  => $otherPayment->amount_to_receive,
                'due_amount'          => $otherPayment->amount_to_receive,
                'amount_received'     => 0,
                'registration_no'     => $registration_no,
                'installment_date'    => $balloting_date,
                'payment_schedule_id' => $request->input('payment_schedule_id'),
                'payment_desc'        => $otherPayment->paymentType->payment_type,
                'is_other_payment'    => 1,
            ];
            memberInstallments::create($data1);
        }

        $person = User::where('cnic', $request->input('cnic'))->first();

        if (!is_null($person->member->registration_no)) {
            //Create a new member
            $new_member                   = new members();
            $new_member->person_id        = $person->id;
            $new_member->registration_no  = $registration_no;
            $new_member->reference_no     = $request->input('reference_no');
            $new_member->is_current_owner = 1;
            $new_member->save();
        } else {
            //Allot registration number to member
            $person->member->registration_no  = $registration_no;
            $person->member->reference_no     = $request->input('reference_no');
            $person->member->is_current_owner = 1;
            $person->member->save();
        }

        $payment->featureset()->attach($request->input('feature_set_id'));

        $message = "Success! New Booking Added";

        $smsBody    = "Congratulation! You have booked a plot in CapitalCity. Your registration number is: " . $registration_no;
        $event      = "Member Registration";
        $created_by = $request->input('created_by');

        $phone = substr($person->phone_mobile, 0, 1) == 0 ? '92' . substr($person->phone_mobile, 1) : $person->phone_mobile;

        // if ($this->SendSMS($smsBody, $phone)) {
        //     SMSModel::create([
        //         'message'    => $smsBody,
        //         'event'      => $event,
        //         'created_by' => $created_by,
        //         ]);
        // }

        return response()->json([
            'status' => 1,
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getInstallmentPlan(Request $request)
    {
        $installments = Installment::where('payment_schedule_id', $request->input('payment_schedule_id'))->get();
        return response()->json($installments, 200, [], JSON_NUMERIC_CHECK);
    }

    public function getMemberinfo(Request $request)
    {
        if ($request->input('registration_no') != "") {

            $booking = Payments::where('registration_no', $request->input('registration_no'))->last();
            $member  = members::where('registration_no', $request->input('registration_no'))->where('is_current_owner', 1)->first();

            $installment_amount = 0;
            foreach ($booking->paymentPlan->installment as $installment) {
                $installment_amount += $installment->installment_amount;
            }

            $total_installment_paid = 0;
            foreach ($booking->paymentPlan->installment as $installment) {
                if ($installment->is_paid == 1) {
                    $total_installment_paid += $installment->installment_amount;
                }

            }

            $installment_amount_plan = $installment_amount - $total_installment_paid;

            $data = ['paymentPlan'    => $booking->paymentPlan,
                'user'                    => $member->user,
                'member'                  => $member,
                'plotSize'                => $booking->paymentPlan->plotSize,
                'installment_amount'      => $installment_amount,
                'total_installment_paid'  => $total_installment_paid,
                'installment_amount_plan' => $installment_amount_plan,
                'installments'            => memberInstallments::where('registration_no', $booking->registration_no)->get(),
                'booking'                 => $booking,
            ];

            return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);

        }

        if ($request->input('cnic') != "") {
            $user = User::where('cnic', $request->input('cnic'))->first();
            if ($user === null) {
                return json_encode(['error' => 1]);
            }

            $data = ['person' => $user,
                'member'          => $user->member,
                'agencies'        => agency::all(),
                'featureset'      => featureset::all(),
                'payment_plan'    => paymentSchedule::all(),
            ];
            return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);
        }

    }

    public function getAgencies()
    {
        return response()->json(['agencies' => agency::all()], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getAgency(Request $request)
    {
        $assignFile = assignFiles::where('registration_no', $request->input('registration_no'))->where('is_reserved', 0)->first();
        if ($assignFile) {
            return response()->json(['result' => $assignFile->agency, 'status' => 200], 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 401, 'msg' => 'sorry! this file either booked or invalid'], 200, [], JSON_NUMERIC_CHECK);
    }

    public function addAgent(Request $request)
    {
        $check = User::where('username', $request->input('username'))->first();
        if($check)
            return response()->json(['satus' => 201 , 'message' => 'This username already exist']);
        

        $person   = new User();
        $password = 123456;

        $person->name              = $request->input('name');
        $person->username          = $request->input('username');
        $person->guardian_type     = $request->input('guardian_type');
        $person->gurdian_name      = $request->input('gurdian_name');
        $person->nationality       = $request->input('nationality');
        $person->cnic              = $request->input('cnic');
        $person->email             = $request->input('email');
        $person->phone_mobile      = $request->input('phone_mobile');
        $person->current_address   = $request->input('address1');
        $person->permanent_address = $request->input('address2');
        $person->country_id        = $request->input('country_id');
        $person->city_id           = $request->input('city_id');
        $person->phone_office      = $request->input('phone_office');
        $person->phone_res         = $request->input('phone_rec');
        $person->passport          = $request->input('passport');
        $person->dateofbirth       = $request->input('dob');
        $person->ref_pk            = $request->input('ref_pk');
        $person->is_approved       = 1;
        $person->password          = $password;

        // return response()->json($request->input());
        $person->save();

        $persons = $person->id;

        $user = JWTAuth::toUser($request->input('token'));

        $principal_officer = principal_officer::where('person_id', $user->id)->first();

        $newagent = agents::create([
            'person_id'  => $persons,
            'commission' => $request->input('commission'),
            'agency_id'  => $principal_officer->agency->id,
        ]);

        $person->Roles()->attach(3);
        $getNonce = Curl::to(URL::to('/') . '/wordpress/api/get_nonce/?controller=user&method=register')->asJson(true)->get();

        $response = Curl::to(URL::to('/') . '/wordpress/api/user/register/')
            ->withData(
                array(
                    'username'     => $person->username,
                    'email'        => $person->email,
                    'password'     => md5(123456),
                    'display_name' => $person->name,
                    'nonce'        => $getNonce['nonce'],
                    'insecure'     => 'cool',
                    'notify'       => 'both',

                )
            )->get();

        Mail::to($person)->send(new SendWelcomeEmail($person, $password));

        // Mail::to($person)->send(new SendWelcomeEmail($person, $password));

        return response()->json([
            'user'    => $person->id,
            'message' => 'Agent Add Successfully',
        ], 200, [], JSON_NUMERIC_CHECK);

        return response()->json(['status' => 1, 'message' => "Agent Added Successfully"]);
    }

    public function findProperty(Request $request)
    {

        $post = Post::type('property')->where('ID', $request->input('ID'))->first();
        $data = [
            'post'  => $post,
            'price' => $post->meta->fave_property_price,
            'error' => 0,
        ];

        return $data;
    }

    public function propertyBooking(Request $request)
    {
        $post = Post::find($request->input('property_id'));
        //dd ($post);

        if ($post->post_status == 'pending') {

            return redirect()->back()->with('message', 'This property is not approve by admin.');

        } else {

            // $user =User::find($request->input('id'));
            $user = JWTAuth::toUser($request->input('token'));
            // $input = $request->input();

            // $input['dateofbirth'] = date('Y-m-d', strtotime(strtr($request->input('dob'), '/', '-')));

            // $user->update($input);

            $booking                 = new property_booking();
            $booking->person_id      = $request->input('user_id');
            $booking->property_id    = $request->input('property_id');
            $booking->paid_price     = $request->input('paid_amount');
            $booking->property_price = $request->input('price');
            $booking->created_by     = $user->id;
            $booking->created_at     = date('Y-m-d');
            $booking->save();

            $member                   = new members();
            $member->registration_no  = $booking->property_id . '-' . $user->id;
            $member->person_id        = $user->id;
            $member->is_current_owner = 1;
            $member->save();

            return response()->json(['status' => 1, 'message' => "Booking Ok"]);
        }
    }

    public function bookingList(Request $request)
    {
        $user = JWTAuth::toUser($request->input('token'));

        $booking6 = [];
        $booking7 = [];
        $booking  = [];
        // return response()->json([$request->input(), $user]);
        if($user->hasRole('agency'))
        {
            $agency    = agency::where('person_id', Auth::user()->id)->first();
            $principal = principal_officer::where('agency_id', $agency->id)->get()->toArray();
            $agent1    = agents::where('agency_id', $agency->id)->get()->toArray();

            $obj     = array_merge($principal, $agent1);

            foreach ($obj as $l) {
                $booking2 = property_booking::where('created_by', $l['person_id'])->first();
                if ($booking2) {
                    $booking['person'][]  = User::find($booking2['person_id']);
                    $booking['property'][]  = Post::find($booking2['property_id']);
                    $booking['price'][]  = Post::find($booking2['property_id'])->meta->fave_property_price;
                    $booking['agent'][]  = User::where('created_by', $booking2['created_by'])->first();
                }
                // foreach ($booking2 as $key => $booking6) {
                //     if (!in_array($booking6->id, $booking7)) {
                        // $booking7[] = $booking2->id;
                        // dd($booking2['property_id']);
                    // }
                // }

            }
        }

        if ($user->hasRole('Agent')) {
            $booking = property_booking::where('created_by', $user->id)->get();
        }

        if ($user->hasRole('principal-officer')) {

            $principal = principal_officer::where('person_id', $user->id)->first();

            $agent = agents::where('principal_id', $principal->person_id)->get();

            foreach ($agent as $l) {
                $booking2 = property_booking::whereIn('created_by', [$l->person_id, $principal->person_id])->get();
                foreach ($booking2 as $key => $booking6) {
                    if (!in_array($booking6->id, $booking7)) {
                        $booking7[] = $booking6->id;
                        $booking['person'][]  = User::find($booking6->person_id);
                        $booking['property'][]  = Post::find($booking6->property_id);
                        $booking['price'][]  = Post::find($booking6->property_id)->meta->fave_property_price;
                        $booking['agent'][]  = User::where('created_by', $booking6->created_by)->first();
                    }
                }
            }
        }
        //person_id
        //property_id
        

        return response()->json(['bookings', $booking]);
    }

    public function generateLead(Request $request)
    {        
        //person creation
        $loggedInUser = JWTAuth::toUser($request->input('token'));

        $user        = new User();
        $user->name  = $request->input('name');
        $user->cnic  = $request->input('cnic');
        $user->email = $request->input('email');

        $user->phone_mobile = $request->input('phone_mobile');

        $user->permanent_address = $request->input('permanent_address');
        $user->country_id        = $request->input('country_id');
        $user->city_id           = $request->input('city_id');
        $user->phone_office      = $request->input('phone_office');

        $user->email_verified = 1;
        $user->is_approved    = 0;

        $user->remember_token = str_random(60);
        $user->created_by     = $loggedInUser->id;

        $user->save();
        $user->Roles()->attach(11);

        $lead = new lead();

        if ($loggedInUser->hasRole('Agent')) {
            $agent             = agents::where('person_id', $loggedInUser->id)->first();
            $lead->person_id   = $user->id;
            $lead->agency_id   = $agent->agency_id;
            $lead->created_by  = $loggedInUser->id;
            $lead->description = $request->input('description');
            $lead->comments    = $request->input('comments');
            $lead->remarks     = $request->input('remarks');
            $lead->reference   = $request->input('reference');
            $lead->save();
        }

        if ($loggedInUser->hasRole('principal-officer')) {

            $agent = principal_officer::where('person_id', $loggedInUser->id)->first();

            $lead->person_id   = $user->id;
            $lead->agency_id   = $agent->agency_id;
            $lead->created_by  = $loggedInUser->id;
            $lead->description = $request->input('description');
            $lead->comments    = $request->input('comments');
            $lead->remarks     = $request->input('remarks');
            $lead->reference   = $request->input('reference');
            $lead->save();

        }

        return response()->json(['New Lead Generated']);
    }

    public function addPrincipalOfficer(Request $request)
    {
        $check = User::where('username', $request->input('username'))->first();
        if($check)
            return response()->json(['satus' => 201 , 'message' => 'This username already exist']);

        $person = new User();

        $password = 123456;

        $person->name              = $request->input('name');
        $person->username           = $request->input('username');
        $person->guardian_type     = $request->input('guardian_type');
        $person->gurdian_name      = $request->input('gurdian_name');
        $person->nationality       = $request->input('nationality');
        $person->cnic              = $request->input('cnic');
        $person->email             = $request->input('email');
        $person->phone_mobile      = $request->input('phone_mobile');
        $person->current_address   = $request->input('address1');
        $person->permanent_address = $request->input('address2');
        $person->country_id        = $request->input('country_id');
        $person->city_id           = $request->input('city_id');
        $person->phone_office      = $request->input('phone_office');
        $person->phone_res         = $request->input('phone_rec');
        $person->passport          = $request->input('passport');
        $person->dateofbirth       = $request->input('dob');
        $person->ref_pk            = $request->input('ref_pk');
        $person->is_approved       = 1;
        $person->password          = $password;
        $person->email_verified       = 1;
        $person->is_approved    = 0;
        

        $person->save();

        $persons = $person->id;

        $person->Roles()->attach(9);

         $newagent= principal_officer::create([
            'person_id' => $persons,
            'agency_id' => $request->input('agency_id')
        ]);


        

        //http://localhost/laravel/dealers/wordpress/api/user/register/?username=john&email=john@domain.com&nonce=7c500fea07&display_name=John&notify=both&insecure=cool
        // Send a GET request to: http://www.foo.com/bar?foz=baz

        $getNonce = Curl::to(URL::to('/') . '/wordpress/api/get_nonce/?controller=user&method=register')->asJson(true)->get();

        $response = Curl::to(URL::to('/') . '/wordpress/api/user/register/')
            ->withData(
                array(
                    'username'     => $person->username,
                    'email'        => $person->email,
                    'password'     => md5(123456),
                    'user_status'  => 1,
                    'display_name' => $person->name,
                    'nonce'        => $getNonce['nonce'],
                    'insecure'     => 'cool',
                    'notify'       => 'both',
                )
            )->asJson(true)->get();

        if($response['status'] == 'error')
        {
            return response()->json(['error' => $response['error']]);
        }  

        Mail::to($person)->send(new SendWelcomeEmail($person, $password));

        return response()->json(['status' => 200, 'message' => 'Principal Officer Added', 'user_id' => $person->id]);
    }


    public function viewGenerateLead()
    {

        if (Auth::user()->hasRole('Agent')) {
            $user = User::where('created_by', Auth::user()->id)->get();
        }
        if (Auth::user()->hasRole('principal-officer')) {

            $principal = principal_officer::where('person_id', Auth::user()->id)->first();

            $agent = agents::where('principal_id', $principal->person_id)->get();

            $lead1 = [];
            $lead2 = [];
            $lead3 = [];
            foreach ($agent as $a) {
                $user1 = User::whereIn('created_by', [$a->person_id, $principal->person_id])->get();
                foreach ($user1 as $key => $lead1) {
                    if (!in_array($lead1->id, $lead2)) {
                        $lead2[] = $lead1->id;
                        $lead3[] = $lead1;
                    }
                }
            }

            $user3 = [];
            $user4 = [];
            $user5 = [];
            foreach ($lead3 as $l) {
                $user2 = lead::where('person_id', $l->id)->get();
                foreach ($user2 as $key => $user3) {
                    if (!in_array($user3->id, $user4)) {
                        $user4[] = $user3->id;
                        $user5[] = $user3;
                    }
                }
            }

            $user6 = [];
            $user7 = [];
            $user  = [];
            foreach ($user5 as $l) {
                $user2 = User::where('id', $l->person_id)->get();
                foreach ($user2 as $key => $user6) {
                    if (!in_array($user6->id, $user7)) {
                        $user7[] = $user6->id;
                        $user[]  = $user6;
                    }
                }
            }

            //dd($user);

        }
        return response()->json(['user' => $user]);

    }

    public function viewGenerateLeadAgency()
    {
        $agency = agency::where('person_id', Auth::user()->id)->first();

        $lead = lead::where('agency_id', $agency->id)->get();
        //dd($lead);

        $user = [];

        foreach ($lead as $u) {

            $user1 = User::where('id', $u->person_id)->find($u);
            if ($user1) {
                $user[] = $user1;
            }

        }
        //dd($user);

        return response()->json(['user' => $user]);

    }

    public function memberList(Request $request)
    {
        
    }
    
    public function getBatch(Request $request)
    {
        $batches = batch::where('agency_id', $request->input('agency_id'))->get();
        $options = "<option value=''>Select Batch</option>";

        $id = $request->id ? $request->id : 0;

        foreach ($batches as $key => $batch) {
            if($id == 1)
                $options .= "<option value='".$batch->id."'>".$batch->batch_no."</option>";
            else
                $options .= "<option value='".$batch->batch_no."'>".$batch->batch_no."</option>";
        }

        return response()->json(['batch' => $batches]);
    }
    
 
   

}
