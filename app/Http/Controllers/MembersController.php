<?php

namespace App\Http\Controllers;

use App\Employees;
use App\Http\Requests\MemberValidation;
use App\InstallmentReceipts;
use App\LPC;
use App\Payments;
use App\SMSModel;
use App\User;
use App\alerts;
use App\assignFiles;
use App\memberInstallments;
use App\members;
use App\paymentSchedule;
use App\pdc;
use App\sms_detail;
use App\transfers;
use App\Role;
use Collective\Html\input;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MembersController extends Controller
{
    public function index()
    {
        $rights = Role::getrights('members');
        if(!Session::get('objects'))
            Session::put('objects', Role::getMenuItems());

        if($rights->can_view)
        {
            $members = members::all();
            return view('admin.member-registration.index', compact('members','rights'));
        }
    }

    public function create()
    {
        // $reg_no = 'ARG-00-'.str_pad($member->id, 4, 0, STR_PAD_LEFT);\
        $payment_schedule = paymentSchedule::all();
        return view('admin.member-registration.create',compact('payment_schedule'));
    }

    public function add($person_id)
    {
        $payment_schedule = paymentSchedule::all();
        $person = User::find($person_id)->first();
        return view('admin.member-registration.create', compact('payment_schedule','person'));
    }

    public function store(Request $request)
    {
        members::create($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Member has been added successfully.');

        return redirect('admin/users');
    }

    public function destroy($id)
    {
        $members = members::find($id);
        $members->delete();

        Session::flash('status', 'success');
        Session::flash('message', 'Member has been deleted successfully.');

        return redirect('admin/member-registration');
    }

    public function edit($id)
    {
        $person = User::find($id);
        $members = members::where('person_id', $id)->first();
        return view('admin.member-registration.edit',compact('person','members'));
    }

    public function edit_info($person_id)
    {
        $person = User::find($person_id);
        return view('admin.member-registration.edit',compact('person'));
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->update($request->input());

        Session::flash('status', 'success');
        Session::flash('message', 'Member has been updated successfully.');

        return redirect('admin/users');
    }

    public function MemberInfo(Request $request)
    {
        if( $request->ajax() )
        {
            if($request->input('registration_no') != "")
            {
                $booking = Payments::where('registration_no',$request->input('registration_no'))->last();
                $member = members::where('registration_no',$request->input('registration_no'))->where('is_current_owner',1)->first();

                $installment_amount = 0;
                foreach ($booking->paymentPlan->installment as $installment) {
                    $installment_amount+=$installment->installment_amount;
                }

                $total_installment_paid = 0;
                foreach ($booking->paymentPlan->installment as $installment) {
                    if($installment->is_paid == 1)
                        $total_installment_paid+=$installment->installment_amount;
                }

                $installment_amount_plan = $installment_amount - $total_installment_paid;

                $data = ['paymentPlan'  => $booking->paymentPlan,
                         'member'  => $member->user,
                         'installment_amount'  => $installment_amount,
                         'total_installment_paid'  => $total_installment_paid,
                         'installment_amount_plan'  => $installment_amount_plan,
                        ];
                return $data;

                // $paymentPlan  = $booking->paymentPlan;
                // $member  = $member->user;
                // $installment_amount  = $installment_amount;
                // $total_installment_paid  = $total_installment_paid;
                // $installment_amount_plan  = $installment_amount_plan;

                // return view('admin.');

            }

            if( $request->input('cnic') != "")
            {
                $user = User::where('cnic',$request->input('cnic'))->first();
                if($user === null)
                    return json_encode(['error' => 1]);
                $data = ['person'  => $user,
                         'member'  => $user->member];
                return $data;
            }
        }
    }

    public  function inquirySearch(Request $request)
    {
        $registration_no = $request->registration_no;

        $html = $this->inquiry($registration_no, 1);
        return response()->json(['html' => $html]);

        return redirect()->route('member.inquiry', $registration_no);
    }


    public function inquiry($registration_no, $isAjaxRedner = 0)
    {
        $rights = Role::getrights('inquiry');
        $member= members::where('registration_no' , $registration_no)->where('is_current_owner', 1)->first();
        $user =  User::where('id' , $member->person_id)->first();

        $sameBookings = members::join('bookings','bookings.registration_no','members.registration_no')->where('person_id', $member->person_id)->where('is_current_owner',1)->whereNull('bookings.deleted_at')->get();

        $os_inst = memberInstallments::where('registration_no', $registration_no)->where('installment_date', '<=', date('Y-m-d'))->where('is_paid','0')->where('is_active_user', 1)->get();

        // $due_amount[$inst->registration_no][] = $amount;

        $lpc = LPC::where('registration_no', $registration_no)->where('is_finalized',1)->get();

        $booking = Payments::where('registration_no', $registration_no)->where('status', 1)->first();

        $received_inst = memberInstallments::where('registration_no', $registration_no)->where('is_paid','1')->where('is_other_payment', 0)->get();
        $received_inst_sum = 0;
        foreach ($received_inst as $key => $inst) {
            $received_inst_sum += $inst->installment_amount;
        }

        $received_other = memberInstallments::where('registration_no', $registration_no)->where('is_paid','1')->where('is_other_payment', 1)->get();
        $received_other_sum = 0;
        foreach ($received_other as $key => $inst) {
            $received_other_sum += $inst->installment_amount;
        }

        // $pdc = pdc::where('registration_no',$registration_no)->where('clear_posted', 0)->get();
        // $pdc_sum = 0;
        // foreach ($pdc as $key => $p) {
        //     $pdc_sum += $p->amount;
        // }
        $pdc_sum = 0;
        $alerts = alerts::where('registration_no', $registration_no)->whereNull('deleted_at')->get();

        $transfers = transfers::where('reg_no',$registration_no)->where('is_ok',1)->get();

        $file = assignFiles::where('registration_no', $registration_no)->first();

        $sms = sms_detail::where('sent_to', $registration_no)->get();
        $smsDetail = [];

        foreach($sms as $msg)
        {
            $smsDetail[] = SMSModel::find($msg->sms_id);
        }

        if($isAjaxRedner == 1)
        {
            return view('admin.users._member-inquiry' , compact('member' , 'user', 'os_inst','lpc','same_reg', 'booking','received_inst_sum', 'received_other_sum', 'pdc_sum','alerts', 'transfers','file','smsDetail','sameBookings','rights'))->render();
        }

        $bookingList = Payments::select('bookings.registration_no')
        ->join('members', 'members.registration_no','=', 'bookings.registration_no')
        ->join('persons', 'members.person_id','=', 'persons.id')
        ->join('payment_schedule', 'payment_schedule.id','=', 'bookings.payment_schedule_id')
        ->join('plot_size', 'plot_size.id','=', 'payment_schedule.plot_size_id')
        ->where('bookings.status',1)
        ->where('bookings.is_cancelled',0)
        ->get();
        // ->toArray();

        // array_prepend($bookingList, "Select Registration No");


        return view('admin.users.member-inquiry' , compact('member' , 'user', 'os_inst','lpc','same_reg', 'booking','received_inst_sum', 'received_other_sum', 'pdc_sum','alerts', 'transfers','file','smsDetail','sameBookings', 'bookingList','rights'));
    }

    public function contactAdmin()
    {
        return view('contact-admin');
    }
}
