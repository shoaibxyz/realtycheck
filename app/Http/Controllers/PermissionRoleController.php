<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\modules;
use App\permissionroles;
use App\permissions;
use App\roles;
use DB;

class PermissionRoleController extends Controller
{
    public function assignPermission($id)
    {
        $modules = modules::orderBy('name','asc')->get();

        $rights_html = '
                        <div class="table-responsive">
                            <table id="rights" class="table table-bordered table-hover jambo_table" width="100%">
                                <thead class="panel-heading">
                                    <th class="panel-heading">Object</th>
                                    <th class="panel-heading">Can Create</th>
                                    <th class="panel-heading">Can View</th>
                                    <th class="panel-heading">Can Edit</th>
                                    <th class="panel-heading">Can Delete</th>
                                    <th class="panel-heading">Can Permanent Delete</th>
                                    <th class="panel-heading">Can Restore</th>
                                    <th class="panel-heading">Can Print</th>
                                </thead>
                                <tbody>
                        ';
                        foreach($modules as $module)
                        {
                            $objects = permissions::where('module_id',$module->id)->orderBy('name','asc')->get();

                            $rights_html .= '<tr><td colspan="8"><h3>'. $module->name .'</h3></td></tr>';

                            $can_create = '';
                            $can_view = '';
                            $can_edit = '';
                            $can_delete = '';
                            $can_deletep = '';
                            $can_restore = '';
                            $can_print = '';

                            foreach($objects as $object)
                            {

                                $perm = permissionroles::where([['role_id',$id],['permission_id',$object->id]])->first();
                                $can_create = ($perm and $perm->can_create == 1) ? "checked=checked" : "";
                                $can_edit = ($perm and $perm->can_edit == 1) ? "checked=checked" : "";
                                $can_delete = ($perm and $perm->can_delete == 1) ? "checked=checked" : "";
                                $can_deletep = ($perm and $perm->can_deletep == 1) ? "checked=checked" : "";
                                $can_restore = ($perm and $perm->can_restore == 1) ? "checked=checked" : "";
                                $can_view = ($perm and $perm->can_view == 1) ? "checked=checked" : "";
                                $can_print = ($perm and $perm->can_print == 1) ? "checked=checked" : "";
                                $rights_html .= '
                                                <tr>
                                                    <td style="text-align:left;"><strong>'.$object->name.'</strong></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbcreate_'.$object->id.'" id="cbcreate_'.$object->id.'" '. $can_create .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbview_'.$object->id.'" id="cbview_'.$object->id.'" '. $can_view .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbedit_'.$object->id.'" id="cbedit_'.$object->id.'" '. $can_edit .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbdel_'.$object->id.'" id="cbdel_'.$object->id.'" '. $can_delete .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbdelp_'.$object->id.'" id="cbdelp_'.$object->id.'" '. $can_deletep .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbrestore_'.$object->id.'" id="cbrestore_'.$object->id.'" '. $can_restore .' /></td>
                                                    <td style="text-align: center"><input type="checkbox" name="cbprint_'.$object->id.'" id="cbprint_'.$object->id.'" '. $can_print .' /></td>
                                                </tr>';
                            }
                        }
        $rights_html .= '</tbody>
                            </table>
                        </div>';

        return view('admin.permission-role.assignRole', compact('rights_html'))->with('role_id',$id);
    }

    public function store(Request $request)
    {
        $roles = roles::find($request->input('role_id'));
        permissionroles::where('role_id',$request->input('role_id'))->delete();

        $objects = permissions::all();

        foreach($objects as $object)
        {
            $canCreate = $request->input('cbcreate_'.$object->id);
            if(!$canCreate)
                $canCreate = 0;
            else
                $canCreate = 1;
            $canView = $request->input('cbview_'.$object->id);
            if(!$canView)
                $canView = 0;
            else
                $canView = 1;
            $canEdit = $request->input('cbedit_'.$object->id);
            if(!$canEdit)
                $canEdit = 0;
            else
                $canEdit = 1;
            $canDel = $request->input('cbdel_'.$object->id);
            if(!$canDel)
                $canDel = 0;
            else
                $canDel = 1;
            $canDelP = $request->input('cbdelp_'.$object->id);
            if(!$canDelP)
                $canDelP = 0;
            else
                $canDelP = 1;
            $canRestore = $request->input('cbrestore_'.$object->id);
            if(!$canRestore)
                $canRestore = 0;
            else
                $canRestore = 1;
            $canPrint = $request->input('cbprint_'.$object->id);
            if(!$canPrint)
                $canPrint = 0;
            else
                $canPrint = 1;

            $pr = new permissionroles();
            $pr->role_id = $request->input('role_id');
            $pr->permission_id = $object->id;
            $pr->can_create = $canCreate;
            $pr->can_view = $canView;
            $pr->can_edit = $canEdit;
            $pr->can_delete = $canDel;
            $pr->can_deletep = $canDelP;
            $pr->can_restore = $canRestore;
            $pr->can_print = $canPrint;
            $pr->save();
        }

        Session::flash('status', 'success');
        Session::flash('message', 'Role has been added successfully.');

        return redirect('admin/roles');
    }
}
