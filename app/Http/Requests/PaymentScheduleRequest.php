<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
           return [
                'payment_code' => 'required|unique:payment_schedule,payment_code,'.$this->input('payment_schedule_id'),
                'total_price' => 'required',
                'booking_price' => 'required',
                'balloting_percentage' => 'required',
                'semi_anual_percentage' => 'required',
                'monthly_percentage' => 'required',
                'plot_nature' => 'required',
                'payment_frequency' => 'required',
                'installments' => 'required|numeric',
            ];
    }
}
