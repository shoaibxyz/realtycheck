<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class uploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules =  [
            // 'files' => 'mimes:jpeg,jpg,png',
            //'registeration_no' => 'required',
            'meeting_date' => 'required',
            'meeting_channel' => 'required',
            'meeting_done_by' => 'required',
            'meeting_detail' => 'required',
            'in_out' => 'required',
            'meeting_status' => 'required'
        ];

        $files = 0;
        foreach ($this->files as $key => $value) {
            $files = count($value);
            break;
        }

        foreach(range(0, $files) as $index) {
            $rules['files.' . $index] = 'mimes:jpg,png,jpeg, bmp, gif,doc,docx,xls,pdf,ppt, csv,txt';
        }

        return $rules;
    }
}
