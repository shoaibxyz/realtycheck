<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class agentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'cnic' => 'required|unique:persons',
            'current_address' => 'required',
            'permanent_address' => 'required',
            'phone_mobile' => 'required',
            'nationality' => 'required',
            'commission' => 'required|numeric',
            'picture' => 'required|mimes:jpeg,jpg,png',
            'agency_id' => 'required',
        ];
    }
}
