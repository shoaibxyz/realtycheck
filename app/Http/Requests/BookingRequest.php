<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    public function response(array $errors){
        return redirect('admin/booking/create')->withErrors($errors);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_no' => 'required|unique:members',
            'reference_no' => 'required|unique:members',
            'payment_schedule_id' => 'required',
            'agency_id' => 'required',
            // 'installment_no' => 'required',
        ];
    }

    public function messages()
    {
        return [
                'installment_no.required' =>'Please choose Installment Plan to save booking',
                'registration_no.unique'=> 'This registration number already exist',
                'reference_no.unique'=> 'This reference number already exist',
        ];
    }
}
