<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            // 'cnic' => 'required|unique:persons',
            'cnic' => 'required|unique:persons',
            'current_address' => 'required',
            'permanent_address' => 'required',
            'phone_mobile' => 'required',
            'nationality' => 'required',
            'picture' => 'required|mimes:jpeg,jpg,png',
            'cnic_pic' => 'required|mimes:jpeg,jpg,png',

        ];
    }
}
