<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class receiptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receipt_no' => 'required|unique:receipts,receipt_no,'.$this->input('receipt_no'),
            'receiving_mode' => 'required',
            'registration_no' => 'required'
        ];
    }
}
