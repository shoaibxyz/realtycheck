<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_number' => 'required',
            'plot_type_id' => 'required|numeric',
            'contract_type_id' => 'required|numeric',
            'agent_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
            'society_id' => 'required|numeric',
            'price' => 'required|numeric',
            'area' => 'required|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'address' => 'required',
            'plot_status' => 'required',
            'block' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute is required.',
        ];
    }
}
