<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReschedulePaymentPlan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_code' => 'required|unique:payment_schedule',
            'reschedule_date' => 'required|date',
            'total_installments' => 'required|numeric',
            'payment_frequency' => 'required'
        ];
    }
}
