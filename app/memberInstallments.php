<?php

namespace App;

use App\memberInstallments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class memberInstallments extends Model
{
    use SoftDeletes;
    public $table = "member_installments";
    protected $fillable = [
    		'installment_date', 
            'rebat_amount', 
    		'remarks', 
    		'amount_received', 
    		'amount_percentage', 
    		'installment_amount', 
    		'payment_schedule_id',
            'installment_no',
    		'due_amount',
            'registration_no',
    		'payment_desc',
            'is_other_payment'
    	];

    protected $dates = ['created_at','installment_date','payment_date'];

    public function pdc_installment()
    {
         return $this->belongsTo('App\pdc_installments','installment_id');
    }

     public function scopePaid($query)
    {
        return $query->where('is_paid', 1);
    }

    public static function getInstallment($id)
    {
        return memberInstallments::find($id);
    }

    public function scopeUnpaid($query)
    {
        return $query->where([['is_paid', 0],['due_amount','>',0]]);
    }

    public function scopeOtherPayment($query, $value = 0)
    {
        return $query->where('is_other_payment', $value);
    }

    public function scopeReg($query, $registration_no)
    {
        return $query->where('registration_no', $registration_no);
    }

    public function scopeActiveUser($query)
    {
        return $query->where('is_active_user',1);
    }

    public static function InstallmentsData($registration_no, $due_inst = 0, $od_inst = 0, $dueCount = 0, $odCount = 0, $is_other_payment = 0) 
    {
        $dueInstallments = memberInstallments::Reg($registration_no)
        ->Unpaid()
        ->ActiveUser()
        ->OtherPayment($is_other_payment)
        ->get();

        $dueInstallment = [];
        $overDueInstallment = [];
        $dueInstallment = [];
        $total_overdue_amount = 0;
        $total_due_amount = 0;
        foreach($dueInstallments as $due)
        {
            //Find the difference between today and installment date
            $installment_date = $due->installment_date;
            $today = Carbon::now();
            $difference = $installment_date->diffInDays($today, false);
            if($difference > 0 )
            {
                $overDueInstallment = $difference;
                $total_overdue_amount += $due->installment_amount;
            }
            elseif($difference <= 10 && $difference > 0){
                $dueInstallment = $difference;
                $total_due_amount += $due->installment_amount;
            }
        }

        if($due_inst == 1)
        {
            return $total_due_amount;
        }

        if($od_inst == 1)
        {
            return $total_overdue_amount;
        }

        if($dueCount == 1)
        {
            return $dueInstallment;
            //return count($dueInstallment);
        }

        if($odCount == 1)
        {
            return $overDueInstallment;
            //return count($overDueInstallment);
        }

        return "";
    }
}
