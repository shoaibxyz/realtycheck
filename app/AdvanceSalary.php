<?php

namespace App;

use App\Models\HR\Employees;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class AdvanceSalary extends Model
{
	use SoftDeletes;
	use LogsActivity;

    public $table = "hr_advance_salary";

    protected $fillable = ['employee_id','amount','deduct_month','deduct_year','is_approved','created_by','updated_by' , 'agency_id'];

	protected static function boot()
    {
        
        parent::boot();
        static::updated(function ($advanceSalary) {

            $changes = $advanceSalary->isDirty() ? $advanceSalary->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at')
                        continue;
                    $status = ($advanceSalary->is_approved == 1) ? "Approved" : "Reject";
                    $oldStatus = ($advanceSalary->getOriginal($attr) == 1) ? "Approved" : "Reject";

                    activity('advance salary')
                        ->performedOn($advanceSalary)
                        ->log("updated advance salary of employee {$advanceSalary->employee->user->name} from {$oldStatus} to {$status} amount {$advanceSalary->amount} for month {$advanceSalary->deduct_month} {$advanceSalary->deduct_year}");
                }
            }
        });

        static::created(function ($advanceSalary) {
            activity('advance salary')
                ->performedOn($advanceSalary)
                ->log("created advance salary of amount {$advanceSalary->amount} to {$advanceSalary->employee->user->name}");
        });

        static::deleted(function ($advanceSalary) {
            activity('advance salary')
                ->performedOn($advanceSalary)
                ->log("deleted advance salary of employee named {$advanceSalary->employee->user->name}");
        });
    }

    public function employee()
    {
    	return $this->belongsTo(Employees::class);
    }
}
