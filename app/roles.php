<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    public $table = "roles";
    protected $fillable = ['name','display_name','description'];

    public function permissions()
    {
    	return $this->belongsToMany('App\permissions','permission_role','role_id','permission_id');
    }
}
