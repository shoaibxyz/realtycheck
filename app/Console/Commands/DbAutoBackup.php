<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DbAutoBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto backup database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        $host = env('DB_HOST');
        $db   = env('DB_DATABASE');
        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');


        $con = mysqli_connect($host, $user,  $pass, $db);

        /* Store All Table name in an Array */
        $allTables = array();
        $result    = mysqli_query($con, 'SHOW TABLES');
        while ($row = mysqli_fetch_row($result)) {
            $allTables[] = $row[0]; 
        }
        
        $return = '';
        foreach ($allTables as $table) {
            $result     = mysqli_query($con, 'SELECT * FROM ' . $table);
            $num_fields = mysqli_num_fields($result);

            $return .= 'DROP TABLE IF EXISTS ' . $table . ';';
            $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE ' . $table));
            $return .= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($result)) {
                    $return .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {$return .= '"' . $row[$j] . '"';} else { $return .= '""';}
                        if ($j < ($num_fields - 1)) {$return .= ',';}
                    }
                    $return .= ");\n";
                }
            }
            $return .= "\n\n";
        }

        // Create Backup Folder
        $folder = 'DB_Backup/';
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

        chmod($folder, 0777);

        $date     = date('d-m-Y-H-i-s', time());
        $filename = $folder . "db-backup-" . $date;

        $handle = fopen($filename . '.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);

       
        $con->query("INSERT INTO database_backup (filename) VALUES ('$filename')") or die("Error in query");

        \Log::info('Auto Database backup');
    }
}
