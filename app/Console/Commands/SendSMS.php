<?php

namespace App\Console\Commands;

use App\SMSModel;
use App\Traits\SMS;
use Illuminate\Console\Command;

class SendSMS extends Command
{
    use SMS;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will send SMS based on the scheduled time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sms = SMSModel::where('sent', 0)->where('sending_date','<=',date("Y-m-d H:i"))->get();
       
        foreach ($sms as $key => $msg) {
            $res = $this->SendSMS($msg->message, $msg->sent_to);
            
              if(strpos($res,'Message Sent Successfully',0) !== false)
                {
                    $msg->sent = 1;
                    $msg->sending_date = date('Y-m-d H:i');
                    $msg->save();
                    \Log::info('Sms sent to '. $msg->sent_to. " Message: ". $msg->message);
                }
           
            
            //\Log::info($res);
        }
    }
}
