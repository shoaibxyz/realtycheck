<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSTemplates extends Model
{
    public $table = "sms_templates";
}
