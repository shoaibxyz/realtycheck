<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member_other_payments extends Model
{
	public $table = "member_other_payments";

    protected $fillable = ['installment_no', 'installment_date', 'installment_amount','registration_no','payment_schedule_id','amount_received','payment_desc'];

    public function setInstallmentDateAttribute($value)
    {
    	return $this->attributes['installment_date'] = date('Y-m-d',strtotime($value));
    }

    
}
