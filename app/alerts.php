<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class alerts extends Model
{
	 use LogsActivity;
    use SoftDeletes;

    protected $fillable  = ['registration_no', 'message','created_by','updated_by'];

    protected $softDelete = true;

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($alert) {

            $changes = $alert->isDirty() ? $alert->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_at')
                        continue;
                    
                    activity('Alert')
                        ->performedOn($alert)
                        ->withProperties(['event' => 'updated', 'registration_no' => $alert->registration_no])
                        ->log("Updated alert '{$alert->message}' field $attr from {$alert->getOriginal($attr)} to {$alert->$attr}");
                }
            }

        });

        static::created(function ($alert) {

            activity('Alert')
                ->performedOn($alert)
                ->withProperties(['event' => 'created', 'registration_no' => $alert->registration_no])
                ->log("Added new alert '{$alert->message}'");
        });

        static::deleted(function ($alert) {

            activity('Alert')
                ->performedOn($alert)
                ->withProperties(['event' => 'deleted', 'registration_no' => $alert->registration_no])
                ->log("Deleted alert '{$alert->message}'");
        });

    }
}
