<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDocuments extends Model
{
    protected $fillable = ['transfer_id','document', 'created_by', 'updated_by']; 


    public function transfer()
    {
    	$this->belongsTo('App\transfers');
    }
}
