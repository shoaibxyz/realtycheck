<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferImages extends Model
{
    public $table = 'transfer_images';

    protected $fillable = ['transfer_id','picture','is_printable','created_by'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function transfer()
    {
    	return $this->belongsTo('App\transfers');
    }
}
