<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FstReceipt extends Model
{
    public $timestamps = false;
    public $table = 'featureset_receipts';
}
