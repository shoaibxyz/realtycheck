<?php

namespace App;

use App\Attendance;
use App\Models\HR\Employees;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public $table = "hr_attendance";

	protected $fillable = ['employee_id', 'clock_in_out', 'type', 'remarks', 'created_by','updated_by','latitude','longitude','formatted_address'];   

	protected $dates = ['clock_in_out'];
	public function employee()
	{
		return $this->belongsTo(Employees::class);
	}

	public static function getEmployeeAttendance($employee_id)
	{
		return Attendance::where('employee_id', $employee_id)->where('clock_in_out', 'LIKE', date('Y-m-d').'%')->orderBy('id', 'DESC')->limit(1)->first();


	}
}
