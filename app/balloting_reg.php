<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class balloting_reg extends Model
{
    
    use SoftDeletes;

    public $table = "ballotable_files";
    protected $fillable= ['registration_no', 'created_by', 'created_at', 'updated_at'];
}
