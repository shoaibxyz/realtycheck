<?php

namespace App;

use App\Payments;
use App\featureset;
use Illuminate\Database\Eloquent\Model;

class booking_featureset extends Model
{
    public $table = 'booking_featureset';

    public function featureset()
    {
    	return $this->belongsTo(App\featureset::class);
    }

    public function booking()
    {
    	return $this->belongsTo(App\Payments::class);
    }
}
