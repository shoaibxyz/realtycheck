<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class units extends Model
{
	public $timestamps = false;
    protected $fillable = ['unit_name'];

    public function property()
    {
    	return $this->hasOne('App\property');
    }
}
