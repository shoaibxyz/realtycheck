<?php

namespace App;

use App\User;
use App\members;
use App\permissionroles;
use App\permissions;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use SoftDeletes;
    protected static $ignoreChangedAttributes = ['updated_at'];

    use Notifiable;
    use LogsActivity;
    // use SoftDeletes;

    protected $table = "persons";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'name', 'email', 'password', 'cnic', 'gurdian_name', 'picture', 'current_address', 'permanent_address', 'nationality', 'passport', 'phone_office', 'phone_res', 'phone_mobile', 'email_verification_code', 'cnic_pic', 'ref_pk', 'username','dateofbirth','guardian_type','city_id','country_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($user) {
            $changes = $user->isDirty() ? $user->getDirty() : false;
            //Check if user is member or not
            $checkMember = User::Active()->Members()->find($user->id);
            $registration_no = null;
            if($checkMember){
                $member = members::where('person_id',$user->id)->first();
                $registration_no = $member->registration_no;
            }

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ( $user->getOriginal($attr) == null ) ? "null" : $user->getOriginal($attr);

                    if($attr == 'updated_at' || $attr == 'remember_token' || $attr == 'status')
                        continue;

                    activity('User')
                        ->performedOn($user)
                        ->withProperties(['registration_no' => $registration_no, 'event' => 'updated'])
                        ->log("Updated user field named $attr from {$getOriginal} to {$user->$attr} having this cnic number {$user->cnic}");
                }
            }
        });

        static::created(function ($user) {
            activity('User')
                    ->performedOn($user)
                    ->withProperties(['event' => 'updated'])
                    ->log("Created new user having this cnic number {$user->cnic}");
        });

        static::deleted(function ($user) {

            $checkMember = User::Active()->Members()->find($user->id);
            $registration_no = null;
            if($checkMember){
                $member = members::where('person_id',$user->id)->first();
                $registration_no = $member->registration_no;
            }

            activity('User')
                    ->performedOn($user)
                    ->withProperties(['registration_no' => $registration_no, 'event' => 'Deleted'])
                    ->log("Deleted user having this cnic number {$user->cnic}");
        });
    }

    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    public function persontype()
    {
        return $this->belongsTo('App\persontype');
    }

    public function Roles()
    {
        return $this->belongsToMany('App\Role','role_person','person_id','role_id');
    }

    public function member()
    {
        return $this->hasOne('App\members','person_id');
    }

    public function employee()
    {
        return $this->hasOne('App\Employees','person_id');
    }

    public function principal()
    {
        return $this->hasOne('App\principal_officer');
    }

    public function agent()
    {
        return $this->hasOne('App\agents','person_id');
    }

    public function scopeMembers($query)
    {
        return $query->where('status',1)->whereHas('roles', function($query){
            $query->where('name', '=', \Config::get('constants.roles.member'));
        })->get();
    }

    public function scopeCCD($query)
    {
        return $query->where('status',1)->whereHas('roles', function($query){
            $query->where('name', '=', \Config::get('constants.roles.call-center'));
        })->get();
    }

    public function scopeAgents($query)
    {
        //dd(\Config::get('constants.roles.agent'));
        return $query->where('status',1)->whereHas('roles', function($query){
            $query->where('name', '=', \Config::get('constants.roles.agent'));
        })->get();
    }

    public function scopeEmployees($query)
    {
        return $query->where('status',1)->whereHas('roles', function($query){
            $query->where('name', '!=', \Config::get('constants.roles.admin'))
                  ->where('name', '!=', \Config::get('constants.roles.Super-Admin'))
                  ->where('name', '!=', \Config::get('constants.roles.member'))
                  ->where('name', '!=', \Config::get('constants.roles.agent'));
        })->get();
    }

    public function scopeApproved($query)
    {
        return $query->where('is_approved',1)->where('email_verified',1);
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }


    public function setDateOfBirthAttribute($value)
    {
        return $this->attributes['dateofbirth'] = date('Y-m-d', strtotime($value));
    }

    public function IsSuperAdmin()
    {
        return Auth::user()->hasRole('Admin');
    }

    public static function getUserInfo($id)
    {
        return User::find($id);
    }

    public static function makeImage($w, $h, $image)
    {
        $img = Image::make($image)->resize($w, $h, function($constrain){
            $constrain->aspectRatio();
        });

        return $img;
    }

    public function isAllowed($permission)
    {

        dd(Auth::user()->roles[0]->name);
    }


    /**
     * Combines SQL and its bindings
     *
     * @param \Eloquent $query
     * @return string
     */
    public static function getQuery($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
