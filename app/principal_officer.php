<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class principal_officer extends Model
{
	use SoftDeletes;
	
    public $timestamps = false;

    protected $table = "agency_principal_officers";
	protected $fillable = ['name','cell' , 'cnic' , 'picture' , 'company_logo','agency_id'];



	public function agency(){
      return $this->belongsTo('App\agency' ,'agency_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User','person_id');
	}
}
