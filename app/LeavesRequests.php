<?php

namespace App;

use App\Models\HR\Employees;
use App\Leaves;
use App\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class LeavesRequests extends Model
{
	use SoftDeletes;
	use LogsActivity;

    public $table = "hr_leaves_requests";

    protected $fillable = ['leave_type_id','leave_start_date','leave_end_date','status','employee_id','reason','attachment','created_by','updated_by'];

    protected $dates = ['created_at','updated_at','leave_start_date','leave_end_date','deleted_at'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($leave) {

            $changes = $leave->isDirty() ? $leave->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at' || $attr == 'updated_by')
                        continue;
                    
                    if($attr == 'leave_type_id'){
                        $type = Helpers::leaveTypes($leave->leave_type_id);
                    }

                    activity('leave')
                        ->performedOn($leave)
                        ->log("updated leave {$attr} from {$leave->$attr} to {$leave->getOriginal($attr)}");
                }
            }
        });

        static::created(function ($leave) {
            activity('leave')
                ->performedOn($leave)
                ->log("created a new leave of employee {$leave->employee->user->name} from {$leave->leave_start_date} to {$leave->leave_end_date}");
        });

        static::deleted(function ($leave) {
            activity('leave')
                ->performedOn($leave)
                ->log("deleted leave of employee {$leave->employee->user->name} from {$leave->leave_start_date} to {$leave->leave_end_date}");
        });
    }

    public function employee()
    {
    	return $this->belongsTo(Employees::class, 'employee_id');
    }

    public function leave()
    {
        return $this->belongsTo(Leaves::class, 'leave_type_id');
    }
    
    public static function getleaves($employee,$month,$year)
     {
         
         
         
         $s_d =cal_days_in_month(CAL_GREGORIAN,$month,$year);
     
       $leaves = LeavesRequests::where('employee_id' , $employee)
       ->where('status',1)
       ->where((DB::raw('MONTH(leave_start_date)')) , $month)
       ->where((DB::raw('MONTH(leave_end_date)')) , $month)
       ->sum('no_days');
        
        $s_days = $s_d - $leaves;
        
        return $s_days;
         
     }
}
