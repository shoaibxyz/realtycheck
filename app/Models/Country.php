<?php

namespace App\Models;

use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public static function getCountry($id)
	{
		return Country::find($id);
	}
}
