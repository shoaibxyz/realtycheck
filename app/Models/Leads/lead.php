<?php

namespace App\Models\Leads;

use App\Models\Leads\Campaign;
use App\Models\Leads\LeadHistory;
use App\Models\Leads\LeadStatus;
use App\Models\Leads\PropertySubtypes;
use App\Models\Leads\PropertyType;
use App\Models\Leads\Source;
use App\User;
use App\Models\Leads\projects;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class lead extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['classification', 'purpose', 'property_type_id', 'property_subtype_id', 'project_id', 'beds', 'land_min_area', 'land_max_area','land_unit', 'budget', 'subject', 'message', 'person_id', 'created_by', 'updated_by', 'description', 'comments', 'remarks', 'reference', 'is_converted', 'lead_date', 'lead_status_id','assigned_to','assigned_by','assigned_date' , 'agency_id','interest','visit_promise_date','customer_feedback_after_visit','whatsapp_sent_on','source_id'];

    // public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($leads) {

            $changes = $leads->isDirty() ? $leads->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ($leads->getOriginal($attr) == null) ? "null" : $leads->getOriginal($attr);

                    if ($attr == 'updated_at') {
                        continue;
                    }

                    activity('Leads')
                        ->performedOn($leads)
                        ->withProperties(['event' => 'updated'])
                        ->log("Updated leads field named $attr from {$getOriginal} to {$leads->$attr}");
                }
            }
        });

        static::created(function ($leads) {
            activity('Leads')
                ->performedOn($leads)
                ->withProperties(['event' => 'created'])
                ->log("Created new lead");
        });

        static::deleted(function ($leads) {
            activity('Leads')
                ->performedOn($leads)
                ->withProperties(['event' => 'Deleted'])
                ->log("Deleted a lead ");
        });
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'person_id');
    }

    public function sources()
    {
        return $this->belongsTo(Source::class);
    }

    public function project()
    {
        return $this->belongsTo(projects::class);
    }

    public function status()
    {
        return $this->belongsTo(LeadStatus::class, 'lead_status_id');
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function PropertySubtypes()
    {
        return $this->belongsTo(PropertySubtypes::class, 'property_type_id');
    }

    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function assignedBy()
    {
        return $this->belongsTo(User::class, 'assigned_by');
    }

    public function history()
    {
        return $this->hasMany(LeadHistory::class);
    }

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class,'lead_campaigns');
    }
    
    public static function countLeads($id)
    {  
        $lead = Lead::where('assigned_to', $id)->whereNull('deleted_at')->count();
       
            if($lead > 0)
                return $lead;
            else
                return '0';
                
            return false;
    }
}
