<?php

namespace App\Models\Leads;

use App\Models\Leads\PropertyType;
use App\Models\Leads\lead;
use Illuminate\Database\Eloquent\Model;

class PropertySubtypes extends Model
{
    public function property()
    {
    	return $this->belongsTo(PropertyType::class);
    }

    public function lead()
    {
    	return $this->hasOne(lead::class);
    }
}
