<?php

namespace App\Models\Leads;

use App\Models\Leads\LeadHistory;
use App\Models\Leads\lead;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class LeadStatus extends Model
{
	use SoftDeletes;
  	use LogsActivity;

    protected $fillable = ['name','created_by','updated_by' , 'agency_id'];

    public $table = 'lead_status';

    public function lead()
    {
    	return $this->hasOne(lead::class);
    }  

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($leads) {

            $changes = $leads->isDirty() ? $leads->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ($leads->getOriginal($attr) == null) ? "null" : $leads->getOriginal($attr);
                    $getAttr     = ($leads->attr == null) ? "null" : $leads->$attr;

                    if ($attr == 'updated_at') {
                        continue;
                    }

                    activity('Leads Status')
                        ->performedOn($leads)
                        ->withProperties(['event' => 'updated'])
                        ->log("Updated leads source field named $attr from {$getOriginal} to {$getAttr}");
                }
            }
        });

        static::created(function ($leads) {
            activity('Leads Status')
                ->performedOn($leads)
                ->withProperties(['event' => 'created'])
                ->log("Created new leads");
        });

        static::deleted(function ($leads) {
            activity('Leads Status')
                ->performedOn($leads)
                ->withProperties(['event' => 'Deleted'])
                ->log("Deleted leads having registration number");
        });
    }  

    public function history()
    {
        return $this->hasMany(LeadHistory::class);
    }
}
