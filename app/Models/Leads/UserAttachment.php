<?php

namespace App\Models\Leads;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserAttachment extends Model
{
    protected $fillable = ['notes','attachment','person_id','created_by','updated_by','title'];

	public function user()
	{
		return $this->belongsTo(User::class,'person_id');
	}   
}