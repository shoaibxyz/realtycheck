<?php

namespace App\Models\Leads;

use App\Models\Leads\Campaign;
use App\Models\Leads\lead;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Source extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['name','created_by','updated_by' , 'agency_id'];

    protected $dates = ['created_at', 'updated_at'];

    
    protected static function boot()
    {
        parent::boot();
        static::updated(function ($leads) {

            $changes = $leads->isDirty() ? $leads->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                  if($attr == 'updated_at')
                        continue;
                      
                    activity('designation')
                        ->performedOn($leads)
                        ->log(":causer.name updated designation $attr from {$leads->getOriginal($attr)} to {$leads->$attr}");
                }
            }

        });

        static::created(function ($leads) {

            activity('designation')
                ->performedOn($leads)
                ->log(":causer.name created designation {$leads->name}");
        });

        static::deleted(function ($leads) {

            activity('designation')
                ->performedOn($leads)
                ->log(":causer.name deleted designation {$leads->name}");
        });
    }
    

    public function leads()
    {
    	return $this->hasOne(lead::class);
    }

    public static function getLeadSources($id)
    {
    	return DB::table('lead_sources')->where('lead_id', $id)->get();
    }

    public static function getSources($id)
    {
    	return Source::find($id);
    }

    public function campaign()
    {
        return $this->hasMany(Campaign::class);
    }
}
