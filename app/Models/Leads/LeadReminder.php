<?php

namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class LeadReminder extends Model
{
	use SoftDeletes;
	use LogsActivity;

	protected $fillable = ['employee_id','lead_id','message','reminder_time','created_by','updated_by','is_completed', 'completed_by', 'completed_date'];

	public $table = "lead_reminder";

	protected static function boot()
    {
         parent::boot();
        static::updated(function ($leadReminder) {

            $changes = $leadReminder->isDirty() ? $leadReminder->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at' || $attr == 'updated_by')
                        continue;

                    activity('leadReminder')
                        ->performedOn($leadReminder)
                        ->log("updated leadReminder {$attr} from {$leadReminder->$attr} to {$leadReminder->getOriginal($attr)}");
                }
            }
        });

        static::created(function ($leadReminder) {

            activity('leadReminder')
                ->performedOn($leadReminder)
                ->log("Added new Lead Reminder of lead Id {$leadReminder->lead_id} as {$leadReminder->message} at time {$leadReminder->reminder_time}");
        });

        static::deleted(function ($leadReminder) {
            activity('leadReminder')
                ->performedOn($leadReminder)
                ->log("Deleted Reminder of lead Id {$leadReminder->lead_id}");
        });
    }
}
