<?php

namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class projects extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','developer','logo','land','architect','created_by','updated_by' ,'agency_id'];

    public function files()
    {
    	return $this->hasMany('App\assignFiles');
    }

    public function leads()
    {
    	return $this->hasOne('App\leads');
    }
}
