<?php

namespace App\Models\Leads;

use Illuminate\Database\Eloquent\Model;
class LeadSource extends Model
{


    public $timestamps = false;
    protected $fillable = ['lead_id','source_id'];

    
    
}
