<?php

namespace App\Models\Leads;

use App\Models\Leads\Source;
use App\Models\Leads\lead;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
	use SoftDeletes;
    protected $fillable = ['name','source_id','agency_id'];
    
    
    public function source()
    {
    	return $this->belongsTo(Source::class);
    }

    public function leads()
    {
    	return $this->belongsToMany(lead::class,'lead_campaigns');
    }
}
