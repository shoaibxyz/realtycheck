<?php

namespace App\Models\Leads;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	use SoftDeletes;
	public $table = "leads_clients";
	
    protected $fillable = ['person_id','agency_id','is_agent','client_type','client_rating','local_interest','future_interest'];

    public function user()
    {
    	return $this->belongsTo(User::class, 'person_id');
    }
    public function deleted_by()
    {
    	return $this->belongsTo(User::class, 'person_id');
    }
}
