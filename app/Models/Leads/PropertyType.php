<?php

namespace App\Models\Leads;

use App\Models\Leads\PropertySubtypes;
use App\Models\Leads\lead;
use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    public function type()
    {
    	return $this->hasMany(PropertySubtypes::class);
    }

    public function lead()
    {
    	return $this->hasOne(lead::class);
    }
}
