<?php

namespace App\Models\leads_followup;

use App\Models\Followup\leads_followup_files;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class leads_followup extends Model
{
    use SoftDeletes;
    use LogsActivity;

    public $table = "leads_followup";

    protected $fillable = ['registeration_no','meeting_channel','meeting_status','meeting_detail','meeting_done_by','in_out', 'next_meeting_channel', 'meeting_date','next_meeting_channel', 'next_meeting_agenda','next_meeting_date', 'next_meeting_by' , 'created_by', 'agency_id'];

    protected static function boot()
    {
        static::updated(function ($followup) {

            $changes = $followup->isDirty() ? $followup->getDirty() : false;
            
            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    $getOriginal = ( $followup->getOriginal($attr) == null ) ? "null" : $followup->getOriginal($attr);
                    $getAttr = ($followup->attr == null ) ? "null" : $followup->$attr;

                    if($attr == 'updated_at')
                        continue;

                    activity('leads_Followup')
                        ->performedOn($followup)
                        ->withProperties(['registration_no' => $followup->registeration_no, 'event' => 'updated'])
                        ->log("Updated followup field named $attr from {$getOriginal} to {$getAttr}");
                }
            }
        });

        static::created(function ($followup) {
            activity('leads_Followup')
                    ->performedOn($followup)
                    ->withProperties(['event' => 'created', 'registration_no' => $followup->registration_no])
                    ->log("Created new <a href='".route('followup.show', $followup->registeration_no)."' style='font-weight: bold; color: #f9a533'>followup</a> ");
        });

        static::deleted(function ($followup) {


            activity('leads_Followup')
                    ->performedOn($followup)
                    ->withProperties(['registration_no' => $followup->registeration_no, 'event' => 'Deleted'])
                    ->log("Deleted followup having registration number {$followup->registeration_no}");
        });
    }
    
    public function files()
    {
    	return $this->hasMany(leads_followup_files::class);
    }

    public function getMeetingDateAttribute($value)
    {
    	return date('d-m-Y', strtotime($value));
    }

    public function getNextMeetingDateAttribute($value)
    {
        if($value != null)
    	   return date('d-m-Y', strtotime($value));

        return "00-00-0000";
    }

    public function getNextMeetingByAttribute($value)
    {
         if($value == null)
            return "";

        $user = User::find($value);
        return $user->name;
    }

    public function getMeetingDoneByAttribute($value)
    {

        $user = User::find($value);
        return $user->name;
    }
}
