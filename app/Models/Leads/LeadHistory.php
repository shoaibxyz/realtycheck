<?php

namespace App\Models\Leads;

use App\Models\Leads\LeadStatus;
use App\Models\Leads\lead;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class LeadHistory extends Model
{
    use SoftDeletes;
    use LogsActivity;

    public $table = "lead_history";

    protected $fillable = ['lead_id','lead_status_id','remarks','created_by','updated_by','meeting_channel','meeting_status','meeting_detail','meeting_done_by','in_out', 'next_meeting_channel', 'meeting_date','next_meeting_channel', 'next_meeting_agenda','next_meeting_date', 'next_meeting_by','agency_id'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($leadHistory) {

            $changes = $leadHistory->isDirty() ? $leadHistory->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                    
                    if($attr == 'updated_at' || $attr == 'updated_by')
                        continue;

                    activity('leadHistory')
                        ->performedOn($leadHistory)
                        ->log("updated leadHistory {$attr} from {$leadHistory->$attr} to {$leadHistory->getOriginal($attr)}");
                }
            }
        });

        static::created(function ($leadHistory) {

            activity('leadHistory')
                ->performedOn($leadHistory)
                ->log("Added a new follow up on lead Id {$leadHistory->lead_id}");
        });

        static::deleted(function ($leadHistory) {
            activity('leadHistory')
                ->performedOn($leadHistory)
                ->log("Deleted Lead Status of lead Id {$leadHistory->lead_id} as {$leadHistory->status->name}");
        });
    }

    public function status()
    {
        return $this->belongsTo(LeadStatus::class,'lead_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function lead()
    {
        return $this->belongsTo(lead::class);
    }

    public function meetingBy()
    {
        return $this->belongsTo(User::class, 'meeting_done_by');
    }

    public static function getHistory($lead_id, $status)
    {
        $query = DB::table('lead_history')
                    ->join('leads', 'leads.id','=','lead_history.lead_id')
                    ->join('lead_status', 'lead_status.id','=','lead_history.lead_status_id')
                    ->where('lead_status.name','like','%'.$status.'%')->where('leads.id', $lead_id)->count();
        return $query;
    }   
    
}
