<?php

namespace App\Models\leads;

use App\Models\leads\leads_followup;
use Illuminate\Database\Eloquent\Model;

class leads_followup_files extends Model
{
    public $table = "leads_followup_files";

    public $timestamps = false;

    protected $fillable = ['registeration_no','leads_followup_id','file_name'];

    public function followup()
    {
    	return $this->belongsToMany(followup::class,'leads_followup_files');
    }
}
