<?php

namespace App\Models\HR;

use App\Models\Access\Role;
use App\Models\HR\AdvanceSalary;
use App\Models\HR\Attendance;
use App\Models\HR\Designation;
use App\Models\HR\EmployeeAllownceDeduction;
use App\Models\HR\Employees;
use App\Models\HR\Leaves;
use App\Models\HR\LeavesRequests;
use App\Models\HR\PaySlip;
use App\Models\HR\department;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    public $table = "hr_employee";
    protected $fillable = ['person_id','ntn_number','joining_date','leaving_date','last_company','total_experience','salary','department_id','designation_id','commission', 'resume' , 'created_by','acc_number','payment_type','appointment_status', 'start_time', 'end_time', 'is_restricted'];
//  'is_restricted',
    public function Roles()
    {
        return $this->belongsToMany(Role::class,'role_person','person_id','role_id');
    }

   	public function user()
    {
        return $this->belongsTo(User::class, 'person_id');
    }

    public function leave()
    {
        return $this->hasMany(Leaves::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class,'designation_id');
    }

    public function department()
	{
		return $this->belongsTo(department::class,'department_id');
	}

    public function allownceDeduction()
    {
        return $this->hasMany(EmployeeAllownceDeduction::class);
    }

    public function payslip()
    {
        return $this->hasMany(PaySlip::class);
    }

    public function advanceSalary()
    {
        return $this->hasMany(AdvanceSalary::class);
    }

    public function attendance()
    {
        return $this->hasMany(Attendance::class);
    }

    public function leaves()
    {
        return $this->hasMany(LeavesRequests::class);
    }

    public static function getEmployee($id)
    {
        return Employees::where('person_id',$id)->first();
    }
}






