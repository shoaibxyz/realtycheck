<?php

namespace App\Models\HR;

use App\Models\HR\Employees;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Designation extends Model
{
	use SoftDeletes;
	use LogsActivity; 

    public $table = "hr_designations";
    protected $fillable = ['name' , 'created_by' , 'agency_id'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($designation) {

            $changes = $designation->isDirty() ? $designation->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                  if($attr == 'updated_at')
                        continue;
                      
                    activity('designation')
                        ->performedOn($designation)
                        ->log(":causer.name updated designation $attr from {$designation->getOriginal($attr)} to {$designation->$attr}");
                }
            }

        });

        static::created(function ($designation) {

            activity('designation')
                ->performedOn($designation)
                ->log(":causer.name created designation {$designation->name}");
        });

        static::deleted(function ($designation) {

            activity('designation')
                ->performedOn($designation)
                ->log(":causer.name deleted designation {$designation->name}");
        });
    }

    public function employee()
    {
        return $this->hasOne(Employees::class);
    }

}
