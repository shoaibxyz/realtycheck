<?php

namespace App\Models\HR;

use App\Models\HR\Employees;
use App\Models\HR\PaySlip;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class department extends Model
{
	use SoftDeletes;
	use LogsActivity; 

    public $table = "hr_department";
    protected $fillable = ['name' , 'created_by' , 'agency_id'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($department) {

            $changes = $department->isDirty() ? $department->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {
                  if($attr == 'updated_at')
                        continue;
                      
                    activity('department')
                        ->performedOn($department)
                        ->log(":causer.name updated department $attr from {$department->getOriginal($attr)} to {$department->$attr}");
                }
            }

        });

        static::created(function ($department) {

            activity('department')
                ->performedOn($department)
                ->log(":causer.name created department {$department->name}");
        });

        static::deleted(function ($department) {

            activity('department')
                ->performedOn($department)
                ->log(":causer.name deleted department {$department->name}");
        });
    }

    public function employee()
    {
        return $this->hasOne(Employees::class);
    }

    public function payslip()
    {
        return $this->hasMany(PaySlip::class);
    }
    
}
