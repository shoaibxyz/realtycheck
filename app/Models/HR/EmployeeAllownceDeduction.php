<?php

namespace App\Models\HR;

use App\Models\HR\Employees;
use Illuminate\Database\Eloquent\Model;

class EmployeeAllownceDeduction extends Model
{
    public $table = 'hr_employee_allow_deduc';
    protected $fillable = ['employee_id','label','amount','type','created_by','updated_by'];

    public function employee()
    {
    	$this->belongsTo(Employees::class);
    }
}


