<?php

namespace App;

use App\User;
use App\agency;
use App\batch;
use App\plotSize;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class assignFiles extends Model
{
    use SoftDeletes;
    use LogsActivity;

    public $table = "assign_files";
    public $timestamps = false;
    protected $fillable = ['registration_no','agency_id','created_by','updated_by','plot_size_id','batch_id','plot_type','is_blocked', 'deleted_at'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($assignFile) {

            $changes = $assignFile->isDirty() ? $assignFile->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                    if($attr == 'updated_by')
                        continue;

                    $log = "Updated assign files $attr from {$assignFile->getOriginal($attr)} to {$assignFile->$attr}";

                    if($attr == 'batch_id')
                    {
                        $preVal = batch::find($assignFile->getOriginal($attr));
                        $newVal = batch::find($assignFile->$attr);
                        $log = "Updated assign files batch number from $preVal->batch_no to $newVal->batch_no";
                    }

                    if($attr == 'agency_id')
                    {
                        $preVal = agency::find($assignFile->getOriginal($attr));
                        $newVal = agency::find($assignFile->$attr);
                        $log = "Updated assign files agency name from $preVal->name to $newVal->name";
                    }

                    if($attr == 'plot_size_id')
                    {
                        $preVal = plotSize::find($assignFile->getOriginal($attr));
                        $newVal = plotSize::find($assignFile->$attr);
                        $log = "Updated assign files plot size from $preVal->plot_size to $newVal->plot_size";
                    }

                    activity('Assign Files')
                        ->performedOn($assignFile)
                        ->withProperties(['event' => 'updated', 'registration_no' => $assignFile->registration_no])
                        ->log($log);
                }
            }

        });

        static::created(function ($assignFile) {
            $agency = agency::find($assignFile->agency_id);
            $batch = batch::find($assignFile->batch_id);
            $plot_size = plotSize::find($assignFile->plot_size_id);

            activity('Assign Files')
                ->performedOn($assignFile)
                ->withProperties(['event' => 'created', 'registration_no' => $assignFile->registration_no])
                ->log("Added new file in assign files having registration no {$assignFile->registration_no} assigned to agency {$agency->name} in batch number {$batch->batch_no} for plot size {$plot_size->plot_size}");
        });


        static::deleted(function($assignFile){
            $agency = agency::find($assignFile->agency_id);
            $batch = batch::find($assignFile->batch_id);
            $plot_size = plotSize::find($assignFile->plot_size_id);

            activity('Assign Files')
                ->performedOn($assignFile)
                ->withProperties(['event' => 'deleted', 'registration_no' => $assignFile->registration_no])
                ->log("Deleted file from assign files having registration no {$assignFile->registration_no} assigned agency {$agency->name} in batch number {$batch->batch_no} for plot size {$plot_size->plot_size}");
        });
    }

    public function agency()
    {
        return $this->belongsTo('App\agency');
    }

    public function plotSize()
    {
        return $this->belongsTo('App\plotSize');
    }

    public function batch()
    {
        return $this->belongsTo('App\batch');
    }
    
    public function booking()
    {
        return $this->hasOne('App\Payments','registration_no','registration_no');
    }

    public function scopeAgencyFilesCount($query, $agency, $plotType)
    {
        return $query->where('assign_files.agency_id', $agency)->where('assign_files.plot_type',$plotType);
    }

    public function scopeExcludeOfferFiles($query, $files)
    {
        return $query->whereNotIn('assign_files.registration_no',$files);
    }

    public function scopeFiles($query, $agency_id, $plotType, $rateType, $booked = 0, $files = null)
    {

        $result = $query
        ->join('batch', 'batch.id','=','assign_files.batch_id')
        ->AgencyFilesCount($agency_id, $plotType)
        ->where('batch.rate_type',$rateType);

        if($booked)
        {
            $query->where('assign_files.is_reserved',1);
        }

        if($files)
            $result->ExcludeOfferFiles($files);

        return $result->count();
    }

    public function scopeRegisteredFiles($query, $batch_id, $booked = 1, $plot_size = null)
    {

        $result = $query
        ->where('assign_files.batch_id', $batch_id)
        ->where('assign_files.plot_size_id', $batch_id)
        ->where('assign_files.is_reserved',$booked);

        return $result->count();
    }

    public function scopeIssuedFiles($query, $batch_id, $plot_size,  $type)
    {

        $result = $query
        ->where('assign_files.batch_id', $batch_id)
        ->where('assign_files.plot_size_id', $plot_size)
        ->where('assign_files.plot_type', $type);

        return $result->count();
    }

    public static function batchBookedFiles($batch_id, $agency_id, $plot_type)
    {

        // $sql = "SELECT bookings.payment_date, batch.batch_no, plot_size.plot_size from assign_files INNER join batch on batch.id = assign_files.batch_id INNER join plot_size on plot_size.id = assign_files.plot_size_id INNER JOIN bookings on bookings.registration_no = assign_files.registration_no WHERE assign_files.agency_id=".$agecny_id." and assign_files.plot_type = ".$plot_type." and batch.batch_no = ".$batch_no."";

        return DB::table('assign_files')
        ->select(DB::Raw('bookings.payment_date, plot_size.plot_size'))
        // ->join('batch', 'batch.id', '=', 'assign_files.batch_id')
        ->join('plot_size', 'plot_size.id', '=', 'assign_files.plot_size_id')
        ->join('bookings', 'bookings.registration_no', '=', 'assign_files.registration_no')
        ->join('members', 'members.registration_no', '=', 'bookings.registration_no')
        ->when($agency_id, function($files) use ($agency_id){
            $files->where('assign_files.agency_id', $agency_id);
        })
        
        ->where('assign_files.plot_type', $plot_type)
        ->where('assign_files.batch_id', $batch_id)->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')
        ->where('bookings.status', 1)
        ->whereNull('bookings.deleted_at')
        ->where('bookings.is_cancelled', 0)
        ->where('members.is_current_owner', 1)
        ->groupBy('bookings.payment_date')
        // ->orderBy('batch.batch_no')
        ->get();
    }


    public static function countDatewiseBooking($agency_id, $payment_date, $batch_id, $plot_type, $plot_size)
    {
//         SELECT * FROM `bookings`
// INNER JOIN assign_files on assign_files.registration_no = bookings.registration_no
// INNER JOIN plot_size on plot_size.id = assign_files.plot_size_id
// WHERE assign_files.batch_id =3 and bookings.payment_date = "2017-07-13" and assign_files.plot_type = "Residential" and plot_size.plot_size = "04-M"

        $query = DB::table('assign_files')

        ->join('bookings', 'bookings.registration_no', '=', 'assign_files.registration_no')
        // ->join('batch', 'batch.id', '=', 'assign_files.batch_id')
        ->join('plot_size', 'plot_size.id', '=', 'assign_files.plot_size_id')
        ->where('assign_files.agency_id', $agency_id)
        ->where('assign_files.plot_type', $plot_type)
        ->where('plot_size.plot_size', $plot_size)
        ->where('assign_files.batch_id', $batch_id)
        ->where('bookings.payment_date', $payment_date)
        ->whereNull('assign_files.deleted_at')->whereNull('assign_files.is_cancelled')
        ->whereNull('bookings.deleted_at')->where('bookings.is_cancelled' , 0)
        // ->groupBy('bookings.payment_date')
        ->count();

        return $query;
        // dd(User::getEloquentSqlWithBindings($query));
    }
}


