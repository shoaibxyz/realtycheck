<?php

namespace App;
use Image;

class Helpers{

	public static function getValue($key, $arr)
	{
		foreach($arr as $keyy => $array)
		{
			if($key == $keyy){
				return $array;
			}
		}
	}

    public static function TransferDocuments($key = null)
	{
		$arr = [
			'1' => 'NDC Application',
			'2' => 'Booking Receipt',
			'3' => 'PAC / NDC',
			'4' => 'All Payment Receipts',
			'5' => 'Late Payment Charges receipt',
			'6' => 'NDC Fees',
			'7' => 'Affidavit on 1200 ruppes stamp paper',
			'8' => 'Valid CNIC copies of Transferor, Transferee and Nominee',
			'9' => 'Welcome Latter',
			'10' => 'No of times already transfered',
		];

		if($key)
			return static::getValue($key, $arr);

		return $arr;
    }

    public static function createImage($file,$destpath,$imgName,$w,$h,$type='')
    {
        $img = Image::make($file->getRealPath());
        $img->resize($w, $h, function ($constraint) {
            //$constraint->aspectRatio();
        });

        $img->save($destpath.'/'.$imgName,80);
        return $imgName;
    }
    
    public static function leaveTypes($key = null)
	{
		$arr = [
			'' => 'Select Type',
			'1' => 'Annual Leaves',
			'2' => 'Casual Leaves',
			'3' => 'Medical Leaves',
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}
	
	public static function Status($key = null)
	{
		$arr = [
			'' => 'Select One',
			'1' => 'Approved',
			'2' => 'Pending',
			'3' => 'Denined',
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}
    
    public static function booking_status($key = null)
	{
		$arr = [
			'1' => 'Cancelled',
			'2' => 'Refunded',
			'3' => 'Merged',
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}
	
	public static function support_status($key = null)
	{
		$arr = [
			'1' => 'Pending',
			'2' => 'In Process',
			'3' => 'Complete',
			'4' => 'Done',
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}
	
	public static function Classification($key = null)
	{
		$arr = [
			'' => 'Select Classification',
			'1' => 'Very Hot',
			'2' => 'Hot',
			'3' => 'Moderate',
			'4' => 'Cold',
			'5' => 'Very Cold',
		];
		if($key)
			return static::getValue($key, $arr);

		return $arr;
	}

	public static function ClientSearchFileds($key = null)
	{
		$arr = [
			'all' => 'All',
			'id' => 'ID',
			'name' => 'Name',
			'phone_mobile' => 'Mobile',
			'permanent_address' => 'Address'
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}

	public static function Budget()
	{
		$arr = [
			'' => 'Any',
			'1' => '200000',
			'2' => '200000 to 500000',
			'3' => '500000 to 750000',
			'4' => '750000 to 1000000',
			'5' => '1000000 to 2000000',
			'6' => '2000000 to 5000000',
			'7' => 'over 5000000',
		];

		return $arr;
	}

	public static function Purpose($key = null)
	{
		$arr = [
			'' => 'Select Purpose',
			'1' => 'Buy',
			'2' => 'Rent',
			'3' => 'Sell',
		];

		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}

	public static function LandUnits()
	{
		$arr = [
			'' => 'Select Unit',
			'1' => 'Kanal',
			'2' => 'Marla',
			'3' => 'Square Feet',
			'4' => 'Square Meters',
			'5' => 'Square Yards',
		];

		return $arr;
	}


	public static function ClinetTypes($key = null)
	{
		$arr = [
			'' => 'Select Type',
			'1' => 'Direct Client',
			'2' => 'Agent',
			'3' => 'Investor',
			'4' => 'Investment Funds',
			'5' => 'Others',
		];


		if($key)
		{
			if($key)
			return static::getValue($key, $arr);
		}

		return $arr;
	}

	public static function ClinetRating()
	{
		$arr = [
			'' => 'Select Rating',
			'1' => 'Very Rich',
			'2' => 'Rich',
			'3' => 'Middle Class',
			'4' => 'Lower Middle',
			'5' => 'Poor',
		];

		return $arr;
	}
	
		public static function Beds()
	{
		$arr = [
			'' => 'Select Beds',
			'studio' => 'studio',
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'10+' => '10+',
		];

		return $arr;
	}
	
	public function getHelper()
	{
		$Classification = static::Classification();
		$ClientSearchFileds = static::ClientSearchFileds();
		$Budget = static::Budget();
		$Purpose = static::Purpose();
		$LandUnits = static::LandUnits();
		$ClinetTypes = static::ClinetTypes();
		$ClinetRating = static::ClinetRating();
		$InstallmentMode = static::InstallmentMode();
		$Beds = static::Beds();
		$leaveTypes = static::leaveTypes();
		$listing_status = static::listing_status();
		$property_status = static::property_status();


		$propertyType = PropertyType::all();
		$Sources = Source::all();
		$LeadStatus = LeadStatus::all();
		$Projects = projects::all();
		$countries = Country::all();

		$helpers = [
			'Classifcation' => $Classification,
			'ClientSearchFileds' => $ClientSearchFileds,
			'Budget' => $Budget,
			'Purpose' => $Purpose,
			'LandUnits' => $LandUnits,
			'ClinetTypes' => $ClinetTypes,
			'ClinetRating' => $ClinetRating,
			'InstallmentMode' => $InstallmentMode,
			'Beds' => $Beds,
			'leaveTypes' => $leaveTypes,
			'listing_status' => $listing_status,
			'property_status' => $property_status,
			'property_type' => $propertyType,
			'Sources' => $Sources,
			'LeadStatus' => $LeadStatus,
			'Projects' => $Projects,
			'countries' => $countries,
			
		];

		return response()->json($helpers);



	}
}
