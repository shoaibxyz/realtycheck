<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reschedule extends Model
{
	public $table = "reschedule_payment_plan";

	protected	$fillable = [
				'current_payment_plan',
				'new_payment_plan',
				'registration_no',
				'reschedule_date',
				'remarks',
	];

	protected $dates = ['created_at', 'updated_at', 'reschedule_date'];

	public function CurrentPaymentPlan()
	{
		return $this->belongsTo('App\paymentSchedule','current_payment_plan');
	}

	public function NewPaymentPlan()
	{
		return $this->belongsTo('App\paymentSchedule','new_payment_plan');
	}

	public function setRescheduleDateAttribute($value)
	{
		return $this->attributes['reschedule_date'] = date('Y-m-d', strtotime($value));
		// return $this->attributes['reschedule_date'] = date('Y-m-d', strtotime($value));
	}
}
