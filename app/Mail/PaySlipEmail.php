<?php

namespace App\Mail;

use App\Models\HR\AdvanceSalary;
use App\Models\HR\EmployeeAllownceDeduction;
use App\Models\HR\Employees;
use App\Models\HR\PaySlip;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaySlipEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $payslip;
    public $employee;
    public $allownceDeduction;
    public $incomeTax;
    public $taxPercentage;
    public $checkAdvanceSalary;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, PaySlip $payslip,Employees $employee, $allownceDeduction, $incomeTax, $taxPercentage, $checkAdvanceSalary = null)
    {
        $this->user = $user;
        $this->payslip = $payslip;
        $this->employee = $employee;
        $this->allownceDeduction = $allownceDeduction;
        $this->incomeTax = $incomeTax;
        $this->taxPercentage = $taxPercentage;
        if($checkAdvanceSalary)
            $this->checkAdvanceSalary = $checkAdvanceSalary;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Payslip generated for month ' .$this->payslip->month. '-'.$this->payslip->year)
        ->markdown('email.payroll.salary', [
            'user' => $this->user,
            'payslip' => $this->payslip,
            'employee' => $this->employee,
            'allownceDeduction' => $this->allownceDeduction,
            'incomeTax' => $this->incomeTax,
            'taxPercentage' => $this->taxPercentage,
            'checkAdvanceSalary' => ($this->checkAdvanceSalary != null) ? $this->checkAdvanceSalary : null,
        ]);
    }
}
