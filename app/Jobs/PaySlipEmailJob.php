<?php

namespace App\Jobs;

use App\Mail\PaySlipEmail;
use App\Models\HR\Employees;
use App\Models\HR\PaySlip;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class PaySlipEmailJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $payslip;
    public $employee;
    public $allownceDeduction;
    public $incomeTax;
    public $taxPercentage;
    public $checkAdvanceSalary;
    public $receiver;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($receiver, User $user, PaySlip $payslip,Employees $employee, $allownceDeduction, $incomeTax, $taxPercentage, $checkAdvanceSalary = null)
    {
        $this->user = $user;
        $this->payslip = $payslip;
        $this->employee = $employee;
        $this->allownceDeduction = $allownceDeduction;
        $this->incomeTax = $incomeTax;
        $this->taxPercentage = $taxPercentage;
        $this->receiver = $receiver;
        if($checkAdvanceSalary)
            $this->checkAdvanceSalary = $checkAdvanceSalary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {
        Mail::to($this->receiver)->send(new PaySlipEmail($this->employee->user, $this->payslip, $this->employee, $this->allownceDeduction, $this->incomeTax, $this->taxPercentage, $this->checkAdvanceSalary));
    }
}
