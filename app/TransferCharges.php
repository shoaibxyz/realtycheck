<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferCharges extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
   protected $table = "transfer_charges";
   
   protected $fillable = ['plot_size_id','charges','created_by','updated_by'];	

   public function plotSize()
	{
		return $this->belongsTo('App\plotSize');
	}	
}

