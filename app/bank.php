<?php

namespace App;

use App\Traits\Encryptable;
use App\bank;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class bank extends Model
{
	use LogsActivity;
    use SoftDeletes;
    // use Encryptable;

    // protected $encryptable = [
    //     'branch_code',
    //     'account'
    // ];

	public $table = "bank";
    protected $fillable = ['bank_name','short_name','branch_code','account','branch_name'];
    protected static $ignoreChangedAttributes = ['updated_at'];

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($bank) {

            $changes = $bank->isDirty() ? $bank->getDirty() : false;

            if (count($changes) > 0) {
                foreach ($changes as $attr => $value) {

                	if($attr == 'updated_at')
                		continue;

                    activity('Bank')
                        ->performedOn($bank)
                        ->log(":causer.name updated bank $attr from {$bank->getOriginal($attr)} to {$bank->$attr}");
                }
            }

        });

        static::created(function ($bank) {

            activity('Bank')
                ->performedOn($bank)
                ->log(":causer.name created bank {$bank->bank_name} having account number {$bank->account}");
        });

        static::deleted(function ($bank) {

            activity('Bank')
                ->performedOn($bank)
                ->log(":causer.name deleted bank {$bank->bank_name} having account number {$bank->account}");
        });
    }

    public function receipt()
     {
        return $this->hasMany('App\receipts');
     }

     public function pdc()
     {
        return $this->hasOne('App\pdc');
     }

     public static function bankInfo($id)
     {
          return bank::find($id);
     }

}
