<?php 

return [
    'roles' => [
        'member' => 'Member',
        'agent' => 'Agent',
        'admin' => 'Admin',
        'employee' => 'Employee',
        'accountant' => 'Accountant',
        'call-center' => 'call-center',
        'Super-Admin' => 'Super-Admin'
        // etc
    ]
];