<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test','ReportsController@test');

Route::get('login2',function () {
    return view('auth.login2');
});


Route::get('testsms', 'SmsController@testsms');


Route::get('/', function () {
    return view('landingpage');
});


Route::get('/exportBookings', 'MainController@exportBookings');
Route::get('/exportReceipts', 'MainController@exportReceipts');

Auth::routes();

Route::get('home', function () {
    return redirect('admin/dashboard');
});
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {



    Route::get('user/getuser' , 'PersonController@getuser')->name('getuser');
  Route::get('user/editgetuser/{id}' , 'PersonController@editgetuser')->name('editgetuser');
  Route::post('user/updategetuser/{id}' , 'PersonController@updategetuser')->name('updategetuser');
  
  
    //Dashboard Routes
    Route::get('dashboard', 'PersonController@dashboard')->name('dashboard');
    Route::get('home', 'PersonController@dashboard')->name('home');
    Route::get('dashboardGraph/{startDate?}/{endDate?}', 'PersonController@dashboardGraph')->name('dashboardGraph');

    //User Management Routes
    Route::resource('module', 'ModulesController');
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('permission-role', 'PermissionRoleController');
    Route::get('permission/assign/{role_id}', 'PermissionRoleController@assignPermission')->name('role.assignPermission');

    //Agency Routes
    Route::resource('agency', 'AgencyController');
    Route::get('agency/view/{id}', 'AgencyController@viewagents')->name('agency.view');
    Route::post('agency/getAgency', 'AgencyController@getAgencyByRegistrationNo')->name('agency.byRegistrationNo');

    //Batch Routes
    Route::resource('batch', 'BatchController');
    Route::post('batch/getBatch', 'BatchController@getBatch')->name('agency.batch');
    Route::post('batch/parentBatch', 'BatchController@parentBatch')->name('agency.parentBatch');

    //Assign Files Routes
    Route::get('assign-file/block/{registration_no}/{block_id}', 'assignFilesController@block')->name('assign-file.block');
    Route::get('assign-file/getFiles', 'assignFilesController@getFiles')->name('assign-file.getFiles');
    Route::resource('assign-file', 'assignFilesController');

    //Plots Routes
    Route::get('plotsize/getPlots', 'plotSizeController@getPlots')->name('plotsize.getPlots');
    Route::resource('plotsize', 'plotSizeController');
    Route::resource('features', 'FeatureSetController');

    //Payment Plan Routes
    Route::post('PlanInfo', 'PaymentScheduleController@PlanInfo')->name('PlanInfo');
    Route::resource('payment-schedule', 'PaymentScheduleController');
    Route::resource('installment', 'InstallmentController');
    Route::get('installment/plan/{plan_id}', 'InstallmentController@create');

    //Users Routes
    Route::get('users/getUsers', 'PersonController@getPersons')->name('users.getPersons');
    
    //Route added by Habib
    Route::patch('updateUserDetail/{UserId}/{bookingId}', 'PersonController@updateUserDetail')->name('users.updateUser');
    
    Route::resource('users', 'PersonController');
    Route::post('users/updatePassword', 'PersonController@updatePassword')->name('users.updatePassword');


    Route::resource('member-registration', 'MembersController');
    Route::get('user/edit1/{person_id}', 'PersonController@edit')->name('users.edit');
    Route::put('user/update/{person_id}', 'PersonController@update1')->name('person.update');

    //Booking Routes
    Route::get('booking/generate/{person_id}', 'PaymentsController@generateBooking')->name('generateBooking');
    Route::post('booking/specialPlan', 'PaymentsController@specialPlan')->name('booking.specialPlans');
    Route::get('booking/getBookings', 'PaymentsController@getBookings')->name('booking.getBookings');

//Route added by Habib
    Route::patch('updateBookingDetail/{id}', 'PaymentsController@updateBookingDetail')->name('booking.updateBookingDetail');

    Route::resource('booking', 'PaymentsController');
    Route::get('booking/cancel/{booking_id}', 'PaymentsController@crm')->name('booking.cancel');
    Route::post('booking/cancel/{booking_id}', 'PaymentsController@cancelBooking');
    Route::get('booking/ledger/{booking_id}', 'PaymentsController@ledger')->name('booking.ledger');
    Route::get('booking/ledger/download/{booking_id}', 'PaymentsController@ledgerDownload')->name('booking.ledgerDownload');
    Route::post('booking/ledger-search', 'PaymentsController@ledgerSearch')->name('booking.ledger.search');
    Route::get('booking/welcome-letter', 'PaymentsController@welcomeLetter')->name('booking.welcomeLetter');
    Route::post('getFstDetail', 'PaymentsController@getFstDetail')->name('getFstDetail');

    Route::post('installmentPlan', 'InstallmentController@installmentPlan')->name('installmentPlan');

    //Members Routes
    Route::post('member/inquiry-search', 'MembersController@inquirySearch')->name('member.inquiry.search');
    Route::get('member/inquiry/{registration_no}', 'MembersController@inquiry')->name('member.inquiry');
    Route::post('memberInfo', 'MembersController@MemberInfo')->name('memberInfo');
    
    Route::get('/inquiry/findolduser/{id}', 'PersonController@olduser');

    //Followup Routes
    Route::get('followup/due-meetings', 'FollowupController@dueMeetings')->name('followup.dueMeetings');
    Route::post('followup/due-meetings', 'FollowupController@getDueMeetings')->name('followup.getDueMeetings');
    Route::get('followup/{followup_file_id}/delete', 'FollowupController@deleteAttachment')->name('followup.attachment.delete');
    Route::get('followup/attachment/{follow_id}', 'FollowupController@viewAttachment')->name('followup.attachment.view');
    Route::resource('followup', 'FollowupController');
    Route::post('followup/registration-search', 'FollowupController@search')->name('followup.search');

    Route::resource('bank', 'BankController');
    Route::get('setting', 'PersonController@setting')->name('admin.setting');

//Receipt Routes
    Route::get('receipt/getReceipts/{discount_type?}', 'ReceiptsController@getReceipts')->name('receipt.getReceipts');
    Route::resource('receipt', 'ReceiptsController');

Route::post('InstallmentReceipt', 'InstallmentController@InstallmentReceipt')->name('InstallmentReceipt');
Route::post('InstallmentReceipt2', 'InstallmentController@InstallmentReceipt2')->name('InstallmentReceipt2');

//PDC Routes
    Route::get('pdc/send', 'PDCController@sendclear')->name('pdc.send');
    Route::get('pdc/mark', 'PDCController@markclear')->name('pdc.mark');
    Route::get('pdc/verify', 'PDCController@verify')->name('pdc.verify');
    Route::post('pdc/sendClearence' , 'PDCController@sendClearence')->name('pdc.sendClearence');
    Route::post('pdc/markClearence' , 'PDCController@markClearence')->name('pdc.markClearence');
    Route::post('pdc/postVerify' , 'PDCController@postVerify')->name('pdc.postVerify');
Route::resource('pdc' , 'PDCController');

//LPC Routes
Route::get('lpc-definition', 'LpcController@lpcDefinition')->name('lpc-definition');
Route::post('lpc-definition', 'LpcController@lpcDefinitionStore')->name('lpc-definition.store');
Route::post('lpc/lpc-calculate', 'LpcController@lpcCalculate')->name('lpc.calculate');
Route::get('lpc/lpc-calculate', 'LpcController@index')->name('lpc-calculate.index');
Route::get('lpc/waive', 'LpcController@waive')->name('lpc.waive');
Route::get('lpc/waive-registeration', 'LpcController@waiveRegisteration')->name('lpc.waiveRegisteration');
Route::post('lpc/waive', 'LpcController@PostWaive')->name('lpc.waive.post');

Route::post('lpc/waive-registeration', 'LpcController@postWaiveRegisteration')->name('lpc.postWaiveRegisteration');
Route::resource('lpc', 'LpcController');
Route::post('LpcCharges', 'LpcController@LpcCharges')->name('LpcCharges');
Route::post('getLPCs', 'LpcController@getLPCs')->name('getLPCs');
Route::post('lpcByRegistration', 'LpcController@lpcByRegistration')->name('lpcByRegistration');


//reschedule routes
Route::resource('reschedule', 'RescheduleController');


//sms routes
Route::get('sms/getSMS', 'SmsController@getSMS')->name('sms.getSMS');
Route::get('sms/send-now', 'SmsController@sendNow')->name('send.sms.now');
Route::post('sms/sent-to', 'SmsController@checkSentTo')->name('sms.checkSentTo');
Route::post('sms/send', 'SmsController@send')->name('sms.send');
Route::resource('sms', 'SmsController');

Route::post('sms/downloadcsv', 'SmsController@downloadcsv');

//transfer routes
Route::resource('transfer_charges', 'Transfer_ChargesController');
Route::post('transfer_charges/{id}', 'Transfer_ChargesController@update');
Route::post('transfer/checkTransfer', 'transfersController@checkTransfer')->name('checkTransfer');
Route::get('transfer/acknowledgement/{id}', 'transfersController@acknowledgement')->name('transfer.ack');
Route::get('transfer/sellerdetail/{id}', 'transfersController@sellerdetail')->name('transfer.sellerdetail');
Route::get('transfer/affidavit/{id}', 'transfersController@affidavit')->name('transfer.affidavit');
Route::get('transfer/init', 'transfersController@init')->name('transfer.init');
Route::get('transfer/ndc/{id?}', 'transfersController@ndc')->name('transfer.ndc');
Route::post('transfer/transfer-sheet-download', 'transfersController@transferSheetDownload')->name('transfer.transfer-sheet');
Route::post('transfer/init', 'transfersController@postinit')->name('transfer.postInit');
Route::post('transfer/download/ndc', 'transfersController@downloadNdc')->name('transfer.downloadNdc');
Route::resource('transfer', 'transfersController');


//support routes
Route::get('support/task','TaskController@index')->name('task');
Route::get('support/create','TaskController@create');
Route::post('support/store','TaskController@store');
//Route::get('support','TaskController@changestatus');
Route::get('support/reply/{id}','TaskController@reply1');
Route::post('support/reply/store','TaskController@replystore');
Route::get('support/categorytask/{id}','TaskController@category_task');



Route::get('category','CategoryController@index');
Route::get('category/create','CategoryController@create');
Route::post('category/store','CategoryController@store');
Route::get('category/edit/{id}','CategoryController@edit');
Route::get('category/update/{id}','CategoryController@update');
Route::delete('category/destroy/{id}','CategoryController@destroy');

//report routes
Route::get('report/receipt', 'ReportsController@CheckReceiptReport')->name('report.CheckReceiptReport');
Route::get('report/bookings', 'ReportsController@bookings')->name('report.bookings');
Route::get('report/due-amounts', 'ReportsController@dueAmounts')->name('report.due_amount');
Route::get('report/bank', 'ReportsController@viewBankReport')->name('report.bank');
Route::get('report/members', 'ReportsController@getMembers')->name('report.membersReports');
    Route::get('report/receipts', 'ReportsController@index')->name('report.dailyReceipt');
    Route::get('report/sms', 'ReportsController@smsReport')->name('report.sms');
        Route::get('report/inventory', 'ReportsController@InventoryReport')->name('report.InventoryReport');
    Route::get('report/receipts/rebate/{discount_type}', 'ReportsController@rebateType')->name('report.rebate_type');
    Route::get('report/recovery/{from?}/{to?}', 'ReportsController@recoveryReport')->name('report.recovery-report');
    Route::get('report/due-amount-accumulated', 'ReportsController@dueAccumulatedAmounts')->name('report.CheckdueAccumulatedAmounts');
    Route::get('report/due-amounts-accumulated', 'ReportsController@CheckdueAccumulatedAmounts')->name('report.CheckdueAccumulatedAmounts');
        Route::get('report/customer-feedback', 'ReportsController@CustomerFeedbackReportForm')->name('report.CustomerFeedbackReportForm');
Route::get('report/sale-summary', 'ReportsController@saleSummary')->name('report.saleSummary');
    Route::get('report/defaulters/{from?}/{to?}', 'ReportsController@defaulters')->name('report.defaulters');
    Route::get('report/alert', 'ReportsController@alert')->name('report.alert');
    Route::get('report/activity', 'ReportsController@ActivityLogs')->name('report.activity');
     Route::get('report/transfer/{from?}/{to?}', 'ReportsController@transferReport')->name('report.transferReport');
     Route::get('report/overdue-amounts', 'ReportsController@overdueAmounts')->name('report.overdue_amount');
    Route::get('report/search-batch-files', 'ReportsController@viewbatchFiles')->name('report.batch-files');
    Route::get('report/check-paid-installments', 'ReportsController@checkPaidInst')->name('report.check-paid-inst');
    Route::get('report/cancel', 'ReportsController@cancelReport')->name('report.cancelReport');
Route::get('report/lpc', 'ReportsController@getLPCs')->name('report.getlpcReport');
Route::get('report/bank-report', 'ReportsController@bankreport')->name('report.bank_report');
Route::get('report/due-amount', 'ReportsController@CheckdueAmounts')->name('report.CheckdueAmounts');
Route::get('report/customers-feedback', 'ReportsController@CustomerFeedbackReport')->name('report.CustomerFeedbackReport');
Route::get('report/sms-report', 'ReportsController@getSMS')->name('report.getSms');
Route::post('report/get-members', 'ReportsController@membersReports')->name('report.getMembers');
Route::get('report/search-paid-installments', 'ReportsController@viewPaidInst')->name('report.view-paid-inst');
Route::get('report/overdue-amount', 'ReportsController@CheckOverDueAmounts')->name('report.CheckOverDueAmounts');
Route::get('report/booking', 'ReportsController@BookingReport')->name('report.bookingsReport');
Route::get('report/alert-report', 'ReportsController@checkAlertReport')->name('report.alert-report');
Route::get('report/cancels', 'ReportsController@Checkcancelreport')->name('report.Checkcancelreport');
Route::post('report/lpcReport', 'ReportsController@lpcReport')->name('report.lpcReport');
Route::get('report/view-batch-files', 'ReportsController@checkBatchFiles')->name('report.check-batch-files');

Route::get('salaryReport', 'ReportsController@salaryReport')->name('salaryReport'); 
    
Route::get('getReport', 'ReportsController@getsalary')->name('getsalaryreport'); 

Route::get('report/agency', 'ReportsController@agency')->name('report.agencyreport'); 

Route::get('report/summaryreport','ReportsController@summaryreport');
// Route::get('report/summaryreport/{from?}/{to?}', 'ReportsController@summaryreport')->name('report.summaryreport');

Route::resource('alert', 'AlertsController');
Route::get('alerts/check', 'AlertsController@checkAlerts')->name('alert.check');
Route::get('alert/{id}/delete', 'AlertsController@delete')->name('alert.delete');

Route::get('tsms', 'SmsController@t_sms');

Route::get('report/merged', 'ReportsController@mergedReport')->name('report.mergedReport');
Route::get('report/merged', 'ReportsController@Checkmergedreport')->name('report.Checkmergedreport');

});

Route::group(['middleware' => 'auth'], function () {

Route::get('member', 'PersonController@showprofile');
Route::get('memberedit/{id}', 'PersonController@editprofile');
Route::post('memberstore/{id}', 'PersonController@storeprofile')->name('memberstore');

//Ajax Routes
Route::post('findname', 'transfersController@findname')->name('findname');

Route::post('receipt/getCancelDetail', 'ReceiptsController@getCancelDetail')->name('receipt.getCancelDetail');
Route::post('receipt/cancel', 'ReceiptsController@cancel')->name('receipt.cancel');
Route::post('PlanInfo', 'PaymentScheduleController@PlanInfo')->name('PlanInfo');
Route::post('memberInfo', 'MembersController@MemberInfo')->name('memberInfo');
Route::get('smstemplates', 'SmsController@smstemplates')->name('smstemplates');
Route::get('getmessage', 'SmsController@getmessage')->name('getmessage');
Route::post('user/city', 'PersonController@getcity')->name('admin.city');
        Route::post('user/state', 'PersonController@getState')->name('admin.state');

});









