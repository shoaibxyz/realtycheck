<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('forgetPassword', "ApiController@forgetPassword");

Route::group(['middleware' => 'jwt.auth'], function(){

	Route::post('userInfo', 'ApiController@get_user_details');	
	Route::post('register', 'ApiController@register');
	Route::post('uploadUserPicture', 'ApiController@uploadUserPicture');
	Route::post('uploadCnicPicture', 'ApiController@uploadCnicPicture');
	Route::post('uploadAffidavitForm', 'ApiController@uploadAffidavitForm');
	Route::post('ledgerDownload', 'ApiController@ledgerDownload');
	Route::post('addNewUser', 'ApiController@addNewMember');
	Route::post('addNewBooking', 'ApiController@AddBooking');
	Route::post('getInstallmentPlan', 'ApiController@getInstallmentPlan');
	Route::post('memberInfo', 'ApiController@getMemberinfo');

	Route::post('getAgencyByReg','ApiController@getAgency');
	Route::post('agencies', 'ApiController@getAgencies');
	Route::post('getAgencyFiles', 'ApiController@getAgencyFiles');

	Route::post('stats', "ApiController@UserStats");
	Route::post('checkUsername', "ApiController@checkUsername");
	Route::post('checkCnic', "ApiController@checkCnic");
	
// 	Batch Routes
    
    Route::post('getBatch', 'ApiController@getBatch')->name('agency.batch');	
	//Booking
	Route::post('getMemberBookings', 'BookingApiController@index');

	//Reports
	Route::post('receiptReport', 'ApiReportController@ReceiptReport');
	Route::post('batchReport', 'ApiReportController@BatchReport');
	Route::post('dueReport', 'ApiReportController@DueReport');
	Route::post('bankReport', 'ApiReportController@BankReport');
	Route::post('bookingReport', 'ApiReportController@BookingReport');
	Route::post('paidInstReport', 'ApiReportController@PaidInstReport');
	Route::post('findProperty', 'ApiController@findProperty');
	Route::get('inventory_res', 'ApiReportController@inventory_res');
	Route::get('inventory_com', 'ApiReportController@inventory_com');
	Route::get('inventory_hous', 'ApiReportController@inventory_hous');
	Route::post('cancelReport', 'ApiReportController@cancelReport');
	Route::post('cancelReport', 'ApiReportController@checkcancelReport');
    
    
    
    Route::post('due-amount-accumulated', 'ApiReportController@dueAccumulatedAmounts');
    Route::post('due-amounts-accumulated', 'ApiReportController@CheckdueAccumulatedAmounts');
    
    
    Route::post('dueAmounts', 'ApiReportController@dueAmounts');
    Route::post('CheckdueAmounts', 'ApiReportController@CheckdueAmounts');
    
    Route::post('CheckOverDueAmounts', 'ApiReportController@CheckOverDueAmounts');
    
    Route::post('PaidInstallment', 'ApiReportController@PaidInstallent');
    
    
    Route::post('recovery', 'ApiReportController@recovery');
    Route::post('getrecoveryReport', 'ApiReportController@getrecoveryReport');
    
	//Dealers
	Route::post('users', 'ApiController@LeadUsers');
	Route::post('propertyList', 'ApiController@propertyList');
	Route::post('allAgents', 'ApiController@allAgents');
	Route::post('addAgent', 'ApiController@addAgent');
	Route::post('propertyBooking', 'ApiController@propertyBooking');
	Route::post('bookingList', 'ApiController@bookingList');
	Route::post('addPrincipalOfficer', 'ApiController@addPrincipalOfficer');
	Route::post('generateLead', 'ApiController@generateLead');
	Route::post('viewGenerateLead', 'ApiController@viewGenerateLead');
	Route::post('viewGenerateLeadAgency', 'ApiController@viewGenerateLeadAgency');
	Route::post('principalOfficer', 'ApiController@principalOfficer');
    
    Route::post('saleSummary', 'ApiReportController@saleSummary');
    Route::post('customerFeedback', 'ApiReportController@customerFeedback');
    
});
