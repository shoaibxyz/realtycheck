<?php 

	Route::group(['prefix' => 'admin/HR', 'middleware' => ['auth'], 'namespace' => 'HR'], function () {

		Route::post('employee/getEmployeeByDepartment', 'EmployeesController@getEmployeeByDepartment')->name('getEmployeeByDepartment');

		Route::resource('employee', 'EmployeesController');
		
		Route::get('hrdashboard', 'EmployeesController@hrdashboard')->name('hrdashboard');
		Route::resource('department', 'DepartmentController');
		Route::resource('designation', 'DesignationController');

		Route::post('attendance/addRemarks', 'AttendanceController@addRemarks')->name('attendance.addRemarks');
		Route::get('attendance/history/{department_id?}/{employee_id?}/{from?}/{to?}', 'AttendanceController@history')->name('attendance.history');
		Route::post('attendance/storeLocation', 'AttendanceController@storeLocation')->name('attendance.storeLocation');
		Route::resource('attendance', 'AttendanceController');

		Route::get('advance-salary/check/{month?}/{year?}', 'AdvanceSalaryController@checkAdvanceSalary')->name('advance-salary.checkAdvanceSalary');
		Route::resource('advance-salary', 'AdvanceSalaryController');

		Route::get('payslip/generate/{department_id?}/{employee_id?}/{month?}/{year?}', 'PaySlipController@generatePayslip')->name('payslip.generatePayslip');
		Route::get('payslip/advance-salary', 'PaySlipController@advanceSalary')->name('payslip.advanceSalary');
		Route::get('payslip/checkPayslip/{department_id?}/{month?}/{year?}', 'PaySlipController@checkPayslip')->name('payslip.checkPayslip');
		Route::resource('payslip', 'PaySlipController');
		Route::resource('tax','TaxController');
		
		Route::get('leave-request/change-status/{leave_id}/{status}', 'LeavesRequestController@changeStatus')->name('leave-request.changeStatus');
		Route::get('leave-request/check', 'LeavesRequestController@checkRequest')->name('leave-request.check');
		Route::resource('leave-request','LeavesRequestController');
		Route::resource('leave','LeavesController');
		
		Route::get('viewsms', 'TaxController@viewsms');
		
		Route::get('employee/getemployee', 'EmployeesController@getemployee')->name('employee.getemployee');
		Route::get('employee/save/{id}',[
            'as' => 'employee.download', 'uses' => 'EmployeesController@downloadImage']);


	});