<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    /*Modules Routes*/
    Route::get('accounting/all-modules', 'Accounting\ModulesController@index')->name('all-module');
    Route::get('accounting/deleted-modules', 'Accounting\ModulesController@deleted_modules')->name('deleted-module1');
    Route::get('accounting/create-module', 'Accounting\ModulesController@create_module')->name('create-module');
    Route::get('accounting/edit-module/{id}', 'Accounting\ModulesController@edit_module')->name('edit-module');
    Route::get('accounting/select-module', 'Accounting\ModulesController@select_module');
    Route::get('accounting/getAllModules', 'Accounting\ModulesController@getAllModules')->name('getAllModules');
    Route::get('accounting/getAllDelModules', 'Accounting\ModulesController@getAllDelModules')->name('getAllDelModules');
    Route::get('accounting/delete-module/{id}', 'Accounting\ModulesController@delete');
    Route::get('accounting/restore-module/{id}', 'Accounting\ModulesController@restore');
    Route::post('accounting/create-module', 'Accounting\ModulesController@save');
    Route::post('accounting/edit-module/{id}', 'Accounting\ModulesController@update');
    Route::post('accounting/select-module', 'Accounting\ModulesController@set_module');
    /*Modules Routes*/

    /*Accounts Routes*/
    Route::get('accounting/acc-dashboard', 'Accounting\DashboardController@index')->name('acc-dashboard');
    Route::get('accounting/accounts', 'Accounting\AccountsController@index')->name('accounts.index');
    Route::get('accounting/getAllAccounts', 'Accounting\AccountsController@getAllAccounts')->name('getAllAccounts');
    /*Accounts Routes*/

    /*Groups Routes*/
    Route::get('accounting/add-group', 'Accounting\GroupsController@index')->name('add-group');
    Route::get('accounting/deleted-groups', 'Accounting\GroupsController@deleted_groups')->name('deleted-group');
    Route::get('accounting/edit-group/{id}', 'Accounting\GroupsController@edit')->name('edit-group');
    Route::get('accounting/delete-group/{id}', 'Accounting\GroupsController@delete');
    Route::get('accounting/restore-group/{id}', 'Accounting\GroupsController@restore');
    Route::get('accounting/getAllDelGroups', 'Accounting\GroupsController@getAllDelGroups')->name('getAllDelGroups');
    Route::post('accounting/add-group', 'Accounting\GroupsController@save');
    Route::post('accounting/edit-group/{id}', 'Accounting\GroupsController@update');
    /*Groups Routes*/

    /*Ledgers Routes*/
    Route::get('accounting/deleted-ledgers', 'Accounting\LedgersController@deleted_ledgers')->name('deleted-ledger');
    Route::get('accounting/add-ledger', 'Accounting\LedgersController@index')->name('add-ledger');
    Route::get('accounting/edit-ledger/{id}', 'Accounting\LedgersController@edit')->name('edit-ledger');
    Route::get('accounting/delete-ledger/{id}', 'Accounting\LedgersController@delete');
    Route::get('accounting/restore-ledger/{id}', 'Accounting\LedgersController@restore');
    Route::get('accounting/getAllDelLedgers', 'Accounting\LedgersController@getAllDelLedgers')->name('getAllDelLedgers');
    Route::get('accounting/ledger-cl', 'Accounting\LedgersController@cl');
    Route::post('accounting/add-ledger', 'Accounting\LedgersController@save');
    Route::post('accounting/edit-ledger/{id}', 'Accounting\LedgersController@update');
    /*Ledgers Routes*/

    /*Entry Types Routes*/
    Route::get('accounting/entrytypes', 'Accounting\EtypesController@index')->name('all-entrytype');
    Route::get('accounting/getAllEtypes', 'Accounting\EtypesController@getAllEtypes')->name('getAllEtypes');
    Route::get('accounting/deleted-entrytypes', 'Accounting\EtypesController@deleted_entrytypes')->name('deleted-entrytype');
    Route::get('accounting/add-entrytype', 'Accounting\EtypesController@add')->name('add-entrytype');
    Route::get('accounting/edit-entrytype/{id}', 'Accounting\EtypesController@edit')->name('edit-entrytype');
    Route::get('accounting/delete-entrytype/{id}', 'Accounting\EtypesController@delete');
    Route::get('accounting/restore-entrytype/{id}', 'Accounting\EtypesController@restore');
    Route::get('accounting/getAllDelEtypes', 'Accounting\EtypesController@getAllDelEtypes')->name('getAllDelEtypes');
    Route::post('accounting/add-entrytype', 'Accounting\EtypesController@save');
    Route::post('accounting/edit-entrytype/{id}', 'Accounting\EtypesController@update');
    /*Entry Types Routes*/

    /*Tags Routes*/
    Route::get('accounting/tags', 'Accounting\TagsController@index')->name('all-tags');
    Route::get('accounting/getAllTags', 'Accounting\TagsController@getAllTags')->name('getAllTags');
    Route::get('accounting/deleted-tags', 'Accounting\TagsController@deleted_tags')->name('deleted-tags');
    Route::get('accounting/add-tag', 'Accounting\TagsController@add')->name('add-tags');
    Route::get('accounting/edit-tag/{id}', 'Accounting\TagsController@edit')->name('edit-tags');
    Route::get('accounting/delete-tag/{id}', 'Accounting\TagsController@delete');
    Route::get('accounting/restore-tag/{id}', 'Accounting\TagsController@restore');
    Route::get('accounting/getAllDelTags', 'Accounting\TagsController@getAllDelTags')->name('getAllDelTags');
    Route::post('accounting/add-tag', 'Accounting\TagsController@save');
    Route::post('accounting/edit-tag/{id}', 'Accounting\TagsController@update');
    /*Tags Routes*/

    /*Entries Routes*/
    Route::get('accounting/entries', 'Accounting\EntriesController@index')->name('all-entry');
    Route::get('accounting/entries/{tid}', 'Accounting\EntriesController@index2');
    Route::get('accounting/getAllEntries', 'Accounting\EntriesController@getAllEntries')->name('getAllEntries');
    Route::get('accounting/deleted-entries', 'Accounting\EntriesController@deleted_entries')->name('deleted-entry');
    Route::get('accounting/view-entry/{id}', 'Accounting\EntriesController@view')->name('view-entry');
    Route::get('accounting/add-entry/{id}', 'Accounting\EntriesController@add')->name('add-entry');
    Route::get('accounting/entry-addrow', 'Accounting\EntriesController@addrow');
    Route::get('accounting/edit-entry/{id}', 'Accounting\EntriesController@edit')->name('edit-entry');
    Route::get('accounting/delete-entry/{id}', 'Accounting\EntriesController@delete');
    Route::get('accounting/restore-entry/{id}', 'Accounting\EntriesController@restore');
    Route::get('accounting/getAllDelEntries', 'Accounting\EntriesController@getAllDelEntries')->name('getAllDelEntries');
    Route::post('accounting/add-entry/{id}', 'Accounting\EntriesController@save');
    Route::post('accounting/edit-entry/{id}', 'Accounting\EntriesController@update');
    Route::get('accounting/updentrystatus', 'Accounting\EntriesController@updpstatus');
    /*Entries Routes*/

    /*Search Routes*/
    Route::get('accounting/search', 'Accounting\SearchController@index')->name('acc-search');
    Route::post('accounting/search_results', 'Accounting\SearchController@showsearch_results');
    Route::get('accounting/getSearchResults', 'Accounting\SearchController@getSearchResults')->name('getSearchResults');
    /*Search Routes*/

    /*Reports Routes*/
    Route::get('accounting/balance_sheet', 'Accounting\ReportsController@balance_sheet')->name('balance-sheet');
    Route::post('accounting/balance_sheet', 'Accounting\ReportsController@showbalance_sheet');
    Route::get('accounting/profit_loss', 'Accounting\ReportsController@profit_loss')->name('profit-loss');
    Route::post('accounting/profit_loss', 'Accounting\ReportsController@showprofit_loss');
    Route::get('accounting/trial_balance', 'Accounting\ReportsController@trial_balance')->name('trial-balance');
    Route::get('accounting/ledger_statement', 'Accounting\ReportsController@ledger_statement')->name('ledger-statement');
    Route::get('accounting/ledger_statement/{id}', 'Accounting\ReportsController@showledger_statement');
    Route::post('accounting/ledger_statement', 'Accounting\ReportsController@showledger_statement');
    Route::get('accounting/getAllLedgerStatements', 'Accounting\ReportsController@getAllLedgerStatements')->name('getAllLedgerStatements');
    Route::get('accounting/ledger_reconcile', 'Accounting\ReportsController@ledger_reconcile')->name('ledger_reconcile');
    Route::post('accounting/ledger_reconcile', 'Accounting\ReportsController@showledger_reconcile');
    Route::get('accounting/getAllLedgerReconciles', 'Accounting\ReportsController@getAllLedgerReconciles')->name('getAllLedgerReconciles');
    Route::post('accounting/ledger_reconcile2', 'Accounting\ReportsController@ledger_reconcile2');
    /*Reports Routes*/

    // Dashboard Links

    Route::get('moduledashboard', function () {
    return view('admin.accounting.module.module-dashboard');
        })->name('moduledashboard');

    Route::get('accountingdashboard', function () {
    return view('admin.accounting.acc.acc-dashboard');
        })->name('accountingdashboard');

    Route::get('tagsdashboard', function () {
    return view('admin.accounting.tags.tags-dashboard');
        })->name('tagsdashboard');

    Route::get('entrydashboard', function () {
    return view('admin.accounting.entries.entry-dashboard');
        })->name('entrydashboard');

    Route::get('entry-typedashboard', function () {
    return view('admin.accounting.entry-type.entry-typedashboard');
        })->name('entry-typedashboard');

    Route::get('reportdashboard', function () {
    return view('admin.accounting.saerch.search-dashboard');
        })->name('reportdashboard');
});