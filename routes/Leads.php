<?php
Route::group(['prefix' => 'admin', 'middleware' => ['auth'], 'namespace' => 'Leads'], function () {    

    Route::post('leads/changeStatus', 'LeadsController@changeStatus')->name('leads.changeStatus');
    Route::post('leads/getPropertySubType', 'LeadsController@getPropertySubType')->name('leads.getPropertySubType');
    Route::get('leads/show-user-leads/{user_id}', 'LeadsController@showUserLeads')->name('leads.showUserLeads');
    Route::get('leads/getLeads', 'LeadsController@getLeads')->name('leads.getLeads');
    Route::POST('leads/leadUpdate', 'LeadsController@updateLead')->name('leadUpdate');
    Route::get('leads/search', 'LeadsController@search')->name('lead.search');
    Route::get('leads/deleted', 'LeadsController@deletedLeads')->name('lead.deleted');
    Route::POST('leads/restore/{id}', 'LeadsController@restoreLeads')->name('lead.restore');    
    Route::get('leads/dashboard', 'LeadsController@dashboard')->name('leads.dashboard');
    
    Route::get('leads/details', 'LeadsController@details')->name('lead.details');
    Route::get('leads/persondetails/{id}', 'LeadsController@persondetails')->name('leads.persondetails');
    
    Route::get('leads/import', 'LeadsController@import')->name('lead.import');
    Route::post('leads/import', 'LeadsController@ImportData')->name('lead.importData');
    Route::get('leads/due-meetings', 'LeadsController@dueMeetings')->name('lead.dueMeetings');
    Route::post('leads/due-meetings', 'LeadsController@getDueMeetings')->name('leads.getDueMeetings');
    
    Route::get('leads/leadStatus/{type}', 'LeadsController@leadStatus')->name('lead.leadStatus');
    Route::get('leads/report/activity', 'LeadsController@activityReport')->name('lead.activityReport');
    Route::post('leads/report/activity/result', 'LeadsController@activityReportResult')->name('lead.activityReportResult');
    Route::post('leads/search', 'LeadsController@searchLead')->name('lead.searchLead');
    Route::get('leads/due-leads', 'LeadsController@index')->name('leads.dueLeads');
    Route::get('leads/index', 'LeadsController@index')->name('leads.index');
    Route::get('leads/create', 'LeadsController@create')->name('leads.create');
    Route::post('leads/store', 'LeadsController@store')->name('leads.store');
    Route::get('leads/edit/{id}', 'LeadsController@edit')->name('leads.edit');
    Route::PATCH('leads/update/{id}', 'LeadsController@update')->name('leads.update');
    Route::get('leads/show/{id}', 'LeadsController@show')->name('leads.show');
    Route::get('leads/view/{id}', 'LeadsController@view')->name('leads.view');
    Route::delete('leads/destroy/{id}', 'LeadsController@destroy')->name('leads.destroy');
    //Route::resource('leads', 'LeadsController');
    //Route::resource('lead/lead-sources', 'SourceController');
    Route::resource('lead-status', 'LeadStatusController');
    Route::get('user-attachment/create/{client_id}', 'PersonAttachmentController@create')->name('user-attachment.add');
    Route::resource('user-attachment', 'PersonAttachmentController');
    
    Route::get('clients/search', 'ClientsController@search')->name('clients.search');
    Route::get('clients/deleted', 'ClientsController@deletedClients')->name('clients.deleted');
    Route::POST('clients/restore/{id}', 'ClientsController@restoreLeads')->name('clients.restore');
    Route::post('clients/search', 'ClientsController@searchLead')->name('clients.searchLead');

    Route::post('clients/get-client-info', 'ClientsController@getClientInfo')->name('clients.getClientInfo');
    Route::resource('clients', 'ClientsController');

    Route::get('reminder/complete/{reminder_id}', 'LeadReminderController@complete')->name('reminder.complete');
    Route::resource('reminder', 'LeadReminderController');
    Route::get('closed/dashboard', 'LeadsController@closeddashboard')->name('closed.dashboard');
    
    // Campaigns
    
    Route::get('lead/campaign', 'CampaignController@index')->name('leads.campaign');
    Route::get('lead/campaign/create', 'CampaignController@create')->name('leads.campaign.create');
    Route::post('lead/campaign/store', 'CampaignController@store')->name('leads.campaign.store');
    Route::delete('lead/campaign/delete/{id}', 'CampaignController@destroy')->name('leads.campaign.delete');
    Route::get('lead/campaign/edit/{id}', 'CampaignController@edit')->name('leads.campaign.edit');
    Route::post('lead/campaign/update/{id}', 'CampaignController@update')->name('leads.campaign.update');
    Route::post('lead/campaign/findcampaign', 'CampaignController@findcampaign')->name('leads.campaign.findcampaign');
    
    //Route::resource('lead/projects', 'ProjectsController');
    Route::get('lead/projects', 'ProjectsController@index')->name('leads.projects');
    Route::get('lead/projects/create', 'ProjectsController@create')->name('leads.projects.create');
    Route::post('lead/projects/store', 'ProjectsController@store')->name('leads.projects.store');
    Route::delete('lead/projects/destroy/{id}', 'ProjectsController@destroy')->name('leads.projects.destroy');
    Route::get('lead/projects/edit/{id}', 'ProjectsController@edit')->name('leads.projects.edit');
    Route::post('lead/projects/update/{id}', 'ProjectsController@update')->name('leads.projects.update');
    
    Route::get('lead/sources', 'SourceController@index')->name('leads.sources');
    Route::get('lead/sources/create', 'SourceController@create')->name('leads.sources.create');
    Route::post('lead/sources/store', 'SourceController@store')->name('leads.sources.store');
    Route::delete('lead/sources/destroy/{id}', 'SourceController@destroy')->name('leads.sources.destroy');
    Route::get('lead/sources/edit/{id}', 'SourceController@edit')->name('leads.sources.edit');
    Route::post('lead/sources/update/{id}', 'SourceController@update')->name('leads.sources.update');
});
