// INIT DATATABLES
$(function () {
    // Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#filters-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter":false,
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreFilter(id)
{
    var isConfirmDelete = confirm('Are you sure you want to restore this filter?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'post',
            url: path + 'delfilter',
            data: {
                'mod': '0',
                'id': id
            },
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#filters-trash').DataTable();
                table.row('#filter-row-' + data.id).remove().draw( false );
                $('.note p').html('Image has been restored successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}

function delFilter(id)
{
    var isConfirmDelete = confirm('Are you sure you want to delete this filter permanently?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'post',
            url: path + 'delfilterp',
            data: {'id': id},
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#filters-trash').DataTable();
                table.row('#filter-row-' + data.id).remove().draw( false );
                $('.note p').html('Image has been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}
function bulkActions()
{
    var chks = [];
    $('#filters-trash tr').each(function (i, row)
    {
        var $row = $(row),
            $checkedBoxes = $row.find('input:checked');
        $checkedBoxes.each(function (i, checkbox) {
            //console.log(checkbox);
            if(checkbox.value != 'on')
                chks.push(checkbox.value);
        });
    });
    if(chks.length > 0)
    {
        $.ajax({
            type: 'post',
            url: path + 'filtersba',
            data: {
                'mod': $('#actions').val(),
                'ids': chks
            },
            success: function(data) {
                var i;
                var table = $('#filters-trash').DataTable();
                for (i = 0; i < chks.length; ++i) {
                    //$('#events-trash tr#' + chks[i]).remove();
                    table.row('#filter-row-' + chks[i]).remove().draw( false );
                }
                if($('#actions').val() == 'rs')
                    $('.note p').html('Images have been restored successfully.');
                else if($('#actions').val() == 'ds')
                    $('.note p').html('Images have been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else
        alert('Please select some row');
}