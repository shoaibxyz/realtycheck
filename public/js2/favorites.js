$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
	// Init
    var table = $('#favorites').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter":false,
    } );
});

$('#users').on("change",function (e)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getfavorites',
        data: {
            'id': $(this).val()
        },
        success: function(data) {
            $('#favorites tbody').html(data);
            $('.portlet.box').find('.loading').remove();
            convertChkRdo();
        }
    });
});

function delFavorite(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this favorite?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delfavorite',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#favorites').DataTable();
					table.row('#favorite-row-' + data.id).remove().draw( false );
					$('.note p').html('Favorite has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
					$('#users').val('');
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#favorites tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmfavorites',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#favorites').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#favorite-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Favorites have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
                    $('#users').val('');
				}
		});
	}
	else
		alert('Please select some row');
}

