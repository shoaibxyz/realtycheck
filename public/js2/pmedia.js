$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var columns = [
    { data: 'selchkbox', name: 'selchkbox', orderable: false },
    { data: 'id', name: 'id', visible: false},
    { data: 'thumb', name: 'thumb', orderable: false,className: 'col-status'},
    { data: 'is_active', name: 'is_active', orderable: true,className: 'col-status'},
    { data: 'title', name: 'title', orderable: true},
    { data: 'created_at', name: 'created_at', orderable: true},
    { data: 'updated_by', name: 'updated_by', orderable: true, className: 'hidden-col'},
    { data: 'updated_at', name: 'updated_at', orderable: true, className: 'hidden-col'},
    { data: 'btn_actions', name: 'btn_actions', orderable: false},
];

var oTable = $('#pmedia').DataTable({
    "processing": true,
    "serverSide": true,
    "bDestroy":true,
    "bFilter":false,
    "order": [[3, "asc" ]],
    ajax: {
        url: $('#hfroute').val(),
        data: function (d) {
            d.business = $('#business').val();
            d.service = $('#service').val();
            d.media_name = $('#media_name').val();
            d.city = $('#city').val();
            d.status = $('#status').val();
            d.startdate = $('#startdate').val();
            d.enddate = $('#enddate').val();
        }
    },
    "columns": columns,
    initComplete: function () {
        $('.mix-grid').mixItUp();
        convertChkRdo();
    }
});

$('#search-form').on('submit', function(e) {
    oTable.draw();
    e.preventDefault();
});

$('#businesses').on("change",function (e) {
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    var id = $(this).val();
    $.ajax({
        type: 'get',
        url: path + 'getPServices',
        data: {
            'id': id
        },
        success: function(data) {
            $('#txtProvider').val (data.provider);
            $('#category').html(data.cats);
            $('.portlet.box').find('.loading').remove();
        }
    });
})
/*$('#services').on("change",function (e) {
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    var id = $(this).val();
    $.ajax({
        type: 'get',
        url: path + 'getCats',
        data: {
            'id': id
        },
        success: function(data) {
            $('#category').html(data);
            $('.portlet.box').find('.loading').remove();
        }
    });
})*/

function changeStatus(id,st)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    if(st != 3) {
        var url = path + 'changeMStatus';
        $.ajax({
            type: 'post',
            url: url,
            data: {
                'st': st,
                'id': id
            },
            success: function (data) {
                oTable.draw();
                $('.portlet.box').find('.loading').remove();
                alert('Status changed')
            }
        });
    }
    else
    {
        $('#statusModal').modal('show');
        $('#hfmid').val(id);
        $('#hfmst').val(st);
        $('.portlet.box').find('.loading').remove();
        $('#lightbox').hide();
        $('#lightboxOverlay').hide();
        $('#templates').css('visibility','visible');
        $('#city').css('visibility','visible');
        $('#business').css('visibility','visible');
        $('#status').css('visibility','visible');
    }
    e.preventDefault();
    return false;
}

function sendMessage()
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    var url = path + 'sendRMMessage';
    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmImage').serialize(),
        success: function (data) {
            oTable.draw();
            $('#statusModal').modal('hide');
            $('.portlet.box').find('.loading').remove();
            alert('Status changed and message sent')
        }
    });
    //e.preventDefault();
    return false;
}

function saveImage()
{
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#image_id').val();
    var url = path + 'gallery';
    if (id){
        url += "/" + id;
    }

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmImage').serialize() + '&dt='+ dt_array[0] + ' ' + dt_array[1],
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                window.location.replace(data.path);
            }
        }
    });
    return false;
}

function delMedia(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this image?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmedia',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#pmedia').DataTable();
					table.row('#image-row-' + data.id).remove().draw( false );
					$('.note p').html('Gallery image has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#pmedia tr').each(function (i, row)
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmmedias',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#pmedia').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#image-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Gallery imagees have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

