// INIT DATATABLES
var columns = [
    /*{ data: 'selchkbox', name: 'selchkbox', orderable: false },*/
    { data: 'title', name: 'title', orderable: true},
    { data: 'updated_by', name: 'updated_by'},
    { data: 'updated_at', name: 'updated_at'},
    { data: 'is_active', name: 'is_active', orderable: true,className: 'col-status'},
    { data: 'btn_actions', name: 'btn_actions', orderable: false},
];

var table = $('#events').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave":true,
    "bDestroy":true,
    "bFilter":false,
    "order": [[3, "asc" ]],
    ajax: {
        url: $('#hfroute').val(),
        data: function (d) {
            d.event = $('#event').val();
        }
    },
    "columns": columns,
    initComplete: function () {
        //convertChkRdo();
    }
});
//}

$('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Event');
			$('#title').val('');
			$('#desc').val('');
			$('#event_id').val('');		
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Event');
            $.ajax({
                type: 'get',
                url: path + 'getevent',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#event_id').val(id);
                    $('#title').val(data.title);
                    $('#desc').val(data.description);
                    $('#status').val(data.is_active).trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function  assign(id) {
    $.ajax({
        type: 'get',
        url: path + 'getEventServices',
        data: {
            'id': id
        },
        success: function(data) {
            $('#assignModal .modal-body').html(data);
            convertChkRdo();
        }
    });
    $('#assevent_id').val(id);
    $('#assignModal').modal('show');
}

function saveassign()
{
    var id = $('#assevent_id').val();
    var url = path + 'saveEventServices';

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmassignServices').serialize(),
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                $('#assignModal').modal('hide');
                $('.note h4').html('Success');
                $('.note p').html('Event services have been saved successfully.');
                $('.note').show(0).delay(3000).hide(0);
            }
        }
    });
    return false;
}

function saveEvent()
{
    var id = $('#event_id').val();
    var url = path + 'events';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmEvent').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                	window.location = path + 'events';
                    if (id)
                    {
                        $('tr#event-row-' + id).replaceWith(data);
                        $('.note p').html('Event has been updated successfully.');
                    }
                    else
                    {
                        var table = $('#events').DataTable();
                        //table.rows.add(data).draw();
                        //$('#events').dataTable().fnAddData(data);
						$('#events').append(data).draw( false );
                        $('.note p').html('Event has been added successfully.');
                    }
                    convertChkRdo();
                    $('#title').val('');
                    $('#desc').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delEvent(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this event?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delevent',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#events').DataTable();
					table.row('#event-row-' + data.id).remove().draw( false );
					$('.note p').html('Event has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#events tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmevents',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#events').DataTable();
					for (i = 0; i < chks.length; ++i) {
						//$('tr#' + chks[i]).remove();							
						table.row('#event-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Events have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}