// INIT DATATABLES
$(function () {
    // Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#busreports-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter": false
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreBusreport(id)
{
    var isConfirmDelete = confirm('Are you sure you want to restore this report?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'post',
            url: path + 'delbusreport',
            data: {
                'mod': '0',
                'id': id
            },
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#busreports-trash').DataTable();
                table.row('#busreport-row-' + data.id).remove().draw( false );
                $('.note p').html('Report has been restored successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}

function delBusreport(id)
{
    var isConfirmDelete = confirm('Are you sure you want to delete this report permanently?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'post',
            url: path + 'delbusreportp',
            data: {'id': id},
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#busreports-trash').DataTable();
                table.row('#busreport-row-' + data.id).remove().draw( false );
                $('.note p').html('Report has been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}
function bulkActions()
{
    var chks = [];
    $('#busreports-trash tr').each(function (i, row)
    {
        var $row = $(row),
            $checkedBoxes = $row.find('input:checked');
        $checkedBoxes.each(function (i, checkbox) {
            //console.log(checkbox);
            if(checkbox.value != 'on')
                chks.push(checkbox.value);
        });
    });
    if(chks.length > 0)
    {
        $.ajax({
            type: 'post',
            url: path + 'busreportsba',
            data: {
                'mod': $('#actions').val(),
                'ids': chks
            },
            success: function(data) {
                var i;
                var table = $('#busreports-trash').DataTable();
                for (i = 0; i < chks.length; ++i) {
                    //$('#events-trash tr#' + chks[i]).remove();
                    table.row('#busreport-row-' + chks[i]).remove().draw( false );
                }
                if($('#actions').val() == 'rs')
                    $('.note p').html('Reports have been restored successfully.');
                else if($('#actions').val() == 'ds')
                    $('.note p').html('Reports have been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else
        alert('Please select some row');
}