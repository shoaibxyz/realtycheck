$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#pages').on('change',function(e) {
    $.ajax({
      type: "get",
      url: path + "getSEO",
      data: {
        page: $(this).val()
      },
      success: function(data) {
        /* $("#meta_title").val(data.meta_title);
        $("#meta_tags").val(data.meta_tags); */
        $("#meta_desc").val(data.tags);
        /* CKEDITOR.instances.meta_desc.setMode("source");
        CKEDITOR.instances.meta_desc.setData(data.tags); */
      }
    });
});

function saveSEO() {
  var url = path + "saveSEO";
  $.ajax({
    type: "post",
    url: url,
    data: $("#frmSEO").serialize(),
    success: function(data) {
      if (data["status"] == "e") {
        //alert(data['message']);
        var errorMsg = data["message"];
        var msgs = errorMsg.toString().replace(",", "<br>");
        showError(msgs);
      } 
      else {
        $(".note").show();
        $(".note p").html("SEO has been updated successfully.");
        $(".note h4").html("Success");
        $(".note")
          .show(0)
          .delay(3000)
          .hide(0);
      }
    }
  });
  return false;
}