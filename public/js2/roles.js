// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#roles').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //"sDom": '<"bottom"i>"bottom"li<"top"f"bottom"lp><"clear">'
        "sDom": '<"dt_head"f>rt<"F"lip>'
        /*ajax: path + "getroles",
        columns: [
			{
				name: 'chk',
				data: null,
				sortable: false,
				searchable: false,
				render: function (data) {
					var actions = '';
					actions = '<input type="checkbox" value="'+ data.id +'" />';
                    convertChkRdo();
					return actions;
				}
			},
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'is_active', 
				name: 'is_active', 
				render: function (data) {
					//console.log(data);
					var active = (data == '0') ? '<span class="label label-sm label-danger label-'+ data.is_active +'">Pending</span>' : '<span class="label label-sm label-success label-'+ data.is_active +'">Active</span>';
					return active;
				}
			},
			{
				name: 'actions',
				data: null,
				sortable: false,
				searchable: false,
				render: function (data) {
					var actions = '';
					actions = '<div class="action-btns"><button type="button" class="btn btn-default btn-xs" data-id="'+ data.id +'" onclick="toggle(\'edit\','+ data.id +')"><i class="fa fa-edit"></i>&nbsp;Edit</button>&nbsp;&nbsp;<button class="btn btn-danger btn-xs btn-delete" data-id="'+ data.id +'" onclick="delRole('+ data.id +')"><i class="fa fa-trash-o"></i>&nbsp;Delete</button></div>';
					return actions.replace(/:id/g, data.id);
				}
			}
        ]*/
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
	
	/*$("form[name='frmRole']").validate({
		// Specify validation rules
		rules: {
		  // The key name on the left side is the name attribute
		  // of an input field. Validation rules are defined
		  // on the right side
		  title: "required",
		},
		// Specify validation error messages
		messages: {
		  title: "Please enter role name",
		},
		// Make sure the form is submitted to the destination defined
		// in the "action" attribute of the form when valid
		submitHandler: function(form) {
		  //form.submit();
		  saveRole();
		  return false;
		}
	  });*/
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Role');
			$('#name').val('');
			$('#role_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Role');
			$('#role_id').val(id);
			var row = $("tr#role-row-"+id);
			$('#name').val(row.find("td:nth-child(2)").html());
			var status = row.find("td span.label").attr('data-id');
            $('#status').val(status).trigger('change');
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function  assign(id) {
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getRoleObjects',
        data: {
            'id': id
        },
        success: function(data) {
            $('#assignModal .modal-body').html(data);
            $('input').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal',
                increaseArea: '20%' // optional
            });
            $('#assrole_id').val(id);
            $('#assignModal').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

function saveassign()
{
    var id = $('#assrole_id').val();
    var url = path + 'saverights';
    $('.portlet.box').append('<div class="loading"><p>Saving please wait....</p></div>');
    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmassignObjects').serialize(),
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                $('#assignModal').modal('hide');
                $('.note h4').html('Success');
                $('.note p').html('Role rights have been saved successfully.');
                $('.note').show(0).delay(3000).hide(0);
                $('.portlet.box').find('.loading').remove();
            }
        }
    });
    return false;
}

function saveRole()
{
    var id = $('#role_id').val();
    var url = path + 'roles';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmRole').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#role-row-' + id).replaceWith(data);
                        $('.note p').html('Role has been updated successfully.');
                    }
                    else
                    {
                        $('#roles').append(data);
                        $('.note p').html('Role has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delRole(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this role?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delrole',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#roles').DataTable();
					table.row('#role-row-' + data.id).remove().draw( false );
					$('.note p').html('Role has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#roles tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmroles',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#roles').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#role-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Roles have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

