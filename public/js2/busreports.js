$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function drawTable()
{
    //alert('aaa');
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false },
        { data: 'business', name: 'business', orderable: true},
        { data: 'name', name: 'name', orderable: true},
        { data: 'email', name: 'email', orderable: true},
        { data: 'message', name: 'message', orderable: false},
        { data: 'created_at', name: 'created_at',orderable: true},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var table = $('#busreports').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [5, "desc"],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.business = $('#business').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
                d.name = $('#repname').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            convertChkRdo();
        }
    });
}

$('#search-form').on('submit', function(e) {
    drawTable();
    e.preventDefault();
    return false;
});

function delBusreport(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this report?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delbusreport',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
                    drawTable();
					$('.note p').html('Report has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#reviews tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmbusreports',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
                    drawTable();
					$('.note p').html('Reports have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

