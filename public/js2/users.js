$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var columns = [
    { data: 'selchkbox', name: 'selchkbox', orderable: false },
    { data: 'id', name: 'id', visible: false},
    { data: 'name', name: 'name', orderable: true},
    { data: 'email', name: 'email', orderable: true},
    { data: 'source', name: 'source', orderable: true},
    { data: 'created_by', name: 'created_by'},
    { data: 'created_at', name: 'created_at'},
    { data: 'is_active', name: 'is_active', orderable: true,className: 'col-status'},
    { data: 'btn_actions', name: 'btn_actions', orderable: false},
];

var oTable = $('#users').DataTable({
    "processing": true,
    "serverSide": true,
    "bDestroy":true,
    "bFilter":false,
    "order": [[7, "asc" ]],
    ajax: {
        url: $('#hfroute').val(),
        data: function (d) {
            d.name = $('#name').val();
            d.city = $('#ucity').val();
            d.startdate = $('#startdate').val();
            d.enddate = $('#enddate').val();
        }
    },
    "columns": columns,
    initComplete: function () {
        //$('.mix-grid').mixItUp();
        convertChkRdo();
    }
});

$('#search-form').on('submit', function(e) {
    oTable.draw();
    e.preventDefault();
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New User');
			$('#name').val('');
            $('#email').val('');
            $('#password').val('');
            $('#confpassword').val('');
			$('#user_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit User');
            $.ajax({
                type: 'get',
                url: path + 'getuser',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#user_id').val(id);
                    $('#uname').val(data.name);
                    $('#email').val(data.email);
                    $('#status').val(data.is_active).trigger('change');
                    $("#city option:contains(" + data.city + ")").attr('selected', 'selected').trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveUser()
{
    var id = $('#user_id').val();
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');

    var url = path + 'users';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmUser').serialize() + '&d='+ dt_array[0] +'&time='+ dt_array[1],
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#user-row-' + id).replaceWith(data);
                        $('.note p').html('User has been updated successfully.');
                    }
                    else
                    {
                        $('#users').append(data);
                        $('.note p').html('User has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#confpassword').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function regUser()
{
    $err = false;
    $cerr = false;
    $merr = false;
    if ($('#uname').val() == '')
    {
        $err = true;
    }
    //alert($err);
    if ($('#uemail').val() == '') {
        $err = true;
    }
    //alert($err);
    if ($('#su_password').val() == '') {
        $err = true;
    }
    //alert($err);
    else if ($('#su_password-confirm').val() == '') {
        $err = true;
    }
    else if ($('#su_password').val() != $('#su_password-confirm').val())
    {
        $cerr = true;
    }
    //alert($err);
    if($('#ot').val() == 'v')
    {
        if ($('#package').val() == '') {
            $err = true;
        }
        if ($('#pcity').val() == '') {
            $err = true;
        }
        if (!isNumeric($('#mobile').val()))
        {
            $merr = true;
        }
    }
    //alert($merr);
    if ($err || $cerr || $merr)
    {
        var err_txt = '';
        if ($err)
            err_txt = 'Please provide required fields.<br>';
        if ($cerr)
            err_txt += 'Password and confirm password are not same.<br>';
        if ($merr)
            err_txt += 'Mobile number must be numeric.';
        //alert('Please provide required fields.');
        showError(err_txt);
    }
    else
    {
        $('#suModal').append('<div class="loading"><p>Loading please wait....</p></div>');
        var currentTime = new Date();
        var dt_array = currentTime.toLocaleString().split(',');

        var url = path + 'reguser';

        $.ajax({
            type: 'post',
            url: url,
            data: $('#frmSU').serialize() + '&d='+ dt_array[0] +'&time='+ dt_array[1],
            success: function(data) {
                //alert(data);
                if (data.error) {
                    //alert(data.error);
                    var errorMsg = data.error;
                    var msgs = errorMsg.toString().replace(',','<br>');
                    //alert(msgs);
                    showError(msgs);
                    $('#suModal').find('.loading').remove();
                }
                else {
                    window.location = APP_URL + data.url;
                    return true;
                }
            }
        });
    }
    return false;
}

function isNumeric(input) {
    var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
    var regex = RegExp(number);
    return regex.test(input) && input.length > 0;
}

function login()
{
    $('#siModal').append('<div class="loading"><p>Logging in please wait....</p></div>');
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');

    var url = path + 'postlogin';

    $.ajax({
        type: 'post',
        url: url,
        data: {
            'd' : dt_array[0],
            'time' : dt_array[1],
            'email' : $('#lemail').val(),
            'password' : $('#lpassword').val(),
            'remember' : $('#remember').val()
        },
        success: function(data) {
            //alert(data);
            if (data.error) {
                //alert(data.error);
                var errorMsg = data.error;
                var msgs = errorMsg.toString().replace(',','<br>');
                //alert(msgs);
                $('#siModal').find('.loading').remove();
                showError(msgs);
            }
            else {
                //alert(data.url);
                window.location = APP_URL + data.url;
                return true;
            }
        }
    });
    return false;
}

function delUser(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this user?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deluser',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#users').DataTable();
					table.row('#user-row-' + data.id).remove().draw( false );
					$('.note p').html('User has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#users tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmusers',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#users').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#user-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Users have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

