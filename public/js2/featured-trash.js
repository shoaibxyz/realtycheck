// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#featured-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter":false,
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreFeatured(id,tid)
{
	/*var isConfirmDelete = confirm('Are you sure you want to restore this featured?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delfeatured',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					var table = $('#featured-trash').DataTable();
					table.row('#featured-row-' + id).remove().draw( false );
					$('.note p').html('Featured has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }*/
    swal({
		title: "Are you sure you want to restore ad?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, restore it!",
		cancelButtonText: "No, cancel please!",
		closeOnConfirm: false,
		closeOnCancel: true
	},
	function(isConfirm){
		if (isConfirm) {
            $.ajax({
                type: 'get',
                url: path + 'deletead',
                data: {
                    'm': '0',
                    'adid': id,
                    'tid': tid
                },
                success: function(data) {
                    var table = $('#featured-trash').DataTable();
                    table.row('#featured-row-' + id).remove().draw( false );
                    swal("Restored!", "Your ad has been restored.", "success");
                }
            });
		}
	});
}

function delFeatured(id,tid)
{
	/*var isConfirmDelete = confirm('Are you sure you want to delete this featured permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delfeaturedp',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#featured-trash').DataTable();
					table.row('#featured-row-' + id).remove().draw( false );
					$('.note p').html('Featured has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }*/
    swal({
		title: "Are you sure you want to delete ad permanently?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel please!",
		closeOnConfirm: false,
		closeOnCancel: true
	},
	function(isConfirm){
		if (isConfirm) {
            $.ajax({
                type: 'post',
                url: path + 'deladp',
                data: {'id': id,
                    'tid': tid},
                success: function(data) {
                    //$('tr#' + data.id).remove();
                    var table = $('#featured-trash').DataTable();
                    table.row('#featured-row-' + id).remove().draw( false );
                    swal("Deleted!", "Your ad has been deleted permanently.", "success");
                    /*$('.note p').html('Featured has been deleted successfully.');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);*/
                }
            });
		}
	});

}
function bulkActions(tid)
{
	var chks = [];
	$('#featured-trash tr').each(function (i, row)
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'adsba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks,
				'tid': tid
			},
			success: function(data) {
				if($('#actions').val() == 'rs')
                    swal("Restored!", "Your ads have been restored successfully.", "success");
					//$('.note p').html('Featureds have been restored successfully.');
				else if($('#actions').val() == 'ds')
                    swal("Deleted!", "Your ads have been deleted permanently.", "success");
                var i;
                var table = $('#featured-trash').DataTable();
                for (i = 0; i < chks.length; ++i) {
                    table.row('#featured-row-' + chks[i]).remove().draw( false );
                }
					//$('.note p').html('Featureds have been deleted successfully.');
				/*$('.note h4').html('Success');
				$('.note').show(0).delay(3000).hide(0);*/
			}
		});
	}
	else
        swal({ title: "", text: 'Please select some row',type: "info"});
		//alert('Please select some row');
}