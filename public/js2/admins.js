// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#admins').dataTable( );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Admin');
			$('#name').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#role').val('');
            $('#password').val('');
            $('#confpassword').val('');
			$('#admin_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Admin');
			$('#admin_id').val(id);
            /*$.ajax({
                type: 'get',
                url: path + 'getAdmin',
                data: {
                    'id': id
                },
                success: function(data) {
                    console.log(data['roles']);
                }
            });*/
			var row = $("tr#admin-row-"+id);
			$('#name').val(row.find("td:nth-child(2)").html());
            $('#phone').val(row.find("td:nth-child(4)").html());
            $('#email').val(row.find("td:nth-child(5)").html());
            $("#role option").each(function() {
                if($(this).text() == row.find("td:nth-child(6)").html()) {
                    $(this).attr('selected', 'selected');
                }
            });
			var status = row.find("td:nth-child(6)").find('span').html();
			$('#status option[text="Pending"]').attr("selected", "selected");
			$("#status option").filter(function() {
				return this.text == status; 
			}).attr('selected', true);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveAdmin()
{
    $('#myModal').append('<div class="loading"><p>Saving please wait....</p></div>');
    var id = $('#admin_id').val();
    var url = path + 'admins';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmAdmin').serialize(),
            success: function(data) {
                //alert(data.error);
                if(data.error)
                {
                    var $errs = '';
                    for(var i=0;i<data.error.length;i++)
                        $errs += data.error[i] + '\n';
                    alert($errs);
                }
                else
                {
                    window.location = path + 'admins';
                    if (id)
                    {
                        $('tr#admin-row-' + id).replaceWith(data);
                        $('.note p').html('Admin has been updated successfully.');
                    }
                    else
                    {
                        $('#admins').append(data);
                        $('.note p').html('Admin has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#role').val('');
                    $('#password').val('');
                    $('#confpassword').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
                $('#myModal').find('.loading').remove();
            }
        });
    return false;
}

function delAdmin(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this admin?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deladmin',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#admins').DataTable();
					table.row('#admin-row-' + data.id).remove().draw( false );
					$('.note p').html('Admin has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#admins tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmadmins',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#admins').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#admin-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Admins have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

