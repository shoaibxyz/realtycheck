// INIT DATATABLES
$(function () {
	// Init
    var table = $('#objects').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Object');
			$('#name').val('');
			$('#object_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Object');
			$('#object_id').val(id);
			var row = $("tr#object-row-"+id);
			$('#name').val(row.find("td:nth-child(2)").html());
            var status = row.find("td span.label").attr('data-id');
            $('#status').val(status).trigger('change');
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveObject()
{
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#object_id').val();
    var url = path + 'objects';
    if (id){
            url += "/" + id;
    }

    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmObject').serialize() + '&d='+ dt_array[0] +'&time='+ dt_array[1],
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#object-row-' + id).replaceWith(data);
                        $('.note p').html('Object has been updated successfully.');
                    }
                    else
                    {
                        $('#objects').append(data);
                        $('.note p').html('Object has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delObject(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this object?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delobject',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#objects').DataTable();
					table.row('#object-row-' + data.id).remove().draw( false );
					$('.note p').html('Object has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#objects tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmobjects',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#objects').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#object-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Objects have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}