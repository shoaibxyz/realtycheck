$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Category');
			$('#cat_name').val('');
			$('#category_id').val('');
			$('#services').val('').trigger('change');
            $('#service').val('');
            $('#servRow').hide();
            break;
        case 'edit':
            $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
            $.ajax({
                type: 'get',
                url: path + 'getcategory',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#myModalLabel').html('Edit Category');
                    $('#category_id').val(id);
                    $('#cat_name').val(data.name);
                    $('#services').val(data.p_service_id).trigger('change');
                    $('#status').val(data.is_active).trigger('change');

                    $('.portlet.box').find('.loading').remove();
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

$('#services').on("click",function (e) {
    $.ajax({
        type: 'get',
        url: path + 'getbusservice',
        data: {
            'id': $(this).val()
        },
        success: function(data) {
            $('#service').val(data.service);
            $('#servRow').show();
            $( "#cat_name" ).autocomplete({
                source: data.cats
            });
        }
    });
});

function editCategory(id)
{
    $('.form-group').removeClass('has-error');
    $('#myModalLabel').html('Edit Category');
    $('#category_id').val(id);
    var row = $("tr#category-row-"+id);
    $('#name').val(row.find("td:nth-child(1)").html());
    $('#services').val(row.find("td:nth-child(4) #hfsid").val()).trigger('change');
    $('#myModal').modal('show');
}

function saveCategory()
{
    var id = $('#category_id').val();
    var url = path + 'categories';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmCategory').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    //alert(data['message']);
                    var errorMsg = data['message'];
                    var msgs = errorMsg.toString().replace(',','<br>');
                    showError(msgs);
                }
                else
                {
                    //window.location = data;
                    if (id)
                    {
                        //$('tr#category-row-' + id).replaceWith(data);
                        $('.note p').html('Category has been updated successfully.');
                    }
                    else
                    {
                        //$('#categories').append(data);
                        $('.note p').html('Category has been added successfully.');
                    }
                    drawTable();
                    //convertChkRdo();
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delCategory(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this category?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delcategory',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#categories').DataTable();
					table.row('#category-row-' + data.id).remove().draw( false );
					$('.note p').html('Category has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#categories tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmcategories',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#categories').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#category-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Categories have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

function drawTable()
{
    //alert('aaa');
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false, className: 'chl-all' },
        { data: 'id', name: 'id', visible: false},
        { data: 'name', name: 'name', orderable: true},
        { data: 'title', name: 'title', orderable: true},
        { data: 'created_by', name: 'created_by',className: 'creat-by'},
        { data: 'created_at', name: 'created_at',className: 'creat-at'},
        { data: 'updated_by', name: 'updated_by',className: 'updt-by'},
        { data: 'updated_at', name: 'updated_at',className: 'updt-at'},
        { data: 'is_active', name: 'is_active',className: 'col-status'},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var table = $('#categories').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [[8, "asc" ]],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.sid = $('#hfsid').val();
                d.name = $('#name').val();
                d.business = $('#business').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal',
                increaseArea: '20%' // optional
            });
            fillBusinesses($('#hfsid').val());
        }
    });
}

function fillBusinesses(sid)
{
    $.ajax({
        type: 'get',
        url: path + 'getservbus',
        data: {
            'sid': sid
        },
        success: function(data) {
            $("#business").select2();
            /*$('#business').html('');
            $('#business').append(data);
            $('#business').val('');*/
            var output = '<option value="">Select Business</option>';

            $.each(data, function(key, val) {
                output += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $("#business").html(output);

        }
    });
}

$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    $(this).addClass('active');
    var sid = $(this).attr('data-filter');
    $('#hfsid').val(sid);
    drawTable();
    //fillBusinesses(sid);
});

$('#search-form').on('submit', function(e) {
    drawTable();
    e.preventDefault();
    return false;
});