$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
  }
});

function initCropper(){
    //console.log("Came here")
    var image = document.getElementById('imagetoCrop');
    var $w = 528;
    var $h = 480;


    var cropper = new Cropper(image, {
        aspectRatio: '1.13',
        viewMode: 2,
        minCropBoxWidth:$w,
        minCropBoxHeight:$h,
        guides: false,
        crop: function(e) {
            console.log(e.detail.width);
            //$('#width').val(Math.round(e.detail.width,0));
            console.log(e.detail.height);
            //$('#height').val(Math.round(e.detail.height,0));
            $('#cropModal').find('.loading').remove();
        }
    });

    // On crop button clicked
    document.getElementById('crop_button').addEventListener('click', function(){

        $('#cropModal').append('<div class="loading"><p>Cropping please wait....</p></div>');
        var imgurl = cropper.getCroppedCanvas().toDataURL();
        var img = document.createElement("img");
        img.src = imgurl;
        /* ----------------	SEND IMAGE TO THE SERVER-------------------------*/

        cropper.getCroppedCanvas().toBlob(function (blob) {
            var formData = new FormData();
            formData.append('croppedImage', blob);
            var filepath = $("#filepath").val();
            formData.append("filepaths", filepath);
            // Use `jQuery.ajax` method
            $.ajax(path.replace('/admin','') + 'api/upload_media2', {
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    var fname = data.filename;
                    var image = '<div class="gthumbs"><img class="gthumb" src="' + APP_URL + TMPIP + '/' + fname + '"><a href="javascript:void(0);" onclick=removeImage("' + fname + '",1)><img src="' + APP_URL +'/public/img/x.png" class="img_delete" alt=""></a></div>';
                    $('#files_list').html(image);
                    $('#filepath').val(fname);
                    $('#cropModal').modal('hide');
                    //cropper.destroy();
                    $('#cropModal').find('.loading').remove();
                },
                error: function () {
                    console.log('Upload error');
                }
            });
        });
        //}
    })
    $('#cropModal').on('hide.bs.modal', function (e) {
        //alert('a');
        cropper.destroy();
    })
    cropper.destroy();
}

function removeImage(file, t) {
    $.ajax({
        type: 'get',
        url: path + 'removepsimage',
        data: {
            'file': file
        },
        success: function (data) {
            if (t == 1) {
                $('#files_list').html('');
                $('#filepath').val('');
            }
            else if (t == 2) {
                $('#cf-files_list').html('');
                $('#clsfied_filepath').val('');
            }
            else if (t == 3) {
                $('#logo-files_list').html('');
                $('#logo_filepath').val('');
            }
        }
    });
}

function viewService(id)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getpservice',
        data: {
            'id': id
        },
        success: function(data) {
            $('#businessModal .modal-body').html(data["str"]);
            $('#businessModal').modal('show');
            $('.portlet.box').find('.loading').remove();
            $('#rate').rating({displayOnly: true, step: 0.5});
            $('.mix-grid').mixItUp();
        }
    });
}

function delService(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this business?');
    if (isConfirmDelete)
	{
        //alert(path);
		$.ajax({
				type: 'get',
                url: path + 'delpservice',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
                    //alert(data);
					var table = $('#services').DataTable();
					table.row('#service-row-' + data.id).remove().draw( false );
					$('.note p').html('Business has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
        });
        return false;
	} 
	else {
    	return false;
    }
}

function delAllSelected(){
	var chks = [];
	$('#services tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmpservices',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#services').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#service-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Businesses have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

/*$(document).on('ready', function()
{

});*/

function getServicesLists(sid)
{
    $('#hfservices').val(sid);
    $('#services-lists').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getServicesLists',
        data: {
            'sid': sid,
            'cid': $('#city.selectpicker').selectpicker('val'),
            'eid': $('#event.selectpicker').selectpicker('val'),
        },
        success: function(data) {
            $('#myTabContent .services_row').html(data.lists);
            if(data.featured == 0) {
                $('.featured_row').hide().slick('slickRemove');
            }
            else {
                $('.featured_row').show();
                //('#myTabContent .featured_row').html('');
                $('#myTabContent .featured_row').html(data.flists);
            }

			if(data.filters) {
                $('#service_filters').html('');
                $('#service_filters').append(data.filters).find('input.form-control-cb').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    increaseArea: '20%' // optional
                });
                $('input.form-control-cb').on('ifChecked', function(event){
                    getFilteredLists('');
                });

                $('input.form-control-cb').on('ifUnchecked', function(event){
                    getFilteredLists('');
                });
                $('#service_filters').show();
                //convertChkRdo();
            }
        	else {
                $('#service_filters').html('');
                $('#service_filters').hide();
            }

            $('#myTab li').removeClass('active');
            $('#myTab li#list-'+sid).addClass('active');
            $('#services-lists').find('.loading').remove();
            $('.psrrate').rating({displayOnly: true, step: 0.5});
            $('#filter_class').val('');
            $('#frate').rating('clear');
            $('.brates').rating({
                step: 1,
                showClear: false,
                showCaption: false,
                displayOnly: true
            }).on('rating.change', function(event, value, caption) {
            });
        }
    });
}

function addtoFav($psid,$uid)
{
    if($uid == 0)
    {
        swal({
            title: "Error!",
            text: "Please login first in order to proceed.",
            type: "error",
            confirmButtonText: "OK"
        });
        showSI();
    }
    else
    {
        $.ajax({
            type: 'post',
            url: path + 'addtofav',
            data: {
                'uid': $uid,
                'psid': $psid
            },
            success: function(data) {
                if(data == -1)
                    swal({
                        title: "Error!",
                        text: "You have already added this business to your favorites list.",
                        type: "error",
                        confirmButtonText: "OK"
                    });
                else
                    swal("Added!", "Business has been added to your favorites list.", "success");
            }
        });
    }
}

function sendReport(){
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    $.ajax({
        type: 'post',
        url: path + 'addbreport',
        data: $('#frmReport').serialize() + '&dt='+ dt_array[0] + ' ' + dt_array[1],
        success: function(data) {
            if(data.status==1)
                alert(data.error);
            else {
                swal("Confirmation!", "The business has been reported.", "success");
                $('#reportModal').modal('hide');
            }
        }
    });
    return false;
}

$(".timepicker-default").timepicker();
/* $("#mobile-phone").mask("999-9999999");
$("#uan-phone").mask("999-999-999"); */

$("#phone_type").on("change", function (e) {
    if ($("#phone_type").val() == 1) {
        $("#mobile-bg").hide();
        $("#uan-bg").hide();
        $("#landline-bg").css("display", "inline-block");
        $("#landline-phone").val("");
    } else if ($("#phone_type").val() == 2) {
        $("#mobile-bg").css("display", "inline-block");
        $("#landline-bg").hide();
        $("#uan-bg").hide();
        $("#mobile-phone").val("");
    } else if ($("#phone_type").val() == 3) {
        $("#uan-bg").css("display", "inline-block");
        $("#landline-bg").hide();
        $("#mobile-bg").hide();
        $("#uan-phone").val("");
    }
});

var geocoder;
var map = new google.maps.Map(document.getElementById("map_canvas"), {
    zoom: 14,
    center: new google.maps.LatLng(31.567, 74.356),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    zoomControl: true
});
geocoder = new google.maps.Geocoder();
var geolocate;
var myMarker;

myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(31.567, 74.356),
    map: map,
    draggable: true
});

google.maps.event.addListener(myMarker, "dragend", function (evt) {
    $("#latitude").val(evt.latLng.lat());
    $("#longitude").val(evt.latLng.lng());
});

$("#city").on("change", function (e) {
    var phone_code = $("#" + $(this).attr("id") + " option:selected").attr(
        "data-id"
    );
    $("#landline-bg label span").html(phone_code);
    if ($(this).val() != "" || $("#address").val() != "") {
        var city = $("#" + $(this).attr("id") + " option:selected").text();
        var address = "";
        if ($("#address").val() != "")
            var address = $("#address").val() + " " + city;
        else var address = city;
        //alert(address);
        geocoder.geocode({ address: address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                    map.setCenter(results[0].geometry.location);
                    myMarker.setPosition(results[0].geometry.location);
                }
            }
        });
    }
});

$("#address").on("blur", function (e) {
    var city = $("#city option:selected").text();
    if ($(this).val() != "" || city != "") {
        var address = "";
        if ($("#city").val() != "") var address = $(this).val() + " " + city;
        else var address = $(this).val();
        //alert(address);
        geocoder.geocode({ address: address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                    map.setCenter(results[0].geometry.location);
                    myMarker.setPosition(results[0].geometry.location);
                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                }
            }
        });
    }
});

$("#services").on("change", function (e) {
    $.ajax({
        type: "get",
        url: path + "getservfilters",
        data: {
            id: $(this).val()
        },
        success: function (data) {
            $("#filters").html(data.rfilters);
            $("#filters2").html(data.nrfilters);
            $("#ddBranches").select2("val", "");
            $("#ddBranches").html(data.branches);
            convertChkRdo();
        }
    });
});

$("#services2").on("change", function (e) {
    loadMedia($(this).val());
});

function loadMedia(id) {
    $("#mediaModal").append(
        '<div class="loading"><p>Loading please wait....</p></div>'
    );
    $.ajax({
        type: "get",
        url: path + "getmedia",
        data: {
            id: id
        },
        success: function (data) {
            $("div.media-thumbs").html(data.rows);
            $(".mix-grid").mixItUp();
            //alert($('#mediaModal').find('.loading'));
            $("#mediaModal")
                .find(".loading")
                .remove();
        }
    });
}

function showmediaPop() {
    if ($("#services").val()) {
        $("#services2").val($("#services").val());
        loadMedia($("#services").val());
    }
    $("#mediaModal").modal("show");
}

$("#submit").on("click", function (e) {
    e.preventDefault();
    var count = 0;
    var count2 = 0;
    $(".error_detail").html('');
    $(".error_detail").hide();
    $(".form-control").each(function () {
        if ($(this).hasClass("validate")) {            
            if (
                ($(this).val() == "" || $(this).val() == null) &&
                $(this)
                    .attr("id")
                    .substr(0, 5) != "s2id_" &&
                ($(this)
                    .parent()
                    .css("display") == "block" ||
                    $(this)
                        .parent()
                        .css("display") == "inline-block" ||
                    $(this)
                        .parent()
                        .css("display") == "table")
            ) {
                $(this)
                    .parent()
                    .addClass("has-error");
                count = parseInt(count) + 1;
            }
            else
                $(this)
                    .parent()
                    .removeClass("has-error");
        }
        if ($(this).hasClass("email_validate")) {
            if ($(this).val() != "" && !isValidEmail($(this).val())) {
                $(this)
                    .parent()
                    .addClass("has-error");
                $(this)
                    .parent()
                    .append('<div class="error_detail">- Email address is invalid.</div>');
                count = parseInt(count) + 1;
            } else {
                if (!$(this).hasClass("validate")) {
                    $(this)
                        .parent()
                        .removeClass("has-error");
                }
                $(this)
                    .parent()
                    .find(".error_detail")
                    .remove();
            }
        }
        if ($(this).hasClass("num_validate")) {
            if ($(this).val() != "" && !isNumeric($(this).val())) {
                $(this)
                    .parent()
                    .addClass("has-error");
                $(this)
                    .parent()
                    .parent()
                    .append('<div class="error_detail">- Number Must be numeric.</div>');
                count = parseInt(count) + 1;
            }
            else {
                //alert('a');
                //if (!$(this).hasClass("num_validate")) {
                /* $(this)
                  .parent()
                  .removeClass("has-error");    */
                //}
                /* $(this)
                  .parent()
                  .parent()
                  .find(".error_detail")
                  .remove(); */
            }
        }
        if ($("#phone_type").val() == 2 && $(this).hasClass("validate_mobile")) {
            if ($(this).val() != "" && !isMobile($(this).val())) {
                $(this)
                    .parent()
                    .addClass("has-error");
                $(this)
                    .parent()
                    .parent()
                    .append('<div class="error_detail">- Mobile number must be in format (301-4012423).</div>');
                count = parseInt(count) + 1;
            }
            else {
                //alert("aa");
                //if ($(this).hasClass("validate_mobile")) {
                /* $(this)
                  .parent()
                  .removeClass("has-error"); */
                //}
                /* $(this)
                  .parent()
                  .parent()
                  .find(".error_detail")
                  .remove(); */
            }
        }
        if ($("#phone_type").val() == 3 && $(this).hasClass("validate_uan")) {
            if ($(this).val() != "" && !isUAN($(this).val())) {
                $(this)
                    .parent()
                    .addClass("has-error");
                $(this)
                    .parent()
                    .parent()
                    .append('<div class="error_detail">- UAN number must be in format (111-000-111).</div>');
                count = parseInt(count) + 1;
                /* $(".errors").css("display", "block");
                $(".errors").html('<span class="error_detail"></span>'); */
                //alert('mobile validate');
            } else {
                //alert('aaaa');
                /* if (!$(this).hasClass("validate_uan")) { */
                /* $(this)
                  .parent()
                  .removeClass("has-error"); */
                /* }
                $(this)
                  .parent()
                  .parent()
                  .find(".error_detail")
                  .remove(); */
            }
        }
    });

    //alert(count);
    if (count > 0) {
        alert("Please correct validation errors.");
        return false;
    } else {
        $(".portlet.box").append(
            '<div class="loading"><p>Loading please wait....</p></div>'
        );
        /* var editor_data = CKEDITOR.instances.desc.getData(); */
        $("#hfDesc").val(CKEDITOR.instances.desc.getData());
        /* $("#hf_tags").val(CKEDITOR.instances.meta_desc.getData()); */
        //alert(CKEDITOR.instances.desc.getData());
        $.ajax({
          type: "post",
          dataType: "json",
          url: path + "validatebusiness",
          data: $("#frmService").serialize() + "&description=" + CKEDITOR.instances.desc.getData(),
          success: function(data) {
            //alert(data);
            if (data.error) {
              var errorMsg = data.error;
              var msgs = errorMsg.toString().replace(",", "<br>");
              showError(msgs);
              $(".portlet.box")
                .find(".loading")
                .remove();
            } else {
              $(".portlet.box")
                .find(".loading")
                .remove();
              window.location = path + data.url;
              return true;
            }
          }
        });       
    }
    return false;
});

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(",")[0].indexOf("base64") >= 0)
        byteString = atob(dataURI.split(",")[1]);
    else byteString = unescape(dataURI.split(",")[1]);

    // separate out the mime component
    var mimeString = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
}


$("#desc").on("keydown, keyup", function (e) {
    var old_length = $(this).val().length;
    $(".desc_chars").html(1500 - old_length + " characters left");
});

function isNumeric(input) {
    var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
    var regex = RegExp(number);
    return regex.test(input) && input.length > 0;
}

function isMobile(input) {
    var number = /^\(?(\d{3})\)?([-]+)(\d{7})$/;
    var regex = RegExp(number);
    return regex.test(input);
}

function isUAN(input) {
    var number = /\(?([0-9]{3})\)?([-]+)([0-9]{3})\2([0-9]{3})/;
    var regex = RegExp(number);
    return regex.test(input);
}

function isValidEmail(input) {
    var number = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var regex = RegExp(number);
    return regex.test(input);
}

$("#thumb").fileupload({
    dataType: "json",
    add: function (e, data) {
        $("#loading").text("Uploading...");
        data.submit();
    },
    done: function (e, data) {
        if (data.result.error) alert(data.result.error);
        else {
            var fname = data.result.filename;
            fname = fname.replace("\\", "");
            $("#filepath").val(fname);
            $("#loading").text("");

            $("#imagetoCrop").attr("src", MAINPATH2 + TMPIP + "/" + fname);

            setTimeout(initCropper, 1000);

            $("#cropModal").append(
                '<div class="loading"><p>Loading please wait....</p></div>'
            );
            //$('#mediaModal').modal('show');
            $("#cropModal").modal({
                backdrop: "static",
                keyboard: false,
                show: true
            });
        }
    }
});

$('#blogo').fileupload({
    dataType: 'json',
    add: function (e, data) {
        $('#logo-loading').text('Uploading...');
        data.submit();
    },
    done: function (e, data) {
        if (data.result.error)
            alert(data.result.error);
        else {
            var fname = data.result.filename;
            fname = fname.replace("\\", '');
            var image = '<div class="gthumbs"><img class="gthumb" src="' + APP_URL + TMPIP + '/' + fname + '"><a href="javascript:void(0);" onclick=removeImage("' + fname + '",3)><img src="' + APP_URL + 'public/img/x.png" class="img_delete" alt=""></a></div>';
            $('#logo-files_list').html(image);
            $('#logo_filepath').val(fname);
        }
        $('#logo-loading').text('');
    }
});

$('#adImage').fileupload({
    dataType: 'json',
    add: function (e, data) {
        $('#cf-loading').text('Uploading...');
        data.submit();
    },
    done: function (e, data) {
        if (data.result.error)
            alert(data.result.error);
        else {
            var fname = data.result.filename;
            fname = fname.replace("\\", '');
            var image = '<div class="gthumbs"><img class="gthumb" src="' + APP_URL + TMPIP + "/" + fname + '"><a href="javascript:void(0);" onclick=removeImage("' + fname + '",2)><img src="' + APP_URL + 'public/img/x.png" class="img_delete" alt=""></a></div>';
            $('#cf-files_list').html(image);
            $('#clsfied_filepath').val(fname);
        }
        $('#cf-loading').text('');
    }
});

function copyImage(file, id) {
    $('#mediaModal').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'copypsimage',
        data: {
            'file': file,
            'filepath': $('#filepath').val(),
            'id': id
        },
        success: function (data) {
            if (data['status'] == 'e') {
                alert(data['message']);
            }
            else {
                var fname = data.filename;
                var image = '<div class="gthumbs"><img id="' + fname + '" class="gthumb" src="' + APP_URL + TMPIP + "/" + fname + '"><a href="javascript:void(0);" onclick=removeImage("' + fname + '",1)><img src="' + APP_URL + 'public/img/x.png" class="img_delete" alt=""></a></div>';
                $('#files_list').html(image);
                $('#filepath').val(fname);
                $('#mediaModal').modal('hide');
                $('#myModal').modal('show');
                $('#mediaModal').find('.loading').remove();
            }
        }
    });
}

function showImport() {
    $("#importModal").modal('show');
    return false;
}