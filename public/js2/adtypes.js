// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#adtypes').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Ad Type');
			$('#name').val('');
			$('#adtype_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Ad Type');
            $.ajax({
                type: 'get',
                url: path + 'getadtype',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#adtype_id').val(id);
                    $('#name').val(data.name);
                    $('#status').val(data.is_active).trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function editAdType(id)
{
    $('.form-group').removeClass('has-error');
    $('#myModalLabel').html('Edit AdType');
    $('#adtype_id').val(id);
    var row = $("tr#adtype-row-"+id);
    $('#name').val(row.find("td:nth-child(2)").html());
    $('#myModal').modal('show');
}

function saveAdType()
{
    var id = $('#adtype_id').val();
    var url = path + 'adtypes';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmAdType').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = path + 'adtypes';
                    if (id)
                    {
                        $('tr#adtype-row-' + id).replaceWith(data);
                        $('.note p').html('Ad Type has been updated successfully.');
                    }
                    else
                    {
                        var table = $('#adtypes').DataTable();
                        $('#adtypes').append(data);
                        $('.note p').html('Ad Type has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#min_budget_land').val('');
                    $('#min_budget_reg').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delAdType(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this ad type?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deladtype',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#adtypes').DataTable();
					table.row('#adtype-row-' + data.id).remove().draw( false );
					$('.note p').html('Ad Type has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#adtypes tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmadtypes',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#adtypes').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#adtype-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Ad Types have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}