// INIT DATATABLES
$(function () {
    // Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#faqs-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter":false,
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreFaq(id)
{
    var isConfirmDelete = confirm('Are you sure you want to restore this faq?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'get',
            url: path + 'delfaq',
            data: {
                'mod': '0',
                'id': id
            },
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#faqs-trash').DataTable();
                table.row('#faq-row-' + data.id).remove().draw( false );
                $('.note p').html('Faq has been restored successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}

function delFaq(id)
{
    var isConfirmDelete = confirm('Are you sure you want to delete this faq permanently?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'post',
            url: path + 'delfaqp',
            data: {'mod': '1','id': id},
            success: function(data) {
                //$('tr#' + data.id).remove();
                var table = $('#faqs-trash').DataTable();
                table.row('#faq-row-' + data.id).remove().draw( false );
                $('.note p').html('Faq has been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}
function bulkActions()
{
    var chks = [];
    $('#faqs-trash tr').each(function (i, row)
    {
        var $row = $(row),
            $checkedBoxes = $row.find('input:checked');
        $checkedBoxes.each(function (i, checkbox) {
            //console.log(checkbox);
            if(checkbox.value != 'on')
                chks.push(checkbox.value);
        });
    });
    if(chks.length > 0)
    {
        $.ajax({
            type: 'post',
            url: path + 'faqsba',
            data: {
                'mod': $('#actions').val(),
                'ids': chks
            },
            success: function(data) {
                var i;
                var table = $('#faqs-trash').DataTable();
                for (i = 0; i < chks.length; ++i) {
                    //$('#events-trash tr#' + chks[i]).remove();
                    table.row('#faq-row-' + chks[i]).remove().draw( false );
                }
                if($('#actions').val() == 'rs')
                    $('.note p').html('Faqs have been restored successfully.');
                else if($('#actions').val() == 'ds')
                    $('.note p').html('Faqs have been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else
        alert('Please select some row');
}