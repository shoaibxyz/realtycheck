// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#events-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "bFilter":false,
        "sDom": '<"dt_head"f>rt<"F"lip>'
        /*"ajax": path + "getdelevents",
        columns: [
			{
				name: 'chk',
				data: null,
				sortable: false,
				searchable: false,
				render: function (data) {
					var actions = '';
					actions = '<input type="checkbox" value="'+ data.id +'" />';
                    convertChkRdo();
					return actions;
				}
			},
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title' },
            { data: 'description', name: 'description' },
            { data: 'is_active', 
				name: 'is_active', 
				render: function (data) {
					//console.log(data);
					var active = (data == '0') ? '<span class="label label-sm label-danger label-'+ data.is_active +'">Pending</span>' : '<span class="label label-sm label-success label-'+ data.is_active +'">Active</span>';
					return active;
				}
			},
			{
				name: 'actions',
				data: null,
				sortable: false,
				searchable: false,
				render: function (data) {
					var actions = '';
					actions = '<div class="action-btns"><button type="button" class="btn btn-default btn-xs" data-id="'+ data.id +'" onclick="restoreEvent('+ data.id +')"><i class="fa fa-edit"></i>&nbsp;Restore</button>&nbsp;&nbsp;<button class="btn btn-danger btn-xs btn-delete" data-id="'+ data.id +'" onclick="delEvent('+ data.id +')"><i class="fa fa-trash-o"></i>&nbsp;Delete</button></div>';
					return actions.replace(/:id/g, data.id);
				}
			}
        ]*/
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreEvent(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this event?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delevent',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					//$('tr#' + data.id).remove().draw(false);
					var table = $('#events-trash').DataTable();
					table.row('#event-row-' + data.id).remove().draw( false );
					$('.note p').html('Event has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delEvent(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this event permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deleventp',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#events-trash').DataTable();
					table.row('#event-row-' + data.id).remove().draw( false );
					$('.note p').html('Event has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function bulkActions()
{
	var chks = [];
	$('#events-trash tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'eventsba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#events-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					//$('#events-trash tr#' + chks[i]).remove();	
					table.row('#event-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Events have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Events have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
			}
		});
	}
	else
		alert('Please select some row');
}