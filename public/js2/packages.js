// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#packages').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Package');
			$('#name').val('');
            $('#no_services').val('');
            $('#no_images').val('');
            $('#no_branches').val('');
            $('#price').val('');
            $('#order').val('');
			$('#package_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Package');
            $.ajax({
                type: 'get',
                url: path + 'getpackage',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#package_id').val(id);
                    $('#name').val(data.name);
                    $('#no_images').val(data.no_images);
                    $('#no_services').val(data.no_services);
                    $('#no_branches').val(data.no_branches);
                    $('#price').val(data.price);
                    $('#order').val(data.order);
                    $('#status').val(data.is_active).trigger('change');
                }
            });
			/*$('#name').val(row.find("td:nth-child(1)").html());
            $('#no_images').val(row.find("td:nth-child(2)").html());
            $('#no_services').val(row.find("td:nth-child(3)").html());
            $('#no_branches').val(row.find("td:nth-child(4)").html());
            $('#price').val(row.find("td:nth-child(5)").html().replace('Rs. ',''));
            var status = row.find("td span.label").attr('data-id');
            $('#status').val(status).trigger('change');*/
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function savePackage()
{
    var id = $('#package_id').val();
    var url = path + 'packages';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmPackage').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#package-row-' + id).replaceWith(data);
                        $('.note p').html('Package has been updated successfully.');
                    }
                    else
                    {
                        $('#packages').append(data);
                        $('.note p').html('Package has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#no_services').val('');
                    $('#no_images').val('');
                    $('#no_branches').val('');
                    $('#price').val('');
                    $('#order').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delPackage(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this package?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delpackage',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#packages').DataTable();
					table.row('#package-row-' + data.id).remove().draw( false );
					$('.note p').html('Package has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#packages tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmpackages',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#packages').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#package-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Packages have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}