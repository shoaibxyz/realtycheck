// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#objects-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
		"bFilter": false
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreObject(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this object?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delobject',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					var table = $('#objects-trash').DataTable();
					table.row('#object-row-' + id).remove().draw( false );
					$('.note p').html('Object has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delObject(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this object permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delobjectp',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#objects-trash').DataTable();
					table.row('#object-row-' + id).remove().draw( false );
					$('.note p').html('Object has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function bulkActions()
{
	var chks = [];
	$('#objects-trash tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'objectsba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#objects-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					table.row('#object-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Objects have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Objects have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
			}
		});
	}
	else
		alert('Please select some row');
}