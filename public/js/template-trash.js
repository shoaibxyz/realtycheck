// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#templates-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreTemplate(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this template?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deltemplate',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					var table = $('#templates-trash').DataTable();
					table.row('#template-row-' + id).remove().draw( false );
					$('.note p').html('Template has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delTemplate(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this template permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deltemplatep',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#templates-trash').DataTable();
					table.row('#template-row-' + id).remove().draw( false );
					$('.note p').html('Template has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function bulkActions()
{
	var chks = [];
	$('#templates-trash tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'templatesba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#templates-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					table.row('#template-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Templates have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Templates have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
			}
		});
	}
	else
		alert('Please select some row');
}