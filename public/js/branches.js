$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
	// Init
    var table = $('#branches').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
});

function viewBranch(id)
{
    $.ajax({
        type: 'get',
        url: path + 'getbranch',
        data: {
            'id': id
        },
        success: function(data) {
            $('#branchModal .modal-body').html(data);
            $('#branchModal').modal('show');
        }
    });
}

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Branch');
            $('#name').val('');
            $('#phone').val('');
            $('#profile_link').val('');
            $('#branch_id').val('');
            $('#files_list').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Branch');
            $('#branch_id').val(id);
            var row = $("tr#branch-row-"+id);
            $('#name').val(row.find("td:nth-child(2)").html());
            var provider = row.find("td:nth-child(3)").html();
            //$("#providers").val(row.find("td:nth-child(3)").html());
            $("#providers option").filter(function() {
                return this.text == provider;
            }).attr('selected', true);
            $('#phone').val(row.find("td:nth-child(4)").html());
            $('#profile_link').val(row.find("td:nth-child(5)").html());
            var status = row.find("td:nth-child(6)").find('span').html();
            $('#status option[text="Pending"]').attr("selected", "selected");
            $("#status option").filter(function() {
                return this.text == status;
            }).attr('selected', true);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function editBranch(id)
{
    $.ajax({
        type: 'get',
        url: path + 'editbranch',
        data: {
            'id': id
        },
        success: function(data) {
            $('#myModalLabel').html('Edit Branch');
            $('#branch_id').val(id);
            $('#name').val(data.name);
            $('#phone').val(data.phone);
            $('#profile_link').val(data.profile_link);
            $('#myModal').modal('show');
        }
    });
}

function saveBranch()
{
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#branch_id').val();
    var url = path + 'branches';
    if (id){
        url += "/" + id;
    }

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmBranch').serialize() + '&dt='+ dt_array[0] + ' ' + dt_array[1],
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                window.location = data;
                if (id)
                {
                    $('tr#branch-row-' + id).replaceWith(data);
                    $('.note p').html('Branch has been updated successfully.');
                }
                else
                {
                    $('#branches').append(data);
                    $('.note p').html('Branch has been added successfully.');
                }
                convertChkRdo();
                $('#name').val('');
                $('#phone').val('');
                $('#profile_link').val('');
                $('#providers').val('');
                $('#myModal').modal('hide');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        }
    });
    return false;
}

function delBranch(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this branch?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delbranch',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#branches').DataTable();
					table.row('#branch-row-' + data.id).remove().draw( false );
					$('.note p').html('Branch has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#branches tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmbranches',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#branches').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#branch-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Branches have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

