// INIT DATATABLES
$(function () {
	// Init
    var table = $('#featured1').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
	
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Featured');
			$('#start_date').val('');
            $('#end_date').val('');
            $('#min_budget').val('');
            $('#max_budget').val('');
            $('#message').html('');
			$('#featured_id').val('');
            break;
        case 'edit':
            $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
            $.ajax({
                type: 'get',
                url: path + 'getfeatured',
                data: {
                    'id': id
                },
                success: function(data) {
                    //alert(data[0].id);
                    $('#myModalLabel').html('Edit Featured');
                    $('#start_date').val(data[0].start_date);
                    $('#end_date').val(data[0].end_date);
                    $('#min_budget').val(data[0].min_budget);
                    $('#max_budget').val(data[0].max_budget);
                    $('#message').html(data[0].message);
                    $('#services').val(data[0].p_service_id);
                    $('#status').val(data[0].is_active);
                    $('.portlet.box').find('.loading').remove();
                }
            });

			$('#featured_id').val(id);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveFeatured()
{
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#featured_id').val();
    var url = path + 'featured';
    if (id){
            url += "/" + id;
    }

    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmFeatured').serialize() + '&d='+ dt_array[0] +'&time='+ dt_array[1],
            success: function(data) {
                if(data['status'] == 'e')
                {
                    var message = data['message'];
                    var msgs = Array.from(message);
                    //msgs = message.split(',');
                    //alert(msgs[0]);
                    var msg = '';
                    for(var i=0;i<msgs.length;i++)
                        msg += msgs[i] + '\n';
                    //alert(msg);
                    //bootbox.alert(msg);
                    //swal({"Error", msg, confirmButtonColor: "#DD6B55"});
                    swal({ title: "", text: msg, type: "error"});
                }
                else
                {
                    if (id)
                    {
                        $('tr#featured-row-' + id).replaceWith(data);
                        $('.note p').html('Featured has been updated successfully.');
                    }
                    else
                    {
                        $('#featured1').append(data);
                        $('.note p').html('Featured has been added successfully.');
                    }
                    convertChkRdo();
                    $('#start_date').val('');
                    $('#end_date').val('');
                    $('#min_budget').val('');
                    $('#max_budget').val('');
                    $('#is_active').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delFeatured(id)
{
	/*var isConfirmDelete = confirm('Are you sure you want to delete this featured?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delfeatured',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#featured').DataTable();
					table.row('#featured-row-' + data.id).remove().draw( false );
					/!*$('.note p').html('Featured has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);*!/
				}
		});
	} 
	else {
    	return false;
    }*/
    swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type: 'post',
                    url: path + 'delfeatured',
                    data: {
                        'mod': '1',
                        'id': id
                    },
                    success: function(data) {
                        var table = $('#featured1').DataTable();
                        table.row('#featured-row-' + data.id).remove().draw( false );
                        swal("Deleted!", "Your service has been deleted.", "success");
                    }
                });
            }
        });
}
function delAllSelected(){
	var chks = [];
	$('#featured1 tr').each(function (i, row)
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmfeatured',
				data: {
						'mod': '1',
                        'ids': chks
				},
				success: function(data) {
                    swal("Deleted!", "Your featured services have been deleted successfully.", "success");
					var i;
					var table = $('#featured1').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#featured-row-' + chks[i]).remove().draw( false );
					}
					/*$('.note p').html('Featureds have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);*/
				}
		});
	}
	else
        swal({ title: "", text: 'Please select some row',type: "info"});
		//alert('Please select some row');
}