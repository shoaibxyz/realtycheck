/*function drawTable()
{*/
    //alert('aaa');
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false },
        { data: 'id', name: 'id', visible: false},
        { data: 'provider', name: 'provider', orderable: true},
        { data: 'package', name: 'package', orderable: true},
        { data: 'city_id', name: 'city_id', orderable: true},
        { data: 'email', name: 'email', orderable: true},
        { data: 'is_active', name: 'is_active', orderable: true,className: 'col-status'},
        { data: 'created_at', name: 'created_at', orderable: true},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var oTable = $('#providers').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [[6, "asc" ]],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.vendor = $('#vendor').val();
                d.city = $('#city').val();
                d.package = $('#package').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            //$('.mix-grid').mixItUp();
            convertChkRdo();
        }
    });
//}

$('#search-form').on('submit', function(e) {
    //var table = $('#providers').DataTable();
    oTable.draw();
    e.preventDefault();
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#providerModalLabel').html('Add New Vendor');
            $('#frmProvider').find("input[type=text],input[type=hidden], textarea, select").val("");
            break;
        case 'edit':
            $('#providerModalLabel').html('Edit Vendor');
            $.ajax({
                type: 'get',
                url: path + 'getprovider',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#provider_id').val(id);
                    var row = $("tr#provider-row-"+id);
                    $('#name').val(row.find("td:nth-child(2)").html());
                    var status = row.find("td:nth-child(3)").find('span').html();
                    $("#status option").filter(function() {
                        return this.text == status;
                    }).attr('selected', true);
                }
            });
            break;
        default:
            break;
    }
    $('#providerModal').modal('show');
}

function saveProvider()
{
    var id = $('#provider_id').val();
    var url = path + 'providers';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmProvider').serialize(),
            success: function(response) {
                if(response.responseJSON.error)
                {
                    alert(response.responseJSON.error);
                }
                else
                {
                    if (id)
                    {
                        $('tr#provider-row-' + id).replaceWith(response);
                        $('.note p').html('Vendor has been updated successfully.');
                    }
                    else
                    {
                        $('#providers').append(response);
                        $('.note p').html('Vendor has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#providerModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function viewProvider(id)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getprovider',
        data: {
            'id': id
        },
        success: function(data) {
            $('#providerModal .modal-body').html(data);
            $('#providerModal').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

$('#frmProvider button').on('click', function (e)
{
    var count=0;
    var comp1 = '';
    var comp2 = '';
    $(':input', $('#frmProvider').id).each(function() {
        if($(this).hasClass('validate'))
        {
            if($(this).val() == '' && $(this).parent().css('display') == 'block')
            {
                $(this).parent().addClass('has-error');
                count = parseInt(count) + 1;
            }
            else
                $(this).parent().removeClass('has-error');
        }
        if($(this).hasClass('email_validate'))
        {
            if($(this).val()!= '' && !isValidEmail($(this).val()))
            {
                $(this).parent().addClass('has-error');
                $(this).parent().append('<span class="error_detail">Email address is invalid.</div>');
                count = parseInt(count) + 1;
            }
            else
            {
                if(!$(this).hasClass('validate'))
                {
                    $(this).parent().removeClass('has-error');
                }
                $(this).parent().find('.error_detail').remove();
            }
        }
    });
    //alert(count);
    if(count > 0)
    {
        alert('Please correct validation errors.');
        return false;
    }
    else {
        //alert('come here');
        $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
        $.ajax({
            type: 'post',
            url: path + 'validatevendor',
            data: $('#frmProvider').serialize() + '&btn='+ $(this).val(),
            success: function(data) {
                if(data.error)
                {
                    //alert(data.error);
                    var errorMsg = data.error;
                    var msgs = errorMsg.toString().replace(',','<br>');
                    showError(msgs);
                    $('.portlet.box').find('.loading').remove();
                }
                else
                {
                    $('.portlet.box').find('.loading').remove();
                    //$('#frmProvider').submit();
                    window.location = path + data.url;
                    return true;
                }
            }
        });

    }
    return false;
})

function isValidEmail(input) {
    var number = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var regex = RegExp(number);
    return regex.test(input);
}

function delProvider(id)
{
    var isConfirmDelete = confirm('Are you sure you want to delete this Vendor?');
    //alert(path);
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'get',
				url: path + 'delprovider',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
                    //alert(data);
					var table = $('#providers').DataTable();
					table.row('#provider-row-' + data.id).remove().draw( false );
					$('.note p').html('Vendor has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#providers tr').each(function (i, row)
	{
		var $row = $(row),
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmproviders',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#providers').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#provider-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Vendors have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
					$('#actions').val('');
					$('.checkall').attr('checked','checked');
				}
		});
	}
	else
		alert('Please select some row');
}