$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    //$('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $(this).addClass('active');
    var sid = $(this).attr('data-filter');
    $('#hfsid').val(sid);
    drawTable();
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*$('.type').on('change',function(e){
   var id = $(this).attr('data-id');
   //alert(id);
   $('#def_options_' + id).show();
});*/

function showOptions(rowId)
{
    $('#def_options_' + rowId).show();
}

//$('.addmore').on('click',function(e){
function showmoreOption(rowId)
{
    //var rowId = $(this).attr('data-id');
    //alert($(this).attr('data-id'));
    $('#def_options_'+ rowId).show();
    var lastElem = $('#def_options_'+ rowId + ' .def_options:last').attr('data-id');
    //alert(lastElem);
    var lidArray = lastElem.split('_');
    //var nr = lidArray[0];
    var nid = (parseInt(lidArray[1])+1);
    var nr = rowId;
    var newElem = '<div id="def_options_'+ nr +'-'+ nid +'" data-id="'+ nr +'_'+ nid +'" class="form-group def_options"><label for="inputEmail3" class="col-sm-3 control-label">Option '+ nid +'</label><div class="col-sm-8"><input type="text" class="form-control" name="option_'+ nr +'-'+ nid +'" id="öption_'+ nr +'-'+ nid +'"></div></div>';
    $('#def_options_'+ rowId).append(newElem);
    //alert(newElem);
}

//$('.addquestion').on('click',function(e){
function showRow(did)
{
   //var did = $(this).attr('data-id');
   //alert(did);
   var nid = parseInt(did) + 1;
   //alert(nid);
   var nr = '<div id="row-'+nid+'" data-id="'+nid+'">';
    nr += '<div class="form-group">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label">Filter <span class="required">*</span></label>';
    nr += '<div class="col-sm-8">';
    nr += '<input type="text" class="form-control" name="filter_'+nid+'" id="filter_'+nid+'" required>';
    nr += '</div>';
    nr += '</div>';
    nr += '<div class="form-group">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label">View Format <span class="required">*</span></label>';
    nr += '<div class="col-sm-8">';
    nr += '<select id="type_'+nid+'" data-id="'+nid+'" class="form-control type" name="type_'+nid+'" onchange="showOptions('+nid+')" required>';
    nr += '<option value="">Select View Format</option>';
    nr += '<option value="D">Checkbox</option>';
    nr += '<option value="M">Dropdown</option>';
    nr += '</select>';
    nr += '</div>';
    nr += '</div>';
    nr += '<div id="def_options_'+nid+'" style="display: none;">';
    nr += '<div id="def_options_'+nid+'-1" data-id="'+nid+'_1" class="form-group def_options">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label">Option 1</label>';
    nr += '<div class="col-sm-8">';
    nr += '<input type="text" class="form-control" name="option_'+nid+'-1" id="öption_'+nid+'-1">';
    nr += '</div>';
    nr += '</div>';
    nr += '<div id="def_options_'+nid+'-2" data-id="'+nid+'_2" class="form-group def_options">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label">Option 2</label>';
    nr += '<div class="col-sm-8">';
    nr += '<input type="text" class="form-control" name="option_'+nid+'-2" id="öption_'+nid+'-2">';
    nr += '</div>';
    nr += '</div>';
    nr += '</div>';
    nr += '<div class="form-group">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label"></label>';
    nr += '<div class="col-sm-8">';
    nr += '<a id="addmore_'+nid+'" class="addmore" data-id="'+nid+'" onclick="showmoreOption('+nid+')" style="color: #4444dd;">+ Add more options</a>';
    nr += '</div>';
    nr += '</div>';
    nr += '<div class="form-group">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label">Show in Filter Search</label>';
    nr += '<div class="col-sm-8">';
    nr += '<input type="checkbox" name="in_search_'+nid+'" id="in_search_'+nid+'" style="margin-top:7px;">';
    nr += '</div>';
    nr += '</div>';
    nr += '</div>';
    nr += '<div class="row-line"></div>';
    nr += '<div class="form-group">';
    nr += '<label for="inputEmail3" class="col-sm-3 control-label"></label>';
    nr += '<div class="col-sm-8 align-right" style="text-align: right">';
    nr += '<a id="addquestion_'+nid+'" class="addquestion" data-id="'+nid+'" onclick="showRow('+nid+')" style="color: #f2994b;">+ Add New Question</a>';
    nr += '</div>';
    nr += '</div>';
    $('#frows').val(nid);
    $('.addquestion').hide();
    //$('.addquestion:last').show();
    $('#qrows').append(nr);
}

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Filter');
			$('#name').val('');
			$('#filter_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Filter');
			$('#filter_id').val(id);
			var row = $("tr#filter-row-"+id);
			$('#name').val(row.find("td:nth-child(2)").html());
			var status = row.find("td:nth-child(3)").find('span').html();
			$('#status option[text="Pending"]').attr("selected", "selected");
			$("#status option").filter(function() {
				return this.text == status; 
			}).attr('selected', true);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

$('#options').on("change",function(e){
    e.preventDefault();
	//alert($(e.target).val());
	if($(e.target).val() == 'filter')
	{
		$('#row_filter').hide();
        $('#row_sfilter').hide();
        $('#row_value').hide();
        $('#row_type').hide();
        $('#row_name').show();
        $('#row_insearch').hide();
	}
	else if($(e.target).val() == 'sub-filter')
    {
        $('#row_filter').show();
        $('#row_sfilter').hide();
        $('#row_value').hide();
        $('#row_name').show();
        $('#row_type').show();
        $('#row_insearch').show();
    }
	else if($(e.target).val() == 'value')
	{
        $('#row_filter').show();
        $('#row_sfilter').show();
        $('#row_value').show();
        $('#row_name').hide();
        $('#row_type').hide();
        $('#row_insearch').hide();
	}
})

$('#filters').on("change",function(e){
    e.preventDefault();
    //alert($(e.target).val());
    $.ajax({
        type: 'get',
        url: path + 'getsfilters',
        data: {
            'id': $(e.target).val()
        },
        success: function(data) {
            $('#sfilters').html(data);
        }
    });
})

function saveFilter()
{
    var id = $('#filter_id').val();
    var url = path + 'filters';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmFilter').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    if (id)
                    {
                        $('tr#filter-row-' + id).replaceWith(data);
                        $('.note p').html('Filter has been updated successfully.');
                    }
                    else
                    {
                        $('#filters').append(data);
                        $('.note p').html('Filter has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delFilter(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this filter?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delfilter',
				data: {
						  'mod': 1,
						  'id': id
				},
				success: function(data) {
                    drawTable();
                    $('.note p').html('Filter has been deleted successfully.');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#filters tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmfilters',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
                    drawTable();
					$('.note p').html('Filters have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}