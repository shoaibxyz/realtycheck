// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#services-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
		"bFilter": false
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreservice(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this business?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delservice',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					var table = $('#services-trash').DataTable();
					table.row('#service-row-' + data.id).remove().draw( false );
					$('.note p').html('Business has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delservice(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this business permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delservicep',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#services-trash').DataTable();
					table.row('#service-row-' + data.id).remove().draw( false );
					$('.note p').html('Business has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function bulkActions()
{
	var chks = [];
	$('#services-trash tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'servicesba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#services-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					table.row('#service-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Businesses have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Businesses have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
			}
		});
	}
	else
		alert('Please select some row');
}