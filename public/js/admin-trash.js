// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#admins-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
		"bFilter": false
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreAdmin(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this admin?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deladmin',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#admins-trash').DataTable();
					table.row('#admin-row-' + id).remove().draw( false );
					$('.note p').html('Admin has been restored successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delAdmin(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this admin permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deladminp',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#admins-trash').DataTable();
					table.row('#admin-row-' + id).remove().draw( false );
					$('.note p').html('Admin has been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function bulkActions()
{
	var chks = [];
	$('#admins-trash tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'adminsba',
			data: {
					'mod': $('#actions').val(),
				  	'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#admins-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					table.row('#admin-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Admins have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Admins have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
			}
		});
	}
	else
		alert('Please select some row');
}