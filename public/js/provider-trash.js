// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#providers-trash').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>',
        "bFilter":false
    } );
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function restoreProvider(id)
{
	var isConfirmDelete = confirm('Are you sure you want to restore this provider?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delprovider',
				data: {
						  'mod': '0',
						  'id': id
				},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#providers-trash').DataTable();
					table.row('#provider-row-' + id).remove().draw( false );
					$('.note p').html('Provider has been restored successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else {
    	return false;
    }
}

function delProvider(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this provider permanently?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delproviderp',
				data: {'id': id},
				success: function(data) {
					//$('tr#' + data.id).remove();
					var table = $('#providers-trash').DataTable();
					table.row('#provider-row-' + id).remove().draw( false );
					$('.note p').html('Provider has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else {
    	return false;
    }
}
function bulkActions()
{
	var chks = [];
	$('#providers-trash tr').each(function (i, row)
	{
		var $row = $(row),
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			//console.log(checkbox);
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});
	});
	if(chks.length > 0)
	{
		$.ajax({
			type: 'post',
			url: path + 'providersba',
			data: {
					'mod': $('#actions').val(),
					  'ids': chks
			},
			success: function(data) {
				var i;
				var table = $('#providers-trash').DataTable();
				for (i = 0; i < chks.length; ++i) {
					//$('#events-trash tr#' + chks[i]).remove();
					table.row('#provider-row-' + chks[i]).remove().draw( false );
				}
				if($('#actions').val() == 'rs')
					$('.note p').html('Providers have been restored successfully.');
				else if($('#actions').val() == 'ds')
					$('.note p').html('Providers have been deleted successfully.');
				$('.note h4').html('Success');					
				$('.note').show(0).delay(3000).hide(0);
                $('#actions').val('');
                $('.checkall').attr('checked','checked');
			}
		});
	}
	else
		alert('Please select some row');
}