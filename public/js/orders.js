$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function delOrder(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this order?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delorder',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#orders').DataTable();
					table.row('#order-row-' + data.id).remove().draw( false );
					$('.note p').html('Order has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#orders tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmorders',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#orders').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#order-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Orders have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}