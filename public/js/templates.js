// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#templates').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
	
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Template');
			$('#title').val('');
			$('#template_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Template');
			$('#template_id').val(id);
            $.ajax({
                type: 'get',
                url: path + 'getTemplate',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#title').val(data.title);
                    $('#form_name').val(data.form_name).trigger('change');
                    /*$("#form_name option").filter(function() {
                        return this.value == data.form_name;
                    }).attr('selected', true);*/
                    $('#form_status').val(data.form_status).trigger('change');
                    $('#body').val(data.body);
                    $('#status').val(data.is_active).trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveTemplate()
{
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#template_id').val();
    var url = path + 'templates';
    if (id){
            url += "/" + id;
    }

    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmTemplate').serialize() + '&d='+ dt_array[0] +'&time='+ dt_array[1],
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#template-row-' + id).replaceWith(data);
                        $('.note p').html('Template has been updated successfully.');
                    }
                    else
                    {
                        $('#templates').append(data);
                        $('.note p').html('Template has been added successfully.');
                    }
                    convertChkRdo();
                    $('#title').val('');
                    $('#body').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delTemplate(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this template?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'deltemplate',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#templates').DataTable();
					table.row('#template-row-' + data.id).remove().draw( false );
					$('.note p').html('Template has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#templates tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmtemplates',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#templates').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#template-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Templates have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}