$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*$(function () {
    // Init
    var table = $('#faqs').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );
});*/

function drawTable(sid)
{
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false },
        { data: 'question', name: 'question'},
        { data: 'is_active', name: 'is_active',className: 'col-status'},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var table = $('#faqs').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [[2, "asc" ]],
        ajax: {
            url: $('#hfroute2').val(),
            data: function (d) {
                d.id = $('#hfsid').val();
            }
        },
        "columns": columns
    });
}

$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    //$('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $(this).addClass('active');
    var sid = $(this).attr('data-filter');
    $('#hfsid').val(sid);
    drawTable(sid);
});

function loadFaqs(id)
{
    $('.faqs').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getsfaqs',
        data: {
            'id': id
        },
        success: function(data) {
            //alert(data);
            $('#faq-accordion').html(data);
            $('.faqs').find('.loading').remove();
            $('.faq-services li').removeClass('active');
            $('#faq-item_'+id).addClass('active');
            $('#hfsid').val(id);
        }
    });
}

function saveFaq()
{
    var id = $('#faq_id').val();
    var url = path + 'faqs';
    if (id){
        url += "/" + id;
    }
    //alert(url);

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmFaq').serialize(),
        success: function(response) {
            if(response.responseJSON.error)
            {
                alert(response.responseJSON.error);
            }
            else
            {
                if (id)
                {
                    $('tr#faq-row-' + id).replaceWith(response);
                    $('.note p').html('Faq has been updated successfully.');
                }
                else
                {
                    $('#faqs').append(response);
                    $('.note p').html('Faq has been added successfully.');
                }
                convertChkRdo();
                $('#name').val('');
                $('#faqModal').modal('hide');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        }
    });
    return false;
}

function saveUserFaq()
{
    var url = path + 'ask-question';

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmFAQ').serialize(),
        success: function(response) {
            if(response.error)
            {
                alert(response.error);
            }
            else
            {

                window.location = response.url;
            }
        }
    });
    return false;
}

function viewFaq(id)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getfaq',
        data: {
            'id': id
        },
        success: function(data) {
            $('.viewmodel .modal-body').html(data);
            $('.viewmodel').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

function delFaq(id)
{
    var isConfirmDelete = confirm('Are you sure you want to delete this faq?');
    if (isConfirmDelete)
    {
        $.ajax({
            type: 'get',
            url: path + 'delfaq',
            data: {
                'mod': '1',
                'id': id
            },
            success: function(data) {
                var table = $('#faqs').DataTable();
                table.row('#faq-row-' + data.id).remove().draw( false );
                $('.note p').html('Faq has been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else {
        return false;
    }
}
function delAllSelected(){
    var chks = [];
    $('#faqs tr').each(function (i, row)
    {
        var $row = $(row),
            $checkedBoxes = $row.find('input:checked');
        $checkedBoxes.each(function (i, checkbox) {
            if(checkbox.value != 'on')
                chks.push(checkbox.value);
        });

    });
    if(chks.length > 0)
    {
        $.ajax({
            type: 'post',
            url: path + 'delmfaqs',
            data: {
                'mod': '1',
                'ids': chks
            },
            success: function(data) {
                var i;
                var table = $('#faqs').DataTable();
                for (i = 0; i < chks.length; ++i) {
                    table.row('#faq-row-' + chks[i]).remove().draw( false );
                }
                $('.note p').html('Faqs have been deleted successfully.');
                $('.note h4').html('Success');
                $('.note').show(0).delay(3000).hide(0);
            }
        });
    }
    else
        alert('Please select some row');
}

