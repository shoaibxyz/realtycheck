$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*$(function () {
	// Init
    var table = $('#images').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
});*/

/*$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $(this).addClass('active');
    var id = $(this).attr('data-filter');
    $.ajax({
        type: 'get',
        url: path + 'getgallery',
        data: {
            'id': id
        },
        success: function(data) {
            //alert(data.rows);
            //var table = $('#images').DataTable();
            //table.row('.mix-grid').remove().draw( true );
            $('#images tbody').html(data.rows);
            $('#images').DataTable();
            $('.mix-grid').mixItUp();
            convertChkRdo();
            $('.portlet.box').find('.loading').remove();
        }
    });
})*/

function drawTable(sid)
{
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false },
        { data: 'thumb', name: 'thumb', orderable: false,className: 'col-status'},
        { data: 'updated_by', name: 'created_by'},
        { data: 'updated_at', name: 'created_at'},
        { data: 'is_active', name: 'is_active',className: 'col-status'},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var table = $('#images').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [[4, "asc" ]],
        ajax: {
            url: $('#hfroute2').val(),
            data: function (d) {
                d.id = $('#hfsid').val();
                d.createdby = $('#createdby').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            $('.mix-grid').mixItUp();
            convertChkRdo();
        }
    });
}

/*$('#search-form').on('submit', function(e) {
    oTable.draw();
    e.preventDefault();
});*/

$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    //$('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $(this).addClass('active');
    var sid = $(this).attr('data-filter');
    $('#hfsid').val(sid);
    drawTable(sid);
});

function showGallery(id)
{
    $.ajax({
        type: 'get',
        url: path + 'getgallery',
        data: {
            'id': id
        },
        success: function(data) {
            //alert(data.rows);
            var table = $('#images').DataTable();
            table.row('.mix-grid').remove().draw( false );
            $('#images tbody').html(data.rows);
            $('.mix-grid').mixItUp();
            convertChkRdo();
        }
    });
}

function toggle(modalstate, id, sid)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Image');
            $('#image_id').val('');
            $('#status').val(1);
            $sid = '';
            $("ul.list-filter li").each(function() {
                if($(this).hasClass('active')) {
                    $sid = $(this).attr('data-filter');
                }
            });
            if($sid) {
                $('#services').val($sid);
            }
            $('#files_list').html('');
            $('#filepaths').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Image');
            $('#image_id').val(id);
            var row = $("tr#image-row-"+id);
            $('#services').val(sid);
            var status = row.find("td:nth-child(3)").find('span').html();
            $('#status option[text="Pending"]').attr("selected", "selected");
            $("#status option").filter(function() {
                return this.text == status;
            }).attr('selected', true);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveImage()
{
    $('#myModal').append('<div class="loading"><p>Submitting please wait....</p></div>');
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    //alert(dt_array[1]);
    var id = $('#image_id').val();
    var url = path + 'gallery';
    if (id){
        url += "/" + id;
    }

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmImage').serialize() + '&dt='+ dt_array[0] + ' ' + dt_array[1],
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                //window.location.replace(data.path);
                drawTable($('#services').val());
                $('#myModal').find('.loading').remove();
                $('#myModal').modal('hide');
            }
        }
    });
    return false;
}

function delImage(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this image?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delimage',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#images').DataTable();
                    table.row('#image-row-' + data.id).remove().draw( false );
                    $('.note p').html('Image has been deleted successfully.');
                    $('.note h4').html('Success');
                    //$('.note').show(0)
                    $('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#images tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmimages',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#images').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#image-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Imagees have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

