$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function drawTable() {
    var columns = [
        {data: 'selchkbox', name: 'selchkbox', orderable: false},
        {data: 'email', name: 'email', orderable: true},
        {data: 'created_at', name: 'created_at', orderable: true},
        {data: 'is_active', name: 'is_active', orderable: true, className: 'col-status'},
        {data: 'btn_actions', name: 'btn_actions', orderable: false}
    ];

    var table = $('#subscribers').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy": true,
        "bFilter": false,
        "order": [[3, "asc"]],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.email = $('#subcemail').val();
                d.city = $('#subccity').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            convertChkRdo();
        }
    });
}

$('#search-form').on('submit', function(e) {
    drawTable();
    e.preventDefault();
    return false;
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalSubscribe').html('Add New Subscriber');
			$('#email').val('');
			$('#subscriber_id').val('');
            break;
        case 'edit':
            $('#myModalSubscribe').html('Edit Subscriber');
            $.ajax({
                type: 'get',
                url: path + 'getSubscriber',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#subscriber_id').val(id);
                    $('#email').val(data.email);
                    $('#status').val(data.is_active).trigger('change');
                    $("#city option:contains(" + data.city + ")").attr('selected', 'selected').trigger('change');
                    //$('#city').val(data.city).trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveSubscriber()
{
    var id = $('#subscriber_id').val();
    //alert(id);
    var url = path + 'subscribers';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmSubscribe').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = path + 'subscribers';
                    //var table = $('#subscribers').DataTable();
                    /*if (id)
                    {
                        //$('tr#subscriber-row-' + id).replaceWith(data);
                        $('.note p').html('Subscriber has been updated successfully.');
                    }
                    else
                    {
                        //$('#subscribers tbody').append(data);
                        $('.note p').html('Subscriber has been added successfully.');
                    }
                    drawTable();
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);*/
                }
            }
        });
    return false;
}

function delSubscriber(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this Subscriber?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delsubscriber',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#subscribers').DataTable();
					table.row('#subscriber-row-' + data.id).remove().draw( false );
					$('.note p').html('Subscriber has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}

function delAllSelected(){
	var chks = [];
	$('#subscribers tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmsubscribers',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#subscribers').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#subscriber-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Subscribers have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}