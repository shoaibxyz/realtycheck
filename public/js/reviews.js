$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function drawTable()
{
    //alert('aaa');
    var columns = [
        { data: 'selchkbox', name: 'selchkbox', orderable: false },
        { data: 'provider', name: 'provider', orderable: true},
        { data: 'business', name: 'business', orderable: true},
        { data: 'name', name: 'name', orderable: true},
        { data: 'phone', name: 'phone', orderable: true},
        { data: 'email', name: 'email', orderable: true},
        { data: 'is_active', name: 'is_active',className: 'col-status'},
        { data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var table = $('#reviews').DataTable({
        "processing": true,
        "serverSide": true,
        "bDestroy":true,
        "bFilter":false,
        "order": [6, "asc"],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.sid = $('#hfsid').val();
                d.city = $('#city').val();
                d.provider = $('#vendor').val();
                d.business = $('#business').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
                d.name = $('#revname').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            convertChkRdo();
            fillBusinesses($('#hfsid').val());
        }
    });
}

function fillBusinesses(sid)
{
    $.ajax({
        type: 'get',
        url: path + 'getservbus',
        data: {
            'sid': sid
        },
        success: function(data) {
            $("#business").select2();
            var output = '<option value="">Select Business</option>';
            $.each(data, function(key, val) {
                output += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $("#business").html(output);
        }
    });
}

$('ul.list-filter li').on("click",function (e) {
    $('ul.list-filter li').removeClass('active');
    $(this).addClass('active');
    var sid = $(this).attr('data-filter');
    $('#hfsid').val(sid);
    drawTable();
    //fillBusinesses(sid);
});

$('#search-form').on('submit', function(e) {
    drawTable();
    e.preventDefault();
    return false;
});

function viewReview(id)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getreview',
        data: {
            'id': id
        },
        success: function(data) {
            $('#reviewModal .modal-body').html(data);
            $('#reviewModal').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

function delReview(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this review?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delreview',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#reviews').DataTable();
					table.row('#review-row-' + data.id).remove().draw( false );
					$('.note p').html('Review has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#reviews tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmpreviews',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#reviews').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#review-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Reviews have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

