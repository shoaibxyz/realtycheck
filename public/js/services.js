
function drawTable()
{
    //alert('aaa');
var columns = [
    { data: 'selchkbox', name: 'selchkbox', orderable: false },
    { data: 'id', name: 'id', visible: false},
    { data: 'thumb', name: 'thumb', orderable: false,className: 'col-status'},
    { data: 'name', name: 'name', orderable: true},
    { data: 'updated_by', name: 'updated_by'},
    { data: 'updated_at', name: 'updated_at'},
    { data: 'is_active', name: 'is_active', orderable: true,className: 'col-status'},
    { data: 'btn_actions', name: 'btn_actions', orderable: false},
];

var table = $('#services').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave":true,
    "bDestroy":true,
    "bFilter":false,
    "order": [[6, "asc" ]],
    ajax: {
        url: $('#hfroute').val(),
        data: function (d) {
            d.service = $('#service').val();
        }
    },
    "columns": columns,
    initComplete: function () {
        $('.mix-grid').mixItUp();
        convertChkRdo();
    }
});
}

$('#search-form').on('submit', function(e) {
    drawTable();
    e.preventDefault();
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalService').html('Add New Service');
			$('#name').val('');
			$('#service_id').val('');
            $('#slug').val('');
            $('#slug').val('');
            $('#files_list').html('');
            $('#filepaths').val('');
            $('#files_list2').html('');
            $('#filepaths2').val('');
            break;
        case 'edit':
            $('#myModalService').html('Edit Service');
            $.ajax({
                type: 'get',
                url: path + 'getService',
                data: {
                    'id': id
                },
                success: function(data) {
                    $('#service_id').val(id);
                    $('#name').val(data.name);
                    $('#slug').val(data.slug);
                    $('#status').val(data.is_active).trigger('change');
                }
            });
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

$('#services').on("change",function(e){
    $.ajax({
        type: 'get',
        url: path + 'getsfilters',
        data: {
            'id': $(this).val()
        },
        success: function(data) {
            $('#filters').html(data);
        }
    });
});

function  assign(id) {
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getServiceFilters',
        data: {
            'id': id
        },
        success: function(data) {
            $('#assignModal .modal-body').html(data);
            convertChkRdo();
            $('#assfilter_id').val(id);
            $('#assignModal').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

function saveassign()
{
    var url = path + 'saveServiceFilters';

    $.ajax({
        type: 'post',
        url: url,
        data: $('#frmassignFilters').serialize(),
        success: function(data) {
            if(data['status'] == 'e')
            {
                alert(data['message']);
            }
            else
            {
                $('#assignModal').modal('hide');
                $('.note h4').html('Success');
                $('.note p').html('Service filters have been saved successfully.');
                $('.note').show(0).delay(3000).hide(0);
            }
        }
    });
    return false;
}

function saveService()
{
    var id = $('#service_id').val();
    var url = path + 'services';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmService').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    var errorMsg = data['message'];
                    var msgs = errorMsg.toString().replace(',','<br>');
                    showError(msgs);
                    //alert(msgs[0]);
                }
                else
                {
                    //window.location = path + 'services';
                    //var table = $('#services').DataTable();
                    if (id)
                    {
                        //$('tr#service-row-' + id).replaceWith(data);
                        $('.note p').html('Service has been updated successfully.');
                    }
                    else
                    {
                        //$('#services tbody').append(data);
                        $('.note p').html('Service has been added successfully.');
                    }
                    drawTable();
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delService(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this Service?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delservice',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
                    drawTable();
					$('.note p').html('Service has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#services tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmservices',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
                    drawTable();
					$('.note p').html('Services have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}