function drawftable() {
    var columns = [
        {data: 'business', name: 'business', orderable: true},
        {data: 'service', name: 'service', visible: true},
        /*{ data: 'city', name: 'city', orderable: true},
         { data: 'adtype', name: 'adtype', orderable: true},
         { data: 'start_date', name: 'start_date', orderable: true},*/
        {data: 'end_date', name: 'end_date', orderable: true},
        {data: 'min_budget', name: 'min_budget', orderable: true},
        {data: 'location', name: 'location', orderable: true},
        {data: 'is_active', name: 'is_active', className: 'col-status'},
        {data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var fTable = $('#featuredads').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "bDestroy": true,
        "bFilter": false,
        "order": [[5, "asc"]],
        ajax: {
            url: $('#hfroute').val(),
            data: function (d) {
                d.business = $('#business').val();
                d.adtype = $('#adtype').val();
                d.location = $('#location2').val();
                d.city = $('#city2').val();
                d.service = $('#service2').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns,
        initComplete: function () {
            $.ajax({
                type: 'get',
                url: path + 'getadtypes',
                data: {
                    'type': $('#adtypes').val()
                },
                success: function(data) {
                    $('#adtype').html(data)
                }
            });
        }
    });
}

$('#search-form').on('submit', function(e) {
    drawftable();
    e.preventDefault();
});

function drawbtable() {
    var columns2 = [
        {data: 'name', name: 'name', orderable: true},
        {data: 'areyou', name: 'areyou', visible: true},
        /*{ data: 'city', name: 'city', orderable: false},
         { data: 'adtype_id', name: 'adtype_id', orderable: true},*/
        {data: 'location', name: 'location', orderable: true},
        /*{ data: 'start_date', name: 'start_date', orderable: true},*/
        {data: 'end_date', name: 'end_date', orderable: true},
        {data: 'min_budget', name: 'min_budget', orderable: true},
        {data: 'is_active', name: 'is_active', className: 'col-status'},
        {data: 'btn_actions', name: 'btn_actions', orderable: false},
    ];

    var bTable = $('#bannerads').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "bDestroy": true,
        "bFilter": false,
        "order": [[5, "asc"]],
        ajax: {
            url: $('#hfroute2').val(),
            data: function (d) {
                d.name = $('#name').val();
                d.adtype = $('#adtype').val();
                d.location = $('#location2').val();
                d.city = $('#city2').val();
                d.areyou = $('#areyou2').val();
                d.startdate = $('#startdate').val();
                d.enddate = $('#enddate').val();
            }
        },
        "columns": columns2,
        initComplete: function () {
            $.ajax({
                type: 'get',
                url: path + 'getadtypes',
                data: {
                    'type': $('#adtypes').val()
                },
                success: function(data) {
                    $('#adtype').html(data)
                }
            });
        }
    });
}

$('#ads_search-form').on('submit', function(e) {
    if($('#adtypes').val() == 1 || $('#adtypes').val() == 2)
        drawftable();
    else
        drawbtable();
    e.preventDefault();
});

$('#search-form2').on('submit', function(e) {
    drawbtable();
    e.preventDefault();
});

$('#adtypes2').on("change",function (e) {
    var id = $(this).val();
    //alert(id);
    if(id == 1 || id==2)
    {
        $('#location_row').show();
        $('#business_row').show();
        $('#service_row').hide();
        $('#vendor_row').hide();
        $('#image_row').hide();
        $('#areyou_row').hide();
        $('#person_row').hide();
        $('#url_row').hide();
        if(id == 1) {
            $('#message_row').hide();
        }
        else {
            $('#message_row').show();
        }
    }
    else if(id >= 3)
    {
        $('#location').select('val',1);
        $('#location_row').show();
        $('#business_row').hide();
        $('#service_row').hide();
        $('#vendor_row').hide();
        $('#areyou_row').show();
        $('#person_row').show();
        $('#image_row').show();
        $('#url_row').show();
        $('#message_row').hide();
        if(id == 3) {
            $('#filesize_info').html('Required file size: 728x90');
            $('#aspratio').val('NaN');
            $('#width').val(728);
            $('#height').val(90);
        }
        if(id==4) {
            $('#filesize_info').html('Required file size: 100x90');
            $('#aspratio').val('1.3333333333333333');
            $('#location_row').hide();
            $('#width').val(240);
            $('#height').val(180);
        }
        if(id==5) {
            $('#filesize_info').html('Required file size: 90x60');
            $('#aspratio').val('1.3333333333333333');
            $('#location_row').hide();
            $('#width').val(180);
            $('#height').val(136);
        }
    }
    if($('#location').val() && $('#min_budget').val()) {
        verifyBudget();
        calcBudget();
    }
    if($('#location').val() != '' || id == 4 || id == 5)
        addPrevClass();
})

function addPrevClass()
{
    var t = $('#adtypes2').val();
    var l = $('#location').val();
    $('#previewModal #preview').attr('class','');
    if(t == 1) {
        $('.preview-box').show();
        $('.banner-ad').hide();
        if (l == 1) {
            $('#previewModal #preview').addClass('landing-featured-preview');
            $('.ftag').show();
            $('.hdtag').hide();
            $('.adtitle').show();
        }
        else if (l == 2) {
            $('#previewModal #preview').addClass('inner-featured-preview');
            $('.ftag').show();
            $('.hdtag').hide();
            $('.adtitle').hide();
        }
    }
    else if(t == 2) {
        $('.preview-box').show();
        $('.banner-ad').hide();
        if (l == 1) {
            $('#previewModal #preview').addClass('landing-hotdeal-preview');
        }
        else if (l == 2) {
            $('#previewModal #preview').addClass('inner-hotdeal-preview');
        }
        $('.ftag').hide();
        $('.hdtag').show();
        $('.adtitle').show();
    }
    else if(t == 3) {
        if (l == 1) {
            $('#previewModal #preview').addClass('landing-banner-preview');
        }
        else if (l == 2) {
            $('#previewModal #preview').addClass('inner-banner-preview');
        }
        $('#width').val(728);
        $('#height').val(90);
        $('.preview-box').hide();
        $('.banner-ad').show();
    }
    else if(t == 4) {
        $('#previewModal #preview').addClass('static-logo-preview');
        $('#width').val(240);
        $('#height').val(180);
        $('.preview-box').hide();
        $('.banner-ad').show();
    }
    else if(t == 5) {
        $('#previewModal #preview').addClass('moving-logo-preview');
        $('#width').val(180);
        $('#height').val(136);
        $('.preview-box').hide();
        $('.banner-ad').show();
    }
}

function openPreview()
{
    $('#previewModal').modal('show');
    if($('#adtypes2').val() == 2)
    {
        $('.hdtag').html($('#message').val());
    }
}

$('#location').on("change",function (e) {
    if($('#adtypes2').val() != '' && $('#min_budget').val() != '') {
        verifyBudget();
        calcBudget();
    }
    if($('#adtypes2').val() != '')
        addPrevClass();
})

function calcBudget()
{
    //alert($('#city').val());
    var citys = $('#city').val();
    //citys = citys.split(',');
    //alert(citys.length);
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var min_budget = $('#min_budget').val();
    if(start_date && end_date && min_budget && citys)
    {
        from = moment(start_date, 'MM/DD/YYYY'); // format in which you have the date
        to = moment(end_date, 'MM/DD/YYYY');     // format in which you have the date

        /* using diff */
        duration = to.diff(from, 'days')
        //alert(duration);
        $('.tadcost span').html('Rs. ' + duration*min_budget*citys.length);
        $('#hfbudget').val(duration*min_budget*citys.length);
    }
}

$('#areyou').on("change",function (e) {
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getPersons',
        data: {
            'type': $('#areyou').val()
        },
        success: function(data) {
            $('#person').html (data);
            $('.portlet.box').find('.loading').remove();
        }
    });
})

$('#services2').on("change",function (e)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getmedia',
        data: {
            'id': $(this).val()
        },
        success: function(data) {
            //alert($('#mediaModal .modal-body .thumbs'));
            $('div.media-thumbs').html(data.rows);
            $('.mix-grid').mixItUp();
            $('.portlet.box').find('.loading').remove();
        }
    });
});

function toggle(modalstate, id, sid)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Ad');
            $('#ad_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Image');
            $('#ad_id').val(id);
            var row = $("tr#ad-row-"+id);
            $('#services').val(sid);
            var status = row.find("td:nth-child(3)").find('span').html();
            $('#status option[text="Pending"]').attr("selected", "selected");
            $("#status option").filter(function() {
                return this.text == status;
            }).attr('selected', true);
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function showmediaPop()
{
    $('#mediaModal').modal('show');
}

function calcAdCost()
{
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var min_budget = $('#min_budget').val();
    if(start_date && end_date && min_budget)
    {
        from = moment(start_date, 'MM/DD/YYYY'); // format in which you have the date
        to = moment(end_date, 'MM/DD/YYYY');     // format in which you have the date

        /* using diff */
        duration = to.diff(from, 'days')
        //alert(duration);
        $('.tadcost span').html('Rs. ' + duration*min_budget);
    }
}

/*$('#adtypes').on('change',function ()
{
    $('#adsForm').submit();

});*/

function verifyBudget()
{
    //alert('aaaa');
    if(!isNumeric($('#min_budget').val()))
    {
        alert('Budget field must be numeric.');
    }
    else {
        $.ajax({
            type: 'get',
            url: path + 'verifybudget',
            data: {
                'lid': $('#location').val(),
                'tid': $('#adtypes2').val(),
                'budget': $('#min_budget').val()
            },
            success: function (data) {
                //alert(data > parseInt($('#min_budget').val()));
                if (data > parseInt($('#min_budget').val())) {
                    //alert(data > $('#min_budget').val());
                    var type = $("#adtypes2 option:selected").text();
                    $('.budget_error').html('* ' + type + ' Ad minimum budget is (' + data + ' PKR per day)');
                    $('.budget_error').show();
                }
                else {
                    $('.budget_error').hide();
                    calcBudget();
                }
            }
        });
    }
}

function isNumeric(input) {
    var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
    var regex = RegExp(number);
    return regex.test(input) && input.length>0;
}

function viewAd($id,$tid)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'viewad',
        data: {
            'id': $id,
            'tid': $tid
        },
        success: function(data) {
            $('#adModal .modal-body').html(data);
            $('#adModal').modal('show');
            $('.portlet.box').find('.loading').remove();
        }
    });
}

function saveAd()
{
    //alert('aaaa');
    var count=0;
    var count2=0;
    $('.form-control', $('#frmImage').id).each(function() {
        if($(this).hasClass('validate'))
        {
            //alert($(this).className);
            if(($(this).val() == '' || $(this).val() == null) && $(this).attr('id').substr(0,5) != 's2id_' && ($(this).parent().parent().css('display') == 'block' || $(this).parent().parent().css('display') == 'table'))
            {
                //alert($(this).attr('id'));
                $(this).parent().addClass('has-error');
                count = parseInt(count) + 1;
            }
            else
                $(this).parent().removeClass('has-error');
        }
        /*if($(this).hasClass('num_validate'))
        {
            if($(this).val() != '' && $(this).val() != null && !isNumeric($(this).val()))
            {
                $(this).parent().addClass('has-error');
                count = parseInt(count) + 1;
            }
        }*/
    });

    //alert(count);
    if(count > 0)
    {
        alert('Please correct validation errors.');
        return false;
    }
    else {
        $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
        $.ajax({
            type: 'post',
            url: path + 'new-ad',
            data: $('#frmImage').serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    var newLine = "\n";
                    var err = data.error;
                    //var errs = JSON.parse(data.error);
                    var msg = '';
                    //alert(err.length);
                    if (err.length > 1) {
                        for (var i = 0; i < err.length; i++)
                            if(i == err.length -1)
                                msg += err[i] + '</li>';
                            else
                                msg += err[i] + '</li><li>';
                        //alert(msg);
                    }
                    else {
                        //alert(err[0]);
                        msg = err[0] + '</li>';
                    }
                    //var errorMsg = data.error;
                    //var msgs = errorMsg.toString().replace(',','</li><li>');
                    showError('<ul><li>'+msg+'</ul>');
                    $('.portlet.box').find('.loading').remove();
                }
                else {
                    $('.portlet.box').find('.loading').remove();
                    //$('#frmProvider').submit();
                    window.location = path + data.url;
                    return true;
                }
            }
        });
    }
    return false;
}

/*$('#frmImage').on('submit', function (e)
{
    $(':input', $('#frmImage').id).each(function() {
        if($(this).hasClass('validate'))
        {
            if($(this).val() == '' && $(this).parent().css('display') == 'block')
            {
                $(this).parent().addClass('has-error');
                count = parseInt(count) + 1;
            }
            else
                $(this).parent().removeClass('has-error');
        }
        if($(this).hasClass('email_validate'))
        {
            if($(this).val()!= '' && !isValidEmail($(this).val()))
            {
                $(this).parent().addClass('has-error');
                $(this).parent().append('<span class="error_detail">Email address is invalid.</div>');
                count = parseInt(count) + 1;
            }
            else
            {
                if(!$(this).hasClass('validate'))
                {
                    $(this).parent().removeClass('has-error');
                }
                $(this).parent().find('.error_detail').remove();
            }
        }
    });
    alert(count);
    if(count > 0)
    {
        alert('Please correct validation errors.');
        return false;
    }
    else {
        $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
        $.ajax({
            type: 'post',
            url: path + 'new-ad',
            data: $('#frmImage').serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    var newLine = "\n";
                    var err = data.error;
                    //var errs = JSON.parse(data.error);
                    var msg = '';
                    //alert(err.length);
                    if (err.length > 1) {
                        for (var i = 0; i < err.length; i++)
                            msg += err[i] + newLine;
                        alert(msg);
                    }
                    else {
                        alert(err);
                    }
                    $('.portlet.box').find('.loading').remove();
                }
                else {
                    $('.portlet.box').find('.loading').remove();
                    //$('#frmProvider').submit();
                    window.location = path + data.url;
                    return true;
                }
            }
        });
    }
    return false;
})*/

function delAd(id,tid)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this ad?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'get',
				url: path + 'deletead',
				data: {
                      'm': 1,
                      'adid': id,
                    'tid': tid
				},
				success: function(data) {
				    if(tid == 1 || tid == 2)
                        drawftable();
				    else
                        drawbtable();
					$('.note p').html('Ad has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#pmedia tr').each(function (i, row)
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmads',
				data: {
						'm': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#ads1').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#image-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Imagees have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

