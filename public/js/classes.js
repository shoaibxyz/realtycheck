// INIT DATATABLES
$(function () {
	// Init
    //var spinner = $( ".spinner" ).spinner();
    var table = $('#classes').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "sDom": '<"dt_head"f>rt<"F"lip>'
    } );

    var tableTools = new $.fn.dataTable.TableTools( table, {
    	"sSwfPath": "../vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
    $(".DTTT_container").css("float","right");
	
	/*$("form[name='frmClass']").validate({
		// Specify validation rules
		rules: {
		  // The key name on the left side is the name attribute
		  // of an input field. Validation rules are defined
		  // on the right side
		  title: "required",
		},
		// Specify validation error messages
		messages: {
		  title: "Please enter class name",		  
		},
		// Make sure the form is submitted to the destination defined
		// in the "action" attribute of the form when valid
		submitHandler: function(form) {
		  //form.submit();
		  saveClass();
		  return false;
		}
	  });*/
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function toggle(modalstate, id)
{
    $('.form-group').removeClass('has-error');
    switch (modalstate) {
        case 'add':
            $('#myModalLabel').html('Add New Class');
			$('#name').val('');
			$('#class_id').val('');
            break;
        case 'edit':
            $('#myModalLabel').html('Edit Class');
			$('#class_id').val(id);
			var row = $("tr#class-row-"+id);
			$('#name').val(row.find("td:nth-child(2)").html());
            var status = row.find("td span.label").attr('data-id');
            $('#status').val(status).trigger('change');
            break;
        default:
            break;
    }
    $('#myModal').modal('show');
}

function saveClass()
{
    var id = $('#class_id').val();
    var url = path + 'classes';
    if (id){
            url += "/" + id;
    }
    $.ajax({
            type: 'post',
            url: url,
            data: $('#frmClass').serialize(),
            success: function(data) {
                if(data['status'] == 'e')
                {
                    alert(data['message']);
                }
                else
                {
                    window.location = data;
                    if (id)
                    {
                        $('tr#class-row-' + id).replaceWith(data);
                        $('.note p').html('Class has been updated successfully.');
                    }
                    else
                    {
                        $('#classes').append(data);
                        $('.note p').html('Class has been added successfully.');
                    }
                    convertChkRdo();
                    $('#name').val('');
                    $('#myModal').modal('hide');
                    $('.note h4').html('Success');
                    $('.note').show(0).delay(3000).hide(0);
                }
            }
        });
    return false;
}

function delClass(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this class?');
    if (isConfirmDelete)
	{
		$.ajax({
				type: 'post',
				url: path + 'delclass',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
					var table = $('#classes').DataTable();
					table.row('#class-row-' + data.id).remove().draw( false );
					$('.note p').html('Class has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	} 
	else {
    	return false;
    }
}
function delAllSelected(){
	var chks = [];
	$('#classes tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmclasses',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#classes').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#class-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Classs have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}