function initCropper(){
    //console.log("Came here")
    var image = document.getElementById('imagetoCrop');
    var $w = 528;
    var $h = 480;


    var cropper = new Cropper(image, {
        aspectRatio: '1.13',
        viewMode: 2,
        minCropBoxWidth:$w,
        minCropBoxHeight:$h,
        guides: false,
        crop: function(e) {
            console.log(e.detail.width);
            //$('#width').val(Math.round(e.detail.width,0));
            console.log(e.detail.height);
            //$('#height').val(Math.round(e.detail.height,0));
            $('#cropModal').find('.loading').remove();
        }
    });

    // On crop button clicked
    document.getElementById('crop_button').addEventListener('click', function(){

        $('#cropModal').append('<div class="loading"><p>Cropping please wait....</p></div>');
        var imgurl = cropper.getCroppedCanvas().toDataURL();
        var img = document.createElement("img");
        img.src = imgurl;

        /* ----------------	SEND IMAGE TO THE SERVER-------------------------*/

        cropper.getCroppedCanvas().toBlob(function (blob) {
            var formData = new FormData();
            formData.append('croppedImage', blob);
            formData.append('filepaths', $('#filepaths').val());
            // Use `jQuery.ajax` method
            $.ajax(path.replace('/admin','') + 'api/upload_media2', {
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    var fname = data.filename;
                    var image = '<div class="gthumbs"><img class="gthumb" src="' + APP_URL + TMPIP + '/' + fname + '"><a href="javascript:void(0);" onclick=removeImage("' + fname + '",1)><img src="' + APP_URL +'/public/img/x.png" class="img_delete" alt=""></a></div>';
                    $('#files_list').html(image);
                    $('#filepath').val(fname);
                    $('#cropModal').modal('hide');
                    //cropper.destroy();
                    $('#cropModal').find('.loading').remove();
                },
                error: function () {
                    console.log('Upload error');
                }
            });
        });
        //}
    })
    $('#cropModal').on('hide.bs.modal', function (e) {
        //alert('a');
        cropper.destroy();
    })
    cropper.destroy();
}

function removeImage(file, t) {
    $.ajax({
        type: 'get',
        url: path + 'removepsimage',
        data: {
            'file': file
        },
        success: function (data) {
            if (t == 1) {
                $('#files_list').html('');
                $('#filepath').val('');
            }
            else if (t == 2) {
                $('#cf-files_list').html('');
                $('#clsfied_filepath').val('');
            }
            else if (t == 3) {
                $('#logo-files_list').html('');
                $('#logo_filepath').val('');
            }
        }
    });
}

function viewService(id)
{
    $('.portlet.box').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getpservice',
        data: {
            'id': id
        },
        success: function(data) {
            $('#businessModal .modal-body').html(data["str"]);
            $('#businessModal').modal('show');
            $('.portlet.box').find('.loading').remove();
            $('#rate').rating({displayOnly: true, step: 0.5});
            $('.mix-grid').mixItUp();
        }
    });
}

function delService(id)
{
	var isConfirmDelete = confirm('Are you sure you want to delete this business?');
    if (isConfirmDelete)
	{
        //alert(path);
		$.ajax({
				type: 'get',
                url: path + 'delpservice',
				data: {
						  'mod': '1',
						  'id': id
				},
				success: function(data) {
                    //alert(data);
					var table = $('#services').DataTable();
					table.row('#service-row-' + data.id).remove().draw( false );
					$('.note p').html('Business has been deleted successfully.');
					$('.note h4').html('Success');
					$('.note').show(0).delay(3000).hide(0);
				}
        });
        return false;
	} 
	else {
    	return false;
    }
}

function delAllSelected(){
	var chks = [];
	$('#services tr').each(function (i, row) 
	{
		var $row = $(row),				
			$checkedBoxes = $row.find('input:checked');
		$checkedBoxes.each(function (i, checkbox) {
			if(checkbox.value != 'on')
				chks.push(checkbox.value);
		});

	});
	if(chks.length > 0)
	{
		$.ajax({
				type: 'post',
				url: path + 'delmpservices',
				data: {
						'mod': '1',
						  'ids': chks
				},
				success: function(data) {
					var i;
					var table = $('#services').DataTable();
					for (i = 0; i < chks.length; ++i) {
						table.row('#service-row-' + chks[i]).remove().draw( false );
					}					
					$('.note p').html('Businesses have been deleted successfully.');
					$('.note h4').html('Success');					
					$('.note').show(0).delay(3000).hide(0);
				}
		});
	}
	else
		alert('Please select some row');
}

/*$(document).on('ready', function()
{

});*/

function getServicesLists(sid)
{
    $('#hfservices').val(sid);
    $('#services-lists').append('<div class="loading"><p>Loading please wait....</p></div>');
    $.ajax({
        type: 'get',
        url: path + 'getServicesLists',
        data: {
            'sid': sid,
            'cid': $('#city.selectpicker').selectpicker('val'),
            'eid': $('#event.selectpicker').selectpicker('val'),
        },
        success: function(data) {
            $('#myTabContent .services_row').html(data.lists);
            if(data.featured == 0) {
                $('.featured_row').hide().slick('slickRemove');
            }
            else {
                $('.featured_row').show();
                //('#myTabContent .featured_row').html('');
                $('#myTabContent .featured_row').html(data.flists);
            }

			if(data.filters) {
                $('#service_filters').html('');
                $('#service_filters').append(data.filters).find('input.form-control-cb').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    increaseArea: '20%' // optional
                });
                $('input.form-control-cb').on('ifChecked', function(event){
                    getFilteredLists('');
                });

                $('input.form-control-cb').on('ifUnchecked', function(event){
                    getFilteredLists('');
                });
                $('#service_filters').show();
                //convertChkRdo();
            }
        	else {
                $('#service_filters').html('');
                $('#service_filters').hide();
            }

            $('#myTab li').removeClass('active');
            $('#myTab li#list-'+sid).addClass('active');
            $('#services-lists').find('.loading').remove();
            $('.psrrate').rating({displayOnly: true, step: 0.5});
            $('#filter_class').val('');
            $('#frate').rating('clear');
            $('.brates').rating({
                step: 1,
                showClear: false,
                showCaption: false,
                displayOnly: true
            }).on('rating.change', function(event, value, caption) {
            });
        }
    });
}

function addtoFav($psid,$uid)
{
    if($uid == 0)
    {
        swal({
            title: "Error!",
            text: "Please login first in order to proceed.",
            type: "error",
            confirmButtonText: "OK"
        });
        showSI();
    }
    else
    {
        $.ajax({
            type: 'post',
            url: path + 'addtofav',
            data: {
                'uid': $uid,
                'psid': $psid
            },
            success: function(data) {
                if(data == -1)
                    swal({
                        title: "Error!",
                        text: "You have already added this business to your favorites list.",
                        type: "error",
                        confirmButtonText: "OK"
                    });
                else
                    swal("Added!", "Business has been added to your favorites list.", "success");
            }
        });
    }
}

function sendReport(){
    var currentTime = new Date();
    var dt_array = currentTime.toLocaleString().split(',');
    $.ajax({
        type: 'post',
        url: path + 'addbreport',
        data: $('#frmReport').serialize() + '&dt='+ dt_array[0] + ' ' + dt_array[1],
        success: function(data) {
            if(data.status==1)
                alert(data.error);
            else {
                swal("Confirmation!", "The business has been reported.", "success");
                $('#reportModal').modal('hide');
            }
        }
    });
    return false;
}