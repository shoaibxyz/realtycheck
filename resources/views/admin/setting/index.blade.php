@extends('admin.layouts.template')

@section('title','Settings')

@section('profile-active','active')

@section('content')

@include('admin/common/breadcrumb',['page'=>'Settings'])
        
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif    

<div class="x_panel">
        <div class="x_title">
           
            
            <div class="clearfix"></div>
          </div>    

        {!! Form::open(['method' => 'POST', 'route' => 'users.updatePassword', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('old_password', 'Old password') !!}
                {!! Form::password('old_password', ['class' => 'form-control', 'id' => 'old_password']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('password', 'New Password') !!}
                {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('password_confirmation', 'Confirm password ') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!} 
            </div>
        
            <div class="form-group">
                {!! Form::submit("Update Password", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>
       
@endsection