@extends('admin.layouts.template')
@section('title','Followup & Metting')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
 <?php if( Session::has('message')) : 
    $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
 ?>
    <div class="alert {{ $alertType }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session('message') }}
    </div>
<?php endif; ?>


<div class="x_panel">
    <div class="x_title">
    <h2>Followup Attachments </h2>
    <div class="clearfix"></div>
</div>   
        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif


        @if(count($followup->files) > 0)
        <div class="col-sm-8 col-sm-offset-2">
            <ul class="list-group">
                @foreach($followup->files as $file)
                    <li class="list-group-item">
                        <a href="{{ asset('public/followup/') .'/'.$file->file_name }}" target="_blank">View/Download</a>
                        <a href="{{ route('followup.attachment.delete', $file->id) }}" class="pull-right">Delete</a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endif

     
        </div>

    </div>

</div>
@endsection



@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    var meeting_status = null;
    $("input[name='meeting_status']").click(function() {
        meeting_status = this.value;

        if(meeting_status == 1)
        {
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
        }else{
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
        }
    });
    
    </script>
@endsection