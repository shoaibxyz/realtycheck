@extends('admin.layouts.template')
@section('title','Followup')

@section('content')
    	
    	<h2>Followup </h2>
    	@if(!empty($message))
    		<p>{{$message}}</p>
    	@endif
    	
    	<div class="x_panel">
         
    	<div class="table-responsive">
        <table class="table table-striped table-bordered jambo_table bulk_action" id="followupTable">
    		<thead>
    			<tr>
    				<th>ID</th>
            <th>Registration No</th>
            <th>Meeting date</th>
    				<th colspan="2">Action</th>
    			</tr>
    		</thead>
    		<tbody>

          @php($i = 1)
    			@foreach($followup as $follow)
	    			<tr>
	    				<td>{{ $i }}</td>
              <td>{{ $follow->registeration_no }}</td>
    			<td>{{ $follow->meeting_date }}</td>
    			<td colspan="2">
                <a href="{{ route('followup.edit', $follow->id ) }}" class="btn btn-info btn-xs">Edit</a>
              {!! Form::open(['method' => 'DELETE', 'route' => ['followup.destroy', $follow->id], 'class' => 'deleteForm', 'style' => 'display: inline']) !!}
                  {!! Form::submit("Delete", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!}
              {!! Form::close() !!}
              </td>
	    			</tr>
            @php($i++)
	    		@endforeach
    		</tbody>
    	</table>
        </div>	</div>
@endsection

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function(){
        $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
        
        $('#followupTable').dataTable({
            bSort: false
        });
    });  
    </script>
@endsection