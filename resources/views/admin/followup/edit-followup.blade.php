@extends('admin.layouts.template')
@section('title','Followup & Metting')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

 <?php if( Session::has('message')) : 
    $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
 ?>
    <div class="alert {{ $alertType }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session('message') }}
    </div>
<?php endif; ?>


<div class="x_panel">
    <div class="x_title">
    <h2>Followup & Metting</h2>
    <div class="clearfix"></div>
</div>   
        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::model($followup, ['method' => 'PATCH', 'route' => ['followup.update', $followup->id], 'files' => 'true', 'class' => 'form-horizontal']) !!}
        <div class="col-sm-6">  
            <div class="form-group">
                {!! Form::label('registeration_no', 'Registration No') !!}
                {!! Form::text('registeration_no', null, ['class' => 'form-control','readonly' => 'readonly', 'required'=>'required', 'id' => 'registeration_no']) !!} 
            </div>
        </div>


         <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('meeting_date', 'Meeting date') !!}
                {!! Form::text('meeting_date', null, ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'meeting_date']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('meeting_channel', 'Meeting Channel') !!}
                <?php 
                        $meeting_channel[''] = 'Choose One';
                        $meeting_channel['email'] = 'Email';
                        $meeting_channel['mobile'] = 'Mobile';
                        $meeting_channel['phone'] = 'Phone';
                        $meeting_channel['mobile_residence'] = 'Mobile Residence';
                        $meeting_channel['sms_mobile'] = 'SMS Mobile';
                ?>

                {!! Form::select('meeting_channel', $meeting_channel, old('meeting_channel'),  ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_channel']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('current_meeting_by', 'Meeting By') !!}
                {!! Form::text('current_meeting_by', Auth::user()->name , ['class' => 'form-control', 'required'=>'required', 'id' => 'current_meeting_by', 'readonly' => 'readonly']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('meeting_detail', 'Meeting Detail') !!}
                {!! Form::text('meeting_detail', null, ['class' => 'form-control', 'waive_days'=>'required', 'id' => 'meeting_detail']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('in_out', 'Call In / Out') !!} <br>
                {!! Form::radio('in_out', 0,  null ) !!} Out
                {!! Form::radio('in_out', 1,  null ) !!} In
            </div>
        </div>

        <div class="col-sm-12">
            <h4>Next Call / Meeting</h4>
        </div>    

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('meeting_status', 'Meeting Status') !!}<br>
                {!! Form::radio('meeting_status', 1, false) !!} Done
                {!! Form::radio('meeting_status', 0, true) !!} Pending
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('next_meeting_date', 'Next Meeting date') !!}
                {!! Form::text('next_meeting_date', null, ['class' => 'form-control datepicker', 'id' => 'next_meeting_date']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('next_meeting_channel', 'Next Meeting Channel') !!}
                <?php 
                        $next_meeting_channel[''] = 'Choose One';
                        $next_meeting_channel['email'] = 'Email';
                        $next_meeting_channel['mobile'] = 'Mobile';
                        $next_meeting_channel['phone'] = 'Phone';
                        $next_meeting_channel['mobile_residence'] = 'Mobile Residence';
                        $next_meeting_channel['sms_mobile'] = 'SMS Mobile';
                ?>

                {!! Form::select('next_meeting_channel', $next_meeting_channel, old('next_meeting_channel'),  ['class' => 'form-control', 'id' => 'next_meeting_channel']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('next_meeting_by', 'Next Meeting By') !!}
                <?php 
                    $ccdArr[''] = 'Choose one';
                    foreach ($ccd as $key => $c) {
                        $ccdArr[$c->id] = $c->name;
                    }
                ?>
                
                {!! Form::select('next_meeting_by', $ccdArr, old('next_meeting_by'), ['class' => 'form-control', 'id' => 'next_meeting_by']) !!} 
            </div>
        </div>
        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('next_meeting_agenda', 'Next Meeting Agenda') !!}
                {!! Form::text('next_meeting_agenda', old('next_meeting_agenda'), ['class' => 'form-control', 'id' => 'next_meeting_agenda']) !!} 
            </div>
        </div>

        <div class="col-sm-6">   
            <div class="form-group">
                {!! Form::label('files', 'Upload Files') !!}
                <input type="file" name="files[]" multiple class="form-control" >
            </div>
        @if(count($files) > 0)
        <div class="col-sm-12">
            <p>Uploaded Files</p>
            <ul class="list-group">
                @foreach($files as $file)
                    <li class="list-group-item">
                        <a href="{{ asset('public/followup/') .'/'.$file->file_name }}" target="_blank">View/Download</a>
                        <a href="{{ route('followup.attachment.delete', $file->id) }}" class="pull-right">Delete</a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="col-sm-12">   
            <div class="form-group">
                {!! Form::hidden('meeting_done_by', Auth::user()->id) !!}
                {!! Form::submit("Save", ['class' => 'btn btn-default pull-right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </div>

    </div>

</div>
@endsection



@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    var meeting_status = null;
    $("input[name='meeting_status']").click(function() {
        meeting_status = this.value;

        if(meeting_status == 1)
        {
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
        }else{
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
        }
    });
    
    </script>
@endsection