<div class="x_panel">
    <div class="x_title">
    
    <div class="clearfix"></div>
</div>   
<div class="loader-image" style="display: none; top: 10%; left: 40%; position:absolute; z-index: 99999;">
            <img src="{{ asset('public/images/ajax-loader.gif') }}" width="200px" height="200px" style="background-repeat: no-repeat; background-position: center center; ">
          </div>
        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <?php if( Session::has('message')) : 
                $alertType = ( Session('status') == 1 ) ? "note-success" : "note-danger";
             ?>
                <div class="note {{ $alertType }}">
                    {{ Session('message') }}
                </div>
            <?php endif; ?>
            
        {!! Form::open(['method' => 'POST', 'route' => 'followup.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Followup & Metting</h2>
        <div class="row">
        <div class="col-sm-6">
           
           
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Registration No</label>
                 <div class="col-sm-7">
                
               
                {!! Form::text('registration_no', $registeration_no , array( 'class'=>'form-control' , 'id' => 'registration_no', 'readonly') )!!}
                
                </div>
                </div>
                
                         <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Meeting Detail</label>
                 <div class="col-sm-7">
                {!! Form::text('meeting_detail', null, ['class' => 'form-control', 'id' => 'meeting_detail']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Meeting Channel</label>
                 <div class="col-sm-7">
                 <div class="form-group">
                <!--{!! Form::label('meeting_channel', 'Meeting Channel') !!}-->
                <?php 
                        $meeting_channel[''] = 'Choose One';
                        $meeting_channel['email'] = 'Email';
                        $meeting_channel['mobile'] = 'Mobile';
                        $meeting_channel['phone'] = 'Phone';
                        $meeting_channel['mobile_residence'] = 'Mobile Residence';
                        $meeting_channel['sms_mobile'] = 'SMS Mobile';
                ?>

                {!! Form::select('meeting_channel', $meeting_channel, 0,  ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_channel']) !!} 
              </div>
                </div>
                </div>
                

        
                
        </div>
        
        
     <div class="col-sm-6">   
        
                        <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Meeting Date</label>
                 <div class="col-sm-7">
                {!! Form::text('meeting_date', null, ['class' => 'form-control', 'id' => 'meeting_date']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Meeting By</label>
                 <div class="col-sm-7">
                {!! Form::text('current_meeting_by', null, ['class' => 'form-control', 'id' => 'current_meeting_by']) !!}
                </div>
                </div>
                
       
                
        <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Call In / Out</label>
                 <div class="col-sm-7">
               <!--{!! Form::label('in_out', 'Call In / Out') !!}<br>-->
                {!! Form::radio('in_out', 0,  true ) !!} Out
                {!! Form::radio('in_out', 1,  false ) !!} In
                </div>
                </div>
        
        
        
        </div>
        
    </div>    
    
    
    <div class="row">
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Next Call / Meeting</h2>
        <div class="col-sm-6">
           
           
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Meeting Status</label>
                 <div class="col-sm-7">
                 <div class="form-group">
                {!! Form::label('meeting_status', 'Meeting Status') !!}<br>
                {!! Form::radio('meeting_status', 1, false) !!} Done
                {!! Form::radio('meeting_status', 0, true) !!} Pending
            </div>
                </div>
                </div>
                
                
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Next Meeting Channel</label>
                 <div class="col-sm-7">
                 <div class="form-group">
                <!--{!! Form::label('meeting_channel', 'Meeting Channel') !!}-->
                <?php 
                        $meeting_channel[''] = 'Choose One';
                        $meeting_channel['email'] = 'Email';
                        $meeting_channel['mobile'] = 'Mobile';
                        $meeting_channel['phone'] = 'Phone';
                        $meeting_channel['mobile_residence'] = 'Mobile Residence';
                        $meeting_channel['sms_mobile'] = 'SMS Mobile';
                ?>

                {!! Form::select('meeting_channel', $meeting_channel, 0,  ['class' => 'form-control', 'required'=>'required', 'id' => 'next_meeting_channel']) !!} 
              </div>
                </div>
                </div>
                         <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Next Meeting Agenda</label>
                 <div class="col-sm-7">
                {!! Form::text('meeting_agenda', null, ['class' => 'form-control', 'id' => 'next_meeting_agenda']) !!}
                </div>
                </div>

        
                
        </div>
        
        
     <div class="col-sm-6">   
        
                        <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Next Meeting Date</label>
                 <div class="col-sm-7">
                {!! Form::text('next_meeting_date', null, ['class' => 'form-control', 'id' => 'next_meeting_date']) !!}
                </div>
                </div>
                
               <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Next Meeting By</label>
                 <div class="col-sm-7">
                 <div class="form-group">
                 <?php 
                    $ccdArr[''] = 'Choose one';
                    foreach ($ccd as $key => $c) {
                        $ccdArr[$c->id] = $c->name;
                    }
                ?>
                {!! Form::select('next_meeting_by', $ccdArr, 0, ['class' => 'form-control', 'id' => 'next_meeting_by']) !!} 
              </div>
                </div>
       
                 <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Upload Files</label>
                 <div class="col-sm-7">
                <input type="file" name="files[]" multiple class="form-control" >
                </div>
                </div>
            
               
                
        
        
        </div>
    </div>    
        


        <div class="clearfix"></div>

        <div class="row"> 
             <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#History">History</a></li>
        </ul>

        <div class="tab-content">
          <div id="History" class="tab-pane fade in active">
            <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
            <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Meeting Date</th>
                        <th>Meeting By/Next</th>
                        <th>Status</th>
                        <th>Meeting Channel</th>
                        <th>Meeting Detail</th>
                        <th>Attachment</th>
                    </tr>
                </thead>
                <tbody>
                @php($i = 1)
                
                @foreach($followup as $follow)
                    @php($loop1 = 1)

                    @while($loop1 <= 2)
                        @if($loop1 == 1)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $follow->meeting_date }}</td>
                                <td>{{ $follow->meeting_done_by }}</td>
                                <td>{{ ($follow->meeting_status == 1) ? "Done" : "Pending"  }}</td>
                                <td>{{ $follow->meeting_channel }}</td>
                                <td>{{ $follow->meeting_detail }}</td>
                                <td>
                                @if(count($follow->files) > 0)
                                   <a href="{{ route('followup.attachment.view', $follow->id) }}">View Attachments</a>
                                @endif
                                </td>
                        
                            </tr>
                        @else 
                            <tr>
                                <td></td>
                                <td>{{ ($follow->next_meeting_date != "01-01-1970") ? $follow->next_meeting_date : "00-00-0000" }}</td>
                                <td>{{ $follow->next_meeting_by }}</td>
                                <td>{{ ($follow->in_out == 0) ? "Out" : "In"  }}</td>
                                <td>{{ $follow->next_meeting_channel }}</td>
                                <td>{{ $follow->next_meeting_agenda  }}</td>
                                <td></td>
                            </tr>
                        @endif
                    @php($loop1++)
                    @endwhile
                    @php($i++)
                @endforeach
                </tbody>
            </table>
            </div>
         </div>

        </div>

    </div>
 <div class="form-group">
                    <div class="text-center">
               {!! Form::hidden('meeting_done_by', Auth::user()->id) !!}
                {!! Form::submit("Save", ['class' => 'btn btn-success btn-md']) !!}
                </div>
                </div>
                {!! Form::close() !!}
</div>
</div>

<script type="text/javascript">
    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    var meeting_status = null;
    $("input[name='meeting_status']").click(function() {
        meeting_status = this.value;

        if(meeting_status == 1)
        {
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
        }else{
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
        }
    });
</script>