@extends('admin.layouts.template')
@section('title','Followup & Metting')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
 <?php if( Session::has('message')) : 
    $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
 ?>
    <div class="alert {{ $alertType }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session('message') }}
    </div>
<?php endif; ?>


<div class="x_panel">
    <div class="x_title">
    <h2>Followups & Mettings</h2>
    <div class="clearfix"></div>
</div>   
        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['method' => 'post', 'route' => 'followup.getDueMeetings', 'files' => 'true', 'class' => 'form-horizontal']) !!}

         <div class="col-sm-4">   
            <div class="form-group">
                {!! Form::label('from_date', 'From date') !!}
                {!! Form::text('from_date', old('from_date'), ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'from_date']) !!} 
            </div>
        </div>

        <div class="col-sm-4">   
            <div class="form-group">
                {!! Form::label('to_date', 'To date') !!}
                {!! Form::text('to_date', old('to_date'), ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'to_date']) !!} 
            </div>
        </div>

        <div class="col-sm-4">   
            <div class="form-group">
                {!! Form::label('user', 'User id') !!}
                <?php 
                    $ccdArr[''] = 'Choose one';
                    foreach ($ccd as $key => $c) {
                        $ccdArr[$c->id] = $c->name;
                    }
                ?>
                {!! Form::select('user', $ccdArr, old('user'), ['class' => 'form-control', 'id' => 'user']) !!} 
            </div>
        </div>

        <div class="col-sm-12">   
            <div class="form-group">
                {!! Form::submit("Save", ['class' => 'btn btn-default pull-right']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <div class="clearfix"></div>
        <div class="row">
        
        <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
            <table class="table table-bordered table-hover table-striped table-condenced" id="datatable">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Meeting Date</th>
                        <th>Reg. No</th>
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                    </tr>
                </thead>
                <tbody>
                @php($i = 1)
                @if(isset($followup))
                    @foreach($followup as $follow)
                        @php($loop1 = 1)
                        @while($loop1 <= 2)
                            @if($loop1 == 1)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $follow->next_meeting_date }}</td>
                                    <td>{{ $follow->registeration_no }}</td>
                                    <td>{{ $follow->meeting_detail }}</td>
                                    <td>{{ $follow->meeting_done_by }}</td>
                                </tr>
                            @else 
                                <tr>
                                    <td colspan="3"></td>
                                    <td>{{ $follow->next_meeting_agenda  }}</td>
                                    <td>{{ $follow->next_meeting_by  }}</td>
                                </tr>
                            @endif
                        @php($loop1++)
                        @endwhile
                        @php($i++)
                    @endforeach
                @endif
                </tbody>
            </table>
            </div>
        </div>
    </div>

</div>
@endsection



@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    var meeting_status = null;
    $("input[name='meeting_status']").click(function() {
        meeting_status = this.value;

        if(meeting_status == 1)
        {
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
        }else{
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
        }
    });
    
    </script>
@endsection