@extends('admin.layouts.template')
@section('title','Add Receipt')

@section('bookings-active','active')
@section('pdc-active','active')
@section('all_pdc-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <style type="text/css">
    tbody.installmentAppend input{
        width: 100px;
    }
    tbody.installmentAppend select{
        width: 120px;
    }
    </style>
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Add New PDC</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
        <div class="clearfix"></div>
       
       <form method="POST" action="{{ route('pdc.store') }}" class="form-horizontal" id="validate_form">

        @include('admin.pdc._form2')

        <div class="row">
            <div class="col-sm-12"> 
            <a href="#" class="btn btn-success btn-md pull-right" id="newEntry"> New Entry</a>
            <a href="#" class="btn btn-success btn-md pull-right" id="newEntry"> View Ledger</a>
             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Installment">Installments</a></li>
              {{-- <li><a data-toggle="tab" href="#other_payment">Other Payments</a></li> --}}
            </ul>

            <div class="tab-content">
              <div id="Installment" class="tab-pane fade in active">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table">
                    <thead>
                        <tr>
                            <th>Sr #</th>
                            <th>Registration No</th>
                            <th>Member Name</th>
                            <th>Payment Type</th>
                            <th>Installment No</th>
                            <th>OS Amount</th>
                            <th>Received Amount</th>
                            <th>Receipt #</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="installmentAppend"></tbody>
                    <input type="hidden" id="count" value="1">
                </table>
                </div>
             </div>

              {{-- <div id="other_payment" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table">
                    <thead>
                        <tr>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Inst. Amount</th>
                            <th>%age</th>
                            <th>Rebat Amount</th>
                            <th>Remaining Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="OtherinstallmentAppend"></tbody>
                </table>
                </div>
             </div> --}}
            </div>

        </div>
        </div> 
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
      {{ csrf_field() }}
        {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
   </div>
    </form>

</div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

    $(document).on('blur','.reg_no',function(){          
        var count = $('#count').val()-1;
        var reg_no = $(this).val();  
        $.ajax({
           url: '{{ URL::to('/findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person'+count).val(data.person.name);
                $('#ledger'+count).attr('href', " {{ URL::to('/admin/booking/ledger/') }}"+'/'+data.payment.id );
            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });


    $(document).on('blur','.installments',function(){          
        var count = $('#count').val()-1;
        var installment_no = $(this).val();
        var reg_no = $('.registration_no'+count).val();  
        var member_installment_id = $('#member_installment_id'+count).val();  

        $.ajax({
           url: '{{ URL::to('/getInstallment') }}',
            type: 'POST',
            data: {'registration_no': reg_no, 'installment_no' : installment_no},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#os_amount'+count).val(parseInt(data.installment.due_amount) - parseInt(data.installment.os_amount));
                $('#member_installment_id'+count).val(data.installment.id);
            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });

    $('#newEntry').click(function(event) {
        event.preventDefault();
        var count = $('#count').val();
        $('.installmentAppend').append("<tr> <td>"+count+"</td> <td><input type='text' name='registration_no[]' class='registration_no"+count+" reg_no '></td> <td><input type='text' name='person' id='person"+count+"' readonly='readonly'></td> <td> @php($payment_typesArr = [])<select class='form-control' name='payment_type'><option value=' disabled='></option>@foreach($payment_types as $payment_type)<option value='{{ $payment_type->id }}'>{{ $payment_type->payment_type }}</option>@endforeach</select> </td> <td><input type='text' class='installments' name='installment_no[]' id='installment_no"+count+"' required></td> <td><input type='text' name='os_amount[]' id='os_amount"+count+"'></td> <td><input type='text' name='received_amount[]' id='received_amount[]' required></td> <td><input type='text' readonly name='receipt_no[]' id='receipt_no[]'></td><td><a class='btn btn-default btn-xs' id='ledger"+count+"' target='_blank' href=''>...</a></td><input type='hidden' name='member_installment_id[]' id='member_installment_id"+count+"'></tr>");
        var countNew = parseInt(count) + 1;
        $('#count').val(countNew);
    });



    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");


    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#img-preview').attr('src','../../public/images/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });

    $("#validate_form").validate({
            rules: {
                registration_no: {

                    required:true,
                    registration_no:true
                },
                instrument_type:{
                    required:true,
                    instrument_type:true
                },
                instrument_date:{
                    required:true,
                    instrument_date:true
                },
                remarks: {
                    required: true,
                    lettersonly:true
                },
                
                amount: {
                    required: true,
                    number: true
                },
               drawee_bank: {
                    required: true,
                    lettersonly:true
                    
                }
               
            }
        });
        
    </script>
@endsection

