@extends('admin.layouts.template')
@section('title','Clearning Verification and Posting')

@section('bookings-active','active')
@section('pdc-active','active')
@section('verify-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Verify'])
@include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Clearning Verification and Posting</h2>
            
            <div class="clearfix"></div>
          </div>    
        {!! Form::open(['route' => 'pdc.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('instrument_date', 'Instrument Date')!!}
            {!! Form::text('instrument_date', null , array('class'=>'form-control datepicker') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('deposit_date', 'Deposite Date')!!}
            {!! Form::text('deposit_date', null , array('class'=>'form-control datepicker') )!!}
            </div>
        </div>

        <div class="col-md-1" style="margin-top:20px;">{!! Form::submit('search',array('class'=>'form-control btn-default pull-right') )!!}</div>

        <div class="col-md-3" >
            
        </div>        
                
            {!! Form::close() !!}


        <div class="clearfix"></div>
       
        <div class="row">
    <div class="col-sm-12"> 
            <table class="table table-bordered table-hover jambo_table">
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Ack #</th>
                        <th>Drawee Bank</th>
                        <th>Deposit Date</th>
                        <th>Deposit Bank</th>
                        <th>Deposit A/c</th>
                        <th>Inst. No</th>
                        <th>Inst. Value</th>
                        <th>Clear Return </th>
                        <th>Clear/Return Date</th>
                        <th>Inst. type</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {!! Form::open(['route' => 'pdc.postVerify', 'class' => 'form-horizontal']) !!}

                    <?php $i = 1;?>
                    @foreach($pdcs as $pdc)
                        <tr>
                            {!! Form::hidden('pdc_id[]', $pdc->id) !!}
                            <td>{{ $i }}</td>
                            <td>{{ $pdc->acknowledgement_no }}</td>
                            <td>{{ \App\bank::bankInfo($pdc->drawee_bank)->short_name }}</td>
                            <td>{{ date('d-m-Y',strtotime($pdc->deposit_date)) }}</td>
                            <td>{{ $pdc->deposit_bank }}</td>
                            <td>{{ $pdc->deposit_acc }}</td>
                            <td>{{ $pdc->reference_no }}</td>
                            <td>{{ $pdc->amount }}</td>
                            <td>
                                <label>
                                    <input type="radio" name="clear_posted[]{{$i}}" id="inputDeposit_pending" checked="checked" value="0">
                                    Clear
                                </label>
                            
                                <label>
                                    <input type="radio" name="clear_posted[]{{$i}}" id="inputDeposit_pending" value="1">
                                    Posted
                                </label>
                            </td>
                            <td>
                                 {{ date('d-m-Y',strtotime($pdc->clear_return_date)) }}
                            </td>
                            <td>
                                {{ $pdc->instrument_type }}
                            </td>
                            <td>
                                <a href="{{ route("pdc.postReceipt", $pdc->id) }}" class="btn btn-default btn-xs">Post</a>
                            </td>
                        </tr>

                        <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
            {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}

                    {!! Form::close() !!}
        </div>

    </div></div>
        
@endsection


@section('javascript')

    
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    </script>

@endsection
