@extends('admin.layouts.template')
@section('title','Acknowledgement')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    @include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Acknowledgement</h2>
            
            <div class="clearfix"></div>
          </div>    
        {!! Form::open(['route' => 'pdc.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                 
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('ins-rec-no', 'Ins Receipt #') !!}
                {!! Form::text('ins-rec-no', null , array('required' => 'required' , 'class'=>'form-control')  )!!}
            </div>
        </div>

 
 <div class="form-group">
                    {!! Form::submit('Print', ['class' => 'btn btn-primary']) !!}
                </div>
{!! Form::close() !!}
        </div>

        <div class="clearfix"></div>

        
       
        
        
@endsection
