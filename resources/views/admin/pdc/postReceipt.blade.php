@extends('admin.layouts.template')
@section('title','Add Receipt')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Post PDC Receipt</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
        <div class="clearfix"></div>
       
       <form method="POST" action="{{ route('pdc.receiptPosting') }}" class="form-horizontal">


        <div class="row">
            <div class="col-sm-12"> 

            <div>
              <div id="Installment">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Registration No</th>
                            <th>Member Name</th>
                            <th>Inst. No</th>
                            <th>Received Amount</th>
                            <th>Receipt No</th>
                        </tr>
                    </thead>
                    <tbody class="installmentAppend">
                        <?php $i = 1; ?>
                        {{-- {{ dd($pdc_inst) }} --}}
                        {!! Form::hidden('pdc_installment_id', $pdcs->id) !!}
                        @foreach($pdcs->installments as $installment)
                        {{-- @php(dd($installment)) --}}
                                {!! Form::hidden('installment_id[]', $installment->installment_id) !!}
                                {!! Form::hidden('installment_amount[]', $installment->installment_amount) !!}
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $installment->registration_no }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ \App\memberInstallments::getInstallment($installment->installment_id)->installment_no }}</td>
                                <td>{{ $installment->received_amount}}</td>
                                <td>{!! Form::text('receipt_no[]', null,['required' => 'required']) !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
             </div>
            </div>
        </div>
        </div>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
      {{ csrf_field() }}
        {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
   </div>
    </form>

</div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });
        
    </script>
@endsection

