     
        <div id="data">
            <div id="member" style="clear: both">  

        <div class="col-md-6">
            <?php 
                $reg_status[''] =  "";
                $reg_status['cheque'] =  "Cheque";
                $reg_status['dd'] =  "Demand Draft";
                $reg_status['po'] =  "Pay Order";
            ?>
            <div class="form-group">
            {!! Form::label('instrument_type', 'Instrument Type')!!}
            {!! Form::select('instrument_type', $reg_status, 0, ['class' => 'form-control']) !!}
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('received_date', 'Receiving Date')!!}
            {!! Form::text('received_date', null , array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('instrument_date', 'Instrument Dated')!!}
            {!! Form::text('instrument_date', null , array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('drawee_bank', 'Drawee Bank') !!}
            @php($drawee_bank = [])
            @php($drawee_bank[] = '')
            @foreach($banks as $bank)
                @php($drawee_bank[$bank->id] = $bank->bank_name)
            @endforeach
            {!! Form::select('drawee_bank', $drawee_bank , 0, ['required' =>'required', 'class' => 'form-control' , 'id' => 'drawee_bank']) !!}            
            </div>         
        </div>
        

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('amount', 'Amount')!!}
            {!! Form::text('amount', null , array('required' => 'required'  , 'class'=>'form-control amount' , 'id' => 'amount') )!!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('reference_no', 'Instrument No')!!}
            {!! Form::text('reference_no', null , array('class'=>'form-control amount') )!!}
            </div>
        </div>

       

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('remarks', 'Remarks')!!}
            {!! Form::text('pdc_remarks', null , array('required' => 'required'  , 'class'=>'form-control' , 'id' => 'remarks') )!!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            {!! Form::label('deposit_by', 'Deposite By & Mobile')!!}
            {!! Form::text('deposit_by', null , array( 'class'=>'form-control') )!!}
            </div>
        </div>
{{-- 
        <div class="col-md-6">
            <div class="form-group">
                    <label>Receiving Type</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="receiving_type" id="installment" value="1" checked="checked">
                            Inst
                        </label>
                        <label>
                            <input type="radio" name="receiving_type" id="other" value="2">
                            Other
                        </label>
                    </div>
                </div>
        </div> --}}
        
            </div>
        </div>