@extends('admin.layouts.template')
@section('title','Mark Clearning and Return ')

@section('bookings-active','active')
@section('pdc-active','active')
@section('mark_clear-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

@include('admin/common/breadcrumb',['page'=>'Mark Clear'])

@include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Mark Clearning and Return</h2>
            
            <div class="clearfix"></div>
          </div>    
        {!! Form::open(['route' => 'pdc.mark', 'method' => 'get', 'class' => 'form-horizontal']) !!}

        <div class="col-md-3">
            <div class="form-group">
            {!! Form::label('instrument_no', 'Instrument No')!!}
            {!! Form::text('instrument_no', null , array('class'=>'form-control') )!!}
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
            {!! Form::label('instrument_date', 'Instrument Date')!!}
            {!! Form::text('instrument_date', null , array('class'=>'form-control datepicker') )!!}
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
            {!! Form::label('deposit_date', 'Deposite Date')!!}
            {!! Form::text('deposit_date', null , array('class'=>'form-control datepicker') )!!}
            </div>
        </div>
        <div class="col-md-1" style="margin-top:20px;">{!! Form::submit('search',array('class'=>'form-control btn-default pull-right') )!!}</div>

        <div class="col-md-2" >
            
        </div>        
                
            {!! Form::close() !!}


        <div class="clearfix"></div>
       
        <div class="row">
    <div class="col-sm-12"> 
            <table class="table table-bordered table-hover jambo_table">
                <thead>
                    <tr>
                        <th>Ack #</th>
                        <th>Ins No</th>
                        <th>Ins Value</th>
                        <th>Drawee Bank</th>
                        <th>Deposit Date</th>
                        <th>Deposit Bank</th>
                        <th>Deposit A/c</th>
                        <th>Clear/Return/Deposit</th>
                        <th>Clear/Ret Date</th>
                    </tr>
                </thead>
                <tbody>
                {!! Form::open(['route' => 'pdc.markClearence', 'class' => 'form-horizontal']) !!}

                    <?php $i = 1;?>
                    @foreach($pdcs as $pdc)
                        <tr>
                            {!! Form::hidden('pdc_id[]', $pdc->id) !!}
                            <td>{{ $pdc->acknowledgement_no }}</td>
                            <td>{{ $pdc->reference_no }}</td>
                            <td>{{ $pdc->amount }}</td>
                            <td>{{ \App\bank::bankInfo($pdc->drawee_bank)->short_name }}</td>
                            <td>{{ date('d-m-Y',strtotime($pdc->deposit_date)) }}</td>
                            <td>{{ $pdc->deposit_bank }}</td>
                            <td>{{ $pdc->deposit_acc }}</td>
                            <td>
                                <label>
                                    <input type="radio" name="is_clear_return[]{{$i}}" id="inputDeposit_pending" value="1">
                                    Clear
                                </label>
                            
                                <label>
                                    <input type="radio" name="is_clear_return[]{{$i}}" id="inputDeposit_pending" value="0">
                                    Return
                                </label>

                                <label>
                                    <input type="radio" name="is_clear_return[]{{$i}}" checked="checked" id="inputDeposit_pending" value="2">
                                    Deposit
                                </label>
                            </td>
                            <td>
                                {!! Form::text('clear_return_date[]'.$i, date('d/m/Y'), ['class' => 'datepicker' , 'id' => 'mark_datapicker'.$i]) !!}
                            </td>
                           {{--  <td>
                                {!! Form::text('pdc_remarks', '' , ['id' => 'mark_datapicker'])  !!}
                            </td> --}}
                        </tr>

                        <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
            {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}

                    {!! Form::close() !!}
        </div>

    </div></div>
        
@endsection


@section('javascript')

    
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    </script>

@endsection
