@extends('admin.layouts.template')
@section('title','PDC')

@section('bookings-active','active')
@section('pdc-active','active')
@section('all_pdc-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">PDC List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">PDC List</li>
        </ol>
    </div>
    
    	 <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>


               <div class="x_panel">
        <div class="x_title">
           <h2>Add New PDC</h2>
                <div class="actions" style="float: right; display: inline-block; {{ $rights->show_create }}">
                    <a href="{{ route('pdc.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                </div>
                <div class="clearfix"></div>
          </div>   
        <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table" id="datatable">
                    <thead>
                        <tr>
                            <th>Receipt #</th>
                            <th>Amount Paid</th>
                            <th>Receiving Mode</th>
                            <th>Receiving Date</th>
                            <th>Drawee Bank</th>
                             <th>Instrument No</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pdc as $pdc)
<tr>
                        <td>{{ $pdc->acknowledgement_no}}</td>
                        <td>{{ $pdc->amount}}</td>
                        <td>{{ $pdc->instrument_type}}</td>
                        <td>{{ $pdc->receiving_date}}</td>
                        <td>{{ $pdc->drawee_bank}}</td>
                        <td>{{ $pdc->reference_no}}</td>
                        <td><a href="{{ route('pdc.print-ack', $pdc->id) }}" class="btn btn-default btn-xs">Print ACK</a></td>
                               
                                <td> 
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['pdc.destroy', $pdc->id]]) !!}
                                        {!! Form::submit("Delete", ['class' => 'btn-danger btn btn-xs DeleteBtn']) !!}
                                    {!! Form::close() !!}
                                </td>
                        </tr>
                    </tbody>
@endforeach
                    </table>
                </div>

        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });

    $('#payment_schedule').change(function(event) {
        var planId = $(this).val();
        $.ajax({
            url: '{{ URL::to('/PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
    });

    var inputs = $('#data input, #data select').attr('disabled','disabled');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){
                var $inputs = $('#data input');
                var $persons = $('#persons input');

                $('#data input, #data select').removeAttr('disabled','disabled');
                $('#data input#cnic').attr('disabled','disabled');
                $('#data input#person').attr('disabled','disabled');
                $('#data input#person').val(result.person.name);
                $('#cnic_field').val($('#cnic').val());
                $('#img-preview').attr('src','../public/images/'+result.person.picture);

                $.each(result.member, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

                $.each(result.person, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

