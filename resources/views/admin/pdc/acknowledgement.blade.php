<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Acknowledgement</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}

</style>

       
    </head>

<body>
    <div id="app">
        
            <div class="container">
                <div class="leadger">
<div class="row">
<div class="col-xs-6">
                
                <img src="{{ asset('public/')}}/images/capital.png" alt="" class="img-responsive" style="width:33%;display:block;margin:0 auto;">
                </div>

                    <div class="col-xs-6">
                        <h5>Address:</h5>
                       <h6>G.T Road, Near Manawa Police Station,Lahore </h6>
                        <h5>Office Contacts:</h5>
                       <h6> +92-42-36583301</h6>
                        <h6>UAN: 111-724-336</h6>
                       <h6> Cell: 0322-5724-336</h6>






                    </div>
                    
                    </div>

                   
                     <hr>
                      
                        
<div class="row">
                    <div class="col-xs-12"><h3><u>Cheque Acknowledgement</u></h3></div>
                    <div class="col-xs-12"><h5>Acknowledgement No:  {{ $ack1->acknowledgement_no }}</h5></div>
                    <div class="col-xs-6">
                        <h5>Receiving Date: {{ date('d-m-Y',strtotime($ack1->receiving_date)) }}</h5>
                       <h5> Instrument Type: {{ $ack1->instrument_type }}</h5>
                        <h5>Drawee Bank: {{ $ack1->drawee_bank }}</h5>
                      
                         </div>
                    <div class="col-xs-6">
                        <h5>Instrument Date: {{ date('d-m-Y',strtotime($ack1->instrument_date)) }}</h5>
                       <h5> Instrument No: {{ $ack1->reference_no }}</h5>
                        <h5>Instrument Value: {{ $ack1->amount }}</h5>
                      


                    </div>
                    </div>
                    
                    <hr>

                   


                    </br>
                    <div class="row">
                        <div class="col-xs-12">
                            
                          <table class="table table-bordered">
                          <thead >
                              <tr style="border-bottom: 3px solid;border-top:3px solid;">
                                  <th>Sr No</th>
                                   <th>Registration No</th>
                                    <th>Installment No</th>
                                    <th>Member Name</th>
                                     <th>Amount</th>
                                      


                              </tr></thead>
                              
                           <tbody>
                            @php($i=1)
                            @php($sum=0)
                             @foreach($ack1->installments as $installment)
                             <tr>
                               <td>{{ $i++ }}</td>
                               <td>{{ strtoupper($installment->registration_no) }}</td>
                               <td>{{ \App\memberInstallments::getInstallment($installment->installment_id)->installment_no }}</td>
                               <td>{{ $member->user->name}}</td>
                               <td>{{ $installment->received_amount }}</td>
                            </tr>
                            @php($sum+=$installment->received_amount)
                            @endforeach
                <tr>
                  <td colspan="4"><strong><i>Total:</i></strong></td>
                  <td colspan="2" ><strong><i>{{ $sum }}</i></strong></td>


                </tr>



                           </tbody>







                          </table>
                          </div>
                          <div class="col-xs-12">
                          <p><i>This is only an acknowledgement of receiving the cheque for date stated on it but not the amount of money stated there in. The cheque will be presented for encashment and only after realization of cash amount of cheque the receipt will be issued, which may be collected from the office of Capital City thereafter.</i></p>
                          </div>
                          </div>
                          



                </div>
                  
               
            </div>
       

       
        

    </body>

</html>
