@extends('admin.layouts.template')
@section('title','Booking')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Edit Booking</h2>
            
            <div class="clearfix"></div>
          </div>   
       
     {!! Form::model($BookingDetail['person'], ['method' => 'POST', 'route' => ['booking.update', $BookingDetail['booking']->id], 'class' => 'form-horizontal']) !!}
       
        <div id="data">
            <div id="member" class="col-sm-6" style="clear: both">

                <div class="form-group">
                {!! Form::label('picture', 'picture', ['class' => 'col-sm-12'])!!}
               <div class="col-md-12">
                <img id="img-preview" src="{{ asset('public/') }}/images/{{ $BookingDetail['person']->picture }}"  style="margin: 10px auto;" alt="Picture" height="100" />
                </div>
                </div>
                
                <div class="form-group">
                {!! Form::label('person', 'Person')!!}
                {!! Form::text('person', $BookingDetail['person']->name , ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
                </div>


                <div class="form-group{{ $errors->has('reference_no') ? ' has-error' : '' }}">
                {!! Form::label('reference_no', 'Reference No')!!}
                {!! Form::text('reference_no', $BookingDetail['member']->reference_no , ['class' => 'form-control', 'id' => 'reference_no']) !!}
                </div>


                <div class="form-group">
                {!! Form::label('payment_schedule', 'Payment Plan') !!}
                   <?php 
                        $paymentScheduleArr[''] = "";
                        foreach ($paymentSchedule as $plan) {
                             $paymentScheduleArr[$plan->id] = $plan->payment_code;
                         } 
                    ?>
                    {!! Form::select('payment_schedule_id', $paymentScheduleArr, $BookingDetail['booking']->payment_schedule_id, ['class' => 'form-control', 'id' => 'payment_schedule']) !!} 
                </div>

                <div class="form-group">
                {!! Form::label('payment_date', 'Payment Date')!!}
                {!! Form::text('payment_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'payment_date']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('Booking_Price', 'Booking Price')!!}
                {!! Form::text('booking_price', $BookingDetail['booking']->paymentPlan->booking_price, ['class' => 'form-control', 'id' => 'Booking_Price', 'readonly' => 'readonly']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('booking_percentage', 'booking_percentage')!!}
                {!! Form::text('booking_percentage', $BookingDetail['booking']->paymentPlan->booking_percentage, ['class' => 'form-control', 'id' => 'booking_percentage', 'readonly' => 'readonly']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('gurdian_name', 'Father Name')!!}
                {!! Form::text('gurdian_name', null, ['class' => 'form-control', 'id' => 'gurdian_name']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('cnic', 'CNIC')!!}
                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic']) !!}
                </div>

                 <div class="form-group">
                {!! Form::label('nationality', 'nationality')!!}
                {!! Form::text('nationality', null, ['class' => 'form-control', 'id' => 'nationality']) !!}
                </div>

                {{-- {{ dd($BookingDetail['booking']->featureset) }} --}}
                <div class="form-group">
                {!! Form::label('feature_set', 'Features Set') !!}
                   @foreach($featureset as $featur => $feature)
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox("feature_set_id[]", $feature->id) !!}
                                {{$feature->feature}}
                            </label>
                        </div>
                    @endforeach
                </div>

            </div>

        <div id="persons" class="col-sm-5 col-sm-offset-1" style="margin-top: 150px;">
            

            <div class="form-group">
            {!! Form::label('current_address', 'current_address')!!}
            {!! Form::text('current_address', $BookingDetail['person']->current_address, ['class' => 'form-control', 'id' => 'current_address', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('country', 'country')!!}
            {!! Form::text('country_id', $BookingDetail['person']->country, ['class' => 'form-control', 'id' => 'country', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('city', 'city')!!}
            {!! Form::text('city_id', $BookingDetail['person']->city, ['class' => 'form-control', 'id' => 'city', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_ofc', 'phone ofc')!!}
            {!! Form::text('phone_office', $BookingDetail['person']->phone_office, ['class' => 'form-control', 'id' => 'phone_ofc', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_res', 'Phone Residential')!!}
            {!! Form::text('phone_res', $BookingDetail['person']->phone_res, ['class' => 'form-control', 'id' => 'phone_res', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_mobile', 'phone_mobile')!!}
            {!! Form::text('phone_mobile', $BookingDetail['person']->phone_mobile, ['class' => 'form-control', 'id' => 'phone_mobile', 'readonly' => 'readonly']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('email', 'email')!!}
            {!! Form::text('email', $BookingDetail['person']->email, ['class' => 'form-control', 'id' => 'email', 'readonly' => 'readonly']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('payment_remarks', 'Payment Remarks') !!}
                {!! Form::text('payment_remarks', $BookingDetail['booking']->payment_remarks , ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('agent', 'Agent') !!}
                   <?php 
                        $agentArr[''] = "";
                        foreach ($agents as $agent) {
                             $agentArr[$agent->id] = $agent->name;
                         } 
                    ?>
                    {!! Form::select('agent_id', $agentArr, $BookingDetail['booking']->agent_id, ['class' => 'form-control', 'id' => 'agent']) !!} 
                </div>
               </div>

               {{-- <div class="col-sm-12">
                  {{ csrf_field() }}
                    {!! Form::hidden('cnic', null, ['id' => 'cnic_field']) !!}
                    {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
               </div> --}}

        </div>

        {!! Form::close() !!}
        <div class="row">
                  <div id="Installment" class="col-sm-12 tab-pane fade in active">
                    <div style="overflow-x: scroll;">
                    <table class="table table-bordered table-hover table-striped table-condenced">
                        <thead>
                            <tr>
                                <th>Inst. No</th>
                                <th>Inst. Date</th>
                                <th>Inst. Amount</th>
                                <th>%age</th>
                                <th>Rebat Amount</th>
                                <th>Received Amount</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($BookingDetail['installmentPlan'] as $installmentPlan)
                                 <tr> 
                                    <td class='sr_no'><input type='text' name='installment_no[]' readonly value='{{ $installmentPlan->installment_no}}'></td>
                                    <td><input class="datepicker2" name="installment_date[]" readonly type="text" value="{{ $installmentPlan->installment_date}}"></td>
                                    <td><input class="" name="installment_amount[]" readonly type="text" value="{{ $installmentPlan->installment_amount}}"></td>
                                    <td><input class="" name="percentage[]" readonly type="text" value="{{ $installmentPlan->amount_percentage}}"></td>
                                    <td><input class="" name="rebat_amount[]" readonly type="text" value="{{ $installmentPlan->installment_amount}}"></td>
                                    <td><input class="" name="amount_received[]" readonly type="text" value="{{ $installmentPlan->amount_received}}"></td>
                                    <td><input class="" name="installment_remarks[]" readonly type="text" value="{{ $installmentPlan->remarks}}"></td>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                 </div>
                </div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

    $('#payment_schedule').change(function(event) {
        var planId = $(this).val();
        $.ajax({
            url: '{{ URL::to('/PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
    });

    var inputs = $('#data input, #data select').attr('disabled','disabled');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){
                var $inputs = $('#data input');
                var $persons = $('#persons input');

                $('#data input, #data select').removeAttr('disabled','disabled');
                $('#data input#cnic').attr('disabled','disabled');
                $('#data input#person').attr('disabled','disabled');
                $('#data input#person').val(result.person.name);
                $('#cnic_field').val($('#cnic').val());
                $('#img-preview').attr('src','../../public/images/'+result.person.picture);

                $.each(result.member, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

                $.each(result.person, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

