@extends('admin.layouts.template')
@section('title','Send for Clearing ')

@section('bookings-active','active')
@section('pdc-active','active')
@section('send_clear-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

@include('admin/common/breadcrumb',['page'=>'Send Clear'])
    @include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Send for Clearing</h2>
            
            <div class="clearfix"></div>
          </div>    
        {!! Form::open(['route' => 'pdc.send', 'method' => 'get', 'class' => 'form-horizontal']) !!}

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('instrument_no', 'Instrument No')!!}
            {!! Form::text('instrument_no', null , array('class'=>'form-control') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('instrument_date', 'Instrument Date')!!}
            {!! Form::text('instrument_date', null , array('class'=>'form-control datepicker') )!!}
            </div>
        </div>

         <div class="col-md-1" style="margin-top:20px;">{!! Form::submit('search',array('class'=>'form-control btn-default') )!!}</div>

        <div class="col-md-3" >
            
        </div>        
                
            {!! Form::close() !!}


        <div class="clearfix"></div>
       
        <div class="row">
    <div class="col-sm-12"> 
            <table class="table table-bordered table-hover jambo_table">
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Ack #</th>
                        <th>Instr. No</th>
                        <th>Instr. Date</th>
                        <th>Instr. Value</th>
                        <th>Drawee Bank</th>
                        <th>Pending/Deposit</th>
                        <th>Deposit Date</th>
                        <th>Deposit Bank</th>
                        <th>Deposit A/c</th>
                    </tr>
                </thead>
                <tbody>
                {!! Form::open(['route' => 'pdc.sendClearence', 'class' => 'form-horizontal']) !!}

                    <?php $i = 1;?>
                    @foreach($pdcs as $pdc)
                        <tr>
                            {!! Form::hidden('pdc_id[]', $pdc->id) !!}
                            {!! Form::hidden('drawee_bank[]', $pdc->drawee_bank) !!}
                            <td>{{ $i }}</td>
                            <td>{{ $pdc->acknowledgement_no }}</td>
                            <td>{{ $pdc->reference_no }}</td>
                            <td>{{ date('d-m-Y', strtotime($pdc->instrument_date)) }}</td>
                            <td>{{ $pdc->amount }}</td>
                            <td>{{ \App\bank::bankInfo($pdc->drawee_bank)->short_name }}</td>
                            <td>
                                <label>
                                    <input type="radio" name="deposit_pending{{$pdc->id}}" id="inputDeposit_pending" value="0">
                                    Pending
                                </label>
                            
                                <label>
                                    <input type="radio" name="deposit_pending{{$pdc->id}}" id="inputDeposit_pending" value="1">
                                    Deposit
                                </label>
                            </td>
                            <td>
                                {!! Form::text('deposit_date[]', date('d/m/Y'), ['class' => 'datepicker']) !!}
                            </td>
                            <td>
                                <?php $bankArr[''] = 'Choose one'  ?>
                                    @foreach($banks as $bank)
                                        <?php $bankArr[$bank->short_name] = $bank->short_name;  ?>
                                    @endforeach
                                    {!! Form::select('deposit_bank[]', $bankArr, old('deposit_bank') , ['required' =>'required']) !!}            
                            </td>

                            <td>
                                <?php $accNoArr[''] = 'Choose one'  ?>
                                    @foreach($banks as $bank)
                                        <?php $accNoArr[$bank->account] = $bank->account;  ?>
                                    @endforeach
                                    {!! Form::select('deposit_acc[]', $accNoArr, old('deposit_acc') , ['required' =>'required']) !!}            
                            </td>
                        </tr>

                        <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
            {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}

                    {!! Form::close() !!}
        </div>

    </div></div>
        
@endsection


@section('javascript')

    
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    </script>

@endsection
