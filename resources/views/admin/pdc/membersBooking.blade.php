@extends('admin.layouts.template')
@section('title','Booking')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    	
    	 <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>


               <div class="x_panel">
        <div class="x_title">
            <h2>All Bookings of {{ Auth::user()->name }}</h2>
            
            <div class="clearfix"></div>
          </div>   
        <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>#Id</th>
                            <th>Reg No</th>
                            <th>Member Name</th>
                            <th>Father Name</th>
                            <th>Plan Code</th>
                            <th>Plot Size</th>
                            <th>Total Price</th>
                            <th>Amount Paid</th>
                            <th>Dated</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        if(isset($BookingDetail['booking']))
                        {
                             foreach ($BookingDetail['booking'] as $bookings => $booking) {?>
                            <tr>
                                <th>{{ $booking->id }}</th>
                                <th>{{ $booking->registration_no }}</th>
                                <th>{{ $BookingDetail['person'][$bookings]->name}}</th>
                                <th>{{ $BookingDetail['person'][$bookings]->gurdian_name}}</th>
                                <th>{{ $BookingDetail['paymentPlan'][$bookings]->payment_code}}</th>
                                <th>{{ $BookingDetail['PlotSize'][$bookings]->plot_size}}</th>
                                <th>{{ $BookingDetail['paymentPlan'][$bookings]->total_price}}</th>
                                <th>0</th>
                                <th>{{ $booking->created_at->format('d-m-Y')}}</th>
                            </tr>
                    <?php } 
                        }else{?>
                                <p>No Record Found</p>
                        <?php }?>
                    </tbody>
                </table>
                </div>

        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

    $('#payment_schedule').change(function(event) {
        var planId = $(this).val();
        $.ajax({
            url: '{{ URL::to('/PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
    });

    var inputs = $('#data input, #data select').attr('disabled','disabled');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){
                var $inputs = $('#data input');
                var $persons = $('#persons input');

                $('#data input, #data select').removeAttr('disabled','disabled');
                $('#data input#cnic').attr('disabled','disabled');
                $('#data input#person').attr('disabled','disabled');
                $('#data input#person').val(result.person.name);
                $('#cnic_field').val($('#cnic').val());
                $('#img-preview').attr('src','../public/images/'+result.person.picture);

                $.each(result.member, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

                $.each(result.person, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

