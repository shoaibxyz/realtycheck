@extends('admin.layouts.template')
@section('title','Agency')

@section('bookings-active','active')
@section('agencies-active','active')
@section('agency-active','active')

@section('content')
        
        
        @if(!empty($message))
            <p>{{$message}}</p>
        @endif
        <div class="col-md-12">
         <div class="panel panel-default">
                <div class="panel-heading">List of all Principal Officers and Sub Agents</div>
                    @include("errors")
                <div class="panel-body">

        <h2>Agency Name: <strong>{{ $agency->name}}</strong></h2>
        </br>
        


        <div class="col-md-6">
            <h3>Principal Officers</h3>
            <table class="table">
                <thead>
                    <tr>
                        <td>Sr. No</td>
                        <td>Name</td>
                        {{-- <td>Image</td> --}}
                        <td>CNIC</td>
                        <td>Phone</td>
                    </tr>
                </thead>  
                <tbody>
                    @php($i =1)
                     @foreach ($principal as $principal)
                        <tr>
                            <td>{{ $i++ }}</td>
                            {{-- <td><img src="{{ URL::to('public/images/').$principal->picture }}" width="60" height="60"></td> --}}
                            <td>{{ $principal->name }}</td>
                            <td>{{ $principal->cnic }}</td>
                            <td>{{ $principal->cell }}</td>
                        </tr>
                     @endforeach
                </tbody>
            </table>

        </div>
        <div class="col-md-6">
            
<h3>Sub Agents</h3>
<table class="table">
    <thead>
        <tr>
            <td>Sr. No</td>
            <td>Name</td>
            <td>CNIC</td>
            <td>Phone</td>
        </tr>
    </thead>  
    <tbody>
        @php($i =1)
         @foreach ($agents as $agent)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $agent->person->name }}</td>
                <td>{{ $agent->person->cnic }}</td>
                <td>{{ $agent->person->cell }}</td>
            </tr>
         @endforeach
    </tbody>
</table>
</div>

</div></div></div>
@endsection
