@extends('admin.layouts.template')

@section('title','Agency')

@section('bookings-active','active')
@section('agencies-active','active')
@section('agency-active','active')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Agencies List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Agencies List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Agencies List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('agency.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th style="width: 5%">#Id</th>
                            <th style="width: 25%">Name</th>
                            <th style="width: 20%">Reg No</th>
                            <th style="width: 20%">Contact No</th>
                            <th class="text-center">Actions</th>
                           
                            <th style="width: 20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($Agency as $agen)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $agen->name }}</td>
                                <td>{{ $agen->agency_reg_no }}</td>
                                <td>{{ $agen->contact_no }}</td>
                                <td>
                                    <a href="{{ route('agency.edit', $agen->id )}}" class="btn btn-info btn-xs" style="{{ $rights->show_edit }}"><i class="fa fa-edit"></i></a>
                                    
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['agency.destroy', $agen->id] , 'style' => 'display:inline;']) !!}
                                    <button class="btn btn-danger btn-xs" type="submit" value="" style="{{ $rights->show_delete }}" title="delete"><i class="fa fa-trash-o"></i></button>
                                {!! Form::close() !!}
                                    </td>
                              
                                <td style="{{ $rights->show_view }}"><a href="{{ route('agency.view', $agen->id )}}" class="btn btn-success" style="{{ $rights->show_view }}">View Principal Officers and Sub Agents</a></td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
