@extends('admin.template')

@section('title','Add Agency')

@section('bookings-active','active')
@section('agencies-active','active')
@section('agency-active','active')

@section('content')
        
        <h2>Add </h2>    
        <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::open(['method' => 'POST', 'route' => 'agency.principal-officer', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Agency Name') !!}
                {!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('contact_no', 'Contact No') !!}
                {!! Form::text('contact_no', '', ['class' => 'form-control', 'id' => 'contact_no']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('address', 'Agency address') !!}
                {!! Form::text('address', '', ['class' => 'form-control', 'id' => 'address']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('cnic_copy', 'CNIC Picture') !!}
                {!! Form::file('cnic_copy', ['class' => 'form-control', 'id' => 'cnic_copy']) !!} 
            </div>
        
            <div class="form-group">
                {!! Form::submit("Add Agency", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>
@endsection