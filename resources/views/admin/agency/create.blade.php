@extends('admin.layouts.template')

@section('title','Add Agency')

@section('bookings-active','active')
@section('agencies-active','active')
@section('agency-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Agency</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Agency</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Agency</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'route' => 'agency.store', 'class' => 'form-horizontal','files' => true]) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Agency Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Contact No</label>
                        <div class="col-sm-8">
                            {!! Form::text('contact_no', '', ['class' => 'form-control', 'id' => 'contact_no']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Registeration No</label>
                        <div class="col-sm-8">
                            {!! Form::text('agency_reg_no', '', ['class' => 'form-control', 'id' => 'agency_reg_no']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-8">
                            {!! Form::text('address', '', ['class' => 'form-control', 'id' => 'address']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Logo</label>
                        <div class="col-sm-8">
                            {!! Form::file('cnic_copy', ['class' => 'form-control', 'id' => 'cnic_copy']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('agency.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
