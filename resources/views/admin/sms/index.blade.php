@extends('admin.layouts.template')
@section('title','All SMS')
@section('marketing-active','active')
@section('sms-active','active')
@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">SMS</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">SMS</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>All SMS</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('sms.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New SMS</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        	@include('errors')
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action" id="smsTable">
        		<thead>
        			<tr>
        				{{-- <th>Sr #</th> --}}
        				<th>Message</th>
        				<th>Event</th>
        				<th>Sending Date</th>
        				<th>Is Sent?</th>
                        <th>Sent To</th>
        			</tr>
        		</thead>
        		<tbody>
                    
        		</tbody>
        	</table>
            </div>	

            <div class="modal fade" id="sentTo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">SMS Detail</h4>
                    </div>
                    <div class="modal-body">
                        
                        <h2>Message:</h2>
                        <div id="smsDetail" ></div>
                   
                        <h2>Sent To:</h2>
                        <div id="SmsSentTo"></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">

        var columns = [
            { data: 'message', name: 'message' },
            { data: 'event', name: 'event' },
            { data: 'sending_date', name: 'sending_date' },
            { data: 'sent', name: 'sent' },
            { data: 'check', name: 'check' },
        ];
   

        $('#smsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bSort": false,
            "pageLength": 10,
            "info": false,
            "ajax": '{!! route('sms.getSMS') !!}',
            "columns": columns
        });


        $(document).on('click' ,'.check', function(e){
            $('#SmsSentTo').html("");
            $('#smsDetail').html("");
            var smsId =  $(this).attr('data-smsId');
            $.ajax({
                url: '{{ route('sms.checkSentTo') }}',
                type: 'POST',
                data: {sms_id: smsId},
                success: function(result){
                    
                    $.each(result.smsDetail, function(index, value){
                        $('#SmsSentTo').prepend(value.sent_to+"<br>");
                    });
                    $('#smsDetail').prepend(result.sms.message+"<br>");
                },
                error: function(xhr, status, responseText){
                    console.log(status);
                }
            });
            });
        // });
    </script>
@endsection