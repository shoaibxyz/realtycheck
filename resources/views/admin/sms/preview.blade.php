@extends('admin.layouts.template')
@section('title','Sms Preview')
@section('marketing-active','active')
@section('sms-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

 
@endsection
@section('content')
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>SMS Preview</h2>
                <div class="clearfix"></div>
                <p>Sending Date: {{ date('d-m-Y H:i', strtotime($sendingDate)) }}</p>
            </div>
            
            @include('errors')
            
            
            <div class="table-responsive">
              
              
                <table class="table table-striped jambo_table table-bordered bulk_action example" id="smstable">
                    <thead>
                      <tr>
                        <th>Sr #</th>
                        <th>Reg No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Message</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php($i = 1)
                      @if(isset($sms_detail['message']))
                      @foreach($sms_detail['message'] as $key => $message)
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $sms_detail['sent_to'][$key] }}</td>
                          <td>
                            <?php 
                              
                              $name = $users_name[$key];
                              
                            ?>
                            {{ $name }}
                          </td>
                          
                          <td>
                           
                            admin@lahorecapitalcity.com
                          </td>
                          <td>
                            <?php 
                              /*$number = explode('/', $users_phone[$key]);
                              $mobile = substr($number[0],0,1) == 0 ? '92'.substr($number[0],1) : $number[0];*/
                              $mobile = $users_phone[$key];
                              
                            ?>
                            {{ $mobile }}
                          </td>
                          
                          <td>
                            {{ $message }}
                          </td>
                        </tr>
                      @endforeach
                      @endif
                    </tbody>
                </table>
            </div>  

              {!! Form::open(['route' => 'sms.send', 'method' => 'post']) !!}
              @if(isset($sms_detail['message']))
                @foreach($sms_detail['message'] as $key2 => $message2)
                   <?php 
                      /*$number = explode('/', $users_phone[$key2]);
                      $mobile = substr($number[0],0,1) == 0 ? '92'.substr($number[0],1) : $number[0];*/
                      $mobile = $users_phone[$key2];
                  ?>   
                  {!! Form::hidden('mobile[]', $mobile) !!}
                  {!! Form::hidden('sent_to[]', $sms_detail['sent_to'][$key2]) !!}
                  {!! Form::hidden('message[]', $message2) !!}
                @endforeach
              {!! Form::hidden('sendnow', $sendNow) !!}
              {!! Form::hidden('dueInstallmentsInput', $dueInstallmentsInput) !!}
              {!! Form::hidden('sendingDate', $sendingDate) !!}
              {!! Form::submit('Send', ['class' => 'btn btn-success']) !!}  
              @endif
              {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    });
   
       var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];
      
    var datatable = $('#smstable').DataTable({ 
      "bSort" : false,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    
                    'csv',
                    
                ]
            }
        ]
    } );

    </script>
@endsection

