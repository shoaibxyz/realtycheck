@extends('admin.layouts.template')
@section('title','Send SMS')
@section('marketing-active','active')
@section('sms-active','active')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ URL::to('public/css/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Send SMS</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Send SMS</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>All SMS</h2>
                <div class="clearfix"></div>
                <p>Use filters to choose persons to send SMS</p>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        	@include('errors')     

            {!! Form::open(['method' => 'POST', 'route' => 'sms.store', 'class' => 'form-horizontal smsForm' ]) !!}

            <div class="row">
                <div class="col-sm-3">
                    <h4>Plot Size</h4>
                    <select class="select2_multiple form-control" multiple="multiple" name="plotSize[]">
                        <option value="">Choose option</option>
                        @foreach($plotSize as $size)
                            <option value="{{ $size->id }}"> {{ $size->plot_size }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-3">
                    <h4>Agency Pricipal Officers</h4>
                    <select class="select2_multiple form-control" multiple="multiple" name="agency[]">
                        <option value="">Choose option</option>
                        @foreach($agency as $agenc)
                            <option value="{{ $agenc->id }}"> {{ $agenc->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-3">
                    <h4>Pricipal Officers</h4>
                    <select class="select2_multiple form-control" multiple="multiple" name="principalOfficers[]">
                        <option value="">Choose option</option>
                        @foreach($principal_officer as $po)
                          
                            <option value="{{ $po->id }}"> {{ $po->agency->name .'-'. $po->name }}</option>
                          
                        @endforeach
                    </select>
                </div>

                 <div class="col-sm-3">
                    <h4>Members</h4>
                    <select class="select2_multiple form-control" multiple="multiple" name="members[]">
                        <option value="">Choose Registration Number</option>
                        @foreach($members as $member)
                            <option value="{{ $member->id }}"> {{ $member->registration_no }}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="row" style="margin-top: 20px;">
                 <div class="col-sm-3">
                    <h4>Sending Date & Time</h4>
                    <input type="text" name="sending_date" id="datetimepicker" required class="form-control" /> 
                            <br />
                </div>

                <div class="col-sm-3">
                    <h4>Target Date</h4>
                    <input type="text" name="target_date"  required class="datepicker form-control" /> 
                            <br />
                </div>

                <div class="col-sm-3">
                    <h4>General</h4>
                    <input type="checkbox" name="dueInstallments" id="dueInstallments" value="1" class="flat" /> Members With Due Installments {{-- ({{ $duePersons }}) --}}
                    {{-- <br />
                     <input type="checkbox" name="overDueInstallments" id="overDueInstallments" value="1" class="flat" /> Members With Over Due Installments ({{ count($overDuePersons) }}) --}}
                    <br />
                    <input type="checkbox" name="allEmployees" id="allEmployees" value="1" class="flat" /> Pricipal Officers + Sub Agents {{-- ({{ count($employees) }}) --}}
                    <br />
                    <input type="checkbox" name="allLPCs" id="allLPCs" value="1" class="flat" /> Members With Late Payment Charges
                </div>
                 <div class="col-sm-3">
                    <h4>General</h4>
                    <input type="checkbox" name="allMembers" id="allMembers" value="1" class="flat" /> All Members {{-- ({{ count($members) }})--}}
                            <br />
                    <input type="checkbox" name="allEmployees" id="allEmployees" value="1" class="flat" /> All Employees {{-- ({{ count($employees) }})--}}
                            <br />
                </div>

            </div>
            
            <div class="row">
              <div class="col-sm-3">
                    <h4>Custom Mobile Number</h4>
                    <input type="text" name="custom_mobile_number" class="form-control" placeholder="03001234567">
                </div>
                
                <div class="col-sm-3">
                    <h4>Plot Type</h4>
                    <input type="checkbox" name="plotTypeCommercial" id="plotType" value="1" class="flat" /> Members with Commerical Plots
                    <br />
                    <input type="checkbox" name="plotTypeHousing" id="plotType" value="1" class="flat" /> Members with Housing Plots
                    <br />
                </div>
                
            </div>
            
            <div class="row" style="margin-top: 50px;">

                <div class="col-sm-12">
                    <div class="form-group">
                        <h4>Message</h4>
                        {!! Form::textarea('message', '', ['class' => 'form-control', 'id' => 'message', 'required' => 'required']) !!}   
                    </div>
                </div>
                
                <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::submit("Schedule", ['class' => 'btn btn-warning pull-right' , 'id' => 'scheduleSMS']) !!}
                    {!! Form::hidden('sendnow', 0, ['id' => 'sendnow']) !!}
                    {!! Form::hidden('event', null, ['id' => 'event']) !!}
                    <button class="btn btn-warning pull-right" id="sendNowBtn">Send Now</button>
                    <button class="btn btn-success" id="quick_response">Quick Response <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button>

                    <button class="btn btn-success" id="custom_msg">Custom Message</button>
                    {{-- <button class="btn btn-default pull-right hide" id="saveQuickResponse">Save Quick Response</button> --}}
                </div>

                <div id="custom_button"></div>
                 
                </div>
                 
            </div>
            
            {!! Form::close() !!}
        </div>
    </div>
                
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ URL::to('public/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#datetimepicker').datetimepicker({
            format:'Y-m-d H:i'
        });
        $(document).ready(function(){


        $('#sendNowBtn').click(function(event){
            event.preventDefault();
            $('#sendnow').val(1);

            console.log($('#datetimepicker').val());
            console.log($('#dueInstallments').is(':checked'));
            console.log($('#message').val());
            console.log($('#target_date').val());

            if($('#datetimepicker').val() === "undefined" || $('#datetimepicker').val() == ""){
              swal("Oops!","Please choose sending date","error");
              return;
            }
            if($('#dueInstallments').is(':checked') && $('#target_date').val() == ""){
              swal("Oops!","Please choose target date","error");
              return;
            }

            if($('#message').val() == ""){
              swal("Oops!","Please write message in textarea or use quick response button","error");
              return;
            }

            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "Do you want to send SMS to selected persons Now?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Send it!",
              cancelButtonText: "Don't Sent pl!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Success!", "Please check the sms preview and click on send button.", "success");
                form.submit();
              } else {
                swal("Cancelled", "You have cancelled this operation", "error");
                // event.preventDefault();
              }
            });
        });


        $('#scheduleSMS').click(function(event){
            event.preventDefault();
            if($('#datetimepicker').val() == ""){
                swal("Error!", "Please select date for schedule", "error");
                return;
            }

            if($('#message').val() == ""){
                swal("Error!", "Please enter message or use quick response", "error");
                return;
            }
            $('#sendnow').val(0);
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "Do you want to schedule SMS to selected persons?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Schedule it!",
              cancelButtonText: "Don't Schedule pl!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Success!", "SMS is scheduled for the the selected persons.", "success");
                form.submit();
              } else {
                swal("Cancelled", "You have cancelled this operation", "error");
                // event.preventDefault();
              }
            });
        });

        $("#quick_response").click(function(e) {
            e.preventDefault();

            $.ajax({
                 type:"GET",
                 url: '{{ route('smstemplates') }}',
                 success : function(results) {
                    var $table = '';
                    $('#custom_button').html();
                    $.each(results, function(index, val) {
                         /* iterate through array or object */
                         $table += '<button class="btn quickMessage" id="' + val.id + '" data-eventName="'+val.name+'">'+val.name+'</button>';
                    });
                    //$table += '</tr></table>';
                    $('#custom_button').html($table);
                 },
                 error: function(xhr, status, responseText){
                    console.log(status);
                 }
            });
        });


        $(document).on('click','.quickMessage',function(e) {
            e.preventDefault();
            $.ajax({
                 type:"GET",
                 url: '{{ route('getmessage') }}',
                 data: {id: $(this).attr('id')},
                 success : function(data) {
                  // console.log($(this).attr('data-eventName'));
                    $('#message').val(data.body);                    
                    // $('#event').val($(this).data('eventName'));                    
                 },
                 error: function(xhr, status, responseText){
                    console.log(status);
                 }
                }); 
             });


        $('#custom_msg').click(function(e){
          e.preventDefault();
          $('#event').val('Custom Message');
          $('#custom_button').html('');
          $('#message').focus();
          $('#message').val('');
          $('#saveQuickResponse').removeClass('hide');
        });


        // $('#saveQuickResponse').click(function(e){
        //   e.preventDefault();
        //   if($('#message').val() == "")
        //   {
        //     swal({
        //       title: "Oops!",
        //       text: "Please add message before saving quick response",
        //       type: "warning",
        //       showCancelButton: false,
        //       confirmButtonColor: "#DD6B55",
        //     });
        //   } 
        // });        

         })
    </script>
@endsection