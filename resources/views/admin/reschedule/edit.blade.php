@extends('admin.layouts.template')
@section('title','Edit Person Type')

@section('bookings-active','active')
@section('reschedule-active','active')
@section('content')
        
        <h2>Edit Unit</h2>    
        <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::model($persontype, ['method' => 'PATCH', 'route' => ['persontype.update', $persontype->id] , 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('type', 'unit Name') !!}
                {!! Form::text('type', null , ['class' => 'form-control', 'id' => 'type', 'required' => 'required']) !!} 
            </div>
        
            <div class="form-group">
                {!! Form::hidden('_method', 'PATCH') !!}
                {!! Form::submit("Update", ['class' => 'btn btn-default']) !!}
            </div>
        {!! Form::close() !!}
        </div>
@endsection