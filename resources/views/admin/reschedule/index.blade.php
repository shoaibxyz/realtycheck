@extends('admin.layouts.template')
@section('title','Payment Plan Rescheduling')


@section('bookings-active','active')
@section('reschedule-active','active')
@section('content')
@include('admin/common/breadcrumb',['page'=>'Reschedule List'])        
        <h2>Payment Plan Reschedule List <small class="pull-right" style="{{ $rights->show_create }}"><a href="{{ route('reschedule.create')}}" class="btn btn-primary">Add New</a></small></h2>
        @if(!empty($message))
            <p>{{$message}}</p>
        @endif
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr>
                    <th>Registration No</th>
                    <th>Previous Plan</th>
                    <th>New Plan</th>
                    <th>Reschedule Date</th>
                    <th>Remarks</th>
                    <th>Created At</th>
                    <th>Generate Plan</th>
                    <th></th>
                    
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($reschedules as $reschedule)
                    <tr>
                        <td>{{ $reschedule->registration_no }}</td>
                        <td>{{ $reschedule->CurrentPaymentPlan->payment_code }}</td>
                        <td>{{ $reschedule->NewPaymentPlan->payment_code }}</td>
                        <td>{{ $reschedule->reschedule_date->format('d-m-Y') }}</td>
                        <td>{{ $reschedule->remarks }}</td>
                        <td>{{ $reschedule->created_at->format('d-m-Y H:i:s') }}</td>
                       
                        <td><a href="installment/plan/{{ $reschedule->id }}" class="btn-success btn btn-xs">Generate</a> </td>
                         <td><a href="{{ route('reschedule.edit', $reschedule->id )}}" class="btn-info btn btn-xs">Edit</a> </td>
                        <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['reschedule.destroy', $reschedule->id]]) !!}
                            {!! Form::submit("Delete", ['class' => 'btn-danger btn btn-xs DeleteBtn']) !!}
                        {!! Form::close() !!} </td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>    
@endsection


@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
        });
    </script>
@endsection