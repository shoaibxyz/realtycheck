 @include('errors')
     <form method="POST" id="paymentForm2" action="{{ route('reschedule.store') }}" >
        <div id="data">
            <div class="row">
                <div id="member" class="col-sm-12" style="clear: both">
			<div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('person', 'Member Name')!!}
                    {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
                    </div></div>
<div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('current_plan', 'Current Payment Plan')!!}
                    {!! Form::text('current_plan', null, ['class' => 'form-control', 'id' => 'current_plan', 'readonly' => 'readonly']) !!}
                    </div></div>
                    <div class="col-md-6">

                    <div class="form-group">
                    {!! Form::label('reschedule_date', 'Reschedule Date')!!}
                    {!! Form::text('reschedule_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'reschedule_date']) !!}
                    </div></div>
                    <div class="col-md-6">

                    <div class="form-group">
                        {!! Form::label('payment_code', 'New Payment Plan Code') !!}
                        {!! Form::text('payment_code', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_code']) !!}
                    </div></div>
                    <div class="col-md-6">

                    <div class="form-group">
                        {!! Form::label('payment_desc', 'New Payment Plan Description') !!}
                        {!! Form::text('payment_desc', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_desc']) !!}
                    </div></div>
                    <div class="col-md-6">

                    <div class="form-group">
                        {!! Form::label('remarks', 'Remarks') !!}
                        {!! Form::text('remarks', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'remarks']) !!}
                    </div>  </div>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <h3>Old Amount</h3>    
                    
                    <div class="form-group">
                    {!! Form::label('acutal_price', 'Total Price')!!}
                    {!! Form::text('acutal_price', null, ['class' => 'form-control', 'id' => 'acutal_price']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('rebat_amount', 'Rebat Amount')!!}
                    {!! Form::text('rebat_amount', 0, ['class' => 'form-control', 'id' => 'rebat_amount']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('total_installment_amount', 'Total Inst. Amount')!!}
                    {!! Form::text('total_installment_amount', null, ['class' => 'form-control', 'id' => 'total_installment_amount']) !!}
                    </div>
                </div>

                 <div class="col-sm-4">
                    <h3>Change Amount</h3>    

                    <div class="form-group">
                    {!! Form::label('change_total_price', 'Total Price')!!}
                    {!! Form::text('change_total_price', 0, ['class' => 'form-control', 'id' => 'change_total_price']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('rebat_amount', 'Rebat Amount')!!}
                    {!! Form::text('rebat_amount', 0, ['class' => 'form-control', 'id' => 'rebat_amount']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('total_installment_paid', 'Total Inst. Received + Rebat')!!}
                    {!! Form::text('total_installment_paid', null, ['class' => 'form-control', 'id' => 'total_installment_paid']) !!}
                    </div>

                </div>

                <div class="col-sm-4">

                <h3>New Amount</h3>
                    
                    <div class="form-group">
                    {!! Form::label('total_price', 'Total Price')!!}
                    {!! Form::text('total_price', null, ['class' => 'form-control', 'id' => 'total_price']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('rebat_amount', 'Rebat Amount')!!}
                    {!! Form::text('rebat_amount', 0, ['class' => 'form-control', 'id' => 'rebat_amount']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('total_amount_plan', 'Total Amt. Plan')!!}
                    {!! Form::text('total_amount_plan', null, ['class' => 'form-control', 'id' => 'total_amount_plan']) !!}
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('total_installments', 'Total Installments')!!}
                    {!! Form::text('total_installments', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'total_installments']) !!}
                    </div>
                </div>
                <?php 
                    $paymentFrequency[''] = "";
                    $paymentFrequency[1] = "After 1 Month";
                    $paymentFrequency[2] = "After 2 Month";
                    $paymentFrequency[3] = "After 3 Month";
                    $paymentFrequency[4] = "After 4 Month";
                ?>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('payment_frequency', 'Payment Frequency') !!}
                        {!! Form::select('payment_frequency', $paymentFrequency, old('payment_frequency'), ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_frequency']) !!}
                    </div>
                </div>

                <div class="col-sm-4">
                    {{ csrf_field() }}
                    {!! Form::hidden('registration', null, ['id' => 'registration_field']) !!}
                    {!! Form::label('', '') !!}
                    {!! Form::button('Generate Plan', ['id'=>'generate','class'=>'btn btn-block btn-default']) !!}      
                </div>
            </div>
        </div>