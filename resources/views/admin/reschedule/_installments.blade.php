<div class="row">
    <div class="col-sm-12"> 
     <?php if( Session::has('error')) : ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success!</strong> {{ Session('error') }}
        </div>
    <?php endif; ?>
    @include('errors')

        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#Installment">Installment</a></li>
          <li><a data-toggle="tab" href="#other_payments">Other Payments</a></li>
              <li><a data-toggle="tab" href="#prev_installments">Prev Installment</a></li>
              <li><a data-toggle="tab" href="#prev_other_payments">Prev Other Payments</a></li>
        </ul>

        <div class="tab-content">
          <div id="Installment" class="tab-pane fade in active">
            <div class="table-responsive1">
            <table class="table table-bordered table-hover table-striped  table-condenced">
                <thead>
                    <tr>
                        <th>Inst. No</th>
                        <th>Inst. Date</th>
                        <th>Inst. Amount</th>
                        <th>Rebat Amount</th>
                        <th>Received Amount</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody class="installmentAppend"></tbody>
            </table>
            </div>
         </div>
          <div id="other_payments" class="tab-pane fade table-responsive">
           <div class="table-responsive1">
           <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Payment Type</th>
                        <th>Inst. No</th>
                        <th>Inst. Date Creteria</th>
                        <th>Inst. Date</th>
                        <th>Days/Month</th>
                        <th>Amount Creteria</th>
                        <th>Amount %</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody class="otherPaymentsTable">

                    <?php $months = 2; $i=1; $j= 1; ?>
                        <tr class="otherPaymentsTableRow" data-row="{{ $i }}"> 
                            <td class="sr_no">1</td>
                            <td>
                            <?php 
                                $payment_typeArr[''] = "";
                                foreach ($paymentTypes as $type) {
                                     $payment_typeArr[$type->id] = $type->payment_type;
                                 } 
                            ?>
                            {!! Form::select('other_payment_type_id[]', $payment_typeArr, 4) !!} </td>
                            <td>
                                {!! Form::text('other_installment_no[]', 1) !!}
                            </td>
                            <td>
                                 <?php 
                                    $date_creteria['Months After Booking'] = 'Months After Booking';
                                    $date_creteria['Fix Date'] = 'Fix Date';
                                    $date_creteria['Days After Booking'] = 'Days After Booking';
                                ?>  
                                {!! Form::select('other_date_creteria[]', $date_creteria, null) !!}
                            </td>
                            <td>{!! Form::text('other_installment_date[]', null, ['class' => 'datepicker date'] ) !!}</td>
                            <td>{!! Form::text('other_days_or_months[]', 1) !!}</td>
                            <td>
                                <?php 
                                    $amount_creteria['Fix Amount'] = 'Fix Amount';
                                    $amount_creteria['% of Net Price'] = '% of Net Price';
                                    $amount_creteria['Equal Instal. Amount'] = 'Equal Instal. Amount';
                                    $amount_creteria['% of Booking'] = '% of Booking';
                                    $amount_creteria['Remaining equal installments'] = 'Remaining equal installments';
                                ?>  
                                {!! Form::select('other_amount_creteria[]', $amount_creteria, null) !!}
                            </td>
                            <td>{!! Form::text('other_amount_percentage[]', '' ,['class'=>'other_amount_percentage']) !!}</td>
                             <?php $booking_percentage = round(1 * 1 / 100, 2); ?>
                            <td>
                                {!! Form::text('other_inst[]', $booking_percentage, ['class' => 'inst_amount installment_amount_'.$i]) !!}
                            </td>
                        </tr>

                        {!! Form::hidden('total_amount', null, ['id' => 'total_amount']) !!}
                </tbody>
            </table>
            </div>
            <a href="#" class="btn btn-danger pull-right" id="add-row">Add Row</a>
          </div>
          <div id="prev_installments" class="tab-pane fade in">
            <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped  table-condenced">
                <thead>
                    <tr>
                        <th>Inst. No</th>
                        <th>Date Creteria</th>
                        <th>Inst Date</th>
                        <th>Days / Month</th>
                        <th>Amount Creteria</th>
                        <th>Amount %</th>
                        <th>Installment Amount</th>                    
                    </tr>
                </thead>
                <tbody class="prevInstallmentAppend"></tbody>
            </table>
            </div>
         </div>
         <div id="prev_other_payments" class="tab-pane fade table-responsive">
            <div class="table-responsive">
           <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Payment Type</th>
                        <th>Inst. No</th>
                        <th>Inst. Date Creteria</th>
                        <th>Inst. Date</th>
                        <th>Days/Month</th>
                        <th>Amount Creteria</th>
                        <th>Amount %</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody class="prevOtherPaymentsTable">

                    <?php $months = 2; $i=1; $j= 1; ?>
                        <tr class="prev_otherPaymentsTableRow" data-row="{{ $i }}"> 
                            <td class="sr_no">1</td>
                            <td>
                            <?php 
                                $payment_typeArr[''] = "";
                                foreach ($paymentTypes as $type) {
                                     $payment_typeArr[$type->id] = $type->payment_type;
                                 } 
                            ?>
                            {!! Form::select('prev_other_payment_type_id[]', $payment_typeArr, 4) !!} </td>
                            <td>
                                {!! Form::text('prev_other_installment_no[]', 1) !!}
                            </td>
                            <td>
                                 <?php 
                                    $date_creteria['Months After Booking'] = 'Months After Booking';
                                    $date_creteria['Fix Date'] = 'Fix Date';
                                    $date_creteria['Days After Booking'] = 'Days After Booking';
                                ?>  
                                {!! Form::select('prev_other_date_creteria[]', $date_creteria, null) !!}
                            </td>
                            <td>{!! Form::text('prev_other_installment_date[]', null, ['class' => 'datepicker date'] ) !!}</td>
                            <td>{!! Form::text('prev_other_days_or_months[]', 1) !!}</td>
                            <td>
                                <?php 
                                    $amount_creteria['Fix Amount'] = 'Fix Amount';
                                    $amount_creteria['% of Net Price'] = '% of Net Price';
                                    $amount_creteria['Equal Instal. Amount'] = 'Equal Instal. Amount';
                                    $amount_creteria['% of Booking'] = '% of Booking';
                                    $amount_creteria['Remaining equal installments'] = 'Remaining equal installments';
                                ?>  
                                {!! Form::select('prev_other_amount_creteria[]', $amount_creteria, null) !!}
                            </td>
                            <td>{!! Form::text('prev_other_amount_percentage[]', '' ,['class'=>'prev_other_amount_percentage']) !!}</td>
                             <?php $booking_percentage = round(1 * 1 / 100, 2); ?>
                            <td>
                                {!! Form::text('other_inst[]', $booking_percentage, ['class' => 'inst_amount installment_amount_'.$i]) !!}
                            </td>
                        </tr>

                        {!! Form::hidden('total_amount', null, ['id' => 'total_amount']) !!}
                </tbody>
            </table>
            </div>
            <a href="#" class="btn btn-danger pull-right" id="add-row">Add Row</a>
          </div>

        </div>
        <div class="form-group">
            {!! Form::submit("Save", ['class' => 'btn btn-primary pull-right']) !!}
        </div>
    {!! Form::close() !!}
    </div>
</div>