@extends('admin.layouts.template')
@section('title','Reschedule Payment Plan')

@section('bookings-active','active')
@section('reschedule-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
         <?php if( Session::has('message')) :
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>

         <div class="x_panel">
        <div class="x_title">
            <h2>Reschedule Payment Plan</h2>
            
            <div class="clearfix"></div>
          </div>    
<div class="row">
          <div class="col-sm-offset-7 col-sm-5">
         {!! Form::open(['method' => 'POST', 'id' => 'paymentForm', 'class' => 'form-horizontal']) !!}
            
            <div class="form-group col-sm-10">
            {!! Form::label('registration_no', 'Search Registration No')!!}
            {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!}
            </div>
            <div class="col-sm-2">
            <button type="submit" id="payment" class="btn btn-default " style="margin-top: 25px;">Search</button>
            </div>
            {!! Form::close() !!}

</div>
</div>
        <div class="clearfix"></div>

           @include('admin.reschedule._form2')

           <div class="clearfix"></div>

           @include('admin.reschedule._installments')
           </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker, datepicker2').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

    $('#payment_schedule').change(function(event) {
        var planId = $(this).val();
        $.ajax({
            url: '{{ URL::to('/PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
    });

    var inputs = $('#data input, #data select').attr('readonly','readonly');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var registration_no = $('#registration_no').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'registration_no': registration_no},
            success: function(result){
                var $inputs = $('#data input');
                var $persons = $('#persons input');

                $('.prevInstallmentAppend tr').remove();
                $('.installmentAppend tr').remove();

                $('#data input, #data select').removeAttr('readonly','readonly');
                $('#total_installment_amount, #total_amount_plan, #person, #current_plan').attr('readonly','readonly');
                
                $('#data input#acutal_price, #total_installment_paid, #total_price').attr('readonly','readonly');

                $('#data input#person').val(result.member.name);
                $('#data input#current_plan').val(result.paymentPlan.payment_code);
                $('#registration_field').val($('#registration_no').val());

                $('#acutal_price').val(result.paymentPlan.total_price);
                $('#total_price').val(result.paymentPlan.total_price);
                $('#total_installment_amount').val(result.installment_amount);
                $('#total_installment_paid').val(result.total_installment_paid);
                $('#total_amount_plan').val(result.installment_amount_plan);
                
                $.each(result.member, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

                var inc = 1;
                var ids=2; //id numbers for add. datepickers
                // console.log(result.paymentPlan.installment);
                // $.each(result.paymentPlan.installment, function(index, val) {
                //     if( val.is_paid == 0)
                //     {
                //         var html = "<tr>"; 
                //         html += "<td class='sr_no'>"+val.id+"</td>";
                //         html += '<td><input class="datepicker" name="installment_date[]" type="text" value="'+val.installment_date+'"></td>';
                //         html += '<td><input class="" name="installment_amount[]" type="text" value="'+val.installment_amount+'"></td>';
                //         html += '<td><input class="" name="rebat_amount[]" type="text" value="'+val.rebat_amount+'"></td>';
                //         html += '<td><input class="" name="amount_received[]" type="text" value="'+val.amount_received+'"></td>';
                //         html += '<td><input class="" name="remarks[]" type="text" value="'+val.remarks+'"></td>';
                //         $('.installmentAppend').append(html);
                //     }
                // });
                

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });

    });

        var ids = 2;
        $('#generate').click(function(event) {
            event.preventDefault();
            var inst = parseInt($('#total_installments').val());
            var payment_frequency = parseInt($('#payment_frequency').val());
            var i = 0;

            var url = '{{ URL::to('/memberInfo') }}';
            var registration_no = $('#registration_no').val();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {'registration_no': registration_no},
                success: function(result){

                    $('.prevInstallmentAppend tr').remove();
                    $('.installmentAppend tr').remove();

                    var ids=2; //id numbers for add. datepickers
                    $.each(result.paymentPlan.installment, function(index, val) {
                        var html = "<tr>"; 
                        html += "<td class='sr_no'>"+val.installment_no+"</td>";
                        html += '<td><input class="text" name="prev_date_creteria[]" type="text" value="'+val.date_creteria+'"></td>';
                        html += '<td><input class="text" name="prev_installment_date[]" type="text" value="'+val.installment_date+'"></td>';
                        html += '<td><input class="text" name="prev_days_or_months[]" type="text" value="'+val.days_or_months+'"></td>';
                        html += '<td><input class="" name="prev_amount_creteria[]" type="text" value="'+val.amount_creteria+'"></td>';
                        html += '<td><input class="" name="prev_amount_percentage[]" type="text" value="'+val.amount_percentage+'"></td>';
                         html += '<td><input class="" name="prev_installment_amount[]" type="text" value="'+val.installment_amount+'"></td>';
                        $('.prevInstallmentAppend').append(html);
                    });
                    
                    var total_amount_plan = parseFloat(result.installment_amount_plan);
                    var installment_amount = parseFloat(total_amount_plan/inst).toFixed(2);

                    while(i < inst)
                    {
                            var installment = result.paymentPlan.installment[i];
                            var splitDate = installment.installment_date.split("-");

                            var dateObj = new Date();
                            // dateObj.setMonth(dateObj.getMonth() + i);
                            // inst_date = dateObj.getDate()  + '/' + dateObj.getMonth() + '/' + dateObj.getFullYear();

                            inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() + payment_frequency) + '-' + dateObj.getFullYear();
                            dateObj.addMonths(payment_frequency);

                            var html = "<tr>"; 
                            html += "<td class='sr_no'><input type='text' name='installment_no[]' readonly value='"+installment.installment_no+"'></td>";
                            html += '<td><input class="datepicker2" name="installment_date[]" type="text" value="'+inst_date+'"></td>';
                            html += '<td><input class="" name="installment_amount[]" type="text" value="'+installment_amount+'"></td>';
                            html += '<td><input class="" name="rebat_amount[]" type="text" value="0"></td>';
                            html += '<td><input class="" name="amount_received[]" type="text" value="0"></td>';
                            html += '<td><input class="" name="installment_remarks[]" type="text" value=""></td>';
                            $('.installmentAppend').append(html);
                        // if(i >= inst) return;
                        i++;
                        // if(payment_frequency == 1)
                        //     payment_frequency++;
                        // if(payment_frequency == 2)
                        //     payment_frequency+=2;
                        // if(payment_frequency == 3)
                        //     payment_frequency+=3;
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        });

    Date.isLeapYear = function (year) { 
        return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
    };

    Date.getDaysInMonth = function (year, month) {
        return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    };

    Date.prototype.isLeapYear = function () { 
        return Date.isLeapYear(this.getFullYear()); 
    };

    Date.prototype.getDaysInMonth = function () { 
        return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
    };

    Date.prototype.addMonths = function (value) {
        var n = this.getDate();
        this.setDate(1);
        this.setMonth(this.getMonth() + value);
        this.setDate(Math.min(n, this.getDaysInMonth()));
        return this;
    };

        
    </script>
@endsection

