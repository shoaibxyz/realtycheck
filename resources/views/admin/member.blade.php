@extends('admin.layouts.template')

@section('title','Profile')

@section('profile-active','active')

@section('content')

@include('admin/common/breadcrumb',['page'=>'Profile'])


    <div class="" role="main">
          <div class="">
           
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Information </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{ env('STORAGE_PATH').$profile->picture }}" width="100" height="100"  alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      
                      <hr>
                     

                      <ul class="list-unstyled user_data">
                        <li style="font-size:16px;"><strong>Name:</strong>   {{ $profile->name}}</br>
                        </li>
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{ $profile->current_address}}</br>
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> Employee
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-external-link user-profile-icon"></i>
                          <a href="" target="_blank">{{ $profile->email}}</br></a>
                        </li>
                      </ul>
                       
                       <a class="btn btn-success" href="{{ URL('memberedit' , $profile->id) }}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                      
                      <br />
                      
                      

                    
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      
						<h4><strong>Person ID: </strong>{{ $profile->id}}</h4>
						
						<h4> <strong>Email:</strong> {{ $profile->email}}</h4>
						<h4> <strong>Current Address:</strong> {{ $profile->current_address}}</h4>
						<h4> <strong>Permanent Address: </strong>{{ $profile->permanent_address}}</h4>
						<h4><strong>Guardian Name:</strong> {{ $profile->gurdian_name}}</h4>
						<h4><strong>Nationality:</strong> {{ $profile->nationality}}</h4>
						<h4><strong>CNIC:</strong> {{ $profile->cnic}}</h4>
						<h4><strong>Phone Office:</strong> {{ $profile->phone_office}}</h4>
						<h4><strong>Phone Resident:</strong> {{ $profile->phone_res}}</h4>
						<h4><strong>Mobile:</strong> {{ $profile->phone_mobile}}</h4>
						
						</div>
            
					</div>
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
@endsection

