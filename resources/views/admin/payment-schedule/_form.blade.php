<?php $plotSizeArr = []; ?>


<?php $plotSizeArr[''] = 'Choose one'  ?>
@foreach($plotSize as $size)
    <?php $plotSizeArr[$size->id] = $size->plot_size;  ?>
@endforeach

<div class="row">

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Plot Size <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::select('plot_size_id', $plotSizeArr, old('plot_size_id') , ['required' =>'required', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Payment Plan Code <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('payment_code', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_code']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Description <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('payment_desc', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_desc']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Total Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('total_price', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'total_price']) !!}
        </div>
    </div>


    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Booking Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('booking_price', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'booking_price']) !!}
        </div>
    </div>
    
    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">PAC Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('pac_percentage', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'pac_percentage']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Balloting Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('balloting_percentage', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'balloting_percentage']) !!}
        </div>
    </div>


    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">6 Month Inst. Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('semi_anual_percentage', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'semi_anual_percentage']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Monthly Inst. Price <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('monthly_percentage', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'monthly_percentage']) !!}
        </div>
    </div>

    <?php
        $plotNatureArr[""] = "";
        $plotNatureArr["Residential"] = 'Residential';
        $plotNatureArr["Commercial"] = 'Commercial';
        $plotNatureArr["Housing"] = 'Housing';
    ?>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Plot Nature <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::select('plot_nature', $plotNatureArr, old('plot_nature'), ['required' =>'required', 'class' => 'form-control', 'id' => 'plot_nature']) !!}
        </div>
    </div>

    <?php
            $paymentFrequency[''] = "";
            $paymentFrequency[1] = "After 1 Month";
            $paymentFrequency[2] = "After 2 Month";
            $paymentFrequency[3] = "After 3 Month";
            $paymentFrequency[4] = "After 4 Month";
    ?>
    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
        <label for="inputEmail3" class="col-sm-4 control-label">Payment Frequency <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::select('payment_frequency', $paymentFrequency, old('payment_frequency'), ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_frequency']) !!}
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
        <label for="inputEmail3" class="col-sm-4 control-label">Installments <span class="required">*</span></label>
        <div class="col-sm-8">
            {!! Form::text('installments', null, ['required' =>'required', 'class' => 'form-control has-feedback-right', 'id' => 'installments']) !!}
        </div>
    </div>

</div>
