@extends('admin.layouts.template')

@section('title','Add Payment Plan')

@section('bookings-active','active')
@section('plot-active','active')
@section('payment_schedule-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Payment Plan</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Payment Plan</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Payment Plan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['method' => 'POST', 'route' => 'payment-schedule.store', 'class' => 'form-horizontal']) !!}

                    @include("admin.payment-schedule._form")

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('payment-schedule.index') }}" class="btn btn-grey">Cancel</a>
                    </div>

                {!! Form::close() !!}
          </div>
        </div>
    </div>
@endsection
