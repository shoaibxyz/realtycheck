@extends('admin.layouts.template')

@section('title','Edit Payment Plan')

@section('bookings-active','active')
@section('plot-active','active')
@section('payment_schedule-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Payment Plan</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Payment Plan</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Payment Plan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($payment_schedule, ['method' => 'PATCH', 'route' => ['payment-schedule.update', $payment_schedule->id], 'class' => 'form-horizontal']) !!}

                    @include("admin.payment-schedule._form")

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PUT') !!}
                        {!! Form::hidden('payment_schedule_id', $payment_schedule->id) !!}
                        {!! Form::submit("Update Plan", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('payment-schedule.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
