@extends('admin.layouts.template')

@section('title','Payment Plan')

@section('bookings-active','active')
@section('plot-active','active')
@section('payment_schedule-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Payment Plans List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Payment Plans List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Payment Plans List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('payment-schedule.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Plan</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Price</th>
                            <th>Booking %age</th>
                            <th>Booking Price</th>
                            <th>Plot Nature</th>
                            <th>Frequency</th>
                            <th>Size</th>
                            <th>Installments</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($payment_schedule as $payment)
                            <tr>
                                <td>{{ $payment->payment_code }}</td>
                                <td>{{ $payment->total_price }}</td>
                                <td>{{ $payment->booking_percentage }}</td>
                                <td>{{ $payment->booking_price }}</td>
                                <td>{{ $payment->plot_nature }}</td>
                                <td>{{ 'After '.$payment->payment_frequency.' Month' }}</td>
                                <td> {{ $payment->plot_size  }} </td>
                                <td> {{ $payment->installments  }} </td>
                                <td width="15%">
                                    
                                    <a href="installment/plan/{{ $payment->id }}" class="btn btn-warning" style="{{ $rights->show_edit }}" title="upload"><i class="fa fa-cloud-upload"></i></a>
                                    <a href="{{ route('payment-schedule.edit', $payment->id )}}" class="btn btn-info" style="{{ $rights->show_edit }}" title="edit"><i class="fa fa-edit"></i></a>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['payment-schedule.destroy', $payment->id],'style' => $rights->show_delete]) !!}
                                    <button class="btn btn-danger" type="submit" value="" style="{{ $rights->show_delete }}" title="delete"><i class="fa fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection


@section('javascript')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
        });
    </script>
@endsection
