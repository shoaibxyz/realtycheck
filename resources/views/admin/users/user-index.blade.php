@extends('admin.layouts.template')
@section('title','User List')
@section('user-list-active','active')
@section('user-list-head-active','active')
@section('user-list-active','active')
@section('content')
            <h2>Users List
                
            </h2>

        <div class="x_panel">
        <div class="x_title">
            
            
            <div class="clearfix"></div>
          </div>    
        <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable" >
            <thead>
                <tr>
                   
                    <th>S#</th>
                    <th>User Name</th>
                    <th>Role Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
              
                @foreach($user as $key => $person)
                    
                    <tr>
                        
                        <td>
                        {{ $i++ }}
                        </td>
                        <td>{{ $person->name }}</td>
                       <td> {{ $person->role }}</td>
                        
                        <td>
                        
                        
                       
                            <a href="{{ route ('editgetuser',$person->id) }}" class="btn btn-info">Change Password</a>
                     
                      
                        
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        </div>    
@endsection
