 
<div class="x_panel">
            
    
    
             <div class="loader-image" style="display: none; top: 10%; left: 40%; position:absolute; z-index: 99999;">
            <img src="{{ asset('public/images/ajax-loader.gif') }}" width="200px" height="200px" style="background-repeat: no-repeat; background-position: center center; ">
          </div>
            @include('errors')
          
        <div class="clearfix"></div>
       
       {!! Form::model($user, ['id' => 'inquiry', 'class' => 'form-horizontal']) !!}
       
       <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Personal Info</h2>
       
         <div class="row">
       <div class="col-sm-2">
           
           <div class="form-group">
                {!! Form::label('picture', 'Picture', ['class' => 'col-sm-12'])!!}
               <div class="col-md-6">
                <img id="img-preview" src="{{ env('STORAGE_PATH'). $user->picture }}" name="picture"  style="margin: 10px auto;width:3.5cm;height:4.5cm;" alt="Picture"/>
                </div>
            </div>
            
            <div class="form-group" style="margin-bottom: 30px;">
                <div class="followup" style="{{ $rights->show_print }}">
                    <a href="{{ route('followup.show', $member->registration_no) }}" class="btn btn-warning btn-xs">Followup</a>
                </div>
            </div>
            
        </div>
        
        <div class="col-sm-5">
           
           
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Name</label>
                 <div class="col-sm-7">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">User Name</label>
                 <div class="col-sm-7">
                {!! Form::text('username', null, ['class' => 'form-control', 'id' => 'username']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Guardian Name ({{ $user-> guardian_type }})</label>
                 <div class="col-sm-7">
                {!! Form::text('gurdian_name', null, ['class' => 'form-control', 'id' => 'gurdian_name']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">CNIC</label>
                 <div class="col-sm-7">
                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Email</label>
                 <div class="col-sm-7">
                {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                </div>
                </div>
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Phone Office</label>
                 <div class="col-sm-7">
                {!! Form::text('phone_office', null, ['class' => 'form-control', 'id' => 'phone_office']) !!}
                </div>
                </div>
           
        </div>
        
        <div class="col-sm-5">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Nationality</label>
                 <div class="col-sm-7">
                {!! Form::text('nationality', null, ['class' => 'form-control', 'id' => 'nationality']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Current Address</label>
                 <div class="col-sm-7">
                {!! Form::text('current_address', null, ['class' => 'form-control', 'id' => 'current_address']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Country</label>
                 <div class="col-sm-7">
                {!! Form::text('country_id', null, ['class' => 'form-control', 'id' => 'country']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Country</label>
                 <div class="col-sm-7">
                {!! Form::text('country_id', null, ['class' => 'form-control', 'id' => 'country']) !!}
                </div>
                </div>
                
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">City</label>
                 <div class="col-sm-7">
                {!! Form::text('city_id', null, ['class' => 'form-control', 'id' => 'city']) !!}
                </div>
                </div>
                <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Phone Res</label>
                 <div class="col-sm-7">
                {!! Form::text('phone_res', null, ['class' => 'form-control', 'id' => 'phone_res']) !!}
                </div>
                </div>
                <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Mobile</label>
                 <div class="col-sm-7">
                {!! Form::text('phone_mobile', null, ['class' => 'form-control', 'id' => 'phone_mobile']) !!}
                </div>
                </div>
          
            
        </div>
        
        </div>
        
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Member Info</h2>
        
        
        <div class="row">
            
       <div class="col-sm-1"></div>
        <div class="col-sm-5">
            
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Nominee Name</label>
                 <div class="col-sm-7">
                {!! Form::text('nominee_name', $member->nominee_name , array( 'class'=>'form-control c_field' , 'id' => 'nominee_name') )!!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Nominee Guardian Name</label>
                 <div class="col-sm-7">
                {!! Form::text('nominee_guardian', $member->nominee_guardian , array('class'=>'form-control' , 'id' => 'nominee_guardian') )!!}
                </div>
                </div>
            
        </div>
        
        <div class="col-sm-5">
            
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Nominee CNIC</label>
                 <div class="col-sm-7">
                {!! Form::text('nominee_cnic', $member->nominee_cnic , array('class'=>'form-control' , 'id' => 'nominee_cnic') )!!}
                </div>
                </div>
            
        </div>
        <div class="col-sm-1"></div>
       
      </div>
      
      <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Payment Detail</h2>
      
      <div class="row">
          <div class="col-sm-6">
              
              <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Registration No</label>
                 <div class="col-sm-7">
                {!! Form::text('registration_no', $member->registration_no, ['class' => 'form-control', 'id' => 'registration_no']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Reference No</label>
                 <div class="col-sm-7">
                {!! Form::text('reference_no', $member->reference_no, ['class' => 'form-control', 'id' => 'reference_no']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Preferences</label>
                 <div class="col-sm-7">
                @foreach($booking->featureset as $featur)
                        <div class="checkbox" style="margin-left:20px;">
                            <label>
                                {!! Form::checkbox("feature_set_id[]", $featur->id, ['checked' => 'checked']) !!}
                                {{$featur->feature}}
                            </label>
                        </div>
                    @endforeach
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Booking Date</label>
                 <div class="col-sm-7">
               {!! Form::text('booking_date', $booking->created_at->format('d-m-Y'), ['class' => 'form-control', 'id' => 'booking_date']) !!}
                </div>
                </div>
              
          </div>
          
          <div class="col-sm-6">
              <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Payment Plan</label>
                 <div class="col-sm-7">
               {!! Form::text('payment_plan', $booking->paymentPlan->payment_code, ['class' => 'form-control', 'id' => 'payment_plan']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Payment Plan</label>
                 <div class="col-sm-7">
               {!! Form::text('payment_plan', $booking->paymentPlan->payment_code, ['class' => 'form-control', 'id' => 'payment_plan']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Total Price</label>
                 <div class="col-sm-7">
               {!! Form::text('total_price', $booking->paymentPlan->total_price, ['class' => 'form-control', 'id' => 'total_price']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Booking Price</label>
                 <div class="col-sm-7">
               {!! Form::text('booking_price', $booking->paymentPlan->booking_price, ['class' => 'form-control', 'id' => 'booking_price']) !!}
                </div>
                </div>
                
            <div class="form-group">
                 <label for="inputEmail3" class="col-sm-5 control-label">Estate Name</label>
                 <div class="col-sm-7">
               {!! Form::text('nominee_cnic', $booking->agency->name , array('class'=>'form-control' , 'id' => 'nominee_cnic') )!!}
                </div>
                </div>
              
        </div>
      </div>
      
       <div class="row" style="margin-top:50px;">
            <div class="col-sm-12" role="tabpanel"> 
             <ul class="nav nav-tabs" role="tablist">
          <li class="active"><a aria-controls="Installment" role="tab" data-toggle="tab" href="#Installment">OS Amount</a></li>
          <li ><a aria-controls="history" role="tab" data-toggle="tab" href="#history">History</a></li>
          <li ><a aria-controls="alert" role="tab" data-toggle="tab" href="#alert">Alert</a></li>
          <li ><a aria-controls="sms" role="tab" data-toggle="tab" href="#sms">SMS History</a></li>
        </ul>

        <div class="tab-content">
        <div id="Installment" class="tab-pane fade in active"  style="overflow:scroll; max-height: 400px; min-height: 400px; margin-top: 20px;">
         <div class="col-sm-4">
          <h5><b>OS Installments</b></h5>
            <div class="table-responsive1">
               <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>OS Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($os_installments = 0)
                        @php($os_sum = 0)
                        @foreach($os_inst as $key => $os)
                        <?php 
                            // $inst_receipt = App\InstallmentReceipts::where('receipt_no', $os->receipt_no)->where('os_amount', '!=', 0)->orderBy('id','DESC')->limit(1)->first();
                            // if($key == 1)
                            // $amount = 0;
                            // $amount += ($inst_receipt->due_amount - $inst_receipt->received_amount);
                            // }
                            // $due_amount[$inst->registration_no][] = $inst->amount;
                        ?>
                        <tr>
                            <td>{{ $os->installment_no }}</td>
                            <td>{{ date('d-m-Y', strtotime($os->installment_date)) }}</td>
                            <td style="color:red;">{{ ($os->os_amount > 0) ? $os->os_amount : $os->due_amount - $os->os_amount }}</td>
                        </tr>
                            @php($os_installments++)
                            @php($os_sum+= ($os->os_amount > 0) ? $os->os_amount : $os->due_amount - $os->os_amount)
                        @endforeach
                        <tr>
                            <td colspan="2"><span class="pull-right">Total:-</span></td>
                            <td style="color:red;">{!! Form::text('os_amt', $os_sum, ['style' => 'width: 100px'] ) !!}</td>
                        </tr>
                    </tbody>
                </table> 
            </div>
            </div>
            
            <div class="col-sm-4">
            <h5><b>Other Receiveable</b></h5>
            <div class="table-responsive1">
            <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Payment Type</th>
                        <th>Due Date</th>
                        <th>OS Amount</th>
                    
                    </tr>
                </thead>
                <tbody>
                    @php($lpc_sum = 0)
                    @foreach($lpc as $charges)
                    <tr>
                        <td style="color:red;">LPC</td>
                        <td>{{ date('d-m-Y', strtotime($charges->calculation_date)) }}</td>
                        <td style="color:red;">{{ $charges->lpc_amount + $charges->lpc_other_amount }}</td>
                        @php($sum = $charges->lpc_amount + $charges->lpc_other_amount)
                    </tr>
                    @php($lpc_sum += $sum)
                    @endforeach
                    <tr>
                        <td colspan="2"><span class="pull-right">Total:-</span></td>
                        <td style="color:red;">{!! Form::text('lpc_amt', $lpc_sum, ['style' => 'width: 100px'] ) !!}</td>
                    </tr>
                </tbody>
            </table>
            </div>
            </div>
            <div class="col-sm-4">
            <h5><b>Same Member ID Registration No</b></h5>
            <div class="table-responsive1">
            <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Registration No</th>
                        <th>Member Name</th>
                    
                    </tr>
                </thead>
                <tbody>
                    @php($j = 1)

                    @foreach($sameBookings as $booking)
                    <tr>
                        <td>{{ $j }}</td>
                        <td>  @if($booking->registration_no != "")
                        <a href="{{ route('booking.ledger', $booking->registration_no) }}">  {{ $booking->registration_no }} </a>
                        @else
                        <a href="">  {{ $booking->registration_no }} </a>
                        @endif</td>
                        <td>{{ $booking->user->name }}</td>
                    </tr>
                    @php($j++)
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
        </div>
    

            <div id="history" class="tab-pane fade" style="overflow:scroll; max-height: 400px; min-height: 400px; margin-top: 20px;">
                <table class="table table-bordered table">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>CNIC</th>
                            <th>Member Name</th>
                            <th>S/W/D</th>
                            <th>Father/Husband Name</th>
                            <th>Transfer Date</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($transfers as $transfer)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $transfer->currentUser->user->cnic}}</td>
                            <td><a style="font-weight:bold;color:#f2994b;" id="detail"  data-toggle="modal" title="Click to see detail" href='#detailModel' data-userid="{{ $transfer->currentUser->user->id }}">{{ $transfer->currentUser->user->name}}</a></td>
                            <td>{{ $transfer->currentUser->user->guardian_type}}</td>
                            <td>{{ $transfer->currentUser->user->gurdian_name}}</td>
                            <td>{{ date('d-m-Y', strtotime($transfer->transfer_date)) }}</td>
                            <td>{{ $transfer->remarks}}</td>
                        </tr>
                        @php($i++)
                        @endforeach
                    </tbody>
                </table>
            </div>
        

        
          <div id="alert" class="tab-pane fade" style="overflow:scroll; max-height: 400px; min-height: 400px; margin-top: 20px;">
                <table class="table table-bordered table dataTable" id="dataTable">
                    <thead>
                        <tr>
                            <th>Alert No</th>
                            <th>Message</th>
                            <th>Date</th>
<?php
$rights = App\Role::getrights('booking');
if(!Session::get('objects')){
Session::put('objects', Role::getMenuItems());
}

if($rights->can_create){
echo '<th>Action</th>'; 
}
?>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($alerts as $alert)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $alert->message}}</td>
                            <td>{{ $alert->created_at->format('d-m-Y')}}</td>

<?php
$rights = App\Role::getrights('booking');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if($rights->can_create){
echo '<td><a href="'.route('alert.delete', $alert->id).'" class="btn btn-xs btn-warning">Delete</a></td>';
}
?>
                        </tr>
                        @php($i++)
                        @endforeach
                    </tbody>
                </table>
                            <?php
$rights = App\Role::getrights('alert');
if(!Session::get('objects'))
Session::put('objects', Role::getMenuItems());
if($rights->can_create){
echo '<a class="btn btn-primary btn-xs" data-toggle="modal" href="#add-alert">Add New Alert</a>';
}
?>
          </div>
        

        
          <div id="sms" class="tab-pane fade" style="overflow:scroll; max-height: 400px; min-height: 400px; margin-top: 20px;">
            <table class="table table-bordered table dataTable" id="smsDataTable">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Mobile No</th>
                            <th>Message</th>
                            <th>Sending Date</th>
                            <th>Event</th>
                            <th>Sent By</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        
                        @foreach($smsDetail as $sms)
                        @if($sms)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $sms->sent_to }}</td>
                            <td>{!! $sms->message !!}</td>
                            <td>{{ $sms->created_at->format('d-m-Y h:i:s')}}</td>
                            <th>{{ $sms->event }}</th>
                            <th>{{ \App\User::getUserInfo($sms->created_by)->name }}</th>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
          </div>
    
</div></div></div>  

<div class="row bottom-row">
    <div class="col-sm-1">
        <span>Os Inst</span> 
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_inst', $os_installments) !!}
    </div>
    <div class="col-sm-1">
        <span>OS As On</span>
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $os_sum+$lpc_sum) !!}
    </div>        
    <div class="col-sm-1">
        <span>Inst. Recv</span>
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $received_inst_sum) !!}
    </div>       
    <div class="col-sm-1">
        <span>Oth. Recv</span>
    </div>
      <div class="col-sm-1">
        {!! Form::text('os_as_on', $received_other_sum) !!}
    </div>
    <div class="col-sm-1">
       <span>Tot. Recv</span> 
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $received_other_sum+$received_inst_sum) !!}
    </div>       
     <div class="col-sm-1">
       <span>Xtra Recv</span>
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', 0) !!}
    </div>

<br><br>

    <div class="col-sm-3">
        <span class="pull-right">OS - (-PDC - HOLD)</span>
    </div>    
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $os_sum+$lpc_sum) !!}
    </div> 
    <div class="col-sm-1">
       <span>PDC</span>
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $pdc_sum) !!}
    </div>
    <div class="col-sm-5">
       <span class="pull-right">Total Recv</span>
    </div>
    <div class="col-sm-1">
        {!! Form::text('os_as_on', $received_other_sum+$received_inst_sum+$pdc_sum) !!}
    </div>

      
      {!! Form::close() !!}
        </div>
  
<div class="modal fade" id="add-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Alert</h4>
            </div>
           
            <div class="modal-body">
                 <form method="post" id="newAlert" >
                 {{ csrf_field() }}
                <div class="form-group">
                    <label for="member-registration">Registration Number</label>
                    <input type="text" name="registration_no" class="form-control" value="{{ $member->registration_no }}"  readonly="readonly" id="member-registration">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <input type="text" name="message" class="form-control" value="" id="message">
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>           
                </form>
            </div>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="detailModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Member Detail</h4>
      </div>
      <div class="modal-body">
        
           
          
        
                  
                  <h2>Personal Detail</h2>
                  <hr>
                  
                  <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Name</span></div>  
          <div class="col-sm-8" id="name1"></div>  
        </div>
        
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">CNIC</span></div>  
          <div class="col-sm-8" id="cnic1"></div>  
        </div>
        
         <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Permanent Address</span></div>  
          <div class="col-sm-8" id="paddress"></div>  
        </div>
        
         <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Current Address</span></div>  
          <div class="col-sm-8" id="caddress"></div>  
        </div>
        
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Guardian </span></div>  
          <div class="col-sm-8" id="guardian"></div>
          </div>

          <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Mobile </span></div>  
          <div class="col-sm-8" id="mobile"></div>
          </div>
        
         
        <h2>Nominee Detail</h2>
         <hr>
        
             <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Nominee Name </span></div>  
          <div class="col-sm-8" id="nname"></div>
          </div>
          
          <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Nominee CNIC </span></div>  
          <div class="col-sm-8" id="ncnic"></div>
          </div>
          
          <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Nominee Guardian </span></div>  
          <div class="col-sm-8" id="nguardian"></div>
          </div>
          
          <div class="row" style="margin-bottom: 10px;">
          <div class="col-sm-4"><span style="font-weight: bold">Relation With </span></div>  
          <div class="col-sm-8" id="relation"></div>
          </div>
          
         
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


