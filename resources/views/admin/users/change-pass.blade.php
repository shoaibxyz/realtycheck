@extends('admin.layouts.template')
@section('title','Change Password')
@section('user-list-active','active')
@section('change-password-head-active','active')
@section('change-password-active','active')
@section('content')
        
        <div class="col-sm-6 col-sm-offset-3"> 
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif    

         <div class="x_panel">
        <div class="x_title">
            Update Password
            
            <div class="clearfix"></div>
          </div>    

        {!! Form::model($user, ['method' => 'POST', 'route' => ['updategetuser', $user->id], 'class' => 'form-horizontal']) !!}
            
            <div class="form-group">
                {!! Form::label('password', 'New Password') !!}
                {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!} 
            </div>

            
            <div class="form-group">
                {!! Form::submit("Update Password", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>
        </div>
@endsection