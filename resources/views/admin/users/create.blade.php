@extends('admin.layouts.template')

@section('title','Add New Member')

@section('bookings-active','active')
@section('members-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

	<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Member</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Member</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Member</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12" style="margin: auto; float: none; padding-top: 30px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['route' => 'users.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Personal Detail</h2>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Name <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('name', null , array('required' => 'required' , 'class'=>'form-control')  )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">User Name <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('username', null , array('class'=>'form-control')  )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Email <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('email', null , array('class'=>'form-control', 'required' => 'required')  )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Picture <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::file('picture',  ['required' => 'required', 'class' => 'form-control', 'id' => 'picture']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Scanned Copy of CNIC <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::file('cnic_pic', ['required' => 'required', 'class' => 'form-control', 'id' => 'cnic_pic']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Referencce In Pakistan</label>
                            <div class="col-sm-7">
                                {!! Form::text('ref_pk', null, ['id'=>'ref_pk', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Current Address <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('current_address', null , array( 'class'=>'form-control', 'required' => 'required') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Permanent Address <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('permanent_address', null , array( 'class'=>'form-control', 'required' => 'required') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">S/O or D/O OR W/O</label>
                            <div class="col-sm-7">
                            <?php
                                $guardian_typeArr[''] = "";
                                $guardian_typeArr['S/O'] = 'S/O';
                                $guardian_typeArr['D/O'] = 'D/O';
                                $guardian_typeArr['W/O'] = 'W/O';
                            ?>
                            {!! Form::select('guardian_type', $guardian_typeArr, 0, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Guardian Name</label>
                            <div class="col-sm-7">
                                {!! Form::text('guardian_name', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Nationality</label>
                            <div class="col-sm-7">
                                {!! Form::text('nationality', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Country</label>
                            <div class="col-sm-7">
                                {!! Form::text('country_id', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">City</label>
                            <div class="col-sm-7">
                                {!! Form::text('city_id', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">CNIC <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('cnic', null , array('required' => 'required', 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Passport No</label>
                            <div class="col-sm-7">
                                {!! Form::text('passport', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Phone Office</label>
                            <div class="col-sm-7">
                                {!! Form::text('phone_office', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Phone Residential</label>
                            <div class="col-sm-7">
                                {!! Form::text('phone_rec', null , array( 'class'=>'form-control') )!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Phone Mobile <span class="required">*</span></label>
                            <div class="col-sm-7">
                                {!! Form::text('phone_mobile', null , array( 'class'=>'form-control','required' => 'required') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Date of Birth</label>
                            <div class="col-sm-7">
                                {!! Form::text('dob', \Carbon\Carbon::now()->format('d/m/Y'),  array( 'class'=>'form-control datepicker') )!!}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6">
                        <div id="picturePreview" style="text-align: right"></div>
                    </div>

                    <div class="col-md-6">
                        <div id="cnicPreview"></div>
                    </div>
                </div>
                @include('admin.member-registration.create')

                <div class="form-actions text-right pal" style="margin-top: 30px;">
                    {!! Form::submit('Save & Generate', ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('users.index') }}" class="btn btn-grey">Cancel</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy'
	    });

	    $("#picture").change(function () {
		    if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#picturePreview').html('<img src="'+e.target.result+'" width="100" height="80"/>');
                }
                reader.readAsDataURL(this.files[0]);
            }
		});

		$("#cnic_pic").change(function () {
		    if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#cnicPreview').html('<img src="'+e.target.result+'" width="100" height="80"/>');
                }
                reader.readAsDataURL(this.files[0]);
            }
		});
    });
    </script>
@endsection
