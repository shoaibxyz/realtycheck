@extends('admin.layouts.template')

@section('title','Members')

@section('bookings-active','active')
@section('members-active','active')

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Members List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Members List</li>
        </ol>
    </div>
    
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Members List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('users.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Member</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                        <h4 class="box-heading">Success</h4>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table" id="personsTable" >
                    <thead>
                        <tr>
                            <th>Mem Id</th>
                            <th>Reg</th>
                            <th>Name</th>
                            <th>Guardian</th>
                            <th>Phone</th>
                            <th>CNIC</th>
                            <th>Date</th>
                           
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection


@section('javascript')

    <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/jquery.timer.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/bootstrap.min.js"></script>
    {{-- <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/ajaxmask.js"></script> --}}

    <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/custom.js"></script>

    <link type="image/gif" href="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/image/favicon.gif" rel="icon">

    <script type="text/javascript">
        $(document).ready(function(){
            var columns = [
                { data: "member_id" },
                { data: "reg_no" },
                { data: 'name', name: 'name' },
                { data: 'guardian', name: 'guardian' },
                { data: 'phone', name: 'phone' },
                { data: 'cnic', name: 'cnic' },
                { data: 'date', name: 'date' },
                
                { data: 'edit', name: 'edit' },
                { data: 'delete', name: 'delete' },
            ];

            $('#personsTable').DataTable({
                "processing": true,
                "serverSide": true,
                "pageLength": 10,
                "info": true,
                "ajax": '{!! route('users.getPersons') !!}',
                "columns": columns
            });

            $(document).on('click', '.biometric', function(e){
                var user_id = $(this).data('userid');
                var user_name = $(this).data('username');
                $('body').ajaxMask();
                regStats = 0;
                regCt = -1;
                try {
                    timer_register.stop();
                } catch (err) {
                    console.log('Registration timer has been init');
                }
                var limit = 4;
                var ct = 1;
                var timeout = 5000;
                timer_register = $.timer(timeout, function() {
                    console.log("'" + user_name + "' registration checking...");
                    user_checkregister(user_id, $("#user_finger_" + user_id).val());
                    if (ct >= limit || regStats == 1) {
                        timer_register.stop();
                        console.log("'" + user_name + "' registration checking end");
                        if (ct >= limit && regStats == 0) {
                            swal("Error", user_name + " biometric verification is failed",'error');
                            $('body').ajaxMask({
                                stop: true
                            });
                        }
                        if (regStats == 1) {
                            $("#user_finger_" + user_id).val(regCt);
                            $("#fingerPrint_" + user_id).html("Verified");
                            $("#fingerPrint_" + user_id).removeClass("btn-default");
                            $("#fingerPrint_" + user_id).addClass("btn-success");
                            $("#fingerPrint_" + user_id).attr("href","#");
                            $("#fingerPrint_" + user_id).removeClass("biometric");
                            swal("Success", user_name + " biometric verification is successfully added",'success');
                            $('body').ajaxMask({
                                stop: true
                            });
                        }
                    }
                    ct++;
                });

            });

            function user_checkregister(user_id, current) {
                $.ajax({
                    url: "{{ env('SDK_PATH') }}user.php?action=checkreg&user_id=" + user_id + "&current=" + current,
                    type: "GET",
                    success: function(data) {
                        try {
                            var res = jQuery.parseJSON(data);
                            if (res.result) {
                                regStats = 1;
                                $.each(res, function(key, value) {
                                    if (key == 'current') {
                                        regCt = value;
                                    }
                                });
                            }
                        } catch (err) {
                            alert(err.message);
                        }
                    }
                });
            }


            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    form.submit();
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                    // event.preventDefault();
                }
                });
            });
        });

        (function($) {
            $.fn.ajaxMask = function(options) {

                return this.each(function() {
                    var settings = $.extend({
                        stop: false,
                        }, options);

                    if (!settings.stop) {
                    var loadingDiv = $('<div class="ajax-mask"><div class="loading"><img src="../public/images/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + 'Please wait...' + '</span></div></div>')
                        .css({
                        'position': 'absolute',
                        'top': 0,
                        'left':0,
                        'width':'100%',
                        'height':'100%',
                        });

                    $(this).css({ 'position':'relative' }).append(loadingDiv);
                    } else {
                    $('.ajax-mask').remove();
                    }
                });

            }
        })(jQuery);
    </script>
@endsection
