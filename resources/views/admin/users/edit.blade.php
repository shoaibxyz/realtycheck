@extends('admin.layouts.template')

@section('title','Edit Member')

@section('bookings-active','active')
@section('members-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Member</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Member</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Member</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12" style="margin: auto; float: none; padding-top: 0px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-sm-12">

                        {!! Form::model($persons, ['method' => 'POST', 'route' => ['person.update', $persons->id], 'class' => 'form-horizontal']) !!}

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email<span class="required">*</span></label>
                                <div class="col-sm-8">
                                    {!! Form::text('email', $persons->email , array('class'=>'form-control','required'=>'required')  )!!}
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Permanent Address<span class="required">*</span></label>
                                <div class="col-sm-8">
                                    {!! Form::text('permanent_address', $persons->current_address , array( 'class'=>'form-control','required'=>'required') )!!}
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Current Address<span class="required">*</span></label>
                                <div class="col-sm-8">
                                    {!! Form::text('current_address', $persons->permanent_address , array( 'class'=>'form-control','required'=>'required') )!!}
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-actions text-right pal">
                                {!! Form::hidden('_method', 'PUT') !!}
                                {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                                <a href="{{ route('users.index') }}" class="btn btn-grey">Cancel</a>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
