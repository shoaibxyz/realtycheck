@extends('admin.layouts.template')

@section('title','Reply')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Comment Box</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Comment Box</li>
        </ol>
    </div>
    
    
    
    
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Issue : {{$task->subject}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-12 col-md-12">
                
        
                         @foreach($reply as $r)
                            <!-- POST -->
                            <div class="post beforepagination">
                                <div class="topwrap">
                                    <div class="userinfo pull-left">
                                        <div class="avatar">
                                            <img src="{{ env('STORAGE_PATH') . $r->user->picture }}">
                                        
                                        </div>

                                      
                                    </div>
                                    <div class="posttext pull-right">
                                        <h2>Reply By : {{$r->user->name}}</h2>
                                        <p>{{$r->content}}</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                              
                                <div class="postinfobot">

                                   
                                    <div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted on : {{$r->created_at}}</div>

                                    
                                    <div class="clearfix"></div>
                                </div>
                            </div>

            @endforeach
                
    
                              <h2>Add New Reply</h2>
                                      <div class="post">
                                        {!! Form::open(['method' => 'POST', 'url' => 'admin/support/reply/store', 'class' => 'form-horizontal','files' => true]) !!}
                                         <div class="topwrap">
                                    
                                           <div class="posttext1 pull-left">
                                            <div class="textwraper">
                                                <div class="postreply">Post a Reply</div>
                                                <textarea name="reply" id="reply" placeholder="Type your message here"></textarea>
                                                
                                            </div>
                                              <div class="form-group" style="padding-left:17px;">
                                     <button type="submit" class="btn btn-primary">Post Reply</button>
                                    <!--{!! Form::submit("Reply", ['class' => 'btn btn-success']) !!}-->
                                    {!! Form::hidden('task_id', $task->id, array('id' => 'task_id')) !!}
                                     </div>
                                      {!! Form::close() !!}
                                    <div class="container-fluid">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>                              
                                                       
                              
                    	
                         
            </div>
        </div>
    </div>
@endsection
