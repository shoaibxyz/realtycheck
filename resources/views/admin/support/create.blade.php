@extends('admin.layouts.template')

@section('title','Task')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Task</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Task</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Task</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'url' => ['admin/support/store'], 'class' => 'form-horizontal','files' => true]) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Subject <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('subject', '', ['class' => 'form-control', 'id' => 'subject', 'required' => 'required']) !!}
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Category <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <?php $categoryArr[''] = 'choose one'; ?>
                            @foreach($category as $c)
                                <?php $categoryArr[$c->id] = $c->name; ?>
                            @endforeach
                            {!! Form::select('category', $categoryArr, 0,  array('class'=>'form-control', 'id' => 'category', 'required' => 'required') )!!}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Message_body</label>
                        <div class="col-sm-8">
                              <textarea class="form-control" name="message_body" rows="5" id="message_body"></textarea>
                        </div>
                    </div>
                   
                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ url('task/index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
