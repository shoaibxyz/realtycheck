@extends('admin.layouts.template')

@section('title','Task')
@section('style')
<style>
 .bs-example{
        margin: 30px;        
    }
</style>
@endsection

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Task List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Task List</li>
        </ol>
    </div>
    <div class="content">
        
           
            
            
            <div class="x_panel">
            <div class="x_title">
                <h2>Task List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ url('admin/support/create')}}" class="btn btn-primary" > <i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                
                <div class="col-lg-4">
                    
                    <div class="bs-example"> 
                    <ul class="list-group">
                    <li class="list-group-item"><h2>Category</h2></li>
                    @foreach($category as $c)
                    <li class="list-group-item"><a href="{{url('admin/support/categorytask',$c->id)}}">{{$c->name}}</a></li>
                    @endforeach
                </ul>
                </div>
                    
                </div>
                <div class="col-lg-8">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th style="width: 5%">#Id</th>
                            <th style="width: 25%">Subject</th>
                            <th style="width: 20%">Message_body</th>
                            <th style="width: 20%">Created By</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                           
        
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($task as $t)
                            <tr>
                                <td>{{ $i }}</td>
                                <td><a href="{{ url('admin/support/reply/' . $t->id)}}">{{ $t->subject }}</a></td>
                                <td>{{ $t->message_body }}</td>
                               
                                <td>{{ $t->user->name }}</td>
                                <td>{{ \App\Helpers::support_status($t->status) }}</td>
                                 <td><a class="btn btn-xs btn-info status_modal" data-toggle="modal" href="#viewstatusModel" data-productid='{{ $t->id }}'>Change Status</a></td>
                            </tr>
                                   
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
            </div>
           
    </div>
      
    
     <div class="modal fade" id="viewstatusModel">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Status</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['method' => 'POST', 'url' => 'admin/support/status', 'class' => 'form-horizontal' , 'id' => 'validate_form']) !!}
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">Task ID</label>
                   <div class="col-sm-9">
                    {!! Form::text('task_id', '', ['class' => 'form-control', 'id' => 'task_id' , 'required' => 'required'  , 'readonly' => 'readonly']) !!} 
                   </div>
                   </div>
                 <div class="form-group">
                    <label for="support_status" class="col-sm-3">Status</label>
                    <div class="col-sm-8">
                        @php($statusArr = [])
                        @foreach(\App\Helpers::support_status() as $key => $status)
                            @php($statusArr[$key] = $status)
                        @endforeach
                        {!! Form::select('support_status', $statusArr, '', ['class' => 'form-control', 'id' => 'support_status']) !!}
                    </div>
                </div>


                </div>
            
            <div class="modal-footer">
            {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            {!! Form::close() !!}
            </div>
        </div>
      </div>
 


@endsection
@section('javascript')
    <script type="text/javascript">
    $(document).ready(function(){
        
        
          $('.status_modal').on('click',function(event){
            event.preventDefault();
          var productId = $(this).data('productid');
          //alret(productId);
          $.ajax({
            url: '{{ url('admin/support/findtask') }}/'+productId,
            type: 'GET'
          })
          .done(function(data) {
            console.log(data);
              
              $('#task_id').val(data.task.id);
              //$('#support_status').val(data.task.status);

          })
          .fail(function() {
            console.log("error");
          });
          
        });
    });
     </script>

@endsection
