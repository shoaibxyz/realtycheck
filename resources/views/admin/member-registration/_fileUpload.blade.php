<div class="bg-danger">Upload Your Documents <small class="pull-right"><a href="#" id="add_file_field">Add File</a></small></div>
<div class="files">
	<div class="form-group">
		{!! Form::label('file', 'Upload File')!!}
		{!! Form::file('file', ['class' => 'form-control', 'id' => 'file']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('file_description', 'File Description')!!}
		{!! Form::textarea('file_description', null, ['class' => 'form-control', 'id' => 'file_description']) !!}
	</div>
</div>

@section('javascript')
	{{-- <script type="text/javascript">
		$(document).ready(function(){
			$('#add_file_field').click(function(e){
				e.preventDefault();
				$('.files').append('<div class="files"><div class="form-group">{!! Form::label('file', 'Upload File')!!}{!! Form::file('file[]', ['class' => 'form-control', 'id' => 'file']) !!}</div><div class="form-group">{!! Form::label('file_description', 'File Description')!!}{!! Form::textarea('file_description[]', null, ['class' => 'form-control', 'id' => 'file_description']) !!}</div></div>');
			});
		});
	</script> --}}
@endsection