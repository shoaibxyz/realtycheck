@extends('admin.template')
@section('title','Add New Member')

@section('bookings-active','active')
@section('members-active','active')

@section('content')
        
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(['route'=>'member-registration.store', 'files' => true])!!}
	@include('admin.member-registration._form')
{!! Form::submit('add' , ['class' => 'btn btn-primary'])!!}

{!! Form::close() !!}



</div>
</div>
@endsection