@extends('admin.layouts.template')

@section('title','Edit Member')

@section('bookings-active','active')
@section('members-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Member</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Member</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Edit Member</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12" style="margin: auto; float: none; padding-top: 0px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
	            {!! Form::model($person, ['method' => 'PATCH', 'route' => ['member-registration.update', $person->id] , 'class' => 'form-horizontal', 'files'=>true]) !!}
	        	<div class="col-sm-6">

			        <div class="form-group">
	                	{!! Form::label('picture', 'picture', ['class' => 'col-sm-6'])!!}
		               <div class="col-md-6">
		                <img id="img-preview" src="{{ env('STORAGE_PATH'). $person->picture }}" name="picture"  style="margin: 10px auto;" alt="Picture" height="100" width="200" />
		                </div>
	                </div>

	                <div class="form-group">
	                	{!! Form::label('cnic_pic', 'CNIC', ['class' => 'col-sm-6'])!!}
		               <div class="col-md-6">
		                <a href=""><img id="img-preview" src="{{ env('STORAGE_PATH'). $person->cnic_pic }}" name="cnic_pic"  style="margin: 10px auto;" alt="Picture" height="100" width="200" /></a>
		                </div>
	                </div>

	                <div class="form-group">
			            {!! Form::label('name', 'Name') !!}
			            {!! Form::text('name', null , array('required' => 'required' , 'class'=>'form-control')  )!!}
			        </div>

					<div class="form-group">
					{!! Form::label('email', 'Email')!!}
					{!! Form::text('email', null , array('required' => 'required' , 'class'=>'form-control')  )!!}
					</div>


					<div class="form-group">
					{!! Form::label('permanent_address', 'Permanent Address')!!}
					{!! Form::text('permanent_address', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('current_address', 'Current Address')!!}
					{!! Form::text('current_address', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('cnic', 'CNIC')!!}
					{!! Form::text('cnic', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('guardian_type', 's/o or d/o')!!}
					{!! Form::text('guardian_type', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('gurdian_name', 'Guardian Name')!!}
					{!! Form::text('gurdian_name', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('nationality', 'Nationality')!!}
					{!! Form::text('nationality', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('ref_pk', 'Reference In Pakistan')!!}
					{!! Form::text('ref_pk', null , array( 'class'=>'form-control') )!!}
					</div>
		        </div>

	        	<div class="col-sm-6">

	        		<div class="form-group">
					{!! Form::label('registration_no', 'Registration No')!!}
					{!! Form::text('registration_no', $person->member->registration_no , array('required' => 'required' , 'readonly' => 'readonly'  , 'class'=>'form-control') )!!}
					</div>

	        		<div class="form-group">
					{!! Form::label('country_id', 'Country')!!}
					{!! Form::text('country_id', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('city_id', 'City')!!}
					{!! Form::text('city_id', null , array( 'class'=>'form-control') )!!}
					</div>



					<div class="form-group">
					{!! Form::label('passport', 'Passport No')!!}
					{!! Form::text('passport', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('phone_office', 'Phone Office')!!}
					{!! Form::text('phone_office', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('phone_res', 'Phone Residential')!!}
					{!! Form::text('phone_res', null , array( 'class'=>'form-control') )!!}
					</div>
					<div class="form-group">
					{!! Form::label('phone_mobile', 'Phone Mobile')!!}
					{!! Form::text('phone_mobile', null , array( 'class'=>'form-control') )!!}
					</div>

					<div class="form-group">
					{!! Form::label('nominee_name', 'Nominee Name')!!}
					{!! Form::text('nominee_name', $members->nominee_name, ['class' => 'form-control', 'id' => 'nominee_name']) !!}
					</div>

					<div class="form-group">
					{!! Form::label('nominee_guardian', 'Nominee Guardian')!!}
					{!! Form::text('nominee_guardian', $members->nominee_guardian, ['class' => 'form-control', 'id' => 'nominee_guardian']) !!}
					</div>

					<div class="form-group">
					{!! Form::label('nominee_cnic', 'Nominee CNIC')!!}
					{!! Form::text('nominee_cnic', $members->nominee_cnic, ['class' => 'form-control', 'id' => 'nominee_cnic']) !!}
					</div>

					<div class="form-group">
					{!! Form::label('relation_with_member', 'Relation With Member')!!}
					{!! Form::text('relation_with_member', $members->relation_with_member, ['class' => 'form-control', 'id' => 'relation_with_member']) !!}
					</div>

					<div class="form-group">
					{!! Form::label('dob', 'Date of Birth')!!}
					{!! Form::text('dob', $person->dob,  array( 'class'=>'form-control datepicker') )!!}
					</div>

	        	</div>

		        <div class="form-group">
		            {!! Form::hidden('_method', 'PATCH') !!}
		            {!! Form::hidden('person_id', $person->id) !!}
		            {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
		        </div>

	            {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection


