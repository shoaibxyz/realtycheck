<div class="col-md-12"></div>
    <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Nominee Detail</h2>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Nominee Name</label>
                <div class="col-sm-7">
                    {!! Form::text('nominee_name', null , array( 'class'=>'form-control') )!!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Nominee Guardian</label>
                <div class="col-sm-7">
                    {!! Form::text('nominee_guardian', null , array('class'=>'form-control') )!!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Nominee CNIC</label>
                <div class="col-sm-7">
                    {!! Form::text('nominee_cnic', null , array('class'=>'form-control') )!!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Relation With Member</label>
                <div class="col-sm-7">
                    {!! Form::text('relation_with_member', null , array( 'class'=>'form-control') )!!}
                </div>
            </div>
        </div>
    </div>
</div>
