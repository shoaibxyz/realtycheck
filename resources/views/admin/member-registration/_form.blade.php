
        <div class="form-group">
		{!! Form::label('person', 'Person')!!}
		{!! Form::text('person', $personname, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('nominee_name', 'Nominee Name')!!}
		{!! Form::text('nominee_name', null, ['class' => 'form-control', 'id' => 'nominee_name']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('nominee_guardian', 'Nominee Guardian')!!}
		{!! Form::text('nominee_guardian', null, ['class' => 'form-control', 'id' => 'nominee_guardian']) !!}
		</div>
		
		<div class="form-group">
		{!! Form::label('nominee_cnic', 'Nominee CNIC')!!}
		{!! Form::text('nominee_cnic', null, ['class' => 'form-control', 'id' => 'nominee_cnic']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('relation_with_member', 'Relation With Member')!!}
		{!! Form::text('relation_with_member', null, ['class' => 'form-control', 'id' => 'relation_with_member']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('lpc_waive', 'LPC Waive')!!}
		{!! Form::text('lpc_waive', null, ['class' => 'form-control', 'id' => 'lpc_waive']) !!}
		</div>

		<?php 
			$reg_status[''] =  "";
			$reg_status[1] =  "Approved";
			$reg_status[0] =  "Pending";
		?>
		<div class="form-group">
		{!! Form::label('registeration_status', 'Registeration Status')!!}
		{!! Form::select('registeration_status', $reg_status, 0, ['class' => 'form-control', 'id' => 'registeration_status']) !!}
		</div>