@extends('admin.layouts.template')

@section('title','Add New Member')

@section('bookings-active','active')
@section('members-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Members List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Members List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Member</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('payment-schedule.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Member</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Reg No</th>
                        <th>Reference No</th>
                        <th>Nominee Name</th>
                        <th>Nominee Guardian</th>
                        <th>Nominee CNIC</th>
                        <th>Relation With Member</th>
                        <th>LPC Waive</th>
                        <th>Registeration Status</th>
                        <th>DD Pay Order No</th>
                        <th>Total Amount</th>
                        <th>Cheque No</th>
                        <th>Bank Name</th>
                        <th>Lumpsum Payment</th>
                        <th>Down Payment</th>
                        <th>Other Payments</th>
                        <th>Payment Schedule ID</th>
                        <th>Person ID</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($members as $member)
                        <tr>
                        <td>{{ $member->id }}</td>
                        <td>{{ $member->registration_no }}</td>
                        <td>{{ $member->reference_no }}</td>
                        <td>{{ $member->nominee_name }}</td>
                        <td>{{ $member->nominee_guardian }}</td>
                        <td>{{ $member->nominee_cnic }}</td>
                        <td>{{ $member->relation_with_member }}</td>
                        <td>{{ $member->lpc_waive }}</td>
                        <td>{{ $member->registeration_status }}</td>
                        <td>{{ $member->dd_pay_order_no }}</td>
                        <td>{{ $member->total_amount }}</td>
                        <td>{{ $member->cheque_no }}</td>
                        <td>{{ $member->bank_name }}</td>
                        <td>{{ $member->lumpsum_payment }}</td>
                        <td>{{ $member->down_payment }}</td>
                        <td>{{ $member->other_payments }}</td>
                        <td>{{ ($member->payment_schedule) }}</td>
                        <td>{{ ($member->persons) }}</td>

                        <td><a href="{{ route('member-registration.edit', $member->id )}}" class="btn btn-info btn-xs">Edit</a></td>
                        <td>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['member-registration.destroy', $member->id]]) !!}
                                        {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
