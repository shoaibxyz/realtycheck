@extends('admin.layouts.template')
@section('title','Transfer Detail')
@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer-active','active')
@section('stylesheet')
@endsection

@section('content')
<div class="x_panel">
  
  <div class="x_title">
    <h2>Transfer Detail</h2>
    
    <div class="clearfix"></div>
  </div>    
  <div id="app">
    
    <div class="container">
      <div class="leadger">
          <div class="main">
            <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                  <div class="row">
                      <div class="col-sm-3">
                        <b>TS Downloaded:</b> {{ $data['transfer']->is_ts_dl ? "Yes" : "No" }}
                      </div>

                      <div class="col-sm-3">
                        @if($data['transfer']->ts_dl_date)
                          @php($date = date('d-m-Y', strtotime($data['transfer']->ts_dl_date )) )
                        @else
                          @php($date = "")
                        @endif
                        <b>TS Downloaded Date:</b> {{ $date }}
                      </div>

                      <div class="col-sm-3">
                        <b>Is NDC Downloaded:</b> {{ $data['transfer']->is_ndc_dl ? "Yes" : "No" }}
                      </div>

                      <div class="col-sm-3">
                        @if($data['transfer']->ndc_dl_date)
                          @php($date = date('d-m-Y', strtotime($data['transfer']->ndc_dl_date )) )
                        @else
                          @php($date = "")
                        @endif
                        <b>NDC Downloaded Date:</b> {{ $date }}
                      </div>

                    </div>
                  </div>
                    </div>

            <div class="memberinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
              <div class="row">
                <div class="col-xs-6">
                  <h3><u>Seller Information</u></h3>
                </div>

                <div class="col-xs-6">
                  <h3><u>Buyer Information</u></h3>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  @if($data['currOwner']['user']->picture)
                    <div style="width:3.5cm;height:4.5cm;">
                        <img src="{{ env('STORAGE_PATH').$data['currOwner']['user']->picture }}" alt="" style="width: inherit">
                    </div>
                   @else
                    <img src="{{ asset('public/')}}/images/user.png" alt="" style="width:3.5cm;height:4.5cm;">
                   @endif
                   <br><br>
                  <p><strong>Member Name: </strong>&nbsp; {{ $data['currOwner']['user']->name }}</p>
                  <p> <strong>{{ $data['currOwner']['user']->guardian_type }}:</strong> &nbsp; {{ $data['currOwner']['user']->gurdian_name }}</p>
                  <p> <strong>CNIC No:</strong> &nbsp; {{ $data['currOwner']['user']->cnic }}</p>
                  <p><strong>Current Address:</strong> &nbsp; {{ $data['currOwner']['user']->current_address }}</p>
                  <p><strong>Permanent Address:</strong> &nbsp; {{ $data['currOwner']['user']->permanent_address }}</p>
                  <p> <strong>Mobile No:</strong> &nbsp; {{ $data['currOwner']['user']->phone_mobile }}</p>
                  <p><strong>Form No : </strong>&nbsp; {{ $data['currOwner']->reference_no }}</p>

                  <hr>
                  <h4>Nominee Detail</h4>
                  <p>
                    <strong>Nominee Name</strong> &nbsp;
                    {{ $data['currOwner']->nominee_name }}
                  </p>
                  <p>
                    <strong>Nominee CNIC </strong> &nbsp;
                    {{ $data['currOwner']->nominee_cnic }}
                  </p>
                  <p>
                    <strong>Nominee Guardian</strong> &nbsp;
                    {{ $data['currOwner']->relation_with_member }}
                  </p>
                  <p>
                    <strong>Nominee Guardian Name</strong> &nbsp;
                    {{ $data['currOwner']->nominee_guardian }}
                  </p>
                </div>
                <div class="col-xs-6">
                  @if($data['newOwner']['user']->picture)
                   <div style="width:3.5cm;height:4.5cm;">
                        <img src="{{ env('STORAGE_PATH').$data['newOwner']['user']->picture }}" alt="" style="width:3.5cm;height:4.5cm;">
                    </div>
                   @else
                    <img src="{{ asset('public/')}}/images/user.png" alt="" style="width:3.5cm;height:4.5cm;">
                   @endif
                   <br><br>
                  <p><strong>Member Name: </strong>&nbsp; {{ $data['newOwner']['user']->name }}</p>
                  <p> <strong>{{ $data['newOwner']['user']->guardian_type }}:</strong> &nbsp; {{ $data['newOwner']['user']->gurdian_name }}</p>
                  <p> <strong>CNIC No:</strong> &nbsp; {{ $data['newOwner']['user']->cnic }}</p>
                  <p><strong>Current Address:</strong> &nbsp; {{ $data['newOwner']['user']->current_address }}</p>
                  <p><strong>Permanent Address:</strong> &nbsp; {{ $data['newOwner']['user']->permanent_address }}</p>
                  <p> <strong>Mobile No:</strong> &nbsp; {{ $data['newOwner']['user']->phone_mobile }}</p>
                  <p><strong>Form No : </strong>&nbsp; {{ $data['newOwner']->reference_no }}</p>
                  <hr>
                  <h4>Nominee Detail</h4>
                  <p>
                    <strong>Nominee Name</strong> &nbsp;
                    {{ $data['newOwner']->nominee_name }}
                  </p>
                  <p>
                    <strong>Nominee CNIC </strong> &nbsp;
                    {{ $data['newOwner']->nominee_cnic }}
                  </p>
                  <p>
                    <strong>Nominee Guardian</strong> &nbsp;
                    {{ $data['newOwner']->relation_with_member }}
                  </p>
                  <p>
                    <strong>Nominee Guardian Name</strong> &nbsp;
                    {{ $data['newOwner']->nominee_guardian }}
                  </p>
                </div>
              </div>
            </div>

              {{-- end --}}
              
              {{-- plotinfo --}}
              <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>File/Plot Information</u></h3></div>
                    <div class="col-xs-3">
                      <p><strong>Size: </strong>{{ $data['paymentPlan']->plotSize->plot_size }}</p>
                      <p> <strong>File/Plot No:</strong> {{ $data['currOwner']->reference_no }}</p>
                      <p><strong>Type:</strong> {{ $data['paymentPlan']->plot_nature}}</p>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-3">
                      <p> <strong>Booking Date:</strong>{{ $data['booking']->created_at->format('d-m-Y') }}</p>
                      <p><strong>Payment Code: </strong>{{ $data['booking']->paymentPlan->payment_desc }}</p>
                      <p> <strong>Street#: </strong></p>
                      
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-3" id="right-col" >
                      <p><strong>Registration No:</strong>   &nbsp; {{ $data['transfer']->reg_no }}</p>
                      <p><strong>Block Name:</strong></p>
                      <p> <strong>Dimension:</strong></p>
                    </div>
                  </div>
                </div>

              <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>Transfer Images</u></h3></div>
                    @foreach($transferImages as $image)
                    @if($image->is_document == 0)
                    <div class="col-sm-3">
                        <img src="{{ $image->picture }}" class="thumbnail">
                        <p><b>Captured By:</b> {{ App\User::getUserInfo($image->created_by)->name }}</p>
                        <p><b>Date:</b> {{ $image->created_at->format('d-m-Y H:i:s') }}</p>
                    </div>   
                    @endif
                    @endforeach
                  </div>
                </div>

                <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>Transfer documents</u></h3></div>
                    @foreach($transferImages as $image)
                    @if($image->is_document == 1)
                    <div class="col-sm-3">
                        @if(pathinfo($image->document, PATHINFO_EXTENSION) == 'jpg' || pathinfo($image->document, PATHINFO_EXTENSION) == 'jpeg' || pathinfo($image->document, PATHINFO_EXTENSION) == 'png') 
                        <img src="{{ asset("storage/app/public/transfer/documents/".$image->document) }}" class="thumbnail" style="width: 100%;">
                        @else
                          <img src="{{ asset('public/images/docx.png') }}" class="thumbnail" style="width: 100%;">
                        @endif
                        <div style="margin-bottom: 25px;">
                          <div class='col-sm-6'>
                            <a href="{{ asset("storage/app/public/transfer/documents/".$image->document) }}" class="btn btn-xs btn-warning" target="_blank">Preview</a>
                          </div>

                          <div class='col-sm-6'>
                            <a href="{{ asset("storage/app/public/transfer/documents/".$image->document) }}" class="btn btn-xs btn-success" target="_blank">Download</a>
                          </div>
                        </div>
                        <p><b>Added By:</b> {{ App\User::getUserInfo($image->created_by)->name }}</p>
                        <p><b>Doc Type:</b> {{ pathinfo($image->document, PATHINFO_EXTENSION) }}</p>
                        <p><b>Date:</b> {{ $image->created_at->format('d-m-Y H:i:s') }}</p>
                    </div>   
                    @endif
                    @endforeach
                  </div>
                </div>

                  

                  </div>


                </div>
              </div></div>
              

              @endsection

              @section('javascript')
              <script type="text/javascript">

                function CallPrint(strid)
                {
                  var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_self';

                  var printWindow = window.open(window.location.href , windowName);
                  printWindow.document.body.innerHTML = printContent.innerHTML;
                  printWindow.document.close();
                  printWindow.focus();
                  printWindow.print();
                  printWindow.close();
                  return false;
                }

                $('#printLedger').on('click',function(){
                  CallPrint('app');
                });
              </script>
              @endsection