@extends('admin.layouts.template')
@section('title','Transfer Edit')
@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer-active','active')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Current Member</h2>
            
            <div class="clearfix"></div>
          </div>    
    {!! Form::model($transfer, ['route' => ['transfer.update', $transfer->id], 'class' => 'form-horizontal' , 'id' => 'form1', 'files' => true]) !!}
    <input type="hidden" name="_method" value="PATCH">
    <div class="row">
        @include('errors')
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('registration_no', 'Registration Number') !!}
                {!! Form::text('registration_no', $currOwner->registration_no, [ 'class' => 'form-control', 'id' => 'registration_no']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', $currUser->name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'name']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('guardian_type', 'Guardian Type') !!}
                {!! Form::text('guardian_type', $currUser->guardian_type, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_type']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('guardian_name', 'Guardian Name') !!}
                {!! Form::text('guardian_name', $currUser->gurdian_name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_name']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('nationality', 'Nationality') !!}
                {!! Form::text('nationality', $currUser->nationality, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'nationality']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('cnic', 'CNIC') !!}
                {!! Form::text('cnic', $currUser->cnic, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'cnic']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('address1', 'Current Address') !!}
                {!! Form::text('address1', $currUser->current_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address1']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('address2', 'Permanenet Address') !!}
                {!! Form::text('address2', $currUser->permanent_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address2']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('country_id', 'Country') !!}
                {!! Form::text('country_id', $currUser->country_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'country_id']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('city_id', 'City') !!}
                {!! Form::text('city_id', $currUser->city_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'city_id']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('phone_office', 'Phone office') !!}
                {!! Form::text('phone_office', $currUser->phone_office, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_office']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('phone_rec', 'Phone REC') !!}
                {!! Form::text('phone_rec', $currUser->phone_res, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_rec']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('phone_mobile', 'Mobile') !!}
                {!! Form::text('phone_mobile', $currUser->phone_mobile, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_mobile']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', $currUser->email, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'email']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('plot_size', 'Plot Size') !!}
                {!! Form::text('plot_size', $paymentPlan->plotSize->plot_size, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'plot_size']) !!}
            </div>
        </div>
        </div>
    </div> 

    <div class="x_panel">
        <div class="x_title">
            <h2>New Member</h2>
            <div class="clearfix"></div>
          </div>     

           <div class="x_panel">
            <div class="x_title">
                <h2>Images Attached</h2>
                <div class="clearfix"></div>
              </div>     
                <div class="col-sm-3">
                    <p>User Pic</p>
                    <img src="{{ asset('public/')}}/images/{{ $newUser->picture }}" alt="" class="img-thumbnail" style="height:150px">
                </div>

                <div class="col-sm-3">
                    <p>CNIC</p>
                    <img src="{{ asset('public/')}}/images/{{ $newUser->cnic_pic }}" alt="" class="img-thumbnail" style="height:150px">
                </div>

                <div class="col-sm-3">
                    <p>Affidavit</p>
                    <img src="{{ asset('public/')}}/images/{{ $newUser->affidavit_address }}" alt="" class="img-thumbnail" style="height:150px">
                </div>
        </div>
        @include('admin.transfers._tformEdit')
    </div>
    <div class="clearfix">
    </div>


            <div class="form-group">
                {!! Form::hidden('user_id', $newUser->id) !!}
                {!! Form::submit("Update", ['class' => 'btn btn-default pull-right', 'id' => 'submit' ,'data-confirm' => 'Have you double checked data? ']) !!}
            </div>

             {!! Form::close() !!}
    @endsection

@section('javascript')
    @section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });


    $('#registration_no').blur(function(){
            var reg_no = $(this).val();             
            $.ajax({
               url: '{{ URL::to('/findname') }}',
                type: 'POST',
                data: {'registration_no': reg_no},
                success: function(data){
                    $('#person_id').val(data.person.id);
                    $('#name').val(data.person.name);
                    $('#guardian_type').val(data.person.guardian_type);
                    $('#guardian_name').val(data.person.gurdian_name);
                    $('#nationality').val(data.person.nationality);
                    $('#cnic').val(data.person.cnic);
                    $('#address1').val(data.person.current_address);
                    $('#address2').val(data.person.permanent_address);
                    $('#country_id').val(data.person.country_id);
                    $('#city_id').val(data.person.city_id);
                    $('#phone_office').val(data.person.phone_office);
                    
                    $('#phone_rec').val(data.person.phone_rec);
                    $('#phone_mobile').val(data.person.phone_mobile);
                    $('#email').val(data.person.email);
                    $('#member_id').val(data.member.id);
                    $('#booking_price').val(data.payment.payment_amount);
                    $('#booking_date').val(data.payment.payment_date);
                    $('#reg_no1').val(data.member.registration_no);
                    $('#plot_size').val(data.plot.plot_size);
                    
                },



                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });


        });


$('#submit').on('click', function (e) {
        if (confirm($(this).data('confirm'))) {
            return true;
        }
        else {
            return false;
        }
    });
    </script>
    @endsection
</link>