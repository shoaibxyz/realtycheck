 <div class="row">
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('reg_no1', 'Registration Number') !!} <span class="required">*</span>
    {!! Form::text('reg_no1', '', [ 'required' => 'required' , 'class' => 'form-control', 'id' => 'reg_no1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('name1', 'Name') !!} <span class="required">*</span>
    {!! Form::text('name1', '', [ 'required' => 'required' ,'class' => 'form-control', 'id' => 'name1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('guardian_type1', 'Relation With Member')!!} <span class="required">*</span>
<?php 
	$guardian_typeArr[''] = "";
	$guardian_typeArr['S/O'] = 'S/O';
	$guardian_typeArr['D/O'] = 'D/O';
	$guardian_typeArr['W/O'] = 'W/O';
?>
{!! Form::select('guardian_type1', $guardian_typeArr, 0, ['class' => 'form-control' , 'required' => 'required', 'id' => 'guardian_type1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('guardian_name1', 'Guardian Name') !!} <span class="required">*</span>
    {!! Form::text('guardian_name1', '', ['required' => 'required' ,'class' => 'form-control', 'id' => 'guardian_name1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('nationality1', 'Nationality') !!} <span class="required">*</span>
    {!! Form::text('nationality1', '', ['required' => 'required' , 'class' => 'form-control', 'id' => 'nationality1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('cnic1', 'CNIC') !!} <span class="required">*</span>
    {!! Form::text('cnic1', '', [ 'required' => 'required' ,'class' => 'form-control', 'id' => 'cnic1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('address11', 'Current Address') !!} <span class="required">*</span>
    {!! Form::text('address11', '', [ 'required' => 'required' ,'class' => 'form-control', 'id' => 'address11']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('address21', 'Permanenet Address') !!} <span class="required">*</span>
    {!! Form::text('address21', '', ['required' => 'required' , 'class' => 'form-control', 'id' => 'address21']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('country_id1', 'Country') !!} <span class="required">*</span>
    {!! Form::text('country_id1', '', [ 'required' => 'required' ,'class' => 'form-control', 'id' => 'country_id1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('city_id1', 'City') !!} <span class="required">*</span>
    {!! Form::text('city_id1', '', ['required' => 'required' , 'class' => 'form-control', 'id' => 'city_id1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('phone_office1', 'Phone office') !!}
    {!! Form::text('phone_office1', '', ['class' => 'form-control', 'id' => 'phone_office1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('phone_rec1', 'Phone REC') !!}
    {!! Form::text('phone_rec1', '', ['class' => 'form-control', 'id' => 'phone_rec1']) !!}
</div></div>
</div>
 <div class="row">
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('phone_mobile1', 'Mobile') !!} <span class="required">*</span>
    {!! Form::text('phone_mobile1', '', ['required' => 'required' , 'class' => 'form-control', 'id' => 'phone_mobile1']) !!}
</div></div>
<div class="col-md-3">
<div class="form-group">
    {!! Form::label('email1', 'Email') !!} <span class="required">*</span>
    {!! Form::text('email1', '', ['required' => 'required' , 'class' => 'form-control', 'id' => 'email1']) !!}
</div></div>

<div class="col-md-3">
<div class="form-group">
    {!! Form::label('transfer_date', 'Transfer Date') !!} <span class="required">*</span>
    {!! Form::text('transfer_date', '', ['required' => 'required' , 'class' => 'form-control datepicker', 'id' => 'transfer_date', 'autocomplete' => 'off']) !!}
</div></div>
 <div class="col-md-3">
<div class="form-group">
    {!! Form::label('remarks', 'Remarks') !!}<span class="required">*</span>
    {!! Form::text('remarks', '', ['required' => 'required' ,'class' => 'form-control', 'id' => 'remarks']) !!}
</div></div>

<div class="col-md-3">
<div class="form-group">
    {!! Form::label('nominee_name', 'Nominee Name') !!} <span class="required">*</span>
     {!! Form::text('nominee_name', null , array( 'required' => 'required' ,'class'=>'form-control') )!!}
</div></div>


<div class="col-md-3">
<div class="form-group">
    {!! Form::label('nominee_guardian', 'Nominee Guardian') !!} <span class="required">*</span>
    {!! Form::text('nominee_guardian', null , array('required' => 'required' ,'class'=>'form-control') )!!}
</div></div>

<div class="col-md-3">
<div class="form-group">
    {!! Form::label('nominee_cnic', 'Nominee CNIC') !!} <span class="required">*</span>
    {!! Form::text('nominee_cnic', null , array('required' => 'required' ,'class'=>'form-control') )!!}
</div></div>

<div class="col-md-3">
<div class="form-group">
    {!! Form::label('relation_with_member', 'Nominee Relation With Member')!!} <span class="required">*</span>
    {!! Form::text('relation_with_member', null , array('required' => 'required' ,'class'=>'form-control', 'id' => 'relation_with_member') )!!}
</div></div>
</div>
            