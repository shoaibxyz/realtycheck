@extends('admin.layouts.template')
@section('title','Transfer')

@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer_app-active','active')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Transfer Application</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Transfer Application</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Transfer Procedure Init</h2>
                <div class="clearfix"></div>
            </div> 
            <br>
            @include('errors')
            {!! Form::open(['route' => 'transfer.postInit', 'class' => 'form-horizontal' , 'id' => 'form1']) !!}
            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Seller Information</h2>
            <div class="row" style="margin:0;">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('registration_no', 'Registration Number') !!}
                        {!! Form::text('registration_no', '', [ 'class' => 'form-control', 'id' => 'registration_no']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('person_id', 'Person ID') !!}
                        {!! Form::text('person_id', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'person_id']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('member_id', 'Member ID') !!}
                        {!! Form::text('member_id', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'member_id']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'name']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('guardian_type', 'Guardian Type') !!}
                        {!! Form::text('guardian_type', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_type']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('guardian_name', 'Guardian Name') !!}
                        {!! Form::text('guardian_name', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_name']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('nationality', 'Nationality') !!}
                        {!! Form::text('nationality', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'nationality']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('cnic', 'CNIC') !!}
                        {!! Form::text('cnic', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'cnic']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('address1', 'Current Address') !!}
                        {!! Form::text('address1', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address1']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('address2', 'Permanenet Address') !!}
                        {!! Form::text('address2', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address2']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::text('country_id', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'country_id']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('city_id', 'City') !!}
                        {!! Form::text('city_id', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'city_id']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('phone_office', 'Phone office') !!}
                        {!! Form::text('phone_office', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_office']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('phone_rec', 'Phone REC') !!}
                        {!! Form::text('phone_rec', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_rec']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('phone_mobile', 'Mobile') !!}
                        {!! Form::text('phone_mobile', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_mobile']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'email']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('plot_size', 'Plot Size') !!}
                        {!! Form::text('plot_size', '', [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'plot_size']) !!}
                    </div>
                </div>
            </div>
            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Buyer Information</h2>
           
                @include('admin.transfers._tform')
            
            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Transfer Required Documents</h2>
            <div class="row" style="margin:0;">
                <div class="col-sm-12" style="padding-left:30px;">
                    @foreach($docs as $key => $doc)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="transfer_docs[]" value="{{ $key }}">
                                {{ $doc }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row" style="margin:0;">
               <div class="col-md-12" style="margin-top: 25px;">
                    <div class="form-group">
                        {!! Form::hidden('ready',null,['id' => 'ready']) !!}
                        {!! Form::submit("Submit Transfer Application", ['class' => 'btn btn-success pull-right', 'id' => 'submit' ,'data-confirm' => 'Have you double checked data? ']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @endsection

@section('javascript')
    @section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });
    

    $('#registration_no').blur(function(){
            var reg_no = $(this).val();             
            $.ajax({
               url: '{{ URL::to('/findname') }}',
                type: 'POST',
                data: {'registration_no': reg_no},
                success: function(data){

                    if(data.error)
                    {
                        swal("Error!", "This registration number not found",'error');
                        return false;
                    }
                    $('#ready').val(1);
                    $('#person_id').val(data.person.id);
                    $('#name').val(data.person.name);
                    $('#guardian_type').val(data.person.guardian_type);
                    $('#guardian_name').val(data.person.gurdian_name);
                    $('#nationality').val(data.person.nationality);
                    $('#cnic').val(data.person.cnic);
                    $('#address1').val(data.person.current_address);
                    $('#address2').val(data.person.permanent_address);
                    $('#country_id').val(data.person.country_id);
                    $('#city_id').val(data.person.city_id);
                    $('#phone_office').val(data.person.phone_office);
                    $('#phone_rec').val(data.person.phone_rec);
                    $('#phone_mobile').val(data.person.phone_mobile);
                    $('#email').val(data.person.email);
                    $('#member_id').val(data.member.id);
                    $('#booking_price').val(data.payment.payment_amount);
                    {{-- $('#booking_date').val("{{ date('d-m-Y', strtotime("+data.payment.payment_date+"))}}"); --}}
                    $('#reg_no1').val(data.member.registration_no);
                    $('#plot_size').val(data.plot.plot_size);
                    
                },



                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });


        });


    $('#submit').on('click', function (e) {
        if (confirm($(this).data('confirm'))) {
            return true;
        }
        else {
            return false;
        }
    });
    </script>
    @endsection
</link>