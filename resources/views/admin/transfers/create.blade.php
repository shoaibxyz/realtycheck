@extends('admin.layouts.template')
@section('title','Transfer')

@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer_sheet-active','active')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
<style type="text/css">
    .cross{
        position: absolute;
        top: 15px;
        right: 25px;
        font-size: 16px;
        color: white;
    }
</style>
    @endsection

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Transfer Documents</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Transfer Documents</li>
        </ol>
    </div>
    
    {!! Form::open(['route' => 'transfer.store', 'class' => 'form-horizontal' , 'id' => 'form1', 'files' => true]) !!}
    <div class="x_panel">
        <div class="x_title">
            <h2>Transfer Documents</h2>
            <div class="clearfix"></div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('transfer_no', 'Transfer Number') !!}  <span class="required">*</span>
                    {!! Form::text('transfer_no', '', [ 'class' => 'form-control', 'id' => 'transfer_no', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Attachments <small style="color:#fff">(Attach multiple documents)</small></h2>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('docuemnts', 'Documents') !!}
                    {!! Form::file('documents[]', array('class'=>'form-control' , 'id' => 'name', 'multiple' => 'multiple'))!!}
                </div>
            </div>
        </div>
        
        <!--<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Capture Images</h2>
        <div class="row">
                <div class="col-md-4">
                    <div id="my_camera"></div>
                    <div id="pre_take_buttons">
                        <input type=button value="Take Snapshot" class="btn btn-md" onClick="preview_snapshot()">
                    </div>
                    <div id="post_take_buttons" style="display:none">
                        <input type=button class="btn btn-md" value="&lt; Take Another" onClick="cancel_preview()">
                        <input type=button class="btn btn-md" value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div id="results"></div>
                </div>    
        </div>-->
        
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">NDC Members Photos</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>NDC Seller Image</label>  <span class="required">*</span>
                    <input type="file" name="seller_photo" id="seller_photo" class="form-control" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>NDC Buyer Image</label>  <span class="required">*</span>
                    <input type="file" name="buyer_photo" id="buyer_photo" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="form-group">      
            <div id="inputs"></div>           
            {!! Form::submit("Submit Transfer Documents", ['class' => 'btn btn-success pull-right', 'id' => 'submit' ,'data-confirm' => 'Have you double checked data? ']) !!}
        </div>
    </div>
     {!! Form::close() !!}
            
    @endsection

@section('javascript')
    @section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ URL::to('public/js/webcamjs/webcam.min.js') }}"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });



        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 100
        });
        //Webcam.attach( '#my_camera' );


        function preview_snapshot() {
            // freeze camera so user can preview pic
            Webcam.freeze();
            // swap button sets
            document.getElementById('pre_take_buttons').style.display = 'none';
            document.getElementById('post_take_buttons').style.display = '';
        }
        
        function cancel_preview() {
            // cancel preview freeze and return to live camera feed
            Webcam.unfreeze();
            
            // swap buttons back
            document.getElementById('pre_take_buttons').style.display = '';
            document.getElementById('post_take_buttons').style.display = 'none';
        }
        
        var i = 0;
        function save_photo() {
            // actually snap photo (from preview freeze) and display it
            Webcam.snap( function(data_uri) {
                var img = new Image();
                img.src = data_uri;
                
                var div = document.createElement('div');
                div.className = "col-sm-3"
                div.className += " image_"+i;
                div.innerHTML = "<img src='"+img.src+"' class='img-thumbnail' id='image_"+i+"'><a href='#'><span class='glyphicon glyphicon-remove cross' id='"+i+"' aria-hidden='true'></span></a>";

                document.getElementById('results').appendChild(div);


                var inputs = $('#inputs');
                inputs.append('<input type="hidden" name="picture[]" value="'+img.src+'" id="capturedImage_'+i+'">');

                document.getElementById('pre_take_buttons').style.display = '';
                document.getElementById('post_take_buttons').style.display = 'none';
                i++;
            } );
        }

        $(document).on('click', '.cross', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $('.image_'+id).remove();
            $('#capturedImage_'+id).remove();
        });







    $('#registration_no').blur(function(){
            var reg_no = $(this).val();             
            $.ajax({
               url: '{{ URL::to('/findname') }}',
                type: 'POST',
                data: {'registration_no': reg_no},
                success: function(data){
                    if(data.error)
                    {
                        swal('error','This is invalid registration number','error');
                    }
                    // console.log(data);
                    $('#person_id').val(data.person.id);
                    $('#name').val(data.person.name);
                    $('#guardian_type').val(data.person.guardian_type);
                    $('#guardian_name').val(data.person.gurdian_name);
                    $('#nationality').val(data.person.nationality);
                    $('#cnic').val(data.person.cnic);
                    $('#address1').val(data.person.current_address);
                    $('#address2').val(data.person.permanent_address);
                    $('#country_id').val(data.person.country_id);
                    $('#city_id').val(data.person.city_id);
                    $('#phone_office').val(data.person.phone_office);
                    
                    $('#phone_rec').val(data.person.phone_rec);
                    $('#phone_mobile').val(data.person.phone_mobile);
                    $('#email').val(data.person.email);
                    $('#member_id').val(data.member.id);
                    $('#booking_price').val(data.payment.payment_amount);
                    $('#booking_date').val(data.payment.payment_date);
                    $('#reg_no1').val(data.member.registration_no);
                    $('#plot_size').val(data.plot.plot_size);
                    
                },



                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });


        });


/*$('#submit').on('click', function (e) {
        if (confirm($(this).data('confirm'))) {
            return true;
        }
        else {
            return false;
        }
    });*/
    </script>
    @endsection
</link>