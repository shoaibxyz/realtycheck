@extends('admin.layouts.template')
@section('title','Transfer')

@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer-active','active')
@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Transfers List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Transfers List</li>
        </ol>
    </div>

    <div class="content">
        
        <div class="x_panel">
            <div class="x_title">
                <h2>All Transfers</h2>
                <div class="actions" style="float: right; display: inline-block;">
                    <a href="{{ route('transfer.init')}}" class="btn btn-primary"  style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Transfer</a>
                </div>
                <div class="clearfix"></div>
            </div> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                        <h4 class="box-heading">Success</h4>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
            <div class="table-responsive">  
                <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th>Transfer No</th>
                            <th>Registration No</th>
                            <th>Old Member</th>
                            <th>New Member</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Is NDC Download</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($transfers as $transfer)
                            <tr>
                                <td><a href="{{ route('transfer.ndc', $transfer->transfer_no) }}">{{ $transfer->transfer_no }}</a></td>
                                <td>{{ $transfer->reg_no }}</td>
                                <td>{{ $transfer->currentUser->user->name }} <span class="label label-info">{{ $transfer->current_user_id }}</span>
                                </td>
                                <td>{{ $transfer->newUser->user->name }} <span class="label label-info">{{ $transfer->new_user_id }}</span>
                                    
                                </td>
                                <td>{{ date('d-m-Y', strtotime($transfer->transfer_date)) }}</td>
                                <?php $status = ($transfer->status == 1) ? "btn-success" : "btn-danger"; ?>
                                <?php $statusText = ($transfer->status == 1) ? "True" : "False"; ?>
                                
                                
                                <td><span class="label label-xs {{ $status }}">{{ $statusText }}</span></td>
                                
                                <?php $is_ndc_dl = ($transfer->is_ndc_dl == 1) ? "btn-primary" : "btn-danger"; ?>
                                <?php $is_ndc_dlText = ($transfer->is_ndc_dl == 1) ? "Yes" : "No"; ?>
                                <td><span class="label label-xs {{ $is_ndc_dl }}">{{ $is_ndc_dlText }}</span></td>
                                <td>
                                   <a href="{{ route('transfer.show', $transfer->id) }}" class="btn btn-xs btn-default" style="{{ $rights->show_view }}">View</a>
                                   <!--<a href="{{ route('transfer.edit', $transfer->id) }}" class="btn btn-xs btn-default" style="{{ $rights->show_edit }}">Edit</a>-->
                                   <a href="{{ route('transfer.ack', $transfer->id) }}" class="btn btn-xs btn-primary" target="_blank" style="{{ $rights->show_print }}">Print Ack</a>
                                   <a href="{{ route('transfer.sellerdetail', $transfer->id) }}" class="btn btn-xs btn-primary" target="_blank">Seller Detail Form</a>
                                   <a href="{{ route('transfer.affidavit', $transfer->id) }}" class="btn btn-xs btn-primary" target="_blank" style="{{ $rights->show_print }}">Affidavit</a>
                                    <form method="post" style="display: inline;" action="{{ route('transfer.transfer-sheet')}}">{{ csrf_field()}}
                                        <input type="hidden" name="transfer_id" value="{{ $transfer->id }}">
                                        {{-- <button class="btn btn-xs" type="submit"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></button> --}}
                                        <input type='submit' class='btn btn-success btn-xs' value='Download TS' style="{{ $rights->show_print }}">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>    
            </div>
        </div>
    </div>
@endsection

@section('javascript')
     <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/jquery.timer.js"></script>
     <script src="{{ URL::to('/')}}/Packages/Rizwan/flexcode/src/assets/js/custom.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });  
        }); 
    </script>
@endsection