<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Transfer Sheet</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/css/print.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}

</style>

       
    </head>

<body>
    <div id="app">
       <div class="container">
                <div class="leadger">
                 <div id="logo_header">
          <div class="row">
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/logo12-5.png"  style="width:240px;margin-top:10px;"></div>
            <div class="col-xs-2"></div>
            <div class="col-xs-3">
              <h2 align="center" style="margin-top:32px;font-size:28px;">Transfer Sheet</h2>
              <h4 align="center">Transfer Number: {{ $transfer->transfer_no }}</h4>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/Realty-check.png" style="height:70px;float:right;margin-top:10px;margin-right:10px;"></div>
          </div></div>

           <div class="main">
             <div class="plotinfo" style="border-bottom: 1px solid #ddd;border-spacing: 5px 5px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>File/Plot Information</u></h3></div>
                  
                  <div class="col-xs-3">
                    <h5><strong>Size: </strong>{{ $booking->paymentPlan->plotSize->plot_size }}</h5>
                    <h5> <strong>File/Plot No:</strong> {{ $member->reference_no }}</h5>
                    <h5><strong>Type:</strong> {{ $booking->paymentPlan->plot_nature}}</h5>
                    
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-3">
                    <h5> <strong>Booking Date:</strong>{{ $booking->created_at->format('d-m-Y') }}</h5>
                    <h5><strong>Payment Code: </strong>{{ $booking->paymentPlan->payment_desc }}</h5>
                    <h5> <strong>Street#: </strong></h5>
                    
                  </div>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-3" id="right-col" >
                    <h5><strong>Block Name:</strong></h5>
                    <h5> <strong>Dimension:</strong></h5>
                  </div>
                </div></div>


   <div class="memberinfo" style="border-bottom: 1px solid #ddd;border-spacing: 5px 5px;">

              
              {{-- memberinfo --}}
              <div class="row">
                <div class="col-xs-12"><h3><u>Seller Imformation</u></h3></div>
                <div class="col-xs-3">
                  <h5><strong>Name of Applicant: </strong>&nbsp; {{ $member->user->name }}</h5>
                  
                <h5> <strong>CNIC No:</strong> &nbsp; {{ $member->user->cnic }}</h5>

                </div>
               
                <div class="col-xs-2"></div>
                  <div class="col-xs-5">
                  <h5> <strong>S/O,D/O,W/O:</strong> &nbsp; {{ $member->user->guardian_type }}.{{ $member->user->gurdian_name }}</h5>
                
                  <h5> <strong>Mobile No:</strong> &nbsp; {{ $member->user->phone_mobile }}</h5>
                  </div>

                  
                 {{-- <div class="col-xs-2" id="right-col" style="width:3.5cm;height:4.5cm;">
                  
                  <img src="{{ asset('public/')}}/images/{{ $member->user->picture }}" alt="" style="width:3.5cm;height:4.5cm;">
                </div> --}}
               
                
              </div></div>

              <div class="memberinfo" style="border-bottom: 1px solid #ddd;border-spacing: 5px 5px;">

              
              {{-- memberinfo --}}
              <div class="row">
                <div class="col-xs-12"><h3><u>Buyer Information</u></h3></div>
                <div class="col-xs-5">
                  <h5><strong>Name of Applicant: </strong>&nbsp; {{ $newUser->name }}</h5>
                  
                <h5> <strong>CNIC No:</strong>&nbsp; {{ $newUser->cnic }}</h5>

                </div>
               
                
                  <div class="col-xs-4">
                  <h5> <strong>S/O,D/O,W/O:</strong>&nbsp; {{ $newUser->gurdian_name }}</h5>
                
                  <h5> <strong>Mobile No:</strong>&nbsp;{{ $newUser->phone_mobile }}</h5>
                  </div>

               
                
              </div></div>


         <div class="memberinfo">

              
             <div class="row">
                <div class="col-xs-12"><h4><u><em>For Office Use Only</em></u></h4></div>
               
                
                 
                  
                 
               
                
              </div></div>      

              <div class="col-xs-12" style="margin-top: 80px;">
                                     <div class="col-xs-4" style="text-align:center;">
                                        <p style="border-top:1px solid #ddd;padding-top:10px;">Signature</p>
                                    </div>

                                    <div class="col-xs-4" style="text-align:center;">
                                        <p style="border-top:1px solid #ddd;padding-top:10px;">Stamp</p>
                                    </div>

                                    <div class="col-xs-4" style="text-align:center;">
                                        <p style="border-top:1px solid #ddd;padding-top:10px;">Date</p>
                                    </div> </div>
                  
               
            </div>


            </div>


           


           
           


          </div></div>
       

       
        

    </body>

</html>
