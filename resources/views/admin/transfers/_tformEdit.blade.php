         <div class="x_panel">
                 <div class="x_title">
            <h2>Personal Detail</h2>
            
            <div class="clearfix"></div>
          </div>  
                 
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('reg_no1', 'Registration No') !!}
                {!! Form::text('reg_no1', $currOwner->registration_no , array('required' => 'required' , 'class'=>'form-control' , 'id' => 'reg_no1', 'readonly' => 'readonly'))!!}
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name1', $newUser->name , array('required' => 'required' , 'class'=>'form-control' , 'id' => 'name'))!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('username', 'Username')!!}
            {!! Form::text('username', $newUser->username , array('class'=>'form-control' , 'id' => 'username')  )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('email', 'Email')!!}
            {!! Form::text('email1', $newUser->email , array('class'=>'form-control' , 'id' => 'email')  )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('picture', 'Picture') !!}
            {!! Form::file('picture',  ['class' => 'form-control' , 'id' => 'picture']) !!}
            </div>
        </div>

<div class="col-md-4">
    <div class="form-group">
    <label for="cnic_pic">CNIC Pic</label>
        {!! Form::file('cnic_pic', ['class' => 'form-control' , 'id' => 'cnic_pic']) !!}                                
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
    <label for="ref_pk">Referencce In Pakistan</label>
        {!! Form::text('ref_pk1', $newUser->ref_pk, ['id'=>'imgFile', 'class' => 'form-control' , 'id' => 'ref_pk']) !!}
                                
    </div>
</div>

<div class="col-md-4">
            <div class="form-group">
            {!! Form::label('permanent_address', 'Permanent Address')!!}
            {!! Form::text('address21', $newUser->permanent_address , array( 'class'=>'form-control') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('current_address', 'Current Address')!!}
            {!! Form::text('current_address1', $newUser->current_address , array( 'class'=>'form-control') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('guardian_type', 'S/O or D/O OR W/O')!!}
            <select name="guardian_type1" class="form-control">
                <option value="" disabled="">Select One</option>
        
            <?php 
                $guardian_typeArr[''] = "";
                $guardian_typeArr['S/O'] = 'S/O';
                $guardian_typeArr['D/O'] = 'D/O';
                $guardian_typeArr['W/O'] = 'W/O';

                foreach ($guardian_typeArr as $arr) {
                    $selected = ($arr == $newOwner->guardian_type) ? "selected" : "";
                    echo "<option value='".$arr."' ".$selected.">".$arr."</option>";
                }
            ?>
                 </select>
            {{-- {!! Form::select('guardian_type1', $guardian_typeArr, $newOwner->guardian_type, ['class' => 'form-control']) !!} --}}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('guardian_name', 'Guardian Name')!!}
            {!! Form::text('guardian_name1', $newUser->gurdian_name , array( 'class'=>'form-control' , 'id' => 'guardian_name') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('nationality', 'Nationality')!!}
            {!! Form::text('nationality', $newUser->nationality , array( 'class'=>'form-control' , 'id' => 'nationality') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('country_id', 'Country')!!}
            {!! Form::text('country_id1', $newUser->country_id , array( 'class'=>'form-control c_field') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('city_id', 'City')!!}
            {!! Form::text('city_id1', $newUser->city_id , array( 'class'=>'form-control c_field') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('cnic', 'CNIC')!!}
            {!! Form::text('cnic1', $newUser->cnic , array('class'=>'form-control' , 'id' => 'cnic') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('passport', 'Passport No')!!}
            {!! Form::text('passport1', $newUser->passport , array( 'class'=>'form-control passport') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('phone_office', 'Phone #')!!}
            {!! Form::text('phone_office1', $newUser->phone_office , array( 'class'=>'form-control' , 'id' => 'phone_office') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('phone_rec', 'Phone Res. #')!!}
            {!! Form::text('phone_rec1', $newUser->phone_res , array( 'class'=>'form-control' , 'id' => 'phone_rec') )!!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('phone_mobile', 'Mobile #')!!}
            {!! Form::text('phone_mobile1', $newUser->phone_mobile , array( 'class'=>'form-control' , 'id' => 'phone_mobile') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('mobile1', 'Mobile1 #')!!}
            {!! Form::text('mobile1', $newUser->mobile2 , array( 'class'=>'form-control' , 'id' => 'mobile1') )!!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('dob', 'Date of Birth')!!}
            {!! Form::text('dob1', date('d/m/Y',strtotime($newUser->dateofbirth)),  array( 'class'=>'form-control datepicker') )!!}
            </div>
        </div>
        <div class="row" style="margin-left:1px;"><div class="col-md-12"><h5 style="color:red;">*Note:In case of alter in permanent address in CNIC, applicant will have to provide the “Affidavit Form”. </h5></div></div>
        <div class="row" style="margin-left:1px;"><div class="col-md-4">
            <div class="form-group">
                    {!! Form::label('affidavit_address', 'Affidavit Form') !!}
                    {!! Form::file('affidavit_address',  ['class' => 'form-control' , 'id' => 'affidavit_address']) !!}
                </div>
               </div></div>
               </div>


            
<div class="x_panel">

        <div class="x_title">
            <h2>Nominee Detail</h2>
            
            <div class="clearfix"></div>
          </div>   
        
        

        
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_name', 'Nominee Name')!!}
        {!! Form::text('nominee_name', $newOwner->nominee_name , array( 'class'=>'form-control c_field' , 'id' => 'nominee_name') )!!}
        </div></div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_guardian', 'Nominee Guardian')!!}
        {!! Form::text('nominee_guardian', $newOwner->nominee_guardian , array('class'=>'form-control' , 'id' => 'nominee_guardian') )!!}
        </div>
        </div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_cnic', 'Nominee CNIC')!!}
        {!! Form::text('nominee_cnic', $newOwner->nominee_cnic , array('class'=>'form-control' , 'id' => 'nominee_cnic') )!!}
        </div></div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('relation_with_member', 'Relation With Member')!!}
        {!! Form::text('relation_with_member', $newOwner->relation_with_member , array( 'class'=>'form-control c_field' , 'id' => 'relation_with_member') )!!}
        </div></div>


            <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('transfer_date', 'Transfer Date') !!}
                {!! Form::text('transfer_date', $transfer->transfer_date, ['required' => 'required' , 'class' => 'form-control datepicker', 'id' => 'transfer_date']) !!}
            </div></div>
            <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('transfer_charges', 'Transfer Charges') !!}
                {!! Form::text('transfer_charges', $transfer->tranfer_charges, ['required' => 'required' , 'class' => 'form-control', 'id' => 'transfer_charges']) !!}
            </div></div>
            <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('transfer_by', 'Transfer By') !!}
                {!! Form::text('transfer_by', $transfer->tranfer_by, ['required' => 'required' , 'class' => 'form-control', 'id' => 'transfer_by']) !!}
            </div></div>
             <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('remarks', 'Remarks') !!}
                {!! Form::text('remarks', $transfer->remarks, ['required' => 'required' , 'class' => 'form-control', 'id' => 'remarks']) !!}
            </div></div>
