<!DOCTYPE html >
<html>
<head>
  <title>NDC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <style type="text/css">
   
   /*@media print {
     a[href]:after {
       content: none !important;
     }
   }*/

  

   
 </style>
  
</head>
<body>
    
   
 <div id="logo_header" style="display:block;">

 <div class="container" style="width: 860px;padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;">
            <div class="row" style="margin-right: -10px;
    margin-left: -10px;">
              <div class="col-xs-6" style="width: 50%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px;"><img src="{{ asset('public/') }}/images/capital.jpg"  style="width:100px;"></div>
              
              
              
              <div class="col-xs-6" style="width: 50%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px;">

      <h5 style="border-bottom:1px solid;margin-right: 200px;color:#231f20;font-size:16px;">TNDC/BATCH1/{{ $transfer->transfer_no}}</h5>
      <h5 style="border-bottom:1px solid;margin-right: 200px;color:#231f20;font-size:16px;">Reg No: {{ $currMember->registration_no }}</h5>
</div>


           
            </div></div></div>

            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            
            
      <div class="main" style="display:block;">

      <div class="container" style="width: 960px;padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;">

          <div class="row" style="margin-right: -10px;
    margin-left: -10px;width:70%;">
              <div class="col-xs-12" style="width: 100%;float:left;text-align: center; color:#231f20;"><img src="{{ asset('public/') }}/img/ddd-02.jpg"  style="width:500px;"></div></div>

            <br>
            <br>
            <br>
            <br>
           <br>
           <br>
            
    <div class="container" style="margin-right: -10px;
    margin-left: -10px;width:75%;">
        
        <table style="width:100%;border-spacing: 0px;border-bottom: 1px solid black;">
            
        <tr><td class="box3_t" style="text-align: left;padding: 10px 5px;font-size:20px;font-weight:bold;"><u><em>Seller Detail</em></u></td></tr>
        <tr>
            <td class="box3_b" style="color:#231f20;padding: 5px;width:60%;">
                <span style="font-weight:bold;">Name: </span><span>{{ $currUser->name }}</span>
                <br>
                <span style="font-weight:bold;">{{ $currUser->guardian_type }}: </span><span>{{ $currUser->gurdian_name }}</span>
                <br>
                <span style="font-weight:bold;">CNIC: </span><span>{{ $currUser->cnic }}</span>
                <br>
                <span style="font-weight:bold;">Address: </span><span>{{ $currUser->current_address }}</span>
              
                </td>
                
               
               
                <td class="box3_b" style="width:10%;"></td>
                <td class="box3_b" style="padding: 5px;text-align: right;width:10%;text-align:right;">
                    
                     @if($cur_thumb)
                     
                    <img src="{{ asset('public/') }}/images/thumbs/{{ $currMember->id . '_' . str_replace(' ','-',$currUser->name) }}/{{ $cur_thumb->filename }}" style="width:2.3cm;height:2.5cm;">
                    @endif
                </td>
            <td class="box3_b" style="color:#231f20;padding: 5px;text-align: right;width:20%">
                
                
                
                <img src="{{ asset('public/') }}/images/ndc/{{ $currMember->id}}/{{ $transfer->current_user_pic}}" style="border:1px solid;width:2.5cm;height:3.3cm;">
               
                
                </td>
        </tr>
            
            </table>
            
            <br>
        
        <table style="width:100%;border-spacing: 0px;border-bottom: 1px solid black;">
            
        <tr><td class="box3_t" style="text-align: left;padding: 10px 5px;font-size:20px;font-weight:bold;"><u><em>Buyer Detail</em></u></td></tr>
        <tr>
            <td class="box3_b" style="color:#231f20;padding: 5px;width:60%;">
                <span style="font-weight:bold;">Name: </span><span>{{ $newUser->name }}</span>
                <br>
                <span style="font-weight:bold;">{{ $newUser->guardian_type }}: </span><span>{{ $newUser->gurdian_name }}</span>
                <br>
                <span style="font-weight:bold;">CNIC: </span><span>{{ $newUser->cnic }}</span>
                <br>
                <span style="font-weight:bold;">Address: </span><span>{{ $newUser->current_address }}</span>
                
                </td>
                
                <td class="box3_b" style="width:10%;"></td>
                
                <td class="box3_b" style="color:#231f20;padding: 5px;text-align: right;width:10%;text-align:right;">
               
            @if($new_thumb)
                 <img src="{{ asset('public/') }}/images/thumbs/{{ $newMember->id . '_' . str_replace('/','',str_replace(' ','-',$newUser->name)) }}/{{ $new_thumb->filename}}" style="width:2.3cm;height:2.5cm;">
                 @endif
                </td>
            <td class="box3_b" style="color:#231f20;padding: 5px;text-align: right;width:20%">
               
                <img src="{{ asset('public/') }}/images/ndc/{{ $newMember->id }}/{{ $transfer->new_user_pic }}" style="border:1px solid;width:2.5cm;height:3.3cm;">

                </td>
        </tr>
            
            </table>
            
            <br>
            
        
        <table style="width:100%;border-spacing: 0px;border-bottom: 1px solid black;">
            
        <tr><td class="box3_t" style="text-align: left;padding: 10px 5px;font-size:20px;font-weight:bold;"><u><em>Property Detail</em></u></td></tr>
        <tr>
            <td class="box3_b" style="color:#231f20;padding: 5px;width:100%;padding-bottom:10px;">
                <span style="font-weight:bold;">Size: </span><span>{{ $booking->paymentPlan->plotSize->plot_size }}</span>
                <br>
                <span style="font-weight:bold;">Plot No/File: </span><span></span>
                <br>
                <span style="font-weight:bold;">Type: </span><span>{{ $booking->paymentPlan->plot_nature}}</span>
                
                </td>
             <td class="box3_b" style="color:#231f20;padding: 5px;width:100%;">
                <span style="font-weight:bold;">Booking Date: </span><span>{{ $booking->created_at->format('d-m-Y') }}</span>
                <br>
                <span style="font-weight:bold;">Payment Code: </span><span>{{ $booking->paymentPlan->payment_desc }}</span>
                <br>
                <span style="font-weight:bold;">Transfer Date: </span><span>{{ $transfer->ndc_dl_date->format('d-m-y')}}</span>
                
                </td>
            
        </tr>
            
            </table>

</div>

            <br>
            

<div class="row" style="margin-right: -10px;
    margin-left: -10px;width:75%;"><div class="col-xs-12" style="width: 100%;float:left;">
    <p style="color:#231f20;text-align: justify;"><em>This certificate is Transferable No Demand Certificate provided at the request of seller. Buyer must appear with this certificate at the time of transfer; Allottee of this certificate is an absolute owner of the registration provided by {{ env('SOCIETY_NAME') }}. Allottee of this certificate shall abide and follow all the current and future policies provided by the management of {{ env('SOCIETY_NAME') }}.</em></p></div></div>    



    <br>
            <br>
            <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        
            

<table style="width:80%;border-spacing: 0px;">
    
     <tr>
            <td class="box3_b" style="color:#231f20;padding:0 2px;width:20px;">
                <p>__________________</p>
                <p style="font-weight:bold;">Manager T&R</p>
                </td>
             <td class="box3_b" style="color:#231f20;padding:0 2px;width:20px;">
                <p>__________________</p>
                <p style="font-weight:bold;">General Manager</p>
                
                </td>
                
            <td class="box3_b" style="color:#231f20;padding:0 2px;width:20px;">
                <p>__________________</p>
                <p style="font-weight:bold;">Management</p>
                </td>
            
        </tr>
        
        
        <tr>
            <td class="box3_b" style="color:#231f20;padding:0 2px;width:20px;">
                <p>UAN : <span style="font-size:14px;">042-111-761-762</span></p>
                </td>
             <td class="box3_b" style="color:#231f20;padding:0 2px;width:20px;">
                <p>Website : <span style="font-size:14px;">www.lahorecapitalcity.com</span></p>
                
                </td>
                
            
            
        </tr>
        
         
            </table>

        </div></div>
              </body>
              </html>