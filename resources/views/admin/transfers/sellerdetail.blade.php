<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Seller Detail Form</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}
    body{
        font-size: 22px 
    }
</style>

       
    </head>

<body>
    
    <div id="watermark" style="position: absolute;z-index:-1000;top:-30;">
            <img src="{{ asset('public/') }}/img/cc-logo.png" width="90%"/>
        </div>
    <div id="app">
        
    <div id="logo_header" style="display:block;margin-bottom:10px;margin-top:10px;">

 <div class="container">
            <div class="row">
              <div class="col-xs-6"><img src="{{ asset('public/') }}/images/capital.png"  style="width:100px;"></div>
              
              
              
              <div class="col-xs-6">
     
      <img src="{{ asset('public/') }}/images/pmc-web-logo.png"  style="width:60%;float:right;"></div>
</div>


           
            </div></div></div>
      
            <div class="container" style="margin: 20px auto">
                <div class="leadger">
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                           <table style="width:100%;border-spacing: 0px;">
                               <tr style="border:none;">
                                   <td width="40%" style="border:none;">
                                       <h4 style="border:1px solid;padding:10px;width:200px;">DEALER'S STAMP</h4>
                                   </td>
                                   <td width="60%" style="float:right;border:none;">
                                       <p >From</p>
                            <p>Mr/Mrs/Miss: <span style="border-bottom:1px solid;width:240px;text-align:center;display:inline_block;">{{ $new_user->name }}</span></p>
                            <p>{{ $new_user->guardian_type }}: <span style="border-bottom:1px solid;width:240px;text-align:center;display:inline_block;">{{ $new_user->gurdian_name }}</span></p>
                            <p>R/O: <span style="border-bottom:1px solid;width:240px;text-align:center;display:inline_block;">{{ $new_user->current_address }}</span></p>
                            <p>TNDC/BATCH1/ <span style="border-bottom:1px solid;width:240px;text-align:center;display:inline_block;">{{ $transfer->transfer_no }}</span></p>
                                   </td>
                               </tr>
                            </table>
                            
                        </div>
                        
                     </div>
                     <br>
                    
                     <div class="row">
                        
                    <div class="col-md-12">
                        
                        <div class="main-content">
                            <p>To :  <span>Manager Transfer & Record Capital City</span></p>
                            <p>Subject :  <span style="font-weight:bold;border-bottom:1px solid;">Request for TNDC</span></p>
                            <p>Capital City Registration No :  <span style="border-bottom:1px solid;">{{ $transfer->reg_no }}</span></p>
                        </div>
                        
                        <br>
                   
                        <div class="sbody">
                            <p>
                                I have decided to transfer above referred Registration number. 
                                I am bound to pay all dues including NDC fee against the Registration number. 
                                It is, therefore, requested that NDC may please be prepared.
                            </p>
                            
                        </div>
                        
                        <br>
                   
                        <div class="sfooter">
                            
                            <p  style="text-align:right;">Yours Sincerely</p>
                            <p  style="text-align:right;">____________________</p>
                            
                            <br>
                   
                            <p>Note: I have read the instructions overleaf and deposited the documents as required.</p>
                            
                        </div>
                        
                    </div>
                    </div>
                     <br>
                    <div class="row">
                    
                    <div class="col-md-12">
                        <div class="text-center">
                        <p><b><u>For Office Use Only</u></b></p>
                        </div>
                        
                       
                     <br>
                        
                        <table style="width:100%;border-spacing: 0px;border-bottom: 1px solid black;text-align:center;">
                            <tr>
                                <td width="30%" style="padding:5px;">Transfer & Record</td>
                                <td width="70%"></td>
                            </tr>
                            
                            <tr>
                                <td width="30%" style="padding:5px;">Accounts</td>
                                <td width="70%"></td>
                            </tr>
                            
                            <tr>
                                <td width="30%" style="padding:5px;">General Manager</td>
                                <td width="70%"></td>
                            </tr>
                            
                        </table>
                        
                    </div>
                    
                    </div>
                    
                    <br>
                   
                    
                    <div class="row">
                    
                    <div class="col-md-12">
                        <div style="height:2px;border-bottom:2px solid;"></div>
                    </div>
                    
                    </div>
                    
                  
                     <br>
                    
                    <div class="row">
                    
                    <div class="col-md-12">
                        <table style="width:100%;border-spacing: 0px;">
                            <tr style="border:none;">
                                <td width="50%" style="border:none;"><span>TNDC/C1/BATCH1/:</span> <span>TNDC/BATCH1/{{ $transfer->transfer_no}}</span></td>
                                <td width="50%" style="border:none;float:right;"><span>Date:</span> <span>{{$transfer->transfer_date->format('d-m-Y') }}</span></td>
                            </tr>
                            
                            <tr style="border:none;">
                                <td width="50%" style="border:none;"><span>Transferor Name:</span> <span>{{ \App\User::getUserInfo($transfer->tranfer_by)->name }}</span></td>
                                <td width="50%" style="border:none;float:right;"><span>NDC Type: </span> <span>TNDC</span></td>
                            </tr>
                            
                            <tr style="border:none;">
                                <td width="50%" style="border:none;"><span>CRO:</span> <span>_____________</span></td>
                                <td width="50%" style="border:none;padding-left: 10%;"><span>Letter Collection Date:</span> <span>____________</span></td>
                            </tr>
                        </table>
                    </div>
                    </div>
                    
                  
                </div>
                  
               
            </div>
        
         <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
            <script type="text/javascript">

                    window.print();


              </script>
       
        

    </body>

</html>
