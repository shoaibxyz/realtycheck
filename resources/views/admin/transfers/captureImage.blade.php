@extends('admin.layouts.template')
@section('title','Transfer')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
<style type="text/css">
    .cross{
        position: absolute;
        top: 15px;
        right: 25px;
        font-size: 16px;
        color: white;
    }
</style>
    @endsection

@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Transfer Procedure Capture Image</h2>
            
            <div class="clearfix"></div>
          </div>    
    {!! Form::open(['route' => 'transfer.saveImages', 'class' => 'form-horizontal' , 'id' => 'form1', 'files' => true]) !!}
    <div class="row">
        @include('errors')

        <div class="row">
            @foreach($TransferImages as $image)
            <div class="col-sm-3">
                <img src="{{ $image->picture }}" class="thumbnail">
                <p><b>Captured By:</b> {{ App\User::getUserInfo($image->created_by)->name }}</p>
                <p><b>Date:</b> {{ $image->created_at->format('d-m-Y H:i:s') }}</p>
            </div>   
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-4">

                <div id="my_camera"></div>

                <div id="pre_take_buttons">
                    <input type=button value="Take Snapshot" class="btn btn-md" onClick="preview_snapshot()">
                </div>
                <div id="post_take_buttons" style="display:none">
                    <input type=button class="btn btn-md" value="&lt; Take Another" onClick="cancel_preview()">
                    <input type=button class="btn btn-md" value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
                </div>
            </div>
                <div id="results"></div>
            </div>
{{-- 
        <div class="row">
            <div class="col-md-12">
                <h3>Image 2</h3>
            </div>
            <div class="col-md-6">
                <div id="my_camera2"></div>

                <div id="pre_take_buttons2">
                    <input type=button value="Take Snapshot" onClick="preview_snapshot2()">
                </div>
                <div id="post_take_buttons2" style="display:none">
                    <input type=button value="&lt; Take Another" onClick="cancel_preview2()">
                    <input type=button value="Save Photo &gt;" onClick="save_photo2()" style="font-weight:bold;">
                </div>
            </div>

            <div class="col-md-6">
                <div id="results2"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Image 3</h3>
            </div>
            <div class="col-md-6">
                <div id="my_camera3"></div>

                <div id="pre_take_buttons3">
                    <input type=button value="Take Snapshot" onClick="preview_snapshot3()">
                </div>
                <div id="post_take_buttons3" style="display:none">
                    <input type=button value="&lt; Take Another" onClick="cancel_preview3()">
                    <input type=button value="Save Photo &gt;" onClick="save_photo3()" style="font-weight:bold;">
                </div>
            </div>

            <div class="col-md-6">
                <div id="results3"></div>
            </div>
        </div> --}}
        <div id="inputs"></div>
        <div class="col-md-3" style="margin-top: 25px;">
            <div class="form-group">
                <input type="hidden" name="transfer_id" value="{{ $transfer_id }}">
                {!! Form::submit("Save Image", ['class' => 'btn btn-default pull-right', 'id' => 'submit' ,'data-confirm' => 'Have you double checked data? ']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
    <div class="clearfix">
    </div>
    @endsection

@section('javascript')
    @section('javascript')
    
    <script type="text/javascript" src="{{ URL::to('public/js/webcam.min.js') }}"></script>

    <script language="JavaScript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 100
        });
        Webcam.attach( '#my_camera' );


        function preview_snapshot() {
            // freeze camera so user can preview pic
            Webcam.freeze();
            // swap button sets
            document.getElementById('pre_take_buttons').style.display = 'none';
            document.getElementById('post_take_buttons').style.display = '';
        }
        
        function cancel_preview() {
            // cancel preview freeze and return to live camera feed
            Webcam.unfreeze();
            
            // swap buttons back
            document.getElementById('pre_take_buttons').style.display = '';
            document.getElementById('post_take_buttons').style.display = 'none';
        }
        
        var i = 0;
        function save_photo() {
            // actually snap photo (from preview freeze) and display it
            Webcam.snap( function(data_uri) {
                var img = new Image();
                img.src = data_uri;
                
                var div = document.createElement('div');
                div.className = "col-sm-3"
                div.className += " image_"+i;
                div.innerHTML = "<img src='"+img.src+"' class='img-thumbnail' id='image_"+i+"'><a href='#'><span class='glyphicon glyphicon-remove cross' id='"+i+"' aria-hidden='true'></span></a>";

                document.getElementById('results').appendChild(div);


                var inputs = $('#inputs');
                inputs.append('<input type="hidden" name="picture[]" value="'+img.src+'" id="capturedImage_'+i+'">');

                document.getElementById('pre_take_buttons').style.display = '';
                document.getElementById('post_take_buttons').style.display = 'none';
                i++;
            } );
        }

        $(document).on('click', '.cross', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $('.image_'+id).remove();
            $('#capturedImage_'+id).remove();
        });
    </script>

    @endsection
</link>