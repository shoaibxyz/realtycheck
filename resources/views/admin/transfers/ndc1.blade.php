@extends('admin.layouts.template')
@section('title','Transfer NDC')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Transfer No Demand Certificate</h2>
            
            <div class="clearfix"></div>
          </div>    
    {!! Form::open(['route' => 'transfer.downloadNdc', 'class' => 'form-horizontal' , 'id' => 'form1']) !!}
        @include('errors')
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('transfer_no', 'Transfer Number') !!}
                    {!! Form::text('transfer_no', $data['transfer']->id, [ 'class' => 'form-control', 'id' => 'transfer_no']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Seller information</h3>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', $data['currOwner']['user']->name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'name']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('guardian_type', 'Guardian Type') !!}
                    {!! Form::text('guardian_type', $data['currOwner']['user']->guardian_type, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_type']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('guardian_name', 'Guardian Name') !!}
                    {!! Form::text('guardian_name', $data['currOwner']['user']->guardian_name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_name']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('nationality', 'Nationality') !!}
                    {!! Form::text('nationality', $data['currOwner']['user']->nationality, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'nationality']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('cnic', 'CNIC') !!}
                    {!! Form::text('cnic', $data['currOwner']['user']->cnic, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'cnic']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('address1', 'Current Address') !!}
                    {!! Form::text('address1', $data['currOwner']['user']->current_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address1']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('address2', 'Permanenet Address') !!}
                    {!! Form::text('address2', $data['currOwner']['user']->permanent_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('country_id', 'Country') !!}
                    {!! Form::text('country_id', $data['currOwner']['user']->country_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'country_id']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('city_id', 'City') !!}
                    {!! Form::text('city_id', $data['currOwner']['user']->city_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'city_id']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_office', 'Phone office') !!}
                    {!! Form::text('phone_office', $data['currOwner']['user']->phone_office, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_office']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_rec', 'Phone REC') !!}
                    {!! Form::text('phone_rec', $data['currOwner']['user']->phone_res, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_rec']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_mobile', 'Mobile') !!}
                    {!! Form::text('phone_mobile', $data['currOwner']['user']->phone_mobile, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_mobile']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', $data['currOwner']['user']->email, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'email']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('plot_size', 'Plot Size') !!}
                    {!! Form::text('plot_size', $data['plot']->plot_size, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'plot_size']) !!}
                </div>
            </div>
        </div>
           

        <div class="row">
            <div class="col-md-12">
                <h3>Buyer information</h3>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name2', $data['newOwner']['user']->name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'name2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('guardian_type', 'Guardian Type') !!}
                    {!! Form::text('guardian_type2', $data['newOwner']['user']->guardian_type, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_type2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('guardian_name', 'Guardian Name') !!}
                    {!! Form::text('guardian_name2', $data['newOwner']['user']->guardian_name, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'guardian_name2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('nationality', 'Nationality') !!}
                    {!! Form::text('nationality2', $data['newOwner']['user']->nationality, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'nationality2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('cnic', 'CNIC') !!}
                    {!! Form::text('cnic2', $data['newOwner']['user']->cnic, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'cnic2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('address1', 'Current Address') !!}
                    {!! Form::text('address12', $data['newOwner']['user']->current_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address12']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('address2', 'Permanenet Address') !!}
                    {!! Form::text('address22', $data['newOwner']['user']->permanent_address, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'address22']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('country_id', 'Country') !!}
                    {!! Form::text('country_id2', $data['newOwner']['user']->country_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'country_id2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('city_id', 'City') !!}
                    {!! Form::text('city_id2', $data['newOwner']['user']->city_id, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'city_id2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_office', 'Phone office') !!}
                    {!! Form::text('phone_office2', $data['newOwner']['user']->phone_office, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_office2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_rec', 'Phone REC') !!}
                {!! Form::text('phone_rec2', $data['newOwner']['user']->phone_res, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_rec2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone_mobile', 'Mobile') !!}
                    {!! Form::text('phone_mobile2', $data['newOwner']['user']->phone_mobile, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'phone_mobile2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email2', $data['newOwner']['user']->email, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'email2']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('plot_size', 'Plot Size') !!}
                    {!! Form::text('plot_size2', $data['plot']->plot_size, [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'plot_size2']) !!}
                </div>
            </div>
           <div class="col-md-3" style="margin-top: 25px;">
                <div class="form-group">
                    {!! Form::hidden('ready',null,['id' => 'ready']) !!}
                    {!! Form::submit("Download NDC", ['class' => 'btn btn-default pull-right', 'id' => 'submit' ,'data-confirm' => 'Have you double checked data? ']) !!}
                </div></div>
        </div>
        {!! Form::close() !!}

    </div>
    <div class="clearfix">
    </div>
    @endsection

@section('javascript')
    @section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });
    

    $('#transfer_no').blur(function(){
            var transfer_no = $(this).val();             
            $.ajax({
               url: '{{ route('checkTransfer') }}',
                type: 'POST',
                data: {'transfer_no': transfer_no},
                success: function(result){

                    if(result.status == 201)
                    {
                        swal("Error!", "This transfer number not found",'error');
                        return false;
                    }
                    $('#ready').val(1);
                    $('#name').val(result.data.currOwner.user.name);
                    $('#guardian_type').val(result.data.currOwner.guardian_type);
                    $('#guardian_name').val(result.data.currOwner.gurdian_name);
                    $('#nationality').val(result.data.currOwner.user.nationality);
                    $('#cnic').val(result.data.currOwner.user.cnic);
                    $('#address1').val(result.data.currOwner.user.current_address);
                    $('#address2').val(result.data.currOwner.user.permanent_address);
                    $('#country_id').val(result.data.currOwner.user.country_id);
                    $('#city_id').val(result.data.currOwner.user.city_id);
                    $('#phone_office').val(result.data.currOwner.user.phone_office);
                    $('#phone_rec').val(result.data.currOwner.user.phone_rec);
                    $('#phone_mobile').val(result.data.currOwner.user.phone_mobile);
                    $('#email').val(result.data.currOwner.user.email);
                    $('#plot_size').val(result.data.plot.plot_size);
                    // $('#booking_price').val(result.data.payment.payment_amount);
                    {{-- $('#booking_date').val("{{ date('d-m-Y', strtotime("+result.data.payment.payment_date+"))}}"); --}}
                    // $('#transfer_no').val(result.data.currOwner.registration_no);
                    // $('#plot_size').val(result.data.plot.plot_size);


                     $('#name2').val(result.data.newOwner.user.name);
                    $('#guardian_type2').val(result.data.newOwner.guardian_type);
                    $('#guardian_name2').val(result.data.newOwner.gurdian_name);
                    $('#nationality2').val(result.data.newOwner.user.nationality);
                    $('#cnic2').val(result.data.newOwner.user.cnic);
                    $('#address12').val(result.data.newOwner.user.current_address);
                    $('#address22').val(result.data.newOwner.user.permanent_address);
                    $('#country_id2').val(result.data.newOwner.user.country_id);
                    $('#city_id2').val(result.data.newOwner.user.city_id);
                    $('#phone_office2').val(result.data.newOwner.user.phone_office);
                    $('#phone_rec2').val(result.data.newOwner.user.phone_rec);
                    $('#phone_mobile2').val(result.data.newOwner.user.phone_mobile);
                    $('#email2').val(result.data.newOwner.user.email);
                    $('#plot_size2').val(result.data.plot.plot_size);
                    // $('#booking_price2').val(result.data.payment.payment_amount);
                    {{-- $('#booking_date').val("{{ date('d-m-Y', strtotime("+data.payment.payment_date+"))}}"); --}}
                    
                },



                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });


        });


    // $('#submit').on('click', function (e) {
    //     if( $('#transfer_').val() == ""){
    //         swal("Error!", "Please Enter registration number",'error');
    //         return false;
    //     }
    //     if( $('#ready').val() != 1){
    //         swal("Error!", "Wrong Registration Number Added",'error');
    //         return false;
    //     }
    //     if (confirm($(this).data('confirm'))) {
    //         return true;
    //     }
    //     else {
    //         return false;
    //     }
    // });
    </script>
    @endsection
</link>