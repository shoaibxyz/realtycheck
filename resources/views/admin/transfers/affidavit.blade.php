<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Affidavit</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}
    body{
        font-size: 20px 
    }

    ul li {
        list-style-type:decimal;
    }
</style>

       
    </head>

<body>
    <div id="app">
        
            <div class="container">
                <div class="leadger">
                    <div class="text-center" style="margin-top: 605px;">
                        <h3><b><u>AFFIDAVIT</u></b></h3>
                    </div>
                    </br>

                    <p>
                        <b>First Party: Seller</b>
                        <div style="margin-left: 50px;">
                            <b>Name: </b> {{ $data['currOwner']['user']->name }} {{ $data['currOwner']['user']->guardian_type }} {{ $data['currOwner']['user']->gurdian_name }}<br>
                            <b>CNIC No: </b> {{ $data['currOwner']['user']->cnic }}<br>
                            <b>R/O (Permanent Address): </b> {{ $data['currOwner']['user']->permanent_address }}<br>
                        </div>
                    </p>

                    <p>
                        <b>Second Party: Buyer</b>
                        <div style="margin-left: 50px;">
                            <b>Name: </b> {{ $data['newOwner']['user']->name }} {{ $data['newOwner']['user']->guardian_type }} {{ $data['newOwner']['user']->gurdian_name }}<br>
                            <b>CNIC No: </b> {{ $data['newOwner']['user']->cnic }}<br>
                            <b>R/O (Permanent Address): </b> {{ $data['newOwner']['user']->permanent_address }}<br>
                        </div>
                    </p>
                    <br>
                    <br>
                    <div style="margin-left: 50px;">
                        Declare on oath as follows: <br><br>
                        <ul>
                            <li>
                                That the seller had booked one {{ $data['plan']->plot_nature }} Plot in {{ env('SOCIETY_NAME') }}, {{ env('SOCIETY_ADDRESS') }}. Through registration number <b>{{ $data['currOwner']->registration_no }}</b> dated <b>{{ date('d-m-Y', strtotime($data['booking']->payment_date)) }}</b> Measuring <b>{{ $data['plan']->plotSize->plot_size }}</b> by the management of this concern.
                            </li>

                            <li>
                                From now onwards the buyer will be responsible for making remaining installments as per schedule of the company and abide by all the present and future terms and conditions/rules and regulations made by {{ env('SOCIETY_NAME') }}'s management.
                            </li>

                            <li>
                                Payment matters are clear between the parties inter se; Seller is willingly surrendering this registration number and has no right to challenge this in furture.
                            </li>

                            <li>
                                That the seller has no objection if the said registration number is transferred to any person and the sale deed to be executed in his name and the possession is transferred to him after completion of all formalities.
                            </li>
                        </ul>




                        <br>

                        <div class="row">
                            <div class="col-sm-6" style="position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width:50%;float:left;">
                                Seller: _____________________
                                <div style="margin-left: 30px;">
                                    {{ $data['currOwner']->user->name }}<br>
                                    CNIC: {{ $data['currOwner']->user->cnic }}
                                </div>  
                            </div>

                            <div class="col-sm-6" style="position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width:50%;float:left;">
                                Buyer: _____________________
                                <div style="margin-left: 30px;">
                                    {{ $data['newOwner']->user->name }}<br>
                                    CNIC: {{ $data['newOwner']->user->cnic }}
                                </div>  
                            </div>
                        </div>

                    </div>
                  
                </div>
                  
               
            </div>
        
         <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
            <script type="text/javascript">

                    // window.print();


              </script>
       
        

    </body>

</html>
