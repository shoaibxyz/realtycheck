<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Acknowledgement Process of Transfer</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}
    body{
        font-size: 22px 
    }
</style>

       
    </head>

<body>
    
    <div id="watermark" style="position: absolute;z-index:-1000;top:-30;">
            <img src="{{ asset('public/') }}/img/cc-logo.png" width="90%"/>
        </div>
    <div id="app">
        
    <div id="logo_header" style="display:block;margin-bottom:10px;margin-top:10px;">

 <div class="container">
            <div class="row">
              <div class="col-xs-6"><img src="{{ asset('public/') }}/images/capital.png"  style="width:100px;"></div>
              
              
              
              <div class="col-xs-6">
     
      <img src="{{ asset('public/') }}/images/pmc-web-logo.png"  style="width:60%;float:right;"></div>
</div>


           
            </div></div></div>
        
        
        
            <div class="container" style="margin: 20px auto">
                <div class="leadger">
                    <div class="text-center" style="margin-top: 80px;">
                        <h3><b>Acknowledgement Process of Transfer</b></h3>
                    </div>
                    </br>
                    </br>
                          
                    <p>
                        Reference number <b>{{ $data['currOwner']->registration_no }}</b>, {{ $data['plan']->plotSize->plot_size}} {{ $data['plan']->plot_nature }} plot is under transfer process. We have received original documents from transferor name <b>{{ $data['currOwner']['user']->name }} ({{ $data['currOwner']['user']->cnic }})</b> and transferee name <b>{{ $data['newOwner']['user']->name }} ({{ $data['newOwner']['user']->cnic }})</b>. (NDC) Non Demand Certificate will be issued to <b>{{ $data['newOwner']['user']->name }} ({{ $data['newOwner']['user']->cnic }})</b> after completion of transfer process.
                    </p>


                    <br>
                    <br>
                    <br>


                    <p><b>Mention documents are received</b></p>
                    <ul>
                        @foreach($data['docs'] as $docs)
                            <li><b>{{ \App\Helpers::TransferDocuments($docs->doc_id) }}</b></li>
                        @endforeach
                    </ul>


                    <br>
                    <br>
                    <br>

                    <p>Sincerely yours,</p>

                    <br>
                    <br>
                    <br>

                    <p><b>Management of {{ env('SOCIETY_NAME') }}</b></p>
                    <p><b>Customer care Dept.</b></p>
                    <p>{{ env('SOCIETY_MOBILE') }}</p>
                    <p>{{ env('SOCIETY_EMAIL') }}</p>
                </div>
                  
               
            </div>
        
         <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
            <script type="text/javascript">

                    window.print();


              </script>
       
        

    </body>

</html>
