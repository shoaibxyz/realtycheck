<!DOCTYPE html>
<html>
<head>
  <title>Ledger</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  
  <style type="text/css">
   
   @media print {
     a[href]:after {
       content: none !important;
     }
   }
   
 </style>
</head>
<body>


  <div class="x_panel">
    
    <div class="x_title">
      
      <div class="clearfix"></div>
    </div>    
    <div id="app">
      <div class="container">
        <div class="leadger">
          
          <div id="logo_header" style="display:block;">
            <div class="row" style="margin-right: -10px;
    margin-left: -10px;">
              <div class="col-xs-4" style="width: 33.33333333%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px;"><img src="{{ asset('public/') }}/images/capital.png"  style="width:100px;"></div>
              
              <div class="col-xs-4" style="width: 33.33333333%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px;"><h3>Ledger - {{ date('d-m-Y') }}</h3></div>
              
              <div class="col-xs-4" style="width: 33.33333333%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px; "><img src="{{ asset('public/') }}/images/Realty-check.png" style="height:50px;"></div>
            </div></div></div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            <div class="main">
              <div class="memberinfo" style="display:inline-block;height:180px;">

                
                {{-- memberinfo --}}
                <div class="row"  style="margin-right: -10px;
    margin-left: -10px;">
                  <div class="col-xs-12" style="width: 100%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px; clear: both;">
    <h3><u>Member Imformation</u></h3>
    </div>
    <br>
    <br>
    <br>
        <br>    
                  <div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;margin-left:12px;">
                     <h5><strong>Member Name: </strong>&nbsp; {{ $person->name }}</h5>
                    <h5> <strong>{{ $person->guardian_type }}:</strong> &nbsp; {{ $person->gurdian_name }}</h5>
                    <h5><strong>Registration No:</strong></strong>   &nbsp; {{ $booking->registration_no }}</h5>
                    <h5> <strong>CNIC No:</strong> &nbsp; </h5>
                    


                  </div>
                  
                  <div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;">
                    <h5><strong>Current Address:</strong> &nbsp; </h5>
                    <h5><strong>Permanent Address:</strong> &nbsp; </h5>
                    <h5> <strong>Mobile No:</strong> &nbsp; </h5>
                    <h5><strong>Form No : </strong>&nbsp; {{ $member->reference_no }}</h5>

                  </div>
                  
                  <div class="col-xs-4" id="right-col" style="width: 33.33333333%;margin-left:0;position: relative; display:inline-block;">
                    
                    <img src="{{ asset('public/')}}/images/{{ $person->picture }}" alt="" style="margin-top:-10px;width:3.5cm;height:4.5cm;">
                  </div>
                </div></div>
<br>
<br>
<br>
                {{-- end --}}
                <hr style="border-bottom:1px solid;">
                {{-- plotinfo --}}
                <br>
            <div class="plotinfo" style="display:inline-block;height:140px;">
                  <div class="row"  style="margin-right: -10px;
    margin-left: -10px;">
                    
                    <div class="col-xs-12" style="width: 100%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px; clear: both;">
    <h3><u>File/Plot Information</u></h3>
    </div>
       <br>
    <br>
    <br>
                        
                    <div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;margin-left:12px;
    ">
                       <h5><strong>Size: </strong>{{ $booking->paymentPlan->plotSize->plot_size }}</h5>
                      <h5> <strong>File/Plot No:</strong> {{ $member->reference_no }}</h5>
                      <h5><strong>Type:</strong> {{ $booking->paymentPlan->plot_nature}}</h5>
                      
                    </div>
                    
                    <div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;
    ">
                      <h5> <strong>Booking Date:</strong>{{ $booking->created_at->format('d-m-Y') }}</h5>
                      <h5><strong>Payment Code: </strong>{{ $booking->paymentPlan->payment_desc }}</h5>
                      <h5> <strong>Street#: </strong></h5>
                      
                    </div>
                    
                    <div class="col-xs-4" id="right-col" style="width: 33.33333333%;margin-left:0;position: relative;display:inline-block;
    ">
                     <h5><strong>Block Name:</strong></h5>
                      <h5> <strong>Dimension:</strong></h5>
                    </div>
                  </div></div>
                  <hr style="border-bottom:1px solid;">
                  {{-- end --}}
<br>
            
                  {{-- paymentdetail --}}
                  <div class="payment" style="display:inline-block;height:180px;">
                    <div class="row"  style="margin-right: -10px;
    margin-left: -10px;">
                      <div class="col-xs-12" style="width: 100%;position: relative;
    min-height: 1px;
    float: left;
    padding-right: 10px;
    padding-left: 10px; clear: both;"><h3><u>Payment Detail</u></h3></div>


                      <?php $received = 0; ?>
                      @foreach($installments as $installment)
                      <?php 
                      if($installment->is_paid == 1)
                        $received += $installment->amount_received ;
                      ?>
                      @endforeach

                      <br>
    <br>
    <br><div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;margin-left:12px;
    ">
                        <h5><strong>Total Amount:</strong> {{ $booking->paymentPlan->total_price }}</h5>
                        <h5><strong> LPC:</strong></h5>
                        <h5><strong> LPC Received: </strong></h5>
                        {{-- <h5>Total Received: {{ round(($received / $booking->paymentPlan->total_price) * 100, 2) }} %</h5> --}}
                        
                      </div>
                      
                      <div class="col-xs-4" style="width: 33.33333333%;position: relative;display:inline-block;
    ">

                        <h5><strong>Total Received:</strong> {{ ($received != 0) ? $received - $booking->rebate_amount : 0 }}</h5>
                        <h5> <strong>Rebate Amount: </strong> {{ $booking->rebate_amount }}</h5>
                        <h5><strong>Over Due Amount:</strong> 0</h5>



                      </div>
                      
                      <div class="col-xs-4" id="right-col" style="width: 33.33333333%;margin-left:0;position: relative;display:inline-block;
    ">

                        <h5> <strong>LPC Os:</strong>0</h5>
                        <h5><strong>Total Os Amount:</strong>0</h5>
                      </div>
                    </div></div>
                  </br>
                  <div class="ins-table">
                    <div class="row"  style="margin-right: -10px;
    margin-left: -10px;">
                      <div class="col-xs-12" style="width: 100%;position: relative;
    ">
                        <button id="printLedger" class="btn btn-success btn-xs" style="display:none;">Print</button>
                        <table class="table" style="width: 100%;
    max-width: 100%;
    margin-bottom: 20px;border-spacing: 0;
    border-collapse: collapse;table-layout: fixed;">
                          <tbody>
                            <tr style="border-bottom: 3px solid;border-top:3px solid;">
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Pay. Decs</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Inst#</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Due Date</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Due Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Rebate Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Recv Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Os Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Recp No</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Recp Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Date</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Pay Mode</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Adj Amt.</th>
                              <th style="border: 2px solid #999;font-size:10px;text-align:center;">Remaining Bal.</th>
                            </tr>
                            
                            @php($remaining_balance = $booking->paymentPlan->total_price)
                            @foreach($installments as $installment)
                            
                            <?php 
                            
                            $received_amt = ($installment->is_paid == 1) ? $installment->due_amount - $installment->os_amount : 0; 
                            if( $installment->payment_desc == 'Balloting')
                            {
                              $balloting = $installment;
                              continue;
                            }
                            
                            
                        
                            $remains = $remaining_balance - ($installment->due_amount - $installment->os_amount);
                            
                            
                           //$remaining_balance =  ($installment->amount_received != 0) ? $remains : "-";
                           $remaining_balance =  ($installment->amount_received != 0) ? $remains : 0;
                        
                           
                            ?>
                            <tr style="border: 2px solid #999">
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $installment->payment_desc }}</td>
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $installment->installment_no}}</td>
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($installment->due_amount) }}</td>
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($installment->rebat_amount != 0) ? number_format($installment->rebat_amount) : '-'    }}</td>
                              <td style="border: 2px solid #999;font-size:10px;text-align:center;">
                                <?php $receiving_amount = ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $received_amt; ?>
                                {{ ($receiving_amount != 0) ? number_format($receiving_amount) : '-'   }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($installment->os_amount != 0) ? number_format($installment->os_amount) : '-'}}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $installment->receipt_no or  "-"}}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($installment->receipt_amount != 0) ? number_format($installment->receipt_amount) : '-'  }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($installment->payment_date != null) ? date('d-m-Y', strtotime($installment->payment_date)): "-"}}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $installment->payment_mode or "-" }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($installment->amount_received != 0) ? number_format($installment->amount_received) : '-' }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($remaining_balance != 0) ? number_format((double)$remaining_balance) : "-" }}</td>
                               
                              </tr>
                              @endforeach
                              <tr style="border: 2px solid #999">
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $balloting->payment_desc }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $balloting->installment_no}}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ date('d-m-Y',strtotime($balloting->installment_date)) }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->due_amount != 0) ? number_format($balloting->due_amount) : '-'  }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->rebat_amount != 0) ? number_format($balloting->rebat_amount)  : '-' }}</td>
                                <td style="border: 2px solid #999;font-size:10px;text-align:center;">
                                  <?php $receiving_amount = ($balloting->os_amount > 0) ? $balloting->due_amount - $balloting->os_amount : $received_amt; ?>
                                  {{ ($receiving_amount != 0) ? number_format($receiving_amount)  : '-' }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->os_amount != 0) ? number_format($balloting->os_amount)  : '-'}}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $balloting->receipt_no }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->receipt_amount != 0) ? number_format($balloting->receipt_amount)  : '-' }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->payment_date != null) ? date('d-m-Y', strtotime($balloting->payment_date)): ""}}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ $balloting->payment_mode }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($balloting->amount_received != 0) ? number_format($balloting->amount_received)  : '-' }}</td>
                                  @php($remaining_balance = $balloting->due_amount-$remaining_balance)
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }}</td>
                                </tr>

                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;font-size:12px;text-align:center;">

                                  <h5>Sub Total =></h5></td>

                                  <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>
                                  @foreach($installments as $installment)
                                  <?php 
                                  $due_amount += $installment->due_amount; 
                                    // $os_amount += $installment->due_amount - $installment->amount_received; 
                                  $rebat_amount += $installment->rebat_amount;
                                  $amount_received += $installment->amount_received;
                                  $adj_amount += $installment->amount_received;
                                  ?>
                                  @endforeach
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($due_amount)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($rebat_amount)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($amount_received) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($os_amount) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($adj_amount) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }}</td>
                                  

                                </tr>


                                <tr style="border: 2px solid #999"><td colspan="2" style="border: 2px solid #999;font-size:12px;text-align:center;">
                                  <h5>LPC =></h5></td>

                                  <?php $lpc = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>
                                  @foreach($installments as $installment)
                                  <?php 
                                  $lpc += $installment->lpc_charges; 
                                    // $os_amount += $installment->due_amount - $installment->amount_received; 
                                  $rebat_amount += $installment->rebat_amount;
                                  $amount_received += $installment->amount_received;
                                  $adj_amount += $installment->amount_received;
                                  ?>
                                  @endforeach
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ Carbon\Carbon::today()->format('d-m-Y') }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($lpc)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;"></td>
                                </tr>

                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;font-size:12px;text-align:center;">

                                  <h5>Sub Total =></h5></td>

                                  <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>
                                  @foreach($installments as $installment)
                                  <?php 
                                  $due_amount += $installment->due_amount; 
                                    // $os_amount += $installment->due_amount - $installment->amount_received; 
                                  $rebat_amount += $installment->rebat_amount;
                                  $amount_received += $installment->amount_received;
                                  $adj_amount += $installment->amount_received;
                                  ?>
                                  @endforeach
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($lpc)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($rebat_amount)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($amount_received) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($os_amount) }}</td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($adj_amount) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }}</td>
                                  

                                </tr>
                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;font-size:12px;text-align:center;">

                                  <h5>Grand Total =></h5></td>

                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($lpc  + $due_amount) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($rebat_amount)  }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($amount_received) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($os_amount) }}</td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999"></td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ number_format($adj_amount) }}</td>
                                  <td style="border: 2px solid #999;font-size:10px;text-align:center;">{{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }}</td>

                                </tr>
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                        {{-- end --}}



                      </div>
                      <footer id="footer" style="text-align:center;display:block;"><img src="{{ asset('public/') }}/images/logo(2).png" style="width:150px;"> © 2017 All Rights Reserved</footer>


                    </div>


                  </div>
                </div></div>

              </body>
              </html>
              