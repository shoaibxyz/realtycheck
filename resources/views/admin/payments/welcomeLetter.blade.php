<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Acknowledgement Process of Transfer</title>

        <!-- CSS -->
               
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
        
        
       
        <style type="text/css">
@media print {
  .visible-print  { display: inherit !important; }
  .hidden-print   { display: none !important; }
}
    body{
        font-size: 22px 
    }
</style>

       
    </head>

<body>
    <div id="app">
        
            <div class="container" style="margin: 180px 50px 0px 50px;">
                <div class="leadger">
                    <div class="text-center" style="margin-top: 150px;">
                        <h2><b>WELCOME LETTER</b></h2>
                    </div>
                    </br>
                    </br>
                          
                        <p>We would like to Welcome you to <b>{{ env('SOCIETY_NAME') }}</b> and congratulate you on becoming a valuable memebr of our family. We appreciate your trust upon us and we will try to maintain it by providing you the best possible service at all time.</p>

                    <br>
                    <br>
                    <br>

                    <p>we are enclosing the <b>Provisional Allotment Certificate</b> to confirm that the plot/File has been registered in your name. The agreed payment plan for the balance payment is also attached.</p>


                    <br>
                    <br>
                    <br>

                    <p>If you have any further question / query please don't hesitate to contact customer care dept. We look forward to have successful realtionship with you.</p>

                    <br>
                    <br>
                    <br>
                    <br>
                    <p>Sincerely yours,</p>

                    <br>
                    <br>
                    <br>

                    <p><b>Management of {{ env('SOCIETY_NAME') }}</b></p>
                    <p><b>Customer care Dept.</b></p>
                    <p>{{ env('SOCIETY_MOBILE') }}</p>
                    <p>{{ env('SOCIETY_EMAIL') }}</p>
                </div>
                  
               
            </div>
        
         <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
            <script type="text/javascript">

                    window.print();


              </script>
       
        

    </body>

</html>
