<div id="data" class="row" style="margin-top: 30px;">
    <div class="col-sm-2" style="text-align: center">
        <img id="img-preview" src="{{ env('STORAGE_PATH') . '/'. $member->picture }}"  style="margin: 0px auto;" alt="Picture" width="155" height="155" />
    </div>
    <div id="member" class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Name <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('person', $member->name, ['class' => 'form-control', 'id' => 'person', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">CNIC <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
            <label for="inputEmail3" class="col-sm-5 control-label">Registration No <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('reference_no') ? ' has-error' : '' }}">
            <label for="inputEmail3" class="col-sm-5 control-label">Reference No <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('reference_no', null, ['class' => 'form-control', 'id' => 'reference_no', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Agency <span class="required">*</span></label>
            <div class="col-sm-7">
            <?php
                $agencyArr[''] = "";
                foreach ($agencies as $agency) {
                        $agencyArr[$agency->id] = $agency->name;
                    }
            ?>
                {!! Form::select('agency_id', $agencyArr, 0, ['class' => 'form-control', 'id' => 'agency', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Preferences</label>
            <div class="col-sm-7">
                @foreach($featureset as $featur)
                    <div class="checkbox">
                        {!! Form::checkbox("feature_set_id[]", $featur->id, false, ['class' => 'flat']) !!}
                        {{$featur->feature}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="persons" class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Plan</label>
            <div class="col-sm-7">
                <?php
                    $paymentScheduleArr[''] = "";

                    foreach ($paymentSchedule as $plan) {
                            $paymentScheduleArr[$plan->id] = $plan->payment_code;
                    }
                ?>
                {!! Form::select('payment_schedule_id', $paymentScheduleArr, 0, ['class' => 'form-control', 'id' => 'payment_schedule','selected' => 'selected', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Date</label>
            <div class="col-sm-7">
                {!! Form::text('payment_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'payment_date', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Rebat Amount</label>
            <div class="col-sm-7">
                <input class="form-control" id="rebate_amount" name="rebate_amount" type="text">
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Remarks</label>
            <div class="col-sm-7">
                {!! Form::text('payment_remarks', null , ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-5 control-label">Offers</label>
            <div class="col-sm-7">
                <div class="checkbox">
                    <input type="checkbox" class="flat" name="offer" value="launch_offer" > Launch Offer
                </div>
                <div class="checkbox">
                    <input type="checkbox" class="flat" value="waive_dp" name="offer"> Waive Down Payment
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        {{ csrf_field() }}
        {!! Form::hidden('cnic', null, ['id' => 'cnic_field']) !!}
        {!! Form::hidden('total_price', null, ['id' => 'total_price']) !!}
        {!! Form::hidden('payment_type_id', 4, ['id' => 'payment_type_id']) !!}
    </div>
</div>
