@extends('admin.template')
@section('title','Reactive Booking')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <style type="text/css">
        .bottom-row{
            margin: 20px auto;
            border: 1px solid;
            padding: 5px;
        }
        .bottom-row span{
            font-size: bold;
        }
        .bottom-row input{
            width: 70px;
        }
    </style>
@endsection

@section('content')
        
               <div class="x_panel">
        <div class="x_title">
            <h2>Reactive Booking</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
          
        <div class="clearfix"></div>
       <div class="row">
            @if(isset($booking) && $booking->is_cancelled)
                <div class="col-sm-3">
                    <p class="text-danger">This File is cancelled</p>
                    <p class="text-danger">Cancel By:  {{ \App\User::getUserInfo($booking->cancel_by)->name }}</p>
                    <p class="text-danger">Cancel Date: {{ $booking->cancel_date->format('d-m-Y') }}</p> 
                    <p class="text-danger">Cancel Remarks: {{ $booking->cancel_remarks }}</p>
                </div>

                 <div class="col-sm-3">
                    <p class="text-success">This File is Reactivated</p>
                    <p class="text-success">Reactivated By:  {{ \App\User::getUserInfo($booking->cancel_by)->name }}</p>
                    <p class="text-success">Reactivated Date: {{ $booking->reactive_date }}</p> 
                    <p class="text-success">Reactivated Remarks: {{ $booking->reactive_remarks }}</p>
                </div>
                <div class="col-sm-6">
            @else
                <div class="col-sm-offset-6 col-sm-6">
            @endif

         {!! Form::open(['method' => 'GET', 'route' =>['booking.cancel-reactive', 1], 'class' => 'form-horizontal']) !!}
            
            <div class="form-group col-sm-10" style="margin-left:-12px;">
            {!! Form::label('registration_no', 'Registration No')!!}
            {!! Form::text('registration_no', $registration_no, ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!}
            </div>
            <div class="col-sm-2">
            <button type="submit" id="payment" class="btn btn-success" style="margin-top: 25px;">Search</button>
            </div>
            {!! Form::close() !!}
            </div>
            </div>
            <hr>

        @if(isset($user))
       {!! Form::model($user, ['id' => 'inquiry', 'class' => 'form-horizontal']) !!}
       {{-- {{ dd($user) }} --}}
        <div>
            <div  class="col-sm-4" style="clear: both">

                <div class="form-group">
                {!! Form::label('name', 'Name')!!}
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('username', 'Username')!!}
                {!! Form::text('username', null, ['class' => 'form-control', 'id' => 'username']) !!}
                </div>

                <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
                {!! Form::label('registration_no', 'Registration No')!!}
                {!! Form::text('registration_no', $member->registration_no, ['class' => 'form-control', 'id' => 'registration_no']) !!}
                </div>


                <div class="form-group{{ $errors->has('reference_no') ? ' has-error' : '' }}">
                {!! Form::label('reference_no', 'Reference No')!!}
                {!! Form::text('reference_no', $member->reference_no, ['class' => 'form-control', 'id' => 'reference_no']) !!}
                </div>


                <div class="form-group">
                {!! Form::label('gurdian_name', 'Father Name')!!}
                {!! Form::text('gurdian_name', null, ['class' => 'form-control', 'id' => 'gurdian_name']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('cnic', 'CNIC')!!}
                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic']) !!}
                </div>

                 <div class="form-group">
                {!! Form::label('nationality', 'Nationality')!!}
                {!! Form::text('nationality', null, ['class' => 'form-control', 'id' => 'nationality']) !!}
                </div>
            </div>

        <div class="col-sm-4">
            

            <div class="form-group">
            {!! Form::label('current_address', 'Current Address')!!}
            {!! Form::text('current_address', null, ['class' => 'form-control', 'id' => 'current_address']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('country', 'Country')!!}
            {!! Form::text('country_id', null, ['class' => 'form-control', 'id' => 'country']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('city', 'City')!!}
            {!! Form::text('city_id', null, ['class' => 'form-control', 'id' => 'city']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_office', 'Phone #')!!}
            {!! Form::text('phone_office', null, ['class' => 'form-control', 'id' => 'phone_office']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_res', 'Phone Res. #')!!}
            {!! Form::text('phone_res', null, ['class' => 'form-control', 'id' => 'phone_res']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('phone_mobile', 'Mobile #')!!}
            {!! Form::text('phone_mobile', null, ['class' => 'form-control', 'id' => 'phone_mobile']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('email', 'Email')!!}
            {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
            </div>
               </div>
        <div class="col-sm-4">
            
            <div class="form-group">
                {!! Form::label('picture', 'Picture', ['class' => 'col-sm-12'])!!}
               <div class="col-md-6">
                <img id="img-preview" src="{{ env('STORAGE_PATH'). $user->picture }}" name="picture"  style="margin: 10px auto;width:3.5cm;height:4.5cm;" alt="Picture"/>
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 30px;">
                <div class="followup">
                    <a href="{{ route('followup.show', $member->registration_no) }}" class="btn btn-warning btn-xs">Followup</a>
                </div>
            </div>

            <div class="form-group">
            {!! Form::label('booking_date', 'Booking Date')!!}
            {!! Form::text('booking_date', $booking->created_at->format('d-m-Y'), ['class' => 'form-control', 'id' => 'booking_date']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('payment_plan', 'Payment Plan')!!}
            {!! Form::text('payment_plan', $booking->paymentPlan->payment_code, ['class' => 'form-control', 'id' => 'payment_plan']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('total_price', 'Total Price')!!}
            {!! Form::text('total_price', $booking->paymentPlan->total_price, ['class' => 'form-control', 'id' => 'total_price']) !!}
            </div>

            <div class="form-group">
            {!! Form::label('booking_price', 'Booking Price')!!}
            {!! Form::text('booking_price', $booking->paymentPlan->booking_price, ['class' => 'form-control', 'id' => 'booking_price']) !!}
            </div>
        </div>
            </div>
            {!! Form::close() !!}

    </div>
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#statusChange" aria-controls="statusChange" role="tab" data-toggle="tab">statusChange</a>
            </li>
            <li role="presentation">
                <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">tab</a>
            </li>
        </ul>
    
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="statusChange">
                <div class="x_panel">
                       <div class="x_title">
                        <h2>Reactive</h2>
                        <div class="clearfix"></div>
                      </div>   
                        <div class="row">
                            @if($booking->is_cancelled == 1)

                             {!! Form::open(['route' => ['booking.postReactive', $registration_no], 'method' => 'post', 'id' => 'reactiveForm']) !!}
                             @endif

                            <div class="col-sm-4">
                                 <div class="form-group">
                                    {!! Form::label('reactive_date', 'Reactive Date') !!}
                                    {!! Form::text('reactive_date', $booking->reactive_date, ['class' => 'form-control datepicker', 'required' => 'required']) !!}
                                </div>  
                            </div>

                            <div class="col-sm-8">
                                 <div class="form-group">
                                    {!! Form::label('reactive_remarks', 'Reactive Remarks') !!}
                                    {!! Form::text('reactive_remarks', $booking->reactive_remarks, ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>  
                            </div>   

                            <div class="col-sm-4">
                                 <div class="form-group">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
                                </div>  
                            </div> 
                        </div>
                        @if($booking->is_cancelled == 1)
                    {!! Form::close() !!}
                        @endif
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab">...</div>
        </div>
    </div>



@endif
@endsection





@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.dataTable').DataTable({});
            $.ajax({
                url: '{{ route('alert.check') }}',
                type: 'GET',
                data: {registration_no: $('#registration_no').val() },
                success: function(data){
                    console.log(data);
                    $.each(data.alert, function(index, key){
                        alert(key.message);
                        // alert(index.message);
                    });
                    // if(data.status == 1)
                    //     swal("Alert!", data.alert.message, "error"); 
                },
                erorr: function(xhr, status, responseText){
                    console.log(status);
                }
            });
            
             $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $('.reactiveRegistration').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You want to reactive this file ?",
                  type: "warning",
                  showreactiveButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, reactive!",
                  reactiveButtonText: "No, return!",
                  closeOnConfirm: false,
                  closeOnreactive: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("reactiveled!", "Your file has been reactiveled.", "success");
                    form.submit();
                  } else {
                    swal("Oops!", "Your file is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
    </script>
@endsection



