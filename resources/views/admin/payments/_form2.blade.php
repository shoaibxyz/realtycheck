<div id="data">
<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Personal Info</h2>
<div class="row">
    <div class="col-sm-2" style="text-align: center">
        <img id="img-preview" src="https://docs.moodle.org/27/en/images_en/7/7c/F1.png"  style="margin: 0px auto;" alt="Picture" width="155" height="155" />
    </div>
    <div id="member" class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Name</label>
            <div class="col-sm-7">
                {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
            <label for="inputEmail3" class="col-sm-5 control-label">Registration No <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">CNIC <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Current Address</label>
            <div class="col-sm-7">
                {!! Form::text('current_address', null, ['class' => 'form-control', 'id' => 'current_address', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">City</label>
            <div class="col-sm-7">
                {!! Form::text('city_id', null, ['class' => 'form-control', 'id' => 'city', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Phone Residential</label>
            <div class="col-sm-7">
                {!! Form::text('phone_res', null, ['class' => 'form-control', 'id' => 'phone_res', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Email</label>
            <div class="col-sm-7">
                {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'readonly' => 'readonly']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Father Name</label>
            <div class="col-sm-7">
                {!! Form::text('gurdian_name', null, ['class' => 'form-control', 'id' => 'gurdian_name']) !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('reference_no') ? ' has-error' : '' }}">
            <label for="inputEmail3" class="col-sm-5 control-label">Reference No <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('reference_no', null, ['class' => 'form-control', 'id' => 'reference_no', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Nationality</label>
            <div class="col-sm-7">
                {!! Form::text('nationality', null, ['class' => 'form-control', 'id' => 'nationality']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Country</label>
            <div class="col-sm-7">
                {!! Form::text('country_id', null, ['class' => 'form-control', 'id' => 'country', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Phone Office</label>
            <div class="col-sm-7">
                {!! Form::text('phone_office', null, ['class' => 'form-control', 'id' => 'phone_ofc', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Phone Mobile</label>
            <div class="col-sm-7">
                {!! Form::text('phone_mobile', null, ['class' => 'form-control', 'id' => 'phone_mobile', 'readonly' => 'readonly']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Agency <span class="required">*</span></label>
            <div class="col-sm-7">
                <?php
                    $agencyArr[''] = "";
                    foreach ($agencies as $agency) {
                        $agencyArr[$agency->id] = $agency->name;
                    }
                ?>
                {!! Form::select('agency_id', $agencyArr, 0, ['class' => 'form-control', 'id' => 'agency', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
</div>
<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Payment Info</h2>
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Plan <span class="required">*</span></label>
            <div class="col-sm-7">
                <?php
                $paymentScheduleArr[''] = "";
                foreach ($paymentSchedule as $plan) {
                        $paymentScheduleArr[$plan->id] = $plan->payment_code;
                }
                ?>
                {!! Form::select('payment_schedule_id', $paymentScheduleArr, 0, ['class' => 'form-control', 'id' => 'payment_schedule','disabled' => 'disabled','selected' => 'selected']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Remarks</label>
            <div class="col-sm-7">
                {!! Form::text('payment_remarks', null , ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Payment Date <span class="required">*</span></label>
            <div class="col-sm-7">
                {!! Form::text('payment_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'payment_date']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Rebat Amount</label>
            <div class="col-sm-7">
                <input class="form-control" id="rebate_amount" name="rebate_amount" type="text">
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>
</div>
<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Nominee Info</h2>
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Nominee Name</label>
            <div class="col-sm-7">
                {!! Form::text('nominee_name', null, ['class' => 'form-control', 'id' => 'nominee_name']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Nominee CNIC</label>
            <div class="col-sm-7">
                {!! Form::text('nominee_cnic', null, ['class' => 'form-control', 'id' => 'nominee_cnic']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Nominee Guardian</label>
            <div class="col-sm-7">
                {!! Form::text('nominee_guardian', null, ['class' => 'form-control', 'id' => 'nominee_guardian']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Relation With Member</label>
            <div class="col-sm-7">
                {!! Form::text('relation_with_member', null, ['class' => 'form-control', 'id' => 'relation_with_member']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>
</div>
<h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Other Info</h2>
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Preferences</label>
            <div class="col-sm-7">
                @foreach($featureset as $featur)
                    <div class="checkbox">
                        {!! Form::checkbox("feature_set_id[]", $featur->id, false, ['class' => 'flat']) !!}
                        {{$featur->feature}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label class="col-sm-5 control-label">Offers</label>
            <div class="col-sm-7">
                <div class="checkbox">
                    <input type="checkbox" class="flat" name="offer" value="launch_offer" > Launch Offer
                </div>
                <div class="checkbox">
                    <input type="checkbox" class="flat" value="waive_dp" name="offer"> Waive Down Payment
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Street No</label>
            <div class="col-sm-7">
                {!! Form::text('street_no', null, ['class' => 'form-control', 'id' => 'street_no']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">Block No</label>
            <div class="col-sm-7">
                {!! Form::text('block_no', null, ['class' => 'form-control', 'id' => 'block_no']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-5 control-label">plot No</label>
            <div class="col-sm-7">
                {!! Form::text('plot_no', null, ['class' => 'form-control', 'id' => 'plot_no']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>
</div>
<div class="row">
    <div class="col-sm-12">
        {{ csrf_field() }}
        {!! Form::hidden('cnic', null, ['id' => 'cnic_field']) !!}
        {!! Form::hidden('total_price', null, ['id' => 'total_price']) !!}
        {!! Form::hidden('payment_type_id', 4, ['id' => 'payment_type_id']) !!}
    </div>
</div>
</div>
