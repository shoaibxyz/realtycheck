@extends('admin.layouts.template')

@section('title','Add New Booking')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Booking</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Booking</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Booking</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('booking.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Booking</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::model($member, ['route'=>'booking.store', 'files' => true, 'id' => 'paymentForm2', 'class' => 'form-horizontal'])!!}

            @include('admin.payments._form3')

            <div class="row form-actions text-right pal" style="margin: 30px auto;">
                <a href="#" class="btn btn-warning" id="generatePaymentPlan" >Generate From Payment Plan</a>
                <input class="btn btn-success bookingBtn" type="submit" value="Save">
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#Installment">Payment Plan</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="Installment" class="tab-pane fade in active">
                            <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                                <table class="table table-bordered table-hover table-striped table-condenced">
                                    <thead>
                                        <tr>
                                            <th>Inst. No</th>
                                            <th>Inst. Date</th>
                                            <th>Inst. Amount</th>
                                            <th>Rebat Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody class="installmentAppend"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="col-sm-4">

                    <h4>Special Plan</h4>
                    {!! Form::open(['method' => 'POST', 'id' => 'specialPlan', 'route' => 'booking.specialPlans', 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                    {!! Form::label('start_date', 'start date')!!}
                    {!! Form::text('start_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'start_date']) !!}
                    </div>

                    <?php
                            $paymentFrequency[''] = "";
                            $paymentFrequency[1] = "After 1 Month";
                            $paymentFrequency[2] = "After 2 Month";
                            $paymentFrequency[3] = "After 3 Month";
                            $paymentFrequency[4] = "After 4 Month";
                    ?>

                    <div class="form-group">
                        {!! Form::label('payment_frequency', 'Payment Frequency') !!}
                        {!! Form::select('payment_frequency', $paymentFrequency, old('payment_frequency'), ['required' =>'required', 'class' => 'form-control has-feedback-left', 'id' => 'payment_frequency']) !!}

                    </div>

                    <div class="form-group">
                    {!! Form::label('total_inst', 'Total Installment')!!}
                    {!! Form::text('total_inst', null, ['class' => 'form-control', 'id' => 'total_inst']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit("Generate Special Plan", ['class' => 'btn btn-info btn-block', 'id' => 'generateSpcialPlan']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $('#paymentForm2').submit(function(){
        $('.bookingBtn').attr('disabled', 'disabled');
        $(this).submit();
    });


    $('#registration_no').blur(function(){
        var reg = $(this).val();
        var URL = "{{ route('agency.byRegistrationNo') }}";
        $.ajax({
            url: URL,
            type: 'POST',
            data: {reg: reg},
            success: function(data){
                if(data.status != 200)
                {
                    swal({
                      title: "Oops!",
                      text: "This registration number does not exist",
                      type: "warning",
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#agency').select2().val("").trigger('change');
                    return;
                }
                $('#agency').select2().val(data.result.id).trigger('change');
                // $('#agency').trigger('change');
            },
            error: function(xhr, status, responseText){
                console.log(xhr);
            }
        });
    });

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('#generatePaymentPlan').click(function(e){
        e.preventDefault();
        $('.installmentAppend').html("");
        var payment_schedule_id = $('#payment_schedule').val();
        var payment_date = $('#payment_date').val();

        if(payment_schedule_id == "")
        {
             swal({
              title: "Oops!",
              text: "Please choose payment plan first",
              type: "warning",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
            });
             return;
        }

        $.ajax({
            url: '{{ route('installmentPlan') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': payment_schedule_id},
            success: function(result){

                var total_inst =  $(result).toArray().length;

                var i = 0;
                var j = 2;
                while(i <= total_inst)
                {
                        var installment = result[i];
                        var rebate_amount = $('#rebate_amount').val();
                        console.log(rebate_amount);
                        var inst_rebate = 0;
                        if(rebate_amount)
                            inst_rebate = rebate_amount / total_inst;
                        // var splitDate = result[i].installment_date.split("-");

                        // var instDate = payment_date.split('/');
                        // if(i > 0)
                        // {
                        //     inst_date = instDate[0]  + '-' + parseInt(dateObj.getMonth() + 1) + '-' + dateObj.getFullYear();
                        //     dateObj.addMonths(1);
                        // }else{
                        //      var dateObj = new Date();
                        //     // dateObj.setMonth(dateObj.getMonth() + i);
                        //     dateObj.addMonths(2);
                        //     // console.log(instDate[0]);
                        //     inst_date = instDate[0]  + '-' + dateObj.getMonth() + '-' + dateObj.getFullYear();
                        // }

                        if(i > 0)
                        {
                            console.log(month);
                            inst_date = 10  + '-' + parseInt(month.getMonth() + 1) + '-' + dateObj.getFullYear();
                            dateObj.addMonths(1);
                        }else{
                            var dateObj = new Date();
                            var month = (dateObj.getMonth() == 10) ? dateObj.addMonths(3) : dateObj.addMonths(2);
                            inst_date = 10  + '-' + month.getMonth() + '-' + dateObj.getFullYear();
                        }

                        var html = "<tr>";
                        html += "<td class='sr_no'><input type='text' name='installment_no[]'  value='"+result[i].installment_no+"'></td>";
                        html += '<td><input class="datepicker2" name="installment_date[]" type="text" value="'+inst_date+'" readonly="readonly"></td>';
                        html += '<td><input class="" name="installment_amount[]" type="text" value="'+ Math.round(result[i].installment_amount - inst_rebate,2) +'" readonly="readonly"></td>';
                        html += '<td><input class="" name="rebat_amount[]" type="text" readonly="readonly" value="'+ Math.round(inst_rebate) +'"></td>';
                        $('.installmentAppend').append(html);
                    // if(i >= inst) return;
                    i++;
                    j++;
                    // if(payment_frequency == 1)
                    //     payment_frequency++;
                    // if(payment_frequency == 2)
                    //     payment_frequency+=2;
                    // if(payment_frequency == 3)
                    //     payment_frequency+=3;
                }
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        });

     });

    Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
    };

    Date.getDaysInMonth = function (year, month) {
        return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    };

    Date.prototype.isLeapYear = function () {
        return Date.isLeapYear(this.getFullYear());
    };

    Date.prototype.getDaysInMonth = function () {
        return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
    };

    Date.prototype.addMonths = function (value) {
        var n = this.getDate();
        this.setDate(1);
        this.setMonth(this.getMonth() + value);
        this.setDate(Math.min(n, this.getDaysInMonth()));
        return this;
    };

     $('#specialPlan').on('submit', function(e){
        e.preventDefault();

        var payment_schedule_id = $('#payment_schedule').val();

        if(payment_schedule_id == "")
        {
             swal({
              title: "Oops!",
              text: "Please choose payment plan first",
              type: "warning",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
            });
             return;
        }

        $('.installmentAppend').html("");
        var total_inst = parseInt($('#total_inst').val());

        var payment_frequency = parseInt($('#payment_frequency').val());
        var start_date = $('#start_date').val();

        var total_price = $('#total_price').val();
        var per_inst = parseInt(total_price / total_inst);

        var i = 1;

        while(i <= total_inst)
        {
                if(i > 1)
                {
                    if(payment_frequency == 3 )
                    {
                        dateObj.addMonths(payment_frequency);
                        inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() % 12) + '-' + dateObj.getFullYear();
                    }else{
                        inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() + payment_frequency) + '-' + dateObj.getFullYear();
                        dateObj.addMonths(payment_frequency);
                    }
                }else{
                    var splitDate = start_date.split("/");
                    var dateObj = new Date(splitDate[2],splitDate[1],splitDate[0]);
                    inst_date = dateObj.getDate()  + '-' + dateObj.getMonth()  + '-' + dateObj.getFullYear();
                }

                var html = "<tr>";
                html += "<td class='sr_no'><input type='text' name='installment_no[]'  value='"+i+"' readonly='readonly'></td>";
                html += '<td><input class="datepicker installment_date" name="installment_date[]" type="text" readonly="readonly" value="'+inst_date+'"></td>';
                html += '<td><input class="" name="installment_amount[]" type="text" value="'+per_inst+'" readonly="readonly"></td>';
                html += '<td><input class="" name="percentage[]" type="text" value="0" readonly="readonly"></td>';
                html += '<td><input class="" name="rebat_amount[]" type="text" value="0" readonly="readonly"></td>';
                html += '<td><input class="" name="amount_received[]" type="text" value="0" readonly="readonly"></td>';
                html += '<td><input class="" name="installment_remarks[]" type="text" value="" readonly="readonly"></td>';
                $('.installmentAppend').append(html);
            i++;
            // payment_frequency += payment_frequency;
        }

     });

    $('#payment_schedule').change(function(event) {
        event.preventDefault();
        var planId = $(this).val();
        //alert('{{ URL::to('/PlanInfo') }}');
        $.ajax({
            url: '{{ route('PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
                $('#total_price').val(result.total_price);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        });
        return false;
    });

    //  $('#agent').change(function(event) {
    //     var agent_id = $(this).val();
    //     $.ajax({
    //         url: '{{ URL::to('/AgentInfo') }}',
    //         type: 'POST',
    //         dataType: 'JSON',
    //         data: {'agent_id': agent_id},
    //         success: function(result){
    //             // console.log(result);
    //             $('.agentCommission').removeClass('hide');
    //             $('#AgentInfo').text(result.commission);
    //         },
    //         error: function(xhr, status,response){
    //             console.log(xhr);
    //         }
    //     });
    // });

    // var inputs = $('#data input, #data select').attr('disabled','disabled');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    // $('#data input, #data select').removeAttr('disabled','disabled');
                    // $('#data input#cnic').attr('disabled','disabled');
                    // $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#cnic_field').val($('#cnic').val());
                    $('#img-preview').attr('src','../../'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });

    </script>
@endsection

