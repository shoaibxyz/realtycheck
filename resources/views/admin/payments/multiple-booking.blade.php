@extends('admin.template')
@section('title','Booking')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    	
    	 <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>


               <div class="x_panel">
        <div class="x_title">
            <h2>Multiple Bookings</h2>
            
            <div class="clearfix"></div>
          </div>   

                </div>

        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    
        
    </script>
@endsection

