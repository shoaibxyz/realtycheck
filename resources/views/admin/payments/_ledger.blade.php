<div class="x_panel">
  
  <div class="x_title">
    
    
    <div class="clearfix"></div>
  </div>    
  <div id="app">
    
    <div class="container">
      <div class="leadger">
        <div class="loader-image" style="display: none; top: 7%; left: 40%; position:absolute;">
            <img src="{{ asset('public/images/ajax-loader.gif') }}" width="200px" height="200px" style="background-repeat: no-repeat; background-position: center center; ">
          </div>
        
        <div id="logo_header">
          <div class="row">
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/capital.png"  style="width:180px;"></div>
            <div class="col-xs-2"></div>
            <div class="col-xs-3"><h2 style="margin-top:32px;">Ledger - {{ date('d-m-Y') }}</h2></div>
            <div class="col-xs-1"></div>
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/Realty-check.png" style="height:90px;float:right;margin-top:10px;"></div>
          </div></div>
          <div class="main">
            <div class="memberinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">

              
              {{-- memberinfo --}}
              <div class="row">
                @if(isset($booking) && $booking->is_cancelled)
                    <div class="col-sm-12">
                        <p class="text-danger">This File is cancelled</p>
                        <p class="text-danger">Cancel By:  {{ \App\User::getUserInfo($booking->cancel_by)->name }}</p>
                        <p class="text-danger">Cancel Date: {{ $booking->cancel_date }}</p> 
                        <p class="text-danger">Cancel Remarks: {{ $booking->cancel_remarks }}</p>
                    </div>
                @endif
                <div class="col-xs-12"><h3><u>Member Information</u></h3></div>
                <div class="col-xs-3">
                  <h5><strong>Member Name: </strong>&nbsp; {{ $person->name }}</h5>
                  <h5> <strong>{{ $person->guardian_type }}:</strong> &nbsp; {{ $person->gurdian_name }}</h5>
                  <h5><strong>Registration No:</strong></strong>   &nbsp; {{ $booking->registration_no }}</h5>
                  <h5> <strong>CNIC No:</strong> &nbsp; {{ $person->cnic }}</h5>

                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-3">
                  <h5><strong>Current Address:</strong> &nbsp; {{ $person->current_address }}</h5>
                  <h5><strong>Permanent Address:</strong> &nbsp; {{ $person->permanent_address }}</h5>
                  <h5> <strong>Mobile No:</strong> &nbsp; {{ $person->phone_mobile }}</h5>
                  <h5><strong>Form No : </strong>&nbsp; {{ $member->reference_no }}</h5>

                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-3" id="right-col" style="width:3.5cm;height:4.5cm;">
                  
                  <img src="{{ env('STORAGE_PATH').$person->picture }}" alt="" style="width:3.5cm;height:4.5cm;">
                </div>
              </div></div>

              {{-- end --}}
              
              {{-- plotinfo --}}
              
              <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>File/Plot Information</u></h3></div>
                  
                  <div class="col-xs-3">
                    <h5><strong>Size: </strong>{{ $booking->paymentPlan->plotSize->plot_size }}</h5>
                    <h5> <strong>File/Plot No:</strong> {{ $member->reference_no }}</h5>
                    <h5><strong>Type:</strong> {{ $booking->paymentPlan->plot_nature}}</h5>
                    
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-3">
                    <h5> <strong>Booking Date:</strong>{{ $booking->created_at->format('d-m-Y') }}</h5>
                    <h5><strong>Payment Code: </strong>{{ $booking->paymentPlan->payment_desc }}</h5>
                    <h5> <strong>Street#: </strong></h5>
                    
                  </div>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-3" id="right-col" >
                    <h5><strong>Block Name:</strong></h5>
                    <h5> <strong>Dimension:</strong></h5>
                  </div>
                </div></div>
                {{-- end --}}

                {{-- paymentdetail --}}
                <div class="payment">
                  <div class="row">
                    <div class="col-xs-12"><h3><u>Payment Detail</u></h3></div>

                    <?php $received = 0;
                    $osamount = 0; ?>
                    @foreach($installments as $installment)
                    <?php 
                    if($installment->is_paid == 1 || ( $installment->is_paid == 0  && $installment->amount_received != 0)  )  

                      $received += $installment->amount_received ;
                    

                    ?>

                    @endforeach

                    <div class="col-xs-3">
                      <h5><strong>Total Amount:</strong> {{ $booking->paymentPlan->total_price }}</h5>
                      <h5><strong> LPC:</strong> </h5>
                      <h5><strong> LPC Received: </strong> </h5>                      
                      {{-- <h5>Total Received: {{ round(($received / $booking->paymentPlan->total_price) * 100, 2) }} %</h5> --}}
                      
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-3">
                      <h5><strong>Total Received:</strong> {{ ($received != 0) ? $received - $booking->rebate_amount : 0 }}</h5>
                      <h5> <strong>Rebate Amount: </strong> {{ $booking->rebate_amount }}</h5>
                      <h5><strong>Over Due Amount:</strong> 0</h5>

                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-3" id="right-col" >

                      <h5> <strong>LPC Os:</strong> 0</h5>
                      <h5><strong>Total Os Amount:</strong> 0</h5>
                    </div>
                  </div>
                </div>
                </br>
                <div class="ins-table">
                  <div class="row">
                    <div class="col-xs-12">
                      <button id="printLedger" class="btn btn-success btn-xs">Print</button>
                      @if(!Auth::user()->getPermission('can_download_ledger'))                        
                      <a href="{{ route('booking.ledgerDownload', $booking->id) }}" id="downloadPdf" class="btn btn-success btn-xs">Download Pdf</a>
                      @endif
                      @if(!Auth::user()->getPermission('can_create_followup'))
                          <td><a href="{{ route('followup.show', $booking->registration_no) }}" class="btn btn-default btn-xs">Followup</a></td>
                       @endif
                      <table class="table" style="table-layout:fixed;">
                        <tbody>
                          <tr style="border-bottom: 3px solid;border-top:3px solid;">
                            <th style="border: 2px solid #999;width:108px;">Pay. Decs</th>
                            <th style="border: 2px solid #999;width:45px">Inst#</th>
                            <th style="border: 2px solid #999;width:85px">Due Date</th>
                            <th style="border: 2px solid #999;">Due Amt.</th>
                            <th style="border: 2px solid #999;">Rebate Amt.</th>
                            <th style="border: 2px solid #999;">Recv Amt.</th>
                            <th style="border: 2px solid #999;">Os Amt.</th>
                            <th style="border: 2px solid #999;">Recp No</th>
                            <th style="border: 2px solid #999;">Recp Amt.</th>
                            <th style="border: 2px solid #999;width:85px;">Date</th>
                            <th style="border: 2px solid #999;">Pay Mode</th>
                            <th style="border: 2px solid #999;">Adj Amt.</th>
                            <th style="border: 2px solid #999;">Remaining Bal.</th>
                          </tr>
                          <div class="tbl-body">

                            @php($balance = 0)
                            @php($remainingAmount = 0)
                            @php($received_amt = 0)
                            @php($amountReceived = 0) 
                            @php($receiptAmount = 0)
                            @php($osAmountReceived = 0)
                            @php($received_amt = 0)

                            @php($remaining_balance = $booking->paymentPlan->total_price)

                            {{-- @php(dd($installments)) --}}

                            @foreach($installments as $installment)

                            <?php 


                           
                            if( $installment->payment_desc == 'Balloting')
                            {
                              $balloting = $installment;
                              continue;
                            } 

                           
                            $inst_arr = [];

                              $installment_receipts = App\Payments::getInstallmentReceipts($installment->id);

                              if(count( $installment_receipts) > 0){                                      

                                foreach ($installment_receipts as $receipt) {

                                   $received_amt = ($installment->is_paid == 1 || ( $installment->is_paid == 0  && $installment->amount_received != 0)  )   ? $installment->due_amount - $installment->os_amount : 0; 

                                    $remains = $remaining_balance - $receipt->received_amount;

                                    $remaining_balance =  ($installment->amount_received != 0) ? $remains : '-';

                                  $received_amt1 = $receipt->received_amount; 

                                  // dd($received_amt1);
                                  // $received_amt1 = ($installment->is_paid == 1) ? $receipt->received_amount : 0; 

                                  if(!in_array($installment->installment_no, $inst_arr)){

                                    $inst_arr[] = $installment->installment_no;

                                    $due_amount = number_format($receipt->due_amount);

                                  }else{

                                    $due_amount = "";

                                  }

                            ?>

                            <tr style="border: 2px solid #999; text-align: center">

                              <td style="border: 2px solid #999">{{ $receipt->payment_desc }}</td>

                              <td style="border: 2px solid #999">{{ $installment->installment_no }}</td>

                              <td style="border: 2px solid #999">{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>

                              <td style="border: 2px solid #999">{{ $due_amount }}</td>

                              <td style="border: 2px solid #999">{{ ($receipt->rebat_amount != 0) ? number_format($receipt->rebat_amount) : '-'    }}</td>

                              <td style="border: 2px solid #999">

                                <?php $receiving_amount = ($receipt->os_amount > 0) ? $receipt->due_amount - $receipt->os_amount : $received_amt1; ?>

                                {{ ($receiving_amount != 0) ? number_format($receipt->received_amount) : '-'   }}</td>

                                <td style="border: 2px solid #999">{{ ($receipt->os_amount != 0) ? number_format($receipt->os_amount) : '-'}}</td>

                                <td style="border: 2px solid #999">{{ $receipt->receipt_no or "-" }}</td>

                                <td style="border: 2px solid #999">{{ ($receipt->receipt_amount != 0) ? number_format($receipt->receipt_amount) : '-'  }}</td>

                                <td style="border: 2px solid #999">{{ ($receipt->receipt_date != null) ? date('d-m-Y', strtotime($receipt->receipt_date)): "-"}}</td>

                                <td style="border: 2px solid #999">{{ ($receipt->payment_mode != null) ? $receipt->payment_mode : "-" }}</td>

                                <td style="border: 2px solid #999">{{ ($receipt->received_amount != 0) ? number_format($receipt->received_amount) : '-' }}</td>

                                <td style="border: 2px solid #999">
                                   <?php 
                                    $receiptAmount += $receipt->receipt_amount; 
                                    $amountReceived += $receipt->received_amount; 
                                    $osAmountReceived += $receipt->os_amount; 
                                  ?>
                                  @if($remaining_balance != 0) 

                                    <?php 
                                      $balance = $remaining_balance;
                                      $remainingAmount += $balance;
                                      echo number_format((double)$balance);
                                    ?>
                                  @else
                                    - 
                                  @endif
                                </td>

                              </tr>

                              <?php }} else{?> 

                                <tr style="border: 2px solid #999; text-align: center">

                                  <td style="border: 2px solid #999">{{ $installment->payment_desc }}</td>

                                  <td style="border: 2px solid #999">{{ $installment->installment_no }}</td>

                                  <td style="border: 2px solid #999">{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>

                                  <td style="border: 2px solid #999">{{ number_format($installment->due_amount) }}</td>

                                  <td style="border: 2px solid #999">{{ ($installment->rebat_amount != 0) ? number_format($installment->rebat_amount) : '-'    }}</td>

                                  <td style="border: 2px solid #999">

                                    <?php $receiving_amount = ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $received_amt; ?>
                                  -</td>
                                    {{-- {{ ($receiving_amount != 0) ? number_format($installment->amount_received) : '-'   }}</td> --}}

                                    <td style="border: 2px solid #999">{{ ($installment->os_amount != 0) ? number_format($installment->os_amount) : '-'}}</td>

                                    <td style="border: 2px solid #999">{{ $installment->receipt_no or "-" }}</td>

                                    <td style="border: 2px solid #999">{{ ($installment->receipt_amount != 0) ? number_format($installment->receipt_amount) : '-'  }}</td>

                                    <td style="border: 2px solid #999">{{ ($installment->receipt_date != null) ? date('d-m-Y', strtotime($installment->receipt_date)): "-"}}</td>

                                    <td style="border: 2px solid #999">{{ ($installment->payment_mode != null) ? $installment->payment_mode : "-" }}</td>

                                    <td style="border: 2px solid #999">{{ ($installment->received_amount != 0) ? number_format($installment->received_amount) : '-' }}</td>

                                    <td style="border: 2px solid #999">
                                      -
                                      </td>
                                  </tr>



                               <?php }

                                ?>

                              @endforeach

                              <tr style="border: 2px solid #999; text-align: center">

                                <td style="border: 2px solid #999">{{ $balloting->payment_desc }}</td>

                                <td style="border: 2px solid #999">{{ $balloting->installment_no }}</td>

                                <td style="border: 2px solid #999">{{ date('d-m-Y',strtotime($balloting->installment_date)) }}</td>

                                <td style="border: 2px solid #999">{{ ($balloting->due_amount != 0) ? number_format($balloting->due_amount) : '-'  }}</td>

                                <td style="border: 2px solid #999">{{ ($balloting->rebat_amount != 0) ? number_format($balloting->rebat_amount)  : '-' }}</td>

                                <td style="border: 2px solid #999">


                                  {{ ($balloting->amount_received != 0) ? number_format($balloting->amount_received)  : '-' }}</td>

                                  <td style="border: 2px solid #999">{{ ($balloting->os_amount != 0) ? number_format($balloting->os_amount)  : '-'}}</td>

                                  <td style="border: 2px solid #999">{{ $balloting->receipt_no or "-" }}</td>

                                  <td style="border: 2px solid #999">{{ ($balloting->receipt_amount != 0) ? number_format($balloting->receipt_amount)  : '-' }}</td>

                                  <td style="border: 2px solid #999">{{ ($balloting->payment_date != null) ? date('d-m-Y', strtotime($balloting->payment_date)): "-"}}</td>

                                  <td style="border: 2px solid #999">{{ ($balloting->payment_mode != null) ? $balloting->payment_mode : "-" }}</td>

                                  <td style="border: 2px solid #999">{{ ($balloting->amount_received != 0) ? number_format($balloting->amount_received)  : '-' }}</td>



                                  @php($balloting_remaining_balance = $balloting->due_amount-$remaining_balance)

                                  <td style="border: 2px solid #999">{{ ($balloting_remaining_balance >= 0) ? $balloting_remaining_balance : '-'  }}</td>

                                </tr>

                                

                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;text-align: center">



                                  <h5>Sub Total =></h5></td>



                                  <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0;?>

                                  @foreach($installments as $installment)

                                  <?php 

                                  $due_amount += $installment->due_amount; 

                                    $os_amount += $installment->os_amount; 

                                  $rebat_amount += $installment->rebat_amount;

                                  $amount_received += $installment->amount_received;

                                  $adj_amount += $installment->amount_received;

                                  ?>

                                  @endforeach

                                  <td style="border: 2px solid #999;">{{ ($due_amount != 0) ? number_format($due_amount) : "-"  }}</td>

                                  <td style="border: 2px solid #999;">{{ ($rebat_amount != 0) ? number_format($rebat_amount) : "-" }}</td>

                                  <td style="border: 2px solid #999;">{{ ($amount_received != 0) ? number_format($amount_received) : "-"}}</td>

                                  <td style="border: 2px solid #999;">{{ ($osAmountReceived != 0) ? number_format($osAmountReceived) : "-"}}</td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;">{{ $receiptAmount }}</td>

                                  <td style="border: 2px solid #999;"></td>
                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;">{{ $receiptAmount }}</td>

                                  <td class="text-center" style="border: 2px solid #999" align="center">{{ ($balloting_remaining_balance >= 0) ? $balloting_remaining_balance : number_format((double)$remaining_balance) }}</td>

                                </tr>





                                <tr style="border: 2px solid #999"><td colspan="2" style="border: 2px solid #999;">

                                  <h5>LPC =></h5></td>



                                  <?php $lpc = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>

                                  @foreach($installments as $installment)

                                  <?php 

                                  $lpc += $installment->lpc_charges; 

                                    // $os_amount += $installment->due_amount - $installment->amount_received; 

                                  $rebat_amount += $installment->rebat_amount;

                                  $amount_received += $installment->amount_received;

                                  $adj_amount += $installment->amount_received;

                                  ?>

                                  @endforeach

                                  <td style="border: 2px solid #999;">{{ Carbon\Carbon::today()->format('d-m-Y') }}</td>

                                  <td style="border: 2px solid #999;">{{ ($lpc != 0) ?  number_format($lpc) : "-"  }}</td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>

                                  <td style="border: 2px solid #999;"></td>
                                  {{-- {{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }} --}}
                                  <td style="border: 2px solid #999;"></td>

                                </tr>



                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;">



                                  <h5>Sub Total =></h5></td>



                                  <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>

                                  @foreach($installments as $installment)

                                  <?php 

                                  $due_amount += $installment->due_amount; 

                                    // $os_amount += $installment->due_amount - $installment->amount_received; 

                                  $rebat_amount += $installment->rebat_amount;

                                  $amount_received += $installment->amount_received;

                                  $adj_amount += $installment->amount_received;

                                  ?>

                                  @endforeach



                                  <td style="border: 2px solid #999">{{ ($lpc != 0) ? number_format($lpc) : "-"  }}</td>

                                  <td style="border: 2px solid #999">{{ ($rebat_amount != 0) ? number_format($rebat_amount) : "-"  }}</td>

                                  <td style="border: 2px solid #999">{{ ($amount_received != 0) ?  number_format($amount_received) : "-"}}</td>

                                  <td style="border: 2px solid #999">{{ ($os_amount != 0) ? number_format($os_amount) : "-"}}</td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999">{{ ($adj_amount != 0) ? number_format($adj_amount) : "-" }}</td>
                                  
                                  <td style="border: 2px solid #999" align="center">{{ ($balloting_remaining_balance >= 0) ? $balloting_remaining_balance : number_format((double)$remaining_balance) }}</td>

                                </tr>

                                <tr style="border: 2px solid #999"><td colspan="3" style="border: 2px solid #999;">



                                  <h5>Grand Total =></h5></td>



                                  <td style="border: 2px solid #999">{{ number_format($lpc  + $due_amount) }}</td>

                                  <td style="border: 2px solid #999">{{ number_format($rebat_amount)  }}</td>

                                  <td style="border: 2px solid #999">{{ number_format($amount_received) }}</td>

                                  <td style="border: 2px solid #999">{{ number_format($os_amount) }}</td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999"></td>

                                  <td style="border: 2px solid #999">{{ number_format($adj_amount) }}</td>
                                  {{-- {{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }} --}}
                                  <td style="border: 2px solid #999" align="center">{{ ($balloting_remaining_balance >= 0) ? $balloting_remaining_balance :  number_format((double)$remaining_balance) }}</td>



                                </tr>

                              </div>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      {{-- end --}}



                    </div>
                    <footer id="footer"><img src="{{ asset('public/') }}/images/logo(2).png" style="width:150px;"> © {{ date('Y') }} All Rights Reserved</footer>


                  </div>


                </div>
              </div></div>

<script type="text/javascript">
  function CallPrint(strid)
                {
                  var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_self';

                  var printWindow = window.open(window.location.href , windowName);
                  printWindow.document.body.innerHTML = printContent.innerHTML; 
                  printWindow.focus();
                  printWindow.print();
                  // printWindow.close();
                  return false;
                }

                $('#printLedger').on('click',function(){
                  CallPrint('app');
                })
</script>