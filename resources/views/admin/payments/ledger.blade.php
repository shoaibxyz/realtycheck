@extends('admin.layouts.template')
@section('title','Ledger - '. $booking->registration_no)

@section('bookings-active','active')
@section('booking-active','active')

@section('stylesheet')
<link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css" media="print">
<style type="text/css">

  
  @media print {
    a[href]:after {
      content: none !important;
    }
  }
</style>
@endsection

@section('content')

<div class="content">
    <div class="x_panel">

        <div class="x_title">
            
            @if(isset($word))
    <div id="alert-status" data-field-id="{{$word}}" ></div>
    
    @endif
            <h2>Ledger</h2>
            <div class="actions" style="float: right; display: inline-block">
              <form method="GET" id="ledgerSearch" class="form-horizontal" action="">
                  <div class="row" style="margin:0;">
                    <div class="col-xs-8">
                     @php($registrationArr = [])
                      @php($registrationArr[''] = '')
                      @foreach($bookingList as $bookingArr)
                          @php($registrationArr[$bookingArr->registration_no] = $bookingArr->registration_no)
                      @endforeach
                      {!! Form::select('registration_no', $registrationArr, $booking->registration_no, ['class' => 'form-control', 'placeholder' => 'Select File No', 'id' => 'registration_no']) !!}
                    </div>
                      <div class="col-xs-3">
                        <input type="button" id="btnSearch" value="Search" class="btn btn-warning btn-md">
                      </div>
                </div>
              {!! Form::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
  
        <div id="app">
            @if($booking->is_cancelled == 1)
              <img src="{{ asset('public/')}}/img/watermark-01.png" style="
                  opacity: 1;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                position: absolute;
                z-index: 1;
                background-size: 100%;
                background-repeat: no-repeat;
                background-position: center left;pointer-events: none;width: 100%;
                height: 100%;">
                
                @elseif($booking->is_cancelled == 2 )
              <img src="{{ asset('public/')}}/img/watermark-02.png" style="
                  opacity: 1;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                position: absolute;
                z-index: 1;
                background-size: 100%;
                background-repeat: no-repeat;
                background-position: center left;pointer-events: none;width: 100%;
                height: 100%;">
               @elseif($booking->is_cancelled == 3 )
                
                <img src="{{ asset('public/')}}/img/watermark-03.png" style="
                  opacity: 1;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                position: absolute;
                z-index: 1;
                background-size: 100%;
                background-repeat: no-repeat;
                background-position: center left;pointer-events: none;width: 100%;
                height: 100%;">
                
                @else
                
            @endif
            <div class="container">
      <div class="leadger">
        <div class="loader-image" style="display: none; top: 7%; left: 40%; position:absolute;">
            <img src="{{ asset('public/images/ajax-loader.gif') }}" width="200px" height="200px" style="background-repeat: no-repeat; background-position: center center; ">
          </div>
        <div id="logo_header">
          <div class="row">
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/capital.png"  style="width:180px;"></div>
            <div class="col-xs-2"></div>
            <div class="col-xs-3"><h2 style="margin-top:32px;">Ledger - {{ date('d-m-Y') }}</h2></div>
            <div class="col-xs-1"></div>
            <div class="col-xs-3"><img src="{{ asset('public/') }}/images/Realty-check.png" style="height:90px;float:right;margin-top:10px;"></div>
          </div></div>
          <div class="main">
            <div class="memberinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">

              
              {{-- memberinfo --}}
              <div class="row">
                
                <div class="col-xs-12"><h3><u>Member Information</u></h3></div>
                <div class="col-xs-3">
                  <h5><strong>Member Name: </strong>&nbsp; {{ $person->name }}</h5>
                  <h5> <strong>{{ $person->guardian_type }}:</strong> &nbsp; {{ $person->gurdian_name }}</h5>
                  <h5><strong>Registration No:</strong></strong>   &nbsp; {{ $booking->registration_no }}</h5>
                  <h5> <strong>CNIC No:</strong> &nbsp; 
                  {{ $person->cnic }}
                  </h5>

                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-3">
                  <h5><strong>Current Address:</strong> &nbsp; 
                  {{ $person->current_address }}
                  </h5>
                  <h5><strong>Permanent Address:</strong> &nbsp; 
                  {{ $person->permanent_address }}
                  </h5>
                  <h5> <strong>Mobile No:</strong> &nbsp; 
                  {{ $person->phone_mobile }}
                  </h5>
                  <h5><strong>Form No : </strong>&nbsp; {{ $member->reference_no }}</h5>
                  

                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-3" id="right-col" style="width:3.5cm;height:4.5cm;">
                  
                  <img src="{{ env('STORAGE_PATH').$person->picture }}" alt="" style="width:3.5cm;height:4.5cm;">
                </div>
              </div></div>

              {{-- end --}}
              
              {{-- plotinfo --}}
              
              <div class="plotinfo" style="border-bottom: 2px solid;border-spacing: 10px 10px;">
                <div class="row">
                  
                  <div class="col-xs-12"><h3><u>File/Plot Information</u></h3></div>
                  
                  <div class="col-xs-3">
                    <h5><strong>Size: </strong>{{ $booking->paymentPlan->plotSize->plot_size }}</h5>
                    <h5> <strong>Reference No:</strong> {{ $member->reference_no }}</h5>
                    <h5><strong>Type:</strong> {{ $booking->paymentPlan->plot_nature}}</h5>
                    
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-3">
                       <h5> <strong>Street No: </strong>{{ $booking->street_no }}</h5>
                    <h5><strong>Block Name: </strong>{{ $booking->block_no }}</h5>
                    <h5> <strong>Plot No: </strong>{{ $booking->plot_no }}</h5>
                  
                 
                    
                  </div>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-3" id="right-col" >
                    
                    <h5> <strong>Booking Date: </strong>{{ $booking->created_at->format('d-m-Y') }}</h5>
                    <h5><strong>Payment Code: </strong>{{ $booking->paymentPlan->payment_desc }}</h5>
                  </div>
                </div>
                </div>
                {{-- end --}}

                {{-- paymentdetail --}}
                <div class="payment">
                  <div class="row">
                    <div class="col-xs-12"><h3><u>Payment Detail</u></h3></div>

                    <?php 
                    $received = 0; 
                    $osamount =0;
                    $install_rebate = 0;
                    $due_amount = 0;
                    ?>
                    @foreach($installments as $installment)
                    <?php 
                    /*if($installment->is_paid == 1 || ( $installment->is_paid == 0  && $installment->amount_received != 0)  )  */

                    if($installment->is_paid == 1 || ( $installment->is_paid == 0  && $installment->os_amount != 0)  )  
                      $received += ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $installment->amount_received;
                      //$received += $installment->receipt_amount;
                    $osamount += $installment->os_amount;
                    $install_rebate += $installment->rebat_amount;
                    if($installment->is_paid == 0 and $installment->installment_date < date('Y-m-d'))
                    {
                        if($installment->os_amount > 0)
                            $due_amount += $installment->os_amount;
                        else
                            $due_amount += $installment->installment_amount;
                    }
                    ?>
                    @endforeach
                    <?php
                    $book_rebate = 0;
                    $rebate = 0;
                    if($booking->rebate_amount and $booking->rebate_amount > 0)
                        $book_rebate = $booking->rebate_amount;
                    
                    $rebate = $book_rebate + $install_rebate;
                    //dd($received);
                    $total_os = $booking->paymentPlan->total_price - $received - $rebate;
                    //$received = $received - $book_rebate;
                    ?>
                    
                    <div class="col-xs-3">
                       <h5><strong>Total Amount:</strong> {{ number_format($booking->paymentPlan->total_price) }}</h5>
                      <h5><strong> LPC:</strong> {{ number_format($lpc->lpc_amount) }}</h5>
                      <h5> <strong>Rebate Amount: </strong> {{ number_format($rebate) }}</h5>
                      {{-- <h5>Total Received: {{ round(($received / $booking->paymentPlan->total_price) * 100, 2) }} %</h5> --}}
                      
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-3">
                      <h5><strong>Total Received:</strong> {{ ($received != 0) ? number_format($received) : 0 }}</h5>
                      <h5><strong> LPC Received: </strong> {{ number_format($lpc->amount_received)  }}</h5>
                      <h5><strong>Over Due Amount:</strong> {{ number_format($due_amount) }}</h5>
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-3" id="right-col" >

                      <h5><strong>Total Os Amount:</strong> {{ number_format($total_os) }}</h5>
                        <h5> <strong>LPC Os:</strong> {{ number_format($lpc->lpc_amount - $lpc->amount_received - $lpc->lpc_waive) }}</h5>
                    </div>
                  </div></div>
                </br>
                <div class="ins-table">
                  <div class="row">
                    <div class="col-xs-12">
                      <button id="printLedger" class="btn btn-success" style="{{ $rights->show_print }}">Print</button>
                      @if(!Auth::user()->can('can_download_ledger'))                        
                      <a href="{{ route('booking.ledgerDownload', $booking->id) }}" id="downloadPdf" class="btn btn-warning" style="{{ $rights->show_print }}">Download Pdf</a>
                      @endif
                      @if(!Auth::user()->can('can_create_followup'))
                          <td><a href="{{ route('followup.show', $booking->registration_no) }}" class="btn btn-info" style="{{ $rights->show_print }}" id="followup_b">Followup</a></td>
                       @endif
                      <table class="table" style="table-layout:fixed;">
                        <tbody>
                          <tr style="border-bottom: 3px solid;border-top:3px solid;">
                            <th style="border: 1px solid #999;width:108px;">Pay. Decs</th>
                            <th style="border: 1px solid #999;width:45px">Inst#</th>
                            <th style="border: 1px solid #999;width:85px">Due Date</th>
                            <th style="border: 1px solid #999;">Due Amt.</th>
                            <th style="border: 1px solid #999;">Rebate Amt.</th>
                            <th style="border: 1px solid #999;">Recv Amt.</th>
                            <th style="border: 1px solid #999;">Os Amt.</th>
                            <th style="border: 1px solid #999;">Recp No</th>
                            <th style="border: 1px solid #999;">Recp Amt.</th>
                            <th style="border: 1px solid #999;width:85px;">Date</th>
                            <th style="border: 1px solid #999;">Pay Mode</th>
                            <th style="border: 1px solid #999;">Adj Amt.</th>
                            <th style="border: 1px solid #999;">Remaining Bal.</th>
                          </tr>
                          <div class="tbl-body">

                            @php($balance = 0)
                            @php($remainingAmount = 0)
                            @php($received_amt = 0)
                            @php($amountReceived = 0) 
                            @php($receiptAmount = 0)
                            @php($osAmountReceived = 0)
                            @php($received_amt = 0)
                            @php($balloting = null)
                            @php($balloting_remaining_balance = 0)

                            @php($remaining_balance = $booking->paymentPlan->total_price - $rebate)

                            @foreach($installments as $installment)

                            <?php 

                           
                            $inst_arr = [];

                              $installment_receipts = App\Payments::getInstallmentReceipts($installment->id);

                              if(count( $installment_receipts) > 0){                                                    

                                foreach ($installment_receipts as $receipt) {

                                   $received_amt = ($installment->is_paid == 1 || ( $installment->is_paid == 0  && $installment->amount_received != 0)  )   ? $installment->due_amount - $installment->os_amount : 0; 

                                    $remains = $remaining_balance - $receipt->received_amount;

                                    $remaining_balance =  $remains;

                                  $received_amt1 = $receipt->received_amount; 

                                  // dd($received_amt1);
                                  // $received_amt1 = ($installment->is_paid == 1) ? $receipt->received_amount : 0; 

                                  if(!in_array($installment->installment_no, $inst_arr)){

                                    $inst_arr[] = $installment->installment_no;

                                    $due_amount = number_format($receipt->due_amount);

                                  }else{

                                    $due_amount = "";

                                  }

                            ?>

                            <tr style="border: 1px solid #999; text-align: center">

                              <td style="border: 1px solid #999">{{ $receipt->payment_desc }}</td>

                              <td style="border: 1px solid #999">{{ $installment->installment_no }}</td>

                              <td style="border: 1px solid #999">{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>

                              <td style="border: 1px solid #999">{{ $due_amount }}</td>

                              <td style="border: 1px solid #999">{{ ($receipt->rebate_amount != 0) ? number_format($receipt->rebate_amount) : '-'    }}</td>

                              <td style="border: 1px solid #999">

                                <?php $receiving_amount = ($receipt->os_amount > 0) ? $receipt->due_amount - $receipt->os_amount : $received_amt1; ?>

                                {{ ($receiving_amount != 0) ? number_format($receipt->received_amount) : '-'   }}</td>

                                <td style="border: 1px solid #999">{{ ($receipt->os_amount != 0) ? number_format($receipt->os_amount) : '-'}}</td>

                                <td style="border: 1px solid #999">{{ $receipt->receipt_no }}</td>

                                <td style="border: 1px solid #999">{{ ($receipt->receipt_amount != 0) ? number_format($receipt->receipt_amount) : '-'  }}</td>

                                <td style="border: 1px solid #999">{{ ($receipt->receipt_date != null) ? date('d-m-Y', strtotime($receipt->receipt_date)): "-"}}</td>

                                <td style="border: 1px solid #999">{{ ($receipt->payment_mode != null) ? $receipt->payment_mode : "-" }}</td>

                                <td style="border: 1px solid #999">{{ ($receipt->received_amount != 0) ? number_format($receipt->received_amount) : '-' }}</td>

                                <td style="border: 1px solid #999">
                                  <?php 
                                    $receiptAmount += $receipt->receipt_amount; 
                                    $amountReceived += $receipt->received_amount; 
                                    $osAmountReceived += $receipt->os_amount; 
                                  ?>
                                  @if($remaining_balance != 0) 

                                    <?php 
                                      $balance = $remaining_balance;
                                      $remainingAmount += $balance;
                                      echo number_format((double)$balance);
                                    ?>
                                  @else
                                    - 
                                  @endif
                                </td>

                              </tr>

                              <?php }} else{
                              $end = \Carbon\Carbon::parse($installment->installment_date);
                              $now = \Carbon\Carbon::now();
                              $length = $end->diffInDays($now);
                              $days = floor((strtotime($installment->installment_date) - strtotime(date('Y-m-d'))) / (60*60*24) );
                              ?> 

                                <tr class="{{ ($installment->is_paid == 0 and $days < 0) ? 'overdue_color' : '' }}" style="border: 1px solid #999; text-align: center; {{ ($installment->is_paid == 0 and $days < 0) ? 'background: #e5e5e5;' : '' }}">

                                  <td style="border: 1px solid #999">{{ $installment->payment_desc }}</td>

                                  <td style="border: 1px solid #999">{{ $installment->installment_no }}</td>

                                  <td style="border: 1px solid #999">{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>

                                  <td style="border: 1px solid #999">{{ number_format($installment->due_amount) }}</td>

                                  <td style="border: 1px solid #999">{{ ($installment->rebat_amount != 0) ? number_format($installment->rebat_amount) : '-'    }}</td>

                                  <td style="border: 1px solid #999">

                                    <?php $receiving_amount = ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $received_amt; ?>
                                  -</td>
                                    {{-- {{ ($receiving_amount != 0) ? number_format($installment->amount_received) : '-'   }}</td> --}}

                                    <td style="border: 1px solid #999">{{ ($installment->os_amount != 0) ? number_format($installment->os_amount) : '-'}}</td>

                                    <td style="border: 1px solid #999">{{ $installment->receipt_no }}</td>

                                    <td style="border: 1px solid #999">{{ ($installment->receipt_amount != 0) ? number_format($installment->receipt_amount) : '-'  }}</td>

                                    <td style="border: 1px solid #999">{{ ($installment->receipt_date != null) ? date('d-m-Y', strtotime($installment->receipt_date)): "-"}}</td>

                                    <td style="border: 1px solid #999">{{ ($installment->payment_mode != null) ? $installment->payment_mode : "-" }}</td>

                                    <td style="border: 1px solid #999">{{ ($installment->received_amount != 0) ? number_format($installment->received_amount) : '-' }}</td>

                                    <td style="border: 1px solid #999">
                                      -
                                      </td>
                                  </tr>



                               <?php }

                                ?>

                              @endforeach

                                <tr style="border: 1px solid #999"><td colspan="3" style="border: 1px solid #999;text-align: left">
                                  <h5>Sub Total =></h5></td>
                                  <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; $ballot_amount = 0;
                                  ?>

                                  @foreach($installments as $installment)

                                  <?php 
                                    $ballot_amount = ($balloting and $balloting->receipt_amount != '0') ? $balloting->receipt_amount : 0;
                                  $due_amount += $installment->due_amount; 

                                  $os_amount += $installment->os_amount; 

                                  $rebat_amount += $installment->rebate_amount;

                                  $amount_received += $installment->amount_received;

                                  $adj_amount += $installment->amount_received;

                                  ?>

                                  @endforeach

                                   <td style="border: 1px solid #999;" align="center">{{ ($due_amount != 0) ? number_format($due_amount) : "-"  }}</td>

                                  <td style="border: 1px solid #999;" align="center">{{ ($rebat_amount != 0) ? number_format($rebat_amount) : "-" }}</td>

                                  <td style="border: 1px solid #999;" align="center">{{ ($receiptAmount != 0) ? number_format($receiptAmount + $ballot_amount) : "-"}}</td>

                                  <td style="border: 1px solid #999;" align="center">
                                      <?php
                                      /*$total_inst_os = ($osAmountReceived != 0) ? $osAmountReceived : 0;
                                      $total_ballot_os = ($balloting->os_amount) ? $balloting->os_amount : 0;
                                      $total_os = $total_inst_os + $total_ballot_os;
                                      echo $total_os;*/
                                      echo number_format($os_amount);
                                      ?>
                                  </td>

                                  <td style="border: 1px solid #999;" align="center"></td>

                                  <td style="border: 1px solid #999;" align="center">{{ number_format($receiptAmount + $ballot_amount) }}</td>

                                  <td style="border: 1px solid #999;"></td>
                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;" align="center">{{ number_format($receiptAmount + $ballot_amount) }}</td>

                                  <td class="text-center" style="border: 1px solid #999" align="center">{{ number_format((double)$remaining_balance) }}</td>
                                </tr>
                                @if($lpc and $lpc->lpc_amount > 0)
                                <tr style="border: 1px solid #999">
                                    <td colspan="3" style="border: 1px solid #999;">
                                
                                    <h5><a id="lpc"  data-toggle="modal" title="Click to see detail" href='#lpcModal' data-registration_no="{{ $booking->registration_no }}">LPC </a> </h5></td>
                                
                                    <td style="border: 1px solid #999;" align="center">{{ number_format($lpc->lpc_amount) }}</td>
                                
                                    <td style="border: 1px solid #999;" align="center"> {{  number_format($lpc->lpc_waive) }} </td>
                                
                                    <td style="border: 1px solid #999;" align="center"> {{  number_format($lpc->amount_received) }} </td>
                                
                                    <td style="border: 1px solid #999;" align="center"> {{  number_format($lpc->lpc_amount - $lpc->amount_received - $lpc->lpc_waive) }} </td>
                                
                                    <td style="border: 1px solid #999; text-align:center;">{{ $lpc->receipt_no }}</td>
                                
                                    <td style="border: 1px solid #999; text-align:center;">{{  number_format($lpc->amount_received) }}</td>
                                
                                    <td style="border: 1px solid #999;">{{  ($lpc->receiving_date) ? date('d-m-Y',strtotime($lpc->receiving_date)) : '' }}</td>
                                
                                    <td style="border: 1px solid #999;">{{ $lpc->receiving_mode }}</td>
                                
                                    <td style="border: 1px solid #999; text-align:center;">{{  number_format($lpc->amount_received) }}</td>
                                    
                                    <td style="border: 1px solid #999;"></td>
                                </tr>
                                @endif
                                <?php 
                                $lpc_c=0
                                ?>
                                @for($lpc_c=0;$lpc_c < count($lpcs);$lpc_c++)
                                <tr style="border: 1px solid #999">
                                    <td colspan="3"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="border: 1px solid #999; text-align:center;">{{ $lpcs[$lpc_c]->receipt_no }}</td>

                                      <td style="border: 1px solid #999; text-align:center;">{{  number_format($lpcs[$lpc_c]->received_amount) }}</td>
    
                                      <td style="border: 1px solid #999;">{{  ($lpcs[$lpc_c]->receiving_date) ? date('d-m-Y',strtotime($lpcs[$lpc_c]->receiving_date)) : '' }}</td>
    
                                      <td style="border: 1px solid #999;">{{ $lpcs[$lpc_c]->receiving_mode }}</td>
    
                                      <td style="border: 1px solid #999; text-align:center;">{{  number_format($lpcs[$lpc_c]->received_amount) }}</td>
                                      <td></td>
                                </tr>    
                                @endfor
                                <?php $transfer_charges = 0; ?>
                                @if($transfer and $transfer->received_amount > 0)
                                <?php $transfer_charges = $transfer->tranfer_charges;
                                $transfer_received = $transfer->received_amount;
                                ?>
                                <tr style="border: 1px solid #999">
                                  <td colspan="3" style="border: 1px solid #999;">

                                  <h5>Transfer Charges</h5></td>

                                  <td style="border: 1px solid #999;" align="center">{{ number_format($transfer->tranfer_charges) }}</td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;" align="center"> {{  $transfer->received_amount }} </td>

                                  <td style="border: 1px solid #999;" align="center">  </td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  $transfer->receipt_no }}</td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  $transfer->received_amount }}</td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  $transfer->receiving_date }}</td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  $transfer->receiving_mode }}</td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  $transfer->received_amount }}</td>
                                  
                                  <td style="border: 1px solid #999;"></td>

                                </tr>
                                @endif
                                <?php $fsPayment = 0; ?>
                                @if($preferences and $preferences->amount_received > 0)
                                <?php
                                $fsPayment = $preferences->fst_payment;
                                ?>
                                <tr style="border: 1px solid #999">
                                  <td colspan="2" style="border: 1px solid #999;">

                                  <h5><a id="fst"  data-toggle="modal" title="Click to see detail" href='#fstModal' data-booking_id="{{ $booking->id }}">Plot Pref </a> =></h5></td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;" align="center">{{ number_format($preferences->fst_payment) }}</td>

                                  <td style="border: 1px solid #999;">  </td>

                                  <td style="border: 1px solid #999;" align="center"> {{  number_format($preferences->amount_received) }} </td>

                                  <td style="border: 1px solid #999;" align="center"> {{  number_format($preferences->os_amount) }} </td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999;"></td>

                                  <td style="border: 1px solid #999; text-align:center;">{{  number_format($preferences->amount_received) }}</td>
                                  
                                  <td style="border: 1px solid #999;"></td>

                                </tr>
                                @endif
                                <tr style="border: 1px solid #999"><td colspan="3" style="border: 1px solid #999;">

                                  <h5>Grand Total =></h5></td>

                                  <td style="border: 1px solid #999" align="center">{{ number_format($lpc->lpc_amount + $due_amount + $transfer_charges + $fsPayment) }}</td>

                                  <td style="border: 1px solid #999" align="center">{{ number_format($rebat_amount)  }}</td>

                                  <td style="border: 1px solid #999" align="center">{{ number_format($ballot_amount + $receiptAmount + $lpc->amount_received + $transfer_charges + $preferences->amount_received) }}</td>

                                  <td style="border: 1px solid #999" align="center">{{ number_format($os_amount + ($lpc->lpc_amount - $lpc->amount_received - $lpc->lpc_waive) + $preferences->os_amount) }}</td>

                                  <td style="border: 1px solid #999"></td>

                                  <td style="border: 1px solid #999"></td>

                                  <td style="border: 1px solid #999"></td>

                                  <td style="border: 1px solid #999"></td>

                                  <td style="border: 1px solid #999; text-align:center;" align="center">{{ number_format($receiptAmount + $ballot_amount + $transfer_charges + $lpc->amount_received + $preferences->amount_received ) }}</td>
                                  {{-- {{ ($remaining_balance < 1) ? number_format((double)$remaining_balance) : "-" }} --}}
                                  <td style="border: 1px solid #999" align="center">{{ number_format((double)$remaining_balance) }}</td>
                                </tr>

                              </div>

                              </div>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      {{-- end --}}

                    </div>
                    
                  </div>

                </div>
              </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lpcModal">
    <div class="modal-dialog modal-lg ">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Late Payment Charges</h4>
        </div>
        <div class="modal-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th style="text-align:center;">ID</th>
                <th style="text-align:center;">Inst #</th>
                <th style="text-align:center;">Days</th>
                <th style="text-align:center;">Charges</th>
                <th style="text-align:center;">Received </th>
                <th style="text-align:center;">Rec No </th>
                <th style="text-align:center;">Rec Date </th>
                <th style="text-align:center;">Waive </th>
                <th style="text-align:center;">OS </th>
                <th style="text-align:center;">Remarks </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
<div class="modal fade" id="fstModal">
<div class="modal-dialog modal-lg ">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Plot Preference Charges</h4>
    </div>
    <div class="modal-body">
      <table class="table table-hover">
        <thead>
          <tr>
            <th style="text-align:center;">S#</th>
            <th style="text-align:center;">Preference</th>
            <th style="text-align:center;">Charges</th>
            <th style="text-align:center;">Received </th>
            <th style="text-align:center;">OS </th>
            <th style="text-align:center;">Rec No </th>
            <th style="text-align:center;">Rec Date </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
  
@endsection

@section('javascript')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    
    $('#lpc').click(function(e){
        $('#lpcModal table tbody').html("");
          // e.preventDefault();
          var registration_no = $(this).data('registration_no');
          $.ajax({
            url: '{{ route('lpcByRegistration') }}',
            type: 'POST',
            data: {registration_no: registration_no},
          })
          .done(function(data) {
            var i = 1;
            var total_od = 0;
            var total_rec = 0;
            var total_wav = 0;
            var total_os = 0;
            $.each(data, function(index, val) {
                
              other_amount = (val.lpc_other_amount) ? val.lpc_other_amount : 0;
              var rec_no = (val.receipt_no != null) ? val.receipt_no : '';
              var rec_date = (val.receiving_date != null) ? val.receiving_date : '';
              var lpc = ( parseFloat(val.lpc_amount) + parseFloat(other_amount) ) - val.amount_received - val.inst_waive;
              $('#lpcModal table tbody').append("<tr><td>"+ i++ +"</td><td>"+val.installment_no+"</td><td>"+val.days+"</td><td>" + parseFloat(val.lpc_amount).toFixed(2) +" </td><td>"+ parseFloat(val.amount_received).toFixed(2) +" </td><td>"+ rec_no +" </td><td>"+ rec_date +" </td><td>"+ parseFloat(val.inst_waive).toFixed(2) +" </td><td>"+ parseFloat(lpc).toFixed(2) + "</td><td>"+ val.waive_remarks + "</td></tr>");
              total_od += parseFloat(val.lpc_amount);
              total_rec += parseFloat(val.amount_received);
              total_wav += parseFloat(val.inst_waive);
              total_os += lpc;
            });
            $('#lpcModal table tbody').append("<tr><th colspan='3' style='text-align:left;'>Total</th><th style='text-align:center;'>" + parseFloat(total_od).toFixed(0) +" </th><th style='text-align:center;'>"+ parseFloat(total_rec).toFixed(0) +" </th><th></th><th></th><th style='text-align:center;'>"+ parseFloat(total_wav).toFixed(0) +" </th><th style='text-align:center;'>"+ parseFloat(total_os).toFixed(0) + "</th><th></th></tr>");
          })
          .fail(function() {
            console.log("error");
          });
          
          // alert(registration_no);
    });
    
    $('#fst').click(function(e){
        $('#fstModal table tbody').html("");
          // e.preventDefault();
          var booking_id = $(this).data('booking_id');
          $.ajax({
            url: '{{ route('getFstDetail') }}',
            type: 'POST',
            data: {booking_id: booking_id},
          })
          .done(function(data) {
            var i = 1;
            $.each(data, function(index, val) {
              var rec_no = (val.receipt_no != null) ? val.receipt_no : '';
              var rec_date = (val.receiving_date != null) ? val.receiving_date : '';
              
              $('#fstModal table tbody').append("<tr><td>"+ i++ +"</td><td>" + val.feature +" </td><td>" + val.fst_payment.toFixed(2) +" </td><td>"+ val.amount_received.toFixed(2) +" </td><td>"+ val.os_amount.toFixed(2) +" </td><td>"+ rec_no +" </td><td>"+ rec_date +" </td></tr>");
               /* iterate through array or object */
            });
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          });
    });
    
    $('#btnSearch').on('click', function () {
       $('.loader-image').css('display', 'block');
        var reg = $('#registration_no').val();

        window.location = "/admin/booking/ledger/"+reg;
    });
    
    $('#ledgerSearch').submit(function(e){
      $('.loader-image').css('display', 'block');
      e.preventDefault();
      console.log($(this).serialize());
      $.ajax({
        url: '{{ route('booking.ledger.search') }}',
        type: 'POST',
        data: $(this).serialize(),
      })
      .done(function(data) {
        var reg_no = $('#registration_no').val();

        if(data.word){
            
            alert("This file has been " + data.word);
        }
        var base_url = "{{ URL::to('/') }}";
        window.history.pushState("Ledger", "Ledger " + reg_no,  base_url + '/admin/booking/ledger/'+ reg_no);
        $('.ledgerHtml').html(data.html);
        $('.loader-image').css('display', 'none');
      })
      .fail(function(xhr) {
        console.log(xhr);
      });
      
    }); 
    
    $( window ).on( "load", function() {
        
        var word = $('#alert-status').data("field-id");
        
        if(word != null){
            
            alert( "This file has been " + word );
            
        }else{
            
        console.log(word);
        }
        
    });
    
    
    function CallPrint(strid)
                {
                  var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_blank';

                  /*var printWindow = window.open('Print Ledger' , 'print');
                  printWindow.document.body.innerHTML = printContent.innerHTML;
                  printWindow.document.close();
                  printWindow.focus();
                  printWindow.print();*/
                  //printWindow.close();
                  
                    var WindowObject = window.open('Ledger', 'PrintWindow' , 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
                    WindowObject.document.writeln('<!DOCTYPE html>');
                    WindowObject.document.writeln('<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title></title>');
                    WindowObject.document.writeln('<link href="{{ asset('public/')}}/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
                    WindowObject.document.writeln('<link href="{{ asset('public/')}}/css/custom.min.css" rel="stylesheet">');
                    WindowObject.document.writeln('<link href="{{ asset('public/')}}/css/leadger.css" rel="stylesheet">');
    
                     WindowObject.document.writeln('<link href="{{ asset('public/')}}/css/yellow-grey.css" rel="stylesheet">');
                    WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')
        
                    WindowObject.document.writeln($('#app').html());
        
                    WindowObject.document.writeln('</body></html>');
        
                    WindowObject.document.close();
        
                    WindowObject.focus();
                    //WindowObject.close();
                    
                  return true;
                }
    $('#printLedger').on('click',function(){
      CallPrint('app');
    })
</script>
@endsection
              