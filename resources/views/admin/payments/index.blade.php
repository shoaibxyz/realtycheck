@extends('admin.layouts.template')

@section('title','Booking')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Bookings List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Bookings List</li>
        </ol>
    </div>
     
    <div class="x_panel">
            
        <div class="x_title">
                <h2>Bookings List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('booking.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Booking</a>
                </div>
                <div class="clearfix"></div>
        </div>
        
    <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
     </div>
     
    <div class="row" style="margin:0;">
          <div class="col-sm-12 align-right" style="text-align: right;">
            <form method="POST" id="search-form" class="form-inline align-right" role="form">
                <div class="form-group">
                    <input type="text" name="squery" id="squery" class="form-control ddsearch">
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    
    <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table bulk_action" id="bookingTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Registration</th>
                        <!--<th>Reference</th>-->
                        <th>Plan</th>
                        <!--<th>Plot</th>-->
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        
    </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

        var columns = [
            { data: 'name', name: 'name' },
            { data: 'registration_no', name: 'registration_no' },
            /*{ data: 'reference_no', name: 'reference_no'},*/
            { data: 'plan', name: 'plan'},
            /*{ data: 'plot_size', name: 'plot_size'},*/
            { data: 'total', name: 'total'},
            { data: 'paid', name: 'paid'},
            { data: 'date', name: 'date'},
            { data: 'actions', name: 'actions'},
        ];

        var table = $('#bookingTable').DataTable({
            "processing": true,
            "serverSide": true,
             bFilter: false,
            "pageLength": 10,
            "info": true,
            ajax: {
                url: '{!! route('booking.getBookings') !!}',
                data: function (d) {
                    d.squery = $('#squery').val()
                }
            },
            "columns": columns,
            "columnDefs": [
                // { "width": "10%", "targets": 0 },
                // { "width": "15%", "targets": 2 },
            ]
        });

        $('#search-form').on('keyup', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });

    </script>
@endsection

