@extends('admin.template')
@section('title','Booking Edit')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Edit Booking</h2>
            
            <div class="clearfix"></div>
          </div>   
       
     {!! Form::model($user, ['method' => 'POST', 'route' => ['booking.update', $user->id], 'class' => 'form-horizontal' , 'files' => 'true']) !!}
       
        <div id="data">
            <div class="col-sm-6" style="clear: both">

                <div class="form-group">
                        {!! Form::label('picture', 'Picture', ['class' => 'col-sm-6'])!!}
                        {!! Form::file('picture', null, ['class' => 'form-control']) !!}
                       <div class="col-md-6">
                        <img id="img-preview" src="{{ env('STORAGE_PATH'). $user->picture }}" name="picture"  style="margin: 10px auto;" alt="Picture" height="100" width="200" />
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('cnic_pic', 'CNIC', ['class' => 'col-sm-6'])!!}
                        {!! Form::file('cnic_pic', null, ['class' => 'form-control']) !!}
                       <div class="col-md-6">
                        <a href=""><img id="img-preview" src="{{ env('STORAGE_PATH'). $user->cnic_pic }}" name="cnic_pic"  style="margin: 10px auto;" alt="Picture" height="100" width="200" /></a>
                        </div>
                    </div>
                
                
<div class="col-sm-6">
      <div class="form-group">      
                <input type="hidden" name="_method" value="POST">
                {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
            </div>

               
        </div>

        {!! Form::close() !!}
       
        </div>
@endsection


