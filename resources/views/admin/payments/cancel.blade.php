@extends('admin.layouts.template')
@section('title','Cancel Booking')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <style type="text/css">
        .bottom-row{
            margin: 20px auto;
            border: 1px solid;
            padding: 5px;
        }
        .bottom-row span{
            font-size: bold;
        }
        .bottom-row input{
            width: 70px;
        }
    </style>
@endsection

@section('content')
        
   <div class="x_panel">
        <div class="x_title">
            <h2>Cancel Booking</h2>
            
            <div class="clearfix"></div>
        </div>   

        @include('errors')
          
        <div class="clearfix"></div>
        <form name="cancel_form" method="post" action="{{ url('admin/booking/cancel/' . $reg_no) }}">
            {{ csrf_field() }}
            <input type="hidden" name="reg_no" value="{{ $reg_no }}">
        <div class="row" style="margin-bottom:50px;">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('action', 'Action')!!}
                    <select name="action" id="action" class="form-control">
                        <option value="1">Cancel</option>
                        <option value="2">Refund</option>
                        <option value="3">Merge</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom:20px;">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('date', 'Date')!!}
                    <input type="text" name="cancel_date" id="cancel_date" class="form-control datepicker">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('amount', 'Amount')!!}
                    <input type="text" name="amount" id="amount" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('deduction', 'Deduction')!!}
                    <input type="text" name="deduction" id="deduction" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('remarks', 'Remarks')!!}
                    <textarea name="remarks" id="remarks" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="merge_rows" style="margin-top:20px; display:none;">
            <button type="button" id="add_new" class="btn btn-success" style="font-size:20px;">+</button>
            <div class="row">
                <div class="col-sm-4">
                    {!! Form::label('merge_into', 'Merge Into')!!}
                </div>
                <div class="col-sm-4">
                    {!! Form::label('merge_amount', 'Merge Amount')!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <input type="text" name="merge_into[]" id="merge_into[]" class="form-control">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="merge_amount[]" id="merge_amount[]" class="form-control">
                </div>
            </div>
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-success" style="font-size:20px;">Submit</button>
        </form>
    </div>

@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
            
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        
        $('#action').on('change',function(e){
            if($(this).val() == 3)
                $('.merge_rows').show();
            else
                $('.merge_rows').hide();
        })
        $('#add_new').on('click',function(e){
            var row = '<div class="row" style="margin-top:5px;"><div class="col-sm-4"><input type="text" name="merge_into[]" id="merge_into[]" class="form-control"></div><div class="col-sm-4"><input type="text" name="merge_amount[]" id="merge_amount[]" class="form-control"></div></div>';
            $('.merge_rows').append(row);
        })
        
    </script>
@endsection