@extends('admin.template')
@section('title','Add New Booking')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
        <div class="x_title">
            <h2>Attach preference to booking</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::open(['method' => 'POST', 'route' => 'booking.attachPreference.store', 'class' => 'form-horizontal','files' => true]) !!}
            <div class="form-group">
                {!! Form::label('registration_no', 'Registration No') !!}
                {!! Form::text('registration_no', '', ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('Preference', 'Preference') !!}
                {!! Form::select('featureset_id', ['choose one'], 0, ['class' => 'form-control', 'id' => 'featureset_id', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('installment_no', 'Installment No (This will reflect in customer ledger)') !!}
                {!! Form::text('installment_no', '', ['class' => 'form-control', 'id' => 'installment_no', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('due_amount', 'Due Amount') !!}
                {!! Form::text('due_amount', '', ['class' => 'form-control', 'id' => 'due_amount', 'readonly' => 'readonly', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('installment_date', 'Installment Date') !!}
                {!! Form::text('installment_date', '', ['class' => 'form-control datepicker', 'id' => 'installment_date', 'required' => 'required']) !!} 
            </div>
            <div class="form-group">
                {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>
       
    </div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $('#paymentForm2').submit(function(){
        $('.bookingBtn').attr('disabled', 'disabled');   
        $(this).submit();     
    });

    $('#registration_no').blur(function(){
        var reg = $(this).val();
        var URL = "{{ route('agency.byRegistrationNo') }}";
        $.ajax({
            url: URL,
            type: 'POST',
            data: {reg: reg, is_preference : 1},
            success: function(data){
                if(data.status != 200)
                {
                    swal({
                      title: "Oops!",
                      text: "This registration number does not exist",
                      type: "warning",
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#agency').select2().val("").trigger('change');
                    return;
                }
                $('#featureset_id').html(data.featureset);
                // $('#agency').trigger('change');
            },
            error: function(xhr, status, responseText){
                console.log(xhr);
            }    
        });        
    });

    $(document).on('change','#featureset_id', function(e){
        var registration_no = $('#registration_no').val();

        $.ajax({
            url: '{{ route('booking.getPreferenceAmount') }}',
            type: 'POST',
            data: {registration_no: registration_no, featureset_id : $(this).val() },
            success: function(data){
                if(data.status != 200)
                {
                    swal({
                      title: "Oops!",
                      text: "Something went wrong",
                      type: "warning",
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#agency').select2().val("").trigger('change');
                    return;
                }
                $('#due_amount').val(data.due_amount);
                // $('#agency').trigger('change');
            },
            error: function(xhr, status, responseText){
                console.log(xhr);
            }    
        });        
    });

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });
        
    </script>
@endsection

