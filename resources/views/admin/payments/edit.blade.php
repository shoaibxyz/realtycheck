@extends('admin.layouts.template')
@section('title','Edit Booking')

@section('bookings-active','active')
@section('booking-active','active')

@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<style type="text/css">
    #Installment input{
        width: 130px;
    }
   
</style>
@endsection

@section('content')

@include('admin/common/breadcrumb',['page'=>'Edit Booking'])

<div class="x_panel">
    <?php if( Session::has('message')) : 
    $alertType = ( Session('status') == 1 ) ? "note-success" : "note-danger";
    ?>

    <div class="note {{ $alertType }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session('message') }}
    </div>
    
<?php endif; ?>
<div class="x_title">
    <h2>Edit Booking</h2>
    
    <div class="clearfix"></div>
</div>



<div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#member" aria-controls="member" role="tab" data-toggle="tab">member</a>
        </li>
        <li role="presentation">
            <a href="#booking" aria-controls="tab" role="tab" data-toggle="tab">booking</a>
        </li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="member">
            
            {!! Form::open(['route' => ['users.updateUser', $BookingDetail['person']->id, $BookingDetail['booking']->id], 'files' => 'true', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('_method', 'PATCH') !!}
            <div class="x_panel">
            <div class="x_title">
                <h2>Personal Detail</h2>
                
                <div class="clearfix"></div>
            </div>  


             <div class="col-md-6" style="margin-bottom: 40px;">
                <img src="{{ env('STORAGE_PATH').$BookingDetail['person']->picture }}" width="150" height="100">
                <div style="margin-bottom: 20px;"></div>
                {!! Form::label('picture', "Picture") !!}
                {!! Form::file('picture', ['class' => 'form-control']) !!}
            </div>

            <div class="col-md-6" style="margin-bottom: 40px;">
                <img src="{{ env('STORAGE_PATH').$BookingDetail['person']->cnic_pic }}" width="150" height="100">
                <div style="margin-bottom: 20px;"></div>
                {!! Form::label('cnic', "CNIC") !!}
                {!! Form::file('cnic_pic', ['class' => 'form-control']) !!}
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}<span class="required">*</span>
                    {!! Form::text('name', $BookingDetail['person']->name , array('required' => 'required' , 'class'=>'form-control')  )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('username', 'Username')!!}<span class="required">*</span>
                    {!! Form::text('username', $BookingDetail['person']->username , array('class'=>'form-control', 'required' => 'required')  )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('email', 'Email')!!}<span class="required">*</span>
                    {!! Form::text('email', $BookingDetail['person']->email , array('class'=>'form-control', 'required' => 'required')  )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="ref_pk">Referencce In Pakistan</label>
                    {!! Form::text('ref_pk', $BookingDetail['person']->ref_pk, ['id'=>'imgFile', 'class' => 'form-control']) !!}
                    
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('permanent_address', 'Permanent Address')!!}<span class="required">*</span>
                    {!! Form::text('permanent_address', $BookingDetail['person']->permanent_address , array( 'class'=>'form-control', 'required' => 'required') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('current_address', 'Current Address')!!}<span class="required">*</span>
                    {!! Form::text('current_address', $BookingDetail['person']->current_address , array( 'class'=>'form-control', 'required' => 'required') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('guardian_type', 'S/O or D/O OR W/O')!!}
                    <?php 
                    $guardian_typeArr[''] = "";
                    $guardian_typeArr['S/O'] = 'S/O';
                    $guardian_typeArr['D/O'] = 'D/O';
                    $guardian_typeArr['W/O'] = 'W/O';
                    ?>
                    {!! Form::select('guardian_type', $guardian_typeArr, $BookingDetail['person']->guardian_type, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('gurdian_name', 'Guardian Name')!!}
                    {!! Form::text('gurdian_name', $BookingDetail['person']->gurdian_name , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('nationality', 'Nationality')!!}
                    {!! Form::text('nationality', $BookingDetail['person']->nationality , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('country_id', 'Country')!!}
                    {!! Form::text('country_id', $BookingDetail['person']->country_id , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('city_id', 'City')!!}
                    {!! Form::text('city_id', $BookingDetail['person']->city_id , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('cnic', 'CNIC')!!}<span class="required">*</span>
                    {!! Form::text('cnic', $BookingDetail['person']->cnic , array('required' => 'required', 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('passport', 'Passport No')!!}
                    {!! Form::text('passport', $BookingDetail['person']->passport , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone_office', 'Telephone #')!!}
                    {!! Form::text('phone_office', $BookingDetail['person']->phone_office , array( 'class'=>'form-control') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone_res', 'Phone Res. #')!!}
                    {!! Form::text('phone_res', $BookingDetail['person']->phone_res , array( 'class'=>'form-control') )!!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone_mobile', 'Mobile #')!!}<span class="required">*</span>
                    {!! Form::text('phone_mobile', $BookingDetail['person']->phone_mobile , array( 'class'=>'form-control', 'required' => 'required') )!!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('dateofbirth', 'Date of Birth')!!}
                    {!! Form::text('dateofbirth',$BookingDetail['person']->dateofbirth,  array( 'class'=>'form-control datepicker') )!!}
                </div>
            </div></div>


           
<div class="x_panel">

        <div class="x_title">
            <h2>Nominee Detail</h2>
            
            <div class="clearfix"></div>
          </div>   
        
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_name', 'Nominee Name')!!}
        {!! Form::text('nominee_name', $BookingDetail['member']->nominee_name , array( 'class'=>'form-control') )!!}
        </div></div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_guardian', 'Nominee Guardian')!!}
        {!! Form::text('nominee_guardian', $BookingDetail['member']->nominee_guardian , array('class'=>'form-control') )!!}
        </div>
        </div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('nominee_cnic', 'Nominee CNIC')!!}
        {!! Form::text('nominee_cnic', $BookingDetail['member']->nominee_cnic , array('class'=>'form-control') )!!}
        </div></div>
        <div class="col-sm-6">

        <div class="form-group">
        {!! Form::label('relation_with_member', 'Relation With Member')!!}
        {!! Form::text('relation_with_member', $BookingDetail['member']->relation_with_member , array( 'class'=>'form-control') )!!}
        </div></div>
        <div class="col-sm-6">
            
            <div class="form-group">
            {!! Form::submit('Update Member', ['class' => 'btn btn-default']) !!}
            </div>
        {!! Form::close() !!}   
        </div>

        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="booking">
        <div class="x_panel">
         {!! Form::model($BookingDetail['person'], ['method' => 'PATCH', 'route' => ['booking.updateBookingDetail', $BookingDetail['booking']->id], 'class' => 'form-horizontal']) !!}
            <div class="x_title">
                <h2>Booking</h2>
                
                <div class="clearfix"></div>
            </div>   
            <div class= "row">
        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
                {!! Form::label('registration_no', 'Registration No')!!}
                <span class="required">*</span>
                {!! Form::text('registration_no', $BookingDetail['member']->registration_no , ['class' => 'form-control', 'id' => 'registration_no', 'required'=>'required']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group{{ $errors->has('reference_no') ? ' has-error' : '' }}">
                {!! Form::label('reference_no', 'Reference No')!!}
                <span class="required">*</span>
                {!! Form::text('reference_no', $BookingDetail['member']->reference_no , ['class' => 'form-control', 'id' => 'reference_no', 'required'=>'required']) !!}
            </div>
        </div>


        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('payment_schedule', 'Payment Plan') !!}
                <span class="required">*</span>
                <?php 
                $paymentScheduleArr[''] = "";
                foreach ($paymentSchedule as $plan) {
                    $paymentScheduleArr[$plan->id] = $plan->payment_code;
                } 
                ?>
                {!! Form::select('payment_schedule_id', $paymentScheduleArr, $BookingDetail['booking']->payment_schedule_id, ['class' => 'form-control', 'id' => 'payment_schedule','style' => 'width: 100%', 'required'=>'required']) !!} 
            </div>
        </div>
        </div>
         <div class= "row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('Agency', 'Agency') !!}
                <span class="required">*</span>
                @php($agencyList = [])
                @foreach($agencies as $key => $agency)
                    @php($agencyList[$agency->id] = $agency->name)
                @endforeach

                {!! Form::select('agency_id', $agencyList, $BookingDetail['booking']->agency_id, ['class' => 'form-control', 'required'=>'required']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('payment_remarks', 'Payment Remarks') !!}
                {!! Form::text('payment_remarks', $BookingDetail['booking']->payment_remarks , ['class' => 'form-control','style' => 'width: 100%' ]) !!}
            </div>
        </div>
        
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('block_no', 'Block No') !!}
                {!! Form::text('block_no', $BookingDetail['booking']->block_no , ['class' => 'form-control','style' => 'width: 100%' ]) !!}
            </div>
        </div>
         </div>
         <div class= "row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('plot_no', 'Plot No') !!}
                {!! Form::text('plot_no', $BookingDetail['booking']->plot_no , ['class' => 'form-control','style' => 'width: 100%' ]) !!}
            </div>
        </div>
        
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('street_no', 'Street No') !!}
                {!! Form::text('street_no', $BookingDetail['booking']->street_no , ['class' => 'form-control','style' => 'width: 100%' ]) !!}
            </div>
        </div>
        

        <div class="col-sm-4" >
            <div class="form-group">
                {!! Form::label('feature_set', 'Preferences') !!}
                @php($preference = [])
                @foreach($BookingDetail['booking']->featureset as $featur => $feature)
                    @php($preference[] = $feature->id)
                @endforeach
                <br>
                @foreach($featureset as $featur => $feature)                    
                {{ Form::checkbox('feature_set_id[]', $feature->id, in_array($feature->id, $preference)) }}
                {{ Form::label('feature_set_id', $feature->feature) }}<br>
                @endforeach
            </div>
        </div>
           </div>     

         <div class="row">
    <div class="col-sm-12"> 
             <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#Installment">Payment Plan</a></li>
        </ul>

        <div class="tab-content">
          <div id="Installment" class="tab-pane fade in active">
            <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
            <table class="table table-bordered table-hover table-striped table-condenced">
                <thead>
                    <tr>
                        <th>Inst. No</th>
                        <th>Inst. Date</th>
                        <th>Inst. Amount</th>
                        <th>Rebat Amount</th>
                        <th>Received Amount</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody class="installmentAppend">
                    @if($installments)
                        @foreach($installments as $installment)
                            <tr>
                                <td><input type="text" name="installment_no[]" value="{{ $installment->installment_no}}"></td>
                                <td><input class="datepicker" name="installment_date[]" type="text" value="{{ date('d-m-Y', strtotime($installment->installment_date)) }}"></td>
                                <td><input class="" name="installment_amount[]" type="text" value="{{ $installment->installment_amount}}"></td>
                                <td><input class="" name="rebat_amount[]" type="text" value="{{ $installment->rebate_amount }}"></td>
                                <td><input class="" name="amount_received[]" type="text" value="{{ $installment->amount_received }}"></td>
                                <td><input class="" name="installment_remarks[]" type="text" value="{{ $installment->installment_remarks }}"></td>
                                <input type="hidden" name="installment_id[]" value="{{ $installment->installment_id }}">
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            </div>
         </div>

        </div>


        </div>

    </div>
        <input type="hidden" name="cnic" value="{{ $BookingDetail['person']->cnic }}">
        {!! Form::submit('Update Booking', ['class' => 'btn btn-success pull-right']) !!}
        {!! Form::close() !!}

    </div>

    </div>        
    </div>
    @endsection

    @section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });

        $('#payment_schedule').change(function(event) {
            var planId = $(this).val();
            $.ajax({
                url: '{{ URL::to('/PlanInfo') }}',
                type: 'POST',
                dataType: 'JSON',
                data: {'payment_schedule_id': planId},
                success: function(result){
                    $('#Booking_Price').val(result.booking_price);
                    $('#booking_percentage').val(result.booking_percentage);
                },
                error: function(xhr, status,response){
                    $('#data').html(xhr.responseText);
                }
            }); 
        });

        $('#paymentForm').submit(function(event){
            event.preventDefault();
            var url = '{{ URL::to('/memberInfo') }}';
            var cnic = $('#cnic').val();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {'cnic': cnic},
                success: function(result){
                    var $inputs = $('#data input');
                    var $persons = $('#persons input');

                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#cnic_field').val($('#cnic').val());
                    $('#img-preview').attr('src','../../storage/app/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                        $inputs.filter(function() {
                            return key == this.name;
                        }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                        $inputs.filter(function() {
                            return key == this.name;
                        }).val(value);
                    });

                },
                error: function(xhr, status, response){
                    console.log(response);
                }
            });
        });

        $('#generatePaymentPlan').click(function(e){
        e.preventDefault();
        $('.installmentAppend').html("");
        var payment_schedule_id = $('#payment_schedule').val();

        if(payment_schedule_id == "")
        {
             swal({
              title: "Oops!",
              text: "Please choose payment plan first",
              type: "warning",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
            });
             return;
        }

        $.ajax({
            url: '{{ URL::to('/installmentPlan') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': payment_schedule_id},
            success: function(result){

                var total_inst =  $(result).toArray().length;

                var i = 0;
                var j = 2;
                while(i <= total_inst)
                {
                        var installment = result[i];
                        var splitDate = result[i].installment_date.split("-");

                        if(i > 0)
                        {
                            inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() + 1) + '-' + dateObj.getFullYear();
                            dateObj.addMonths(1);
                        }else{
                             var dateObj = new Date();
                            // dateObj.setMonth(dateObj.getMonth() + i);
                            dateObj.addMonths(2);
                            inst_date = dateObj.getDate()  + '-' + dateObj.getMonth() + '-' + dateObj.getFullYear();

                        }

                        var html = "<tr>"; 
                        html += "<td class='sr_no'><input type='text' name='installment_no[]'  value='"+result[i].installment_no+"'></td>";
                        html += '<td><input class="datepicker2" name="installment_date[]" type="text" value="'+inst_date+'"></td>';
                        html += '<td><input class="" name="installment_amount[]" type="text" value="'+result[i].installment_amount+'"></td>';
                        html += '<td><input class="" name="rebat_amount[]" type="text" value="0"></td>';
                        html += '<td><input class="" name="amount_received[]" type="text" value="0"></td>';
                        html += '<td><input class="" name="installment_remarks[]" type="text" value=""></td>';
                        $('.installmentAppend').append(html);
                    // if(i >= inst) return;
                    i++;
                    j++;
                    // if(payment_frequency == 1)
                    //     payment_frequency++;
                    // if(payment_frequency == 2)
                    //     payment_frequency+=2;
                    // if(payment_frequency == 3)
                    //     payment_frequency+=3;
                }
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
        
     });

    Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
    };

    Date.getDaysInMonth = function (year, month) {
        return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    };

    Date.prototype.isLeapYear = function () { 
        return Date.isLeapYear(this.getFullYear()); 
    };

    Date.prototype.getDaysInMonth = function () { 
        return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
    };

    Date.prototype.addMonths = function (value) {
        var n = this.getDate();
        this.setDate(1);
        this.setMonth(this.getMonth() + value);
        this.setDate(Math.min(n, this.getDaysInMonth()));
        return this;
    };

     $('#specialPlan').on('submit', function(e){
        e.preventDefault();

        var payment_schedule_id = $('#payment_schedule').val();

        if(payment_schedule_id == "")
        {
             swal({
              title: "Oops!",
              text: "Please choose payment plan first",
              type: "warning",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
            });
             return;
        }

        $('.installmentAppend').html("");
        var total_inst = parseInt($('#total_inst').val());

        var payment_frequency = parseInt($('#payment_frequency').val());
        var start_date = $('#start_date').val();

        var total_price = $('#total_price').val();
        var per_inst = parseInt(total_price / total_inst);

        var i = 1;  

        while(i <= total_inst)
        {
                if(i > 1)
                {
                    if(payment_frequency == 3 )
                    {   
                        dateObj.addMonths(payment_frequency);
                        inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() % 12) + '-' + dateObj.getFullYear();
                    }else{
                        inst_date = dateObj.getDate()  + '-' + parseInt(dateObj.getMonth() + payment_frequency) + '-' + dateObj.getFullYear();
                        dateObj.addMonths(payment_frequency);
                    }
                }else{
                    var splitDate = start_date.split("/");
                    var dateObj = new Date(splitDate[2],splitDate[1],splitDate[0]);
                    inst_date = dateObj.getDate()  + '-' + dateObj.getMonth()  + '-' + dateObj.getFullYear();
                }

                var html = "<tr>";  
                html += "<td class='sr_no'><input type='text' name='installment_no[]'  value='"+i+"'></td>";
                html += '<td><input class="datepicker installment_date" name="installment_date[]" type="text" value="'+inst_date+'"></td>';
                html += '<td><input class="" name="installment_amount[]" type="text" value="'+per_inst+'"></td>';
                html += '<td><input class="" name="percentage[]" type="text" value="0"></td>';
                html += '<td><input class="" name="rebat_amount[]" type="text" value="0"></td>';
                html += '<td><input class="" name="amount_received[]" type="text" value="0"></td>';
                html += '<td><input class="" name="installment_remarks[]" type="text" value=""></td>';
                $('.installmentAppend').append(html);
            i++;
            // payment_frequency += payment_frequency;
        }

     });

    </script>
    @endsection

