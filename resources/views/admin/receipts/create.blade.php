@extends('admin.layouts.template')
@section('title','Add Receipt')

@section('bookings-active','active')
@section('receipts-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        #receipt-form label {
            display: flex;
        }
        #receipt-form .select2-container {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
        
   <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Receipt</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Receipt</li>
        </ol>
    </div>
   <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>
        <div class="alert {{ $alertType }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session('message') }}
        </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Add New Receipt</h2>
            <div class="clearfix"></div>
        </div>   
        @include('errors')
        <div class="clearfix"></div>
       
       <form method="POST" action="{{ route('receipt.store') }}" class="form-horizontal submit-once" id="receiptForm">

        @include('admin.receipts._form2')

        <div class="row form-actions text-right pal" style="margin: 30px auto;">
            <a href="#" class="btn btn-warning btn-md" id="populate"> Populate</a>
        </div>
        <div class="row">
            <div class="col-sm-12"> 
             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Installment">Installments</a></li>
              <li><a data-toggle="tab" href="#other_payment">Other Payments</a></li>
              <li><a data-toggle="tab" href="#extra_charges">Extra Charges</a></li>
              <li><a data-toggle="tab" href="#late_payment_charges">Late Payment Charges</a></li>
              <li><a data-toggle="tab" href="#transfer_charges">Transfer Charges</a></li>
            </ul>

            <div class="tab-content">
              <div id="Installment" class="tab-pane fade in active">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Due Amt.</th>
                            <th>Inst. Amt</th>
                            <th>Remaining Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="installmentAppend"></tbody>
                </table>
                </div>
             </div>

              <div id="other_payment" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Due Amt.</th>
                            <th>Inst. Amount</th>
                            <th>Receiving Amount</th>
                            <th>Remaining Amount</th>
                         </tr>
                    </thead>
                    <tbody class="OtherinstallmentAppend"></tbody>
                </table>
                </div>
             </div>
             
              <div id="extra_charges" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Sr#</th>
                            <th>Due Amount</th>
                            <th>Receiving Amount</th>
                            <th>Remaining Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="ExtraChargesAppend"></tbody>
                </table>
                </div>
             </div>
             
            <div id="late_payment_charges" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Sr#</th>
                            <th>Inst No</th>
                            <th>Calculation Date</th>
                            <th>Charges</th>
                            <th>Paid Amount</th>
                            <th>Remaining Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="late_payment_chargesAppend"></tbody>
                </table>
                </div>
             </div>
                <div id="transfer_charges" class="tab-pane fade in">
                    <div class="table-responsive" style="overflow:scroll;">
                        <table class="table table-bordered table-hover jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th>Due Amount</th>
                                    <th>Receiving Amount</th>
                                    <th>Remaining Amount</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody class="TransferChargesAppend"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
        <div class="row form-actions pal" style="margin: 30px auto;">
            {{ csrf_field() }}
            {!! Form::submit('Save', ['class' => 'btn btn-success saveBtn']) !!}
        </div>
        
        </form>

    </div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {

        $('form.submit-once').submit(function(e){
          if( $(this).hasClass('form-submitted') ){
            e.preventDefault();
            return;
          }
          $(this).addClass('form-submitted');
        });
        $('#receiving_type').change(function() {
            if (this.value == 3) {
               $('.both').removeClass('hide');
               $('.received_amount').addClass('hide');
            }
            else{
                $('.both').addClass('hide');
                $('.received_amount').removeClass('hide');
            }
        });
    });

    $('#receiving_mode').change(function(e){
        if($(this).val() == 'online' || $(this).val() == 'po'){
            $('.bank_info').removeClass('hide');
        }else{
            $('.bank_info').addClass('hide');
        }
    });

    var date = new Date();
    date.setDate(date.getDate());
    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: date
    });

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('#registration_no').blur(function(){
        var reg_no = $(this).val();             
        $.ajax({
           url: '{{ route('findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person').val(data.person.name);
                $('#img-preview').attr('src',"../../storage/app/public/"+data.person.picture);             
            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });

    $('#populate').click(function(event) {
        event.preventDefault();
        var reg_no = $('#registration_no').val();
        var received_amount = $('#received_amount').val();
        var inst_amount = $('#inst_amount').val();
        var rebate_amount = $('#rebate_amount').val();
        var other_amount = $('#other_amount').val();
        var receipt_no = $('#receipt_no').val();
        var receiving_type =  $('#receiving_type').val();

        if(reg_no == "" || receipt_no == "")
        {
            swal({
                title: "Oops!",
                text: "Please Enter Registration Number, Receipt No and Receiving Amount",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
            });
             return;
        }
        if(receiving_type == 3)
        {
            if(inst_amount == '' || other_amount == '')
            {
                swal({
                    title: "Oops!",
                    text: "Amounts and Other Amounts are not supplied.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                });
                return;
            }
        }
        else
        {
            if(received_amount == '')
            {
                swal({
                    title: "Oops!",
                    text: "Receiving Amount is not supplied.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                });
                return;
            }
        }
        $('.installmentAppend').html("");
        $('.OtherinstallmentAppend').html("");
        $('.late_payment_chargesAppend').html("");

        if(receiving_type == 1)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type: receiving_type, rebate_amount : rebate_amount},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("Received amount is less than installment amount");
                        $('.OtherinstallmentAppend').html("");
                    }
                    else
                    {
                        $.each(data.installments, function(key, val) {

                            remaining = data.remaining[key];
                            due_amount_total += parseInt(val.due_amount);
                            inst_total += parseInt(val.installment_amount);
                            remaining_amount = parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.installmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";

                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td><input type='text' value='"+inst_total+"' style='background: orange; color: white;' readonly>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.installmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }

        if(receiving_type == 2)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type:receiving_type},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("");
                        $('.OtherinstallmentAppend').html("Received amount is less than installment amount");
                    }
                    else
                    {
                        var receiving=received_amount;
                        var remaining=0;
                        $.each(data.installments, function(key, val) {
                            if(remaining!=0){
                                receiving=remaining;
                            }

                            remaining = data.remaining[key];
                            due_amount_total += parseInt(val.due_amount);
                            inst_total += parseInt(val.installment_amount);

                            remaining_amount = parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                   
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' readonly value='"+receiving+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.OtherinstallmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";
                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td><input type='text' value='"+inst_total+"' style='background: orange; color: white;' readonly>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.OtherinstallmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }

        if(receiving_type == 3)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt2') }}',
                type: 'POST',
                data: {reg_no: reg_no, inst_amount: inst_amount, other_amount: other_amount, receipt_no: receipt_no, receiving_type:receiving_type},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("Received amount is less than installment amount");
                        $('.OtherinstallmentAppend').html("");
                    }
                    else
                    {
                        var last = 0;
                        $.each(data.installments, function(key, val) {

                            if(i == 1)
                                remaining = $('#inst_amount').val();
                            else
                                remaining = last;
                            last = data.remaining[key];
                            
                            remaining_amount = (val.os_amount > 0) ? parseInt(remaining-val.os_amount) : parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                            
                            due_amount_total += parseInt(dueAmount);
                            inst_total += parseInt(remaining);
                            
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                           // html += "<td><input type='text' readonly value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining_amount+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.installmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";

                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.installmentAppend').append(html2);
                    }
                    
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    // return;
                    if(data.installments_other == null)
                    {
                        $('.installmentAppend').html("");
                        $('.OtherinstallmentAppend').html("Received amount is less than installment amount");
                    }
                    else
                    {
                        var last = 0;
                        $.each(data.installments_other, function(key, val) {

                            if(i == 1)
                                remaining = $('#other_amount').val();
                            else
                                remaining = last;
                            last = data.remaining_other[key];

                            remaining_amount = (val.os_amount > 0) ? parseInt(remaining-val.os_amount) : parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                            
                            due_amount_total += parseInt(dueAmount);
                            inst_total += parseInt(remaining);
                            
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no2[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date2[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount2[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount2[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' readonly value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount2[]' value='"+remaining_amount+"'></td>"; 
                           // html += "<td><input type='text' name='remarks2[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id2[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.OtherinstallmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";
                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.OtherinstallmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        } 
        
        if(receiving_type == 4)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type: receiving_type, rebate_amount : rebate_amount},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var rec_amount_total = 0;
                    var inst_total = 0;
                    var paid = 0;
                    var remaining = 0;
                    
                    $('.ExtraChargesAppend').html("");
                    if(data.installments == null)
                    {
                        $('.ExtraChargesAppend').html("");
                    }
                    else
                    {
                        var last = 0;
                        $.each(data.installments, function(key, val) {
                            
                            due_amount_total += parseInt(val.fst_payment);
                            inst_total += parseInt(val.fst_payment);
                            remaining_amount = parseInt(remaining-val.fst_payment);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.fst_payment;
                            var inst_amount = parseFloat(val.fst_payment - val.os_amount).toFixed(2);
                            if(i == 1)
                            {
                                paid = parseFloat($('#received_amount').val());
                            }
                            else
                            {
                                paid = last;
                            }
                            remaining = data.remaining[key];
                            last = data.remaining[key];
                            
                            rec_amount_total += paid;

                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+ dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='paid_amount[]' value='"+paid+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='fst_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.ExtraChargesAppend').append(html);
                            i++;
                        });
                        html2 = "<tr>";
                        html2 += "<td>Total Amount</td>";
                        html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'></td>";
                        html2 += "<td></td>";
                        html2 += "<td></td>";
                        html2 += "<td></td>";
                        html2 += "</tr>";
                        $('.ExtraChargesAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }
        
        //Late Payment Charges
        if(receiving_type == 5)
        {
            //alert(receiving_type);
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type: receiving_type, rebate_amount : rebate_amount},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    var paid = 0;
                    var remaining = 0;
                    
                    if(data.installments == null)
                    {
                        $('.late_payment_chargesAppend').html("");
                    }
                    else
                    {
                        var last = 0;
                        $.each(data.installments, function(key, val) {
                            
                            due_amount_total += parseInt(val.lpc_amount);
                            inst_total += parseInt(val.lpc_amount);
                            remaining_amount = parseInt(remaining-val.lpc_amount);
                            var dueAmount = (val.lpc_os_amount > 0) ? val.lpc_os_amount  : val.lpc_amount;
                            var inst_amount = parseFloat(val.lpc_amount - val.lpc_os_amount).toFixed(2);
                            if(i == 1)
                            {
                                //remaining = parseFloat($('#received_amount').val()) - inst_amount;
                                //if(remaining > 0)
                                    paid = parseFloat($('#received_amount').val());
                                /* else
                                    paid = $('#received_amount').val(); */
                            }
                            else
                            {
                                paid = last;
                            }
                            remaining = data.remaining[key];
                            last = data.remaining[key];

                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.calculation_date+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+ dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='paid_amount[]' value='"+paid+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.installment_id+"'>";
                            html += "<input type='hidden' name='lpc_id[]' value='"+val.lpc_id+"'>";
                            html += "<tr>";
                            $('.late_payment_chargesAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";

                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.late_payment_chargesAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }
        
        //Transfer Charges
        if(receiving_type == 6)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no,receiving_type: receiving_type},
                success: function(data){
                    html = "";
                    $('.TransferChargesAppend').html('');
                    if(data)
                    {
                        var paid = parseFloat($('#received_amount').val());
                        var remaining = data.tranfer_charges - paid;
                        html = "<tr>";
                        html += "<td><input type='text' name='due_amount' readonly value="+ data.tranfer_charges +"></td>"; 
                        html += "<td><input type='text' name='paid_amount' value='"+paid+"' readonly></td>"; 
                        html += "<td><input type='text' name='receiving_amount' value='"+ remaining +"' readonly></td>"; 
                        html += "<td><input type='text' name='remarks' value=''></td>"; 
                        html += "<input type='hidden' name='transfer_id' value='"+data.id+"'>";
                        html += "<tr>";
                    }
                    else
                    {
                        html = "<tr><td colspan=4 style='text-align:center;'><h3>Charges already paid.</h3></td></tr>";
                    }
                    $('.TransferChargesAppend').append(html);
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }
    });


    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ route('memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#img-preview').attr('src','../../storage/app/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
    // $("#receiptForm").validate({
    
    //         rules: {
    //             registration_no: {
    //                 required: true,
    //                 registration_no:true
    //            },
    //             receipt_no:{
    //                 required:true,
    //                 receipt_no:true
    //             },
    //             receiving_mode:{
    //                 required:true,
    //                 receiving_mode:true
    //             },
    //             received_amount:{
    //                 required:true,
    //                 received_amount:true
    //             }
    //         }
    //     });
        
    </script>
@endsection

