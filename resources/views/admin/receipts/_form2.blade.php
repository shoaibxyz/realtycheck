     
        <div id="data">
            
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                <label for="inputEmail3" class="col-sm-5 control-label">Registration No<span class="required">*</span></label>
                <div class="col-sm-7">
                {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no', 'required' => 'required']) !!}
                </div>
                </div>
                </div>
            </div>
            
            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Personal Info</h2>
            
            <div class="row">
                <div id="member"  style="clear: both">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                    <div class="form-group">
                    <img id="img-preview" class="" src="https://docs.moodle.org/27/en/images_en/7/7c/F1.png"  style="margin: 10px auto; height: 100px; " alt="Picture" height="100" />
                    </div>
                    
                    <div class="clearfix"></div>
                    
                </div></div>
                
                
             <div class="col-sm-5">

                <div class="form-group">
                {!! Form::label('person', 'Person')!!}
                {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
                </div>
             </div>
                
            </div>
            
            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Receipt Info</h2>
            
            <h3>Receipt</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Receipt No <span class="required">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('receipt_no', null, ['class' => 'form-control', 'id' => 'receipt_no', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Receiving Date</label>
                        <div class="col-sm-7">
                            {!! Form::text('receiving_date', \Carbon\Carbon::now()->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'receiving_date', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Receiving Mode <span class="required">*</span></label>
                        <div class="col-sm-7">
                            <?php 
                                $receiving_mode['CASH'] = 'cash';
                                $receiving_mode['online'] = 'online';
                                $receiving_mode['po'] = 'po';
                                $receiving_mode['cheque'] = 'cheque';
                            ?>         
                            {!! Form::select('receiving_mode', $receiving_mode, old('receiving_mode'), ['class' => 'form-control', 'id' => 'receiving_mode', 'required' => 'required']) !!}           
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Receiving Type</label>
                        <div class="col-sm-7">
                            <select name="receiving_type" id="receiving_type" class="form-control">
                                <option value="1">Installment</option>
                                <option value="2">Other</option>
                                <option value="3">Both</option>
                                <option value="4">Plot Preference</option>
                                <option value="5">Late Payment Charges</option>
                                <option value="6">Transfer Charges</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Remarks <span class="required">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('receipt_remarks', null, ['class' => 'form-control', 'id' => 'receipt_remarks', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <h3>Payment</h3>
            <div class="row">
                <div class="col-sm-6 received_amount">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Receiving Amount <span class="required">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('received_amount', null, ['class' => 'form-control', 'id' => 'received_amount']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 both hide">
                    <div class="form-group">
                        <label class="col-sm-5 control-label">Installment Amount <span class="required">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('inst_amount', null, ['class' => 'form-control', 'id' => 'inst_amount']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 both hide">
                    <div class="form-group">
                        <label class="col-sm-5 control-label">Other Amount <span class="required">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('other_amount', null, ['class' => 'form-control', 'id' => 'other_amount']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <h3>Discount</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Rebate Amount</label>
                        <div class="col-sm-7">
                            {!! Form::text('rebate_amount', null, ['class' => 'form-control', 'id' => 'rebate_amount']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php 
                        $discountType[''] = '';
                        $discountType['management discount'] = 'management discount';
                        $discountType['promotion discount'] = 'promotion discount';
                        $discountType['rebate discount'] = 'rebate discount';
                    ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Discount Type</label>
                        <div class="col-sm-7">
                            {!! Form::select('discount_type', $discountType, 0, ['class' => 'form-control', 'id' => 'discount_type']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="bank_info hide">
                <h3>Bank</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Transaction/Cheque No</label>
                            <div class="col-sm-7">
                                {!! Form::text('transaction_cheque_no', null , ['class' => 'form-control', 'id' => 'transaction_cheque_no']) !!}  
                            </div>
                        </div>
                        
                         <div class="form-group" id="receipt-form">
                            <label class="col-sm-5 control-label">From Bank</label>
                            <div class="col-sm-7">
                                <?php 
                                $banksArr[''] = "Choose One";
                                $banksArr2[''] = "Choose One";
                                foreach ($banks as $bank) {
                                    $banksArr[$bank->id] = $bank->bank_name;
                                    $banksArr2[$bank->account] = $bank->account.'-'.$bank->short_name;
                                }
                            ?>         
                            {!! Form::text('from_bank', null, ['class' => 'form-control']) !!}  
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-6">
                        
                        <div class="form-group">
                            <label class="col-sm-5 control-label">To Bank</label>
                            <div class="col-sm-7">
                                {!! Form::select('to_bank', $banksArr, old('to_bank'), ['class' => 'form-control']) !!}  
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <label class="col-sm-5 control-label">Deposited in account no</label>
                            <div class="col-sm-7">
                                       
                            {!! Form::select('deposited_in', $banksArr2, old('deposited_in'), ['class' => 'form-control']) !!}  
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>   
