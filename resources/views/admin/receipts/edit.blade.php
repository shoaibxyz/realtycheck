@extends('admin.layouts.template')
@section('title','Edit Receipt')

@section('bookings-active','active')
@section('receipts-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         


         <?php if( Session::has('message')) :
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>
           {{--
            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>

--}}

        <?php endif; ?>
        <div class="x_title">
            <h2>Edit Receipt</h2>
            
            <div class="clearfix"></div>
            @if($receipts->is_cancelled)
                <div class="text-danger">
                    <h4>This receipt is cancelled</h4>
                    <p>Cancel Date: {{ date('d-m-Y', strtotime($receipts->cancel_date)) }}</p>
                    <p>Cancel Remarks: {{ $receipts->cancel_remarks }}</p>
                    <p>Cancelled By: {{ App\User::getUserInfo($receipts->cancel_by)->name }}</p>
                </div>
            @endif
          </div>   

            @include('errors')
        <div class="clearfix"></div>
       
        {!! Form::model($receipts, ['route' => ['receipt.update', $receipts->id ]]) !!}
        {!! Form::hidden('_method', 'PUT') !!}
        {!! Form::hidden('receipt_id', $receipts->id) !!}
             
        <div id="data">
            <div id="member" class="col-sm-6" style="clear: both">

                <div class="form-group">
                {!! Form::label('receipt_no', 'Receipt No')!!}
                {!! Form::text('receipt_no', null, ['class' => 'form-control', 'id' => 'receipt_no']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('registration_no', 'Registration No')!!}
                {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no']) !!}
                </div>

                <div class="form-group">
                {!! Form::label('person', 'Person')!!}
                {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
                </div>
                

              {{--   <div class="form-group">
                {!! Form::label('receiving_date', 'Receiving Date')!!}
                {!! Form::text('receiving_date', null, ['class' => 'form-control datepicker', 'id' => 'receiving_date']) !!}
                </div> --}}

                <div class="form-group">
                    <label>Receiving Type</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="receiving_type" id="installment" value="1" checked="checked">
                            Inst
                        </label>
                        <label>
                            <input type="radio" name="receiving_type" id="other" value="2">
                            Other
                        </label>
                        <label>
                            <input type="radio" name="receiving_type" id="both" value="3">
                            Both
                        </label>
                    </div>                    
                </div>

                <div class="form-group">
                    <label>Receiving Mode</label>
                    <?php 
                        $receiving_mode[''] = "";
                        $receiving_mode['CASH'] = 'cash';
                        $receiving_mode['online'] = 'online';
                    ?>         
                    {!! Form::select('receiving_mode', $receiving_mode, old('receiving_mode'), ['class' => 'form-control']) !!}           
                </div>    

                <div class="form-group received_amount">
                {!! Form::label('received_amount', 'Receiving Amount')!!}
                {!! Form::text('received_amount', null, ['class' => 'form-control', 'id' => 'received_amount']) !!}
                </div>

                <div class="form-group both  hide">
                {!! Form::label('inst_amount', 'Installment Amount')!!}
                {!! Form::text('inst_amount', null, ['class' => 'form-control', 'id' => 'inst_amount']) !!}
                </div>

                <div class="form-group both hide">
                {!! Form::label('other_amount', 'Other Amount')!!}
                {!! Form::text('other_amount', null, ['class' => 'form-control', 'id' => 'other_amount']) !!}
                </div>   

            </div>

            <div class="col-sm-6">

                    <div class="form-group pull-right">
                    <img id="img-preview" class="" src="https://docs.moodle.org/27/en/images_en/7/7c/F1.png"  style="margin: 10px auto;" alt="Picture" height="100" />
                    </div>
                    
                    <div class="clearfix"></div>


            </div>

            <div class="col-sm-12">
                
        
                <div class="form-group">
                {!! Form::label('receipt_remarks', 'Remarks')!!}
                {!! Form::text('receipt_remarks', null, ['class' => 'form-control', 'id' => 'receipt_remarks']) !!}
                </div>

            </div>      

            </div>

        <div class="row">
            <div class="col-sm-12"> 
            <a href="#" class="btn btn-default btn-md pull-right" id="populate"> Populate</a>
            @if(Auth::user()->Roles->first()->name == 'Super-Admin')
             <a href="#" title="Once you cancel this receipt, this receipt number cannot be used any where else." class="btn btn-warning btn-md pull-right" data-id="{{ $receipts->id }}" id="cancel"> Cancel Receipt</a>
             @endif

             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Installment">Installments</a></li>
              <li><a data-toggle="tab" href="#other_payment">Other Payments</a></li>
            </ul>

            <div class="tab-content">
              <div id="Installment" class="tab-pane fade in active">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                    <table class="table table-bordered table-hover table-striped table-condenced">
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Inst. No</th>
                                <th>Inst. Date</th>
                                <th>Due Amt.</th>
                                <th>Inst. Amt</th>
                                <th>Remaining Amt</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody class="installmentAppend">
                            @php($i = 1)
                            @foreach($installments as $installment)
                                @if($installment->is_other_payment == 0)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{!! Form::text('installment_no[]', $installment->installment_no, ['readonly' => 'readonly']) !!}</td>
                                        <td>{!! Form::text('installment_date[]', date('Y-m-d', strtotime($installment->installment_date)), ['readonly' => 'readonly']) !!}</td>
                                        <td>{!! Form::text('due_amount[]', $installment->due_amount, ['readonly' => 'readonly']) !!}</td>
                                        <td>{!! Form::text('installment_amount[]', $installment->installment_amount, ['readonly' => 'readonly']) !!}</td>
                                        {{-- <td>{!! Form::text('receiving_amount[]', $installment->receiving_amount, ['readonly' => 'readonly']) !!}</td> --}}
                                        <td>{!! Form::text('remarks[]', $installment->remarks, ['readonly' => 'readonly']) !!}</td>
                                        <td>{!! Form::hidden('member_installment_id[]', $installment->id, ['readonly' => 'readonly']) !!}</td>
                                    </tr>
                                @endif
                            @php($i++)
                            @endforeach
                        </tbody>
                    </table>
                </div>
             </div>

              <div id="other_payment" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No@</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Inst. Amount</th>
                            <th>Received Amount</th>
                            <th>Remaining Amount</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="OtherinstallmentAppend">
                        @php($j = 1)
                        @foreach($installments as $installment)
                            @if($installment->is_other_payment == 1)
                                <tr>
                                    <td>{{ $j }}</td>
                                    <td>{!! Form::text('installment_no[]', $installment->installment_no, ['readonly' => 'readonly']) !!}</td>
                                    <td>{!! Form::text('installment_date[]', date('Y-m-d', strtotime($installment->installment_date)), ['readonly' => 'readonly']) !!}</td>
                                    <td>{!! Form::text('due_amount[]', $installment->due_amount, ['readonly' => 'readonly']) !!}</td>
                                    <td>{!! Form::text('installment_amount[]', $installment->installment_amount, ['readonly' => 'readonly']) !!}</td>
                                    <td>{!! Form::text('remarks[]', $installment->remarks, ['readonly' => 'readonly']) !!}</td>
                                    <td>{!! Form::hidden('member_installment_id[]', $installment->id, ['readonly' => 'readonly']) !!}</td>
                                </tr>
                            @endif
                        @php($j++)
                        @endforeach
                    </tbody>
                </table>
                </div>
             </div>

            </div>

        </div>
        </div>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
    
        {!! Form::submit('Save', ['class' => 'btn btn-success pull-right']) !!}
    </div>
    {!! Form::close() !!}

</div>
        </div>


        <div class="modal fade" id="ReceiptModel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Cancel Receipt</h4>
                    </div>
                     {!! Form::open(['id' => 'receiptCancelForm']) !!}
                    <div class="modal-body">
                        <h5>Are you sure you want to cancel this receipt?</h5>
                        {!! Form::hidden('receipt_id', null, ['id' => 'receipt_id']) !!}
                        <div class="form-group">
                            {!! Form::label('remarks') !!}
                            {!! Form::textarea('remarks', null, ['class' => 'form-control', 'id'=> 'remarks']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="saveReceiptCancel" >Save changes</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $(document).on('click','#cancel',function(e){
                e.preventDefault();
                $('#receipt_id').val($(this).data('id'));
                $('#remarks').val("");
                $.ajax({
                    url: '{{ route('receipt.getCancelDetail') }}',
                    type: 'POST',
                    data: {receipt_id: $('#receipt_id').val()},
                })
                .done(function(data) {
                    $('#remarks').val(data.result.cancel_remarks);
                    $('#ReceiptModel').modal('show');
                })
                .fail(function() {
                    console.log("error");
                });
            }); 

            $('#receiptCancelForm').submit(function(e){
                e.preventDefault();
                $.ajax({
                    url: '{{ route('receipt.cancel') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                })
                .done(function(data) {
                    if(data.result == true)
                        swal("Success!", data.message, "success");
                    else
                        swal("Error!", data.message, "error");
                    $('#ReceiptModel').modal('hide');

                    window.location.reload();

                })
                .fail(function() {
                    console.log("error");
                });
            });
        });

    var date = new Date();
    date.setDate(date.getDate());
    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: date
    });
    
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('#registration_no').blur(function(){
        var reg_no = $(this).val();             
        $.ajax({
           url: '{{ URL::to('/findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person').val(data.person.name);
                $('#img-preview').attr('src',"../../../public/images/"+data.person.picture);             
            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });

    $('#populate').click(function(event) {
        event.preventDefault();
        var reg_no = $('#registration_no').val();
        var received_amount = $('#received_amount').val();
        var inst_amount = $('#inst_amount').val();
        var other_amount = $('#other_amount').val();
        var receipt_no = $('#receipt_no').val();
        var receiving_type =  $('input[name=receiving_type]:checked').val();

        if(reg_no == "" || receipt_no == "")
        {
            swal({
                title: "Oops!",
                text: "Please Enter Registration Number, Receipt No and Receiving Amount",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
            });
             return;
        }
        $('.installmentAppend').html("");
        $('.OtherinstallmentAppend').html("");

        if(receiving_type == 1)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type: receiving_type, edit : 1},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("Received amount is less than installment amount");
                        $('.OtherinstallmentAppend').html("");
                    }
                    else
                    {
                        $.each(data.installments, function(key, val) {

                            remaining = data.remaining[key];
                            due_amount_total += val.due_amount;
                            inst_total += val.installment_amount;

                            remaining_amount = parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.installmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";

                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td><input type='text' value='"+inst_total+"' style='background: orange; color: white;' readonly>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.installmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }

        if(receiving_type == 2)
        {
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, amount: received_amount, receipt_no: receipt_no,receiving_type:receiving_type},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("");
                        $('.OtherinstallmentAppend').html("Received amount is less than installment amount");
                    }
                    else
                    {
                        $.each(data.installments, function(key, val) {

                            remaining = data.remaining[key];
                            due_amount_total += val.due_amount;
                            inst_total += val.installment_amount;

                            remaining_amount = parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;

                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.OtherinstallmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";
                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td><input type='text' value='"+inst_total+"' style='background: orange; color: white;' readonly>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.OtherinstallmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }

        if(receiving_type == 3)
        {
            alert();
            $.ajax({
                url: '{{ route('InstallmentReceipt') }}',
                type: 'POST',
                data: {reg_no: reg_no, inst_amount: inst_amount, other_amount: other_amount, receipt_no: receipt_no, receiving_type:receiving_type},
                success: function(data){
                    var i = 1;
                    var due_amount_total = 0;
                    var inst_total = 0;
                    // return;
                    if(data.installments == null)
                    {
                        $('.installmentAppend').html("");
                        $('.OtherinstallmentAppend').html("Received amount is less than installment amount");
                    }
                    else
                    {
                        $.each(data.installments, function(key, val) {

                            remaining = data.remaining[key];
                            due_amount_total += val.due_amount;
                            inst_total += val.installment_amount;

                            remaining_amount = parseInt(remaining-val.installment_amount);
                            var dueAmount = (val.os_amount > 0) ? val.os_amount  : val.due_amount;
                            html = "<tr>";
                            html += "<td>"+i+"</td>"; 
                            html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                            html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                            html += "<td><input type='text' name='due_amount[]' readonly value='"+dueAmount+"'></td>"; 
                            html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                            html += "<td><input type='text' name='receiving_amount[]' value='"+remaining+"'></td>"; 
                            html += "<td><input type='text' name='remarks[]' value=''></td>"; 
                            html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                            html += "<tr>";
                            $('.OtherinstallmentAppend').append(html);
                            i++;
                        });
                            html2 = "<tr>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>&nbsp;</td>";
                            html2 += "<td>Total Amount</td>";
                            html2 += "<td><input type='text' readonly value='"+due_amount_total+"' style='background: orange; color: white;'>";
                            html2 += "<td><input type='text' value='"+inst_total+"' style='background: orange; color: white;' readonly>";
                            html2 += "<td></td>";
                            html2 += "<td></td>";
                            html2 += "</tr>";
                            $('.OtherinstallmentAppend').append(html2);
                    }
                },
                error: function(xhr, status, response){
                    console.log(xhr);
                }
            });
        }        
    });


    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#img-preview').attr('src','../../public/images/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

