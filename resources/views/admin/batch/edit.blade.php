@extends('admin.layouts.template')

@section('title','Edit Batch')

@section('bookings-active','active')
@section('agencies-active','active')
@section('batch-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Batch</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Batch</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($batch, ['method' => 'PATCH', 'route' => ['batch.update', $batch->id], 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Batch No <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('batch_no', null, ['class' => 'form-control', 'id' => 'batch_no', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Batch Assign Date <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('batch_assign_date', null, ['class' => 'form-control datepicker', 'id' => 'batch_assign_date', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">File Qty <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('file_qty', null, ['class' => 'form-control', 'id' => 'file_qty', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Agency <span class="required">*</span></label>
                        <div class="col-sm-8">
                        <?php
                            $agencyArr[''] = "";
                            foreach ($agencies as $agency) {
                                $agencyArr[$agency->id] = $agency->name;
                            }
                        ?>
                            {!! Form::select('agency_id', $agencyArr, old('agency_id'), ['class' => 'form-control', 'id' => 'agent', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('batch.index') }}" class="btn btn-grey">Cancel</a>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection



@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });
    </script>
@endsection
