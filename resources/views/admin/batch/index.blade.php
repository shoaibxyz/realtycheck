@extends('admin.layouts.template')

@section('title','Batches List')

@section('bookings-active','active')
@section('agencies-active','active')
@section('batch-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Batches List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Batches List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Batches List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('batch.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th width="5%">#Id</th>
                            <th width="30%">Agency Name</th>
                            <th width="15%">Batch No</th>
                            <th width="20%">Batch Assign Date</th>
                            <th width="15%">Total Files</th>
                            <th width="20%" class="text-center">Actions</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @php($count = 0)
                        @foreach($batches as $batch)
                            <tr>
                                
                                <td>{{ $batch->id }}</td>
                                <td>{{ $batch->agency->name }}</td>
                                <td>{{ $batch->batch_no }}</td>
                                <td>{{ date('d-m-Y', strtotime($batch->batch_assign_date)) }}</td>
                                <td>{{ $batch->file_qty }}</td>
                                <td class="text-center"><a href="{{ route('batch.edit', $batch->id )}}" class="btn btn-info btn-xs" style="{{ $rights->show_edit }}"><i class="fa fa-edit"></i></a>
                                
                                    @if($rights->can_delete)
                                    {!! Form::open(['style'=>'display:inline;', 'method' => 'DELETE', 'route' => ['batch.destroy', $batch->id]]) !!}
                                        <button class="btn btn-danger btn-xs" type="submit" value="" style="{{ $rights->show_delete }}" title="delete"><i class="fa fa-trash-o"></i></button>
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @php($count += $batch->file_qty)
                        @endforeach
                    </tbody>
                    <tfoot>
                        <td colspan="4">Total</td>
                        <td colspan="3">{{ $count }}</td>
                    </tfoot>
                </table>
            </div>
    </div>
    </div>
    </div>
@endsection
