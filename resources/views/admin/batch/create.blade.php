@extends('admin.layouts.template')

@section('title','Add Batch')

@section('bookings-active','active')
@section('agencies-active','active')
@section('batch-active','active')


@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Batch</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Batch</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Batch</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'route' => 'batch.store', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Batch No <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('batch_no', '', ['class' => 'form-control', 'id' => 'batch_no', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Batch Assign Date <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('batch_assign_date', '', ['class' => 'form-control datepicker', 'id' => 'batch_assign_date', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">File Qty <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('file_qty', '', ['class' => 'form-control', 'id' => 'file_qty', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Agency <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <?php
                                $agencyArr[''] = "";
                                foreach ($agencies as $agency) {
                                    $agencyArr[$agency->id] = $agency->name;
                                }
                            ?>
                            {!! Form::select('agency_id', $agencyArr, 0, ['class' => 'form-control', 'id' => 'agency', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <!--<div class="form-group">-->
                    <!--    <label for="inputEmail3" class="col-sm-3 control-label">Main Batch</label>-->
                    <!--    <div class="col-sm-8">-->
                    <!--        <select class="form-control" name="parent_id" id="parent_id">-->
                    <!--            <option value="">Select Agency</option>-->
                    <!--        </select>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('batch.index') }}" class="btn btn-grey">Cancel</a>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
 $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = "{{ route('agency.parentBatch') }}";
        $('#agency').on('change', function(){
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    'agency_id': $(this).val()
                },
                success: function(data){
                    $('#parent_id').html(data);
                },
                error: function(xhr, status, responseText){
                    console.log(xhr);
                }
            });
        });
    });
    </script>
@endsection
