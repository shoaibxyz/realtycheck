@extends('admin.layouts.template')

@section('title','Dashboard')

@section('home-active','active')


@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
  <style>
      #sum_box h4 span:last-child {
    font-size: 15px;
}
#sum_box .icon {
    font-size: 40px;
}

  </style>
@endsection

@section('content')
    
{{-- Include a dynamic file --}}
@include('admin/common/breadcrumb',['page'=>'Leads Dashboard'])
    

    <div class="content">
        <div id="sum_box" class="row mbl" style="margin-top:20px;">
            
            @if(App\Role::getrights('sources'))
               <a  href="{{ url('admin/lead/sources') }}">
             <div class="col-sm-3 col-md-3">
                <div class="panel profit db mbm" style="background-color:olive;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-soundcloud"></i></p>
                        <h4 class="value "><span data-counter="" data-start="10" data-end="50" data-step="1"
                        data-duration="0"></span><span>Sources</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
              @if(App\Role::getrights('campaign'))
               <a  href="{{ url('admin/lead/campaign') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel visit db mbm" style="background-color:yellowgreen;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa  fa-envelope"></i></p>
                        <h4 class="value "><span data-counter="" data-start="10" data-end="50" data-step="1"
                        data-duration="0"></span><span>Campaign</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('projects'))
             <a  href="{{ url('admin/lead/projects') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel income db mbm" style="background-color:slategray ;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-star"></i></p>
                        <h4 id="t_vendors" class="value"><span>Projects</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
            
            @if(App\Role::getrights('leads'))
             <a  href="{{ route('leads.index') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel task db mbm" style="background-color:slategray;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-users"></i></p>
                        <h4 id="t_businesses" class="value"><span>Leads</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('reminder')->can_view)
            <a  href="{{ route('reminder.index') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel agent db mbm" style="background-color:palevioletred;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-clipboard"></i></p>
                        <h4 id="t_businesses" class="value"><span>Reminder</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('advance')->can_view)
             <a  href="{{ route('lead.search') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:mediumpurple;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-search"></i></p>
                        <h4 id="t_businesses" class="value"><span>Advanced Search</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('closelead')->can_view)
            <a href="{{ route('closed.dashboard') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:cornflowerblu;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-trash-o"></i></p>
                        <h4 id="t_businesses" class="value"><span>Closed Leads</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('openlead')->can_view)
            <a href="{{ route('lead.leadStatus',1) }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:darkcyan;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa  fa-folder-open"></i></p>
                        <h4 id="t_businesses" class="value"><span>Open Leads</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('deletedlead')->can_view)
             <a href="{{ route('lead.deleted') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:rosybrown;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon  fa fa-trash-o"></i></p>
                        <h4 id="t_businesses" class="value"><span>Deleted Leads</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
             @if(App\Role::getrights('tokenreceived')->can_view)
            <a href="{{ route('lead.leadStatus',10) }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:slategray;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-file"></i></p>
                        <h4 id="t_businesses" class="value"><span>Token Received</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
           @if(App\Role::getrights('activityreport')->can_view)
              <a href="{{ route('lead.activityReport') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:palevioletred;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-file"></i></p>
                        <h4 id="t_businesses" class="value"><span>Activity Report</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
            @if(App\Role::getrights('import')->can_view)
              
             <a href="{{ route('lead.import') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:olive;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-upload"></i></p>
                        <h4 id="t_businesses" class="value"><span>Import</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
                @if(App\Role::getrights('clients')->can_view)
              
             <a href="{{ route('clients.index') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:slategray;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-user"></i></p>
                        <h4 id="t_businesses" class="value"><span>Clients</span></h4>
                    </div>
                </div>
            </div></a>
            
            <a href="{{ route('clients.search') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:mediumpurple;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-search"></i></p>
                        <h4 id="t_businesses" class="value"><span>Clients Advanced Search</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
            @if(App\Role::getrights('deletedclients')->can_view)
             <a href="{{ route('clients.deleted') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:darkcyan;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-trash-o"></i></p>
                        <h4 id="t_businesses" class="value"><span>Deleted Clients</span></h4>
                    </div>
                </div>
            </div></a>
             @endif
             
             @if(App\Role::getrights('Duemeetings')->can_view)
             <a href="{{ route('lead.dueMeetings') }}">
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm" style="background-color:rosybrown;">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-phone"></i></p>
                        <h4 id="t_businesses" class="value"><span>Due Meetings</span></h4>
                    </div>
                </div>
            </div></a>
            @endif
            
        </div>
    </div>
        
        
        @endsection


@section('javascript')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

  <script type="text/javascript">
  $(function() {
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    });
    function getGraph(startDate = null, endDate = null){

        $.ajax({
          url: '{{ route('dashboardGraph') }}',
          type: 'GET',
          dataType: 'JSON',
          data: {startDate: startDate, endDate : endDate},
        })
        .done(function(data) {

            var obj = data.installments_received;
            var bookingIncome = data.booking_income;
            var ballotingReceived = data.ballotingReceived;

            Highcharts.chart('container', {
            chart: {
                  type: 'areaspline',
                  color: '#000'
              },
            title: {
                text: 'Bookings & Installment Revenue'
            },

            legend: {
                    layout: 'horizontal',
                    align: 'center',
                    lineColor: '#1C3146',
                    verticalAlign: 'bottom',
                    borderWidth: 0
                },
            yAxis: {
                title: {
                    text: 'Amount in PKR'
                }
            },
            xAxis: {
                categories: $.each(obj.time, function(key, value) {
                               value
                            })
            },
             rangeSelector: {
                    allButtonsEnabled: true,
                    selected: 2
                },
            tooltip: {
                shared: true,
                // valueSuffix: ' units'
            },
            series: [
            {
                name: 'Installments Received',
                data: $.each(obj.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Booking Received',
                data: $.each(bookingIncome.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Balloting Received',
                data: $.each(ballotingReceived.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Booked Files',
                data: data.bookings
            }]
          });
          })
          .fail(function() {
            console.log("error");
          });
    }

    getGraph();

    // $('#dashboardGraphRange').submit(function(e){
    //   e.preventDefault();

    // });


    $('.reportrange').daterangepicker(
          {
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                  'Last 7 Days': [moment().subtract('days', 6), moment()],
                  'Last 30 Days': [moment().subtract('days', 29), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment().subtract('days', 29),
              endDate: moment(),
              opens: 'left'
          },
          function(start, end) {
              $('.reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
              $('input[name="startdate"]').val(start.format("YYYY-MM-DD"));
              $('input[name="enddate"]').val(end.format("YYYY-MM-DD"));

              var startDate = $('input[name="startdate"]').val();
              var endDate = $('input[name="enddate"]').val();

              getGraph(startDate, endDate);
          }
      );

});


</script>

    @endsection
