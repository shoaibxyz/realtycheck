@extends('admin.layouts.template')
@section('title','Deleted Leads')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Deleted Leads</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Deleted Leads</li>
        </ol>
    </div>
    <div class="content">
<div class="x_panel">
    <div class="x_title">
        <h2>Deleted Leads</h2>
          <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
        <div class="clearfix">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Project</th>
                    <th>Deleted By</th>
                    <th>Added Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ ($lead->lead_status_id )  ? $lead->status->name : "" }}</td>
                  <td>{{ $lead->user->name  }}</td>
                  <td>{{ $lead->user->phone_mobile }}</td>
                  <td>{{ ($lead->project_id) ? $lead->project->name : "" }}</td>
                  <td>{{ ($lead->updated_by) ? \App\User::getUserInfo($lead->updated_by)->name : "" }}</td>
                  <td>{{ date('d-m-Y', strtotime($lead->created_at)) }}</td>
                  <td>
                        <form method="post" action="{{ route('lead.restore', $lead->id) }}" style="display: inline;">{{ csrf_field() }} 
                        
                        <button type="submit" class="btn btn-danger btn-xs DeleteBtn restore" data-toggle="tooltip" title="Restore"><i class="fa fa-undo"></i></button>
                        </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $(document).on('click','.restore',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "This lead will be restored.",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, restore it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your lead has been restored.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your lead is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
