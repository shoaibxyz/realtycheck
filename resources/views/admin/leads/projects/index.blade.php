@extends('admin.layouts.template')
@section('title','Projects')

@section('content')


<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
            <div class="page-title">Projects List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Projects List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Projects List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.projects.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>    
            <div class="row">
                <div class="col-lg-12">
                    <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                        <h4 class="box-heading">Success</h4>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
    	        <table class="table table-bordered table-hover jambo_table bulk_action">
    		<thead>
    			<tr>
                    <th>ID</th>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Developer</th>
                    <th>Land</th>
                    <th>Architect</th>
                    <th class="text-center">Actions</th>
                </tr>
    		</thead>
    		<tbody>
    			@php($i = 1)
                @foreach($projects as $project)
	    			<tr>
	    				<td>{{ $i++ }}</td>
                        <td><img src="{{ env('STORAGE_PATH1') .  $project->logo }}" class="img-responsive" style="width:60px;height:60px;"></td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->developer }}</td>
                        <td>{{ $project->land }}</td>
                        <td>{{ $project->architect }}</td>
                        <td class="text-center">
                        @if($rights->show_edit)
                            <a href="{{ route('leads.projects.edit', $project->id) }}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                        @endif
                        @if($rights->show_delete)
                        {!! Form::open(['method' => 'DELETE', 'route' => ['leads.projects.destroy', $project->id] , 'id' => 'table_form']) !!}
                            <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                            {{-- {!! Form::submit("<p>a</p>", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!} --}}
                        {!! Form::close() !!}   
                        @endif
                        </td> 
                    </tr>
	    		@endforeach
    		</tbody>
    	</table>	
    	    </div>
	    </div>
	</div>
@endsection


