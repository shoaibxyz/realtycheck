@extends('admin.layouts.template')
@section('title', 'Edit Project')

@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection


@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Project</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Project</li>
        </ol>
    </div>
     <div class="content">

         <div class="x_panel">
        <div class="x_title">
            <h2>Edit Project</h2>
	  
            <div class="clearfix"></div>
          </div>   
	<div class="row"> 

		<div class="col-sm-6 col-sm-offset-3" style="padding-top:40px;">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif    
			{!! Form::model($project, ['method' => 'POST', 'route' => ['leads.projects.update', $project->id], 'files' => 'true', 'class' => 'form-horizontal'  , 'id' => 'validate_form']) !!}
			<div class="form-group">
               <label for="label" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                <div class="col-sm-9">
					{!! Form::text('name', null , array('required' => 'required' , 'class'=>'form-control')  )!!}
				</div></div>
				<div class="form-group">
                {!! Form::label('developer', 'Developer', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
						{!! Form::text('developer', null , array('class'=>'form-control')  )!!}
					</div>
				</div>
				<div class="form-group">
                <label for="label" class="col-sm-3 control-label">Logo</label>
                <div class="col-sm-9">
						{!! Form::file('logo', ['class' => 'form-control']) !!}
					</div>
				</div>
					<div class="form-group">
                {!! Form::label('land', 'Land', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
						{!! Form::number('land', null , array('class'=>'form-control')  )!!}
					</div>
				</div>
					<div class="form-group">
                {!! Form::label('architect', 'Architect', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
						{!! Form::text('architect', null , array('class'=>'form-control')  )!!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-9">
					<img src="{{ env('STORAGE_PATH') .  $project->logo }}" width="60" height="60" class="img-responsive">
					</div></div>
				
				<div class="col-sm-12">   
        
            <div class="form-actions">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                <a href="{{ url('admin/lead/projects') }}" class="btn btn-grey">Cancel</a>
            </div></div>
				{!! Form::close() !!}
			</div></div>
		</div>
		@endsection

@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
    
    	$("#validate_form").validate({
			rules: {
				name: {
					required: true,
					
                    
				},
				
				

				
				logo: {
					required: true,
					accept: 'image/*'
				},
				

			}
		});

		
    </script>
@endsection