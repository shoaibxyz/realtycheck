@extends('admin.layouts.template')
@section('title','Followup & Metting')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Followups & Mettings'])

      <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Followups & Mettings</h2>
                
                
                <div class="clearfix"></div>
            </div>
       
        {{-- @php(dd($input['from_date'])); --}}
        {!! Form::open(['method' => 'post', 'route' => 'leads.getDueMeetings', 'files' => 'true', 'class' => 'form-horizontal', 'id'=>'getMeetingsForm']) !!}

         <div class="col-sm-3"> 
         
        
         
            <div class="form-group">
                {!! Form::label('from_date', 'From date') !!}
                
                {!! Form::text('from_date', (isset($input['from_date'])) ? $input['from_date'] : "", ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'from_date']) !!} 
            </div>
        </div>

        <div class="col-sm-3">   
            <div class="form-group">
                {!! Form::label('to_date', 'To date') !!}
                {!! Form::text('to_date', (isset($input['to_date'])) ? $input['to_date'] : "", ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'to_date']) !!} 
            </div>
        </div>
        
        <div class="col-sm-3">   
            <div class="form-group">
                {!! Form::label('User', 'User id') !!}
                <?php 
                    $agentsArr[''] = 'Choose one';
                    foreach ($sale_officers as $key => $a) {
                        if($a->user){
                            $agentsArr[$a->user->id] = $a->user->name . ' - Agents';
                        }
                    }

                    
                ?>
                {!! Form::select('user', $agentsArr, 0, ['class' => 'form-control', 'id' => 'user' , 'required' => 'required']) !!} 
            </div>
            
            <input type="hidden" id="hidden_user" 
value="@if(isset($hidden_id)){{$hidden_id}}@endif" />
        </div>
        

        <div class="col-sm-3">   
            <div class="form-group" style="margin-top: 22px;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <div class="clearfix"></div>
        @if(isset($followup))
        <div class="row">
        
        <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
            <table class="table table-bordered table-hover jambo_table table-condenced" id="datatable">
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Meeting Date</th>
                        <th>Person Name</th>
                        <th>Mobile</th>
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                        <th class="text-center">Actions</th>

                    </tr>
                </thead>
                <tbody>
                @php($i = 1)
                    @foreach($followup as $follow)
                        @php($loop1 = 1)
                        @while($loop1 <= 2)
                            @if($loop1 == 1)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $follow->meeting_date }}</td>
                                    <td>{{ $follow->lead['user']['name'] }}</td>
                                    <td>{{ $follow->lead['user']['phone_mobile'] }}</td>
                                    <td>{{ $follow->meeting_detail }}</td>
                                    <td>{{  ($follow->meeting_done_by) ? App\User::getUserInfo($follow->meeting_done_by)->name : "" }}</td>
                                    <td></td>
                                </tr>
                            @else 
                                <tr>
                                    <td colspan="1"></td>
                                    <td>{{ $follow->next_meeting_date  }}</td>
                                    <td colspan="2"></td>
                                    <td>{{ $follow->next_meeting_agenda  }}</td>
                                    <td>{{ ($follow->next_meeting_by) ? App\User::getUserInfo($follow->next_meeting_by)->name : ""  }}</td>
                                    <td>
                                        
                                        <a class="btn btn-xs btn-info viewLeadHistory" data-toggle="modal" href="#viewLeadHistory" data-leadid="{{ $follow->lead_id }}">History</a>
                                        <a class="btn btn-xs btn-default viewLead" data-toggle="modal" href="#updateLeadModel" data-leadid="{{ $follow->lead_id }}">Followup</a>
                                    </td>
                                </tr>
                            @endif
                        @php($loop1++)
                        @endwhile
                        @php($i++)
                    @endforeach
               
                </tbody>
            </table>
            </div>
        </div>
         @endif
    </div>

</div>

<div class="modal fade" id="updateLeadModel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          
          
          
          
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Followup</h4>
      </div>
      {!! Form::open(['route' => 'leads.changeStatus', 'id' => 'changeLeadsStatus']) !!}
      <div class="modal-body form-horizontal">
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Lead Info</h2>
        <div class="row">
                    <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Lead Id</label>
                    <div class="col-sm-7 leadId val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Status</label>
                    <div class="col-sm-7 leadStatus val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Name</label>
                    <div class="col-sm-7 leadClient val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Phone</label>
                    <div class="col-sm-7 leadClientPhone val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Project</label>
                    <div class="col-sm-7 leadProject val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Budget</label>
                    <div class="col-sm-7 leadBudget val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Type</label>
                    <div class="col-sm-7 leadPropertyType val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Sub Type</label>
                    <div class="col-sm-7 leadPropertySubType val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Sources</label>
                    <div class="col-sm-7 leadSources val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Campaign</label>
                    <div class="col-sm-7 leadCampaign val"> 
                    </div>
                </div>
            </div>
        </div>
        
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Meeting Info</h2>
        
         <div class="row">
           <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting date</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('meeting_date', date('d/m/Y'), ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_date', 'readonly' => 'readonly']) !!} 
                  </div>
              </div>
          </div>
          <div class="col-sm-6">   
              <div class="form-group">
                   <label class="col-sm-5 control-label">Meeting Channel <span class="required" style="color:red;">*</span></label>
                   <div class="col-sm-7 val"> 
                      <?php 
                      $meeting_channel[''] = 'Choose One';
                      $meeting_channel['sms'] = 'SMS';
                      $meeting_channel['email'] = 'Email';
                      $meeting_channel['whatsapp'] = 'Whatsapp';
                      $meeting_channel['call'] = 'Call';
                      ?>
                      {!! Form::select('meeting_channel', $meeting_channel, 0,  ['class' => 'form-control meeting_channel', 'required'=>'required', 'id' => 'meeting_channel']) !!} 
                    </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting By</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('current_meeting_by', Auth::user()->name , ['class' => 'form-control current_meeting_by', 'required'=>'required', 'id' => 'current_meeting_by', 'readonly' => 'readonly']) !!} 
                    {!! Form::hidden('meeting_done_by', Auth::user()->id) !!}
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting Detail</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('meeting_detail', null, ['class' => 'form-control meeting_detail', 'waive_days'=>'required', 'id' => 'meeting_detail']) !!} 
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Call In / Call Out</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::radio('in_out', 0,  true ) !!} Out
                    {!! Form::radio('in_out', 1,  false ) !!} In
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
                <label class="col-sm-5 control-label">Status <span class="required" style="color:red;">*</span></label>
                <div class="col-sm-7 val"> 
                    <select class="form-control lead_status" name="lead_status" id="lead_status" style="width: 100%" required>

                <option value=''>Choose One</option>
                @foreach($leadStatus as $status)
                  <option value="{{ $status->id}}"> {{ $status->name }}</option>
                @endforeach  
              </select>
                </div>
          </div>
        </div>
        
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Next Call / Meeting</h2>
        <div class="row">  
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting Status</label>
                  <div class="col-sm-7 val"> 
                      {!! Form::radio('meeting_status', 1, false) !!} Done
                      {!! Form::radio('meeting_status', 0, true) !!} Pending
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting date</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('next_meeting_date', null, ['class' => 'form-control datepicker next_meeting_date', 'id' => 'next_meeting_date']) !!} 
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting Channel</label>
                  <div class="col-sm-7 val">
                      <?php 
                              $next_meeting_channel[''] = 'Choose One';
                              $next_meeting_channel['sms'] = 'SMS';
                              $next_meeting_channel['email'] = 'Email';
                              $next_meeting_channel['whatsapp'] = 'Whatsapp';
                              $next_meeting_channel['call'] = 'Call';
                      ?>
    
                      {!! Form::select('next_meeting_channel', $next_meeting_channel, 0,  ['class' => 'form-control next_meeting_channel', 'id' => 'next_meeting_channel']) !!} 
                  </div>
              </div>
          </div>
          <div class="col-sm-6">   
                <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting By</label>
                  <div class="col-sm-7 val">
                      {!! $assign_to_html !!}
                      {{-- {!! Form::select('next_meeting_by', $ccdArr, 0, ['class' => 'form-control', 'id' => 'next_meeting_by']) !!}  --}}
                    </div>  
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting Agenda</label>
                  <div class="col-sm-7 val">
                    {!! Form::text('next_meeting_agenda', null, ['class' => 'form-control next_meeting_agenda', 'id' => 'next_meeting_agenda']) !!} 
                   </div> 
              </div>
          </div>
        </div>

        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Remarks</h2>
          <div class="form-group">
            <label>Remarks</label>  
            <textarea class="form-control" name='remarks' id="remarks"></textarea>
            <input type="hidden" class="lead_id">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>


<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Meeting Date</th>
                  <th>Status</th>
                   
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                        <th>Next Meeting Date</th>
                        <th>Next Meeting Detail</th>
                        <th>Next Meeting By</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




@endsection



@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">








     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    var meeting_status = null;
    $("input[name='meeting_status']").click(function() {
        meeting_status = this.value;

        if(meeting_status == 1)
        {
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
        }else{
            $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
        }
    });
    
    
    
    

    $('#changeLeadsStatus').submit(function(e){
          e.preventDefault();
          
          
          
          var next_meeting_date=$('#next_meeting_date').val(); 
          var next_meeting_by=$('#hidden_user').val();
          
          
          var lead_status = $('#lead_status').val();
          var remarks = $('#remarks').val();
          var lead_id = $('.lead_id').val();

          $.ajax({
            url: '{{ route('leads.changeStatus') }}',
            type: 'POST',
            data: $(this).serialize() + "&lead_id="+lead_id+"&remarks="+remarks,
            beforeSend: function() {
              $('.loader').removeClass('hide');
            }, 
            complete: function(data) {
                $('.loader').addClass('hide');
            },
          })
          .done(function(data) {
          $('#ROW_'+ lead_id + " .status").html(data.leadStatus.name);
          
          
          $('#to_date').val(next_meeting_date);
          $('#user').val(next_meeting_by);
          
          
          $('#getMeetingsForm').submit();
          
          //console.log(next_meeting_date+'---'+next_meeting_by);
          //return;
           
           
            swal("Success!", "Lead has been updated",'success');
            $('#updateLeadModel').modal('toggle');










          })
          .fail(function() {
            console.log("error");
          })
        });

    $(document).on('click', '.viewLead, .updateLeadModel, .viewLeadHistory', function(){
         //alert($(this).data('leadid'));
          $('.leadId').html("");
          $('.leadStatus').html("");
          $('.leadClient').html("");
          $('.leadClientPhone').html("");
          $('.leadProject').html("");
          $('.leadBudget').html("");
          $('.leadPropertyType').html("");
          $('.leadPropertySubType').html("");
          $('.leadSources').html("");
          $('.leadHistoryTable').html("");
          $('.meeting_channel').val("");
          $('.meeting_detail').html("");
          $('.lead_status').val("");
          $('.next_meeting_date').html("");
          $('.next_meeting_channel').val("");
          $('.next_meeting_agenda').html("");
          
          var leadId = $(this).data('leadid');
          $.ajax({

            url: '{{ url('admin/lead/leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {

            //console.log(data.lead);

              $('.leadId').html(data.lead.id);
              $('.lead_id').val(data.lead.id);
              $('.leadStatus').html(data.lead.status.name);
              $('.leadClient').html(data.lead.user.name);
              $('.leadClientPhone').html(data.lead.user.phone_mobile);
              $('.leadBudget').html(data.lead.budget);
              $('.meeting_channel').val("");
              $('.meeting_detail').html("");
              $('.lead_status').val("");
              $('.next_meeting_date').html("");
              $('.next_meeting_channel').val("");
              $('.next_meeting_agenda').html("");

              if(data.lead.sources)
                $('.leadSources').append(data.lead.sources.name);

              if(data.lead.property_type)
                $('.leadPropertyType').html(data.lead.property_type.name);
              
              if(data.lead.property_subtypes)
                $('.leadPropertySubType').html(data.lead.property_subtypes.name);

               $('#lead_created_date').html(data.lead.created_at);
              $('#lead_assigned_date').html(data.lead.assigned_date);
              
              if(data.lead.assigned_by)
                $('#lead_assigned_by').html(data.lead.assigned_by.name);
              
              if(data.lead.assigned_to)
                $('#lead_assigned_to').html(data.lead.assigned_to.name);

              if(data.history != "")
              {

                $.each(data.history, function(index2, val2) {
                   $('.leadHistoryTable').append('<tr><td>'+val2.meeting_date+'</td><td>'+val2.status.name+'</td><td>'+val2.meeting_detail+'</td><td>'+val2.user.name+'</td><td>'+val2.next_meeting_date+'</td><td>'+val2.next_meeting_agenda+'</td><td>'+data.next_meeting_By[index2].name+'</td></tr>');
                });
              }
          })
          .fail(function() {
            console.log("error");
          });
          
        });

    $(document).ready(function() {
    $('#updateLeadModel').on('hidden.bs.modal', function(e) {
    $(this).find('form').trigger('reset');
})


  
});
    
    </script>
@endsection