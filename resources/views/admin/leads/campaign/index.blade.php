@extends('admin.layouts.template')
@section('title','Campaigns')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Campaigns List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Campaigns List</li>
        </ol>
    </div>
     <div class="content">

         <div class="x_panel">
            <div class="x_title">
                <h2>Campaigns List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.campaign.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>  
            <div class="row">
                <div class="col-lg-12">
                    <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                        <h4 class="box-heading">Success</h4>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
    	<table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
    		<thead>
    			<tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Source</th>
                    <th class="text-center">Actions</th>
                </tr>
    		</thead>
    		<tbody>
              <?php $i=1 ?> 
    			@foreach($campaign as $lf)
	    			<tr>
                        <td>{{ $i++ }}</td>
                       <td>{{ $lf->name }}</td>
                       <td>{{ ($lf->source) ? $lf->source->name: ""}}</td>
                        <td class="text-center">
                         @if($rights->show_edit)
                        <a href="{{ route('leads.campaign.edit', $lf->id )}}" class="btn btn-info btn-xs edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                        @endif

                        @if($rights->show_delete)
                       
                        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['leads.campaign.delete', $lf->id] , 'id' => 'table_form']) !!}
                            <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                            {{-- {!! Form::submit("<p>a</p>", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!} --}}
                        {!! Form::close() !!}
                        @endif
                        </td>
                       
                    </tr>
	    		@endforeach
    		</tbody>
    	</table>	</div>
    	</div>
	</div>
@endsection

@section('javascript')
    
     <script type="text/javascript">
    $(document).ready(function(){
       

    $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
    });  


    
    </script>
@endsection
