@extends('admin.layouts.template')
@section('title','Add Campaign')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Campaign</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Campaign</li>
        </ol>
    </div>
     <div class="content">

         <div class="x_panel">
        <div class="x_title">
            <h2>Edit Campaign</h2>
            
            <div class="clearfix"></div>
          </div>   
            @include('errors')
             <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">      

       
        {!! Form::open(['method' => 'POST', 'route' => 'leads.campaign.store', 'class' => 'form-horizontal' , 'id' => 'validate_form']) !!}
            
            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                <div class="col-sm-8">
                {!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name' , 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="form-group">
                    <label for="label" class="col-sm-3 control-label">Sources</label>
                    <div class="col-sm-8">
                    @php($lsourcesArr[''] = 'Select Source')
                    @foreach($lsources as $key => $project)
                        @php($lsourcesArr[$project->id] = $project->name)
                    @endforeach
                    {!! Form::select('source_id', $lsourcesArr, 0, ['class' => 'form-control', 'id' => 'source_id']) !!}
                    </div>
                </div>

            </div>

             <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/lead/campaign') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
</div>
@endsection
@section('javascript')
<script>
    
$("#validate_form").validate({
            rules: {
            
                name: {
                    required: true,
                    alphanumeric:true
                    
                },
               
              
                
                

            }
        });


jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
}, "Letters, numbers, and underscores only please");  

</script>

@endsection