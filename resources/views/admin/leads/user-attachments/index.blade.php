@extends('admin.template')
@section('title','Agents')

@section('content')

    	<h2> All Front Desk Officer<small class="pull-right"><a href="{{ route('fd')}}" class="btn btn-primary">Add New</a></small></h2>
        @if(!empty($message))
    		<p>{{$message}}</p>
    	@endif

         <div class="x_panel">
        <div class="x_title">
            
            <div class="clearfix">

            </div>
          </div>

        <div class="table-responsive">
    	<table class="table table-bordered table-hover jambo_table bulk_action">
    		<thead>
    			<tr>
                    <th>Picture</th>
                    <th>Name</th>
                    
                   <th>Email</th>
                    <th>Phone</th> 
                    <th>CNIC</th> 
                    <th>Agency</th>
                    {{-- <th>Status</th> --}}
                    {{-- <th>Change Status</th> --}}
                    <th></th>
                </tr>
    		</thead>
    		<tbody>
               
    			@foreach($persons as $agent)
	    			<tr>
                        <td><img src="{{ asset('/public/images/'.$agent->picture) }}" width="60" height="60" class="img-responsive"></td>
                        <td>{{ $agent->name }}</td>
                       
                       <td>{{ $agent->email }}</td>
                        <td>{{ $agent->phone_mobile }}</td>
                        <td>{{ $agent->cnic }}</td>
                        @if(Entrust::hasRole('Admin'))
                        <td>Admin</td>
                        @endif

                        @if(Entrust::hasRole('agency'))
                        <td>{{ $agent->agent->agency->name or "" }}</td>
                        @endif
                        <?php 
                            $btnSuccess = '<a href="#" class="btn btn-success btn-xs">Approved</a>';
                            $email_verified_yes = '<a href="#" class="btn btn-success btn-xs">Yes</a>';
                            $btnDanger = '<a href="#" class="btn btn-danger btn-xs">Pending</a>';
                            $email_verified_no = '<a href="#" class="btn btn-danger btn-xs">No</a>';
                        ?>
                        {{-- <td>{!! ($agent->is_approved == 0) ? $btnDanger : $btnSuccess !!}</td> --}}
                       
                        <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['fd.destroy', $agent->id]]) !!}
                            {!! Form::submit("Delete", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!}
                        {!! Form::close() !!}
                        </td>
                       
                    </tr>
	    		@endforeach
    		</tbody>
    	</table>	</div></div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
        });
    </script>
@endsection
