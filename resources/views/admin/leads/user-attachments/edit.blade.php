@extends('admin.template')
@section('title', 'Add Attachments')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection


@section('content')

      <div class="x_panel">
        <div class="x_title">
            <h2>Add Attachments for customer '{{ $attachment->user->name }}'</h2>
            
            <div class="clearfix"></div>
          </div>   
        <div class="row"> 
        @include('errors')
        {!! Form::model($attachment,['method' => 'PATCH', 'route' => ['user-attachment.update', $attachment->id], 'files' => 'true', 'class' => 'form-horizontal']) !!}
            <div class="col-sm-6 col-sm-offset-3">

                <div class="form-group">
                    {!! Form::label('attachment', 'Attachment') !!}
                    {!! Form::file('attachment[]', array('class'=>'form-control attachmentField', 'multiple' => true)  )!!}
                </div>

                <div class="form-group">
                    {!! Form::label('title', 'title')!!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('notes', 'notes')!!}
                    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
                </div>

            </div>
        
           
        <div class="col-sm-6 col-sm-offset-3">
            <div class="form-group">
                {!! Form::hidden('person_id', $attachment->person_id) !!}
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div></div>
        {!! Form::close() !!}
        </div> </div>
        
@endsection

@section('javascript')
@endsection