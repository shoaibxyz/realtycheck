@extends('admin.layouts.template')
@section('title', 'Add Attachments')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Attachments for customer</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Attachments for customer</li>
        </ol>
    </div>
     <div class="content">
      <div class="x_panel">
        <div class="x_title">
            <h2>Add Attachments for customer '{{ $customer->name }}'</h2>
            
            <div class="clearfix"></div>
          </div>   
        <div class="row"> 
        @include('errors')
        {!! Form::open(['method' => 'POST', 'route' => 'user-attachment.store', 'files' => 'true', 'class' => 'form-horizontal']) !!}
            <div class="col-sm-6 col-sm-offset-3">

                <div class="form-group">
                    {!! Form::label('attachment', 'Attachment') !!}
                    {!! Form::file('attachment[]', array('class'=>'form-control attachmentField', 'multiple' => true)  )!!}
                </div>

                <div class="form-group">
                    {!! Form::label('title', 'title')!!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('notes', 'notes')!!}
                    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
                </div>

            </div>
        
           
        <div class="col-sm-6 col-sm-offset-3">
            <div class="form-group">
                {!! Form::hidden('person_id', $customer->id) !!}
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div></div>
        {!! Form::close() !!}
        </div> </div>
        </div>
@endsection

@section('javascript')
@endsection