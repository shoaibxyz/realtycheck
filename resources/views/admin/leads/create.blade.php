@extends('admin.layouts.template')
@section('title','New Lead')

@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">New Lead</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">New Lead</li>
        </ol>
    </div>
     <div class="content">
         
    <div class="x_panel">
<div class="x_title">
            <h2>New Lead</h2>
                
               
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div>
        <div class="row">
            @include('errors')
            <div class="col-sm-12">

                {!! Form::open(['method' => 'POST', 'route' => 'leads.store', 'class' => 'form-horizontal']) !!}
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Lead Info</h2>
                
                <div class="row">
                    <div class="col-sm-6">
                       <div class="form-group">
                            <label class="col-sm-5 control-label control-label">Classification <span style="color: red">*</span></label>
                            <div class="col-sm-7"> 
                            @php($classificationArr = [])
                            @foreach(\App\Helpers::Classification() as $key => $classification)
                                @php($classificationArr[$key] = $classification)
                            @endforeach
                            {!! Form::select('classification', $classificationArr, 0, ['class' => 'form-control', 'id' => 'classification', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group"> 
                            <label class="col-sm-5 control-label control-label">Purpose <span style="color: red">*</span></label>
                           <div class="col-sm-7"> 
                            @php($PurposeArr = [])
                            @foreach(\App\Helpers::Purpose() as $key => $Purpose)
                                @php($PurposeArr[$key] = $Purpose)
                            @endforeach
                            {!! Form::select('purpose', $PurposeArr, 0, ['class' => 'form-control', 'id' => 'Purpose', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('propertyType', 'Property Type', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            @php($propertyTypesArr = [])
                            @php($propertyTypesArr[''] = 'Select Type')
                            @foreach($propertyTypes as $key => $types)
                                @php($propertyTypesArr[$types->id] = $types->name)
                            @endforeach
                            {!! Form::select('property_type_id', $propertyTypesArr, 0, ['class' => 'form-control', 'id' => 'propertyType']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('propertySubtype', 'Property Sub Type', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            @php($propertySubTypesArr = [])
                            @php($propertySubTypesArr[''] = 'Select Property Type')
                            {!! Form::select('property_subtype_id', $propertySubTypesArr, 0, ['class' => 'form-control', 'id' => 'propertySubtype']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label class="col-sm-5 control-label">Source</label>
                            <div class="col-sm-7">
                            @php($lsourcesArr[''] = 'Select Source')
                            @foreach($LeadSource as $key => $project)
                                @php($lsourcesArr[$project->id] = $project->name)
                            @endforeach
                            {!! Form::select('source_id', $lsourcesArr, 0, ['class' => 'form-control', 'id' => 'source_id' ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Campaigns</label>
                            <div id="campaign" class="col-sm-7">
                                 @php($campaignsArr[''] = 'Select Campaign')
                                        @foreach($campaigns as $key => $campaign)
                                            @php($campaignsArr[$campaign->id] = $campaign->name)
                                        @endforeach
                                {!! Form::select('campaign_id', $campaignsArr, 0, ['class' => 'form-control', 'id' => 'campaign_id' ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Projects</label>
                            <div class="col-sm-7">
                            @php($projectsArr[''] = 'Select Project')
                            @foreach($projects as $key => $project)
                                @php($projectsArr[$project->id] = $project->name)
                            @endforeach
                            {!! Form::select('project_id', $projectsArr, 0, ['class' => 'form-control', 'id' => 'propertyType'  , 'id' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('Beds', 'Beds', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            @php($BedsArr = [])
                           
                            @foreach(\App\Helpers::Beds() as $key => $Beds)
                                @php($BedsArr[$key] = $Beds)
                            @endforeach
                           
                            {!! Form::select('beds', $BedsArr, 0, ['class' => 'form-control', 'id' => 'Beds']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('land_area', 'Land Area', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {!! Form::text('land_min_area', null, ['class' => 'form-control', 'placeholder' => 'Min Land']) !!}
                                    </div>
        
                                    <div class="col-sm-4">
                                        {!! Form::text('land_max_area', null, ['class' => 'form-control', 'placeholder' => 'Max Land']) !!}
                                    </div>    
        
                                    <div class="col-sm-4">
                                        @php($LandUnitsArr = [])
                                        @foreach(\App\Helpers::LandUnits() as $key => $LandUnits)
                                            @php($LandUnitsArr[$key] = $LandUnits)
                                        @endforeach
                                        {!! Form::select('land_unit', $LandUnitsArr, 0, ['class' => 'form-control', 'id' => 'Beds']) !!}
                                    </div>    
                                </div>    
                            </div>    
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            {!! Form::text('interest', null, ['class' => 'form-control', 'id' => 'interest']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('visit_promise_date', 'Visit Promise Date', ['class' => 'col-sm-5 control-label']) !!}
                             <div class="col-sm-7">
                                   {!! Form::text('visit_promise_date', \Carbon\Carbon::now()->format('d/m/Y'),  array( 'class'=>'form-control datepicker') )!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('budget', 'Budget', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            {!! Form::text('budget', null, ['class' => 'form-control', 'id' => 'budget']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                             {!! Form::label('whatsapp_sent_on', 'Whatsapp Sent On', ['class' => 'col-sm-5 control-label']) !!}
                             <div class="col-sm-7">
                             {!! Form::text('whatsapp_sent_on', \Carbon\Carbon::now()->format('d/m/Y'),  array( 'class'=>'form-control datepicker') )!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('customer_feedback', 'Customer Feedback', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            {!! Form::textarea('customer_feedback_after_visit', null, ['class' => 'form-control', 'id' => 'customer_feedback', 'rows' => 5]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('comments', 'Comments', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                            {!! Form::textarea('comments', null, ['class' => 'form-control', 'id' => 'comments', 'rows' => 5]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('Customer Type', 'Customer Type', ['class' => 'col-sm-5 control-label']) !!}
                            <div class="col-sm-7" style="padding-left:30px;">
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="customer" id="inputExisting_customer" value="0">
                                        Existing Customer
                                    </label>
                                </div>
        
                                <div class="radio-inline" style="margin-left:25px;">
                                    <label>
                                        <input type="radio" name="customer" id="inputExisting_customer" value="1">
                                        New Customer
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
                <div class="hide customerForm">
                    <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Customer Info</h2>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name', 'name', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('email', 'email', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('cnic', 'CNIC', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('phone', 'Phone', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="label" class="col-sm-5 control-label">Whatsapp Number</label>
                                <div class="col-sm-7"> 
                                {!! Form::text('whatsapp_number', null, ['class' => 'form-control', 'id' => 'whatsapp_number']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('address', 'Address', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                {!! Form::text('zip_code', null, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('Countries', 'Country', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                @php($CountriesArr[''] = 'Select Country')
                                @foreach($countries as $key => $country)
                                    @php($CountriesArr[$country->id] = $country->name)
                                @endforeach
                                {!! Form::select('country_id', $CountriesArr, 0, ['class' => 'form-control', 'id' => 'country_id']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('ClinetTypes', 'Clinet Type', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                    @php($ClinetTypesArr = [])
                                    @foreach(\App\Helpers::ClinetTypes() as $key => $ClinetTypes)
                                        @php($ClinetTypesArr[$key] = $ClinetTypes)
                                    @endforeach
                                    {!! Form::select('client_type', $ClinetTypesArr, 0, ['class' => 'form-control', 'id' => 'ClinetTypes']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('ClientRating', 'Client Rating', ['class' => 'col-sm-5 control-label']) !!}
                                <div class="col-sm-7">
                                @php($ClinetRatingArr = [])
                                @foreach(\App\Helpers::ClinetRating() as $key => $ClinetRating)
                                    @php($ClinetRatingArr[$key] = $ClinetRating)
                                @endforeach
                                {!! Form::select('client_rating', $ClinetRatingArr, 0, ['class' => 'form-control', 'id' => 'ClinetRating']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>


                <div class="existingCustomerForm hide" style="margin: 20px 0px  20px 0px;">
                    <div class="form-group">
                        {!! Form::label('client', 'Search Clients', ['class' => 'col-sm-3']) !!}
                        <div class="col-sm-3">    
                            {!! Form::text('client_search', null, ['class' => 'form-control', 'id' => 'client_search']) !!}
                        </div>
                        <div class="col-sm-3">    
                            @php($ClientSearchFiledsArr = [])
                            @foreach(\App\Helpers::ClientSearchFileds() as $key => $ClientSearchFileds)
                                @php($ClientSearchFiledsArr[$key] = $ClientSearchFileds)
                            @endforeach
                            {!! Form::select('client_search_in', $ClientSearchFiledsArr, 0, ['class' => 'form-control', 'id' => 'client_search_in']) !!}
                        </div>
                        <div class="col-sm-3">    
                            {!! Form::button('Search', ['class' => 'btn btn-md btn-default', 'id' => 'searchClient']) !!}
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <table class="table table-bordered table-hover existingCustomerTable hide">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>   
                </div>
                <!--<div class="form-group">-->
                <!--        {!! Form::label('interest', 'Interest', ['class' => 'col-sm-3 control-label']) !!}-->
                <!--        <div class="col-sm-9">-->
                <!--        <div class="checkbox">-->
                <!--            <label>-->
                <!--                <input type="checkbox" value="future" name="future_interest">-->
                <!--                Future Oppurtunity-->
                <!--            </label>-->
                <!--        </div>-->

                <!--        <div class="checkbox">-->
                <!--            <label>-->
                <!--                <input type="checkbox" value="local" name="local_interest">-->
                <!--                Local Oppurtunity-->
                <!--            </label>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->

                <div class="clearfix"></div>

                

                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('leads.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function($) {

var date = new Date();
		// console.log(date.Get)
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy',
	       // maxDate: date.getMonth()
	    });
            $('#searchClient').click(function(e){
                e.preventDefault();
                $('.existingCustomerTable tbody').html("");
                var search = $('#client_search').val();
                var search_in = $('#client_search_in').val();

                $.ajax({
                    url: '{{ route('clients.getClientInfo') }}',
                    type: 'POST',
                    data: {'search': search, 'search_in' : search_in },
                })
                .done(function(data) {
                    //console.log(data.clients);
                    $('.existingCustomerTable').removeClass('hide');
                    $.each(data.clients, function(index, val) {
                        //console.log(val);
                         /* iterate through array or object */
                        $('.existingCustomerTable tbody').append("<tr><td><input type='radio' value='"+val.person_id+"' name='customer_id'></td><td>"+val.name+"</td><td>"+val.phone_mobile+"</td><td><a href='{{ URL::to('admin/lead/clients/') }}/"+val.person_id+"'>Detail</a></td></tr>")
                    });
                })
                .fail(function() {
                    console.log("error");
                });
                
            });

            $('input[type=radio][name=customer]').change(function() {
                if (this.value == 0) {
                    $('.customerForm').addClass('hide');
                    $('.existingCustomerForm').removeClass('hide');
                    $('.existingCustomerTable tbody').html("");
                }
                else {
                    $('.customerForm').removeClass('hide');
                    $('.existingCustomerForm').addClass('hide');
                    $('.existingCustomerTable tbody').html("");
                }
            });


            $('#propertyType').change(function(){
                $.ajax({
                    url: '{{ route('leads.getPropertySubType') }}',
                    type: 'POST',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {property_type_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#propertySubtype').html(data.html);
                })
                .fail(function() {
                    console.log("error");
                });
                
            }); 
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#source_id').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{ route('leads.campaign.findcampaign') }}",
                    type: 'POST',
                    data: {id: $(this).val()},
                })
                .done(function(data) {
                    $('#campaign').html(data.campaign);
                })
                .fail(function() {
                    console.log("error");
                });    
            });
        });
    </script>
@endsection