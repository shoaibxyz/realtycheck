@extends('admin.layouts.template')
@section('title','Leads With Token Received')

@section('content')
@include('admin/common/breadcrumb',['page'=>'Leads List'])
   
    <div class="content">
<div class="x_panel">
 <div class="x_title">
            <h2>Leads List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div>    
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Status</th>
                    <th>Client</th>
                    <th>Mobile</th>
                    <th>Project</th>
                    <th>Budget</th>
                    {{-- @if(Entrust::hasRole('admin'))
                    <th>Assign To</th>
                    @endif --}}
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
                @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ ($lead->lead_status_id != "")  ? $lead->status->name : "" }}</td>
                  <td>{{ ($lead->User and $lead->User->name != "")  ? $lead->User->name : "" }}</td>
                  <td>{{ ($lead->User and $lead->User->phone_mobile != "")  ? $lead->User->phone_mobile : "" }}</td>
                  <td>{{ ($lead->project_id) ? $lead->project->name : "" }}</td>
                  <td>{{ "Rs. ".number_format($lead->budget) }}</td>
                  {{-- <td>{{ ($lead->assignedTo != "") ? $lead->assignedTo->name : ""}}</td> --}}
                  <td class="text-center">
                    <a class="btn btn-xs btn-primary viewLead" data-toggle="modal" href='#viewLeadModel' data-leadid='{{ $lead->id }}' title="View"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>


{{-- <a class="btn btn-primary" >Trigger modal</a> --}}
<div class="modal fade" id="viewLeadModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- <div class="modal fade" id="updateLeadModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      {!! Form::open(['route' => 'leads.changeStatus', 'id' => 'changeLeadsStatus']) !!}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>
        <hr>
        <div class="row">
          <div class="form-group">
            <label class="col-sm-4">Status</label>  
            <div class="col-sm-8">
              <select class="form-control" name="lead_status" id="lead_status" style="width: 100%">
                <option value="">Select One</option>
                @foreach($leadStatus as $status)
                  <option value="{{ $status->id}}"> {{ $status->name }}</option>
                @endforeach  
              </select>
            </div>
          </div>
        </div>

        <br>

        <div class="row">

          <div class="form-group">
            <label class="col-sm-4">Remarks</label>  
            <div class="col-sm-8">
              <textarea class="form-control" name='remarks' id="remarks"></textarea>
              <input type="hidden" class="lead_id">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div> --}}

{{-- <div class="modal fade" id="assignedLeads">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">User Leads</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <h4>User: <span style="font-weight: bold;" class="username"></span></h4>
          </div>  
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Sr. No</th>
                <th>Lead Id</th>
                <th>Customer</th>
                <th>Phone</th>
                <th>Assigned Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="userLeadsTable">
            </tbody>
          </table>    
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> --}}

<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Status</th>
                  <th>By</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

        $('#changeLeadsStatus').submit(function(e){
          e.preventDefault();
          var lead_status = $('#lead_status').val();
          var remarks = $('#remarks').val();
          var lead_id = $('.lead_id').val();

          $.ajax({
            url: '{{ route('leads.changeStatus') }}',
            type: 'POST',
            data: {'lead_id': lead_id,'status' : lead_status, 'remarks' : remarks},
            beforeSend: function() {
              $('.loader').removeClass('hide');
            }, 
            complete: function() {
                $('.loader').addClass('hide');
            },
          })
          .done(function(data) {
            swal("Success!", "Lead has been updated",'success');
            $('#updateLeadModel').modal('toggle');
          })
          .fail(function() {
            console.log("error");
          })
        });
        

        var columns = [
            { data: 'status', name: 'status' },
            { data: 'name', name: 'name'},
            { data: 'phone_mobile', name: 'phone_mobile'},
            { data: 'project', name: 'project'},
            { data: 'budget', name: 'budget'},
            { data: 'assigned_date', name: 'assigned_date'},
        ];

      columns.push({ data: 'assign', name: 'assign'});
        
        columns.push({ data: 'actions', name: 'actions'});

        var table = $('#leadsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "info": true,
            "ajax": '{!! route('leads.getLeads') !!}',
            "columns": columns,
        });


        $(document).on('change','.assignTo', function(e){
          e.preventDefault();

          var assignTo = $(this).val();
          if(assignTo != "")
          {
            var lead = $(this).data('lead_id');
            $.ajax({
              url: '{{ route('leadUpdate') }}',
              type: 'POST',
              data: {lead_id: lead, assignTo: assignTo},
            })
            .done(function(data) {
              table.draw();
            })
            .fail(function() {
              console.log("error");
            });
          }
        });

        $(document).on('click', '.viewLead, .updateLeadModel, .viewLeadHistory', function(){
         
          $('.leadId').html("");
          $('.leadStatus').html("");
          $('.leadClient').html("");
          $('.leadClientPhone').html("");
          $('.leadProject').html("");
          $('.leadBudget').html("");
          $('.leadPropertyType').html("");
          $('.leadPropertySubType').html("");
          $('.leadSources').html("");
          $('.leadHistoryTable').html("");
          
          var leadId = $(this).data('leadid');
          $.ajax({
            url: '{{ url('admin/lead/leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {
              $('.leadId').html(data.lead.id);
              $('.lead_id').val(data.lead.id);
              $('.leadStatus').html(data.lead.status.name);
              $('.leadClient').html(data.lead.user.name);
              $('.leadClientPhone').html(data.lead.user.phone_mobile);
              $('.leadProject').html(data.lead.project.name);
              $('.leadBudget').html(data.lead.budget);
              $('.leadPropertyType').html(data.lead.property_type.name);
              $('.leadPropertySubType').html(data.lead.property_subtypes.name);
              $.each(data.lead.sources, function(index, val) {
                 $('.leadSources').append(val.name + ", ");
              });

              if(data.history != "")
              {
                $('#lead_created_date').html(data.lead.created_at);
                $('#lead_assigned_date').html(data.lead.assigned_date);
                $('#lead_assigned_by').html(data.lead.assigned_by.name);
                $('#lead_assigned_to').html(data.lead.assigned_to.name);

                $.each(data.history, function(index2, val2) {
                   $('.leadHistoryTable').append('<tr><td>'+val2.created_at+'</td><td>'+val2.status.name+'</td><td>'+val2.user.name+'</td><td>'+val2.remarks+'</td></tr>');
                });
              }
          })
          .fail(function() {
            console.log("error");
          });
          
        });

        $(document).on('click', '.assignedLeads', function(){
          $('.username').html("");
          var leadId = $(this).data('userid');
          $.ajax({
            url: '{{ url('admin/lead/leads/show-user-leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {
              $('#userLeadsTable').html("");
              $('.username').html(data.user.name);
              // $('.leadId').html(data.lead.id);
              // $('.leadStatus').html(data.lead.status.name);
              // $('.leadClient').html(data.lead.user.name);
              // $('.leadClientPhone').html(data.lead.user.phone_mobile);
              // $('.leadProject').html(data.lead.project.name);
              // $('.leadBudget').html(data.lead.budget);
              // $('.leadPropertyType').html(data.lead.property_type.name);
              var i = 1;
              $.each(data.leads, function(index, val) {
                 $('#userLeadsTable').append("<tr><td>"+ i++ +"</td><td>"+val.id+"</td><td>"+val.user.name+"</td><td>"+val.user.phone_mobile+"</td><td>"+val.assigned_date+"</td><td>"+val.status.name+"</td></tr>");
              });

              // $('.assign_to').html(data.saleOfficer);
          })
          .fail(function() {
            console.log("error");
          });
          
        });

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
