@extends('admin.layouts.template')
@section('title','Search Lead')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection
</link>

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Search Lead</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Search Lead</li>
        </ol>
    </div>
    <div class="content">
    <div class="x_panel">
 <div class="x_title">
            <h2>Search Lead</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div>    
        <div class="row">
            @include('errors')
            <div class="col-sm-12">
                
                {!! Form::open(['method' => 'POST', 'route' => 'lead.searchLead', 'class' => 'form-horizontal']) !!}
              <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Lead Info</h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-5 control-label control-label">Classification <span style="color: red">*</span></label>
                    <div class="col-sm-7">
                        @php($classificationArr = [])
                        @foreach(\App\Helpers::Classification() as $key => $classification)
                        @php($classificationArr[$key] = $classification)
                        @endforeach
                        {!! Form::select('classification', $classificationArr, 0, ['class' => 'form-control', 'id' => 'classification']) !!}
                </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-5 control-label control-label">Purpose <span style="color: red">*</span></label>
                <div class="col-sm-7">
                    @php($PurposeArr = [])
                    @foreach(\App\Helpers::Purpose() as $key => $Purpose)
                        @php($PurposeArr[$key] = $Purpose)
                    @endforeach
                    {!! Form::select('purpose', $PurposeArr, 0, ['class' => 'form-control', 'id' => 'Purpose']) !!}
                </div>
                </div>
                </div>
            </div>    
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                  {!! Form::label('propertyType', 'Property Type', ['class' => 'col-sm-5 control-label']) !!}
                <div class="col-sm-7">    
                    @php($propertyTypesArr = [])
                    @php($propertyTypesArr[''] = 'Select Type')
                    @foreach($propertyTypes as $key => $types)
                        @php($propertyTypesArr[$types->id] = $types->name)
                    @endforeach
                    {!! Form::select('property_type_id', $propertyTypesArr, 0, ['class' => 'form-control', 'id' => 'propertyType']) !!}
                </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                 {!! Form::label('propertySubtype', 'Property Sub Type', ['class' => 'col-sm-5 control-label']) !!}    
                <div class="col-sm-7">
                    @php($propertySubTypesArr = [])
                    @php($propertySubTypesArr[''] = 'Select Property Type')
                    {!! Form::select('property_subtype_id', $propertySubTypesArr, 0, ['class' => 'form-control', 'id' => 'propertySubtype']) !!}
                </div>
                </div>
                </div>
            </div>
            
            
            <div class="row">
                
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-5 control-label">Source</label>
                    <div class="col-sm-7">
                     @php($LeadSourceArr = [])
                     @php($LeadSourceArr[0] = "Any")
                    @foreach($LeadSource as $key => $source)
                        @php($LeadSourceArr[$source->id] = $source->name)
                    @endforeach
                    {!! Form::select('source_id', $LeadSourceArr, 0, ['class' => 'form-control', 'id' => 'propertySubtype']) !!}
                </div>
                </div>
                </div>


             <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Projects</label>
                    <div class="col-sm-7">
                    @php($projectsArr[''] = 'Select Project')
                    @foreach($projects as $key => $project)
                        @php($projectsArr[$project->id] = $project->name)
                    @endforeach
                    {!! Form::select('project_id', $projectsArr, 0, ['class' => 'form-control', 'id' => 'propertyType']) !!}
                </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Beds', 'Beds', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($BedsArr = [])
                    @foreach(\App\Helpers::Beds() as $key => $Beds)
                        @php($BedsArr[$key] = $Beds)
                    @endforeach
                    {!! Form::select('beds', $BedsArr, 0, ['class' => 'form-control', 'id' => 'Beds']) !!}
                </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('lead_id', 'Lead ID', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                        {!! Form::text('lead_id', null, ['class' => 'form-control']) !!}
                </div>
                </div>
                </div>
            </div>

            <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Client Info</h2>
            <div class="row">
                <div class="col-sm-6">    
                <div class="form-group">
                    {!! Form::label('name', 'name', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                    </div>
                </div>
                </div>
               
                <div class="col-sm-6">
                <div class="form-group">
                   {!! Form::label('email', 'email', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                  {!! Form::label('phone', 'Phone', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                  {!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile']) !!}
                    </div>
                </div>
                </div>
            </div>    

            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('address', 'Address', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('zip_code', null, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                    </div>
                </div>
                </div>
            </div> 
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('lead_from', 'From Date', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                        {!! Form::text('lead_from', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                </div>
                <div class="col-sm-6"> 
                <div class="form-group">
                    {!! Form::label('lead_to', 'To Date', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                        {!! Form::text('lead_to', null, ['class' => 'form-control datepicker']) !!}
                   </div>
                </div>
                </div>
            </div>

            <div class="row">    
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('budget', 'Budget', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($BudgetArr = [])
                    @foreach(\App\Helpers::Budget() as $key => $Budget)
                        @php($BudgetArr[$key] = $Budget)
                    @endforeach
                    {!! Form::select('budget', $BudgetArr, 0, ['class' => 'form-control', 'id' => 'Budget']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7" >
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="future_interest">
                            Future Oppurtunity
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="local_interest">
                            Local Oppurtunity
                        </label>
                    </div>  
                    </div>
                </div>
                </div>
            </div> 
            <div class="clearfix"></div>
            <div class="form-actions">
                {!! Form::submit("Search", ['class' => 'btn btn-warning']) !!}
                <a href="{{ route('lead.search') }}" class="btn btn-grey">Cancel</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>

@endsection


@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function($) {
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $('#propertyType').change(function(){
                $.ajax({
                    url: '{{ route('leads.getPropertySubType') }}',
                    type: 'POST',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {property_type_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#propertySubtype').html(data.html);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });   
        });
    </script>
@endsection