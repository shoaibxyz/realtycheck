@extends('admin.layouts.template')
@section('title','Edit Lead reminder')

@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('reminder_menu', 'display:block')
@section('lead_menu', 'display:block')
@section('leads', 'active')
@section('reminder-all', 'current-page')


@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::to('public/css/jquery.datetimepicker.min.css') }}">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Lead Reminder</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Lead Reminder</li>
        </ol>
    </div>
    <div class="x_panel">
       
        <div class="row" style="padding-top: 20px;">
            <div class="col-sm-6 col-sm-offset-3">
            @include('errors')
                {!! Form::model($reminder, ['method' => 'PATCH', 'route' => ['reminder.update', $reminder->id], 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('lead_id', 'Lead', ['class' => 'col-sm-3 control-label'] ) !!}
                    <div class="col-sm-9">
                    {!! Form::select('lead_id', $leadsArr, null, ['class' => 'form-control ', 'id' => 'lead_id', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('message', 'message', ['class' => 'col-sm-3 control-label'] ) !!}
                    <div class="col-sm-9">
                    {!! Form::text('message', null, ['class' => 'form-control', 'id' => 'message', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('reminder_time', 'reminder_time', ['class' => 'col-sm-3 
                    control-label'] ) !!}
                    <div class="col-sm-9">
                    {!! Form::text('reminder_time', null, ['class' => 'form-control datetimepicker', 'id' => 'reminder_time', 'required' => 'required']) !!}
                    </div>
                </div>
             <div class="col-sm-12 form-actions text-right pal">  
                <div class="form-group">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('reminder.index') }}" class="btn btn-grey">Cancel</a>
                </div></div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</link>
@endsection


@section('javascript')

    <script type="text/javascript" src="{{ URL::to('public/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript">
        $('.datetimepicker').datetimepicker({
            format:'Y-m-d H:i'
        });

    </script>
@endsection