@extends('admin.layouts.template')
@section('title','Lead Reminder')

@section('content')


    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Lead Reminders List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Lead Reminders List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
        <div class="x_title">
            <h2>Lead Reminders List</h2>
           
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('reminder.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
         
          </div>  
        <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Lead</th>
                    <th>Mesasge</th>
                    <th>Reminder Time</th>
                    <th>Completed</th>
                    <th>Completed By</th>
                    <th>Completed Date</th>
                    <th>Date Added</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            
            <tbody class="table table-bordered table-striped">
                @php($i = 1)
                @foreach($reminders as $reminder)
                 <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $reminder->lead_id }}</td>
                    <td>{{ $reminder->message }}</td>
                    <td>{{ date('d-m-Y h:i:s', strtotime($reminder->reminder_time)) }}</td>
                    <td>{!! ( $reminder->is_completed  == 0) ? "No" : "Yes" !!}</td>
                    <td>{{ ($reminder->completed_by != "") ? \App\User::getUserInfo($reminder->completed_by)->name : "" }}</td>
                    <td>{{ ($reminder->completed_date != null) ? date('d/m/y H:i:s', strtotime($reminder->completed_date)) : "" }}</td>
                    <td>{{ date('d/m/y H:i:s', strtotime($reminder->created_at)) }}</td>
                    <td class="text-center">
                    <a href="{{ route('reminder.edit',$reminder->id) }}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                     @if(!$reminder->is_completed)
                    <a href="{{ route('reminder.complete',$reminder->id) }}" class="btn btn-xs btn-success">Complete</a>@endif
                    <form method="post" action="{{ route('reminder.destroy', $reminder->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'> <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                           </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table></div></div>
    </div>
@endsection

@section('javascript')
    
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
    </script>
@endsection