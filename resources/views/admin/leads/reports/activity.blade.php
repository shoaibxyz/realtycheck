@extends('admin.layouts.template')
@section('title','Lead Activity Report')
@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('lead_menu', 'display:block')
@section('lead-reports', 'active')
@section('report_menu', 'display:block')
@section('activity', 'current-page')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

@include('admin/common/breadcrumb',['page'=>'Lead Activity Report'])
<div class="content">

        <div class="x_panel">
            <div class="x_title">
            <h2>Lead Activity Report</h2>
               
                <div class="clearfix"></div>
            
          </div>   
            @include('errors')
    <div class="row">

        {!! Form::open(['method' => 'POST', 'route' => 'lead.activityReportResult', 'class' => 'form-horizontal']) !!}

            {{-- <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="call" checked>
                    calls
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="email" checked>
                    Emails
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="attempt" checked>
                    attempt
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="site visit" checked>
                    site visit
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="Company Office" checked>
                    Company Office
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="out-door" checked>
                    Out-door
                </label>
            </div>

            <div class="checkbox checkbox-inline">
                <label>
                    <input type="checkbox" name="status_filter[]" value="site office" checked>
                    Site Office
                </label>
            </div> --}}

            <div class="" style="display: inline; margin-right: 50px;">
                <label class="col-sm-2 control-label">Choose Date</label>
                <div class="col-sm-3">
                <input type="text" name="date" class="form-control datepicker">
                </div>
                <input type="hidden" id="startdate" name="startdate"/>
                <input type="hidden" name="enddate" id="enddate" />
                <div class="col-sm-4">
                <input type="submit" value="Search" class="btn btn-warning">
              </div>
            </div>


        {!! Form::close() !!}
        
    </div>

    <div class="x_panel" style="margin-top: 50px;">
        <div class="x_title">
               <h2>Summary</h2>
            <div class="clearfix"></div>
        </div>

        <div class="row" >
            <div class="col-sm-4">
                @if($leadStatus)
                    @foreach($leadStatus as $status)
                        @php($count = 0)
                        @if(isset($leadStatusCount[$status->id]))
                            @php($count = $leadStatusCount[$status->id])
                        @endif
                        <p>{{ $status->name}}: <span class="pull-right">{{ $count }}</span></p>
                    @endforeach
                @endif
            </div>
        </div>
    </div>


    <div class="x_panel" style="margin-top: 50px;">
        <div class="x_title">
               <h2>Result</h2>
            <div class="clearfix"></div>
        </div>

        <div class="row" >
            <div class="col-sm-12">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Date Added</th>
                            <th class="text-center">Lead Id</th>
                            <th class="text-center">Client Id</th>
                            <th class="text-center">Assigned To</th>
                            <th class="text-center">Activity</th>
                            <th class="text-center">Meetings</th>
                            <th class="text-center">Calls</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($leads))
                        @foreach($leads as $lead)
                            <tr>
                                <td class="text-center">{{ $lead->created_at->format('d-m-Y')}}</td>
                                <td class="text-center">{{ $lead->id}}</td>
                                <td class="text-center">{{ App\User::getClient($lead->user->id) ? App\User::getClient($lead->user->id)->id : ""}}</td>
                                <td class="text-center">{{ ($lead->assigned_to != "") ? \App\User::find($lead->assigned_to)->name : "" }}</td>
                                <td class="text-center"><a class="btn btn-xs btn-info viewLeadHistory" data-toggle="modal" href="#viewLeadHistory" data-leadid='{{ $lead->id }}'>Activity</a></td>
                                <td class="text-center">{{ App\Models\Leads\LeadHistory::getHistory($lead->id,'meeting') }}</td>
                                <td class="text-center">{{ App\Models\Leads\LeadHistory::getHistory($lead->id,'call') }}</td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>



</div>
</div>

<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>By</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
 <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        var startDate = $('input[name="startdate"]').val(moment().subtract('days', 1));
        var endDate = $('input[name="enddate"]').val(moment());
        $('.reportrange').daterangepicker(
          {
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                  'Last 7 Days': [moment().subtract('days', 6), moment()],
                  'Last 30 Days': [moment().subtract('days', 29), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment().subtract('days', 1),
              endDate: moment(),
              opens: 'right'
          },
          function(start, end) {
              $('.reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
              $('input[name="startdate"]').val(start.format("YYYY-MM-DD"));
              $('input[name="enddate"]').val(end.format("YYYY-MM-DD"));

              var startDate = $('input[name="startdate"]').val();
              var endDate = $('input[name="enddate"]').val();
          }
      );

        $(document).on('click', '.viewLeadHistory', function(){
         
          $('.leadId').html("");
          $('.leadStatus').html("");
          $('.leadClient').html("");
          $('.leadClientPhone').html("");
          $('.leadProject').html("");
          $('.leadBudget').html("");
          $('.leadPropertyType').html("");
          $('.leadPropertySubType').html("");
          $('.leadSources').html("");
          $('.leadHistoryTable').html("");
          
          var leadId = $(this).data('leadid');
          $.ajax({
            url: '{{ url('admin/lead/leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {
              $('.leadId').html(data.lead.id);
              $('.lead_id').val(data.lead.id);
              $('.leadStatus').html(data.lead.status.name);
              $('.leadClient').html(data.lead.user.name);
              $('.leadClientPhone').html(data.lead.user.phone_mobile);
              $('.leadProject').html(data.lead.project.name);
              $('.leadBudget').html(data.lead.budget);
              $('.leadPropertyType').html(data.lead.property_type.name);
              $('.leadPropertySubType').html(data.lead.property_subtypes.name);
              $.each(data.lead.sources, function(index, val) {
                 $('.leadSources').append(val.name + ", ");
              });

              if(data.history != "")
              {
                $('#lead_created_date').html(data.lead.created_at);
                $('#lead_assigned_date').html(data.lead.assigned_date);
                $('#lead_assigned_by').html(data.lead.assigned_by.name);
                $('#lead_assigned_to').html(data.lead.assigned_to.name);

                $.each(data.history, function(index2, val2) {
                   $('.leadHistoryTable').append('<tr><td>'+val2.created_at+'</td><td>'+val2.status.name+'</td><td>'+val2.user.name+'</td><td>'+val2.remarks+'</td></tr>');
                });
              }
          })
          .fail(function() {
            console.log("error");
          });
          
        });

    
    // // Set the datepicker's date format
    // $.datepicker.setDefaults({
    //     dateFormat: 'yy-mm-dd',
    //     onSelect: function(dateText) {
    //         chart.xAxis[0].setExtremes($('input.highcharts-range-selector:eq(0)').datepicker("getDate").getTime(), $('input.highcharts-range-selector:eq(1)').datepicker("getDate").getTime()); 
    //         //this.onchange();
    //         this.onblur();
    //     }
    // });

        $('.datepicker').datepicker({
            dateFormat: 'dd-mm-yy'
        });
        
              var buttons = [
              {
                  extend: 'print',
                  customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );

                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                  }
              }];
          

        var datatable = $('#attendanceTable').DataTable({ 
          "bSort" : true,
          dom: 'Bfrtip',
          buttons: buttons
        } );


        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
        
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        
    </script>
@endsection