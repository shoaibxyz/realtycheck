@extends('admin.layouts.template')
@section('title','Deleted Clients')
@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('clients', 'active')
@section('clients_menu', 'display:block')
@section('clients-all', 'current-page')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Deleted Clients</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Deleted Clients</li>
        </ol>
    </div>
    <div class="content">
<div class="x_panel">
    <div class="x_title">
        <h2>Deleted Clients</h2>
         <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('clients.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
        <div class="clearfix">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    {{-- <th>Type</th> --}}
                    <th>Deleted By</th>
                    <th>Added Date</th>
                    <th>Actions</th>
                </tr>
            </thead>


            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($clients as $client)
                <tr>
                  <td>{{ $i }}</td>
                  <td>{{ $client->user->name }}</td>
                  <td>{{ $client->user->phone_mobile }}</td>
                  {{-- <td>{{ ($client->client_type) ? \App\Helpers::ClinetTypes($client->client_type) : "" }}</td> --}}
                  <td>{{ ($client->deleted_by) ? App\User::getUserInfo($client->deleted_by)->name : "" }}</td>
                  <td>{{ $client->created_at->format('d-m-Y') }}</td>
                  <td class="text-center">
                    <form method="post" action="{{ route('clients.restore', $client->id) }}" style="display: inline;">{{ csrf_field() }} <button type="submit" class="btn btn-danger btn-xs DeleteBtn restore" data-toggle="tooltip" title="Restore"><i class="fa fa-undo"></i></button></form>
                  </td>
                </tr>
                @php($i++)
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

@section('javascript')
    
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $(document).on('click','.DeleteBtn',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
