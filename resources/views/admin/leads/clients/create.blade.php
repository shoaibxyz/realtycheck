
@extends('admin.layouts.template')
@section('title','New Client')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">New Client</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">New Client</li>
        </ol>
    </div>
     <div class="content">
    
    <div class="x_panel">
        <div class="x_title">
            <h2>
                New Client 
            </h2>
            <div class="clearfix">
            </div>
        </div>
        <div class="row">
            @include('errors')
            <div class="col-sm-12">
                {!! Form::open(['method' => 'POST', 'route' => 'clients.store', 'class' => 'form-horizontal']) !!}
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Client Info</h2>
              
            <div class="row">  
               <div class="col-sm-6">
               <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Name <span class="required">*</span></label>
                    <div class="col-sm-7"> 
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Email <span class="required">*</span></label>
                    <div class="col-sm-7"> 
                    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
            
            </div>   
            
            <div class="row">  
                <div class="col-sm-6">        
                <div class="form-group">
                    {!! Form::label('cnic', 'CNIC', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                    {!! Form::text('cnic', null, ['class' => 'form-control', 'id' => 'cnic']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                    {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) !!}
                    </div>
                </div>
                </div>
            
            </div>    
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Mobile <span class="required">*</span></label>
                    <div class="col-sm-7"> 
                    {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile', 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('address', 'Address', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                    {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
                    </div>
                </div>
                </div>
            
            </div>   
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                    {!! Form::text('zip_code', null, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Countries', 'Country', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                        @php($CountriesArr[''] = 'Select Country')
                        @foreach($countries as $key => $country)
                            @php($CountriesArr[$country->id] = $country->name)
                        @endforeach
                        {!! Form::select('country_id', $CountriesArr, 0, ['class' => 'form-control', 'id' => 'country_id']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    
                    <label for="label" class="col-sm-5 control-label">Clinet Type <span class="required">*</span></label>
                    <div class="col-sm-7"> 
                        @php($ClinetTypesArr = [])
                        @foreach(\App\Helpers::ClinetTypes() as $key => $ClinetTypes)
                            @php($ClinetTypesArr[$key] = $ClinetTypes)
                        @endforeach
                        {!! Form::select('client_type', $ClinetTypesArr, 0, ['class' => 'form-control', 'id' => 'ClinetTypes', 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    
                    <label for="label" class="col-sm-5 control-label">Clinet Rating <span class="required">*</span></label>
                    <div class="col-sm-7"> 
                    @php($ClinetRatingArr = [])
                    @foreach(\App\Helpers::ClinetRating() as $key => $ClinetRating)
                        @php($ClinetRatingArr[$key] = $ClinetRating)
                    @endforeach
                    {!! Form::select('client_rating', $ClinetRatingArr, 0, ['class' => 'form-control', 'id' => 'ClinetRating', 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
            </div>    
                
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7"> 
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="future" name="future_interest">
                            Future Oppurtunity
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="local" name="local_interest">
                            Local Oppurtunity
                        </label>
                    </div>
                    </div>
                </div>
                </div>
            </div>    

                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('clients.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection


@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#propertyType').change(function(){
                $.ajax({
                    url: '{{ route('leads.getPropertySubType') }}',
                    type: 'POST',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {property_type_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#propertySubtype').html(data.html);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });   
        });
    </script>
@endsection