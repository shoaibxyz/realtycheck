@extends('admin.layouts.template')
@section('title','Edit Client')
@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('clients', 'active')
@section('clients_menu', 'display:block')
@section('clients-all', 'current-page')


@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Client</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Client</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Edit Client 
            </h2>
            <div class="clearfix">
            </div>
        </div>
        <div class="row">
            @include('errors')
            <div class="col-sm-12">
                {!! Form::model($client->user, ['method' => 'PATCH', 'route' => ['clients.update', $client->id], 'class' => 'form-horizontal']) !!}
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Edit Client Info</h2>
              
            <div class="row">
               <div class="col-sm-6">  
               <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Name <span class="required">*</span></label>
                    <div class="col-sm-7">
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Email <span class="required">*</span></label>
                    <div class="col-sm-7">
                    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">        
                <div class="form-group">
                    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7">
                    {!! Form::text('phone', $client->user->phone_res, ['class' => 'form-control', 'id' => 'phone']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Mobile <span class="required">*</span></label>
                    <div class="col-sm-7">
                    {!! Form::text('mobile', $client->user->phone_mobile, ['class' => 'form-control', 'id' => 'mobile' , 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
            </div> 
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('address', 'Address', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7">
                    {!! Form::text('address', $client->user->current_address, ['class' => 'form-control', 'id' => 'address']) !!}
                    </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7">
                    {!! Form::text('zip_code', null, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Countries', 'Country', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7">
                    @php($CountriesArr[''] = 'Select Country')
                    @foreach($countries as $key => $country)
                        @php($CountriesArr[$country->id] = $country->name)
                    @endforeach
                    {!! Form::select('country_id', $CountriesArr, $client->user->country_id, ['class' => 'form-control']) !!}
                    </div>
                </div>
                </div> 
                <div class="col-sm-6">
                <div class="form-group">
                     <label for="label" class="col-sm-5 control-label">Clinet Type <span class="required">*</span></label>
                    <div class="col-sm-7">
                    @php($ClinetTypesArr = [])
                    @foreach(\App\Helpers::ClinetTypes() as $key => $ClinetTypes)
                        @php($ClinetTypesArr[$key] = $ClinetTypes)
                    @endforeach
                    {!! Form::select('client_type', $ClinetTypesArr, $client->client_type, ['class' => 'form-control', 'id' => 'ClinetTypes' , 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
            </div> 
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    <label for="label" class="col-sm-5 control-label">Clinet Rating <span class="required">*</span></label>
                    <div class="col-sm-7">
                    @php($ClinetRatingArr = [])
                    @foreach(\App\Helpers::ClinetRating() as $key => $ClinetRating)
                        @php($ClinetRatingArr[$key] = $ClinetRating)
                    @endforeach
                    {!! Form::select('client_rating', $ClinetRatingArr, $client->client_rating, ['class' => 'form-control', 'id' => 'ClinetRating' , 'required' => 'required']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label'] ) !!}
                    <div class="col-sm-7">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="future" name="future_interest" {{($client->future_interest) ? "checked" : "" }}>
                                Future Oppurtunity
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="local" name="local_interest" {{($client->local_interest) ? "checked" : "" }}>
                                Local Oppurtunity
                            </label>
                        </div>
                    </div>
                </div>
                </div>
            </div>    

                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('clients.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</link>
@endsection