@extends('admin.layouts.template')
@section('title','Clients')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Clients List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Clients List</li>
        </ol>
    </div>
   
<div class="content">
    <div class="x_panel">
   
    <div class="x_title">
            <h2>Clients List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('clients.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix">
                 {{--  @include('errors') --}}
                </div> 
          </div> 
                @if(session()->has('message'))
                <div class="alert alert-success">
                {{ session()->get('message') }}
                </div>
                @endif
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Client ID</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Type</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($clients as $client)
                <tr>
                  <td>{{ $client->id }}</td>
                  <td>{{ $client->user->name }}</td>
                  <td>{{ $client->user->phone_mobile }}</td>
                  <td>{{ ($client->client_type) ? \App\Helpers::ClinetTypes($client->client_type) : "" }}</td>
                  <td class="text-center">
                    @if($rights->can_print)
                      <a href="{{ route('user-attachment.add', $client->user->id) }}" class="btn btn-xs btn-default">Attach File</a>
                    @endif
                    <!-- {{ link_to_route('clients.show', 'View', $client->id, ['class' => 'btn btn-xs btn-success']) }} -->
                    <a href="{{ route('clients.show', $client->id) }}" class="btn btn-xs btn-success view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
                    @if($rights->can_edit)
                      <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                    @endif
                    @if($rights->can_delete)
                        <form method="post" action="{{ route('clients.destroy', $client->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'> <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button></form>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $(document).on('click','.DeleteBtn',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
