@extends('admin.layouts.template')
@section('title','Clients')
@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('clients', 'active')
@section('clients_menu', 'display:block')
@section('clients-all', 'current-page')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Client</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Client</li>
        </ol>
    </div>
     <div class="content">

<div class="x_panel">
    <div class="x_title">
        <h2>Client</h2>
        <div class="clearfix">
        </div>
         @include('errors')
    </div>

    <div class="row">
      <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active">
            <a href="#ClientDetail" aria-controls="ClientDetail" role="tab" data-toggle="tab">Client Detail</a>
          </li>
          <li role="presentation">
            <a href="#documents" aria-controls="documents" role="tab" data-toggle="tab">Documents</a>
          </li>
        </ul>
      
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="ClientDetail">

            <div class="col-sm-4">  
              <h4>Personal Detail</h4>
              <div class='col-sm-4'>
                <span style="font-weight: bold">Name: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->name }}</div>

              <div class='col-sm-4'>
                <span style="font-weight: bold">email: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->email }}</div>


              <div class='col-sm-4'>
                <span style="font-weight: bold">phone: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->phone_res }}</div>


              <div class='col-sm-4'>
                <span style="font-weight: bold">Mobile: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->phone_mobile }}</div>


              <div class='col-sm-4'>
                <span style="font-weight: bold">zip Code: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->zip_code }}</div>

              <div class='col-sm-4'>
                <span style="font-weight: bold">country: </span>
              </div>
              <div class="col-sm-8">
                  {{-- {{ $client->user->country_id }} --}}
                {{ ($client->user->country_id != "") ? \App\Models\Country::getCountry($client->user->country_id)->name : "-" }}
              </div>
              <div class='col-sm-4'>
                <span style="font-weight: bold">Type: </span>
              </div>
              <div class="col-sm-8">{{ $client->client_type }}</div>
              <div class='col-sm-4'>
                <span style="font-weight: bold">Rating: </span>
              </div>
              <div class="col-sm-8">{{ $client->client_rating }}</div>
              <div class='col-sm-4'>
                <span style="font-weight: bold">Zip Code: </span>
              </div>
              <div class="col-sm-8">{{ $client->user->zip_code }}</div>
              <div class='col-sm-4'>
                <span style="font-weight: bold">Interest: </span>
              </div>
              <div class="col-sm-8">{{ $client->local_interest .' '. $client->future_interest }} </div>
            </div>

            <div class="col-sm-8">
              <h4>Notes <small class="pull-right">{{ link_to_route('user-attachment.add', 'Add Note', $client->user->id, ['class' => 'btn btn-xs btn-success']) }}</small></h4>
              <table class="table table-bordered table-hover jambo_table">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Notes</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notes as $note)
                  @if($note->title != "")
                  <tr>
                    <td>{{ $note->title }}</td>
                    <td>{{ $note->notes }}</td>
                    <td>{{ $note->created_at->format('d-m-Y') }}</td>
                    <td>
                      {{ link_to_route('user-attachment.edit', 'Edit', $note->id, ['class' => 'btn btn-xs btn-info']) }}
                      <form method="post" action="{{ route('user-attachment.destroy', $note->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'> <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'></form>
                    </td>
                  </tr>

                  @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="documents">
            <div class="col-sm-12">
              <h4>Documents <small class="pull-right">{{ link_to_route('user-attachment.add', 'Add Document', $client->user->id, ['class' => 'btn btn-xs btn-success']) }}</small></h4>
              <table class="table table-bordered table-hover jambo_table">
                <thead>
                  <tr>
                    <th>Sr. No</th>
                    <th>Size</th>
                    <th>Type</th>
                    <th>Date Added</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @php($i=1)
                  @foreach($notes as $note)
                  @if($note->attachment)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>
                      {{-- @if(file_exists(asset('storage/app/leads/attachments/'.$note->attachment))) --}}
                      {{ number_format((float)\Storage::getSize('leads/attachments/'.$note->attachment)/1024, 2, '.', '') . ' KB' }}
                      {{-- @endif --}}
                    </td>

                      <td>
                        {{-- @if(file_exists(asset('storage/app/leads/attachments/'.$note->attachment))) --}}
                          {{ \Storage::mimeType('leads/attachments/'.$note->attachment) }}
                        {{-- @endif --}}
                      </td>
                      <td>{{ $note->created_at->format('d-m-Y') }}</td>
                      <td>
                        <a href="{{ asset('storage/app/leads/attachments/'.$note->attachment) }}" target="_blank" class="btn btn-xs btn-success">Download</a>
                        <form method="post" action="{{ route('user-attachment.destroy', $note->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'> <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'></form>
                      </td>
                    </tr>
                  @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> 
    </div>


    <div class="row" style="margin-top: 50px;">
        <div class="col-sm-12">
          <h4>Leads <small class="pull-right">{{ link_to_route('leads.create', 'Add Lead', null, ['class' => 'btn btn-xs btn-success']) }}</small></h4>
          <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Source</th>
                    <th>Project</th>
                    <th>Added</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $lead->status_name }}</td>
                  <td>{{ $lead->name }}</td>
                  <td>{{ ($lead->client_type) ? \App\Models\Helpers::ClinetTypes($lead->client_type) : "" }}</td>
                  <td>
                  <?php $sources = \App\Models\Leads\Source::getLeadSources($lead->lead_id); ?>
                  @foreach($sources as $source)
                    {{ \App\Models\Leads\Source::getSources($source->source_id)->name.', ' }}
                  @endforeach
                  </td>
                  <td>{{ $lead->project_name }}</td>
                  <td>{{ date('d-m-Y', strtotime($lead->created_at)) }}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
        </div>
      </div>
</div>
@endsection

@section('javascript')
    
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
