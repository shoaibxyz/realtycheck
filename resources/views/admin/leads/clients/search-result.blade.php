@extends('admin.layouts.template')
@section('title','Lead Search Result')
@section('lead-clients', 'active')
@section('child_menu', 'display:block')
@section('clients_menu', 'display:block')
@section('clients-search', 'current-page')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Search Clients</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Search Clients</li>
        </ol>
    </div>
    <div class="content">
<div class="x_panel">
    <div class="x_title">
        <h2>
            Lead Search Result
        </h2>
        <div class="clearfix">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($clients as $client)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $client->name }}</td>
                  <td>{{ $client->phone_mobile }}</td>
                  <td>{{ ($client->client_type) ? \App\Models\Helpers::ClinetTypes($client->client_type) : "" }}</td>
                  <td><a href="mailto:{{ $client->email }}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>  </td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

@section('javascript')
    
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
