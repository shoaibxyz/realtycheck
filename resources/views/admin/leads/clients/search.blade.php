@extends('admin.layouts.template')
@section('title','Search Clients')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection


@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Search Clients</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Search Clients</li>
        </ol>
    </div>
    <div class="content">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Search Clients
            </h2>
            <div class="clearfix">
            </div>
        </div>
        <div class="row">
            @include('errors')
            <div class="col-sm-12">   
                {!! Form::open(['method' => 'POST', 'route' => 'clients.searchLead', 'class' => 'form-horizontal']) !!}
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Client Info</h2>
            
            <div class="row">
                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('client_id', 'ID', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('client_id', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('name', 'name', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
                        </div>
                     </div>
               </div> 
            </div>   
            
            <div class="row">
                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('email', 'email', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                        </div>
                    </div>
               </div> 

                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('phone', 'phone', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) !!}
                        </div>
                    </div>
               </div> 
            </div>   
            
            <div class="row">
                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('mobile', 'mobile', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile']) !!}
                        </div>
                    </div>
               </div> 

                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('address', 'address', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
                        </div>
                    </div>
               </div> 
            </div> 
            
            <div class="row">
                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('zip_code', 'zip code', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        {!! Form::text('zip_code', null, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                        </div>
                    </div>
               </div> 

                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('ClinetRating', 'Clinet Rating', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        @php($ClinetRatingArr = [])
                        @foreach(\App\Helpers::ClinetRating() as $key => $ClinetRating)
                            @php($ClinetRatingArr[$key] = $ClinetRating)
                        @endforeach
                        {!! Form::select('client_rating', $ClinetRatingArr, 0, ['class' => 'form-control', 'id' => 'ClinetRating']) !!}
                        </div>
                    </div>
                </div>
            </div>    
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('ClinetTypes', 'Clinet Type', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        @php($ClinetTypesArr = [])
                        @foreach(\App\Helpers::ClinetTypes() as $key => $ClinetTypes)
                            @php($ClinetTypesArr[$key] = $ClinetTypes)
                        @endforeach
                        {!! Form::select('client_type', $ClinetTypesArr, 0, ['class' => 'form-control', 'id' => 'ClinetTypes']) !!}
                        </div>
                    </div>
                </div>
            

                <div class="col-sm-6">    
                    <div class="form-group">
                        {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label']) !!}
                        <div class="col-sm-7">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="future_interest">
                                Future Oppurtunity
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="local_interest">
                                Local Oppurtunity
                            </label>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
 
                    <div class="form-actions">
                        {!! Form::submit("Search", ['class' => 'btn btn-warning pull-right']) !!}
                    </div>    
                    
               
            {!! Form::close() !!}
        </div>
    </div></div>
    </div>
</link>
@endsection


@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function($) {
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $('#propertyType').change(function(){
                $.ajax({
                    url: '{{ route('leads.getPropertySubType') }}',
                    type: 'POST',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {property_type_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#propertySubtype').html(data.html);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });   
        });
    </script>
@endsection