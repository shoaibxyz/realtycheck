@extends('admin.layouts.template')
@section('title','Lead Sources')
@section('lead-clients', 'active')
@section('lead_clients_menu', 'display:block')
@section('leadSourceCreate', 'active')
@section('source_menu', 'display:block')
@section('source', 'active')
@section('leadSource', 'current-page')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Lead Source</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Lead Source</li>
        </ol>
    </div>
     <div class="content">
        <div class="x_panel">
 <div class="x_title">
                <h2>Edit Lead Source</h2>
                
                <div class="clearfix"></div>
            </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-sm-6 col-sm-offset-3">
            @include('errors')
             {!! Form::model($lead, ['method' => 'POST', 'route' => ['leads.sources.update', $lead->id], 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                    </div>
                </div>

                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('leads.sources') }}" class="btn btn-grey">Cancel</a>
                </div>
            {!! Form::close() !!}
        </div>
        </div>
    </div>
</link>
@endsection
