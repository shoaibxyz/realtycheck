@extends('admin.layouts.template')
@section('title','Leads')

@section('style')
  <link type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" rel="stylesheet" />
  <link type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" />
@endsection
@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Leads List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('leads.dashboard')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Leads List</li>
        </ol>
    </div>
     <div class="content">
<div class="x_panel">
    <div class="x_title">
            <h2>Leads List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div>    
    <div class="row">
            <div class="col-sm-12" style="float: right; position: relative; z-index: 1000;">
                
                <form method="POST" id="search-form" class="form-horizontal align-right" role="form">
                      <div class="col-sm-3">
                      <input type="text" id="query" class="form-control" placeholder="Name">
                    </div>
                      <div class="col-sm-3">
                      <input type="text" id="mobile_no" class="form-control" placeholder="mobile no">
                    </div>
                      <div class="col-sm-3">
                        <select name="status" id="status" class="form-control">
                            <option value="">Select Status</option>
                        @foreach($leadStatus as $ls)
                            <option value="{{ $ls->id }}">{{ $ls->name }}</option>
                        @endforeach
                        </select>
                      </div>
                     
                      <div class="col-sm-3">
                      <input type="text" id="lead_id" class="form-control" placeholder="lead id">
                      </div>
                      
                      
                      <div class="col-sm-3" style="padding-top:5px; padding-bottom:5px;">
                          
                      <select id="leadtype" class="form-control">
                          <option selected>Select Lead Type</option>
                          <option value="0" >All</option>
                          <option value="1">Open</option>
                          <option value="2">Assigned</option>
                      </select>
                      
                      </div>
                      
                      
                      
                </form>
            </div>
        </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="leadsTable">
            <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Status</th>
                    <th>Client</th>
                    <th>Mobile</th>
                    <th>Query</th>
                    <th>Interest</th>
                    <th>campaign</th>
                    <th>Assigned Date</th>
                    <th>Assigned To</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              {{-- @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ ($lead->lead_status_id != "")  ? $lead->status->name : "" }}</td>
                  <td>{{ $lead->user->name }}</td>
                  <td>{{ $lead->user->phone_mobile }}</td>
                   
                  <td>{{ ($lead->project_id) ? $lead->project->name : "" }}</td>
                  <td>RS {{ number_format($lead->budget) }}</td>
                  <td>{{ ($lead->assignedTo != "") ? $lead->assignedTo->name : ""}}</td>
                  
                  <td>
                    {!! Form::select('assign_to', $saleOfficersArr, 0, ['class' => 'form-control assignTo', 'data-lead_id' => $lead->id, 'style' => 'width: 200px']) !!}
                  </td>
                 <td class="text-center">
                  
                  </td>
                </tr>
              @endforeach --}}

            </tbody>
            
        </table>
       
        <form id="frm-example"> 
            <input data-toggle="modal" href='#assignLeadModel' type="submit" value="Assign Leads" id="save_value" class="btn btn-primary btn-xs"> 
          </form> 
    </div>
</div>
</div>

{{-- <a class="btn btn-primary" >Trigger modal</a> --}}
<div class="modal fade" id="viewLeadModel">
  <div class="modal-lg" style="width:70%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <!-- <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Lead Info</h2>
        <div class="row">
            <div class="col-sm-6">
                <label class="col-sm-5 control-label">Classification</label>
                <div class="col-sm-7"> 
                    
                </div>
            </div>
            <div class="col-sm-6">
                
            </div>
        </div>  
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>-->
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateLeadModel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Followup</h4>
      </div>
      {!! Form::open(['route' => 'leads.changeStatus', 'id' => 'changeLeadsStatus']) !!}
      <div class="modal-body form-horizontal">
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Lead Info</h2>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Lead Id</label>
                    <div class="col-sm-7 leadId" id="status_lead_id"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Status</label>
                    <div class="col-sm-7 leadStatus val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Name</label>
                    <div class="col-sm-7 leadClient val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Phone</label>
                    <div class="col-sm-7 leadClientPhone val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Project</label>
                    <div class="col-sm-7 leadProject val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Budget</label>
                    <div class="col-sm-7 leadBudget val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Type</label>
                    <div class="col-sm-7 leadPropertyType val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Sub Type</label>
                    <div class="col-sm-7 leadPropertySubType val"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Sources</label>
                    <div class="col-sm-7 leadSources val"> 
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group" style="margin-bottom:2px;">
                    <label class="col-sm-5 control-label control-label">Campaign</label>
                    <div class="col-sm-7 leadCampaign val"> 
                    </div>
                </div>
            </div>
        </div>
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Meeting Info</h2>
        <div class="row">
           <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting date</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('meeting_date', date('d/m/Y'), ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_date', 'readonly' => 'readonly']) !!} 
                  </div>
              </div>
          </div>
          <div class="col-sm-6">   
              <div class="form-group">
                   <label class="col-sm-5 control-label">Meeting Channel <span class="required" style="color:red;">*</span></label>
                   <div class="col-sm-7 val"> 
                      <?php 
                      $meeting_channel[''] = 'Choose One';
                      $meeting_channel['sms'] = 'SMS';
                      $meeting_channel['email'] = 'Email';
                      $meeting_channel['whatsapp'] = 'Whatsapp';
                      $meeting_channel['call'] = 'Call';
                      ?>
                      {!! Form::select('meeting_channel', $meeting_channel, 0,  ['class' => 'form-control meeting_channel', 'required'=>'required', 'id' => 'meeting_channel']) !!} 
                    </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting By</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('current_meeting_by', Auth::user()->name , ['class' => 'form-control current_meeting_by', 'required'=>'required', 'id' => 'current_meeting_by', 'readonly' => 'readonly']) !!} 
                    {!! Form::hidden('meeting_done_by', Auth::user()->id) !!}
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting Detail</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('meeting_detail', null, ['class' => 'form-control meeting_detail', 'waive_days'=>'required', 'id' => 'meeting_detail']) !!} 
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Call In / Call Out</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::radio('in_out', 0,  true ) !!} Out
                    {!! Form::radio('in_out', 1,  false ) !!} In
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
                <label class="col-sm-5 control-label">Status <span class="required" style="color:red;">*</span></label>
                <div class="col-sm-7 val"> 
                    <select class="form-control lead_status" name="lead_status" id="lead_status" style="width: 100%" required>

                <option value=''>Choose One</option>
                @foreach($leadStatus as $status)
                  <option value="{{ $status->id}}"> {{ $status->name }}</option>
                @endforeach  
              </select>
                </div>
          </div>
        </div>
        
        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Next Call / Meeting</h2>
        <div class="row">  
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Meeting Status</label>
                  <div class="col-sm-7 val"> 
                      {!! Form::radio('meeting_status', 1, false) !!} Done
                      {!! Form::radio('meeting_status', 0, true) !!} Pending
                  </div>
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting date</label>
                  <div class="col-sm-7 val"> 
                    {!! Form::text('next_meeting_date', null, ['class' => 'form-control datepicker next_meeting_date', 'id' => 'next_meeting_date']) !!} 
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting Channel</label>
                  <div class="col-sm-7 val">
                      <?php 
                              $next_meeting_channel[''] = 'Choose One';
                              $next_meeting_channel['sms'] = 'SMS';
                              $next_meeting_channel['email'] = 'Email';
                              $next_meeting_channel['whatsapp'] = 'Whatsapp';
                              $next_meeting_channel['call'] = 'Call';
                      ?>
    
                      {!! Form::select('next_meeting_channel', $next_meeting_channel, 0,  ['class' => 'form-control next_meeting_channel', 'id' => 'next_meeting_channel']) !!} 
                  </div>
              </div>
          </div>
          <div class="col-sm-6">   
                <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting By</label>
                  <div class="col-sm-7 val">
                      {!! $assign_to_html !!}
                      {{-- {!! Form::select('next_meeting_by', $ccdArr, 0, ['class' => 'form-control', 'id' => 'next_meeting_by']) !!}  --}}
                    </div>  
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">   
              <div class="form-group">
                  <label class="col-sm-5 control-label">Next Meeting Agenda</label>
                  <div class="col-sm-7 val">
                    {!! Form::text('next_meeting_agenda', null, ['class' => 'form-control next_meeting_agenda', 'id' => 'next_meeting_agenda']) !!} 
                   </div> 
              </div>
          </div>
        </div>

        <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px; text-align:left;">Remarks</h2>
          <div class="form-group">
            <label>Remarks</label>  
            <textarea class="form-control" name='remarks' id="remarks"></textarea>
            <input type="hidden" class="lead_id">
          </div>
      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="assignLeadModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Assign Selected Leads</h4>
      </div>
      {!! Form::open(['id' => 'assignLeadModelForm']) !!}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 selectedLeads"></div>
          <div id="leads"></div>
          <textarea class="form-control" rows="5" name = "comment" id="comment"></textarea>
        </div>
        
        <hr>
        <div class="row">
          <div class="form-group">
            <label class="col-sm-4">Assign Leads To</label>  
            <div class="col-sm-8">
               <div class="form-group">
                  
                  {!! $assign_to_html !!}
                  
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
        {{ Form::hidden('yourhiddenfieldname', '', array('id' => 'companyIdHidden')) }}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="assignedLeads">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">User Leads</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <h4>User: <span style="font-weight: bold;" class="username"></span> <span id="countLeads"></span></h4>
          </div>  
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Lead Id</th>
                <th>Customer</th>
                <th>Phone</th>
                <th>Assigned Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="userLeadsTable">
            </tbody>
          </table>    
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Meeting Date</th>
                  <th>Status</th>
                   
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                        <th>Next Meeting Date</th>
                        <th>Next Meeting Detail</th>
                        <th>Next Meeting By</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        
        $(function(){
      $('#save_value').click(function(){
          $('#comment').empty();
        var val = [];
        $(':checkbox:checked').each(function(i){
            val[i] = $(this).val();
            // alert(val[i]);
         
                      
                         $('#comment').append(val[i]+',');
                      
        });
      });
    });
        
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy',
        });


        var meeting_status = null;
        $("input[name='meeting_status']").click(function() {
            meeting_status = this.value;

            if(meeting_status == 1)
            {
                $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
            }else{
                $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
            }
        });

         var columns = [
            
            { data: 'checkbox', name: 'checkbox' },
            { data: 'id', name: 'id' },
            { data: 'status', name: 'status', className : 'status' },
            { data: 'name', name: 'name'},
            { data: 'phone_mobile', name: 'phone_mobile'},
            { data: 'query', name: 'query'},
            { data: 'interest', name: 'interest'},
            { data: 'campaign_name', name: 'campaign_name'},
            { data: 'assigned_date', name: 'assigned_date'},
            { data: 'assigned_to', name: 'assigned_to'},
        ];

        columns.push({ data: 'actions', name: 'actions'});

        var table = $('#leadsTable').DataTable({
             'columnDefs': [
               {
                  'targets': 0,
                  'checkboxes': true
               }
            ],
            'order': [[1, 'asc']],
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "bStateSave": true,
            "bFilter": false,
            "info": true,
            "columns": columns,
            ajax: {
                url: '{!! route('leads.getLeads') !!}',
                data: function (d) {
                    d.query = $('#query').val();
                    d.mobile_no = $('#mobile_no').val();
                    d.lead_id = $('#lead_id').val();
                    d.status = $('#status').val();
                    d.leadtype=$('#leadtype').val();
                }
                
            },
            fnCreatedRow: function( nRow, aData, iDataIndex ) {
              $(nRow).attr('id', "ROW_" + aData.lead_id);
            }
        });


        $('#changeLeadsStatus').submit(function(e){
          e.preventDefault();
          var lead_status = $('#lead_status').val();

          var remarks = $('#remarks').val();
        //   var lead_id = $('#status_lead_id').val();
        //   alert(lead_id);
          var lead_id = $('#status_lead_id').text();
        //   alert(lead_id);
          var inputData = $(this).serialize() + "&lead_id="+lead_id+"&remarks="+remarks;
        //   alert(inputData);
          $.ajax({
            url: '{{ route('leads.changeStatus') }}',
            type: 'POST',
            data: inputData,
            beforeSend: function() {
              $('.loader').removeClass('hide');
            }, 
            complete: function(data) {
                $('.loader').addClass('hide');
            },
          })
          .done(function(data) {

            $('#ROW_'+ lead_id).remove();
            // $('#ROW_'+ lead_id + " .status").html(data.leadStatus.name);
            swal("Success!", "Lead has been updated",'success');
            $('#updateLeadModel').modal('toggle');

          })
          .fail(function() {
            console.log("error");
          })
          location.reload(true);
        });

        $('#frm-example').on('submit', function(e){
          e.preventDefault();
            var form = this;
            $('.selectedLeads').html('');

            var rows_selected = table.column(0).checkboxes.selected();

            $.each(rows_selected, function(index, rowId){
              $('.selectedLeads').append(rowId + ',');
               // Create a hidden element
                // $(form).append(
                //   $('<input>')
                //       .attr('type', 'hidden')
                //       .attr('name', 'id[]')
                //       .val(rowId)
                // );
            });
         });


         $('#query, #mobile_no, #lead_id').on('keyup', function(e) {
          // if(e.keyCode == 13)
            e.preventDefault();
            table.draw();

        });

        $('#query, #mobile_no, #lead_id').on('keypress', function(e) {
          if(e.keyCode == 13){
            e.preventDefault();
            table.draw();

          }
        });
        
        $('#status').on('change', function(e) {
          e.preventDefault();
          table.draw();
        });
        
        $('#leadtype').on('change', function(e) {
          e.preventDefault();
          var v=$(this).val();
           if(v==0 || v==1 || v==2){
               console.log(v);
            table.draw();   
           }
        });
        
        

   
   $(function() {
    $('.assignTo').change(function() {
        
        var x = $(this).val();
        // alert(x);
        $('#companyIdHidden').val(x);
    });
});

        $(document).on('submit','#assignLeadModelForm', function(e){
          e.preventDefault();
          
          $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });

          var assignTo = $('#companyIdHidden').val();
          
        //   alert(assignTo);

          if(assignTo != "")
          {
            //   alert(assignTo);
              
            var lead_id = $('#comment').text();
            // var lead_idss = lead_ids.replace(/,\s*$/, "");
            // var lead_id = lead_idss.split(',');
            // console.log(lead_id);
            if(lead_id == "")
            {
              return swal("Oops!","Please select a lead to assign",'error');
            }
           
            $.ajax({
                
               url: '{{ route('leadUpdate') }}',
              type: 'POST',
              data: $('#assignLeadModelForm').serialize(),
            })
            .done(function(data) {
            //   table.draw();
            //   alert(data.msg);
              console.log(data);
             swal('success', 'Lead assigned.', 'success');
              $('#assignLeadModel').modal('hide');

            })
            .fail(function() {
              console.log("error");
            });
          }
        });
        
        $(document).on('click','.viewLead',function(e){
            var leadId = $(this).data('leadid');
            console.log(leadId); 
            $.ajax({
                url: '{{ url('admin/leads/view') }}/'+leadId,
                type: 'GET'
              })
              .done(function(data) {
                  $('#viewLeadModel .modal-body').html(data);
              })
            .fail(function() {
                console.log("error");
            });
        })

        $(document).on('click', '.viewFU, .updateLeadModel, .viewLeadHistory', function(){
        //  alert($(this).data('leadid'));
          $('.leadId').html("");
          $('.leadStatus').html("");
          $('.leadClient').html("");
          $('.leadClientPhone').html("");
          $('.leadProject').html("");
          $('.leadBudget').html("");
          $('.leadPropertyType').html("");
          $('.leadPropertySubType').html("");
          $('.leadSources').html("");
          $('.leadHistoryTable').html("");
          $('.meeting_channel').select2().val("").trigger('change');
          $('.meeting_detail').html("");
          $('.lead_status').select2().val("").trigger('change');
          $('.next_meeting_date').html("");
          $('.next_meeting_channel').select2().val("").trigger('change');
          // $('.next_meeting_channel').val("");
          $('.next_meeting_agenda').html("");

          var leadId = $(this).data('leadid');
          $.ajax({

            url: '{{ url('admin/leads/show') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {

            console.log(data);

              $('.leadId').html(data.lead.id);
              $('.lead_id').val(data.lead.id);
              $('.leadStatus').html(data.lead.status.name);
              $('.leadClient').html(data.lead.user.name);
              $('.leadClientPhone').html(data.lead.user.phone_mobile);
              $('.leadBudget').html(data.lead.budget);
              // $('.meeting_channel').val("");
              $('.meeting_channel').select2().val("").trigger('change');
              $('.meeting_detail').html(data.lead.status.name);
              $('.lead_status').select2().val(null).trigger('change');
              $('.next_meeting_date').html("");
              $('.next_meeting_channel').select2().val("").trigger('change');
              // $('.next_meeting_channel').val("");
              $('.next_meeting_agenda').html("");
              if(data.lead.project_id != null)
                $('.leadProject').html(data.lead.project.name);

                if(data.lead.source_id != null)
              $('.leadSources').append(data.leadSource.name);
              
                
              if(data.lead.property_type)
                $('.leadPropertyType').html(data.lead.property_type.name);
              
              if(data.lead.property_subtypes)
                $('.leadPropertySubType').html(data.lead.property_subtypes.name);

               $('#lead_created_date').html(data.lead.created_at);
              $('#lead_assigned_date').html(data.lead.assigned_date);
              
              if(data.lead.assigned_by)
                $('#lead_assigned_by').html(data.lead.assigned_by.name);
              
              if(data.lead.assigned_to)
                $('#lead_assigned_to').html(data.lead.assigned_to.name);

              if(data.history != "")
              {
                $.each(data.history, function(index2, val2) {
                   $('.leadHistoryTable').append('<tr><td>'+val2.meeting_date+'</td><td>'+data.lead.status.name+'</td><td>'+val2.meeting_detail+'</td><td>'+val2.meeting_by.name+'</td><td>'+val2.next_meeting_date+'</td><td>'+val2.next_meeting_agenda+'</td><td>'+data.next_meeting_By[index2].name+'</td></tr>');
                });
              }
          })
          .fail(function() {
            console.log("error");
          });
          
        });

        $(document).on('click', '.assignedLeads', function(){
          $('.username').html("");
          var leadId = $(this).data('userid');
          $.ajax({
            url: '{{ url('admin/lead/leads/show-user-leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {
              $('#userLeadsTable').html("");
              $('.username').html(data.user.name);
              // $('.leadId').html(data.lead.id);
              // $('.leadStatus').html(data.lead.status.name);
              // $('.leadClient').html(data.lead.user.name);
              // $('.leadClientPhone').html(data.lead.user.phone_mobile);
              // $('.leadProject').html(data.lead.project.name);
              // $('.leadBudget').html(data.lead.budget);
              // $('.leadPropertyType').html(data.lead.property_type.name);
              var i = 1;
              $.each(data.leads, function(index, val) {
                 $('#userLeadsTable').append("<tr><td>"+val.id+"</td><td>"+val.user.name+"</td><td>"+val.user.phone_mobile+"</td><td>"+val.assigned_date+"</td><td>"+val.status.name+"</td></tr>");
                 i++;
              });
              $('#countLeads').html("("+parseInt(i-1)+")");
              table.draw();
              // $('.assign_to').html(data.saleOfficer);
          })
          .fail(function() {
            console.log("error");
          });
          
        });


        

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });


$(document).ready(function() {
    $('#updateLeadModel').on('hidden.bs.modal', function(e) {
    $(this).find('form').trigger('reset');
})


  
});

// $(document).on('hide.bs.modal', '#updateLeadModel', function (e) {
        
//     //     $dropdown = $("#lead_status");
    
//     // $dropdown[0].selectedIndex = -1;
//         $("#lead_status option:first").attr("selected","selected");
//         $('#select2-lead_status-container').val('');
//         $('#remarks').val('');
//         $('#meeting_detail').val('');
//         $('#next_meeting_date').val('');
//         $('#next_meeting_agenda').val('');
//     });
</script>
@endsection
