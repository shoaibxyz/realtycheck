@extends('admin.layouts.template')
@section('title','Edit Lead')
@section('lead-clients', 'active')
@section('leads', 'active')
@section('lead_clients_menu', 'display:block')
@section('lead_menu', 'display:block')
@section('leads-all', 'current-page')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit List</li>
        </ol>
    </div>
     <div class="content">
    <div class="x_panel">
    <div class="x_title">
            <h2>Edit Lead</h2>
            
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div>    
        <div class="row">
            @include('errors')
            
            <div class="col-sm-12">
                {!! Form::model($lead, ['method' => 'PATCH', 'route' => ['leads.update', $lead->id], 'class' => 'form-horizontal']) !!}
                <h2 style="background-color: #d58512; font-size: 30px; color: #fff; padding: 5px 20px; border-radius:2px;">Edit Lead Info</h2>
                
            <div class="row"> 
            
              <div class="col-sm-6">
              <div class="form-group">
                    {!! Form::label('classification', 'Classification *', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($classificationArr = [])
                    @foreach(\App\Helpers::Classification() as $key => $classification)
                        @php($classificationArr[$key] = $classification)
                    @endforeach
                    {!! Form::select('classification', $classificationArr, $lead->classification, ['class' => 'form-control', 'id' => 'classification']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Purpose', 'Purpose *', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($PurposeArr = [])
                    @foreach(\App\Helpers::Purpose() as $key => $Purpose)
                        @php($PurposeArr[$key] = $Purpose)
                    @endforeach
                    {!! Form::select('purpose', $PurposeArr, $lead->purpose, ['class' => 'form-control', 'id' => 'Purpose']) !!}
                    </div>
                </div>
                </div>
                
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('propertyType', 'Property Type', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($propertyTypesArr = [])
                    @php($propertyTypesArr[''] = 'Select Type')
                    @foreach($propertyTypes as $key => $types)
                        @php($propertyTypesArr[$types->id] = $types->name)
                    @endforeach
                    {!! Form::select('property_type_id', $propertyTypesArr, $lead->property_type_id, ['class' => 'form-control', 'id' => 'propertyType']) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('propertySubtype', 'Property Sub Type', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($propertySubTypesArr  = [])
                    @php($propertySubTypesArr [''] = 'Select Sub Type')
                    @foreach($propertySubTypes as $key => $typess)
                        @php($propertySubTypesArr[$typess->id] = $typess->name)
                    @endforeach
                    {!! Form::select('property_subtype_id', $propertySubTypesArr, $lead->property_subtype_id, ['class' => 'form-control', 'id' => 'propertySubtype']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group ">
                    <label class="col-sm-5 control-label">Source</label>
                    <div class="col-sm-7">
                    @php($lsourcesArr[''] = 'Select Source')
                    @foreach($LeadSource as $key => $project)
                        @php($lsourcesArr[$project->id] = $project->name)
                    @endforeach
                    {!! Form::select('source_id', $lsourcesArr, $lead->source_id, ['class' => 'form-control', 'id' => 'source_id' ]) !!}
                    </div>
                </div>
                </div>
               <div class="col-sm-6">
               <div class="form-group ">
                    <label class="col-sm-5 control-label">Campaign</label>
                    <div class="col-sm-7">
                    @php($camArr[''] = 'Select Campaign')
                    @foreach($campaign as $key => $cam)
                        @php($camArr[$cam->id] = $cam->name)
                    @endforeach
                    {!! Form::select('campaign', $camArr, $lead->campaign_id, ['class' => 'form-control', 'id' => 'campaign_id' ]) !!}
                    </div>
                </div> 
                </div>
            </div>    

              {{--   <div class="form-group">
                   <label class="col-sm-3 control-label">Campaigns</label>
                    <div class="col-sm-9">
                    <select id="campaign" class="form-control input-sm" name="campaign">
                        <option value=""></option>
                   </select>
               
                    
                </div></div> --}}

            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Projects', 'Project', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($projectsArr[''] = 'Select Project')
                    @foreach($projects as $key => $project)
                        @php($projectsArr[$project->id] = $project->name)
                    @endforeach
                    {!! Form::select('project_id', $projectsArr, old('project_id'), ['class' => 'form-control', 'id' => 'propertyType']) !!}
                    </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Beds', 'Beds', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($BedsArr = [])
                    @foreach(\App\Helpers::Beds() as $key => $Beds)
                        @php($BedsArr[$key] = $Beds)
                    @endforeach
                    {!! Form::select('beds', $BedsArr, $lead->beds, ['class' => 'form-control', 'id' => 'Beds']) !!}
                    </div>
                </div>
                </div>
            </div>    

                        <div class="row">   
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('land_area', 'Land Area', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    <div class="row">
                    <div class="col-sm-4">
                        {!! Form::text('land_min_area', $lead->land_min_area, ['class' => 'form-control', 'placeholder' => 'Min Land']) !!}
                    </div>

                    <div class="col-sm-4">
                        {!! Form::text('land_max_area', $lead->land_max_area, ['class' => 'form-control', 'placeholder' => 'Max Land']) !!}
                    </div>    

                    <div class="col-sm-4">
                        @php($LandUnitsArr = [])
                        @foreach(\App\Helpers::LandUnits() as $key => $LandUnits)
                            @php($LandUnitsArr[$key] = $LandUnits)
                        @endforeach
                        {!! Form::select('land_unit', $LandUnitsArr, $lead->land_unit, ['class' => 'form-control', 'id' => 'Beds']) !!}
                    </div>   
                     
                    </div> 
                    </div> 
                </div>
                </div>
                
                <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('interest', 'Interest', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('interest',$lead->interest, ['class' => 'form-control', 'id' => 'interest']) !!}
                    </div>
                </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                {!! Form::label('budget', 'Budget', ['class' => 'col-sm-5 control-label']) !!}
                <div class="col-sm-7">
                {!! Form::text('budget', null, ['class' => 'form-control', 'id' => 'budget']) !!}
                </div>
                </div>    
                </div>
               
               <div class="col-sm-6">
               <div class="form-group">
                    {!! Form::label('subject', 'subject', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('subject', null, ['class' => 'form-control', 'id' => 'subject']) !!}
                    </div>
                </div>
                </div>
            </div> 
        
            <div class="row">
               <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description','rows' => 5]) !!}
                    </div>
                </div>
                </div>
                
                <div class="col-sm-6">   
                  <div class="form-group">
                    {!! Form::label('customer_feedback', 'Customer Feedback', ['class' => 'col-sm-5 control-label']) !!}
                  <div class="col-sm-7">
                    {!! Form::textarea('customer_feedback_after_visit', $lead->customer_feedback_after_visit, ['class' => 'form-control', 'id' => 'customer_feedback', 'rows' => 5]) !!}
                  </div>
                  </div>
                </div>
            </div>
            
        <div class="row">
            <div class="col-sm-6"> 
              <div class="form-group">
               {!! Form::label('whatsapp_sent_on', 'Whatsapp Sent On', ['class' => 'col-sm-5 control-label']) !!}
              <div class="col-sm-7">
               {!! Form::text('whatsapp_sent_on', $lead->whatsapp_sent_on,  array( 'class'=>'form-control datepicker') )!!}
              </div>
              </div>
            </div>
            
            <div class="col-sm-6">
                <div class="form-group">
                 {!! Form::label('visit_promise_date', 'Visit Promise Date', ['class' => 'col-sm-5 control-label']) !!}
                 <div class="col-sm-7">
               {!! Form::text('visit_promise_date', $lead->visit_promise_date,  array( 'class'=>'form-control datepicker') )!!}
                </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6"> 
            <div class="form-group">
                {!! Form::label('name', 'name', ['class' => 'col-sm-5 control-label']) !!}
            <div class="col-sm-7">
                {!! Form::text('name', $lead->user->name, ['class' => 'form-control', 'id' => 'name']) !!}
            </div>
            </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                 {!! Form::label('email', 'email', ['class' => 'col-sm-5 control-label']) !!}
                <div class="col-sm-7">
                 {!! Form::email('email', $lead->user->email, ['class' => 'form-control', 'id' => 'email']) !!}
                </div>
                </div>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-sm-6"> 
                <div class="form-group">
                    {!! Form::label('phone', 'phone', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('phone', $lead->user->phone_res, ['class' => 'form-control', 'id' => 'phone']) !!}
                    </div>
                </div>
            </div>    
            <div class="col-sm-6"> 
                <div class="form-group">
                    {!! Form::label('mobile', 'mobile', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('mobile', $lead->user->phone_mobile, ['class' => 'form-control', 'id' => 'mobile']) !!}
                    </div>
                </div>
            </div>
        </div>   
        
        <div class="row">
            <div class="col-sm-6"> 
                <div class="form-group">
                    {!! Form::label('address', 'address', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('address', $lead->user->current_address, ['class' => 'form-control', 'id' => 'address']) !!}
                    </div>
                </div>
            </div>    
            <div class="col-sm-6"> 
                <div class="form-group">
                    {!! Form::label('zip_code', 'zip code', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::text('zip_code', $lead->user->zip_code, ['class' => 'form-control', 'id' => 'zip_code']) !!}
                    </div>
                </div>
            </div>
        </div>    

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Countries', 'Country', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($CountriesArr[''] = 'Select Country')
                    @foreach($countries as $key => $country)
                        @php($CountriesArr[$country->id] = $country->name)
                    @endforeach
                    {!! Form::select('country_id', $CountriesArr, $lead->user->country_id, ['class' => 'form-control', 'id' => 'country_id']) !!}
                    </div>
                </div>
            </div>    
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('ClinetTypes', 'Clinet Type', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($ClinetTypesArr = [])
                    @foreach(\App\Helpers::ClinetTypes() as $key => $ClinetTypes)
                        @php($ClinetTypesArr[$key] = $ClinetTypes)
                    @endforeach
                    {!! Form::select('client_type', $ClinetTypesArr, $client->client_type, ['class' => 'form-control', 'id' => 'ClinetTypes']) !!}
                    </div>
                </div>
            </div>
        </div>    

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('ClinetRating', 'Clinet Rating', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    @php($ClinetRatingArr = [])
                    @foreach(\App\Helpers::ClinetRating() as $key => $ClinetRating)
                        @php($ClinetRatingArr[$key] = $ClinetRating)
                    @endforeach
                    {!! Form::select('client_rating', $ClinetRatingArr, $client->client_type, ['class' => 'form-control', 'id' => 'ClinetRating']) !!}
                    </div>
                </div>
            </div>
                {{-- <div class="form-group">
                    {!! Form::label('interest', 'Interest', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-9">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="future" name="future_interest" {{ $lead->user->future_interest ? "checked" : "" }}>
                            Future Oppurtunity
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="local" name="local_interest" {{ $lead->user->local_interest ? "checked" : "" }}>
                            Local Oppurtunity
                        </label>
                    </div>
                </div>
                </div> --}}
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('comments', 'Comments', ['class' => 'col-sm-5 control-label']) !!}
                    <div class="col-sm-7">
                    {!! Form::textarea('comments', null, ['class' => 'form-control', 'id' => 'comments','rows' => 5]) !!}
                    </div>
                </div>
            </div>
        </div>    

                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('leads.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            </div>

                

               
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function($) {

            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy',
               // maxDate: date.getMonth()
            });

            $('#propertyType').change(function(){
                $.ajax({
                    url: '{{ route('leads.getPropertySubType') }}',
                    type: 'POST',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {property_type_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#propertySubtype').html(data.html);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });   

            $('#source_id').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{ route('leads.campaign.findcampaign') }}",
                    type: 'POST',
                    data: {id: $(this).val()},
                })
                .done(function(data) {
                    $('#campaign').html(data.campaign);
                })
                .fail(function() {
                    console.log("error");
                });    
            });
        });
    </script>
@endsection