@extends('admin.layouts.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Close Leads ']) 

<!-- top tiles -->
          
          <!-- /top tiles -->
          <div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">
                     <div id="sum_box" class="row mbl" style="margin-top:20px;">
                 <a  href="{{ route('lead.leadStatus',6) }}">
             <div class="col-sm-3 col-md-3">
                <div class="panel profit db mbm">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-thumbs-up"></i></p>
                        <h4 class="value "><span data-counter="" data-start="10" data-end="50" data-step="1"
                        data-duration="0"></span><span>Won</span></h4>
                    </div>
                </div>
            </div></a>
            
            
            <a  href="{{ route('lead.leadStatus',8) }}">
             <div class="col-sm-3 col-md-3">
                <div class="panel income db mbm">
                    <div class="panel-body"><p class="icon">
                        <i class="icon fa fa-thumbs-down"></i></p>
                        <h4 class="value "><span data-counter="" data-start="10" data-end="50" data-step="1"
                        data-duration="0"></span><span>Lost</span></h4>
                    </div>
                </div>
            </div></a>
            </div>
              
    
        
       
        
      
</div>
              </div>
          </div></div>
          <!-- /dashboard nav -->


          
          
          
@endsection


