@extends('admin.layouts.template')
@section('title','Import Leads')

@section('content')


<style>
    .btn-default {
       background-color: goldenrod;
       color: white;
    }
    .btn-default:hover {
    
    background-color: goldenrod;
    color: white;
    }
</style>
       
 @include('admin/common/breadcrumb',['page'=>'Leads Dashboard'])      
        <div class="x_panel">
            <div class="x_title">
        
            <h2>Import Leads</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
          <div class="row">
          <div class="col-sm-4">
         {!! Form::open(['method' => 'POST', 'route' => 'lead.importData', 'class' => 'form-horizontal', 'files' => true]) !!}
            
            <div class="form-group col-sm-10" style="margin-left:-12px;">
            {!! Form::label('ImportData', 'Import Data (xls,csv,xlxs)')!!}
            {!! Form::file('ImportData', ['class' => 'form-control', 'id' => 'ImportData', 'required' => 'required' ]) !!}
            </div>
            <div class="col-sm-2">
            <button type="submit" id="payment" class="btn btn-default" style="margin-top: 25px;">Import</button>
            </div>
            {!! Form::close() !!}
            </div>
            </div>
        <div class="clearfix"></div>
       
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        
    </script>
@endsection

