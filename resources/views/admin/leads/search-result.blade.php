@extends('admin.layouts.template')
@section('title','Lead Search Results')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Lead Search Results</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Lead Search Results</li>
        </ol>
    </div>
    <div class="content">
    <div class="x_panel">
 <div class="x_title">
            <h2>Search Lead</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create') }}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            
            <div class="clearfix"></div>
          </div> 
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Client</th>
                    <th>Mobile</th>
                    <th>Project</th>
                    <th>Budget</th>
                    <th>Assigned Date</th>
                    <th>Assigned To</th>
                    <th>History</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
              @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $lead->status_name }}</td>
                  <td>{{ $lead->name }}</td>
                  <td>{{ $lead->phone_mobile }}</td>
                   <td>{{ $lead->project_name }}</td>
                   <td>{{ $lead->budget }}</td>
                  <td>{{ ($lead->assigned_date) ? date('d-m-Y', strtotime($lead->assigned_date)) : "" }}</td>
                  <td>{{ ($lead->assigned_to) ? \App\User::getUserInfo($lead->assigned_to)->name : ""}}</td>
                  <td><a class="btn btn-xs btn-info viewLeadHistory" data-toggle="modal" href="#viewLeadHistory" data-leadid='{{$lead->lead_id}}'>History</a></td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                 <th>Meeting Date</th>
                  <th>Status</th>
                   
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                        <th>Next Meeting Date</th>
                        <th>Next Meeting Detail</th>
                        <th>Next Meeting By</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });


            $(document).on('click', '.viewLeadHistory', function(){
         
              $('.leadId').html("");
              $('.leadStatus').html("");
              $('.leadClient').html("");
              $('.leadClientPhone').html("");
              $('.leadProject').html("");
              $('.leadBudget').html("");
              $('.leadPropertyType').html("");
              $('.leadPropertySubType').html("");
              $('.leadSources').html("");
              $('.leadHistoryTable').html("");
              
              var leadId = $(this).data('leadid');
              $.ajax({
                url: '{{ url('admin/lead/leads') }}/'+leadId,
                type: 'GET'
              })
              .done(function(data) {
                  $('.leadId').html(data.lead.id);
                  $('.lead_id').val(data.lead.id);
                  $('.leadStatus').html(data.lead.status.name);
                  $('.leadClient').html(data.lead.user.name);
                  $('.leadClientPhone').html(data.lead.user.phone_mobile);
                  // $('.leadProject').html(data.lead.project.name);
                  $('.leadBudget').html(data.lead.budget);
                  // $('.leadPropertyType').html(data.lead.property_type.name);
                  // $('.leadPropertySubType').html(data.lead.property_subtypes.name);
                  $.each(data.lead.sources, function(index, val) {
                     $('.leadSources').append(val.name + ", ");
                  });

                  if(data.history != "")
                  {
                    $('#lead_created_date').html(data.lead.created_at);
                    $('#lead_assigned_date').html(data.lead.assigned_date);
                    $('#lead_assigned_by').html(data.lead.assigned_by.name);
                    $('#lead_assigned_to').html(data.lead.assigned_to.name);
                    // console.log(data.next_meeting_by);
                    $.each(data.history, function(index2, val2) {
                      // if(data.next_meeting_by[index2] != "")
                        var next_meeting_by = data.next_meeting_by[index2].name;
                      // else
                        // $next_meeting_by = "";
                      $('.leadHistoryTable').append('<tr><td>'+val2.meeting_date+'</td><td>'+val2.status.name+'</td><td>'+val2.meeting_detail+'</td><td>'+val2.user.name+'</td><td>'+val2.next_meeting_date+'</td><td>'+val2.next_meeting_agenda+'</td><td>'+next_meeting_By+'</td></tr>');
                    });
                  }
              })
              .fail(function() {
                console.log("error");
              });
              
            });


        });



</script>
@endsection
