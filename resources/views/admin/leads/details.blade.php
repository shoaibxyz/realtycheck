@extends('admin.layouts.template')
@section('title','Employees Wise Leads')

@section('style')
  <link type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" rel="stylesheet" />
  <link type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Employees Wise Leads</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Employees Wise Leads</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Employees Wise Leads</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leads.create')}}" class="btn btn-primary" style=""><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>No. of Leads</th>
                            
                            <th class="text-center">Actions</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($employees as $employee)
                        @if($employee->user)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $employee->user->name }}</td>
                                <td>{{ $employee->user->email }}</td>
                                <td>{{ \App\Models\Leads\lead::countLeads($employee->person_id)}}</td>
                                <td style=""><a href="persondetails/{{$employee->person_id}}" class="btn btn-success" style="">View Lead Details</a></td>
                            </tr>
                            @endif
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

@endsection


@section('javascript')

@endsection