@extends('admin.layouts.template')
@section('title','Due Leads')

@section('style')
  <link type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" rel="stylesheet" />
  <link type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" />
@endsection
@section('content')
@include('admin/common/breadcrumb',['page'=>'Due Leads'])
   
    <div class="content">
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix">
        </div>
    </div>
    <div class="row">
            <div class="col-sm-4 align-right" style="text-align: right; float: right; position: relative; top: 20px; z-index: 1000;">
                <form method="POST" id="search-form" class="form-inline align-right" role="form">
                      <input type="text" id="query" class="form-control">

                </form>
            </div>
        </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover jambo_table bulk_action" id="leadsTable">
            <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Status</th>
                    <th>Client</th>
                    <th>Mobile</th>
                    <th>Query</th>
                    <th>Interest</th>
                    <th>campaign</th>
                    <th>Assigned Date</th>
                    <th>Assigned To</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody class="table table-bordered table-striped">
             
            </tbody>
        </table>
        @if(Auth::user()->roles[0]->name == "principal-office" || Auth::user()->roles[0]->name == "agency")
          <form id="frm-example"> 
            <input data-toggle="modal" href='#assignLeadModel' type="submit" value="Assign Leads" class="btn btn-primary btn-xs"> 
          </form>
        @endif
    </div>
</div>

 {{-- @php($i = 1)
              @foreach($leads as $lead)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ ($lead->lead_status_id != "")  ? $lead->status->name : "" }}</td>
                  <td>{{ $lead->user->name }}</td>
                  <td>{{ $lead->user->phone_mobile }}</td>
                  <td>{{ ($lead->project_id) ? $lead->project->name : "" }}</td>
                  <td>RS {{ number_format($lead->budget) }}</td>
                  <td>{{ ($lead->assignedTo != "") ? $lead->assignedTo->name : ""}}</td>
                  <td>
                    {!! Form::select('assign_to', $saleOfficersArr, 0, ['class' => 'form-control assignTo', 'data-lead_id' => $lead->id, 'style' => 'width: 200px']) !!}
                  </td>
                  <td class="text-center">
                    <a class="btn btn-xs btn-primary viewLead" data-toggle="modal" href='#viewLeadModel' data-leadid='{{ $lead->id }}' title="View"><i class="fa fa-eye"></i></a>
                    @if($canEdit)
                      <a href="{{ route('leads.edit', $lead->id) }}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                    @endif
                    @if($canDelete)
                        
                    @endif
                    @if($canAssign)
                      <a class="btn btn-xs btn-info assignLead" data-toggle="modal" href='#assignLead' data-leadid='{{ $lead->id }}'>Assign</a>
                    @endif
                  </td>
                </tr>
              @endforeach --}}


{{-- <a class="btn btn-primary" >Trigger modal</a> --}}
<div class="modal fade" id="viewLeadModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateLeadModel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Followup</h4>
      </div>
      {!! Form::open(['route' => 'leads.changeStatus', 'id' => 'changeLeadsStatus']) !!}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>
        <hr>
        <div class="row">

           <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('meeting_date', 'Meeting date') !!}
                  {!! Form::text('meeting_date', date('d/m/Y'), ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_date', 'readonly' => 'readonly']) !!} 
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('meeting_channel', 'Meeting Channel') !!}
                  <?php 
                          $meeting_channel[''] = 'Choose One';
                          $meeting_channel['sms'] = 'SMS';
                          $meeting_channel['email'] = 'Email';
                          $meeting_channel['whatsapp'] = 'Whatsapp';
                          $meeting_channel['call'] = 'Call';
                  ?>

                  {!! Form::select('meeting_channel', $meeting_channel, 0,  ['class' => 'form-control', 'required'=>'required', 'id' => 'meeting_channel']) !!} 
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('current_meeting_by', 'Meeting By') !!}
                  {!! Form::text('current_meeting_by', Auth::user()->name , ['class' => 'form-control', 'required'=>'required', 'id' => 'current_meeting_by', 'readonly' => 'readonly']) !!} 
                  {!! Form::hidden('meeting_done_by', Auth::user()->id) !!}
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('meeting_detail', 'Meeting Detail') !!}
                  {!! Form::text('meeting_detail', null, ['class' => 'form-control', 'waive_days'=>'required', 'id' => 'meeting_detail']) !!} 
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('in_out', 'Call In / Call Out') !!}<br>
                  {!! Form::radio('in_out', 0,  true ) !!} Out
                  {!! Form::radio('in_out', 1,  false ) !!} In
              </div>
          </div>

          <div class="col-sm-6">   
            <label>Status</label>  
              <select class="form-control" name="lead_status" id="lead_status" style="width: 100%">
                <option value="">Select One</option>
                @foreach($leadStatus as $status)
                  <option value="{{ $status->id}}"> {{ $status->name }}</option>
                @endforeach  
              </select>
          </div>

          <div class="col-sm-12">
              <h4>Next Call / Meeting</h4>
          </div>    

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('meeting_status', 'Meeting Status') !!}<br>
                  {!! Form::radio('meeting_status', 1, false) !!} Done
                  {!! Form::radio('meeting_status', 0, true) !!} Pending
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('next_meeting_date', 'Next Meeting date') !!}
                  {!! Form::text('next_meeting_date', null, ['class' => 'form-control datepicker', 'id' => 'next_meeting_date']) !!} 
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('next_meeting_channel', 'Next Meeting Channel') !!}
                  <?php 
                          $next_meeting_channel[''] = 'Choose One';
                          $next_meeting_channel['sms'] = 'SMS';
                          $next_meeting_channel['email'] = 'Email';
                          $next_meeting_channel['whatsapp'] = 'Whatsapp';
                          $next_meeting_channel['call'] = 'Call';
                  ?>

                  {!! Form::select('next_meeting_channel', $next_meeting_channel, 0,  ['class' => 'form-control', 'id' => 'next_meeting_channel']) !!} 
              </div>
          </div>

          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('Next Meeting By', 'Next Meeting By') !!}
                  {!! $assign_to_html !!}
                  {{-- {!! Form::select('next_meeting_by', $ccdArr, 0, ['class' => 'form-control', 'id' => 'next_meeting_by']) !!}  --}}
              </div>
          </div>
          <div class="col-sm-6">   
              <div class="form-group">
                  {!! Form::label('next_meeting_agenda', 'Next Meeting Agenda') !!}
                  {!! Form::text('next_meeting_agenda', null, ['class' => 'form-control', 'id' => 'next_meeting_agenda']) !!} 
              </div>
          </div>
        </div>

        <br>


          <div class="form-group">
            <label>Remarks</label>  
              <textarea class="form-control" name='remarks' id="remarks"></textarea>
              <input type="hidden" class="lead_id">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>



<div class="modal fade" id="assignLeadModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Assign Selected Leads</h4>
      </div>
      {!! Form::open(['id' => 'assignLeadModelForm']) !!}
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 selectedLeads"></div>
        </div>
        
        <hr>
        <div class="row">
          <div class="form-group">
            <label class="col-sm-4">Assign Leads To</label>  
            <div class="col-sm-8">
              {!! $assign_to_html !!}
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success saveLeadStatus">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>









<div class="modal fade" id="assignedLeads">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">User Leads</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <h4>User: <span style="font-weight: bold;" class="username"></span> <span id="countLeads"></span></h4>
          </div>  
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Lead Id</th>
                <th>Customer</th>
                <th>Phone</th>
                <th>Assigned Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="userLeadsTable">
            </tbody>
          </table>    
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="viewLeadHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lead Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Lead Id </span></div>  
          <div class="col-sm-4 leadId"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Status </span></div>  
          <div class="col-sm-4 leadStatus"></div>  
        </div>
        <div class="row">
          <div class="col-sm-2"><span style="font-weight: bold">Name </span></div>  
          <div class="col-sm-4 leadClient"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Phone </span></div>  
          <div class="col-sm-4 leadClientPhone"></div>
         </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Project </span></div>  
          <div class="col-sm-4 leadProject"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Budget </span></div>  
          <div class="col-sm-4 leadBudget"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Type </span></div>  
          <div class="col-sm-4 leadPropertyType"></div>
          <div class="col-sm-2"><span style="font-weight: bold">Sub Type </span></div>  
          <div class="col-sm-4 leadPropertySubType"></div>  
        </div>
        <div class="row"> 
          <div class="col-sm-2"><span style="font-weight: bold">Sources </span></div>  
          <div class="col-sm-10 leadSources"></div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12"><h4>History</h4></div>
          <div class="col-sm-12">
            <p>Lead created Date: <span id="lead_created_date"></span></p>
            <p>Lead Assigned Date: <span id="lead_assigned_date"></span></p>
            <p>Lead Assigned By: <span id="lead_assigned_by"></span></p>
            <p>Lead Assigned To: <span id="lead_assigned_to"></span></p>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Meeting Date</th>
                  <th>Status</th>
                   
                        <th>Meeting Detail</th>
                        <th>Meeting By</th>
                        <th>Next Meeting Date</th>
                        <th>Next Meeting Detail</th>
                        <th>Next Meeting Detail</th>
                </tr>
              </thead>
              <tbody class="leadHistoryTable">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@section('javascript')
<script src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy',
        });


        var meeting_status = null;
        $("input[name='meeting_status']").click(function() {
            meeting_status = this.value;

            if(meeting_status == 1)
            {
                $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').attr('disabled','disabled')
            }else{
                $('#next_meeting_date, #next_meeting_agenda, #next_meeting_by, #next_meeting_channel').removeAttr('disabled')
            }
        });

         var columns = [
            { data: 'id', name: 'id' },
            { data: 'checkbox', name: 'checkbox' },
            { data: 'status', name: 'status', className : 'status' },
            { data: 'name', name: 'name'},
            { data: 'phone_mobile', name: 'phone_mobile'},
            { data: 'query', name: 'query'},
            { data: 'interest', name: 'interest'},
            { data: 'campaign_name', name: 'campaign_name'},
            { data: 'assigned_date', name: 'assigned_date'},
            { data: 'assigned_to', name: 'assigned_to'},
        ];

        columns.push({ data: 'actions', name: 'actions'});

        var table = $('#leadsTable').DataTable({
             'columnDefs': [
               {
                  'targets': 0,
                  'checkboxes': true
               }
            ],
            'order': [[1, 'asc']],
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "bStateSave": true,
            "bFilter": false,
            "info": true,
            "columns": columns,
            ajax: {
                url: '{!! route('leads.getLeads') !!}',
                data: function (d) {
                    d.query = $('#query').val();
                }
            },
            fnCreatedRow: function( nRow, aData, iDataIndex ) {
              $(nRow).attr('id', "ROW_" + aData.lead_id);
            }
        });

        $('#changeLeadsStatus').submit(function(e){
          e.preventDefault();
          var lead_status = $('#lead_status').val();
          var remarks = $('#remarks').val();
          var lead_id = $('.lead_id').val();

          $.ajax({
            url: '{{ route('leads.changeStatus') }}',
            type: 'POST',
            data: $(this).serialize() + "&lead_id="+lead_id+"&remarks="+remarks,
            beforeSend: function() {
              $('.loader').removeClass('hide');
            }, 
            complete: function(data) {
                $('.loader').addClass('hide');
            },
          })
          .done(function(data) {
            $('#ROW_'+ lead_id + " .status").html(data.leadStatus.name);
            swal("Success!", "Lead has been updated",'success');
            $('#updateLeadModel').modal('toggle');

          })
          .fail(function() {
            console.log("error");
          })
        });
               

        $('#frm-example').on('submit', function(e){
          e.preventDefault();
            var form = this;
            $('.selectedLeads').html('');

            var rows_selected = table.column(0).checkboxes.selected();

            $.each(rows_selected, function(index, rowId){
              $('.selectedLeads').append(rowId + ',');
               // Create a hidden element
               // $(form).append(
               //     $('<input>')
               //        .attr('type', 'hidden')
               //        .attr('name', 'id[]')
               //        .val(rowId)
               // );
            });
         });


         $('#query').on('keyup', function(e) {
          // if(e.keyCode == 13)
            e.preventDefault();
            table.draw();

        });

        $('#query').on('keypress', function(e) {
          if(e.keyCode == 13){
            e.preventDefault();
            table.draw();

          }
        });



        $(document).on('submit','#assignLeadModelForm', function(e){
          e.preventDefault();

          var assignTo = $('.assignTo').val();

          if(assignTo != "")
          {
            var lead = $('.selectedLeads').html();
            if(lead == "")
            {
              return swal("Oops!","Please select a lead to assign",'error');
            }
            $.ajax({
              url: '{{ route('leadUpdate') }}',
              type: 'POST',
              data: {lead_id: lead, assignTo: assignTo},
            })
            .done(function(data) {
              table.draw();
              swal('success', 'Lead assigned.', 'success');
              $('#assignLeadModel').modal('hide');

            })
            .fail(function() {
              console.log("error");
            });
          }
        });

        $(document).on('click', '.viewLead, .updateLeadModel, .viewLeadHistory', function(){
         //alert($(this).data('leadid'));
          $('.leadId').html("");
          $('.leadStatus').html("");
          $('.leadClient').html("");
          $('.leadClientPhone').html("");
          $('.leadProject').html("");
          $('.leadBudget').html("");
          $('.leadPropertyType').html("");
          $('.leadPropertySubType').html("");
          $('.leadSources').html("");
          $('.leadHistoryTable').html("");
          
          var leadId = $(this).data('leadid');
          $.ajax({

            url: '{{ url('admin/lead/leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {

            console.log(data.lead);

              $('.leadId').html(data.lead.id);
              $('.lead_id').val(data.lead.id);
              $('.leadStatus').html(data.lead.status.name);
              $('.leadClient').html(data.lead.user.name);
              $('.leadClientPhone').html(data.lead.user.phone_mobile);
              $('.leadBudget').html(data.lead.budget);

              if(data.lead.sources)
                $('.leadSources').append(data.lead.sources.name);

              if(data.lead.property_type)
                $('.leadPropertyType').html(data.lead.property_type.name);
              
              if(data.lead.property_subtypes)
                $('.leadPropertySubType').html(data.lead.property_subtypes.name);

               $('#lead_created_date').html(data.lead.created_at);
              $('#lead_assigned_date').html(data.lead.assigned_date);
              
              if(data.lead.assigned_by)
                $('#lead_assigned_by').html(data.lead.assigned_by.name);
              
              if(data.lead.assigned_to)
                $('#lead_assigned_to').html(data.lead.assigned_to.name);

              if(data.history != "")
              {
                
                $.each(data.history, function(index2, val2) {
                   $('.leadHistoryTable').append('<tr><td>'+val2.meeting_date+'</td><td>'+val2.status.name+'</td><td>'+val2.meeting_detail+'</td><td>'+val2.user.name+'</td><td>'+val2.next_meeting_date+'</td><td>'+val2.next_meeting_agenda+'</td><td>'+val2.next_meeting_by+'</td></tr>');
                });
              }
          })
          .fail(function() {
            console.log("error");
          });
          
        });

        $(document).on('click', '.assignedLeads', function(){
          $('.username').html("");
          var leadId = $(this).data('userid');
          $.ajax({
            url: '{{ url('admin/lead/leads/show-user-leads') }}/'+leadId,
            type: 'GET'
          })
          .done(function(data) {
              $('#userLeadsTable').html("");
              $('.username').html(data.user.name);
              // $('.leadId').html(data.lead.id);
              // $('.leadStatus').html(data.lead.status.name);
              // $('.leadClient').html(data.lead.user.name);
              // $('.leadClientPhone').html(data.lead.user.phone_mobile);
              // $('.leadProject').html(data.lead.project.name);
              // $('.leadBudget').html(data.lead.budget);
              // $('.leadPropertyType').html(data.lead.property_type.name);
              var i = 1;
              $.each(data.leads, function(index, val) {
                 $('#userLeadsTable').append("<tr><td>"+val.id+"</td><td>"+val.user.name+"</td><td>"+val.user.phone_mobile+"</td><td>"+val.assigned_date+"</td><td>"+val.status.name+"</td></tr>");
                 i++;
              });
              $('#countLeads').html("("+parseInt(i-1)+")");
              table.draw();
              // $('.assign_to').html(data.saleOfficer);
          })
          .fail(function() {
            console.log("error");
          });
          
        });

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this source ",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your source has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your source is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
</script>
@endsection
