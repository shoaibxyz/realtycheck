@extends('admin.layouts.template')

@section('page-title', 'Unauthorized')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="note note-danger">
                <h4 class="box-heading">Error</h4>
                <p>{{ $exception }}</p>
            </div>
        </div>
    </div>
@endsection
