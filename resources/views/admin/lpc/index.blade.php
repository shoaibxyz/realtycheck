<?php use Illuminate\Support\Facades\Request; ?>
@extends('admin.layouts.template')
@section('title','LPC')

@section('bookings-active','active')
@section('lpc-active','active')
@section('calculate_lpc-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'LPC List'])        
<br>
         @include('errors')
         
               <div class="x_panel">
        <div class="x_title">
           
            <h2>LPC List</h2>
            <div class="clearfix"></div>
          </div>   
          <div class="col-sm-6">
            {!! Form::open(['route'=>'lpc.calculate', 'files' => true])!!}

            <!--<div class="form-group">
                {!! Form::label('charge_date', 'Charge to date') !!}
                {!! Form::text('charge_date', Carbon\Carbon::today()->format('d-m-Y'), ['class' => 'form-control datepicker']) !!}
            </div>-->

            {!! Form::submit('Calculate' , ['class' => 'btn btn-primary'])!!}
            {!! Form::close() !!}
          </div>
        <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Registration No</th>
                            <th>Installment Date</th>                            
                            <th>Installment no</th>
                            <th>OS Days</th>
                            <th>Rate</th>
                            <th>Inst. Charges</th>
                            <!--<th>Other Charges</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        @php ($i = 1)
                        @if( isset($lpcInstArr) )
                        @foreach($lpcInstArr as $key => $lpc)
                            @foreach($lpc as $lp)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $key }}</td>
                                    <td>{{ date('d/m/Y',strtotime($lp['booking_date'])) }}</td>                                    
                                    <td>{{ $lp['installment_no'] }}</td>
                                    <td>{{ ($lp['os_days'] > 0) ?  $lp['os_days'] : "0" }}</td>
                                    <td>{{ $lp['rate'] }}</td>
                                    <td>{{ ($lp['chargesSum'] > 0) ? $lp['chargesSum']: 0 }}</td>
                                </tr>
                                @php ($i++)
                            @endforeach
                        @endforeach
                        @endif
                    </tbody>

                    </table>

                </div>
                {!! Form::open(['route' => 'lpc.store']) !!}
                    {!! Form::hidden('calculation_date', Request::input('charge_date')) !!}
                    {!! Form::submit('Save', ['class' => 'btn btn-success' ]) !!}
                {!! Form::close() !!}
        </div>
@endsection




@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });
    </script>
@endsection