@extends('admin.layouts.template')
@section('title','LPC Waive')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Add New LPC Waive</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
        <div class="clearfix"></div>
       
       <form method="POST" action="{{ route('lpc.waive.post') }}" class="form-horizontal">
       {!! Form::open(['route' => 'lpc.waive']) !!}

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('registration_no', 'Registration No')!!}
            {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('person', 'Person')!!}
            {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
            </div>
       </div>

        <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('inst_charges', 'Net Inst. Charges')!!}
            {!! Form::text('inst_charges', null, ['class' => 'form-control', 'id' => 'inst_charges']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('other_charges', 'Net other charges')!!}
            {!! Form::text('other_charges', null, ['class' => 'form-control', 'id' => 'other_charges']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('charges_received', 'Charges Received')!!}
            {!! Form::text('charges_received', null, ['class' => 'form-control', 'id' => 'charges_received']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('charges_to_received', 'Charges To Received')!!}
            {!! Form::text('charges_to_received', null, ['class' => 'form-control', 'id' => 'charges_to_received']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('waive_date', 'Waive Date')!!}
            {!! Form::text('waive_date', null, ['class' => 'datepicker form-control', 'id' => 'waive_date']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('waive_amount', 'Waive Amount')!!}
            {!! Form::text('waive_charges', null, ['class' => 'form-control', 'id' => 'waive_amount']) !!}
            </div>
       </div>

       <div class="col-sm-12">
            <div class="form-group">
            {!! Form::label('waive_remarks', 'Remarks')!!}
            {!! Form::text('waive_remarks', null, ['class' => 'form-control', 'id' => 'waive_remarks']) !!}
            </div>
       </div>

        

        <div class="row">
            <div class="col-sm-12"> 
            <a href="#" class="btn btn-default btn-md pull-right" id="populate"> Populate</a>
             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Installment">Installment Charges</a></li>
              <li><a data-toggle="tab" href="#other_payment">Other Payment charges</a></li>
              <li><a data-toggle="tab" href="#lpc">Late Payment Charge</a></li>
            </ul>

            <div class="tab-content lpc_waive">
              <div id="Installment" class="tab-pane fade in active">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Inst. Amt.</th>
                            <th>Rebate Amt.</th>
                            <th>Amt Received</th>
                            <th>Charges</th>
                            <th>Already Waive</th>
                            <th>Waive</th>
                        </tr>
                    </thead>
                    <tbody class="installmentAppend"></tbody>
                </table>
                </div>
             </div>

             <div id="other_payment" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Payment Type</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Amt to receive</th>
                            <th>rebat_amount</th>
                            <th>Amt Received</th>
                            <th>Charges</th>
                            <th>Already Waive</th>
                            <th>Waive</th>
                        </tr>
                    </thead>
                    <tbody class="otherInstallmentAppend2"></tbody>
                </table>
                </div>
             </div>

              <div id="lpc" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Payment Type</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Rebate Amt.</th>
                            <th>Amt Received</th>
                            <th>O.S Amt</th>
                            <th>PDC Amt</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody class="LPCAppend"></tbody>
                </table>
                </div>
             </div>

            </div>

        </div>
        </div>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
        {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
   </div>
    {!! Form::close() !!}

</div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('#registration_no').blur(function(){
        var reg_no = $(this).val();             
        $.ajax({
           url: '{{ URL::to('/findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no, 'page' : 'waive'},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person').val(data.person.name);
                $('#inst_charges').val(data.sum);
                $('#other_charges').val(data.other);
                $('#charges_to_received').val(data.total);

            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });

    $('#populate').click(function(event) {
        event.preventDefault();
        var reg_no = $('#registration_no').val();
        var waive_amount = $('#waive_amount').val();
        var charges_to_received = $('#charges_to_received').val();

       

        if(reg_no == "")
        {
            swal({
                title: "Oops!",
                text: "Please Enter Registration Number",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
            });
             return;
        }
        $('.installmentAppend').html("");
        $('.otherInstallmentAppend2').html("");
        $('.LPCAppend').html("");

      
        $.ajax({
            url: '{{ URL::to('/LpcCharges') }}',
            type: 'POST',
            data: {reg_no: reg_no},
            success: function(data){
                var i = 1;
                var j = 1;
                var k = 1;
                var due_amount_total = 0;
                var due_amount_total2 = 0;
                var inst_total = 0;
                var inst_total2 = 0;
                var remaining_waive = 0;
                 var waive = 0;
                var total_waive = 0;

                var waive_sum = 0;
                var check = 0;
                var total_waived = 0;
                $.each(data.otherPayments, function(key2, val2) {
                    // remaining = data.remaining[key];
                    due_amount_total2 += val2.due_amount;
                    inst_total2 += val2.installment_amount;

                    // remaining_amount = parseInt(remaining-val2.installment_amount                    

                    var charges = (val2.lpc_charges != "") ? val2.lpc_charges : 0;

                    htmlOther = "<tr>";
                    htmlOther += "<td>"+j+"</td>";
                    htmlOther += "<td><input type='text' name='payment_desc[]' readonly value='"+val2.payment_desc+"'></td>"; 
                    htmlOther += "<td><input type='text' name='installment_no[]' readonly value='"+val2.installment_no+"'></td>"; 
                    htmlOther += "<td><input type='text' name='installment_date[]' readonly value='"+val2.installment_date+"'></td>";
                    htmlOther += "<td><input type='text' name='installment_amount[]' readonly value='"+val2.installment_amount+"'></td>"; 
                    htmlOther += "<td><input type='text' name='rebate_amount[]' readonly value='"+val2.rebate_amount+"'></td>"; 
                    htmlOther += "<td><input type='text' name='amount_received[]' value='"+val2.amount_received+"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_charges[]' readonly value='"+charges+"'></td>"; 
                    htmlOther += "<td><input type='text' name='already_waive[]' readonly value='"+val2.waive_amount+"'></td>";
                    

                    if( val2.lpc_charges > 0 && waive_amount > 0 && waive_amount != "" && val2.lpc_charges != "")
                        // remaining_waive = val2.lpc_charges > waive_amount;

                    var charges = val2.lpc_charges;
                    if (waive_amount - waive_sum >= charges && charges > 0)
                    {
                        waive = charges;
                        waive_sum += charges;
                        check++;
                    }else{
                        if( waive_amount - waive_sum < charges && check == 0){
                            // waive_sum += val2.lpc_charges;
                            waive = waive_amount;  
                            waive_amount = 0;
                        }
                        else{
                            waive =   waive_amount - waive_sum;
                            waive_sum += charges;  
                            check++;
                        }
                    }

                    total_waived += waive;


                    htmlOther += "<td><input type='text' name='waive_amount[]' value='"+waive+"'></td>";  
                    htmlOther += "<input type='hidden' name='member_installment_id[]' value='"+val2.id+"'>";
                    htmlOther += "</tr>";
                    $('.otherInstallmentAppend2').append(htmlOther);
                    j++;
                });


                var inst_ = 0;
                var waive_inst_sum = 0;
                var new_waive_amount = waive_amount - total_waived;
                // console.log(waive_amount);
                // console.log(waive_sum);
                // console.log(new_waive_amount);
                var waive2 = 0;
                var lpc_charges_total = 0;
                var rebate_amount_total = 0;
                var amount_received_total = 0;
                var waive2_total = 0;

                $.each(data.installments, function(key, val) {

                    // remaining = data.remaining[key];
                    due_amount_total += val.due_amount;
                    inst_total += val.installment_amount;
                    lpc_charges_total += ( val.lpc_charges > 0 ) ? val.lpc_charges : 0; 
                    console.log(lpc_charges_total);
                    rebate_amount_total += val.rebate_amount;
                    amount_received_total += val.amount_received;

                    // remaining_amount = parseInt(remaining-val.installment_amount);

                    html = "<tr>";
                    html += "<td><input type='text' name='installment_no[]' readonly value='"+val.installment_no+"'></td>"; 
                    html += "<td><input type='text' name='installment_date[]' readonly value='"+val.installment_date+"'></td>"; 
                    html += "<td><input type='text' name='installment_amount[]' readonly value='"+val.installment_amount+"'></td>"; 
                    html += "<td><input type='text' name='rebate_amount[]' readonly value='"+val.rebate_amount+"'></td>"; 
                    html += "<td><input type='text' name='amount_received[]' readonly value='"+val.amount_received+"'></td>"; 
                    html += "<td><input type='text' name='lpc_charges[]' readonly value='"+val.lpc_charges+"'></td>"; 
                    html += "<td><input type='text' name='already_waive[]' readonly value=''></td>"; 
                    
                    if( val.lpc_charges > 0 && new_waive_amount > 0 && new_waive_amount != "" && val.lpc_charges != "")
                        // remaining_waive = val2.lpc_charges > waive_amount;

                    var charges2 = val.lpc_charges;

                    if (new_waive_amount - waive_inst_sum >= charges2 && charges2 > 0)
                    {
                        waive2 = charges2;
                        waive_inst_sum += charges2;
                        inst_++;
                    }else{
                        if( new_waive_amount - waive_inst_sum < charges2 && inst_ == 0){
                            // waive_sum += val2.lpc_charges2;
                            waive2 = new_waive_amount;  
                            new_waive_amount = 0;
                        }
                        else{
                            waive2 =   new_waive_amount - waive_inst_sum;
                            waive_inst_sum += charges2;  
                            inst_++;
                        }
                    }

                    var waive2_val = ( waive2 > 0 ) ? waive2 : 0; 
                    
                    waive2_total += waive2_val;

                    html += "<td><input type='text' name='waive_amount[]' value='"+waive2_val+"'></td>"; 
                    html += "<input type='hidden' name='member_installment_id[]' value='"+val.id+"'>";
                    html += "<tr>";
                    $('.installmentAppend').append(html);
                    i++;
                });

                html2 = "<tr>";
                html2 += "<td>&nbsp;</td>";
                html2 += "<td>Total Amount</td>";
                html2 += "<td><input type='text' readonly value='"+inst_total.toFixed(2)+"' style='background: orange; color: white;'>";
                html2 += "<td><input type='text' value='"+rebate_amount_total.toFixed(2)+"' style='background: orange; color: white;' readonly>";
                html2 += "<td><input type='text' value='"+amount_received_total.toFixed(2)+"' style='background: orange; color: white;' readonly>";
                html2 += "<td><input type='text' value='"+lpc_charges_total.toFixed(2)+"' style='background: orange; color: white;' readonly>";
                html2 += "<td><input type='text' value='0' style='background: orange; color: white;' readonly>";
                html2 += "<td><input type='text' value='"+waive2_total.toFixed(2)+"' style='background: orange; color: white;' readonly>";
                html2 += "</tr>";
                $('.installmentAppend').append(html2);


                $.each(data.lpc, function(key3, val3) {
                    // remaining = data.remaining[key];
                    due_amount_total2 += val3.due_amount;
                    inst_total2 += val3.installment_amount;
                    // remaining_amount = parseInt(remaining-val3.installment_amount                    

                    htmlOther = "<tr>";
                    htmlOther += "<td>"+k+"</td>";
                    htmlOther += "<td><input type='text' name='lpc_payment_desc[]' readonly value='LPC'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_installment_no[]' readonly value='1'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_due_date[]' readonly value='"+val3.calculation_date+"'></td>";
                    htmlOther += "<td><input type='text' name='lpc_installment_amount[]' readonly value='"+parseInt(val3.lpc_other_amount + val3.lpc_amount) +"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_rebate_amount[]' readonly value='"+val3.rebate_amount+"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_amount_received[]' readonly value='"+val3.amount_received+"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_lpc_charges[]'readonly  value='"+val3.lpc_charges+"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_already_waive[]'readonly value='"+val3.waive_amount+"'></td>"; 
                    htmlOther += "<td><input type='text' name='lpc_waive_amount[]'  value='"+val3.waive_amount+"'></td>"; 
                    htmlOther += "<input type='hidden' name='lpc_member_installment_id[]' value='"+val3.id+"'>";
                    htmlOther += "</tr>";
                    $('.LPCAppend').append(htmlOther);
                    k++;
                });
            },
            error: function(xhr, status, response){
                console.log(xhr);
            }
        });       
    });


    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#img-preview').attr('src','../../public/images/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

