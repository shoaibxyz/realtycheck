@extends('admin.layouts.template')
@section('title','Ledger')
@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css">
@endsection

@section('content')
  <div class="x_panel">
        <div class="x_title">
            
            
            <div class="clearfix"></div>
          </div>    
    <div id="app">
            <div class="container">
                <div class="leadger">

                <h1>Capital City</h1>
                 {{-- memberinfo --}}
                <div class="row">
                    <div class="col-xs-12"><h3><u>Member Imformation</u></h3></div>
                    <div class="col-xs-6">
                        <h5>Registration No:   &nbsp; {{ $booking->registration_no }}</h5>
                       <h5> CNIC No: &nbsp; {{ $person->cnic }}</h5>
                        <h5>Member Name: &nbsp; {{ $person->name }}</h5>
                       <h5> S/O, D/O: &nbsp; {{ $person->gurdian_name }}</h5>
                        <h5>Current Address: &nbsp; {{ $person->current_address }}</h5>
                        <h5>Permanent Address: &nbsp; {{ $person->permanent_address }}</h5>
                       <h5> Mobile No: &nbsp; {{ $person->phone_mobile }}</h5>


                    </div>
                    <div class="col-xs-3">
                        <h5>Form No : &nbsp; {{ $member->reference_no }}</h5>

                    </div>
                    <div class="col-xs-3">
                        
                         <img src="{{ asset('public/')}}/images/{{ $person->picture }}" alt="" style="width:150px;height:150px;">
                    </div>
                    </div>

                     {{-- end --}}
                     <hr>
                      {{-- plotinfo --}}
                        
<div class="row">
                    <div class="col-xs-12"><h3><u>File/Plot Information</u></h3></div>
                    
                    <div class="col-xs-6">
                        <h5>Block Name:</h5>
                       <h5> File/Plot No: {{ $member->reference_no }}</h5>
                        <h5>Type: {{ $booking->paymentPlan->plot_nature}}</h5>
                       <h5> Dimension:</h45>
                         </div>
                    <div class="col-xs-6">
                        <h5>Payment Code: {{ $booking->paymentPlan->payment_desc }}</h5>
                       <h5> Street#: </h5>
                        <h5>Size: {{ $booking->paymentPlan->plotSize->plot_size }}</h5>
                       <h5> Booking Date: {{ $booking->created_at->format('d-m-Y') }}</h5>


                    </div>
                    </div>
                    {{-- end --}}
                    <hr>

                     {{-- paymentdetail --}}

<div class="row">
                    <div class="col-xs-12"><h3><u>Payment Detail</u></h3></div>
                    
                    <?php $received = 0; ?>
                     @foreach($installments as $installment)
                        <?php 
                          if($installment->is_paid == 1)
                            $received += $installment->amount_received ;
                        ?>
                     @endforeach

                    <div class="col-xs-4">
                        <h5>Total Amount: {{ $booking->paymentPlan->total_price }}</h5>
                       <h5> LPS: </h5>
                       
                        {{-- <h5>Total Received: {{ round(($received / $booking->paymentPlan->total_price) * 100, 2) }} %</h5> --}}
                        
                         </div>
                    <div class="col-xs-4">
                        <h5>Total Received: {{ $received - $booking->rebate_amount }}</h5>
                       <h5> LPS Received: </h5>
                       <h5> Rebate Amount:  {{ $booking->rebate_amount }}</h5>
                        
                      


                    </div>
                    <div class="col-xs-4">
                        <h5>Over Due Amount:</h5>
                       <h5> LPS Outstanding: </h5>
                        <h5>Total Outstanding Amount:</h5>
                       </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-xs-12">
                          <button id="printLedger">Print</button>
                          <table class="table table-bordered" id="ledgerTable">
                          <thead >
                              <tr style="border-bottom: 3px solid;border-top:3px solid;">
                                <th>Payment Decs</th>
                                <th>Inst #</th>
                                <th>Due Date</th>
                                <th>Due Amt.</th>
                                <th>Rebate Amt.</th>
                                <th>Recv Amt.</th>
                                <th>Os Amt.</th>
                                <th>Recp No</th>
                                <th>Recp Amt.</th>
                                <th>Date</th>
                                <th>Pay Mode</th>
                                <th>Adj Amt.</th>
                              </tr>
                            </thead>
                              <tbody>
                                @foreach($installments as $installment)

                                <?php $received_amt = ($installment->is_paid == 1) ? $installment->due_amount - $installment->os_amount : 0; ?>
                                <tr>
                                  <td>{{ $installment->payment_desc }}</td>
                                  <td>{{ $installment->installment_no}}</td>
                                  <td>{{ date('d-m-Y',strtotime($installment->installment_date)) }}</td>
                                  <td>{{ $installment->due_amount or "-" }}</td>
                                  <td>{{ $installment->rebat_amount or "-" }}</td>
                                  <td>
                                     <?php $receiving_amount = ($installment->os_amount > 0) ? $installment->due_amount - $installment->os_amount : $received_amt; ?>
                                  {{ $receiving_amount }}</td>
                                  <td>{{ $installment->os_amount or 0}}</td>
                                  <td>{{ $installment->receipt_no }}</td>
                                  <td>{{ $installment->receipt_amount or ""  }}</td>
                                  <td>{{ ($installment->payment_date != null) ? date('d-m-Y', strtotime($installment->payment_date)): ""}}</td>
                                  <td>{{ $installment->payment_mode }}</td>
                                  <td>{{ $installment->amount_received }}</td>
                                </tr>
                                @endforeach


                     <tr ><td colspan="3"><form >
                               
                              <h5>Subtotal=></h5></td>
                              
                                <?php $due_amount = 0; $os_amount = 0; $rebat_amount = 0; $amount_received = 0; $adj_amount = 0; ?>
                                @foreach($installments as $installment)
                                  <?php 
                                    $due_amount += $installment->due_amount; 
                                    // $os_amount += $installment->due_amount - $installment->amount_received; 
                                    $rebat_amount += $installment->rebat_amount;
                                    $amount_received += $installment->amount_received;
                                    $adj_amount += $installment->amount_received;
                                  ?>
                                @endforeach
                               <td><input type="text" value="{{ $due_amount }}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $rebat_amount }}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $amount_received}}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $os_amount}}" style="width:80px;"></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td><input type="text" value="{{ $adj_amount}}" style="width:80px;"></td>
                              

                              </form></tr>
                              <tr><td colspan="3">
                              <form >
                               
                                <h5>Grandtotal=></h5></td>
                                
                               <td><input type="text" value="{{ $due_amount }}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $rebat_amount }}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $amount_received}}" style="width:80px;"></td>
                               <td><input type="text" value="{{ $os_amount}}" style="width:80px;"></td>
                             <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td><input type="text" value="{{ $adj_amount}}" style="width:80px;"></td>

                              </form></tr>
                              </tbody>
                          </table>
                          </div>
                          </div>
                           {{-- end --}}



                </div>
                  
               
            </div>
            </div>
       
@endsection

@section('javascript')
  <script type="text/javascript">
  function printData()
  {
     var divToPrint=document.getElementById("ledgerTable");
     newWin= window.open("");
     newWin.document.write(divToPrint.outerHTML);
     newWin.print();
     newWin.close();
  }

  $('#printLedger').on('click',function(){
  printData();
  })
  </script>
@endsection