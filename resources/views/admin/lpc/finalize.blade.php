<?php use Illuminate\Support\Facades\Request; ?>
@extends('admin.layouts.template')
@section('title','LPC')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
         <h2>LPC Finalize<small class="pull-right"><a href="" class="btn btn-primary">Add New</a></small></h2>

         @include('errors')
         <?php if( Session::has('message')) : 

            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>
            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>


               <div class="x_panel">
        <div class="x_title">
           
            
            <div class="clearfix"></div>
          </div>   
        <div class="table-responsive">
          {!! Form::open(['route' => 'lpc.finalize.store']) !!}
                <table class="table table-striped table-bordered jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th>Waive No</th>
                            <th>Registration No</th>
                            <th>Member Name</th>
                            <th colspan="2">Inst. Charges & Waive</th>
                            <th colspan="2">Other Charges & Waive</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php ($i = 1)
                        @if( isset($inst) )
                        @foreach($inst as $key => $lpc)
                            <tr>
                                <td>{{ $lpc['id'] }}</td>
                                <td>{{ $key }}</td>
                                <td>{{ $lpc['member']->name or "" }}</td>
                                <td colspan="2">
                                {!! Form::text('lpc_amt', $lpc['lpc_amt'], ['disabled' => 'disabled','style' => 'width:100px']) !!}
                                {!! Form::text('lpc_amt', 0, ['disabled' => 'disabled','style' => 'width:100px']) !!}
                                </td>
                                <td colspan="2">
                                    {!! Form::text('lpc_amt', $lpc['lpc_other_amt'], ['disabled' => 'disabled','style' => 'width:100px']) !!}
                                {!! Form::text('lpc_amt', 0 , ['disabled' => 'disabled','style' => 'width:100px']) !!}
                                </td>
                            </tr>
                            {!! Form::hidden('lpc_id[]', $lpc['id']) !!}
                            @php ($i++)
                        @endforeach
                        @endif
                    </tbody>

                    </table>
                      {!! Form::submit('Save', ['class' => 'btn btn-xs btn-default' ]) !!}
                {!! Form::close() !!}
                </div>
        </div>
@endsection




@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });
    </script>
@endsection