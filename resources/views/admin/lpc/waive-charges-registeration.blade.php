@extends('admin.layouts.template')
@section('title','LPC full Waive')

@section('bookings-active','active')
@section('lpc-active','active')
@section('waiveRegisteration-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>LPC FULL Waive</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
       
       <form method="POST" action="{{ route('lpc.postWaiveRegisteration') }}" class="form-horizontal">
       {!! Form::open(['route' => 'lpc.waive']) !!}

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('registration_no', 'Registration No')!!}
            {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('person', 'Person')!!}
            {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
            </div>
       </div>

        <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('inst_charges', 'Net Inst. Charges')!!}
            {!! Form::text('inst_charges', null, ['class' => 'form-control', 'id' => 'inst_charges']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('other_charges', 'Net other charges')!!}
            {!! Form::text('other_charges', null, ['class' => 'form-control', 'id' => 'other_charges']) !!}
            </div>
       </div>


       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('charges_to_received', 'Charges To Received')!!}
            {!! Form::text('charges_to_received', null, ['class' => 'form-control', 'id' => 'charges_to_received']) !!}
            </div>
       </div>        

        <div class="row">
            <div class="col-sm-12"> 
             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Waive">Waive Charges</a></li>
            </ul>

            <div class="tab-content">
            <div class="Waive" class="tab-pane fade in active">
                    {{-- <label class="radio-inline">
                        <input type="radio" name="full_waive" id="inputFull_waive" value="0">
                        No
                    </label>

                    <label class="radio-inline">
                        <input type="radio" name="full_waive" id="inputFull_waive" value="1">
                        Yes
                    </label>
 --}}

                <div class="form-group">
                    {!! Form::label('waive_date', 'Full Waive Charges Y/N',['class' => 'col-sm-4  control-label']) !!}
                    <div class="col-sm-8">
                      <label class="radio-inline">
                          <input type="radio" name="full_waive" value="1">Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="full_waive" value="0">No
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('waive_date', 'Waive Date',['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                      {!! Form::text('waive_date', Carbon\Carbon::now()->format("d-m-Y"), ['class' => 'datepicker form-control']) !!}
                    </div>
                  </div>



                <div class="form-group">
                    {!! Form::label('waive_remarks', 'Waive Remarks',['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                       {!! Form::text('waive_remarks', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
             </div>

            </div>

        </div>
        </div>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
        {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
   </div>
    {!! Form::close() !!}

</div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

     $('#registration_no').blur(function(){
        var reg_no = $(this).val();             
        $.ajax({
           url: '{{ URL::to('/findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no, 'page' : 'waive'},
            success: function(data){
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person').val(data.person.name);
                $('#inst_charges').val(data.sum);
                $('#other_charges').val(data.other);
                $('#charges_to_received').val(data.total);

            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
    });

    


    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){

                var $inputs = $('#data input');
                var $persons = $('#persons input');

                if(result.error == 1)
                {
                    swal({
                      title: "Oops! Record not found",
                      text: "This registration number does not exist.",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                    $('#img-preview').attr('src','https://docs.moodle.org/27/en/images_en/7/7c/F1.png');
                    $('#paymentForm2')[0].reset();
                    return;
                }else{
                    $('#data input, #data select').removeAttr('disabled','disabled');
                    $('#data input#cnic').attr('disabled','disabled');
                    $('#data input#person').attr('disabled','disabled');
                    $('#data input#person').val(result.person.name);
                    $('#img-preview').attr('src','../../public/images/'+result.person.picture);

                    $.each(result.member, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });

                    $.each(result.person, function(key, value) {
                      $inputs.filter(function() {
                        return key == this.name;
                      }).val(value);
                    });
                }

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

