@extends('admin.layouts.template')
@section('title','LPC Waive')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        
               <div class="x_panel">
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>

            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>
        <div class="x_title">
            <h2>Add New Receipt</h2>
            
            <div class="clearfix"></div>
          </div>   

            @include('errors')
        <div class="clearfix"></div>
       

       {!! Form::open(['route' => ['lpc.postwaive'], 'class' => 'form-horizontal']) !!}
      
       

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('registration_no', 'Registration No')!!}
            {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('person', 'Person')!!}
            {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'readonly' => 'readonly']) !!}
            </div>
       </div>

        <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('inst_charges', 'Net Inst. Charges')!!}
            {!! Form::text('inst_charges', null, ['class' => 'form-control', 'id' => 'inst_charges']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('other_charges', 'Net other charges')!!}
            {!! Form::text('other_charges', null, ['class' => 'form-control', 'id' => 'other_charges']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('charges_received', 'Charges Received')!!}
            {!! Form::text('charges_received', null, ['class' => 'form-control', 'id' => 'charges_received']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('charges_to_received', 'Charges To Received')!!}
            {!! Form::text('charges_to_received', null, ['class' => 'form-control', 'id' => 'charges_to_received']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('waive_date', 'Waive Date')!!}
            {!! Form::text('waive_date', null, ['class' => 'form-control','id' => 'waive_date']) !!}
            </div>
       </div>

       <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('waive_amount', 'Waive Amount')!!}
            {!! Form::text('waive_amount', null, ['class' => 'form-control', 'id' => 'waive_amount']) !!}
            </div>
       </div>

       <div class="col-sm-12">
            <div class="form-group">
            {!! Form::label('waive_remarks', 'Remarks')!!}
            {!! Form::text('waive_remarks', null, ['class' => 'form-control', 'id' => 'waive_remarks']) !!}
            </div>
       </div>
 
       
            <div class="col-sm-6">
            <div class="form-group">
            {!! Form::label('waive_no', 'Charges Waive No')!!}
            {!! Form::text('waive_no', null, ['class' => 'form-control', 'id' => 'waive_no']) !!}
            </div>
       </div>
       
        
        

        <div class="row">
            <div class="col-sm-12"> 
           <a href="#" class="btn btn-default btn-md pull-right" id="search"> Search</a>
             <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#Installment">Installment Charges</a></li>
              <li><a data-toggle="tab" href="#other_payment">Other Payment charges</a></li>
              <li><a data-toggle="tab" href="#lpc">Late Payment Charge</a></li>
            </ul>

            <div class="tab-content lpc_waive">
              <div id="Installment" class="tab-pane fade in active">
                <div class="table-responsive1" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Paymnet Type</th>
                            <th>Inst. No.</th>
                            
                            <th>Waive Amount</th>
                        </tr>
                    </thead>
                    <tbody class="installmentAppend">
                       


                    </tbody>
                    
                </table>
                </div>
             </div>

             <div id="other_payment" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Payment Type</th>
                            <th>Inst. No</th>
                            <th>Waive Amount</th>
                            
                        </tr>
                    </thead>
                    <tbody class="otherInstallmentAppend2"></tbody>
                </table>
                </div>
             </div>

              <div id="lpc" class="tab-pane fade in">
                <div class="table-responsive" style="overflow:scroll; max-height: 400px; min-height: 400px;">
                <table class="table table-bordered table-hover table-striped table-condenced">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Payment Type</th>
                            <th>Inst. No</th>
                            <th>Inst. Date</th>
                            <th>Waive Amount</th>
                            
                        </tr>
                    </thead>
                    <tbody class="LPCAppend"></tbody>
                </table>
                </div>
             </div>

            </div>

        </div>
        </div>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
        {!! Form::submit('Save', ['class' => 'btn btn-default pull-right' , 'name' => 'Save']) !!}
   </div>
    {!! Form::close() !!}

</div>
        </div>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     

     $('#registration_no').blur(function(){
        var reg_no = $(this).val();             
        $.ajax({
           url: '{{ URL::to('/findname') }}',
            type: 'POST',
            data: {'registration_no': reg_no, 'page' : 'waive'},
            success: function(data){
              console.log(data);
                if(data.error == 1)
                {
                    swal({
                      title: "Oops!",
                      text: "Registration No '" +reg_no + "' does not exist",
                      type: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                    });
                     return;
                }    
                $('#person').val(data.person.name);
                $('#inst_charges').val(data.sum);
                $('#other_charges').val(data.other);
                $('#charges_to_received').val(data.total);
                $('#waive_date').val(data.receipt[0].waive_date);
                $('#waive_amount').val(data.receipt[0].waive_amount);
                $('#waive_remarks').val(data.receipt[0].waive_remarks);

            },
            error: function(xhr, status, response){
                $('#member').html(xhr);
                console.log(xhr);
            }
        });
        });
        
        $('#search').click(function(){

        var waive_no = $('#waive_no').val(); 
        $('.installmentAppend').html("");
        $('.otherInstallmentAppend2').html("");
            
        $.ajax({
           url: '{{ URL::to('/cancelwaive1') }}',
            type: 'POST',
            data: {'waive_no': waive_no},
            success: function(data){
              var k = 1;
              var j=1;
              var i=1;
              //console.log(data);
              $.each(data.installaments, function(key, val) {
                                 //console.log(val);
                    htmlOther = "<tr>";
                    htmlOther += "<td>"+k+"</td>";
                    htmlOther += "<td>"+val.payment_desc+"</td>"; 
                    htmlOther += "<td>"+val.installment_no+"</td>"; 
                    htmlOther += "<td>"+val.lpc_charges+"</td>"; 
                    htmlOther += "</tr>";
                    $('.installmentAppend').append(htmlOther);
                    k++;
                });
             
            

            $.each(data.otherPayments, function(key2, val2) {
                                 //console.log(val);
                    htmlOther = "<tr>";
                    htmlOther += "<td>"+j+"</td>";
                    htmlOther += "<td>"+val2.payment_desc+"</td>"; 
                    htmlOther += "<td>"+val2.installment_no+"</td>"; 
                    htmlOther += "<td>"+val2.lpc_charges+"</td>"; 
                    htmlOther += "</tr>";
                    $('.otherInstallmentAppend2').append(htmlOther);
                    j++;
                });

            
              },
            error: function(xhr, status, response){
                
                
                
                console.log('.otherInstallmentAppend2');
            }
        });

     });

    
        
        
    </script>
@endsection

