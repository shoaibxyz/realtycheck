@extends('admin.layouts.template')
@section('title','LPC')

@section('bookings-active','active')
@section('lpc-active','active')
@section('define_lpc-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Define LPC'])

 <?php if( Session::has('message')) : 
    $alertType = ( Session('status') == 1 ) ? "note-success" : "note-danger";
 ?>
    <div class="note {{ $alertType }}">
        {{ Session('message') }}
    </div>
<?php endif; ?>


<div class="x_panel">
    <div class="x_title">
    <h2>Define Late payment charges</h2>
    <div class="clearfix"></div>
</div>   
    
    <div class="col-sm-6 col-sm-offset-3">      
        {!! Form::model($lpcDef, ['method' => 'POST', 'route' => 'lpc-definition.store', 'class' => 'form-horizontal']) !!}
        
        <div class="form-group">
            {!! Form::label('period_from', 'Period From') !!}
            {!! Form::text('period_from', null, ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'period_from']) !!} 
        </div>


        <div class="form-group">
            {!! Form::label('period_to', 'Period to') !!}
            {!! Form::text('period_to', null, ['class' => 'form-control datepicker', 'required'=>'required', 'id' => 'period_to']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('amount_from', 'Amount From') !!}
            {!! Form::text('amount_from', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'amount_from']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('amount_to', 'amount to') !!}
            {!! Form::text('amount_to', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'amount_to']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('delay_from', 'delay From') !!}
            {!! Form::text('delay_from', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'delay_from']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('delay_to', 'delay to') !!}
            {!! Form::text('delay_to', null, ['class' => 'form-control', 'waive_days'=>'required', 'id' => 'delay_to']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('waive_days', 'Waive Days') !!}
            {!! Form::text('waive_days', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'waive_days']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('fix_charges', 'Fix Charges') !!}
            {!! Form::text('fix_charges', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'fix_charges']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('rate', 'Rate') !!}
            {!! Form::text('rate', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'rate']) !!} 
        </div>

        <div class="form-group">
            {!! Form::label('frequency', 'Frequency') !!}
            <?php 
                $frequency[''] = 'Choose';
                $frequency[1] = 'Daily';
                $frequency[2] = 'Monthly';
                $frequency[3] = 'Yearly';
            ?>
            {!! Form::select('frequency', $frequency, old('frequency'), ['class' => 'form-control', 'required'=>'required', 'id' => 'frequency']) !!} 
        </div>

        <div class="form-group">
            {!! Form::submit("Save", ['class' => 'btn btn-success']) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
@endsection



@section('javascript')
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    </script>
@endsection