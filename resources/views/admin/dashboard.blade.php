@extends('admin.layouts.template')

@section('title','Dashboard')

@section('home-active','active')


@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')
    
{{-- Include a dynamic file --}}
@include('admin/common/breadcrumb',['page'=>'Dashboard'])
    

    <div class="content">
        <div id="sum_box" class="row mbl" style="margin-top:20px;">
            <div class="col-sm-6 col-md-6">
                <div class="panel profit db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-book"></i></p>
                        <h4 class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                data-duration="0"></span><span>Total Assigned Files = {{ $totalFiles }}</span></h4>
                        <h4 class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                data-duration="0"></span><span>Cancelled Files = {{ $cancelFiles }}</span></h4>
                        <h4 class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                data-duration="0"></span><span>Refunded Files = {{ $refFiles }}</span></h4>
                        <h4 class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                data-duration="0"></span><span>Merged Files = {{ $merFiles }}</span></h4>
                        <h4 class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                data-duration="0"></span><span>Remaining Files = {{ $remFiles }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="panel visit db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-bookmark-o"></i></p>
                        <h4 id="t_visitors" class="value"><span>Total Bookings = {{ $bookings_count }}</span></h4>
                        <h4 id="t_visitors" class="value"><span>Cancelled = {{ $cancelbookings_count }}</span></h4>
                        <h4 id="t_visitors" class="value"><span>Refunded = {{ $refbookings_count }}</span></h4>
                        <h4 id="t_visitors" class="value"><span>Merged = {{ $merbookings_count }}</span></h4>
                        <h4 id="t_visitors" class="value"><span>Remaining = {{ $rembookings_count }}</span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div id="sum_box" class="row mbl" style="margin-top:20px;">
            <div class="col-sm-3 col-md-3">
                <div class="panel income db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-share-square"></i></p>
                        <p class="description">Inst. Received %</p>
                        <h4 id="t_vendors" class="value"><span>{{ $memberInstallments . ' / ' .$installmentReceived }}%</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <div class="panel task db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-group"></i></p>
                        <p class="description">Total Members</p>
                        <h4 id="t_businesses" class="value"><span>{{ $members }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <div class="panel agent db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-group"></i></p>
                        <p class="description">Total Agents</p>
                        <h4 id="t_businesses" class="value"><span>{{ $agents }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <div class="panel plans db mbm">
                    <div class="panel-body"><p class="icon"><i class="icon fa fa-briefcase"></i></p>
                        <p class="description">Total Plans</p>
                        <h4 id="t_businesses" class="value"><span>{{ $paymentPlans }}</span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Bookings & Installments</h3>
                  </div>
                  <div class="col-md-6">
                    {!! Form::open(['route' => 'dashboardGraph', 'id' => 'dashboardGraphRange']) !!}
                    <div  class="pull-right reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                      <input type="hidden" id="startdate" name="startdate"/>
                      <input type="hidden" name="enddate" id="enddate" />
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span>Jan 1, 2017 - <?php echo date('M d, Y'); ?></span>
                      <b class="caret"></b>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
                  <div id="container"></div>
                  {{-- <div id="chart_plot_01" class="demo-placeholder"></div> --}}
                </div>
                {{-- <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>Top Campaign Performance</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Facebook Campaign</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Twitter Campaign</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>Conventional Media</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Bill boards</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="clearfix"></div>
              </div> --}}
            </div>
        </div>
        <br />
        @if (Auth::user()->roles[0]->name === 'admin' or Auth::user()->roles[0]->name === 'Super-Admin')
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Activities</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
    
                    <ul class="list-unstyled timeline widget">
                      @if($activites)
                        @foreach($activites as $activity)
                        <li>
                          <div class="block">
                            <div class="block_content">
                              <p class="excerpt">
                                <a>{!! $activity->description !!}</a>
                              </p>
                              <div class="byline">
                                <span>{{ $activity->created_at->format('d-m-Y h:i:s') }}</span> by <a>{{ Auth::user()->name }}</a>
                              </div>
                            </div>
                          </div>
                        </li>
                        @endforeach
                      @endif
                     </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>SMS Stats</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
    
                    <ul class="list-unstyled timeline widget">
                        <li>
                          <div class="block">
                            <div class="block_content">
                              <p class="excerpt">
                                 {!! $balance_inquiry !!}
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="block">
                            <div class="block_content">
                              <p class="excerpt">
                                {!! $package_expiry !!}
                              </p>
                            </div>
                          </div>
                        </li>
                     </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    @endif
@endsection


@section('javascript')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

  <script type="text/javascript">
  $(function() {
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    });
    function getGraph(startDate = null, endDate = null){

        $.ajax({
          url: '{{ route('dashboardGraph') }}',
          type: 'GET',
          dataType: 'JSON',
          data: {startDate: startDate, endDate : endDate},
        })
        .done(function(data) {

            var obj = data.installments_received;
            var bookingIncome = data.booking_income;
            var ballotingReceived = data.ballotingReceived;

            Highcharts.chart('container', {
            chart: {
                  type: 'areaspline',
                  color: '#000'
              },
            title: {
                text: 'Bookings & Installment Revenue'
            },

            legend: {
                    layout: 'horizontal',
                    align: 'center',
                    lineColor: '#1C3146',
                    verticalAlign: 'bottom',
                    borderWidth: 0
                },
            yAxis: {
                title: {
                    text: 'Amount in PKR'
                }
            },
            xAxis: {
                categories: $.each(obj.time, function(key, value) {
                               value
                            })
            },
             rangeSelector: {
                    allButtonsEnabled: true,
                    selected: 2
                },
            tooltip: {
                shared: true,
                // valueSuffix: ' units'
            },
            series: [
            {
                name: 'Installments Received',
                data: $.each(obj.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Booking Received',
                data: $.each(bookingIncome.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Balloting Received',
                data: $.each(ballotingReceived.amount, function(key, value) {
                         value
                      })
            },
            {
                name: 'Booked Files',
                data: data.bookings
            }]
          });
          })
          .fail(function() {
            console.log("error");
          });
    }

    getGraph();

    // $('#dashboardGraphRange').submit(function(e){
    //   e.preventDefault();

    // });


    $('.reportrange').daterangepicker(
          {
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                  'Last 7 Days': [moment().subtract('days', 6), moment()],
                  'Last 30 Days': [moment().subtract('days', 29), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment().subtract('days', 29),
              endDate: moment(),
              opens: 'left'
          },
          function(start, end) {
              $('.reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
              $('input[name="startdate"]').val(start.format("YYYY-MM-DD"));
              $('input[name="enddate"]').val(end.format("YYYY-MM-DD"));

              var startDate = $('input[name="startdate"]').val();
              var endDate = $('input[name="enddate"]').val();

              getGraph(startDate, endDate);
          }
      );

});


</script>

    @endsection
