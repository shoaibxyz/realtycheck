@extends('admin.layouts.template')

@section('title','Categary')

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Category List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Category List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Category List</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ url('admin/category/create')}}" class="btn btn-primary" > <i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
                    <thead>
                        <tr>
                            <th style="width: 5%">#Id</th>
                            <th style="width: 25%">Name</th>
                            <th style="width: 20%">Created By</th>
                            <th class="text-center">Actions</th>
                            
                         
                        
        
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($category as $c)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $c->name }}</td>
                                <td>{{ $c->user->name }}</td>
                                 <td class="text-center">
                                     
                                    <a href="{{ url('admin/category/edit', $c->id )}}" class="btn btn-info btn-xs" ><i class="fa fa-edit"></i></a>
                                    
                                    {!! Form::open(['method' => 'DELETE', 'url' => ['admin/category/destroy', $c->id] ]) !!}
                                    <button class="btn btn-danger btn-xs" type="submit" value=""  title="delete"><i class="fa fa-trash-o"></i></button>
                                {!! Form::close() !!}
                                    </td>
                            </tr>
                                   
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    

@endsection
