@extends('admin.layouts.template')

@section('title','Edit Category')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Category</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Category</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($category, ['method' => 'GET', 'url' => ['admin/category/update', $category->id], 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PUT') !!}
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ url('admin/category') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
