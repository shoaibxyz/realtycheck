<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ asset('public/vendor/')}}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css" media="print">
    <!-- Font Awesome -->
    <link href="{{ asset('public/vendor/')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('public/vendor/')}}/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('public/vendor/')}}/iCheck/skins/flat/green.css" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('public/vendor/')}}/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    {{-- <link href="{{ asset('public/vendor/')}}/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/> --}}
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('public/vendor/')}}/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ asset('public/vendor/')}}/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('public/')}}/css/custom.min.css" rel="stylesheet">

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/') }}/css/datatables.bootstrap.min.css">
    
    <!-- Loading template style-->
    <link rel="stylesheet" href="{{ asset('public/css/yellow-grey.css') }}"/>
    <link rel="stylesheet" href="{{ asset('public/css/style-responsive.css') }}"/>
   @yield('style')
   
  </head>

  <body>
      
   @include('admin.layouts.top-nav')
   <div id="wrapper">
    
        @include('admin.layouts.sidebar')
         
        <div id="page-wrapper">
            <!-- page content -->
            <div class="page-content">
                @yield('content')
           </div>
        <!-- /footer content -->
        </div>
      
        <footer id="footer">
          <div class="pull-right">
            &copy; X Effect Studio {{ date('Y') }} All Rights Reserved
          </div>
          <div class="clearfix"></div>
        </footer>
   
    </div>
    <!-- jQuery -->
    {{-- <script src="{{ asset('public/vendor/')}}/jquery/dist/jquery.min.js"></script> --}}
    <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
<script src="{{ asset('public/js/libraries/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('public/vendor/')}}/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    {{-- <script src="{{ asset('public/vendor/')}}/fastclick/lib/fastclick.js"></script> --}}
    <!-- NProgress -->
    <script src="{{ asset('public/vendor/')}}/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    {{-- <script src="{{ asset('public/vendor/')}}/Chart.js/dist/Chart.min.js"></script> --}}
    <!-- gauge.js -->
    {{-- <script src="{{ asset('public/vendor/')}}/gauge.js/dist/gauge.min.js"></script> --}}
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('public/vendor/')}}/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="{{ asset('public/vendor/')}}/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    {{-- <script src="{{ asset('public/vendor/')}}/skycons/skycons.js"></script> --}}
    <!-- Flot -->
    {{-- <script src="{{ asset('public/vendor/')}}/Flot/jquery.flot.js"></script>
    <script src="{{ asset('public/vendor/')}}/Flot/jquery.flot.pie.js"></script>
    <script src="{{ asset('public/vendor/')}}/Flot/jquery.flot.time.js"></script>
    <script src="{{ asset('public/vendor/')}}/Flot/jquery.flot.stack.js"></script>
    <script src="{{ asset('public/vendor/')}}/Flot/jquery.flot.resize.js"></script> --}}
    <!-- Flot plugins -->
    {{-- <script src="{{ asset('public/vendor/')}}/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="{{ asset('public/vendor/')}}/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="{{ asset('public/vendor/')}}/flot.curvedlines/curvedLines.js"></script> --}}
    <!-- DateJS -->
    {{-- <script src="{{ asset('public/vendor/')}}/DateJS/build/date.js"></script> --}}
    <!-- JQVMap -->
    {{-- <script src="{{ asset('public/vendor/')}}/jqvmap/dist/jquery.vmap.js"></script>
    <script src="{{ asset('public/vendor/')}}/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="{{ asset('public/vendor/')}}/jqvmap/examples/js/jquery.vmap.sampledata.js"></script> --}}
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('public/vendor/')}}/moment/min/moment.min.js"></script>
    <script src="{{ asset('public/vendor/')}}/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script src="{{ asset('public/vendor/')}}/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('public/')}}/js/custom.js"></script>
    
<!--loading bootstrap js-->
<script src="{{ asset('public/vendor/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('public/vendor/slimScroll/jquery.slimscroll.js') }}"></script>
<script src="https://pakcheers.com/public/vendors/jquery-cookie/jquery.cookie.js"></script>
<script src="{{ asset('public/js/libraries/jquery.menu.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
<script src="{{ asset('public/js/libraries/main.js') }}"></script>

    @yield('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    
    <script>
        $('#datatable').dataTable({ "bSort" : false } );
        $('select').select2();
        
        $('.selectpicker').selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
    
   
    function checkMove(path)
    {
        if($('#hfcanmove').val() == "1") {
            $.confirm({
                'title': 'Confirmation',
                'message': 'You are about to leave this page. <br />Are you sure you want to leave?',
                'buttons': {
                    'Yes': {
                        'class': 'blue',
                        'action': function () {
                            window.location = path;
                        }
                    },
                    'No': {
                        'class': 'gray',
                        'action': function () {
                        }	// Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
        }
        else
            window.location = path;
    }

    function showError(msg)
    {
        $.confirm({
            'title': 'Error',
            'message': msg,
            'buttons': {
                'Ok': {
                    'class': 'blue',
                    'action': function () {

                    }
                }
            }
        });
    }
    
    $(document).keydown(function(e) {
    //     console.log(e.key);
         /*if(e.key == "M" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('users.index') }}";
         }*/
    //     else if(e.key == "m") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('users.create') }}";
    //     }
         /*else if(e.key == "B" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('booking.index') }}";
         }*/
    //     else if(e.key == "b") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('booking.create') }}";
    //     }
         /*else if(e.key == "R" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('receipt.index') }}";
         }*/
    //     else if(e.key == "r") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('receipt.create') }}";
    //     }
         /*else if(e.key == "T" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('transfer.index') }}";
         }*/
    //     else if(e.key == "t") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('transfer.init') }}";
    //     }
         /*else if(e.key == "P" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('payment-schedule.index') }}";
         }*/
    //     else if(e.key == "p") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('payment-schedule.create') }}";
    //     }
         /*else if(e.key == "A" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('agency.index') }}";
         }*/
    //     else if(e.key == "a") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('agency.create') }}";
    //     }
         /*else if(e.key == "F" && e.shiftKey && !e.ctrlKey) {
             window.location = "{{ route('assign-file.index') }}";
         }*/
    //     else if(e.key == "f") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('assign-file.create') }}";
    //     }
    //     /*else if(e.key == "B" && e.shiftKey && !e.ctrlKey) {
    //         window.location = "{{ route('batch.index') }}";
    //     }
    //     else if(e.key == "b") {
    //         if(!e.ctrlKey)
    //             window.location = "{{ route('batch.create') }}";
    //     }*/
    });
    
    </script>
  </body>
</html>
