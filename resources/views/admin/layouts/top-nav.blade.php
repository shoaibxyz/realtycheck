
 


<div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;"
             class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span></button>
            <div class="logo-bar">
                <a href="{{ URL::to('admin/dashboard') }}" class="site_title">
      <img src="{{ asset('public/') }}/images/opt-logo.png"  class="logo1">
      <img src="{{ asset('public/') }}/images/RC.png"  class="logo2">

    </a></div>
            </div>
            <div class="topbar-main">
                <a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                    
                    
                    <li class="dropdown topbar-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ env('STORAGE_PATH'). Auth::user()->picture }}" alt="" class="img-responsive img-circle" />&nbsp;<span class="hidden-xs">{{ Auth::user()->name }}</span>&nbsp;<span
                            class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                            <li><a href="{{ URL('member') }}"><i class="fa fa-user"></i>My Profile</a></li>
                           <li><a href="{{ route('admin.setting') }}"><i class="fa fa-calendar"></i>Settings</a></li>
                            <li><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"><i
                                        class="fa fa-sign-out"></i>Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
