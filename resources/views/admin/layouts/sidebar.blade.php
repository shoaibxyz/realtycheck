    <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">
                    <li class="user-panel">
                        
                        
                    </li>
                   
                    <li class="@yield('home-active')" style="{{ (in_array("dashboard", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('dashboard')}}"><i class="fa fa-tachometer fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Dashboard</span></a></li>
                    
                    <li class="@yield('user-management-active')" style="{{ (in_array("user-management", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-users fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">User Management</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="@yield('modules-active')" style="{{ (in_array("modules", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('module.index') }}"><span
                                            class="submenu-title">Modules</span></a></li>
                            <li class="@yield('roles-active')" style="{{ (in_array("roles", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('roles.index') }}"><span
                                            class="submenu-title">Roles</span></a></li>
                            <li class="@yield('permissions-active')" style="{{ (in_array("permissions", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('permissions.index') }}"><span
                                            class="submenu-title">Permissions</span></a></li>
                        </ul>
                    </li>
					
					<li class="@yield('bookings-active')" style="{{ (in_array("bookings", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-dollar fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">Bookings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="@yield('agencies-active')"  style="{{ (in_array("agency", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Agency</span><span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
                                    <li class="@yield('agency-active')" style="{{ (in_array("agency", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('agency.index') }}"><span class="submenu-title">Agency</span></a></li>
									 <li class="@yield('batch-active')" style="{{ (in_array("batch", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('batch.index') }}"><span class="submenu-title">Batch</span></a></li>
													
													
									 <li class="@yield('assign_file-active')" style="{{ (in_array("assign-file", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('assign-file.index') }}"><span class="submenu-title">Assign File</span></a></li>
                                   
                                </ul>
											
							</li>
											
							<li class="@yield('plot-active')" style="{{ (in_array("plotsize", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Plot</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
                                    <li class="@yield('plotsize-active')" style="{{ (in_array("plotsize", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('plotsize.index') }}"><span class="submenu-title">Plot Size</span></a></li>
									 <li class="@yield('features-active')" style="{{ (in_array("features", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('features.index') }}"><span class="submenu-title">Feature Set</span></a></li>
								 <li class="@yield('payment_schedule-active')" style="{{ (in_array("payment-schedule", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('payment-schedule.index')}}"><span class="submenu-title">Payment Plans</span></a></li>
						
                                    </ul>
											
							</li>
							
							<li class="@yield('bank-active')" style="{{ (in_array("bank", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('bank.index')}}"><span class="submenu-title">Bank Accounts</span></a></li>
                                                    
                           
                          
                           							
							<li class="@yield('members-active')" style="{{ (in_array("members", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('users.index')}}"><span class="submenu-title">Members</span></a></li>
							
							<li class="@yield('booking-active')" style="{{ (in_array("booking", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('booking.index')}}"><span class="submenu-title">Booking</span></a></li>
							
							<li class="@yield('receipts-active')" style="{{ (in_array("receipts", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('receipt.index') }}"><span class="submenu-title">Receipts</span></a></li>
							
							
							<!--<li class="@yield('pdc-active')"><a href="#"><span-->
       <!--                                     class="submenu-title">PDC</span><span class="fa arrow"></span></a>-->
							<!--				<ul class="nav nav-third-level">-->
       <!--                             <li class="@yield('all_pdc-active')" style="{{ (in_array("all-pdc", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="javascript:void(0)" onclick="checkMove('{{ route('pdc.index') }}')"><<span class="submenu-title">All PDC</span></a></li>-->
							<!--		 <li class="@yield('send_clear-active')" style="{{ (in_array("send-clear", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="javascript:void(0)" onclick="checkMove('{{ route('pdc.send') }}')"><span class="submenu-title">Send Clear</span></a></li>-->
													
									
							<!--		<li class="@yield('mark_clear-active')" style="{{ (in_array("mark-clear", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="javascript:void(0)" onclick="checkMove('{{ route('pdc.mark') }}')"><span class="submenu-title">Mark Clear</span></a></li>-->
													
							<!--		<li class="@yield('verify-active')" style="{{ (in_array("verify", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="javascript:void(0)" onclick="checkMove('{{ route('pdc.verify') }}')"><span class="submenu-title">Verify</span></a></li>-->
       <!--                         </ul>-->
											
							<!--				</li>-->
						    <li class="@yield('lpc-active')" style="{{ (in_array("lpc", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">LPC</span><span class="fa arrow"></span></a>
											<ul class="nav nav-third-level">
                                    <li class="@yield('define_lpc-active')" style="{{ (in_array("define-lpc", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('lpc-definition') }}"><span class="submenu-title">Define LPC</span></a></li>
									 <li class="@yield('calculate_lpc-active')" style="{{ (in_array("calculate-lpc", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('lpc-calculate.index') }}"><span class="submenu-title">Calculate LPC</span></a></li>
									<li class="@yield('waive_charges-active')" style="{{ (in_array("waive-charges", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('lpc.waive') }}"><span class="submenu-title">Waive Charges</span></a></li>
									<li class="@yield('waiveRegisteration-active')" style="{{ (in_array("waiveRegisteration", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('lpc.waiveRegisteration') }}"><span class="submenu-title">Waive Charges Against Registration Number</span></a></li>				
									 
                                   
                                </ul>
											
											</li>
											
							<!--<li class="@yield('reschedule-active')" style="{{ (in_array("reschedule", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="javascript:void(0)" onclick="checkMove('{{ route('reschedule.index') }}')"><span class="submenu-title">Reschedule</span></a></li>				-->
							
							
							
							<li class="@yield('transfers-active')" style="{{ (in_array("transfer", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Transfer</span><span class="fa arrow"></span></a>
											<ul class="nav nav-third-level">
                                    <li class="@yield('transfer-active')" style="{{ (in_array("transfer", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('transfer.index') }}"><span class="submenu-title">All Transfers</span></a></li>
                                    <li class="@yield('transfer_charges-active')" style="{{ (in_array("transfer-charges", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('transfer_charges.index') }}"><span class="submenu-title">Transfer Charges</span></a></li>
									 <li class="@yield('transfer_app-active')" style="{{ (in_array("transfer-application", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('transfer.init') }}" ><span class="submenu-title">Transfer Application</span></a></li>
									<li class="@yield('transfer_sheet-active')" style="{{ (in_array("transfer-sheet", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('transfer.create') }}"><span class="submenu-title">Transfer Documents</span></a></li>
									<li class="@yield('ndc-active')" style="{{ (in_array("ndc", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('transfer.ndc') }}"><span class="submenu-title">NDC</span></a></li>
									 
                                   
                                </ul>
											
											</li>
											
                        </ul>
                    </li>
                    
					
					<li class="@yield('marketing-active')" style="{{ (in_array("marketing", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-inbox fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">Marketing</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">

						<li class="@yield('sms-active')" style="{{ (in_array("sms", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('sms.index') }}"><span class="submenu-title">SMS</span></a></li>

                        </ul>
                    </li>
                    
                    
                    <li class="@yield('leads-active')" style="{{ (in_array("Lead", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('leads.dashboard')}}"><i class="fa fa-phone">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Leads & Clients</span></a></li>
                    
                    
                    <li class="@yield('support-active')" style="{{ (in_array("support", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-inbox fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">Support</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            
											
							
							
						<li class="@yield('category-active')" style="{{ (in_array("category", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/category') }}"><span class="submenu-title">Category</span></a></li>
                                    
				        <li class="@yield('task-active')" style="{{ (in_array("task", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/support/task') }}"><span class="submenu-title">Task</span></a></li>
                          
							
							
							
							
				
                        </ul>
                    </li>
                    
                    
                    <li class="@yield('reports-active')" style="{{ (in_array("reports", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-database fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">Reports</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            
                            
							
							<li class="@yield('booking-report-active')" style="{{ (in_array("booking-report-head", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Booking Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									   <li class="@yield('booking-reports-active')" style="{{ (in_array("booking-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.bookings') }}" ><span class="submenu-title">Booking Report</span></a></li>
                                                                
                                        <li class="@yield('batch-files-report-active')" style="{{ (in_array("batch-files-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.batch-files') }}"><span class="submenu-title">Batch Files Report</span></a></li>		
									
                                    <li class="@yield('cancel_report-active')" style="{{ (in_array("cancel-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.cancelReport') }}"><span class="submenu-title">Book Cancel Report</span></a></li> 
                                                        
                                    <li class="@yield('receipt-daily-report-active')" style="{{ (in_array("receipt-daily-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.dailyReceipt') }}"><span class="submenu-title">Daily Receipt Report</span></a></li>
                                    
                                    <li class="@yield('inventory-report-active')" style="{{ (in_array("inventory-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.InventoryReport') }}"><span class="submenu-title">Inventory Report</span></a></li>
                                      
                                    <li class="@yield('open-file-cancel-report-active')" style="{{ (in_array("open-file-cancel-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/report/merged') }}"><span class="submenu-title">Open Cancel Report</span></a></li> 
                                   
                                                          
                                    </ul>
											
							</li>
							
							
							<li class="@yield('installment-report-active')" style="{{ (in_array("head-due-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Due/Over Due Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									   <li class="@yield('accumulated-due-report-active')" style="{{ (in_array("accumulated-due-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.CheckdueAccumulatedAmounts') }}"><span class="submenu-title">Accumulated Due Report</span></a></li>	
                                                            
                                         <li class="@yield('defaulters-report-active')" style="{{ (in_array("defaulters-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.defaulters') }}"><span class="submenu-title">Defaulters Report</span></a></li>                              
                            <li class="@yield('due-report-active')" style="{{ (in_array("due-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.due_amount') }}"><span class="submenu-title">Due Report</span></a></li>	
                                                            
                                <li class="@yield('over-due-report-active')" style="{{ (in_array("over-due-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.overdue_amount') }}"><span class="submenu-title">Over Due Report</span></a></li>
                            <li class="@yield('paid-report-active')" style="{{ (in_array("paid-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.check-paid-inst') }}" ><span class="submenu-title">Paid Report</span></a></li> 
                                                        
                            
                            <li class="@yield('recovery-report-active')" style="{{ (in_array("recovery-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.recovery-report') }}"><span class="submenu-title">Recovery Report</span></a></li>
									
                                    
                                    </ul>
											
							</li>
							
							
								<li class="@yield('user-report-active')" style="{{ (in_array("user-report-head", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">User Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									
									<li class="@yield('agency-report-active')" style="{{ (in_array("agency-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.agencyreport') }}"><span class="submenu-title">Agency Report</span></a></li>
									    
									<li class="@yield('c-h-customers-report-active')" style="{{ (in_array("c-h-customers-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.CustomerFeedbackReportForm') }}"><span class="submenu-title">Customers Report</span></a></li>
                                    
                                    <li class="@yield('members-report-active')" style="{{ (in_array("members-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.membersReports') }}" ><span class="submenu-title">Members Report</span></a></li>
                                    
                                    <li class="@yield('salary-report-active')" style="{{ (in_array("salary-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('salaryReport') }}"><span class="submenu-title">Salary Report</span></a></li>
                                                         
                                         
									
                                    
                                    </ul>
											
							</li>
							
							<li class="@yield('discount-report-active')" style="{{ (in_array("discount-report-head", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Discount Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									   <!--<li class="@yield('management-discount-active')" style="{{ (in_array("management-discount", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.rebate_type', 'management discount') }}"><span class="submenu-title">Management Discount</span></a></li>
                            
                            <li class="@yield('promotional-discount-active')" style="{{ (in_array("promotional-discount", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.rebate_type', 'promotion discount') }})"><span class="submenu-title">Promotional Discount</span></a></li>-->
                            <li class="@yield('rebate-discount-active')" style="{{ (in_array("rebate-discount", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.rebate_type', 'rebate discount') }}"><span class="submenu-title">Rebate Discount</span></a></li>		
                                         
									
                                    
                                    </ul>
											
							</li>
							
							
 
                            <li class="@yield('marketing-report-active')" style="{{ (in_array("marketing-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Marketing Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									  
									<li class="@yield('alert-report-active')" style="{{ (in_array("alert-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.alert') }}"><span class="submenu-title">Alert Report</span></a></li> 
                                    
									<li class="@yield('sms-report-active')" style="{{ (in_array("sms-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.sms') }}"><span class="submenu-title">SMS Report</span></a></li>
							
                                    </ul>
											
							</li>
							
							<li class="@yield('sales-report-active')" style="{{ (in_array("bank-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span class="submenu-title">Sales Report</span><span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
								    <li class="@yield('bank-report-active')" style="{{ (in_array("bank-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.bank') }}"><span class="submenu-title">Bank Report</span></a></li>
								    <li class="@yield('sale-report-active')" style="{{ (in_array("sale-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.saleSummary') }}"><span class="submenu-title">Sales Report</span></a></li>
							    </ul>
							</li>
							
							<li class="@yield('other-report-active')"><a href="#"><span
                                            class="submenu-title">Others Report</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									   <li class="@yield('activity-logs-active')" style="{{ (in_array("activity-logs", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.activity') }}"><span class="submenu-title">Activity Logs</span></a></li> 
                                       
							<li class="@yield('lpc-report-active')" style="{{ (in_array("lpc-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.getlpcReport') }}"><span class="submenu-title">LPC Report</span></a></li>                                  
                            

                            <li class="@yield('transfer-report-active')" style="{{ (in_array("transfer-report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('report.transferReport') }}"><span class="submenu-title">Transfer Report</span></a></li> 		
                            
                           <!--<li class="@yield('summary_report-active')" style="{{ (in_array("summary_report", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/report/summaryreport') }}"><span class="submenu-title">Summary Report</span></a></li>-->
                            
                            
                                   
                                    </ul>
											
							</li>
                    
                        </ul>
                    </li>
					
					<li class="@yield('HR-active')" style="{{ (in_array("HR", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-sitemap fa-fw">
                        <div class="icon-bg bg-pink"></div>
                     </i><span class="menu-title">Human Resource</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            
                           
						
						<li class="@yield('employee-head-active')" style="{{ (in_array("Employee", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><span
                                            class="submenu-title">Employee</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
                                    <li class="@yield('designation-active')" style="{{ (in_array("Designation", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('designation.index') }}">
							        
							        <span class="submenu-title">Designation</span>
							    </a>
							</li>
									 <li class="@yield('department-active')" style="{{ (in_array("Department", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('department.index') }}">
							        
							        <span class="submenu-title">Department</span>
							    </a>
							</li>
							
							<li class="@yield('employees-active')" style="{{ (in_array("Employee", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('employee.index') }}">
							        
							        <span class="submenu-title">Employee</span>
							    </a>
							</li>
							
								<li class="@yield('attendance-active')" style="{{ (in_array("attendance", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('attendance.index') }}">
							        
							        <span class="submenu-title">Attendance</span>
							    </a>
							</li>
							
                                    </ul>
											
							</li>

                            <li class="@yield('payroll-active')"><a href="#"><span
                                            class="submenu-title">Payroll</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
                                    <li class="@yield('generate-payslip-active')" style="{{ (in_array("Generate-Payslip", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('payslip.create') }}"><span class="submenu-title">Generate Payslip</span></a></li>
									 <li class="@yield('payroll-summary-active')" style="{{ (in_array("Payroll-Summary", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ route('payslip.checkPayslip') }}"><span class="submenu-title">Payroll Summary</span></a></li>
                                            
                                     <li class="@yield('advance-salary-active')" style="{{ (in_array("Advance-Salary", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('advance-salary.index') }}">
							        
							        <span class="submenu-title">Advance-Salary</span>
							    </a>
							</li>
							
							<li class="@yield('tax-active')" style="{{ (in_array("Tax", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('tax.index') }}">
							        
							        <span class="submenu-title">Tax</span>
							    </a>
							</li>
								
                                    </ul>
											
							</li>

							<li class="@yield('leave-active')"><a href="#"><span
                                            class="submenu-title">Leave</span><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
									    
									 <li class="@yield('leaves-active')" style="{{ (in_array("Leave-Type", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('leave.index') }}">
							        
							        <span class="submenu-title">Leave Type</span>
							    </a>
							</li>
							
                                    <li class="@yield('leaves-type-active')" style="{{ (in_array("Leave", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
							    <a href="{{ route('leave-request.index') }}">
							        
							        <span class="submenu-title">Leave Requests</span>
							    </a>
							</li>

                                    </ul>
											
							</li>
						   
                        </ul>
                    </li>
					<li class="@yield('acc-dashboard-active')"  style="{{ (in_array("accounting", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="#"><i class="fa fa-bar-chart-o fa-fw">
                        <div class="icon-bg bg-pink"></div>
                     </i><span class="menu-title">Accounting</span><span class="fa arrow"></span></a>
                     
                        <ul class="nav nav-second-level">
                            
						 <li class="@yield('acc-dashboard-active')" style="{{ (in_array("acc-dashboard", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/accounting/acc-dashboard') }}"><span
                                            class="submenu-title">Dashboard</span></a></li>
<li class="@yield('accounting-head-active')"  style="{{ (in_array("charts-of-accounts", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
    <a href="#"><span class="submenu-title">Accounts</span><span class="fa arrow"></span></a>
		<ul class="nav nav-third-level">
          
	        <li class="@yield('charts-of-accounts-active')" style="{{ (in_array("charts-of-accounts", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/accounts") }}">
			        
			        <span class="submenu-title">Charts of Accounts</span>
			    </a>
			</li>
	
	
        </ul>
					
</li>
<li class="@yield('entry-types-head-active')" style="{{ (in_array("entry-types", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
    <a href="#"><span class="submenu-title">Entry Types</span><span class="fa arrow"></span></a>
		<ul class="nav nav-third-level">
          
	        <li class="@yield('entry-types-active')" style="{{ (in_array("entry-types", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/entrytypes") }}">
			        
			        <span class="submenu-title">All Entry Types</span>
			    </a>
			</li>
	
	
        </ul>
					
</li>							
<li class="@yield('tags-head-active')" style="{{ (in_array("tags", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
    <a href="#"><span class="submenu-title">Tags</span><span class="fa arrow"></span></a>
		<ul class="nav nav-third-level">
          
	        <li class="@yield('tags-active')" style="{{ (in_array("tags", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/tags") }}">
			        
			        <span class="submenu-title">All Tags</span>
			    </a>
			</li>
	
	
        </ul>
					
</li>
<li class="@yield('entries-head-active')" style="{{ (in_array("entries", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
    <a href="#"><span class="submenu-title">Entries</span><span class="fa arrow"></span></a>
		<ul class="nav nav-third-level">
          
	        <li class="@yield('entries-active')" style="{{ (in_array("entries", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/entries") }}">
			        
			        <span class="submenu-title">All Entries</span>
			    </a>
			</li>
	
	
        </ul>
					
</li>
<li class="@yield('search-active')" style="{{ (in_array("search", Session::get('objects'))) ? 'display:block' : 'display:none' }}"><a href="{{ url('admin/accounting/search') }}"><span
                                            class="submenu-title">Search</span></a></li>
<li class="@yield('acc-reports-head-active')" style="{{ (in_array("acc-reports", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
    <a href="#"><span class="submenu-title">Reports</span><span class="fa arrow"></span></a>
		<ul class="nav nav-third-level">
          
	        <li class="@yield('balance-sheet-active')" style="{{ (in_array("balance-sheet", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/balance_sheet") }}">
			        
			        <span class="submenu-title">Balance Sheet</span>
			    </a>
			</li>
			  <li class="@yield('profit-loss-active')" style="{{ (in_array("profit-loss", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/profit_loss") }}">
			        
			        <span class="submenu-title">Profit Loss</span>
			    </a>
			</li>
	        <li class="@yield('trial-balance-active')" style="{{ (in_array("trial-balance", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/trial_balance") }}">
			        
			        <span class="submenu-title">Trial Balance</span>
			    </a>
			</li>
			<li class="@yield('ledger-statement-active')" style="{{ (in_array("ledger-statement", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/ledger_statement") }}">
			        
			        <span class="submenu-title">Ledger Statement</span>
			    </a>
			</li>
			<li class="@yield('ledger-reconciliation-active')" style="{{ (in_array("ledger-reconciliation", Session::get('objects'))) ? 'display:block' : 'display:none' }}">
			    <a href="{{ url("admin/accounting/ledger_reconcile") }}">
			        
			        <span class="submenu-title">Ledger Reconciliation</span>
			    </a>
			</li>
	
        </ul>
					
</li>							
							
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <!--END SIDEBAR MENU--><!--BEGIN CHAT FORM-->

        
        
        
