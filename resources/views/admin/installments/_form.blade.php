            <div class="form-group">
                {!! Form::label('installment_no', 'Installment No') !!}
                {!! Form::text('installment_no', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'installment_no']) !!}
            </div>
            <?php 
                $date_creteria['Months After Booking'] = 'Months After Booking';
            ?>
            <div class="form-group">
                {!! Form::label('date_creteria', 'Date Creteria') !!}
                {!! Form::select('date_creteria', $date_creteria, 0, ['required' =>'required', 'class' => 'form-control', 'id' =>'date_creteria']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('installment_date', 'Installment Date') !!}
                {!! Form::text('installment_date', \Carbon\Carbon::now()->format('Y-m-d'), ['required' =>'required', 'class' => 'form-control', 'id' => 'installment_date']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('booking_percentage', 'Booking Percentage') !!}
                {!! Form::text('booking_percentage', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'booking_percentage']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('booking_price', 'Booking Price') !!}
                {!! Form::text('booking_price', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'booking_price']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('plot_size', 'Plot size') !!}
                {!! Form::text('plot_size', null , ['required' =>'required', 'class' => 'form-control', 'id' => 'plot_size']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('payment_frequency', 'Payment Frequency') !!}
                {!! Form::select('payment_frequency', [null,'After 1 Month','After 2 Months','After 3 Month','After 4 Months'], old('payment_frequency'), ['required' =>'required', 'class' => 'form-control', 'id' => 'payment_frequency']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('installments', 'installments') !!}
                {!! Form::text('installments', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'installments']) !!} 
            </div>


            <div class="form-group">
                {!! Form::label('first_installment_date', 'first_installment_date') !!}
                 {!! Form::date('first_installment_date', date('Y-m-d'), ['required' =>'required', 'class' => 'form-control', 'id' => 'installments']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('plot_dimensions', 'plot dimensions') !!}
                {!! Form::text('plot_dimensions', null, ['required' =>'required', 'class' => 'form-control', 'id' => 'plot_dimensions']) !!} 
            </div>
        