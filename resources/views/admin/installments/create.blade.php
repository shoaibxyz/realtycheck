@extends('admin.layouts.template')

@section('title','Generate Payment Plan')

@section('bookings-active','active')
@section('payment_schedule-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Generate Payment Plan</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Generate Payment Plan</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Generate Payment Plan</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('payment-schedule.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;New Plan</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            {!! Form::open(['method' => 'POST', 'route' => ['installment.update', $plan->id], 'class' => 'form-horizontal inst_form', 'style' => 'padding:20px;']) !!}

                <h3>Plan Code: {{ $plan->payment_code}}
                    <span class="pull-right">Total Price: <span class="plot_total_price">{{ $plan->total_price }}</span></span>
                </h3>
                <ul class="nav nav-tabs" style="margin-top: 20px;">
                    <li class="active"><a data-toggle="tab" href="#Installment">Installment</a></li>
                    <li><a data-toggle="tab" href="#other_payments">Other Payments</a></li>
                </ul>
                <div class="tab-content">
                    <div id="Installment" class="tab-pane fade in active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped jambo_table bulk_action">
                                <thead>
                                    <tr>
                                        <th>Inst. No</th>
                                        <th>Days / Month</th>
                                        <th>Installment Amount</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {!! Form::hidden('_method', 'PUT') !!}
                                    <?php $months = 2; $i = 1; ?>
                                    @while($i <= $plan->installments)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! Form::text('days_or_months[]', $months) !!}</td>
                                            <td>
                                                <?php
                                                    //calculate 6% of total amount and add after every 6th month
                                                    $six_month = $plan->semi_anual_percentage + $plan->monthly_percentage;
                                                    $installment = ($i%6 ==0) ? $six_month :  $plan->monthly_percentage ;
                                                ?>
                                                {!! Form::text('inst[]', $installment, ['class' => 'inst_amount sum_inst installment_amount_'.$i, 'data-rowid' => $i]) !!}
                                            </td>
                                        </tr>
                                        {!! Form::hidden('loop[]', $i) !!}
                                        <?php $months++; $i++; ?>
                                    @endwhile
                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <h4>Sum Of Installments: <span class="displayInstSum"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="other_payments" class="tab-pane fade table-responsive">
                        <div class="table-responsive" style="overflow:auto;">
                            <table class="table table-bordered table-hover table-striped jambo_table bulk_action">
                                <thead>
                                    <tr>
                                        <th>Sr.#</th>
                                        <th>Payment Type</th>
                                        <th>Label</th>
                                        <th>Inst #</th>
                                        <th>Days/Month</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody class="otherPaymentsTable">
                                    <?php $months = 2; $j= 1;  ?>
                                    <tr class="otherPaymentsTableRow" data-row="{{ $j }}">
                                        <td class="sr_no">1</td>
                                        <td>
                                        <?php
                                            $payment_typeArr[''] = "";
                                            foreach ($payment_type as $type) {
                                                $payment_typeArr[$type->id] = $type->payment_type;
                                            }
                                        ?>
                                        {!! Form::select('other_payment_type_id[]', $payment_typeArr, 4) !!} </td>

                                        <td>
                                            {!! Form::text('other_payment_label[]', null) !!}
                                        </td>

                                        <td>
                                            {!! Form::text('other_installment_no[]', 1) !!}
                                        </td>
                                        <td>{!! Form::text('other_days_or_months[]', 1) !!}</td>
                                        <td>
                                            {!! Form::text('other_inst[]', $plan->booking_price, ['class' => 'inst_amount sum_other  installment_amount_'.$j]) !!}
                                        </td>
                                    </tr>
                                    <?php  $j++; ?>
                                    @if($plan->installments > 0)

                                    <tr class="otherPaymentsTableRow" data-row="{{ $j }}">
                                        <td class="sr_no">2</td>
                                        <td>
                                        <?php
                                            $payment_typeArr[''] = "";
                                            foreach ($payment_type as $type) {
                                                $payment_typeArr[$type->id] = $type->payment_type;
                                            }
                                        ?>
                                        {!! Form::select('other_payment_type_id[]', $payment_typeArr, 5) !!} </td>

                                        <td>
                                            {!! Form::text('other_payment_label[]', null) !!}
                                        </td>

                                        <td>
                                            <input name="other_installment_no[]" type="text" value="1">
                                        </td>
                                        <td>{!! Form::text('other_days_or_months[]', 5) !!}</td>
                                        <td>
                                            {!! Form::text('other_inst[]', $plan->pac_percentage, ['class' => 'inst_amount sum_other installment_amount_'.$j]) !!}
                                        </td>
                                    </tr>
                                    @endif
                                    @if($plan->installments > 0)

                                    <tr class="otherPaymentsTableRow" data-row="{{ $j }}">
                                        <td class="sr_no">3</td>
                                        <td>
                                        <?php
                                            $payment_typeArr[''] = "";
                                            foreach ($payment_type as $type) {
                                                $payment_typeArr[$type->id] = $type->payment_type;
                                            }
                                        ?>
                                        {!! Form::select('other_payment_type_id[]', $payment_typeArr, 1) !!} </td>

                                        <td>
                                            {!! Form::text('other_payment_label[]', null) !!}
                                        </td>

                                        <td>
                                            <input name="other_installment_no[]" type="text" value="{{ $plan->installments + 1 }}">
                                        </td>
                                        <td>{!! Form::text('other_days_or_months[]', 1) !!}</td>
                                        <td>
                                            {!! Form::text('other_inst[]', $plan->balloting_percentage, ['class' => 'inst_amount sum_other installment_amount_'.$j]) !!}
                                        </td>
                                    </tr>
                                    @endif
                                    <?php  $j++; ?>
                                    {!! Form::hidden('total_amount', null, ['id' => 'total_amount']) !!}
                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <h4>Sum Of OtherPayments: <span class="displayothersum"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <h4 class="pull-right">Total Price: <span id="total_price"></span></h4>
                    {!! Form::submit("save", ['class' => 'btn btn-success']) !!}&nbsp;
                    <a href="{{ route('payment-schedule.index') }}" class="btn btn-grey">Cancel</a>
                </div>

            {!! Form::close() !!}
        </div>
    </div>

    <style type="text/css">
        input[type=text]{
            width: 135px !important;
        }
    </style>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
            });

            var $inst_form = $('.inst_form'),
                $total_amount = $('#total_amount'),
                $total_price = $('#total_price');

            calculateSum();
            calculateInstSum();
			calculateotherSum();

            $(document).on('blur', '.inst_amount', function() {
                    calculateSum();
                    calculateInstSum();
					calculateotherSum();
              });// run when loading

            var totalPrice = $('.plot_total_price').text();

            $(document).on('blur','.other_amount_percentage', function(){
                // calculatePercentage($(this));
            });

            function calculateSum() {
               var total = 0,val;
               var inst = $('.inst_amount');
                $.each(inst,function(index,val) {
                    total += ($(this).val())?parseInt($(this).val()):0;
                });
                $total_price.html(Math.round(total));
                $total_amount.val(Math.round(total));
            }

            function calculateInstSum() {
                var total = 0,val;
                var inst = $('.sum_inst');
                $.each(inst,function(index,val) {
                    total += ($(this).val())?parseInt($(this).val()):0;
                });
                $('.displayInstSum').html(Math.round(total));
            }
			function calculateotherSum() {
                var total = 0,val;
                var inst = $('.sum_other');
                $.each(inst,function(index,val) {
                    total += ($(this).val())?parseInt($(this).val()):0;
                });
                $('.displayothersum').html(Math.round(total));
            }
            function calculatePercentage(obj) {
                //Calcuate percentage
                var percentage = parseFloat(totalPrice) * parseFloat(obj.val() / 100);
                var rowid = obj.parent().parent().data('row');
                $('.installment_amount_'+rowid).val(percentage);
            }


            var ids=5; //id numbers for add. datepickers

            $(document).on('click', '#add-row' ,function(event) {
                event.preventDefault();
                var sr_no = $('.sr_no').last().text();
                $('.inst_amount:last').removeClass('installment_amount_'+sr_no);
                $('.include_in_total:last').removeClass('include_in_total_'+sr_no);
                sr_no++;

                var newRow = $('.otherPaymentsTableRow').last().clone().appendTo('.otherPaymentsTable');
                $('.sr_no:last').text(sr_no);
                $('.sr_no:last').parent().attr('data-row',sr_no);
                // var prev_row = $(newRow).prev().data('row');
                // var row = $(newRow).data('row',sr_no);
                $('.inst_amount:last').addClass('installment_amount_'+sr_no);
                $('.include_in_total:last').addClass('include_in_total_'+sr_no);
                // $('.date:last').attr('id',rowid).datepicker();
                ids++;
                // calculateSum();
                // calculatePercentage(newRow);
            });

            $(document).on('load',".datepicker", function(){ //bind to all instances of class "date".
                $(this).datepicker();
            });

            $(".include_in_total").change(function(){
                if(this.checked) {
                    $(this).val(1);
                    var row_id = $(this).parent().parent().parent().parent().data('row');
                    var amount = $(".installment_amount_"+row_id).val();
                    console.log($(".installment_amount_"+row_id));
                }else{
                    $(this).val(0);
                }
            });
        });
    </script>
@endsection
