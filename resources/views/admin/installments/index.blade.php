@extends('admin.template')
@section('title','Payment Schedule')

@section('content')

    	@if(!empty($message))
    		<p>{{$message}}</p>
    	@endif
    	 <div class="x_panel">
        <div class="x_title">
            <h2>Installment Plan <small class="pull-right"><a href="{{ route('payment-schedule.create')}}">Add New</a></small></h2>

            <div class="clearfix"></div>
          </div>
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
    		<thead>
    			<tr>
    				<th>Payment Code</th>
                    <th>Plan Description</th>
    				<th>Total Price</th>
                    <th>Booking Percentage</th>
                    <th>Booking Price</th>
    				<th>Plot Nature</th>
                    <th>Payment Frequency</th>
                    <th>Plot Size</th>
                    <th>Installments</th>
                    <th>First Installment Date</th>
                    <th>Plot Dimensions</th>
    				<th>Action</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($payment_schedule as $payment)
                    <tr>
                        <td>{{ $payment->payment_code }}</td>
	    				<td>{{ $payment->payment_desc }}</td>
                        <td>{{ $payment->total_price }}</td>
                        <td>{{ $payment->booking_percentage }}</td>
                        <td>{{ $payment->booking_price }}</td>
                        <td>{{ $payment->plot_nature }}</td>
                        <td>{{ 'After '.$payment->payment_frequency.' Month' }}</td>
                        <td> {{ $payment->plot_size  }} </td>
                        <td> {{ $payment->installments  }} </td>
                        <td> {{ $payment->first_installment_date }} </td>
                        <td>{{ $payment->plot_dimensions }}</td>
	    				<td>
                            <ul class="nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actions <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('payment-schedule.edit', $payment->id )}}">Edit</a></li>
                                        <li><a href="installment/plan/{{ $payment->id }}">Generate</a></li>
                                        <li>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['payment-schedule.destroy', $payment->id]]) !!}
                                                {!! Form::submit("Delete", ['class' => 'btn-link']) !!}
                                            {!! Form::close() !!}
                                        </li>
                                    </ul>
                                </li>
                            </ul>
	    				</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
        </div>
        </div>
        </div>
@endsection
