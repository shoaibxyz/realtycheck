@extends('admin.template')
@section('title','Edit Property')

@section('content')
        
        <h2>Edit Property</h2>    
        <div class="col-sm-6 col-sm-offset-3">  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif    

        {!! Form::model($payment_schedule, ['method' => 'POST', 'route' => ['property.update', $payment_schedule->id], 'class' => 'form-horizontal']) !!}
            
             @include("admin.payment-schedule._form")
        
            <div class="form-group">
                {!! Form::hidden('_method', 'PUT') !!}
                {!! Form::submit("Update Property", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>

@endsection