@extends('admin.layouts.template')
@section('title','Select Module')

@section('accounting-active','active')
@section('acc-dashboard-active','active')
@section('style')

@endsection

@section('content')
      <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Select Module</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Select Module</li>
        </ol>
         @if(!empty($message))
        <p>{{$message}}</p>
    @endif
    </div>
    <div class="x_panel">
         <div class="x_title">
                <h2>Select Module</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmModule" id="frmModule" class="form-horizontal" method="post" action="{{ url('admin/accounting/select-module') }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-4 control-label">Select module <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="module" id="module" class="form-control" required>
                                    <option value="">None</option>
                                    @foreach($modules as $module)
                                        <option value="{{ $module->id }}" {{ (\Illuminate\Support\Facades\Session::get('module') == $module->id) ? "selected=selected" : "" }}>{{ $module->label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <input type="hidden" name="module_id" id="module_id" value="" />
                        <button type="submit" class="btn btn-default btn-orange">Activate</button>
                        &nbsp;
                        <a href="{{ url('admin/accounting/all-modules') }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script type="text/javascript">

    </script>
@endsection