@extends('admin.layouts.template')
@section('title','Tags')
@section('accounting-active','active')
@section('tags-head-active','active')
@section('tags-active','active')

@section('content')
 <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Tags</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Tags</li>
        </ol>
    </div>
    
    <div class="x_panel">
        <div class="x_title">
                <h2>Tags</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ url('/admin/accounting/add-tag')}}" class="btn btn-primary add" style="$rights->show_create" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{ route('getAllTags') }}" name="hfroute" id="hfroute">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblTags">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Tag</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'title', name: 'title', orderable: true},
            { data: 'tag', name: 'tag', orderable: false, className: 'col-center'},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblTags').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[2, "asc" ]],
            ajax: {
                url: $('#hfroute').val()
            },
            "columns": columns
        });
	</script>
@endsection