@extends('admin.layouts.template')
@section('title','Ledger Reconciliation')
@section('accounting-active','active')
@section('acc-reports-head-active','active')
@section('ledger-reconciliation-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

 <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Ledger Reconciliation</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Ledger Reconciliation</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>Ledger Reconciliation</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div id="row accordion">
            <h3>Filter Options</h3>
            <div class="balancesheet form">
                <form action="{{ env('ACC_ADMIN_PATH') . 'ledger_reconcile' }}" id="LSForm" name="LSForm" method="post">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="label">Ledger account <span class="required">*</span></label>
                        <select name="ledgers" id="ledgers" class="form-control" required>
                            <option value="" selected="selected">Please select...</option>
                            @foreach($ledgers as $ledger)
                                <option value="{{ $ledger->id }}" {{ ($req and $req->ledgers == $ledger->id) ? "selected=selected" : "" }}>{{ $ledger->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetStartdate">Start date</label>
                        <input name="start_date" class="form-control datepicker" type="text" id="start_date" value="{{ ($req) ? $req->start_date : "" }}">
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetEnddate">End date</label>
                        <input name="end_date" class="form-control datepicker" type="text" id="end_date" value="{{ ($req) ? $req->end_date : "" }}">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
        <br>
        @if($showStatement)
            <input type="hidden" value="{{ route('getAllLedgerReconciles') }}" name="hfroute" id="hfroute">
        <div class="row" style="text-align: right; padding-right: 15px;">
            <div class="btn-group" role="group" style="display: inline-block">
                {{--<a href="" class="btn btn-default btn-sm">DOWNLOAD .CSV</a>
                <a href="" class="btn btn-default btn-sm">DOWNLOAD .XLS</a>--}}
                @if(App\Models\User::getPermission('can_print_reconcile'))
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onclick="PrintBS()">PRINT</a>
                @endif
            </div>
        </div>
        <br><br><br>
        <div id="subtitle">
            <div class="subtitle text-center">
                {{ $subtitle }}
            </div>
        </div>
        <br><br>
        <div id="summaries">
        <table class="summary stripped table-condensed">
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Bank or cash account</td>
                <td class=" col-sm-4">{{ ($ledger->type == 1) ? "Yes" : "No" }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Notes</td>
                <td class=" col-sm-4">{{ $ledger->notes }}</td>
            </tr>
        </table>
        <br />
        <table class="summary stripped table-condensed">
            <tr>
                <td class="td-fixwidth-summary col-sm-8">{{ $opening_title }}</td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency($op['dc'], $op['amount']) }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8">{{ $closing_title  }}</td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency($cl['dc'], $cl['amount']) }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Debit {{ $reconcile_title }}</td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency('D', $rp['dr_total']) }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Credit {{ $reconcile_title }}</td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency('C', $rp['cr_total']) }}</td>
            </tr>
        </table>
        </div>
        <br><br>
        <form name="reconsileForm" id="reconsileForm" method="post" action="{{ env('ACC_ADMIN_PATH') . 'ledger_reconcile2' }}">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblLS">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Ledger</th>
                        <th>Type</th>
                        <th>Tag</th>
                        <th>Debit Amount (RS)</th>
                        <th>Credit Amount (RS)</th>
                        <th>Reconciliation Date</th>
                    </tr>
                </thead>
            </table>
        </div>
        @if(App\Models\User::getPermission('can_reconcile'))
            <br>
            <div class="row">
                <div class="col-sm-12" style="text-align: right">
                    <button type="submit" class="btn btn-primary">Reconcile</button>
                </div>
            </div>
            <br><br>
        @endif
        @endif
        </form>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        <?php $module = \App\Accounting\Module::find(Session::get('module')); ?>
        /*$(document).ready(function() {*/

            if($('#hfroute').val()) {
                var columns = [
                    {data: 'date', name: 'date', orderable: true},
                    {data: 'number', name: 'number', orderable: true, className: 'col-center'},
                    {data: 'ledger', name: 'ledger', orderable: false, className: 'col-center'},
                    {data: 'etname', name: 'etname', orderable: true, className: 'col-center'},
                    {data: 'tag_id', name: 'tag_id', orderable: true, className: 'col-center'},
                    {data: 'dr_total', name: 'dr_total', orderable: true, className: 'col-center'},
                    {data: 'cr_total', name: 'cr_total', orderable: true, className: 'col-center'},
                    {data: 'reconsile_date', name: 'reconsile_date', orderable: false, className: 'col-center'},
                ];

                var oTable = $('#tblLS').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "stateSave": true,
                    "bDestroy": true,
                    "bFilter": false,
                    "order": [0, "desc"],
                    ajax: {
                        url: $('#hfroute').val(),
                        data: function (d) {
                            d.ledger_id = $('#ledgers').val();
                            d.start_date = $('#start_date').val();
                            d.end_date = $('#end_date').val();
                        }
                    },
                    "columns": columns,
                    initComplete: function () {
                        /* Calculate date range in javascript */
                        //startDate = new Date(<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>);
                        //alert('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>');
                        //endDate = new Date(<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>);
                        //alert(endDate);
                        $('.reconsile_date').datepicker({
                            dateFormat: 'dd-M-yy',
                            minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                            maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                            numberOfMonths: 1
                        });
                    }
                });
            }
            <?php
            //$d = \Carbon\Carbon::parse(\App\Accounting\Module::find(Session::get('module'))->fy_start)->format('d/m/Y');
            //$dt = \Carbon\Carbon::parse(\App\Accounting\Module::find(Session::get('module'))->fy_start)->format('Y, m, d, 0');
            //$std = \Carbon\Carbon::createFromDate(date('Y-m-d',strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start)))->addDays(1);
            ?>
            /* Calculate date range in javascript */
            //startDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>);
            //alert(startDate);
            //endDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_end . ' 23:00:00') * 1000; ?>);
            //alert(endDate);
            /* Setup jQuery datepicker ui */
            $('#start_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "minDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "minDate", startDate);
                    }
                }
            });

            $('#end_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "maxDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "maxDate", endDate);
                    }
                }
            });

            $('.reconsile_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1
            });
        /*});*/

        function PrintBS()
        {
            var WindowObject = window.open('Print Balance Sheet', 'PrintWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=1000,height=600');
            WindowObject.document.writeln('<!DOCTYPE html>');
            WindowObject.document.writeln('<html><head><title></title>');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/custom.min.css" rel="stylesheet">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/print.css" rel="stylesheet">');
            WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')

            WindowObject.document.writeln('<br><br><center style="font-weight: bold;"><strong>');
            WindowObject.document.writeln($('#page-heading').html());
            WindowObject.document.writeln('</strong></center><br><br>');

            WindowObject.document.writeln($('#subtitle').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('#summaries').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('.table-responsive').html());

            WindowObject.document.writeln('</body></html>');

            WindowObject.document.close();

            WindowObject.focus();
            //WindowObject.print();

            //WindowObject.close();
            return true;
        }
    </script>
@endsection