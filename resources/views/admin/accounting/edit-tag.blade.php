@extends('admin.layouts.template')
@section('title','Add New Tag')
@section('accounting-active','active')
@section('tags-head-active','active')
@section('tags-active','active')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/pick-a-color-1.2.3.min.css') }}"/>
@endsection

@section('content')
   <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Tags</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Tag</li>
        </ol>
    </div>
    <div class="x_panel">
         <div class="x_title">
                <h2>Edit Tag</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmModule" id="frmModule" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'edit-tag/' . $tag->id }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Title <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="title" name="title" value="{{ $tag->title }}"
                                       placeholder="Enter title here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Color <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control pick-a-color" id="color" name="color" value="{{ $tag->color }}"
                                       placeholder="000000" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Background</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control pick-a-color" id="background" name="background" value="{{ $tag->background }}"
                                       placeholder="000000">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}" {{ ($value['id'] == $tag->status) ? "selected=selected" : "" }}>{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        &nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'tags' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script type="text/javascript" src="{{ asset('public/js/tinycolor-0.9.15.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/pick-a-color-1.2.3.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pick-a-color").pickAColor();
        });
    </script>
@endsection