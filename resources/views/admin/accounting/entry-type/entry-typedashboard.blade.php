@extends('admin.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')
{!! Breadcrumbs::render('entry-typedashboard') !!}

<!-- top tiles -->
          
          <!-- /top tiles -->
          <div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              
        <div class="metro-nav">
        @if(App\Models\User::getPermission('can_view_entrytypes'))
        <div class="metro-nav-block nav-light-green">
          <a data-original-title="" href="{{ url('admin/accounting/entrytypes') }}">
            <i class="fa fa-arrow-circle-right"></i>
            <div class="info">All Entry Types</div>
            <div class="status">All Entry Types</div>
          </a>
        </div>
        <div class="metro-nav-block nav-light-blue">
          <a data-original-title="" href="{{ url('admin/accounting/deleted-entrytypes') }}">
            <i class="fa fa-trash"></i>
            <div class="info">Deleted Entry Types</div>
            <div class="status">Deleted Entry Types</div>
          </a>
        </div>
        
        

        @endif
      </div>
</div></div>
              </div>
          </div>
          <!-- /dashboard nav -->


          
          
          
@endsection


