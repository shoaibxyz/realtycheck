@extends('admin.template')
@section('title','Search Results')

@section('content')
    <h2>Search Results</h2>
    <div class="x_panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{ route('getSearchResults') }}" name="hfroute" id="hfroute">
        <input type="hidden" name="ledgers" id="ledgers" value="{{ $ledgers }}">
        <input type="hidden" name="tags" id="tags" value="{{ $tags }}">
        <input type="hidden" name="entrytypes" id="entrytypes" value="{{ $entrytypes }}">
        <input type="hidden" name="number" id="number" value="{{ $req->number }}">
        <input type="hidden" name="number2" id="number2" value="{{ $req->number2 }}">
        <input type="hidden" name="amount" id="amount" value="{{ $req->amount }}">
        <input type="hidden" name="amount2" id="amount2" value="{{ $req->amount2 }}">
        <input type="hidden" name="from_date" id="from_date" value="{{ $req->from_date }}">
        <input type="hidden" name="to_date" id="to_date" value="{{ $req->to_date }}">
        <input type="hidden" name="narration" id="narration" value="{{ $req->narration }}">
        <input type="hidden" name="number_restrict" id="number_restrict" value="{{ $req->number_restrict }}">
        <input type="hidden" name="drcr_restrict" id="drcr_restrict" value="{{ $req->drcr_restrict }}">
        <input type="hidden" name="amount_restrict" id="amount_restrict" value="{{ $req->amount_restrict }}">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblEntries">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Ledger</th>
                    <th>Type</th>
                    <th>Tag</th>
                    <th>Debit Amount (RS)</th>
                    <th>Credit Amount (RS)</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
    <script>
        var columns = [
            { data: 'date', name: 'date', orderable: true},
            { data: 'number', name: 'number', orderable: true, className: 'col-center'},
            { data: 'ledger', name: 'ledger', orderable: false, className: 'col-center'},
            { data: 'etname', name: 'etname', orderable: true, className: 'col-center'},
            { data: 'tag_id', name: 'tag_id', orderable: true, className: 'col-center'},
            { data: 'dr_total', name: 'dr_total', orderable: true, className: 'col-center'},
            { data: 'cr_total', name: 'cr_total', orderable: true, className: 'col-center'},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblEntries').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            /*"order": [5, "asc"],*/
            ajax: {
                url: $('#hfroute').val(),
                data: function (d) {
                    d.ledgers = $('#ledgers').val();
                    d.entrytypes = $('#entrytypes').val();
                    d.tags = $('#tags').val();
                    d.number = $('#number').val();
                    d.number2 = $('#number2').val();
                    d.number_restrict = $('#number_restrict').val();
                    d.drcr_restrict = $('#drcr_restrict').val();
                    d.amount = $('#amount').val();
                    d.amount2 = $('#amount2').val();
                    d.amount_restrict = $('#amount_restrict').val();
                    d.from_date = $('#from_date').val();
                    d.to_date = $('#to_date').val();
                    d.narration = $('#narration').val();
                }
            },
            "columns": columns
        });

    </script>
@endsection