@extends('admin.layouts.template')
@section('title','Ledger Statement')
@section('accounting-active','active')
@section('acc-reports-head-active','active')
@section('ledger-statement-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
 <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Ledger Statement</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Ledger Statement</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>Ledger Statement</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div id="row accordion">
            <h3>Filter Options</h3>
            <div class="balancesheet form">
                <form action="{{ env('ACC_ADMIN_PATH') . 'ledger_statement' }}" id="LSForm" name="LSForm" method="post">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="label">Ledger account <span class="required">*</span></label>
                        <select name="ledgers" id="ledgers" class="form-control" required>
                            <option value="" selected="selected">Please select...</option>
                            @foreach($ledgers as $key => $val)
                                <option value="{{ $key }}" {{ ($key < 0) ? "disabled=disabled" : "" }} {{ ($req and $req->ledgers == $key) ? "selected=selected" : "" }}>{{ $val }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetStartdate">Start date</label>
                        <input name="start_date" class="form-control datepicker" type="text" id="start_date" value="{{ ($req) ? $req->start_date : "" }}">
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetEnddate">End date</label>
                        <input name="end_date" class="form-control datepicker" type="text" id="end_date" value="{{ ($req) ? $req->end_date : "" }}">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
        <br>
        @if($showStatement)
            <input type="hidden" value="{{ route('getAllLedgerStatements') }}" name="hfroute" id="hfroute">
        <div class="row" style="text-align: right; padding-right: 15px;">
            <div class="btn-group" role="group" style="display: inline-block">
                {{--<a href="" class="btn btn-default btn-sm">DOWNLOAD .CSV</a>
                <a href="" class="btn btn-default btn-sm">DOWNLOAD .XLS</a>--}}
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onclick="PrintBS()">PRINT</a>
            </div>
        </div>
        <br><br><br>
        <div id="subtitle">
            <div class="subtitle text-center">
                {{ $subtitle }}
            </div>
        </div>
        <br><br>
        <div id="summaries">
        <table class="summary stripped table-condensed">
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Bank or cash account</td>
                <td class=" col-sm-4">{{ ($ledger->type == 1) ? "Yes" : "No" }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8">Notes</td>
                <td class=" col-sm-4">{{ $ledger->notes }}</td>
            </tr>
        </table>
        <br />
        <table class="summary stripped table-condensed">
            <tr>
                <td class="td-fixwidth-summary col-sm-8">{{ $opening_title }}</td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency($op['dc'], $op['amount']) }}</td>
            </tr>
            <tr>
                <td class="td-fixwidth-summary col-sm-8"><?php echo $closing_title; ?></td>
                <td class="col-sm-4">{{ \App\Accounting\Helpers::toCurrency($cl['dc'], $cl['amount']) }}</td>
            </tr>
        </table>
        </div>
        <br><br>
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblLS">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Ledger</th>
                        <th>Type</th>
                        <th>Tag</th>
                        <th>Debit Amount (RS)</th>
                        <th>Credit Amount (RS)</th>
                        <th>Balance (RS)</th>
                        <th class="action_col">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        @endif
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">

        <?php $module = \App\Accounting\Module::find(Session::get('module')); ?>

        $(document).ready(function() {

            if($('#hfroute').val()) {
                var columns = [
                    {data: 'date', name: 'date', orderable: true},
                    {data: 'number', name: 'number', orderable: true, className: 'col-center'},
                    {data: 'ledger', name: 'ledger', orderable: false, className: 'col-center'},
                    {data: 'etname', name: 'etname', orderable: true, className: 'col-center'},
                    {data: 'tag_id', name: 'tag_id', orderable: true, className: 'col-center'},
                    {data: 'dr_total', name: 'dr_total', orderable: true, className: 'col-center'},
                    {data: 'cr_total', name: 'cr_total', orderable: true, className: 'col-center'},
                    {data: 'balance', name: 'balance', orderable: false, className: 'col-center'},
                    {data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center', className: 'action_col'},
                ];

                var oTable = $('#tblLS').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "stateSave": true,
                    "bDestroy": true,
                    "bFilter": false,
                    /*"order": [5, "asc"],*/
                    ajax: {
                        url: $('#hfroute').val(),
                        data: function (d) {
                            d.ledger_id = $('#ledgers').val();
                            d.start_date = $('#start_date').val();
                            d.end_date = $('#end_date').val();
                        }
                    },
                    "columns": columns
                });
            }

            /* Calculate date range in javascript */
            //startDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //endDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //alert(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>);
            /* Setup jQuery datepicker ui */
            $('#start_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "minDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "minDate", startDate);
                    }
                }
            });

            $('#end_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "maxDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "maxDate", endDate);
                    }
                }
            });
        });

        function PrintBS()
        {
            var WindowObject = window.open('Print Balance Sheet', 'PrintWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=1000,height=600');
            WindowObject.document.writeln('<!DOCTYPE html>');
            WindowObject.document.writeln('<html><head><title></title>');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/custom.min.css" rel="stylesheet">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/print.css" rel="stylesheet">');
            WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')

            WindowObject.document.writeln('<br><br><center style="font-weight: bold;"><strong>');
            WindowObject.document.writeln($('#page-heading').html());
            WindowObject.document.writeln('</strong></center><br><br>');

            WindowObject.document.writeln($('#subtitle').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('#summaries').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('.table-responsive').html());

            WindowObject.document.writeln('</body></html>');

            WindowObject.document.close();

            WindowObject.focus();
            //WindowObject.print();

            //WindowObject.close();
            return true;
        }
    </script>
@endsection