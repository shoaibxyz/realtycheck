@extends('admin.layouts.template')
@section('title','Chart Of Accounts')
@section('accounting-active','active')
@section('accounting-head-active','active')
@section('charts-of-accounts-active','active')
@section('content')


<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Chart Of Accounts</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Chart Of Accounts</li>
        </ol>
    </div>
    
    
    
    <div class="x_panel">
         <div class="x_title">
                <h2>Chart Of Accounts</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ url('/admin/accounting/add-group')}}" class="btn btn-primary add" style="{{$group_rights->show_create}}" data-toggle="tooltip" title="Add Group"><i class="fa fa-plus"></i>&nbsp;Group</a>&nbsp;&nbsp;
            <a href="{{ url('/admin/accounting/add-ledger')}}" class="btn btn-primary add" style="{{$ledger_rights->show_create}}" data-toggle="tooltip" title="Add Ledger"><i class="fa fa-plus"></i>&nbsp;Ledger</a>
                </div>
                <div class="clearfix"></div>
            </div>
            
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-danger" style="{{ (Session('status') == 'error') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Error</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{ route('getAllAccounts') }}" name="hfroute" id="getAllAccounts">
        <?php
        if (\App\Accounting\Helpers::calculate($opdiff['opdiff_balance'], 0, '!=')) {
            echo '<div><div role="alert" class="alert alert-danger">There is a difference in opening balance of ' . \App\Accounting\Helpers::toCurrency($opdiff['opdiff_balance_dc'], $opdiff['opdiff_balance']) . '</div></div>';
        }
        ?>
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table stripped" id="tblAccounts">
                <thead>
                    <tr>
                        <th>Account Name</th>
                        <th>Type</th>
                        <th>O/P Balance (RS)</th>
                        <th>C/L Balance (RS)</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                print_account_chart($accountlist, -1);
                function print_account_chart($account, $c = 0)
                {
                    $counter = $c;
                    //dd($account);
                    /* Print groups */
                    if ($account->id != 0) {
                        if ($account->id <= 4) {
                            echo '<tr class="tr-group tr-root-group">';
                        } else {
                            echo '<tr class="tr-group">';
                        }
                        echo '<td class="td-group">';
                        echo \App\Accounting\Helpers::print_space($counter);
                        echo \App\Accounting\Helpers::toCodeWithName($account->code, $account->name);
                        echo '</td>';

                        echo '<td>Group</td>';

                        echo '<td>';
                        echo \App\Accounting\Helpers::toCurrency($account->op_total_dc, $account->op_total);
                        echo '</td>';

                        echo '<td>';
                        echo \App\Accounting\Helpers::toCurrency($account->cl_total_dc, $account->cl_total);
                        echo '</td>';

                        /* If group id less than 4 dont show edit and delete links */
                        if ($account->id <= 4) {
                            echo '<td class=""></td><td class="td-actions"></td>';
                        } else {
                            echo '<td style="text-align: center">';
                            $status = \App\Accounting\Helpers::getStatus($account->status);
                            $status2 = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                            echo $status2;
                            echo '</td>';
                            echo '<td class="td-actions">';
                                echo '<a href="edit-group/'. $account->id .'" class="btn btn-info btn-xs edit" style="{{$group_rights->show_edit}}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                                echo '<span class="link-pad"></span>';
                            
                                echo '<a href="delete-group/'. $account->id .'" class="btn btn-danger btn-xs DeleteBtn delete" style="{{$group_rights->show_delete}}" data-toggle="tooltip" title="Delete" onclick="return confirm(\'Are you sure you want to delete the group ?\')"><i class="fa fa-times"></i></a>';
                            echo '</td>';
                        }
                        echo '</tr>';
                    }

                    /* Print child ledgers */
                    if (count($account->children_ledgers) > 0) {
                        $counter++;
                        foreach ($account->children_ledgers as $id => $data) {
                            echo '<tr class="tr-ledger">';
                            echo '<td class="td-ledger">';
                            echo \App\Accounting\Helpers::print_space($counter);
                            echo '<a href="ledger_statement/'. $data['id'] .'">'. \App\Accounting\Helpers::toCodeWithName($data['code'], $data['name']) .'</a>';
                            echo '</td>';
                            echo '<td>Ledger</td>';

                            echo '<td>';
                            echo \App\Accounting\Helpers::toCurrency($data['op_total_dc'], $data['op_total']);
                            echo '</td>';

                            echo '<td>';
                            echo \App\Accounting\Helpers::toCurrency($data['cl_total_dc'], $data['cl_total']);
                            echo '</td>';

                            echo '<td style="text-align: center">';
                            $status = \App\Accounting\Helpers::getStatus($account->status);
                            $status2 = '<span class="label label-sm label-'. $status[1] .'">'. $status[0] .'</span>';
                            echo $status2;
                            echo '</td>';

                            echo '<td class="td-actions">';
                                echo '<a href="edit-ledger/'. $data['id'] .'" class="btn btn-info btn-xs edit" style="{{$ledger_rights->show_edit}}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                                echo '<span class="link-pad"></span>';
                                echo '<a href="delete-ledger/'. $data['id'] .'" class="btn btn-danger btn-xs DeleteBtn delete" style="{{$ledger_rights->show_delete}}" data-toggle="tooltip" title="Delete" onclick="return confirm(\'Are you sure you want to delete the ledger ?\')"><i class="fa fa-times"></i></a>';
                            echo '</td>';

                            echo '</tr>';
                        }
                        $counter--;
                    }

                    /* Print child groups recursively */
                    foreach ($account->children_groups as $id => $data) {
                        $counter++;
                        print_account_chart($data, $counter);
                        $counter--;
                    }
                }
                ?>
                </tbody>
        </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        /*var columns = [
            { data: 'account', name: 'account', orderable: true },
            { data: 'type', name: 'type', orderable: true},
            { data: 'opbalance', name: 'opbalance', orderable: true},
            { data: 'clbalance', name: 'clbalance', orderable: true},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblModules').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[5, "asc" ]],
            ajax: {
                url: $('#getAllAccounts').val()
            },
            "columns": columns
        });*/
	</script>
@endsection