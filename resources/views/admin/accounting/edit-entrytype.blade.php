@extends('admin.layouts.template')
@section('title','Edit Entry Type')
@section('accounting-active','active')
@section('entry-types-head-active','active')
@section('entry-types-active','active')

@section('style')

@endsection

@section('content')

   <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Entry Types</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Entry Type</li>
        </ol>
    </div>
    
    <div class="x_panel">
         <div class="x_title">
                <h2>Edit Entry Type</h2>
                
              
                <div class="clearfix"></div>
            </div>
            
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmModule" id="frmModule" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'edit-entrytype/' . $entrytype->id }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Label <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="label" name="label" value="{{ $entrytype->label }}"
                                       placeholder="Enter label here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $entrytype->name }}"
                                       placeholder="Enter name here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Prefix</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="prefix" name="prefix" value="{{ $entrytype->prefix }}"
                                       placeholder="Enter prefix here">
                                <div class="help-block">Note : Prefix to add before entry numbers.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Suffix</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="suffix" name="suffix" value="{{ $entrytype->suffix }}"
                                       placeholder="Enter suffix here">
                                <div class="help-block">Note : Suffix to add after entry numbers.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Zero Padding</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="zero_padding" name="zero_padding" value="{{ $entrytype->zero_padding }}"
                                       placeholder="Enter zero padding here">
                                <div class="help-block">Note : Number of zeros to pad before entry numbers.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="db_name" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="description" name="description" placeholder="Enter description here">{{ $entrytype->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Numbering <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="numbering" id="numbering" class="form-control" required>
                                    <option value="1" {{ $entrytype->numbering == 1 ? "selected=selected" : "" }}>Auto</option>
                                    <option value="2" {{ $entrytype->numbering == 2 ? "selected=selected" : "" }}>Manual (required)</option>
                                    <option value="3" {{ $entrytype->numbering == 3 ? "selected=selected" : "" }}>Manual (optional)</option>
                                </select>
                                <div class="help-block">Note : How the entry numbering is handled.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Restrictions <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="restriction" class="form-control" id="restriction" required="required">
                                    <option value="1" {{ $entrytype->restriction_bankcash == 1 ? "selected=selected" : "" }}>Unrestricted</option>
                                    <option value="2" {{ $entrytype->restriction_bankcash == 2 ? "selected=selected" : "" }}>Atleast one Bank or Cash account must be present on Debit side</option>
                                    <option value="3" {{ $entrytype->restriction_bankcash == 3 ? "selected=selected" : "" }}>Atleast one Bank or Cash account must be present on Credit side</option>
                                    <option value="4" {{ $entrytype->restriction_bankcash == 4 ? "selected=selected" : "" }}>Only Bank or Cash account can be present on both Debit and Credit side</option>
                                    <option value="5" {{ $entrytype->restriction_bankcash == 5 ? "selected=selected" : "" }}>Only NON Bank or Cash account can be present on both Debit and Credit side</option>
                                </select>
                                <div class="help-block">Note : How the entry numbering is handled.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}" {{ ($value['id'] == $entrytype->status) ? "selected=selected" : "" }}>{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        &nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'entrytypes' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script language="JavaScript">
        
    </script>
@endsection