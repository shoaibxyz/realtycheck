@extends('admin.layouts.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')
{!! Breadcrumbs::render('moduledashboard') !!}

<!-- top tiles -->
          
          <!-- /top tiles -->
          <div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              
        <div class="metro-nav">
        @if(App\Models\User::getPermission('can_view_modules'))
        <div class="metro-nav-block nav-light-green">
          <a data-original-title="" href="{{ url('admin/accounting/all-modules') }}">
            <i class="fa fa-arrow-circle-right"></i>
            <div class="info">All Modules</div>
            <div class="status">All Modules</div>
          </a>
        </div>
        <div class="metro-nav-block nav-light-blue">
          <a data-original-title="" href="{{ url('admin/accounting/deleted-modules') }}">
            <i class="fa fa-trash"></i>
            <div class="info">Deleted Modules</div>
            <div class="status">Deleted Modules</div>
          </a>
        </div>
        @endif
        <div class="metro-nav-block nav-light-brown">
          <a data-original-title="" href="{{ url('admin/accounting/select-module') }}">
            <i class="fa fa-building-o"></i>
            <div class="info">Select Module</div>
            <div class="status">Select Module</div>
          </a>
        </div>

        
    </div>  
</div></div>
              </div>
          </div>
          <!-- /dashboard nav -->


          
          
          
@endsection


