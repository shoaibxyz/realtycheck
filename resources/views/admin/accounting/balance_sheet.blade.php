@extends('admin.layouts.template')
@section('title','Balance Sheet')
@section('accounting-active','active')
@section('acc-reports-head-active','active')
@section('balance-sheet-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
    <?php
    function account_st_short($account, $c = 0, $THIS, $dc_type)
    {
        $counter = $c;
        if ($account->id > 4)
        {
            if ($dc_type == 'D' && $account->cl_total_dc == 'C' && \App\Accounting\Helpers::calculate($account->cl_total, 0, '!=')) {
                echo '<tr class="tr-group dc-error">';
            } else if ($dc_type == 'C' && $account->cl_total_dc == 'D' && \App\Accounting\Helpers::calculate($account->cl_total, 0, '!=')) {
                echo '<tr class="tr-group dc-error">';
            } else {
                echo '<tr class="tr-group">';
            }

            echo '<td class="td-group">';
            echo print_space($counter);
            echo \App\Accounting\Helpers::toCodeWithName($account->code, $account->name);
            echo '</td>';

            echo '<td class="text-right">';
            echo \App\Accounting\Helpers::toCurrency($account->cl_total_dc, $account->cl_total);
            echo print_space($counter);
            echo '</td>';

            echo '</tr>';
        }
        foreach ($account->children_groups as $id => $data)
        {
            $counter++;
            account_st_short($data, $counter, $THIS, $dc_type);
            $counter--;
        }
        if (count($account->children_ledgers) > 0)
        {
            $counter++;
            foreach ($account->children_ledgers as $id => $data)
            {
                if ($dc_type == 'D' && $data['cl_total_dc'] == 'C' && \App\Accounting\Helpers::calculate($data['cl_total'], 0, '!=')) {
                    echo '<tr class="tr-ledger dc-error">';
                } else if ($dc_type == 'C' && $data['cl_total_dc'] == 'D' && \App\Accounting\Helpers::calculate($data['cl_total'], 0, '!=')) {
                    echo '<tr class="tr-ledger dc-error">';
                } else {
                    echo '<tr class="tr-ledger">';
                }

                echo '<td class="td-ledger">';
                echo print_space($counter);
                echo '<a href="ledgerstatement/'. $data['id'] .'">'. \App\Accounting\Helpers::toCodeWithName($data['code'], $data['name']) .'</a>';
                echo '</td>';

                echo '<td class="text-right">';
                echo \App\Accounting\Helpers::toCurrency($data['cl_total_dc'], $data['cl_total']);
                echo print_space($counter);
                echo '</td>';

                echo '</tr>';
            }
            $counter--;
        }
    }

    function print_space($count)
    {
        $html = '';
        for ($i = 1; $i <= $count; $i++) {
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        return $html;
    }

    ?>
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Balance Sheet</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Balance Sheet</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>Balance Sheet</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <?php
        /* Show difference in opening balance */
        if ($bsheet['is_opdiff']) {
            echo '<div id="alert1"><div role="alert" class="alert alert-danger">' .
               'There is a difference in opening balance of ' .
                \App\Accounting\Helpers::toCurrency($bsheet['opdiff']['opdiff_balance_dc'], $bsheet['opdiff']['opdiff_balance']) .
                '</div></div>';
        }

        /* Show difference in liabilities and assets total */
        if (\App\Accounting\Helpers::calculate($bsheet['final_liabilities_total'], $bsheet['final_assets_total'], '!=')) {
            $final_total_diff = \App\Accounting\Helpers::calculate($bsheet['final_liabilities_total'], $bsheet['final_assets_total'], '-');
            echo '<div id="alert2"><div role="alert" class="alert alert-danger">' .
                'There is a difference in Total Liabilities and Total Assets of ' .
                \App\Accounting\Helpers::toCurrency('X', $final_total_diff) .
                '</div></div>';
        }
        ?>
        <div id="row accordion">
            <h3>Filter Options</h3>
            <div class="balancesheet form">
                <form action="{{ env('ACC_ADMIN_PATH') . 'balance_sheet' }}" id="BalancesheetBalancesheetForm" method="post">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div><input type="checkbox" name="opening" id="opening" {{ ($req and $req->opening == 'on') ? "checked=checked" : "" }}> Show Opening Balance Sheet</div>
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetStartdate">Start date</label>
                        <input name="start_date" class="form-control datepicker" type="text" id="start_date" value="{{ ($req) ? $req->start_date : "" }}">
                        <span class="help-block">Note : Leave start date as empty if you want statement from the start of the financial year.</span>
                    </div>
                    <div class="form-group">
                        <label for="BalancesheetEnddate">End date</label>
                        <input name="end_date" class="form-control datepicker" type="text" id="end_date" value="{{ ($req) ? $req->end_date : "" }}">
                        <span class="help-block">Note : Leave end date as empty if you want statement till the end of the financial year.</span>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row" style="text-align: right; padding-right: 15px;">
            <div class="btn-group" role="group" style="display: inline-block">
                {{--<a href="" class="btn btn-default btn-sm">DOWNLOAD .CSV</a>
                <a href="" class="btn btn-default btn-sm">DOWNLOAD .XLS</a>--}}
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onclick="PrintBS()">PRINT</a>
            </div>
        </div>
        <br><br><br>
        <div id="subtitle">
        <div class="subtitle text-center">
            <?php echo $subtitle ?>
        </div>
        </div>
        <br><br>
        <div class="table-responsive">
            <table class="table table-hover" id="tblBS">
                <!-- Liabilities and Assets -->
                <tr>

                    <!-- Assets -->
                    <td class="table-top width-50" style="border: 0 !important; width: 50%">
                        <table class="stripped" style="width: 100%;">
                            <tr>
                                <th><?php echo 'Assets (Dr)'; ?></th>
                                <th class="text-right" style="text-align: right"><?php echo 'Amount'; ?> (RS)</th>
                            </tr>
                            <?php echo account_st_short($bsheet['assets'], $c = -1, $this, 'D'); ?>
                        </table>
                    </td>

                    <!-- Liabilities -->
                    <td class="table-top width-50" style="border: 0 !important; width: 50%">
                        <table class="stripped" style="width: 100%;">
                            <tr>
                                <th><?php echo 'Liabilities and Owners Equity (Cr)'; ?></th>
                                <th class="text-right" style="text-align: right"><?php echo 'Amount'; ?> (RS)</th>
                            </tr>
                            <?php echo account_st_short($bsheet['liabilities'], $c = -1, $this, 'C'); ?>
                        </table>
                    </td>

                </tr>

                <tr>

                    <!-- Assets Calculations -->
                    <td class="table-top width-50" style="border: 0 !important; width: 50%">
                        <div class="report-tb-pad"></div>
                        <table class="stripped" style="width: 100%;">
                            <?php
                            /* Assets Total */
                            if (\App\Accounting\Helpers::calculate($bsheet['assets_total'], 0, '>=')) {
                                echo '<tr class="bold-text">';
                                echo '<td>' . 'Total Assets' . '</td>';
                                echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('D', $bsheet['assets_total']) . '</td>';
                                echo '</tr>';
                            } else {
                                echo '<tr class="dc-error bold-text">';
                                echo '<td>' . 'Total Assets' . '</td>';
                                echo '<td class="text-right show-tooltip" data-toggle="tooltip" data-original-title="Expecting positive Dr Balance">' . \App\Accounting\Helpers::toCurrency('D', $bsheet['assets_total']) . '</td>';
                                echo '</tr>';
                            }
                            ?>
                            <tr class="bold-text">
                                <?php
                                /* Net loss */
                                if (\App\Accounting\Helpers::calculate($bsheet['pandl'], 0, '>=')) {
                                    echo '<td>&nbsp</td>';
                                    echo '<td>&nbsp</td>';
                                } else {
                                    echo '<td>' . 'Profit & Loss Account (Net Loss)' . '</td>';
                                    $positive_pandl = \App\Accounting\Helpers::calculate($bsheet['pandl'], 0, 'n');
                                    echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('D', $positive_pandl) . '</td>';
                                }
                                ?>
                            </tr>
                            <?php
                            /* Difference in opening balance */
                            if ($bsheet['is_opdiff']) {
                                echo '<tr class="bold-text error-text">';
                                /* If diff in opening balance is Dr */
                                if ($bsheet['opdiff']['opdiff_balance_dc'] == 'D') {
                                    echo '<td>' . 'Diff in O/P Balance' . '</td>';
                                    echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('D', $bsheet['opdiff']['opdiff_balance']) . '</td>';
                                } else {
                                    echo '<td>&nbsp</td>';
                                    echo '<td>&nbsp</td>';
                                }
                                echo '</tr>';
                            }
                            ?>

                            <?php
                            /* Total */
                            if (\App\Accounting\Helpers::calculate($bsheet['final_liabilities_total'],
                                $bsheet['final_assets_total'], '==')) {
                                echo '<tr class="bold-text bg-filled">';
                            } else {
                                echo '<tr class="bold-text error-text bg-filled">';
                            }
                            echo '<td>' . 'Total' . '</td>';
                            echo '<td class="text-right">' .
                                \App\Accounting\Helpers::toCurrency('D', $bsheet['final_assets_total']) .
                                '</td>';
                            echo '</tr>';
                            ?>
                        </table>
                    </td>

                    <!-- Liabilities Calculations -->
                    <td class="table-top width-50" style="border: 0 !important; width: 50%">
                        <div class="report-tb-pad"></div>
                        <table class="stripped" style="width: 100%;">
                            <?php
                            /* Liabilities Total */
                            if (\App\Accounting\Helpers::calculate($bsheet['liabilities_total'], 0, '>=')) {
                                echo '<tr class="bold-text">';
                                echo '<td>' . 'Total Liability and Owners Equity' . '</td>';
                                echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('C', $bsheet['liabilities_total']) . '</td>';
                                echo '</tr>';
                            } else {
                                echo '<tr class="dc-error bold-text">';
                                echo '<td>' . 'Total Liability and Owners Equity' . '</td>';
                                echo '<td class="text-right show-tooltip" data-toggle="tooltip" data-original-title="Expecting positive Cr balance">' . \App\Accounting\Helpers::toCurrency('C', $bsheet['liabilities_total']) . '</td>';
                                echo '</tr>';
                            }
                            ?>
                            <tr class="bold-text">
                                <?php
                                /* Net profit */
                                if (\App\Accounting\Helpers::calculate($bsheet['pandl'], 0, '>=')) {
                                    echo '<td>' . 'Profit & Loss Account (Net Profit)' . '</td>';
                                    echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('C', $bsheet['pandl']) . '</td>';
                                } else {
                                    echo '<td>&nbsp</td>';
                                    echo '<td>&nbsp</td>';
                                }
                                ?>
                            </tr>
                            <?php
                            /* Difference in opening balance */
                            if ($bsheet['is_opdiff']) {
                                echo '<tr class="bold-text error-text">';
                                /* If diff in opening balance is Cr */
                                if ($bsheet['opdiff']['opdiff_balance_dc'] == 'C') {
                                    echo '<td>' . 'Diff in O/P Balance' . '</td>';
                                    echo '<td class="text-right">' . \App\Accounting\Helpers::toCurrency('C', $bsheet['opdiff']['opdiff_balance']) . '</td>';
                                } else {
                                    echo '<td>&nbsp</td>';
                                    echo '<td>&nbsp</td>';
                                }
                                echo '</tr>';
                            }
                            ?>

                            <?php
                            /* Total */
                            if (\App\Accounting\Helpers::calculate($bsheet['final_liabilities_total'],
                                $bsheet['final_assets_total'], '==')) {
                                echo '<tr class="bold-text bg-filled">';
                            } else {
                                echo '<tr class="bold-text error-text bg-filled">';
                            }
                            echo '<td>' . 'Total' . '</td>';
                            echo '<td class="text-right">' .
                                \App\Accounting\Helpers::toCurrency('C', $bsheet['final_liabilities_total']) .
                                '</td>';
                            echo '</tr>';
                            ?>
                        </table>
                    </td>

                </tr>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">

        <?php $module = \App\Accounting\Module::find(Session::get('module')); ?>

        $(document).ready(function() {

            var path = '{{ env('ACC_ADMIN_PATH') }}';

            $('#opening').change(function() {
                if ($(this).prop('checked')) {
                    $('#start_date').prop('disabled', true);
                    $('#end_date').prop('disabled', true);
                } else {
                    $('#start_date').prop('disabled', false);
                    $('#end_date').prop('disabled', false);
                }
            });
            $('#opening').trigger('change');

            /* Calculate date range in javascript */
            //startDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //endDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //alert(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>);
            /* Setup jQuery datepicker ui */
            $('#start_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "minDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "minDate", startDate);
                    }
                }
            });

            $('#end_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "maxDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "maxDate", endDate);
                    }
                }
            });
        });

        function PrintBS()
        {
            var WindowObject = window.open('Print Balance Sheet', 'PrintWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=1000,height=600');
            WindowObject.document.writeln('<!DOCTYPE html>');
            WindowObject.document.writeln('<html><head><title></title>');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/custom.min.css" rel="stylesheet">');
            WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')

            WindowObject.document.writeln('<br><br><center style="font-weight: bold;"><strong>');
            WindowObject.document.writeln($('#page-heading').html());
            WindowObject.document.writeln('</strong></center><br><br>');

            WindowObject.document.writeln($('#alert1').html());

            WindowObject.document.writeln($('#alert2').html());

            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('#subtitle').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('.table-responsive').html());

            /*WindowObject.document.writeln('<br><br><input class="hide-print print-button" type="button" onclick="window.print()" value="Print">');*/

            WindowObject.document.writeln('</body></html>');

            WindowObject.document.close();

            WindowObject.focus();
            //WindowObject.print();

            //WindowObject.close();
            return true;
        }
    </script>
@endsection