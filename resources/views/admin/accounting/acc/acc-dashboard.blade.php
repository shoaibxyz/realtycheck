@extends('admin.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')

{!! Breadcrumbs::render('accdashboard') !!}
<!-- top tiles -->
          
          <!-- /top tiles -->
          <div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              
        <div class="metro-nav">
        @if(App\Models\User::getPermission('can_view_accounts'))
        <div class="metro-nav-block nav-light-green">
          <a data-original-title="" href="{{ url('admin/accounting/accounts') }}">
            <i class="fa fa-arrow-circle-right"></i>
            <div class="info">Chart of Accounts</div>
            <div class="status">Chart of Accounts</div>
          </a>
        </div>
        <div class="metro-nav-block nav-light-blue">
          <a data-original-title="" href="{{ url('admin/accounting/deleted-groups') }}">
            <i class="fa fa-trash"></i>
            <div class="info">Deleted Groups</div>
            <div class="status">Deleted Groups</div>
          </a>
        </div>
        
        <div class="metro-nav-block nav-light-brown">
          <a data-original-title="" href="{{ url('admin/accounting/deleted-ledgers') }}">
            <i class="fa fa-trash"></i>
            <div class="info">Deleted Ledgers</div>
            <div class="status">Deleted Ledgers</div>
          </a>
        </div>

        @endif
      </div>
</div></div>
              </div>
          </div>
          <!-- /dashboard nav -->


          
          
          
@endsection


