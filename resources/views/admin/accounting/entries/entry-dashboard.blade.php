@extends('admin.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')

{!! Breadcrumbs::render('entrydashboard') !!}
<!-- top tiles -->
          
          <!-- /top tiles -->
          <div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              
        <div class="metro-nav">
        @if(App\Models\User::getPermission('can_view_entries'))
        <div class="metro-nav-block nav-light-green">
          <a data-original-title="" href="{{ url('admin/accounting/entries') }}">
            <i class="fa fa-arrow-circle-right"></i>
            <div class="info">All Entries</div>
            <div class="status">All Entries</div>
          </a>
        </div>
        <div class="metro-nav-block nav-light-blue">
          <a data-original-title="" href="{{ url('admin/accounting/deleted-entries') }}">
            <i class="fa fa-trash"></i>
            <div class="info">Deleted Entries</div>
            <div class="status">Deleted Entries</div>
          </a>
        </div>
        
        

        @endif
      </div>
</div></div>
              </div>
          </div>
          <!-- /dashboard nav -->


          
          
          
@endsection


