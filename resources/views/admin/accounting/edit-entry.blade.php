@extends('admin.layouts.template')
@section('title','Edit Entry')
@section('accounting-active','active')
@section('entries-head-active','active')
@section('entries-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

   <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">List of Entries</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Entry</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>Edit Entry</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-12" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmEntry" id="frmEntry" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'edit-entry/' . $entry->id }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        {{--<div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Number <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="number" name="number" value="{{ $entry->number }}"
                                       placeholder="Enter number here" required>
                            </div>
                        </div>--}}
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Date <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control datepicker" id="date" name="date" value="{{ \Carbon\Carbon::parse($entry->date)->format('d-M-Y') }}"
                                       placeholder="Entry date" required>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover jambo_table" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th>Dr/Cr</th>
                                        <th>Ledger</th>
                                        <th>Dr Amount (RS)</th>
                                        <th>Cr Amount (RS)</th>
                                        <th>Actions</th>
                                        <th>Cur Balance (RS)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($curEntryitems as $row => $entryitem)
                                    <tr>
                                        <td><div class="form-group-entryitem">
                                            <select name="entryitems[{{ $row }}][dc]" class="form-control dc-dropdown" id="dc_{{ $row }}">
                                                <option value="D" {{ (!empty($entryitem['dc']) and $entryitem['dc'] == 'D') ? 'selected="selected"' : '' }}>Dr</option>
                                                <option value="C" {{ (!empty($entryitem['dc']) and $entryitem['dc'] == 'C') ? 'selected="selected"' : '' }}>Cr</option>
                                            </select></div>
                                        </td>
                                        <td><div class="form-group-entryitem">
                                            <select name="entryitems[{{ $row }}][ledger_id]" id="ledger_{{ $row  }}" class="form-control ledger-dropdown">
                                                @foreach($ledger_options as $key => $val)
                                                    <option value="{{ $key }}" {{ ($key < 0) ? "disabled=disabled" : "" }} {{ (!empty($entryitem['ledger_id']) and $entryitem['ledger_id'] == $key) ? 'selected="selected"' : '' }}>{{ $val }}</option>
                                                @endforeach
                                            </select></div>
                                        </td>
                                        <td><div class="form-group-entryitem">
                                            <input name="entryitems[{{ $row }}][dr_amount]" class="form-control dr-item" type="text" id="dr_{{ $row }}" value="{{ (!empty($entryitem['dr_amount'])) ? $entryitem['dr_amount'] : "" }}">
                                            </div>
                                        </td>
                                        <td><div class="form-group-entryitem">
                                            <input name="entryitems[{{ $row }}][cr_amount]" class="form-control cr-item" type="text" id="cr_{{ $row }}" value="{{ (!empty($entryitem['cr_amount'])) ? $entryitem['cr_amount'] : "" }}">
                                            </div>
                                        </td>
                                        <td style="text-align: center;">
                                            <a class="addrow" data-id="{{ $row }}"><i class="glyphicon glyphicon-plus"></i> Add</a>
                                            <span class="link-pad"></span>
                                            <a class="deleterow"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                        </td>
                                        <td class="ledger-balance"><div></div></td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td><strong>Total</strong></td>
                                        <td></td>
                                        <td id="dr-total" style="font-weight: bold"></td>
                                        <td id="cr-total" style="font-weight: bold"></td>
                                        <td><span class="recalculate"><i class="glyphicon glyphicon-refresh"></i></span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Difference</strong></td>
                                        <td></td>
                                        <td id="dr-diff"></td>
                                        <td id="cr-diff"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Narration</label>
                            <div class="col-sm-8">
                                <textarea name="narration" id="narration" class="form-control">{{ $entry->narration }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Tag <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="tag" id="tag" class="form-control ddsearch">
                                    <option value="">Select tag</option>
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}" {{ $entry->tag_id == $tag->id ? "selected=selected" : "" }}>{{ $tag->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}" {{ $entry->status == $value['id'] ? "selected=selected" : "" }}>{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        <input type="hidden" name="entrytype_id" id="entrytype_id" value="{{ $entrytype->id }}">&nbsp;
                        <input type="hidden" name="restriction_bankcash" id="restriction_bankcash" value="{{ $entrytype->restriction_bankcash }}">&nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'entries' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">

        <?php $module = \App\Accounting\Module::find(Session::get('module')); ?>

        $(document).ready(function() {

            var path = '{{ env('ACC_ADMIN_PATH') }}';
            /* Calculate date range in javascript */
            //startDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //endDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //alert(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>);
            /* Setup jQuery datepicker ui */
            $('.datepicker').datepicker({
                dateFormat: 'd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
            });

            /* javascript floating point operations */
            var jsFloatOps = function(param1, param2, op) {
                param1 = param1 * 100;
                param2 = param2 * 100;
                param1 = param1.toFixed(0);
                param2 = param2.toFixed(0);
                param1 = Math.floor(param1);
                param2 = Math.floor(param2);
                var result = 0;
                if (op == '+') {
                    result = param1 + param2;
                        result = result/100;
                        return result;
                }
                if (op == '-') {
                    result = param1 - param2;
                        result = result/100;
                        return result;
                }
                if (op == '!=') {
                    if (param1 != param2)
                        return true;
                    else
                        return false;
                }
                if (op == '==') {
                    if (param1 == param2)
                        return true;
                    else
                        return false;
                }
                if (op == '>') {
                    if (param1 > param2)
                        return true;
                    else
                        return false;
                }
                if (op == '<') {
                    if (param1 < param2)
                        return true;
                    else
                        return false;
                }
            }

            /* Calculating Dr and Cr total */
            $(document).on('change', '.dr-item', function() {
                var drTotal = 0;
                $("table tr .dr-item").each(function() {
                    var curDr = $(this).prop('value');
                    curDr = parseFloat(curDr);
                    if (isNaN(curDr))
                        curDr = 0;
                    drTotal = jsFloatOps(drTotal, curDr, '+');
                });
                $("table tr #dr-total").text(drTotal);
                var crTotal = 0;
                $("table tr .cr-item").each(function() {
                    var curCr = $(this).prop('value');
                    curCr = parseFloat(curCr);
                    if (isNaN(curCr))
                        curCr = 0;
                    crTotal = jsFloatOps(crTotal, curCr, '+');
                });
                $("table tr #cr-total").text(crTotal);

                if (jsFloatOps(drTotal, crTotal, '==')) {
                    $("table tr #dr-total").css("background-color", "#FFFF99");
                    $("table tr #cr-total").css("background-color", "#FFFF99");
                    $("table tr #dr-diff").text("-");
                    $("table tr #cr-diff").text("");
                } else {
                    $("table tr #dr-total").css("background-color", "#FFE9E8");
                    $("table tr #cr-total").css("background-color", "#FFE9E8");
                    if (jsFloatOps(drTotal, crTotal, '>')) {
                        $("table tr #dr-diff").text("");
                        $("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
                    } else {
                        $("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
                        $("table tr #cr-diff").text("");
                    }
                }
            });

            $(document).on('change', '.cr-item', function() {
                var drTotal = 0;
                $("table tr .dr-item").each(function() {
                    var curDr = $(this).prop('value')
                    curDr = parseFloat(curDr);
                    if (isNaN(curDr))
                        curDr = 0;
                    drTotal = jsFloatOps(drTotal, curDr, '+');
                });
                $("table tr #dr-total").text(drTotal);
                var crTotal = 0;
                $("table tr .cr-item").each(function() {
                    var curCr = $(this).prop('value')
                    curCr = parseFloat(curCr);
                    if (isNaN(curCr))
                        curCr = 0;
                    crTotal = jsFloatOps(crTotal, curCr, '+');
                });
                $("table tr #cr-total").text(crTotal);

                if (jsFloatOps(drTotal, crTotal, '==')) {
                    $("table tr #dr-total").css("background-color", "#FFFF99");
                    $("table tr #cr-total").css("background-color", "#FFFF99");
                    $("table tr #dr-diff").text("-");
                    $("table tr #cr-diff").text("");
                } else {
                    $("table tr #dr-total").css("background-color", "#FFE9E8");
                    $("table tr #cr-total").css("background-color", "#FFE9E8");
                    if (jsFloatOps(drTotal, crTotal, '>')) {
                        $("table tr #dr-diff").text("");
                        $("table tr #cr-diff").text(jsFloatOps(drTotal, crTotal, '-'));
                    } else {
                        $("table tr #dr-diff").text(jsFloatOps(crTotal, drTotal, '-'));
                        $("table tr #cr-diff").text("");
                    }
                }
            });

            /* Dr - Cr dropdown changed */
            $(document).on('change', '.dc-dropdown', function() {
                var drValue = $(this).parent().parent().next().next().children().children().prop('value');
                var crValue = $(this).parent().parent().next().next().next().children().children().prop('value');

                if ($(this).parent().parent().next().children().children().val() == "0") {
                    return;
                }

                drValue = parseFloat(drValue);
                if (isNaN(drValue))
                    drValue = 0;

                crValue = parseFloat(crValue);
                if (isNaN(crValue))
                    crValue = 0;

                if ($(this).prop('value') == "D") {
                    if (drValue == 0 && crValue != 0) {
                        $(this).parent().parent().next().next().children().children().prop('value', crValue);
                    }
                    $(this).parent().parent().next().next().next().children().children().prop('value', "");
                    $(this).parent().parent().next().next().next().children().children().prop('disabled', 'disabled');
                    $(this).parent().parent().next().next().children().children().prop('disabled', '');
                } else {
                    if (crValue == 0 && drValue != 0) {
                        $(this).parent().parent().next().next().next().children().prop('value', drValue);
                    }
                    $(this).parent().parent().next().next().children().children().prop('value', "");
                    $(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
                    $(this).parent().parent().next().next().next().children().children().prop('disabled', '');
                }
                /* Recalculate Total */
                $('.dr-item:first').trigger('change');
                $('.cr-item:first').trigger('change');
            });

            /* Ledger dropdown changed */
            $(document).on('change', '.ledger-dropdown', function() {
                if ($(this).val() == "0") {
                    //alert($(this).parent().parent())
                    /*var lid = $(this).attr('id');
                    var idArray = lid.split('_');
                    var id = idArray[1];
                    $('#dr_'+id).val('');
                    $('#dr_'+id).prop('disabled', 'disabled');*/
                    /* Reset and disble dr and cr amount */
                    $(this).parent().parent().next().children().children().prop('value', "");
                    $(this).parent().parent().next().next().children().children().prop('value', "");
                    $(this).parent().parent().next().children().children().prop('disabled', 'disabled');
                    $(this).parent().parent().next().next().children().children().prop('disabled', 'disabled');
                } else {
                    /* Enable dr and cr amount and trigger Dr/Cr change */
                    $(this).parent().parent().next().children().children().prop('disabled', '');
                    $(this).parent().parent().next().next().children().children().prop('disabled', '');
                    $(this).parent().parent().prev().children().children().trigger('change');
                }
                /* Trigger dr and cr change */
                $(this).parent().parent().next().children().children().trigger('change');
                $(this).parent().parent().next().next().children().children().trigger('change');

                var ledgerid = $(this).val();
                var rowid = $(this);
                if (ledgerid > 0) {
                    $.ajax({
                        url: path + 'ledger-cl',
                        data: 'id=' + ledgerid,
                        dataType: 'json',
                        success: function(data)
                        {
                            var ledger_bal = parseFloat(data['cl']['amount']);

                            var prefix = '';
                            var suffix = '';
                            if (data['cl']['status'] == 'neg') {
                                prefix = '<span class="error-text">';
                                suffix = '</span>';
                            }

                            if (data['cl']['dc'] == 'D') {
                                rowid.parent().parent().next().next().next().next().children().html(prefix + "Dr " + ledger_bal + suffix);
                            } else if (data['cl']['dc'] == 'C') {
                                rowid.parent().parent().next().next().next().next().children().html(prefix + "Cr " + ledger_bal + suffix);
                            } else {
                                rowid.parent().parent().next().next().next().next().children().html("");
                            }
                        }
                    });
                } else {
                    rowid.parent().parent().next().next().next().next().children().text("");
                }
            });

            /* Recalculate Total */
            $(document).on('click', 'table td .recalculate', function() {
                /* Recalculate Total */
                $('.dr-item:first').trigger('change');
                $('.cr-item:first').trigger('change');
            });

            /* Delete ledger row */
            $(document).on('click', '.deleterow', function() {
                $(this).parent().parent().remove();
                /* Recalculate Total */
                $('.dr-item:first').trigger('change');
                $('.cr-item:first').trigger('change');
            });

            /* Add ledger row */
            $(document).on('click', '.addrow', function() {
                var cur_obj = this;
                $.ajax({
                    data: {
                    'restriction_bankcash': {{ $entrytype->restriction_bankcash }},
                    'row_id': $(this).attr('data-id')
                    },
                    dataType: 'json',
                    url: path + 'entry-addrow',
                    success: function(data) {
                        $(cur_obj).parent().parent().after(data);
                        /* Trigger ledger item change */
                        $(cur_obj).parent().parent().next().children().first().next().children().children().children().trigger('change');
                    }
                });
            });

            /* On page load initiate all triggers */
            $('.dc-dropdown').trigger('change');
            $('.ledger-dropdown').trigger('change');
            $('.dr-item:first').trigger('change');
            $('.cr-item:first').trigger('change');
        });

    </script>
@endsection