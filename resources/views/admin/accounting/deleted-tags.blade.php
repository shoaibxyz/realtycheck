@extends('admin.template')
@section('title','Tags')

@section('content')
    <h2>Deleted Tags</h2>

    <div class="x_panel">
        <div class="row">
            <div class="col-lg-12">
            <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                <h4 class="box-heading">Success</h4>
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
        </div>
        <input type="hidden" value="{{ route('getAllDelTags') }}" name="hfroute" id="hfroute">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblTags">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
        </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'title', name: 'title', orderable: true },
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblTags').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[1, "asc" ]],
            ajax: {
                url: $('#hfroute').val()
            },
            "columns": columns
        });
	</script>
@endsection