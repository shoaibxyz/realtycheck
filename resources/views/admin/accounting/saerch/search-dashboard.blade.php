@extends('admin.template')
@section('title','Dashboard')

@section('style')

@endsection

@section('content')

{!! Breadcrumbs::render('reportdashboard') !!}
    
    <!-- buttons links -->

    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              <div class="metro-nav">
             @if(App\Models\User::getPermission('can_view_bsheet')) 
        <div class="metro-nav-block nav-olive">
          <a data-original-title="" href="{{ url('admin/accounting/balance_sheet') }}">
            <i class="fa fa-book"></i>
            <div class="info">Balance Sheet</div>
            <div class="status">Balance Sheet</div>
          </a>
        </div>
        @endif
        @if(App\Models\User::getPermission('can_view_profitloss'))
        <div class="metro-nav-block nav-block-orange">
          <a data-original-title="" href="{{ url('admin/accounting/profit_loss') }}">
             <i class="fa fa-book"></i>
            <div class="info">Profit Loss</div>
            <div class="status">Profit Loss</div>
          </a>
        </div>
        @endif
        @if(App\Models\User::getPermission('can_view_trialbalance'))
        <div class="metro-nav-block nav-block-yellow">
          <a data-original-title="" href="{{ url('admin/accounting/trial_balance') }}">
            <i class="fa fa-book"></i>
            <div class="info">Trial Balance</div>
            <div class="status">Trial Balance</div>
          </a>
        </div>
        @endif
        @if(App\Models\User::getPermission('can_view_lgstatement'))
        <div class="metro-nav-block nav-block-red">
          <a data-original-title="" href="{{ url('admin/accounting/ledger_statement') }}">
            <i class="fa fa-book"></i>
            <div class="info">Ledger Statement</div>
            <div class="status">Ledger Statement</div>
          </a>
        </div>
        @endif
        @if(App\Models\User::getPermission('can_view_reconcile'))
        <div class="metro-nav-block nav-light-purple">
          <a data-original-title="" href="{{ url('admin/accounting/ledger_reconcile') }}">
            <i class="fa fa-book"></i>
            <div class="info">Ledger Reconciliation</div>
            <div class="status">Ledger Reconciliation</div>
          </a>
        </div>
        @endif
        
      </div>

      
        

      
</div></div>
              </div>
          </div>


    <!-- buttons links -->

    
@endsection
