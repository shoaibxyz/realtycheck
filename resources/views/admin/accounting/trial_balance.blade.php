@extends('admin.layouts.template')
@section('title','Trial Balance')
@section('accounting-active','active')
@section('acc-reports-head-active','active')
@section('trial-balance-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    <?php
    function print_account_chart($account, $c = 0, $THIS)
    {
        $counter = $c;

        /* Print groups */
        if ($account->id != 0) {
            if ($account->id <= 4) {
                echo '<tr class="tr-group tr-root-group">';
            } else {
                echo '<tr class="tr-group">';
            }
            echo '<td class="td-group">';
            echo print_space($counter);
            echo \App\Accounting\Helpers::toCodeWithName($account->code, $account->name);
            echo '</td>';

            echo '<td>Group</td>';

            echo '<td>';
            echo \App\Accounting\Helpers::toCurrency($account->op_total_dc, $account->op_total);
            echo '</td>';

            echo '<td>' . \App\Accounting\Helpers::toCurrency('D', $account->dr_total) . '</td>';

            echo '<td>' . \App\Accounting\Helpers::toCurrency('C', $account->cr_total) . '</td>';

            if ($account->cl_total_dc == 'D') {
                echo '<td>' . \App\Accounting\Helpers::toCurrency('D', $account->cl_total) . '</td>';
            } else {
                echo '<td>' . \App\Accounting\Helpers::toCurrency('C', $account->cl_total) . '</td>';
            }

            echo '</tr>';
        }

        /* Print child ledgers */
        if (count($account->children_ledgers) > 0) {
            $counter++;
            foreach ($account->children_ledgers as $id => $data) {
                echo '<tr class="tr-ledger">';
                echo '<td class="td-ledger">';
                echo print_space($counter);
                echo '<a href="ledgerstatement/'. $data['id'] .'">'. \App\Accounting\Helpers::toCodeWithName($data['code'], $data['name']) .'</a>';
                echo '</td>';
                echo '<td>Ledger</td>';

                echo '<td>';
                echo \App\Accounting\Helpers::toCurrency($data['op_total_dc'], $data['op_total']);
                echo '</td>';

                echo '<td>' . \App\Accounting\Helpers::toCurrency('D', $data['dr_total']) . '</td>';

                echo '<td>' . \App\Accounting\Helpers::toCurrency('C', $data['cr_total']) . '</td>';

                if ($data['cl_total_dc'] == 'D') {
                    echo '<td>' . \App\Accounting\Helpers::toCurrency('D', $data['cl_total']) . '</td>';
                } else {
                    echo '<td>' . \App\Accounting\Helpers::toCurrency('C', $data['cl_total']) . '</td>';
                }

                echo '</tr>';
            }
            $counter--;
        }

        /* Print child groups recursively */
        foreach ($account->children_groups as $id => $data) {
            $counter++;
            print_account_chart($data, $counter, $THIS);
            $counter--;
        }
    }

    function print_space($count)
    {
        $html = '';
        for ($i = 1; $i <= $count; $i++) {
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        return $html;
    }

    ?>
     <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Trial Balance</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Trial Balance</li>
        </ol>
    </div>
    <div class="x_panel">
          <div class="x_title">
                <h2>Trial Balance</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row" style="text-align: right; padding-right: 15px;">
            <div class="btn-group" role="group" style="display: inline-block">
                {{--<a href="" class="btn btn-default btn-sm">DOWNLOAD .CSV</a>
                <a href="" class="btn btn-default btn-sm">DOWNLOAD .XLS</a>--}}
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onclick="PrintBS()">PRINT</a>
            </div>
        </div>
        <br><br><br>
        <div id="subtitle">
        <div class="subtitle text-center">
            <?php echo $subtitle ?>
        </div>
        </div>
        <br><br>
        <div class="table-responsive">
            <table class="table table-hover stripped" id="tblBS">
                <tr>
                    <th>Account Name</th>
                    <th>Type</th>
                    <th>O/P Balance</th>
                    <th>Debit Total</th>
                    <th>Credit Total</th>
                    <th>C/L Balance</th>
                </tr>
                {{ print_account_chart($accountlist, -1, $this) }}
                <tr class="{{ (\App\Accounting\Helpers::calculate($accountlist->dr_total, $accountlist->cr_total, '==')) ? "bold-text ok-text" : "bold-text error-text" }}">
                    <td>Total</td>
                    <td></td>
                    <td></td>
                    <td>{{ \App\Accounting\Helpers::toCurrency('D', $accountlist->dr_total) }}</td>
                    <td>{{ \App\Accounting\Helpers::toCurrency('C', $accountlist->cr_total) }}</td>
                    <td><span class="{{ (\App\Accounting\Helpers::calculate($accountlist->dr_total, $accountlist->cr_total, '==')) ? "glyphicon glyphicon-ok-sign" : "glyphicon glyphicon-remove-sign" }}"></span></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">

        function PrintBS()
        {
            var WindowObject = window.open('Print Balance Sheet', 'PrintWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=1000,height=600');
            WindowObject.document.writeln('<!DOCTYPE html>');
            WindowObject.document.writeln('<html><head><title></title>');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
            WindowObject.document.writeln('<link href="http://localhost:8/pcs/public/css/custom.min.css" rel="stylesheet">');
            WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')

            WindowObject.document.writeln('<br><br><center style="font-weight: bold;"><strong>');
            WindowObject.document.writeln($('#page-heading').html());
            WindowObject.document.writeln('</strong></center><br><br>');

            WindowObject.document.writeln($('#subtitle').html());
            WindowObject.document.writeln('<br><br>');
            WindowObject.document.writeln($('.table-responsive').html());

            WindowObject.document.writeln('</body></html>');

            WindowObject.document.close();

            WindowObject.focus();
            //WindowObject.print();

            //WindowObject.close();
            return true;
        }
    </script>
@endsection