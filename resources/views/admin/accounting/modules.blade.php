@extends('admin.template')
@section('title','Modules')

@section('content')

    <h2>Modules
    @if(App\User::getPermission('can_add_module'))
    <small class="pull-right"><a href="{{ url('/admin/accounting/create-module')}}" class="btn btn-primary add" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a></small></h2>
    @endif
    <div class="x_panel">
        <div class="row">
            <div class="col-lg-12">
            <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                <h4 class="box-heading">Success</h4>
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
        </div>
        <input type="hidden" value="{{ route('getAllModules') }}" name="hfroute" id="getAllModules">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblModules">
                <thead>
                    <tr>
                        <th>Label</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Financial Year Start</th>
                        <th>Financial Year End</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
        </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'label', name: 'label', orderable: true },
            { data: 'company', name: 'company', orderable: true},
            { data: 'email', name: 'email', orderable: true},
            { data: 'fy_start', name: 'fy_start', orderable: true},
            { data: 'fy_end', name: 'fy_end', orderable: true},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblModules').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[5, "asc" ]],
            ajax: {
                url: $('#getAllModules').val()
            },
            "columns": columns
        });
	</script>
@endsection