@extends('admin.layouts.template')
@section('title','Search')
@section('accounting-active','active')
@section('search-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
   <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Search</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Search</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>Search</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmSearch" id="frmSearch" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'search_results' }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Ledgers</label>
                            <div class="col-sm-8">
                                <select name="ledgers[]" id="ledgers" class="form-control" multiple="multiple">
                                    <option value="" selected="selected">All</option>
                                    @foreach($ledgers as $key => $val)
                                        <option value="{{ $key }}" {{ ($key < 0) ? "disabled=disabled" : "" }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Entrytypes</label>
                            <div class="col-sm-8">
                                <select name="entrytypes[]" id="entrytypes" class="form-control" multiple="multiple">
                                    <option value="" selected="selected">All</option>
                                    @foreach($entrytypes as $entrytype)
                                        <option value="{{ $entrytype->id }}">{{ $entrytype->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Entry Number</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <select name="number_restrict" id="number_restrict" class="form-control">
                                            <option value="1" selected="selected">Equal to</option>
                                            <option value="2">Less than or Equal to</option>
                                            <option value="3">Greater than or equal to</option>
                                            <option value="4">In between</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" name="number" id="number" class="form-control">
                                        <input type="text" name="number2" id="number2" class="form-control" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Dr or Cr</label>
                            <div class="col-sm-8">
                                <select name="drcr_restrict" id="drcr_restrict" class="form-control">
                                    <option value="" selected="selected">(Any)</option>
                                    <option value="D">Dr</option>
                                    <option value="C">Cr</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Amount</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <select name="amount_restrict" id="amount_restrict" class="form-control">
                                            <option value="1" selected="selected">Equal to</option>
                                            <option value="2">Less than or Equal to</option>
                                            <option value="3">Greater than or equal to</option>
                                            <option value="4">In between</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" name="amount" id="amount" class="form-control">
                                        <input type="text" name="amount2" id="amount2" class="form-control" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">From date</label>
                            <div class="col-sm-8">
                                <input type="text" name="from_date" id="from_date" class="form-control datepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">To date</label>
                            <div class="col-sm-8">
                                <input type="text" name="to_date" id="to_date" class="form-control datepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Tags</label>
                            <div class="col-sm-8">
                                <select name="tags[]" id="tags" class="form-control" multiple="multiple">
                                    <option value="" selected="selected">All</option>
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}">{{ $tag->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Narration contains</label>
                            <div class="col-sm-8">
                                <input type="text" name="narration" id="narration" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <button type="submit" class="btn btn-default btn-orange">Search</button>
                        <a href="{{ env('ACC_ADMIN_PATH') . 'acc-dashboard' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">

        <?php $module = \App\Accounting\Module::find(Session::get('module')); ?>

        $(document).ready(function() {

            var path = '{{ env('ACC_ADMIN_PATH') }}';
            /* Calculate date range in javascript */
            //startDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //endDate = new Date(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_end) * 1000; ?>  + (new Date().getTimezoneOffset() * 60 * 1000));
            //alert(<?php echo strtotime(\App\Accounting\Module::find(Session::get('module'))->fy_start) * 1000; ?>);
            /* Setup jQuery datepicker ui */
            $('#from_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "minDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "minDate", startDate);
                    }
                }
            });

            $('#to_date').datepicker({
                dateFormat: 'dd-M-yy',
                minDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_start)); ?>'),
                maxDate: new Date('<?php echo date('Y-m-d',strtotime($module->fy_end)); ?>'),
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    if (selectedDate) {
                        $("#from_date").datepicker("option", "maxDate", selectedDate);
                    } else {
                        $("#from_date").datepicker("option", "maxDate", endDate);
                    }
                }
            });
        });

        $('#number_restrict').on('change',function(e){
            if($(this).val() == 4)
            {
                $('#number2').show();
                $('#number1').css({'width':'49%'});
            }
            else
            {
                $('#number2').hide();
                $('#number1').attr({'width':'59%'});
            }
        });

        $('#amount_restrict').on('change',function(e){
            if($(this).val() == 4)
            {
                $('#amount2').show();
                $('#amount1').css({'width':'49%'});
            }
            else
            {
                $('#amount2').hide();
                $('#amount1').attr({'width':'59%'});
            }
        });

    </script>
@endsection