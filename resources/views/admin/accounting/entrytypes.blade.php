@extends('admin.layouts.template')
@section('title','Entry Types')
@section('accounting-active','active')
@section('entry-types-head-active','active')
@section('entry-types-active','active')
@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Entry Types</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Entry Types</li>
        </ol>
    </div>
    
    <div class="x_panel">
         <div class="x_title">
                <h2>Entry Types</h2>
                
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ url('/admin/accounting/add-entrytype')}}" class="btn btn-primary add" style="{{$rights->show_create}}" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{ route('getAllEtypes') }}" name="hfroute" id="hfroute">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblEtypes">
                <thead>
                    <tr>
                        <th>Label</th>
                        <th>Name</th>
                        <th>Prefix</th>
                        <th>Suffix</th>
                        <th>Zero Padding</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'label', name: 'label', orderable: true },
            { data: 'name', name: 'name', orderable: true},
            { data: 'prefix', name: 'prefix', orderable: true},
            { data: 'suffix', name: 'suffix', orderable: true},
            { data: 'zero_padding', name: 'zero_padding', orderable: true},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblEtypes').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[0, "desc" ]],
            ajax: {
                url: $('#hfroute').val()
            },
            "columns": columns
        });
	</script>
@endsection