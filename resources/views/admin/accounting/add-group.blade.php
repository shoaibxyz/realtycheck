@extends('admin.layouts.template')
@section('title','Add New Group')
@section('accounting-active','active')
@section('accounting-head-active','active')
@section('charts-of-accounts-active','active')

@section('style')

@endsection

@section('content')

    
   
    
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Charts of Accounts</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Group</li>
        </ol>
         @if(!empty($message))
        <p>{{$message}}</p>
    @endif
    </div>
    
    
    <div class="x_panel">
        <div class="x_title">
                <h2>Add New Group</h2>
                
              
                <div class="clearfix"></div>
            </div>
            
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmModule" id="frmModule" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'add-group' }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Group name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                                       placeholder="Enter group name here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="db_name" class="col-sm-3 control-label">Group code</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="code" name="code" value="{{ old('code') }}"
                                       placeholder="Enter group code here">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Parent Group <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="parent" id="parent" class="form-control" required>
                                    @foreach($parents as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id="AffectsGross" class="form-group" style="display: none">
                            <label for="inputEmail3" class="col-sm-3 control-label">Affects</label>
                            <div class="col-sm-8">
                                <input type="radio" name="rdo_affects" id="rdo_affects_1" value="1" checked="checked">&nbsp;Gross Profit & Loss<br>
                                <input type="radio" name="rdo_affects" id="rdo_affects_0" value="0">&nbsp;Net Profit & Loss
                                <span class="help-block"><strong>Note:</strong> Changes to whether it affects Gross or Net Profit &amp; Loss is reflected in final Profit &amp; Loss statement.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <input type="hidden" name="module_id" id="module_id" value="" />
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        &nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'accounts' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script language="JavaScript">
        $(document).ready(function() {
            /**
             * On changing the parent group select box check whether the selected value
             * should show the "Affects Gross Profit/Loss Calculations".
             */
            $('#parent').change(function() {
                if ($(this).val() == '3' || $(this).val() == '4') {
                    $('#AffectsGross').show();
                } else {
                    $('#AffectsGross').hide();
                }
            });
            $('#GroupParentId').trigger('change');

            $("#GroupParentId").select2({width:'100%'});
        });
    </script>
@endsection