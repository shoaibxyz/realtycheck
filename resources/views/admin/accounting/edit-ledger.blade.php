@extends('admin.layouts.template')
@section('title','Edit Ledger')
@section('accounting','active')
@section('acc_accounts','active')
@section('acc_child','display:block')
@section('acc_accounts_child','display:block')

@section('style')

@endsection

@section('content')

     <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Charts of Accounts</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Ledger</li>
        </ol>
         @if(!empty($message))
        <p>{{$message}}</p>
    @endif
    </div>
    <div class="x_panel">
         <div class="x_title">
                <h2>Edit Ledger</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmLedger" id="frmLedger" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'edit-ledger/' . $ledger->id }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Ledger name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $ledger->name }}"
                                       placeholder="Enter ledger name here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="db_name" class="col-sm-3 control-label">Ledger code</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="code" name="code" value="{{ $ledger->code }}"
                                       placeholder="Enter ledger code here">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Parent Group <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select name="group" id="group" class="form-control" required>
                                    @foreach($parents as $key => $val)
                                        <option value="{{ $key }}" {{ $key == $ledger->group_id ? "selected=selected" : "" }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Opening Balance</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <select name="op_balance_dc" class="form-control" id="LedgerOpBalanceDc" required>
                                            <option value="D" {{ "D" == $ledger->op_balance_dc ? "selected=selected" : "" }}>Dr</option>
                                            <option value="C" {{ "C" == $ledger->op_balance_dc ? "selected=selected" : "" }}>Cr</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-10"><input name="op_balance" class="form-control" step="0.01" type="number" value="{{ $ledger->op_balance }}" id="LedgerOpBalance"><span class="help-block">Note : Assets / Expenses always have Dr balance and Liabilities / Incomes always have Cr balance.</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"></label>
                            <div class="col-sm-8">
                                <input type="hidden" name="hf_type" id="hf_type" value="0">
                                <input type="checkbox" name="ledger_type" id="ledger_type" {{ $ledger->type == 1 ? "checked=checked" : "" }}> Bank or cash account<br>
                                <span class="help-block">Note : Select if the ledger account is a bank or a cash account.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"></label>
                            <div class="col-sm-8">
                                <input type="hidden" name="hf_reconciliation" id="hf_reconciliation" value="0">
                                <input type="checkbox" name="reconciliation" id="reconciliation" {{ $ledger->reconciliation == 1 ? "checked=checked" : "" }}> Reconciliation<br>
                                <span class="help-block">Note : If selected the ledger account can be reconciled from Reports > Reconciliation.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Notes</label>
                            <div class="col-sm-8">
                                <textarea name="notes" class="form-control" rows="3" maxlength="500" cols="30" id="notes">{{ $ledger->notes }}</textarea>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}" {{ $ledger->status == $value['id'] ? "selected=selected" : "" }}>{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                    </div>
                    <div class="form-actions text-right pal">
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        &nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'accounts' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')

@endsection