@extends('admin.template')
@section('title','Groups')

@section('content')

    <h2>Deleted Groups</h2>

    <div class="x_panel">
        <div class="row">
            <div class="col-lg-12">
            <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                <h4 class="box-heading">Success</h4>
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
        </div>
        <input type="hidden" value="{{ route('getAllDelGroups') }}" name="hfroute" id="hfroute">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblGroups">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Deleted at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
        </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'name', name: 'name', orderable: true },
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'deleted_at', name: 'deleted_at', orderable: true},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblGroups').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "order": [[1, "asc" ]],
            ajax: {
                url: $('#hfroute').val()
            },
            "columns": columns
        });
	</script>
@endsection