@extends('admin.layouts.template')
@section('title','Accounting Dashboard')
@section('accounting-active','active')
@section('acc-dashboard-active','active')
@section('style')

@endsection

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Accounting Dashboard</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Accounting Dashboard</li>
        </ol>
        @if(!empty($message))
        <p>{{$message}}</p>
    @endif
    </div>
    

    <!-- buttons links -->

    <div class="x_panel">
        <div class="row mbm">
            <div class="col-lg-12" style="margin: auto; float: none; padding: 20px;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"><strong>Account details</strong></div>
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="5" style="width: 100%">
                                    <tbody><tr>
                                        <td width="35%" style="padding-bottom: 10px">Name</td>
                                        <td width="65%">{{ $module->label }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Email</td>
                                        <td>{{ $module->email }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Currency</td>
                                        <td>PKR</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Financial Year</td>
                                        <td>{{ \Carbon\Carbon::parse($module->fy_start)->format('d-M-Y') }} to {{ \Carbon\Carbon::parse($module->fy_end)->format('d-M-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Status</td>
                                        <td>{{ \App\Accounting\Helpers::getStatus($module->status)[0] }}</td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Bank &amp; cash summary</div>
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="5" style="width: 100%">
                                    <tbody>
                                    @if(count($ledgers) > 0)
                                        @foreach($ledgers as $ledger)
                                            <tr><td width="50%">{{ $ledger['name'] }}</td><td width="50%">{{ \App\Accounting\Helpers::toCurrency($ledger['balance']['dc'], $ledger['balance']['amount']) }}</td></tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">Account summary</div>
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="5" style="width: 100%">
                                    <tbody><tr>
                                        <td width="50%" style="padding-bottom: 10px">Assets</td>
                                        <td width="50%">{{ \App\Accounting\Helpers::toCurrency($accsummary['assets_total_dc'], $accsummary['assets_total']) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px"> Liabilities and Owners Equity</td>
                                        <td>{{ \App\Accounting\Helpers::toCurrency($accsummary['liabilities_total_dc'], $accsummary['liabilities_total']) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Income</td>
                                        <td>{{ \App\Accounting\Helpers::toCurrency($accsummary['income_total_dc'], $accsummary['income_total']) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px">Expense</td>
                                        <td>{{ \App\Accounting\Helpers::toCurrency($accsummary['expense_total_dc'], $accsummary['expense_total']) }}</td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script type="text/javascript">

    </script>
@endsection