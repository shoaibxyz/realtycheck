@extends('admin.template')
@section('title','List Of Entries')

@section('content')


    <h2>List Of Entries</h2>

    <div class="x_panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>

        <input type="hidden" value="{{ route('getAllDelEntries') }}" name="hfroute" id="hfroute">

        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblEntries">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Ledger</th>
                        <th>Type</th>
                        <th>Tag</th>
                        <th>Debit Amount (RS)</th>
                        <th>Credit Amount (RS)</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')
	<script>
        var columns = [
            { data: 'date', name: 'date', orderable: true},
            { data: 'number', name: 'number', orderable: true, className: 'col-center'},
            { data: 'ledger', name: 'ledger', orderable: false, className: 'col-center'},
            { data: 'etname', name: 'etname', orderable: true, className: 'col-center'},
            { data: 'tag_id', name: 'tag_id', orderable: true, className: 'col-center'},
            { data: 'dr_total', name: 'dr_total', orderable: true, className: 'col-center'},
            { data: 'cr_total', name: 'cr_total', orderable: true, className: 'col-center'},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblEntries').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            /*"order": [5, "asc"],*/
            ajax: {
                url: $('#hfroute').val()
            },
            "columns": columns
        });

	</script>
@endsection