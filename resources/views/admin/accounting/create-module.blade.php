@extends('admin.template')
@section('title','Add New Account')
@section('accounting','active')
@section('acc_modules','active')
@section('acc_child','display:block')
@section('acc_modules_child','display:block')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

     <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Module</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Account</li>
        </ol>
    </div>
    <div class="x_panel">
         <div class="x_title">
                <h2>Add New Account</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form name="frmModule" id="frmModule" class="form-horizontal" method="post" action="{{ env('ACC_ADMIN_PATH') . 'create-module' }}">
                    <div class="form-body pal">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="label" class="col-sm-3 control-label">Label <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="label" name="label" value="{{ old('label') }}"
                                       placeholder="Enter account label here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="db_name" class="col-sm-3 control-label">Company name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="company" name="company" value="{{ old('company') }}"
                                       placeholder="Enter company name here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                                   placeholder="Enter email here" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="address" name="address" value="{{ old('address') }}"
                                   placeholder="Enter address here"></textarea>
                            </div>
                        </div>
                        {{--<div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Currency symbol</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="currency_symbol" name="currency_symbol" value="{{ old('currency_symbol') }}"
                                   placeholder="Enter currency symbol here" required>
                            </div>
                        </div>--}}
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Financial year start <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control datepicker" id="fy_start" name="fy_start" value="{{ old('fy_start') }}"
                                   placeholder="Financial year start" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Financial year end <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control datepicker" id="fy_end" name="fy_end" value="{{ old('fy_end') }}"
                                   placeholder="Financial year end" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select name="status" id="status" class="form-control">
                                    @foreach(App\Accounting\Helpers::statuses() as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right pal">
                        <input type="hidden" name="module_id" id="module_id" value="" />
                        <button type="submit" class="btn btn-default btn-orange">Submit</button>
                        &nbsp;
                        <a href="{{ env('ACC_ADMIN_PATH') . 'all-modules' }}" class="btn btn-grey">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                dateFormat: 'dd-M-yy'
            });
        });
    </script>
@endsection