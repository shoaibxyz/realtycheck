@extends('admin.layouts.template')
@section('title','List Of Entries')
@section('accounting-active','active')
@section('entries-head-active','active')
@section('entries-active','active')
@section('content')
 <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">List Of Entries</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">List of Entries</li>
        </ol>
    </div>
    
    
    <div class="x_panel">
         <div class="x_title">
                <h2>List Of Entries</h2>
                
                 <small class="pull-right btn-group" style="margin-bottom: 5px; {{$rights->show_create}}">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" title="Add New"><i class="fa fa-plus"></i>&nbsp;
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" style="z-index: 10000;">
                @foreach($etypes as $etype)
                    <li><a href="{{ url('/admin/accounting/add-entry/' . $etype->id)}}">{{ $etype->name }}</a></li>
                @endforeach
            </ul>
            
            </small>
                <div class="clearfix"></div>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 align-right" style="text-align: right; float: right; position: relative; top: 20px; z-index: 1000;">
                <form method="POST" id="search-form" class="form-inline align-right" role="form">
                    <div class="form-group" style="width: 100%;">
                        <select name="etype" id="etype" class="form-control ddsearch" style="visibility: visible; text-align: left" onchange="$('#search-form').submit()">
                            <option value="">All</option>
                            @foreach($etypes as $etype)
                                <option value="{{ $etype->id }}">{{ $etype->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <input type="hidden" value="{{ route('getAllEntries') }}" name="hfroute" id="hfroute">
        <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="tblEntries">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Ledger</th>
                        <th>Type</th>
                        <th>Tag</th>
                        <th>Debit Amount (RS)</th>
                        <th>Credit Amount (RS)</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section ('javascript')

	<script>
        var columns = [
            { data: 'date', name: 'date', orderable: true},
            { data: 'number', name: 'number', orderable: true, className: 'col-center'},
            { data: 'ledger', name: 'ledger', orderable: false, className: 'col-center'},
            { data: 'etname', name: 'etname', orderable: true, className: 'col-center'},
            { data: 'tag_id', name: 'tag_id', orderable: true, className: 'col-center'},
            { data: 'dr_total', name: 'dr_total', orderable: true, className: 'col-center'},
            { data: 'cr_total', name: 'cr_total', orderable: true, className: 'col-center'},
            { data: 'status', name: 'status', orderable: true, className: 'col-center'},
            { data: 'btn_actions', name: 'btn_actions', orderable: false, className: 'col-center'},
        ];

        var oTable = $('#tblEntries').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave":true,
            "bDestroy":true,
            "bFilter":false,
            "bLength":false,
            "order": [0, "desc"],
            ajax: {
                url: $('#hfroute').val(),
                data: function (d) {
                    d.etype = $('#etype').val();
                    d.tid = '{{ !empty($tid) ? $tid : '' }}';
                }
            },
            "columns": columns
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });

        function changeStatus(id, status)
        {
            var url = '{{ env('ACC_ADMIN_PATH') }}updentrystatus';
            if(status == 0)
                status = 1
            else
                status = 0;

            $.ajax({
                type: 'get',
                url: url,
                data: {
                    'status': status,
                    'id': id,
                },
                success: function(data) {
                    alert('Entry status has been changed successfully.');
                    oTable.draw();
                }
            });
            return false;
        }

	</script>
@endsection