@extends('admin.layouts.template')
@section('title','View Payment Entry')
@section('accounting-active','active')
@section('entries-head-active','active')
@section('entries-active','active')

@section('style')
@endsection

@section('content')

    
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">View Payment Entry</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">View Payment Entry</li>
        </ol>
    </div>
    <div class="x_panel">
        <div class="x_title">
                <h2>View Payment Entry</h2>
                
              
                <div class="clearfix"></div>
            </div>
        <div class="row mbm">
            <div class="col-lg-12" style="margin: auto; float: none; padding: 20px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="table-responsive">
                <table class="table table-bordered table-hover" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>Dr/Cr</th>
                        <th>Ledger</th>
                        <th>Dr Amount (RS)</th>
                        <th>Cr Amount (RS)</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($entryitems as $row => $entryitem)
                        <tr>
                            <td>
                                {{ ($entryitem['dc'] == 'D') ? 'Dr' : 'Cr' }}
                            </td>
                            <td>{{ $entryitem['ledger_name'] }}</td>
                            <td>{{ ($entryitem['dc'] == 'D') ? $entryitem['dr_amount'] : '' }}</td>
                            <td>{{ ($entryitem['dc'] == 'C') ? $entryitem['cr_amount'] : '' }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td><strong>{{ \App\Accounting\Helpers::toCurrency('D',$entry->dr_total) }}</strong></td>
                            <td><strong>{{ \App\Accounting\Helpers::toCurrency('C',$entry->cr_total) }}</strong></td>
                        </tr>
                        @if(\App\Accounting\Helpers::calculate($entry->dr_total,$entry->cr_total, '=='))
                        @else
                            <tr class="error-text">
                                <td></td>
                                <td>Difference</td>
                            @if(\App\Accounting\Helpers::calculate($entry->dr_total,$entry->cr_total, '>'))
                                <td id="dr-diff">{{ \App\Accounting\Helpers::toCurrency('D',\App\Accounting\Helpers::calculate($entry->dr_total,$entry->cr_total, '-')) }}</td>
                                <td></td>
                            @else
                                <td></td>
                                <td id="cr-diff">{{ \App\Accounting\Helpers::toCurrency('C',\App\Accounting\Helpers::calculate($entry->dr_total,$entry->cr_total, '-')) }}</td>
                            @endif
                            </tr>
                        @endif
                        <tr>
                            <td colspan="4">
                                <span style="float: left; font-weight: bold;">Narration:</span>&nbsp;&nbsp;<span style="float: left; margin-left: 10px">{!! $entry->narration !!}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span style="float: left; font-weight: bold;">Tag:</span>&nbsp;&nbsp;<span style="float: left; margin-left: 10px; margin-top: 2px;">{!! (!empty($tag)) ? $tag : '' !!}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                        <a href="{{ env('ACC_ADMIN_PATH') . 'entries' }}" class="btn btn-grey">Cancel</a>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section ('javascript')

@endsection