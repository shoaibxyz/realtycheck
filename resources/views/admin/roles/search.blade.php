@extends('admin.template')
@section('title','Search Results')

@section('content')
<table class="table table-bordered table-hover">
    		<thead>
    			<tr>
    				<th>Id</th>
    				<th>Name</th>
    				<th>Display Name</th>
                    <th>Description</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($roles as $role)
	    			<tr>
	    				<td>{{ $role->id }}</td>
	    				<td>{{ $role->name }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td>{{ $role->description }}</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
@endsection