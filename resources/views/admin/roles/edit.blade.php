@extends('admin.layouts.template')

@section('title','Edit Role')

@section('user-management-active','active')
@section('roles-active','active')

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Role</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Role</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'PATCH', 'route' => ['roles.update', $role->id] , 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name', $role->name, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Display Name</label>
                        <div class="col-sm-8">
                            {!! Form::text('display_name', $role->display_name, ['class' => 'form-control', 'id' => 'display_name']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            {!! Form::text('description', $role->description, ['class' => 'form-control', 'id' => 'description']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PATCH') !!}
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('roles.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
