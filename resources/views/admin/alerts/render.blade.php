<table class="table table-bordered table" id="alertTable">
    <thead>
        <tr>
            <th>Alert No</th>
            <th>Message</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php($i = 1)
        @foreach($alerts as $alert)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $alert->message}}</td>
            <td>{{ $alert->created_at->format('d-m-Y')}}</td>
            <td>
                <a href="{{ route('alert.delete', $alert->id) }}" class="btn btn-xs btn-warning">Delete</a>
            </td>
        </tr>
        @php($i++)
        @endforeach
    </tbody>
</table>