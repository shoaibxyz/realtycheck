@extends('admin.layouts.template')

@section('title','Modules')

@section('user-management-active','active')
@section('modules-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Modules List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Modules List</li>
        </ol>
    </div>

     <div class="x_panel">
            <div class="x_title">
                <h2>Modules List</h2>
                <div class="actions" style="float: right; display: inline-block;  {{ $rights->show_create }}">
                    <a href="{{ route('module.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div></div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table bulk_actione" id="datatable">
                    <thead>
                        <tr>
                            
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($modules as $module)
                        <tr>
                            <td>{{ $module->id }}</td>
                            <td>{{ $module->name }}</td>
                            <td>{{ $module->slug }}</td>
                            
                            <td class="text-center" width="15%">
                        <a href="{{ route('module.edit', $module->id )}}" class="btn btn-info btn-xs" style="{{ $rights->show_edit }}"><i class="fa fa-edit"></i></a>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['module.destroy', $module->id] ,'style' => $rights->show_delete]) !!}
                                    <button class="btn btn-danger btn-xs" type="submit" value="" style="{{ $rights->show_delete }}" title="delete"><i class="fa fa-trash-o"></i></button>
                                {!! Form::close() !!}
	    				</td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
 
@endsection

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function(){
        $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
    });
    </script>
@endsection
