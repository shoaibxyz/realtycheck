@extends('admin.layouts.template')

@section('title','Edit Module')

@section('user-management-active','active')
@section('modules-active','active')

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Module</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Module</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                {!! Form::model($modules ,['method' => 'PATCH', 'route' => ['module.update', $modules->id] , 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name',null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Slug <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PATCH') !!}
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('module.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
