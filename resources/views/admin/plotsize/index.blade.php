@extends('admin.layouts.template')

@section('title','Plot Sizes List')

@section('bookings-active','active')
@section('plot-active','active')
@section('plotsize-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Plot Sizes List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Plot Sizes List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Plot Sizes List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('plotsize.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                        <h4 class="box-heading">Success</h4>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table bulk_action" id="plotTable">
                    <thead>
                        <tr>
                            <th width="5%">Sr #</th>
                            <th width="75%">Plot Sizes</th>
                            <th width="10%"></th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@section('javascript')
    <script type="text/javascript">
        var columns = [
            { data: 'id', name: 'id' },
            { data: 'plot_size', name: 'plot_size' },
            { data: 'edit', name: 'edit' },
            { data: 'delete', name: 'delete' }
        ];

        $('#plotTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 5,
            "info": false,
            "ajax": '{!! route('plotsize.getPlots') !!}',
            "columns": columns
        });
        $(document).ready(function() {
            $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
        });
    </script>

@endsection
