@extends('admin.layouts.template')

@section('title','Add Bank Account')

@section('bookings-active','active')
@section('bank-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Bank Account</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Bank Account</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Bank Account</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'route' => 'bank.store', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Bank Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('bank_name', '', ['class' => 'form-control', 'id' => 'bank_name', 'required'=>'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Short Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('short_name', '', ['class' => 'form-control', 'id' => 'short_name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Branch Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('branch_name', '', ['class' => 'form-control', 'id' => 'branch_name', 'required'=>'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Branch Code <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('branch_code', '', ['class' => 'form-control', 'id' => 'branch_code', 'required'=>'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Account NO <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('account', '', ['class' => 'form-control', 'id' => 'account', 'required'=>'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('bank.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
