@extends('admin.layouts.template')

@section('title','Bank Accounts')

@section('bookings-active','active')
@section('bank-active','active')

@section('content')

@include('admin/common/breadcrumb',['page'=>'Bank Accounts'])
   
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Bank Accounts</h2>
                <div class="actions" style="float: right; display: inline-block; {{ $rights->show_create }}">
                    <a href="{{ route('bank.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Account</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th width="5%">Id</th>
                            <th>Bank Name</th>
                            <th>Short Name</th>
                            <th>Branch Code</th>
                            <th>Account</th>
                            <th width="10%">Edit</th>
                            <th width="10%">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banks as $bank)
                            <tr>
                                <td>{{ $bank->id }}</td>
                                <td>{{ $bank->bank_name }}</td>
                                <td>{{ $bank->short_name }}</td>
                                <td>{{ $bank->branch_code }}</td>
                                <td>{{ $bank->account }}</td>
                                <td><a href="{{ route('bank.edit', $bank->id )}}" class="btn btn-info" style="{{ $rights->show_edit }}">Edit</a></td>
                                <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['bank.destroy', $bank->id],'style' => $rights->show_delete]) !!}
                                    {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
