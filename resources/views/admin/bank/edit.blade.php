@extends('admin.layouts.template')

@section('title','Edit Bank')

@section('bookings-active','active')
@section('bank-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Bank</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Bank</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'PATCH', 'route' => ['bank.update', $bank->id] , 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Bank Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('bank_name', $bank->bank_name, ['class' => 'form-control', 'id' => 'bank_name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Short Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('short_name', $bank->short_name, ['class' => 'form-control', 'id' => 'short_name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Branch Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('branch_name', $bank->branch_name, ['class' => 'form-control', 'id' => 'branch_name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Branch Code <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('branch_code', $bank->branch_code, ['class' => 'form-control', 'id' => 'branch_code', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Account NO <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('account', $bank->account, ['class' => 'form-control', 'id' => 'account', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PATCH') !!}
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('bank.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
