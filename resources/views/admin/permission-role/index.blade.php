@extends('admin.template')
@section('title','Permission Role')

@section('content')

      <h2>Permission Role List<small class="pull-right"><a href="{{ route('permission-role.create')}}" class="btn btn-primary">Add New</a></small></h2>

      @if(!empty($message))
        <p>{{$message}}</p>
      @endif

       <div class="x_panel">
        <div class="x_title">
            
            
            <div class="clearfix"></div>
          </div>    
        <div class="table-responsive">
      <table class="table table-bordered table-hover jambo_table">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Role Name</th>
            <th>Assign role</th>
          </tr>
        </thead>
        <tbody>
          @foreach($perms as $key=>$perms)
            <tr>
              <td>{{ $key+1}}</td>              
              <td>{{ $perms->name }}</td>
              <td><a href="{{ route('role.assignPermission', $perms->id )}}" class="btn btn-default btn-xs">Assign Role</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>  </div></div>
@endsection