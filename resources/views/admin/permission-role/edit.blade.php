@extends('admin.template')
@section('title','Update Permission Role')

@section('content')
        
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

        <h2>Update Permission Role</h2>
        {!! Form::open(['method' => 'POST', 'route'=>'permission-role.store', 'class' => 'form-horizontal']) !!}

        <?php $rolesArr[''] = 'Choose one'  ?>
        @foreach($roles as $role)
           <?php $rolesArr[$role->id] = $role->name;  ?>
        @endforeach

        <div class="form-group">
            {!! Form::label('role_id', 'Role Name') !!}
            {!! Form::select('role_id', $rolesArr, '' , ['required' =>'required', 'class' => 'form-control']) !!}
        </div>
        <?php 
            $permissionsArr[''] = 'Choose one';
            $modulesArr = [];
          ?>

          <p>{!! Form::checkbox('checkall',null, false, ['class'=>'checkall']) !!} Check All</p>
            <div class="form-group">
                 @foreach($permissions as $permission)
                     @if(!in_array($permission->module_id, $modulesArr))
                        @php($modulesArr[] = $permission->module_id)
                        <h3>{{ $permission->module->name }}</h3>
                    @endif
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permission_id[]" value="{{ $permission->id }}">
                            {{ $permission->display_name }}
                        </label>
                    </div>
                @endforeach
            </div>

         {{--   <div class="form-group">
                {!! Form::label('permission_id', 'Permission Name') !!}
                {!! Form::select('permission_id', $permissionsArr, '' , ['required' =>'required', 'class' => 'form-control']) !!}
            </div> --}}        
            <div class="form-group">
                {!! Form::submit("Add Permission Role", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>
@endsection

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function() {
        $(".checkall").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
    </script>
@endsection