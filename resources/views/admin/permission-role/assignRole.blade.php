@extends('admin.layouts.template')

@section('title','Assign Permissions')

@section('user-management-active','active')
@section('roles-active','active')

@section('content')
    <link type="text/css" rel="stylesheet" href="{{ asset('public/vendor/iCheck/skins/all.css') }}"/>
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Assign Permissions</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;</li>
            <li><a href="{{ url("admin/roles") }}">Roles</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;</li>
            <li class="active">Assign Permissions</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Assign Permissions</h2>
                <div class="actions" style="float: right; display: inline-block">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <br><br>
            {!! Form::open(['method' => 'POST', 'route'=>'permission-role.store', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('role_id', $role_id) !!}
                {!! $rights_html !!}
                <div class="form-actions text-right pal">
                    {!! Form::submit("Assign", ['class' => 'btn btn-success']) !!}&nbsp;
                    <a href="{{ route('roles.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('public/vendor/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/vendor/iCheck/custom.min.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $(".checkall").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // optional
    });
    </script>
@endsection
