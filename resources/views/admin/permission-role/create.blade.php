@extends('admin.template')
@section('title','Add New Permission Role')

@section('content')
        
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <div class="x_panel">
        <div class="x_title">
            
             <h2>Add New Permission Role</h2>
            <div class="clearfix"></div>
          </div>   
       
		{!! Form::open(['method' => 'POST', 'route'=>'permission-role.store', 'class' => 'form-horizontal']) !!}

        <?php $rolesArr[''] = 'Choose one'  ?>
        @foreach($roles as $role)
           <?php $rolesArr[$role->id] = $role->name;  ?>
        @endforeach

        <div class="form-group">
            {!! Form::label('role_id', 'Role Name') !!}
            {!! Form::select('role_id', $rolesArr, '' , ['required' =>'required', 'class' => 'form-control']) !!}
        </div>
        <?php 
            $permissionsArr[''] = 'Choose one';
            $modulesArr = [];
          ?>

          <p>{!! Form::checkbox('checkall',null, false, ['class'=>'checkall']) !!} Check All</p>
            <div class="form-group">
                 @foreach($permissions as $permission)
                     @if(!in_array($permission->module_id, $modulesArr))
                        @php($modulesArr[] = $permission->module_id)
                        <h3>{{ $permission->module->name }}</h3>
                    @endif
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permission_id[]" class="permissionCheck" id="permission_{{$permission->id}}" value="{{ $permission->id }}">
                            {{ $permission->display_name }}
                        </label>
                    </div>
                @endforeach
            </div>

         {{--   <div class="form-group">
                {!! Form::label('permission_id', 'Permission Name') !!}
                {!! Form::select('permission_id', $permissionsArr, '' , ['required' =>'required', 'class' => 'form-control']) !!}
            </div> --}}        
            <div class="form-group">
                {!! Form::submit("Add Permission Role", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div></div>
@endsection

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function() {
        $(".checkall").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $('#role_id').change(function(e){
            e.preventDefault();
            $.ajax({
                url: '{{ route('role.getPermissions') }}',
                type: 'POST',
                data: {'role_id': $(this).val()},
            })
            .done(function(data) {
               var input = $('.permissionCheck');

               $.each(input, function(index, val) {
                    var permission = $(this);
                    $.each(data.permissions, function(key, value) {
                        console.log($(this).attr('value'));
                        // if(permission.attr('value') == $(this).attr('value'))
                        //     console.log($(this).attr('id'));
                    });
               });      
            })
            .fail(function(xhr, status, responseText) {
                console.log(xhr);
            });
        });
    });
    </script>
@endsection