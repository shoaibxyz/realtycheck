@extends('admin.template')
@section('title','Permission Role')

@section('content')

      <h2>Permissions List<small class="pull-right"><a href="{{ route('permission-role.create')}}" class="btn btn-primary">Add New</a></small></h2>

      @if(!empty($message))
        <p>{{$message}}</p>
      @endif
      <table class="table table-bordered table-hover">
      <h4>{{ $roles->name }}</h4> has following permissions.
        <thead>
          <tr>
            <th>Sr No</th>
            <th>Permissions</th>
            <th></th>
            
          </tr>
        </thead>
        <tbody>
          @foreach($permissions as $key=>$permissions)
            <tr>
              
              {{-- <td>{{ $permission->PermissionName }}</td> --}}
              <td>{{ $key+1 }}</td>
              <td>{{ $permissions->name }}</td>
              <td><a href="{{ route('permission-role.permissiondelete', $permissions->id )}}" class="btn btn-danger btn-xs">Delete</a></td>
              
            </tr>
          @endforeach
        </tbody>
      </table>  
@endsection