@extends('admin.layouts.template')
@section('title', 'Edit Employee')
@section('hr-active','active')
@section('employee-head-active','active')
@section('employees-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection


@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Employee</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Employee</li>
        </ol>
    </div>
    	

      <div class="x_panel">
        <div class="x_title">
           
             <h2>Edit Employee</h2>
            <div class="clearfix"></div>
          </div>   
        <div class="row"> 
        @include('errors')
        {!! Form::model($employee, ['method' => 'PUT', 'route' => ['employee.update', $employee->id], 'files' => 'true', 'class' => 'form-horizontal' , 'id' => 'validate_form']) !!}

        <div class="x_panel">
            <div class="x_title">
                <h2>Personal Detail</h2>
                <div class="clearfix"></div>
            </div>
                <div class="col-sm-4">

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', $employee->user->name , array( 'class'=>'form-control s_name' , 'id' => 'name')  )!!}
                </div></div>
                 <div class="col-md-4">
                        <div class="form-group">
                        {!! Form::label('guardian_type', 'S/O or D/O OR W/O')!!}
                        <?php 
                            $guardian_typeArr[''] = "";
                            $guardian_typeArr['S/O'] = 'S/O';
                            $guardian_typeArr['D/O'] = 'D/O';
                            $guardian_typeArr['W/O'] = 'W/O';
                        ?>
                        {!! Form::select('guardian_type', $guardian_typeArr, $employee->user->guardian_type, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('gurdian_name', 'Guardian Name')!!}
                        {!! Form::text('gurdian_name', $employee->user->gurdian_name , array( 'class'=>'form-control b_name' , 'id' => 'gurdian_name') )!!}
                        </div>
                    </div>

                <div class="col-sm-4">
                     <div class="form-group">
                        {!! Form::label('picture', 'Picture', ['class' => 'col-sm-12'])!!}
                        {!! Form::file('picture', ['class' => 'form-control',  'id' => 'picture']) !!}
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('cnic_pic', 'CNIC Pic', ['class' => 'col-sm-12'])!!}
                        {!! Form::file('cnic_pic', ['class' => 'form-control' , 'id' => 'cnic_pic']) !!}
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('cnic', 'CNIC')!!}
                    {!! Form::text('cnic', $employee->user->cnic , array( 'class'=>'form-control',  'id' => 'cnic' , 'maxlength' => '30') )!!}
                    </div>
                </div>
                <div class="col-sm-4"><img src="{{ env('STORAGE_PATH') . $employee->user->picture }}" width="40" height="40"></div>
                <div class="col-sm-4"><img src="{{ env('STORAGE_PATH') . $employee->user->cnic_pic }}" width="40" height="40"></div>
                <div class="col-sm-4" style="height:50px;"></div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('nationality', 'Nationality')!!}
                    {!! Form::text('nationality', $employee->user->nationality , array( 'class'=>'form-control b_name',  'id' => 'nationality') )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('email', 'Email')!!}
                    {!! Form::text('email', $employee->user->email , array('class'=>'form-control email',  'id' => 'email')  )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('ref_pk', 'Reference in Pakistan (In case of foreigner)') !!}
                        {!! Form::text('ref_pk', $employee->user->ref_pk , array('class'=>'form-control'  , 'id' => 'ref_pk')  )!!}
                    </div>
                </div>
                <div class="col-sm-4">

                    <div class="form-group">
                    {!! Form::label('permanent_address', 'Permanent Address')!!}
                    {!! Form::text('permanent_address', $employee->user->permanent_address , array( 'class'=>'form-control') )!!}
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('current_address', 'Current Address')!!}
                    {!! Form::text('current_address', $employee->user->current_address , array( 'class'=>'form-control') )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('dob', 'Date of Birth')!!}
                        {!! Form::text('dateofbirth', date('d/m/Y', strtotime($employee->user->dateofbirth)),  array( 'class'=>'form-control datepicker') )!!}
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('passport', 'Passport No')!!}
                        {!! Form::text('passport', $employee->user->passport , array( 'class'=>'form-control ac') )!!}
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('phone_rec', 'Phone Res. #')!!}
                        {!! Form::text('phone_rec', $employee->user->phone_res , array( 'class'=>'form-control' , 'id' => 'phone_rec') )!!}
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('phone_mobile', 'Mobile #')!!}
                        {!! Form::text('phone_mobile', $employee->user->phone_mobile , array( 'class'=>'form-control' , 'id' => 'phone_mobile') )!!}
                        </div>
                    </div>

                      

                    {{-- @include("admin.member-registration._form")  --}}
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Appointment Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('department', 'department', ['class'=>'control-label']) !!}
                            <?php 
                                $countriesArr = [];
                                $departmentArr[''] = 'Select One';
                                foreach ($departments as $department) {
                                    $departmentArr[$department->id] = $department->name;
                                }
                            ?>
                            {!! Form::select('department_id', $departmentArr, $employee->department_id, ['class'=>'form-control', 'id' => 'department']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('designation', 'designation', ['class'=>'control-label']) !!}
                            <?php 
                                $countriesArr = [];
                                $designationArr[''] = 'Select One';
                                foreach ($designations as $designation) {
                                    $designationArr[$designation->id] = $designation->name;
                                }
                            ?>
                            {!! Form::select('designation_id', $designationArr, $employee->designation_id, ['class'=>'form-control', 'id' => 'designation']) !!}
                        </div>

                    </div>


                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('joining_date', 'Joining date', ['class'=>'control-label']) !!}
                            {!! Form::text('joining_date', null,['class'=>'form-control datepicker', 'id' => 'joining_date']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('leaving_date', 'Leaving date', ['class'=>'control-label']) !!}
                            {!! Form::text('leaving_date', null,['class'=>'form-control datepicker', 'id' => 'leaving_date']) !!}
                        </div>

                    </div>


                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('salary', 'Basic salary', ['class'=>'control-label']) !!}
                            {!! Form::text('salary', null,['class'=>'form-control', 'id' => 'salary']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('commission', 'commission', ['class'=>'control-label']) !!}
                            {!! Form::text('commission', null,['class'=>'form-control', 'id' => 'commission']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            {!! Form::label('resume', 'Resume', ['class' => 'col-sm-12'])!!}
                            {!! Form::file('resume', ['class' => 'form-control' , 'id' => 'resume']) !!}
                             <img src="{{ env('STORAGE_PATH') . $employee->resume }}" width="40" height="40">
                        </div>
                    </div>
                    
                   <!--  <div class="col-sm-4">-->

                   <!--     <div class="form-group">-->
                   <!--     {!! Form::label('start_time', 'Start Time')!!}-->
                   <!--         {!! Form::time('start_time', $employee->start_time,['class'=>'form-control timepicker', 'id' => 'start_time']) !!}-->
                   <!--     </div>-->

                   <!-- </div>-->
                   <!-- <div class="col-sm-4">-->

                   <!--     <div class="form-group">-->
                   <!--     {!! Form::label('end_time', 'End Time')!!}-->
                   <!--         {!! Form::time('end_time', $employee->end_time,['class'=>'form-control timepicker', 'id' => 'end_time']) !!}-->
                   <!--     </div>-->

                   <!-- </div>-->

                   <!--   <div class="form-group">-->
                   <!--{!! Form::label('is_restricted', 'Time Restriction?')!!}-->
                   <!--     <div class="checkbox" style="padding-top:3px;">-->
                               
                               <!--{!! Form::checkbox('is_restricted', $employee->is_restricted,['class'=>'form-control', 'id' => 'is_restricted']) !!}-->
                   <!--            <input type="checkbox" name="is_restricted" value="{{$employee->is_restricted}}">-->
                   <!--         </div>-->
                   <!-- </div>-->
                    
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Deductions</h2>
                    <div class="clearfix"></div>
                </div>
            <div class="x_content">
                <div class="row">
                    @if($EmployeeAllownceDeduction)
                        @foreach($EmployeeAllownceDeduction as $deduction)
                            @if($deduction->type == 0)
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {!! Form::label($deduction->label, $deduction->label, ['class'=>'control-label']) !!}
                                        {!! Form::hidden('deduction_label[]', $deduction->label) !!}
                                        {!! Form::text('deduction_amount[]', $deduction->amount,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    <div class="col-sm-4">
                        <p style="margin-top: 25px;"><a href="#" class="addDeduction"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add More</a></p>
                    </div>

                </div>


                <div class="row">
                    <div id="additionalDeductions"></div>
                </div>
            </div>
</div>


            <div class="x_panel">
                <div class="x_title">
                    <h2>Allownces</h2>
                    <div class="clearfix"></div>
                </div>
            <div class="x_content">
                <div class="row">

                    @if($EmployeeAllownceDeduction)
                        @foreach($EmployeeAllownceDeduction as $allownce)
                            @if($allownce->type == 1)
                                <div class="col-sm-4">
                                   <div class="form-group">
                                        {!! Form::label($allownce->label, $allownce->label, ['class'=>'control-label']) !!}
                                        {!! Form::hidden('allownce_label[]', $allownce->label) !!}
                                        {!! Form::text('allownce_amount[]', $allownce->amount ,['class'=>'form-control', 'id' => $allownce->label]) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                     <div class="col-sm-4">
                        <p style="margin-top: 25px;"><a href="#" class="addAllownce"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add More</a></p>
                    </div>

                </div>

                <div class="row">
                    <div id="additionalallownces"></div>
                </div>
            </div></div>
        </div>


            <div class="form-actions">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                <a href="{{ route('employee.index') }}" class="btn btn-grey">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {


        $('.addDeduction').click(function(e){
            e.preventDefault();
            $('#additionalDeductions').append('<div class="col-sm-4"><div class="form-group"><input type="text" name="deduction_label[]" class="form-control" style="margin-bottom: 10px;" placeholder="Deduction Label"><input type="text" name="deduction_amount[]" class="form-control"  placeholder="Deduction Amount"></div></div>');
        });

        $('.addAllownce').click(function(e){
            e.preventDefault();
            $('#additionalallownces').append('<div class="col-sm-4"><div class="form-group"><input type="text" name="allownce_label[]" class="form-control" style="margin-bottom: 10px;" placeholder="Allownce Label"><input type="text" name="allownce_amount[]" class="form-control"  placeholder="Allownce Amount"></div></div>');
        });

        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
     

    </script>
@endsection