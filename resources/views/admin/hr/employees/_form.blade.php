
            <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('name', 'Agent Name') !!}
                {!! Form::text('name', null, ['required' =>'required', 'class' => 'form-control has-feedback-left', 'id' => 'name']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'email']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('password', 'password') !!}
                {!! Form::password('password', ['required' =>'required', 'class' => 'form-control has-feedback-left', 'id' => 'password']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('permanent_address', 'Permanent Address') !!}
                {!! Form::text('permanent_address', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'permanent_address']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('current_address', 'Current Address') !!}
                {!! Form::text('current_address', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'current_address']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('guardian_type', 'S/O Or D/O') !!}
                {!! Form::text('guardian_type', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'guardian_type']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('guardian_name', 'Guardian Name') !!} 
                {!! Form::text('guardian_name', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'guardian_name']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('nationality', 'Nationality') !!} 
                {!! Form::text('nationality', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'nationality']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('city_id', 'City') !!} 
                {!! Form::text('city_id', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'city_id']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('country_id', 'Country') !!} 
                {!! Form::text('country_id', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'country_id']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('passport', 'Passport No') !!} 
                {!! Form::number('passport', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'passport']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('phone_office', 'Phone Office') !!}
                {!! Form::number('phone_office', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'phone_office']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('phone_rec', 'Phone Residential') !!}
                {!! Form::number('phone_rec', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'phone_rec']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('phone_mobile', 'Phone Mobile') !!}
                {!! Form::number('phone_mobile', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'phone_mobile']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('cnic', 'CNIC') !!}
                {!! Form::number('cnic', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'cnic']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('dob', 'Date of Birth')!!}
                {!! Form::text('dob', \Carbon\Carbon::now()->format('d/m/Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('commission', 'Commission') !!}
                {!! Form::number('commission', null, [ 'class' => 'form-control has-feedback-left', 'id' => 'commission']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <?php $personsArr = []; ?>
            <?php $personsArr[''] = 'Choose one'  ?>
            @foreach($persons as $person)
                <?php $personsArr[$person->id] = $person->name;?>
            @endforeach

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('person_id', 'Person Name') !!}
                {!! Form::select('person_id', $personsArr,'' , [ 'class' => 'form-control has-feedback-left', 'id' => 'person_id']) !!}
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                {!! Form::label('photo', 'Photo') !!}
                {!! Form::file('photo', null, [ 'class' => 'form-control has-feedback-right', 'id' => 'photo']) !!}
                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
            </div>

            </div>