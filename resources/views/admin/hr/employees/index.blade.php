@extends('admin.layouts.template')
@section('title','Employee List')
@section('hr-active','active')
@section('employee-head-active','active')
@section('employees-active','active')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Employees List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Employees List</li>
        </ol>
    </div>
    	
    	
    	@if(!empty($message))
    		<p>{{$message}}</p>
    	@endif

        <div class="x_panel">
        <div class="x_title">
               
               
               <h2>Employees List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('employee.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
            <div class="clearfix"></div>
                @include('errors')

    </div>  
        <div class="table-responsive">
    	<table class="table table-bordered table-hover jambo_table bulk_action" id="employeetable">
    		<thead>
    			<tr>
                    <th>Picture</th>
                    <th>Name</th>
                   
                    <th>Email</th>
                    <th>Phone</th> 
                    
                    <th>Status</th>
                    
                    <th class="text-center">Actions</th>
                </tr>
    		</thead>
    		
    		<tbody>
                
    			@foreach($employees as $employee)
          @if($employee->user)
	    			<tr>
	    			  
	    			    <?php 
	    			        $temp = $employee->user->id;
	    			    ?>
                        <td class="text-center"><img src="{{ env('STORAGE_PATH') .$employee->user->picture }}" width="40" height="40" class="img-responsive"></td>
                        <td>{{ $employee->user->name }}</td>
                    
                        <td>{{ $employee->user->email }}</td>
                        <td>{{ $employee->user->phone_mobile }}</td>
                       
                        <td>
                        <?php if($employee->user->is_approved == 0) :?>
                        <a href="employee/approveStatus/{{ $employee->user->id . '/'. $employee->user->is_approved}}" class="btn btn-success btn-xs">Approve</a>
                        <?php else: ?>
                        <a href="employee/approveStatus/{{ $employee->user->id . '/'. $employee->user->is_approved}}" class="btn btn-danger btn-xs">Disapprove</a>
                        <?php endif; ?>
                        </td>
                        @endif
                       
                      <td class="text-center">
                        <a class="btn btn-xs btn-primary viewEmployee" data-toggle="modal" href='#viewEmoloyeeModel' data-employeeid='{{ $temp }}' title="View"><i class="fa fa-eye"></i></a>
                        <a href="{{ route('employee.edit', $temp )}}" class="btn btn-info btn-xs edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['employee.destroy', $temp, 'style' => 'display: inline'] , 'id' => 'table_form']) !!}
                             <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                            {{-- {!! Form::submit("<p>a</p>", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!} --}}
                        {!! Form::close() !!}
                        
                        @if ($employee->resume != "")
                        
                        <a href="{{ route('employee.download', $employee->id) }}" class="btn btn-xs btn-success" target="_blank">Download Resume</a>
                        @else
                        @endif
                         
                        </td>
                    </tr>
	    		@endforeach
    		</tbody>
    		
    	</table>	</div></div></div>


        <div class="modal fade" id="viewEmoloyeeModel">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Employee Detail</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-4">
                      <img id="employeePicture" class="center-block" style="width:150px;">
                  </div>   
                  <div class="col-sm-4">
                      <h4 class="text-center"><span id="employeeName"></span></h4>  
                      {{-- <p>Usename: <span id="username"></span></p> --}}
                      {{-- <p>Created By: <span id="username"></span></p> --}}
                  </div>
                  <div class="col-sm-4">
                      <img id="employeeCNIC" class="center-block" style="width:150px;">
                  </div>
                </div>
                <div class="x_panel" style="margin-top: 20px;">
                    <div class="x_title">
                        <h2>Personal Detail</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        
                        <div class="col-sm-6">CNIC: <span id="cnic"></span></div>
                        <div class="col-sm-6">Nationality: <span id="nationality"></span></div>
                        <div class="col-sm-6">Reference in Pakistan: <span id="ref_pk"></span></div>
                        <div class="col-sm-6">Current Address: <span id="currentAddress"></span></div>
                        <div class="col-sm-6">Permanent Address: <span id="permanentAddress"></span></div>
                        <div class="col-sm-6">Date of Birth: <span id="dob"></span></div>
                        <div class="col-sm-6"><span id="GuardianType"></span> &nbsp <span id="GuardianName"></span></div>
                    </div>
                </div>


                <div class="x_panel" style="margin-top: 20px;">
                    <div class="x_title">
                        <h2>Appointment Information</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <div class="row">
                       
                        <div class="col-sm-6">Department: <span id="department"></span></div>
                        <div class="col-sm-6">Designation: <span id="designation"></span></div>
                        <div class="col-sm-6">Joining Date: <span id="joining_date"></span></div>
                        <div class="col-sm-6">Leaving Date: <span id="leaving_date"></span></div>
                        <div class="col-sm-6">Salary: <span id="salary"></span></div>
                        <div class="col-sm-6">Commission: <span id="commission"></span></div>
                    </div></div>
                </div>

                <div class="x_panel" style="margin-top: 20px;">
                    <div class="x_title">
                        <h2>Deductions</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <div class="row">
                        <div id="Deductions"></div>
                    </div>
                </div></div>


                <div class="x_panel" style="margin-top: 20px;">
                    <div class="x_title">
                        <h2>Allownces</h2>
                        <div class="clearfix"></div>
                    </div>
                     <div class="x_content">
                    <div class="row">
                        <div id="Allownces"></div>
                    </div>
                </div></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
</div>
@endsection

@section('javascript')

    <script type="text/javascript">
        $(document).ready(function() {
            
            var columns = [
            { data: 'picture', name: 'picture'},
            { data: 'name', name: 'name'},
            { data: 'email', name: 'email'},
            { data: 'phone', name: 'phone'},
            { data: 'status', name: 'status'},
            { data: 'actions', name: 'actions'},
            
        ];
        
        // var table = $('#employeetable').DataTable({
        //     "processing": true,
        //     "serverSide": true,
        //     "pageLength": 10,
        //     "info": true,
        //     "ajax": '{!! route('employee.getemployee') !!}',
        //     "columns": columns,
        // });
        


        $('.viewEmployee').on('click', function(){
              $('#GuardianType').html("");
              $('#GuardianName').html("");
              $('#email').html("");
              $('#phone_mobile').html("");
              $('#employeeId').html("");
              $('#ref_pk').html("");
              $('#cnic').html("");
              $('#nationality').html("");
              $('#dob').html("");
              $('#permanentAddress').html("");
              $('#currentAddress').html("");
              $('#employeePicture').html("");
            //   $('#allowncesDeductions').html("");
              
              var employeeId = $(this).data('employeeid');
              
              //alert(employeeId);
              $.ajax({
                url: '{{ url('admin/HR/employee/') }}/'+employeeId,
                type: 'GET'
              })
              .done(function(data) {

                    $('#employeePicture').attr("src" , "{{ env('STORAGE_PATH') }}" + data.employee.user.picture)
                    $('#employeeCNIC').attr("src" , "{{ env('STORAGE_PATH') }}" + data.employee.user.cnic_pic)
                    $('#employeeId').html(data.employee.id);
                    $('#username').html(data.employee.user.username);
                    $('#employeeName').html(data.employee.user.name);
                    $('#GuardianType').html(data.employee.user.guardian_type + ':');
                    $('#GuardianName').html(data.employee.user.gurdian_name);
                    $('#email').html(data.employee.user.email);
                    $('#phone_mobile').html(data.employee.user.phone_mobile);
                    $('#cnic').html(data.employee.user.cnic);
                    $('#nationality').html(data.employee.user.nationality);
                    $('#ref_pk').html(data.employee.user.ref_pk);
                    $('#currentAddress').html(data.employee.user.current_address);
                    $('#permanentAddress').html(data.employee.user.permanent_address);
                    $('#dob').html(data.employee.user.dateofbirth);


                    $('#department').html(data.employee.department.name);
                    $('#designation').html(data.employee.designation.name);
                    $('#joining_date').html(data.employee.joining_date);
                    $('#leaving_date').html(data.employee.leaving_date);
                    $('#commission').html(data.employee.commission);
                    $('#salary').html(data.employee.salary);

                    $('#Deductions').empty();
                    $.each(data.allownceDeduction, function(index, val) {
                        if(val.type== 0)
                        {
                         $('#Deductions').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6"> Rs. '+ val.amount +'</div>');
                        }
                    });

                    $('#Allownces').empty();
                    $.each(data.allownceDeduction, function(index, val) {
                        if(val.type== 1)
                        {
                         $('#Allownces').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6"> Rs. '+ val.amount +'</div>');
                        }
                    });
                    // $.each(data[0].sources, function(index, val) {
                    // $('#employeeSources').append(val.name + ", ");
                    // });
              })
              .fail(function() {
                console.log("error");
              });
              
            });

            $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
    </script>
@endsection
