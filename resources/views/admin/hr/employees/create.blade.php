@extends('admin.layouts.template')
@section('title', 'Add Employee')
@section('hr-active','active')
@section('employee-head-active','active')
@section('employees-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection


@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Employee</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Employee</li>
        </ol>
    </div>
    	

      <div class="x_panel">
        <div class="x_title">
            
            <h2>Add New Employee</h2>
            <div class="clearfix"></div>
          </div>   
        <div class="row"> 
        @include('errors')
        {!! Form::open(['method' => 'POST', 'route' => 'employee.store', 'files' => 'true', 'class' => 'form-horizontal' , 'id' => 'validate_form']) !!}

        <div class="x_panel">
            <div class="x_title">
                <h2>Personal Detail</h2>
                <div class="clearfix"></div>
            </div>
                <div class="col-sm-4">

                <div class="form-group">
                    <label>Name <span style="color: red">*</span></label>
                    {!! Form::text('name', null , array('required' => 'required' , 'class'=>'form-control s_name' , 'id' => 'name')  )!!}
                </div></div>
                 <div class="col-md-4">
                        <div class="form-group">
                            <label>S/O or D/O OR W/O <span style="color: red">*</span></label>
                        <?php 
                            $guardian_typeArr[''] = "";
                            $guardian_typeArr['S/O'] = 'S/O';
                            $guardian_typeArr['D/O'] = 'D/O';
                            $guardian_typeArr['W/O'] = 'W/O';
                        ?>
                        {!! Form::select('guardian_type', $guardian_typeArr, 0, ['class' => 'form-control','required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                        <label>Guardian Name <span style="color: red">*</span></label>
                        {!! Form::text('gurdian_name', null , array( 'class'=>'form-control b_name' , 'id' => 'gurdian_name','required' => 'required') )!!}
                        </div>
                    </div>

                <div class="col-sm-4">
                     <div class="form-group">
                        <label>Picture <span style="color: red">*</span></label>
                        {!! Form::file('picture', ['class' => 'form-control', 'required' => 'required' , 'id' => 'picture']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>CNIC Picture <span style="color: red">*</span></label>
                        {!! Form::file('cnic_pic', ['class' => 'form-control' , 'id' => 'cnic_pic', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    <label>CNIC <span style="color: red">*</span></label>
                    {!! Form::text('cnic', null , array( 'class'=>'form-control', 'required' => 'required' , 'id' => 'cnic' , 'maxlength' => '30') )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    {!! Form::label('nationality', 'Nationality')!!}
                    {!! Form::text('nationality', null , array( 'class'=>'form-control b_name', 'id' => 'nationality') )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    <label>Email <span style="color: red">*</span></label>
                    {!! Form::text('email', null , array('class'=>'form-control email', 'required' => 'required' , 'id' => 'email')  )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('ref_pk', 'Reference in Pakistan (In case of foreigner)') !!}
                        {!! Form::text('ref_pk', null , array('class'=>'form-control'  , 'id' => 'ref_pk')  )!!}
                    </div>
                </div>
                <div class="col-sm-4">

                    <div class="form-group">
                    <label>Permanent Address <span style="color: red">*</span></label>
                    {!! Form::text('permanent_address', null , array( 'class'=>'form-control') )!!}
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                    <label>Current Address <span style="color: red">*</span></label>
                    {!! Form::text('current_address', null , array( 'class'=>'form-control') )!!}
                    </div>
                </div>
                <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('dob', 'Date of Birth')!!}
                        {!! Form::text('dob', \Carbon\Carbon::now()->format('d/m/Y'),  array( 'class'=>'form-control datepicker') )!!}
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('passport', 'Passport No')!!}
                        {!! Form::text('passport', null , array( 'class'=>'form-control ac') )!!}
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('phone_rec', 'Phone Res. #')!!}
                        {!! Form::text('phone_rec', null , array( 'class'=>'form-control' , 'id' => 'phone_rec') )!!}
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                        <label>Mobile # <span style="color: red">*</span></label>
                        {!! Form::text('phone_mobile', null , array( 'class'=>'form-control' , 'id' => 'phone_mobile', 'required' => 'required') )!!}
                        </div>
                    </div>

                       

                
                    {{-- @include("admin.member-registration._form")  --}}
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Appointment Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="col-sm-4">

                        <div class="form-group">
                            <label>Department <span style="color: red">*</span></label>
                           <?php 
                                $countriesArr = [];
                                $departmentArr[''] = 'Select One';
                                foreach ($departments as $department) {
                                    $departmentArr[$department->id] = $department->name;
                                }
                            ?>
                            {!! Form::select('department_id', $departmentArr, 0, ['class'=>'form-control', 'id' => 'department', 'required' => 'required']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            <label>Designation <span style="color: red">*</span></label>
                            <?php 
                                $countriesArr = [];
                                $designationArr[''] = 'Select One';
                                foreach ($designations as $designation) {
                                    $designationArr[$designation->id] = $designation->name;
                                }
                            ?>
                            {!! Form::select('designation_id', $designationArr, 0, ['class'=>'form-control', 'id' => 'designation', 'required' => 'required']) !!}
                        </div>

                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('joining_date', 'Joining date') !!}
                            {!! Form::text('joining_date', null,['class'=>'form-control datepicker', 'id' => 'joining_date']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('leaving_date', 'Leaving date') !!}
                            {!! Form::text('leaving_date', null,['class'=>'form-control datepicker', 'id' => 'leaving_date']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            <label>salary <span style="color: red">*</span></label>

                            {!! Form::text('salary', null,['class'=>'form-control', 'id' => 'salary', 'required' => 'required']) !!}
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('commission', 'commission') !!}
                            {!! Form::text('commission', null,['class'=>'form-control', 'id' => 'commission']) !!}
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Payment Type <span style="color: red">*</span></label>
                        <?php 
                            $payment_typeArr[''] = "";
                            $payment_typeArr['Online'] = 'Online';
                            $payment_typeArr['Cash'] = 'Cash';
                            
                        ?>
                        {!! Form::select('payment_type', $payment_typeArr, 0, ['class' => 'form-control','required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('acc_number', 'Account Number') !!}
                            {!! Form::text('acc_number', null,['class'=>'form-control', 'id' => 'acc_number']) !!}
                        </div>

                    </div>
                    
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('ntn_number', 'NTN Number') !!}
                            {!! Form::text('ntn_number', null,['class'=>'form-control', 'id' => 'ntn_number']) !!}
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Appointment Status <span style="color: red">*</span></label>
                        <?php 
                            $appointment_statusArr[''] = "";
                            $appointment_statusArr['Permanent'] = 'Permanent';
                            $appointment_statusArr['Contract'] = 'Contract';
                            $appointment_statusArr['Internship'] = 'Internship';
                            
                        ?>
                        {!! Form::select('appointment_status', $appointment_statusArr, 0, ['class' => 'form-control','required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                            {!! Form::label('resume', 'Resume')!!}
                            {!! Form::file('resume', ['class' => 'form-control' , 'id' => 'resume']) !!}
                        </div>
                    </div>
                    
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('start_time', 'Start Time')!!}
                            {!! Form::time('start_time', null,['class'=>'form-control timepicker', 'id' => 'start_time']) !!}
                        </div>

                    </div>
                    <div class="col-sm-4">

                        <div class="form-group">
                        {!! Form::label('end_time', 'End Time')!!}
                            {!! Form::time('end_time', null,['class'=>'form-control timepicker', 'id' => 'end_time']) !!}
                        </div>

                    </div>
                      <div class="col-sm-4">      

                    <div class="form-group">
                   {!! Form::label('is_restricted', 'Time Restriction?')!!}
                        <div class="checkbox" style="padding-top:3px;">
                               
                                    <input type="checkbox" value="1" name="is_restricted" id="is_restricted">
                                    
                                
                            </div>
                    </div>
                </div>  
                
                    <div class="col-sm-4">      

                    <div class="form-group">
                   {!! Form::label('can_report', 'Can other employees report to him?')!!}
                        <div class="checkbox" style="padding-top:3px;">
                               
                                    <input type="checkbox" value="1" name="can_report" id="can_report">
                                    
                                
                            </div>
                    </div>
                </div>  
            </div>

            <div class="x_panel">
                <div class="x_title" style="margin-bottom: 0;">
                    <h2 style="margin-bottom: 0;">Deductions</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <p style="margin-top: 7px;margin-left: 5px;"><a href="#" class="addDeduction"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New</a></p>
                    </div>

                </div>


                <div class="row">
                    <div id="additionalDeductions"></div>
                </div>
            </div>



            <div class="x_panel">
                <div class="x_title" style="margin-bottom: 0;">
                    <h2 style="margin-bottom: 0;">Allownces</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="row">

                     <div class="col-sm-4">
                        <p style="margin-top: 7px;margin-left: 5px;"><a href="#" class="addAllownce"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New</a></p>
                    </div>

                </div>


                <div class="row">
                    <div id="additionalallownces"></div>
                </div>

                </div>
                </div>


        
            <div class="form-actions">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                <a href="{{ route('employee.index') }}" class="btn btn-grey">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {


        $('.addDeduction').click(function(e){
            e.preventDefault();
            $('#additionalDeductions').append('<div class="col-sm-4"><div class="form-group"><input type="text" name="deduction_label[]" class="form-control" style="margin-bottom: 10px;" placeholder="Deduction Label"><input type="text" name="deduction_amount[]" class="form-control"  placeholder="Deduction Amount"></div></div>');
        });

        $('.addAllownce').click(function(e){
            e.preventDefault();
            $('#additionalallownces').append('<div class="col-sm-4"><div class="form-group"><input type="text" name="allownce_label[]" class="form-control" style="margin-bottom: 10px;" placeholder="Allownce Label"><input type="text" name="allownce_amount[]" class="form-control"  placeholder="Allownce Amount"></div></div>');
        });

        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

            $('#country').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: "{{ route('admin.state') }}",
                    type: 'POST',
                    data: {country: $(this).val()},
                })
                .done(function(data) {
                    $('#states').html(data.states);
                })
                .fail(function() {
                    console.log("error");
                });    
            });

            $(document).on('change','#state',function(e){
                e.preventDefault();
                console.log($(this).val());
                $.ajax({
                    url: "{{ route('admin.city') }}",
                    type: 'POST',
                    data: {state: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#cities').html(data.cities);
                })
                .fail(function() {
                    console.log("error");
                });    
            });
        });

        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        
        $('.timepicker').timepicker({
            timeFormat: 'hh/mm/ss'
        });
     

    </script>
@endsection