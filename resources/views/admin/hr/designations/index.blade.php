@extends('admin.layouts.template')
@section('title','Designation')
@section('hr-active','active')
@section('employee-head-active','active')
@section('designation-active','active')
@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Designation List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Designation List</li>
        </ol>
    </div>

<div class="row">
        <div class="col-md-12">

        <div class="x_panel">
        <div class="x_title">
               
               
               <h2>Designation List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('designation.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
            <div class="clearfix"></div>
                @include('errors')

        </div>
    
    	<div class="table-responsive">
         <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
    		<thead>
    			<tr>
    				<th>ID</th>
    				<th>Designation Name</th>
    				
    				<th class="text-center">Actions</th>
    			</tr>
    		</thead>
    		<tbody>
                @php($i = 1)
    			@foreach($designations as $designation)
	    			<tr>
	    				<td>{{ $i++ }}</td>
                        <td>{{ $designation->name }}</td>
	    				<td class="text-center"><a href="{{ route('designation.edit', $designation->id )}}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
	    				
                        {!! Form::open(['method' => 'DELETE', 'route' => ['designation.destroy', $designation->id] , 'id' => 'table_form']) !!}
                            <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                            {{-- {!! Form::submit("<p>a</p>", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!} --}}
                        {!! Form::close() !!}
	    				</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
        </div>
    </div></div>
</div>	
@endsection