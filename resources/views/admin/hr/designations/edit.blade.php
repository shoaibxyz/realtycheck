@extends('admin.layouts.template')
@section('title','Edit Designations')
@section('hr-active','active')
@section('employee-head-active','active')
@section('designation-active','active')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Designation</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Designation</li>
        </ol>
    </div>
     <div class="x_panel">
        <div class="x_title">
                <h2>Edit Designation</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">      

            {!! Form::model($designation, ['method' => 'PATCH', 'route' => ['designation.update', $designation->id] , 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label'] ) !!}
                    <div class="col-sm-9"> 
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
                    </div>
                </div>

                <div class="form-actions">
                    {!! Form::hidden('_method', 'PATCH') !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('designation.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>

@endsection