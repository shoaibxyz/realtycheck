@extends('admin.template')
@section('title','Dashboard')

@section('style')
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" />
@endsection

@section('content')

{{-- {!! Breadcrumbs::render('hrdashboard') !!} --}}
<!-- top tiles -->
          
          <!-- /top tiles -->

          
<div style="height:20px;"></div>

          <!-- dashboard nav -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">

              <div class="metro-nav">
              @if(App\User::getPermission('can_view_department'))   
        <div class="metro-nav-block nav-olive">
          <a data-original-title="" href="{{ route('department.index') }}">
            <i class="fa fa-building"></i>
            <div class="info">Departments</div>
            <div class="status">Departments</div>
          </a>
        </div>
        @endif
        @if(App\User::getPermission('can_view_designation')) 
        <div class="metro-nav-block nav-block-orange">
          <a data-original-title="" href="{{ route('designation.index') }}">
            <i class="fa fa-pencil-square-o"></i>
            <div class="info">Designation</div>
            <div class="status">Designation</div>
          </a>
        </div>
        @endif
        @if(App\User::getPermission('can_view_employee'))
        <div class="metro-nav-block nav-block-yellow">
          <a data-original-title="" href="{{ route('employee.index') }}">
            <i class="fa fa-users"></i>
            <div class="info">Employees</div>
            <div class="status">Employees</div>
          </a>
        </div>
        @endif
        @if(App\User::getPermission('can_view_attendance'))
        <div class="metro-nav-block nav-block-red">
          <a data-original-title="" href="{{ route('attendance.index') }}">
            <i class="fa fa-clipboard"></i>
            <div class="info">Attendence</div>
            <div class="status">Attendence</div>
          </a>
        </div>
        @endif

        @if(App\User::getPermission('can_create_attendance'))
        <div class="metro-nav-block nav-block-green">
          <a data-original-title="" href="{{ route('attendance.create') }}">
            <i class="fa fa-clipboard"></i>
            <div class="info">Create Attendence</div>
            <div class="status">Create Attendence</div>
          </a>
        </div>
        @endif

        
        
      </div>

              
        <div class="metro-nav">

        @if(App\User::getPermission('can_view_payroll'))

        <div class="metro-nav-block nav-light-purple">
          <a data-original-title="" href="{{ route('payslip.create') }}">
            <i class="fa fa-book"></i>
            <div class="info">Generate Payslip</div>
            <div class="status">Generate Payslip</div>
          </a>
        </div>
        
        <div class="metro-nav-block nav-light-blue ">
          <a data-original-title="" href="{{ route('payslip.checkPayslip') }}">
            <i class="fa fa-file"></i>
            <div class="info">Payroll Summary</div>
            <div class="status">Payroll Summary</div>
          </a>
        </div>
        <div class="metro-nav-block nav-light-green">
          <a data-original-title="" href="{{ route('advance-salary.index') }}">
            <i class="fa fa-credit-card"></i>
            <div class="info">Advance Salary</div>
            <div class="status">Advance Salary</div>
          </a>
        </div>
        @endif
        @if(App\User::getPermission('can_view_tax'))
        <div class="metro-nav-block nav-light-brown">
          <a data-original-title="" href="{{ route('tax.index') }}">
            <i class="fa fa-align-justify"></i>
            <div class="info">Tax</div>
            <div class="status">Tax</div>
          </a>
        </div>
        @endif
        @if(App\User::getPermission('can_view_leave'))
        <div class="metro-nav-block nav-block-grey">
          <a data-original-title="" href="{{ route('leave.index') }}">
            <i class="fa fa-th-list"></i>
            <div class="info">Leave Types</div>
            <div class="status">Leave Types</div>
          </a>
        </div>
        @endif
      </div>

       <div class="metro-nav">

      @if(App\User::getPermission('can_view_leave_request'))
        <div class="metro-nav-block nav-block-green">
          <a data-original-title="" href="{{ route('leave-request.index') }}">
            <i class="fa fa-clipboard"></i>
            <div class="info">Leaves</div>
            <div class="status">Leaves</div>
          </a>
        </div>
        @endif
        
        
       
        
        
        
        
      </div> 
</div></div>
              </div>
          </div>



          
        
          
@endsection


