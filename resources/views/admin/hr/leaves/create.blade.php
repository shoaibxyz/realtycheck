@extends('admin.layouts.template')
@section('title','Add Leave')
@section('hr-active','active')
@section('leave-active','active')
@section('leaves-active','active')
@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Leave Type</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Leave Type</li>
        </ol>
    </div>
    

        <div class="x_panel">
        <div class="x_title">
                <h2>Add New Leave Type</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">  

        {!! Form::open(['method' => 'POST', 'route' => 'leave.store', 'class' => 'form-horizontal']) !!}

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Leave Name </label>
                <div class="col-sm-8">
                
                {!! Form::text('name', null ,['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Leaves </label>
                <div class="col-sm-8">
                
                {!! Form::text('leaves', null, ['class' => 'form-control', 'id' => 'leaves', 'required' => 'required']) !!} 
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/leave') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function($) {
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
             });
            $('#department_id').change(function(){
                $.ajax({
                    url: '{{ route('getEmployeeByDepartment') }}',
                    type: 'POST',
                    data: {department_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#employees').html(data.employees);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        });
       
    </script>
@endsection