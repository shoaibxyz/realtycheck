@extends('admin.layouts.template')
@section('title','Edit Leave')

@section('hr-active','active')
@section('leave-active','active')
@section('leaves-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Leave</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Leave</li>
        </ol>
    </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Leave Type</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="row">
    @include('errors')
       <div class="col-sm-6 col-sm-offset-3" style="margin: auto; float: none; padding: 20px;">   

        {!! Form::model($leave, ['method' => 'PATCH', 'route' => ['leave.update', $leave->id], 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Leave Name </label>
                <div class="col-sm-8">
                {!! Form::text('name', null ,['class' => 'form-control', 'id' => 'name']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Leaves </label>
                <div class="col-sm-8">
                {!! Form::text('leaves', null, ['class' => 'form-control', 'id' => 'leaves']) !!} 
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/leave') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
