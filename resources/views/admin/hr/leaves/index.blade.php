@extends('admin.layouts.template')
@section('title','Leave Management')
@section('hr-active','active')
@section('leave-active','active')
@section('leaves-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">All Leave Types</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">All Leave Types</li>
        </ol>
    </div>
    


        <div class="x_panel">
        <div class="x_title">
            
            <h2>All Leave Types</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leave.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
               
            <div class="clearfix"></div>
                @include('errors')

    </div>
    <div class="row">
    	<div class="table-responsive">
        <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
    		<thead>
    			<tr>
                    <th>Name</th>
                    <th>Leaves</th>
                    <th class="text-center">Actions</th>
    			</tr>
    		</thead>
    		<tbody>
                @php($i = 1)
    			@foreach($leaves as $leave)
	    			<tr>
	    				
                        <td>{{ $leave->name }}</td>
                        <td>{{ $leave->leaves }}</td>
                        <td class="text-center">
                            <a href="{{ route('leave.edit', $leave->id )}}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                            <form method="post" action="{{ route('leave.destroy', $leave->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'> <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button></form>  

	    				</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
        </div>
    </div>
</div>	
@endsection