@extends('admin.template')
@section('title','Edit Department')
@section('hr', 'active')
@section('hr_menu', 'display:block')
@section('designations', 'active')
@section('designation_menu', 'display:block')
@section('designation-all', 'current-page')


@section('content')
<h2>Edit Payroll</h2>    
        <div class="x_panel">
        <div class="x_title">
           
            <div class="clearfix">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">      
                
        <h2>Edit Department</h2>    
        <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::model($designation, ['method' => 'PATCH', 'route' => ['designation.update', $designation->id] , 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Department Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::hidden('_method', 'PATCH') !!}
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
        {!! Form::close() !!}
        </div>

@endsection