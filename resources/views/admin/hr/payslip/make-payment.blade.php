@extends('admin.layouts.template')
@section('title','Make Payment')
@section('hr', 'active')
@section('hr_menu', 'display:block')
@section('payslips', 'active')
@section('payslip_menu', 'display:block')
@section('payslip-add', 'current-page')
@section('hr-active','active')
@section('payroll-active','active')
@section('payroll-summary-active','active')


@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        .ui-datepicker-calendar {
        display: none;
        }
    </style>
@endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Make Payment</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Make Payment</li>
        </ol>
    </div>
        <div class="x_panel">
        <div class="x_title">
                <h2>Make Payment</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'GET', 'route' => 'payslip.generatePayslip', 'class' => 'form-horizontal']) !!}

        <div class="row">

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, $department->id ,['class' => 'form-control', 'id' => 'department_id']) !!} 
                </div>
            </div>

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('Employee', 'Employee') !!}
                    {!! Form::select('employee_id', $employeeList, $employee->id ,['class' => 'form-control', 'id' => 'employees']) !!} 
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('forMonth', 'For Month') !!}
                    <input name="forMonth" id="forMonth" class="date-picker form-control" value="{{ $date}}" />
                </div>
            </div>
        
            <div class="col-sm-3">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        
    </div>


    <div class="x_panel">
        <div class="x_title">
                <h2>Allowances & Deductions</h2>   
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                 <div class="x_panel">
                    <div class="x_title">
                        <h2>Allowances</h2>   
                        <div class="clearfix"></div>
                    </div>
                    @php($total_allowances = 0)
                    @foreach($allownceDeduction as $allowance)
                        @if($allowance->type == 1)
                            <div class="col-sm-6">
                                {{ $allowance->label }} 
                            </div>
                            <div class="col-sm-6">
                                {{ $allowance->amount }}
                                @php($total_allowances += $allowance->amount)
                            </div>
                        @endif
                    @endforeach
                </div>    
            </div>

            <div class="col-sm-6">
                 <div class="x_panel">
                    <div class="x_title">
                        <h2>Deductions</h2>   
                        <div class="clearfix"></div>
                    </div>

                     @php($total_deductions = 0)
                    @foreach($allownceDeduction as $deductions)
                        @if($deductions->type == 0)
                            <div class="col-sm-6">
                                {{ $deductions->label }} 
                            </div>
                            <div class="col-sm-6">
                                {{ $deductions->amount }}
                                @php($total_deductions += $deductions->amount)
                            </div>
                        @endif
                    @endforeach
                </div>    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="x_panel">
                <div class="col-sm-6">    
                    <div class="x_title">
                        <h2>Leave Requests</h2>   
                        <div class="clearfix"></div>
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Days</th>
                                <th>Reason</th>
                                <th>Type</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($leaveArr = [])
                        @foreach($leaveRequests as $request)
                            <tr>
                                @php($days = $request->leave_end_date->diffInDays($request->leave_start_date))
                                <td>{{  $days }} </td>
                                <td>{{ $request->reason}}</td>
                                <td>{{ $request->leave->name }}</td>
                                <?php 
                                    if($request->status == 1)
                                        $label = "label-success";
                                    if($request->status == 2)
                                        $label = "label-info";
                                    if($request->status == 3)
                                        $label = "label-danger";
                                ?>
                                <td><span class="label {{ $label }}">{{$request->status}}</span></td>
                                @php($leaveArr[$request->leave->name][] = $days)
                            </tr>
                      
                        @endforeach
                          </tbody>
                    </table>

                    {{-- @php(dd($leaveArr)) --}}

{{--                     <div class="col-sm-6">    
                            <div class="x_title">
                                <h2>Remaining Leaves</h2>   
                                <div class="clearfix"></div>
                            </div>
                        @foreach($leaveArr as $key => $arr)

                            <div class="col-sm-6">
                            {{ $key }} 
                            </div>
                            <div class="col-sm-6">
                                @php($remainingLeaves = 0)
                                @foreach($arr as $ar)
                                    @php($remainingLeaves += $ar)
                                @endforeach
                                {{ $remainingLeaves }}
                            </div>
                        @endforeach
                        </div> --}}

                </div>

                <div class="col-sm-6">    
                    <div class="x_title">
                        <h2>Leaves</h2>   
                        <div class="clearfix"></div>
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Available</th>
                                <th>Used</th>
                                <th>Remaining</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($countLeave = 0)
                            @php($usedLeaves = 0)
                            @php($countLeave = 0)
                            @foreach($leaves as $leave)
                                <tr>
                                    <td> {{ $leave->name }} </td>
                                    <td> {{ $leave->leaves }} </td>
                                    @if(isset($leaveArr[$leave->name]))
                                        @php($remainingLeaves = 0)
                                        @foreach($leaveArr[$leave->name] as $ar)
                                            @php($remainingLeaves += $ar)
                                        @endforeach
                                        <td> {{ $remainingLeaves }} </td>
                                        @php($usedLeaves += $remainingLeaves)
                                    @else
                                        <td>0</td>
                                    @endif
                                    <td>
                                        
                                    </td>
                                </tr>
                                    @php($countLeave += $leave->leaves)
                            @endforeach
                                <tr>
                                    <td>Total</td>
                                    <td>{{ $countLeave }}</td>
                                    <td>{{ $usedLeaves }}</td>
                                </tr>
                        </tbody>

                    </table>
                </div>
    
            </div>    
        </div>
    </div>


    <div class="x_panel">
        <div class="x_title">
                <h2>Summary</h2>   
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                {!! Form::open(['method' => 'POST', 'route' => 'payslip.store', 'class' => 'form-horizontal']) !!}
                
                <div class="form-group">
                    {!! Form::label('fine', 'Fine', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('fine', null, ['class' => 'form-control', 'id' => 'fine']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Fine Reason', 'Fine Reason', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('fine_reason', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                  <div class="form-group">
                    {!! Form::label('bonus', 'Bonus', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('bonus', null, ['class' => 'form-control', 'id' => 'bonus']) !!}
                    </div>
                </div>

                @if($checkAdvanceSalary)
                    <div class="form-group">
                        {!! Form::label('advance_salary', 'Advance Salary', ['class' => 'col-sm-4']) !!}
                        <div class="col-sm-8"> 
                            {!! Form::text('advance_salary', $checkAdvanceSalary->amount, ['class' => 'form-control', 'id' => 'fine', 'readonly' => 'readonly']) !!}
                            {!! Form::hidden('advance_salary_id', $checkAdvanceSalary->id) !!}
                        </div>
                    </div>
                @endif


                <div class="form-group">
                    {!! Form::label('Basic', 'Basic', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('basic_salary', $employee->salary, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'basic_salary']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Tax', 'Tax', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        <?php 
                            $tax = round(($incomeTax + $taxPercentage) / 12);
                        ?>
                        {!! Form::text('tax_amount', $tax , ['class' => 'form-control', 'readonly' => 'readonly' , 'id' => 'tax_amount']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('total_deductions', 'Total Deductions', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('total_deductions', $total_deductions, ['class' => 'form-control', 'readonly' => 'readonly' , 'id' => 'total_deductions']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('total_allowances', 'Total Allowances', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('total_allowances', $total_allowances, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'total_allowances']) !!}
                    </div>
                </div>

                <?php
                    $net_salary = ($employee->salary + $total_allowances) - $total_deductions - $tax;
                    if($checkAdvanceSalary)
                        $net_salary -= ($checkAdvanceSalary->amount - $tax);
                ?>
                <div class="form-group">
                    {!! Form::label('net_salary', 'Net Salary', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('net_salary', $net_salary, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'net_salary']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', 'Status', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        @php($status = [])
                        @php($status[1] = 'Paid')
                        @php($status[0] = 'Unpaid')
                        {!! Form::select('status', $status, 1, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        {!! Form::hidden('employee_id', $employee->id) !!}
                        {!! Form::hidden('department_id', $department->id) !!}
                        {!! Form::hidden('month', $month) !!}
                        {!! Form::hidden('year', $year) !!}
                        {!! Form::submit('Create Payslip', ['class' => 'btn btn-warning']) !!}  
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
        
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
          $(function() {
    $('.date-picker').datepicker(
    {
        dateFormat: "mm-yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {


            function isDonePressed(){
                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
            }

            if (isDonePressed()){
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                
                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
            }
        },
        beforeShow : function(input, inst) {

            inst.dpDiv.addClass('month_year_datepicker')

            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month-1, 1));
                $(".ui-datepicker-calendar").hide();
            }
        }
        })
    });
        $('#fine').blur(function(event) {
            var fine = $(this).val();
            if(fine != "")
            {
                var net_salary = $('#net_salary').val();
                var new_net_salary = parseInt(net_salary) - parseInt(fine);
                $('#net_salary').val(new_net_salary);
            }
            else{
                var basic_salary = $('#basic_salary').val();
                var total_allowances = $('#total_allowances').val();
                var total_deductions = $('#total_deductions').val();

                $('#net_salary').val((parseInt(basic_salary) + parseInt(total_allowances)) - parseInt(total_deductions));   
            }
        });
        
        $('#bonus').blur(function(event) {
            var bonus = $(this).val();
            if(bonus != "")
            {
                var net_salary = $('#net_salary').val();
                var new_net_salary = parseInt(net_salary) + parseInt(bonus);
                $('#net_salary').val(new_net_salary);
            }
            else{
                var basic_salary = $('#basic_salary').val();
                var total_allowances = $('#total_allowances').val();
                var total_deductions = $('#total_deductions').val();

                $('#net_salary').val((parseInt(basic_salary) + parseInt(total_allowances)) - parseInt(total_deductions));   
            }
        });
        
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    </script>
@endsection