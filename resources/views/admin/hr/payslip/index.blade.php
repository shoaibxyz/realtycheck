@extends('admin.layouts.template')
@section('title','Add payslip')
@section('hr-active','active')
@section('payroll-active','active')
@section('payroll-summary-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
   
    <style type="text/css">
        .ui-datepicker-calendar {
        display: none;
        }
        .modal {
            position: fixed;
            top: -28px;
        }
        .table{
            color: #000;
        }
        #td-css{
            font-weight: bold;
        
            width: 150px;
            background-color: whitesmoke;
        }
        #td-css-1{
            font-weight: bold;
            font-size: 14px;
        }
        td {
            font-size: 13px !important;
        }
        .note {
            margin: 183px 0 20px 0;
            padding: 17px 30px 15px 15px;
        }
        .footer-text{
            float:left;
            color: #000;
            font-size: 13px;
            padding-top: 5px;
        }
        .modal-footer {
            padding: 0px;
        }
        #footer-button{
            margin-top: 3px;
        }
        .modal-open .modal{
            /*overflow-y: hidden;*/
        }
        @media (min-width: 768px){
        .modal-dialog {
            width: 1170px;
            
        }
        
    .panel{
        margin-bottom: 0px;
        margin-top: -5px;
    }
    .panel-heading {
    font-size: 14px;
    }
    .panel-heading {
    padding: 3px 0px;
    text-align: -webkit-center;
    }
        }
     

    </style>
    
<style type="text/css" media="print">

@page {
    size: auto; 
    size: A4 landscape;
    margin: 0 0px 0 250px;
    border: 1px solid red;
  }

.table{
            color: #000 !important;
        }
        #td-css{
            font-weight: bold ;
        
            width: 179px;
            background-color: whitesmoke !important;
        }
        #td-css-1{
            font-weight: bold;
            font-size: 13px !important;
        }
        td {
            font-size: 12px !important;
        }
        .note {
            margin: 183px 0 20px 0;
            padding: 17px 30px 15px 15px;
        }
        .footer-text{
            float:left;
            color: #000;
            font-size: 12px !important;
            padding-top: 5px;
        }
        
        .panel{
        margin-bottom: 0px;
        margin-top: -5px;
    }
    .panel-heading {
    font-size: 13px;
    }
    .panel-heading {
    padding: 3px 0px;
    text-align: -webkit-center;
    }
#footer-button{display:none;} 


</style>
@endsection

@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Payslip List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Payslip List</li>
        </ol>
    </div>
  
        <div class="x_panel">
        <div class="x_title">
            
            <h2>Payslip List</h2> 
                
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'GET', 'route' => 'payslip.checkPayslip', 'class' => 'form-horizontal']) !!}

        <div class="row">

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, $department_id ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('forMonth', 'For Month') !!}
                    <input name="forMonth" id="forMonth" class="date-picker form-control" value="{{ $month.'-'.$year}}" required/>
                </div>
            </div>
        
            <div class="col-sm-3">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if(isset($payslip))
                <table class="table table-bordered table-hover jambo_table" id="datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Code</th>
                            <th>Department</th>
                            <th>Employee</th>
                            <th>Net Salary</th>
                            <th>Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($payslip as $slip)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $slip->code}}</td>
                            <td>{{ $slip->department_name}}</td>
                            <td>{{ $slip->username}}</td>
                            <td>
                               {{--  <p>Basic Salary: {{ $slip->basic_salary}}</p>
                                <p>Total Deductions: {{ $slip->total_deductions}}</p>
                                <p>Total Allowances: {{ $slip->total_allowances}}</p>
                                <p>Tax: {{ $slip->tax_amount or  0}}</p>
                                @if($slip->fine != "")
                                    <p>Fine: {{ $slip->fine}}</p>
                                @endif
                                <hr> --}}
                                {{ $slip->net_salary}}
                            </td>
                            <td>{{ date("F", mktime(0, 0, 0, $slip->month, 10)). ",".($slip->year) }}</td>
                            <td class="text-center"><span class="label {{ ($slip->status == 1) ? 'label-success' : 'label-danger' }}">{{ ($slip->status == 1) ? "Paid" : "Unpaid"  }}</span></td>
                            <td class="text-center"><a class="btn btn-xs btn-primary viewSlip" data-toggle="modal" href='#viewSlipModal' data-payslip_id='{{ $slip->id }}' title="View"><i class="fa fa-eye"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="modal fade" id="viewSlipModal">
          <div class="modal-dialog modal-md" style="width:1100px;">
            <div class="modal-content">
              <!--<div class="modal-header">-->
              <!--  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                
              <!--</div>-->
              <div class="modal-body">

              <div class="row">
                  
                  
                  <div class="col-sm-6"><div class="slip_logo">
                          <img src="{{ asset('public/') }}/images/pmc-web-logo.png">

                      </div></div>
                  <div class="col-sm-6"><div class="slip_heading">
                      <h4>PAYSLIP FOR THE MONTH OF {<span id="slip_month"></span>, <span id="slip_year"></span>}</h4>
                  </div></div>
                </div>
                
                
<div class="row">
    <div class="col-sm-4">
        
        <table class="table table-bordered" id="datatable">
    
        <tr>
            <td id="td-css">Emp. No.</td>
            <td id="code"></td>
        </tr>
        <tr>
            <td id="td-css">Name</td>
            <td id="employeeName">ABC</td>
        </tr>
        <tr>
            <td id="td-css">Designation</td>
            <td id="designation"></td>
        </tr>
        <tr>
            <td id="td-css">Department</td>
            <td id="department"></td>
        </tr>
            
        </table>
        
        <table class="table table-bordered" id="datatable">
        <div class="panel panel-default">
            <div class="panel-heading">Allownces</div>
        </div>
          <thead>
      <tr>
        <th id="td-css-1">Fix Pay</th>
        <th id="td-css-1">For The Month</th>
        <th id="td-css-1">Y.T.D</th>
        
      </tr>
    </thead>
        <tbody id="Allownces"></tbody>
        <tfoot>
        <tr>
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading">Sub Total</div>
                </div>
            </td>
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading" id="total_allowances"></div>
                </div>
            </td>
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading" id="total_allowances1"></div>
                </div>
            </td>
            
        </tr></tfoot>
            
        </table>
        
    </div>
    <div class="col-sm-4">
        
        <table class="table table-bordered" id="datatable">
    
        <tr>
            <td id="td-css">Employment Status</td>
            <td id="appointment_status"></td>
        </tr>
        <tr>
            <td id="td-css">Joining Date</td>
            <td id="joining_date">Dooley</td>
        </tr>
        <tr>
            <td id="td-css">Confirmation Date</td>
            <td id="confirm_date"></td>
        </tr>
        <tr>
            <td id="td-css">Salary Days</td>
            <td id="salary_days"></td>
        </tr>
            
        </table>
        
        
        <table class="table table-bordered" id="datatable">
        <div class="panel panel-default">
            <div class="panel-heading">DEDUCTIONS</div>
        </div>
          <thead>
      <tr>
        <th class="td-css-1">Items</th>
        <th class="td-css-1">For The Month</th>
        <th class="td-css-1">Y.T.D</th>
      </tr>
    </thead>
    
    <tbody id="Deductions"></tbody>
        
       <tfoot> <tr>
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading">Sub Total</div>
                </div>
            </td>
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading" id="total_deductions"></div>
                </div>
            </td>
            
            <td>
                <div class="panel panel-default">
                    <div class="panel-heading" id="total_deductions1"></div>
                </div>
            </td>
            
        </tr></tfoot>
            
        </table>
        
        <table class="table table-bordered" id="datatable">
        <div class="panel panel-default">
            <div class="panel-heading">Other DEDUCTIONS</div>
        </div>
          <thead>
      <tr>
        <th class="td-css-1">Items</th>
        <th class="td-css-1">For The Month</th>
        
      </tr>
    </thead>
    
    <tbody>
        <tr>
            <td>Fine</td>
            <td id="fine"></td>
        </tr>
        
          <tr>
            <td>Bonus</td>
            <td id="bonus"></td>
        </tr>
        
        <tr>
            <td>Tax</td>
            <td id="tax"></td>
        </tr>
     
     <tr>
            <td>Advanced Salary</td>
            <td id="advance"></td>
        </tr>
        
    </tbody>
        
       
            
        </table>
        
    </div>
    <div class="col-sm-4">
        
        <table class="table table-bordered" id="datatable">
    
        <tr>
            <td id="td-css">Date of Payment</td>
            <td id=""></td>
        </tr>
        <tr>
            <td id="td-css">Account No.</td>
            <td id="acc_no"></td>
        </tr>
        <tr>
            <td id="td-css">N.T.N.</td>
            <td id="ntn_number"></td>
        </tr>
        <tr>
            <td id="td-css">C.N.I.C</td>
            <td id="cnic"></td>
        </tr>
            
        </table>
        
         <table class="table table-bordered" id="datatable">
        
         
      <tr>
        <th id="td-css-1">Net Salary</th>
        <th id="salary"></th>
    
      </tr>
      <tr>
        <th id="td-css-1">Yearly Net Salary</th>
        <th id="salary1"></th>
    
      </tr>
      
        </table>
        
        <div class="note">
            <p><b>Note: </b> This salary slip is not valid for presentation as proof of salary to any individual/institution until and unless stamped/signed and verified by HRD.</p>
        </div>
        
    </div>
</div>



              <div class="modal-footer salary_footer">
                  <div class="footer-text">
                      * In case of any change/error please inform HRD immediately
                  </div>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="footer-button">Close</button>
                <button type="button" class="btn btn-success view_slip" id="footer-button">Print</button>
              </div>
            </div>
          </div>
        </div>
          </div>
        </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
      $(function() {
             
    $('.date-picker').datepicker(
    {
        dateFormat: "mm-yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {


            function isDonePressed(){
                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > 1);
            }

            if (isDonePressed()){
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                
                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
            }
        },
        beforeShow : function(input, inst) {

            inst.dpDiv.addClass('month_year_datepicker')

            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
                $(".ui-datepicker-calendar").hide();
            }
        }
        })
    });
    
    
    function CallPrint(strid)
                {
                  var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_self';

                  var printWindow = window.open(window.location.href , windowName);
                  printWindow.document.body.innerHTML = printContent.innerHTML;
                  printWindow.document.close();
                  printWindow.focus();
                  printWindow.print();
                  printWindow.close();
                  return false;
                }

                $('.view_slip').on('click',function(){
                  CallPrint('viewSlipModal');
                })
        
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });


        $('.viewSlip').on('click', function(){
              $('#code').html("");
              $('#department').html("");
               $('#designation').html("");
              $('#employeeName').html("");
              $('#slipDate').html("");
              $('#Allownces').html("");
              $('#Deductions').html("");
              $('#appointment_status').html("");
              $('#joining_date').html("");
              $('#confirm_date').html("");
              $('#acc_no').html("");
              $('#ntn_number').html("");
              $('#cnic').html("");
              $('#salary').html("");
              $('#salary_days').html("");
              
            $('#advance').html("");
             $('#fine').html("");
             $('#tax').html("");
             $('#advance').html("");
             
              $('#slip_month').html("");
              
               $('#slip_year').html("");
              
            
              
              var payslip_id = $(this).data('payslip_id');
              $.ajax({
                url: '{{ url('admin/HR/payslip/') }}/'+payslip_id,
                type: 'GET'
              })
              .done(function(data) {
                $('#code').html(data[0].payslip.code);
                $('#employeeName').html(data[0].user.name);
                $('#department').html(data[0].payslip.department.name);
                $('#designation').html(data[0].designation.name);
                $('#appointment_status').html(data[0].employee.appointment_status);
                $('#joining_date').html(data[0].employee.joining_date);
                $('#confirm_date').html(data[0].employee.joining_date);
                $('#acc_no').html(data[0].employee.acc_number);
                $('#ntn_number').html(data[0].employee.ntn_number);
                $('#cnic').html(data[0].user.cnic);
                var slipDate = data[0].payslip.created_at.split('-');
                
                // $('#slipDate').html(data[0].payslip.created_at);
                $('#salary_days').html(data[0].s_days);
               
               var formattedMonth = moment().month(data[0].payslip.month - 1).format('MMMM');
                $('#slip_month').html(formattedMonth);
                $('#slip_year').html(data[0].payslip.year);
                
              
                var deductionSum = 0;
                var deductionSum1 = 0;
                var month = data[0].payslip.month;
                $.each(data[0].allownceDeduction, function(index, val) {
                    if(val.type== 0)
                    {
                        var year_amt = month * val.amount;
                     $('#Deductions').append('<tr><td>'+ val.label +'</td><td>'+ val.amount +'</td><td>'+ year_amt +'</td></tr>');
                     deductionSum += val.amount;
                     deductionSum1 += year_amt;
                    }
                });
                $('#total_deductions').html(deductionSum);
                $('#total_deductions1').html(deductionSum1);

                var allownceSum = 0;
                var allownceSum1 = 0;
                $.each(data[0].allownceDeduction, function(index, val) {
                    if(val.type== 1)
                    {
                     var year_amt1 = month * val.amount;
                     $('#Allownces').append('<tr><td>'+ val.label +'</td><td>'+ val.amount +'</td><td>'+ year_amt1 +'</td></tr>');
                     allownceSum += val.amount;
                     allownceSum1 += year_amt1;
                    }
                });
                $('#total_allowances').html(allownceSum);
                $('#total_allowances1').html(allownceSum1);

                
                 $('#salary').html(data[0].payslip.net_salary);
                 
                 
                 $('#salary1').html(data[0].yearly_salary);
                 
                 $('#fine').html(data[0].payslip.fine);
                 $('#bonus').html(data[0].payslip.bonus);
                 $('#tax').html(data[0].payslip.tax_amount);
                 $('#advance').html(data[0].payslip.advance_salary);
               
               
              })
              .fail(function() {
                console.log("error");
              });
              
            });
            
    
    </script>
@endsection