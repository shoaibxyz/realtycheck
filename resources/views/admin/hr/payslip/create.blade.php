@extends('admin.layouts.template')
@section('title','Add payslip')
@section('hr-active','active')
@section('payroll-active','active')
@section('generate-payslip-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        .ui-datepicker-calendar {
        display: none;
        }
    </style>
@endsection
@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Payslip</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Payslip</li>
        </ol>
    </div>

        <div class="x_panel">
        <div class="x_title">
                <h2>Add New Payslip</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'GET', 'route' => 'payslip.generatePayslip', 'class' => 'form-horizontal']) !!}

        <div class="row">

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, 0 ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('Employee', 'Employee') !!}
                    {!! Form::select('employee_id', ['Select Department'], 0 ,['class' => 'form-control', 'id' => 'employees', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('forMonth', 'For Month') !!}
                    <input name="forMonth" id="forMonth" class="date-picker form-control" required/>
                </div>
            </div>
        
            <div class="col-sm-3">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                </div>
            </div></div>
        {!! Form::close() !!}
        
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });

        $(function() {
            var myDate = new Date();
    $('.date-picker').datepicker(
    {
        dateFormat: "mm-yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        
        })
    });
    </script>
@endsection