@extends('admin.layouts.template')
@section('title','Add Department')
@section('hr-active','active')
@section('employee-head-active','active')
@section('department-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Department</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Department</li>
        </ol>
    </div>


        <div class="x_panel">
        <div class="x_title">
               
               
        <h2>Add New Department</h2>   
                
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">      

            {!! Form::open(['method' => 'POST', 'route' => 'department.store', 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-9"> 
                    {!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
                    </div>
                </div>
            
                <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('department.index') }}" class="btn btn-grey">Cancel</a>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection