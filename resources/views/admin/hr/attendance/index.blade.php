@extends('admin.layouts.template')
@section('title','attendance')
@section('hr-active','active')
@section('employee-head-active','active')
@section('attendance-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Attendance History</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Attendance History</li>
        </ol>
    </div>

 
 
        <div class="x_panel">
        <div class="x_title">
               
               
               <h2>Attendance History</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('attendance.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
            <div class="clearfix"></div>
                @include('errors')

    </div>
    <div class="row">
            {!! Form::open(['method' => 'GET', 'route' => 'attendance.history', 'class' => 'form-horizontal']) !!}
            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, 0 ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('Employee', 'Employee') !!}
                    {!! Form::select('employee_id', ['Select Department'], 0 ,['class' => 'form-control', 'id' => 'employees', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('from', 'from') !!}
                    {!! Form::text('from', null, ['class' => 'form-control datepicker', 'id' => 'month', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('to', 'to') !!}
                    {!! Form::text('to', null, ['class' => 'form-control datepicker', 'id' => 'to', 'required' => 'required']) !!}
                </div>
            </div>
        
            <div class="col-sm-2">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                </div>
            </div>
            {{ csrf_field() }} 
        {!! Form::close() !!}
        
    </div>

    @if(isset($attendance))
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table-hover" id="attendanceTable">
                <thead>
                    <tr>
                        <th>Employee Id</th>
                        <th>Name</th>
                        <th>Clock In / Out</th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Remarks</th>
                        <th>Marked By</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($attendance as $attend)
                    <tr>
                        <td>{{ $attend->employee->id }}</td>
                        <td>{{ $attend->employee->user->name}}</td>
                        <td>{{ ($attend->type == 1) ? "Clock in": "Clock out"}}</td>
                        <td>{{ $attend->clock_in_out->format('d-m-y H:i:s')}}</td>
                        <td>{{ $attend->formatted_address }}</td>
                        <td>{{ $attend->remarks}}</td>
                        <td>{{ App\User::getUserInfo($attend->created_by)->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
</div>	
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.32/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.32/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        $('.datepicker').datepicker({
            dateFormat: 'dd-mm-yy',
        });
       


        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    </script>
@endsection