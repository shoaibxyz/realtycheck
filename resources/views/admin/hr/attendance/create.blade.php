@extends('admin.layouts.template')
@section('title','Add attendance')
@section('hr-active','active')
@section('employee-head-active','active')
@section('attendance-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Attendance</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Attendance</li>
        </ol>
    </div>

 
        <div class="x_panel">
        <div class="x_title">
                <h2>Add Attendance</h2>  
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="col-sm-12">      
         {!! Form::open(['method' => 'POST', 'route' => 'attendance.store', 'class' => 'form-horizontal']) !!}

        <table class="table table-bordered table-hover table-stripped" id="datatable22">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>Department</th>
                    <th>Employee</th>
                    <th>Designation</th>
                    <th>Clock In</th>
                    <th>Clock Out</th>
                    <th>Last Activity</th>
                </tr>
            </thead>
            <tbody>
                @php($i = 1)
                @foreach($employees as $employee)
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->department->name}}</td>
                    <td>{{ $employee->user->name }}</td>
                    <td>{{ $employee->designation->name}}</td>
                    <td>
                        <?php 
                            $checkAttendance = App\Attendance::getEmployeeAttendance($employee->id);
                            $clockIn_disabled = "";
                            if(isset($checkAttendance) && $checkAttendance->type == 1)
                                $clockIn_disabled = "disabled='disabled'";
                        ?>
                        <button class="btn btn-xs btn-success employeeModal" data-employee="{{ $employee->id }}" data-toggle="modal" id="employee_{{ $employee->id }}_1" data-status="1" href='#attendanceModel' {{ $clockIn_disabled }} >Clock In</button></td>
                    <td>
                        <?php 
                            $clockOut_disabled = "";
                            if(isset($checkAttendance) && $checkAttendance->type == 0)
                                $clockOut_disabled = "disabled='disabled'";
                            // else
                                // $clockOut_disabled = "disabled";
                        ?>
                        <button class="btn btn-xs btn-warning employeeModal" data-toggle="modal" data-employee="{{ $employee->id }}" id="employee_{{ $employee->id }}_0" data-status="0" @if(!isset($checkAttendance)) disabled @endif  href='#attendanceModel' {{ $clockOut_disabled }}>Clock Out</button></td>
                    <td id="employee_{{ $employee->id }}_activity">
                        @if(isset($checkAttendance))
                            {{ ($checkAttendance->type == 0) ? "Clock Out At: " : "Clock In At: " }}
                            {{ $checkAttendance->clock_in_out->format('d-m-y  H:i:s') }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! Form::close() !!}
        </div>
    </div>

    <div class="modal fade" id="attendanceModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Attendance Remakrs <small>(Optional)</small></h4>
                </div>
                <div class="modal-body">
                    {!! Form::label('remarks', 'Remarks') !!}
                    {!! Form::textarea('remarks', '', ['id' => 'remarks', 'class' => 'form-control']) !!}
                    {!! Form::hidden('attendance_id', null, ['id' => 'attendance_id']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="addRemarks" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        $(document).ready(function(){
        
        
        
        function handle_errors(error)
        {
            switch(error.code)
            {
                case error.PERMISSION_DENIED: alert("user did not share geolocation data");
                break;
 
                case error.POSITION_UNAVAILABLE: alert("could not detect current position");
                break;
 
                case error.TIMEOUT: alert("retrieving position timed out");
                break;
 
                default: alert("unknown error");
                break;
            }
        }
 
        function handle_geolocation_query(position){
            $.ajax({
                url: '{{ route('attendance.storeLocation') }}',
                type: 'POST',
                data: {lat: position.coords.latitude, lng: position.coords.longitude, attendance_id : $('#attendance_id').val()},
            })
            .done(function(data) {
                console.log(data);
            })
            .fail(function(xhr, status, responseText) {
                console.log(responseText);
            });
            //position.coords.latitude
            //position.coords.longitude
        }

        $('.employeeModal').click(function(e){
            e.preventDefault();
            var employee = $(this).data('employee');
            var attendanceStatus = $(this).data('status');
            $('#remarks').val('');
            $.ajax({
                url: '{{ route('attendance.store') }}',
                type: 'POST',
                data: {'attendanceStatus': attendanceStatus, 'employee' : employee},
            })
            .done(function(data) {
                if(data.status == 1)
                {
                    $('#attendance_id').val(data.attendance.id);
                    // navigator.geolocation.getCurrentPosition(handle_geolocation_query,handle_errors);
                    var status = (attendanceStatus == 1) ? 0 : 1;
                    $('#employee_'+employee+'_'+attendanceStatus).attr('disabled', 'disabled');
                    $('#employee_'+employee+'_'+status).removeAttr('disabled');
                    if(attendanceStatus == 1)
                        $('#employee_'+employee+'_activity').html("Clock in At: " + data.attendance.clock_in_out);
                    else
                        $('#employee_'+employee+'_activity').html("Clock out At: " + data.attendance.clock_in_out);
                    swal("success", "Attendance is marked", 'success');
                }
            })
            .fail(function() {
                swal("Oops!","Something went wrong","error");
            });
            
        });

        $('#addRemarks').click(function(e){
            e.preventDefault();
            var attendance_id = $('#attendance_id').val();
            var remarks = $('#remarks').val();
            $.ajax({
                url: '{{ route('attendance.addRemarks') }}',
                type: 'POST',
                data: {'remarks': remarks, 'attendance_id' : attendance_id},
            })
            .done(function(data) {
                swal("Success","Remarks Added","success");
                $('#attendanceModel').modal('toggle');
            })
            .fail(function() {
                swal("Oops!","Something went wrong","error");
            });
        });
        });
    </script>
@endsection