@extends('admin.template')
@section('title','Edit Department')
@section('hr-active','active')
@section('employee-head-active','active')
@section('attendance-active','active')
@section('content')
<h2>Edit Attendance</h2>    
        <div class="x_panel">
        <div class="x_title">
           
            <div class="clearfix">
            </div>
        </div>
        <div class="row">   
        
        <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::model($department, ['method' => 'PATCH', 'route' => ['department.update', $department->id] , 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Department Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::hidden('_method', 'PATCH') !!}
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
        {!! Form::close() !!}
        </div>
        </div>
        </div>

@endsection