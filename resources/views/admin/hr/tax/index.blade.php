@extends('admin.layouts.template')
@section('title','Taxes')

@section('hr-active','active')
@section('payroll-active','active')
@section('tax-active','active')

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Taxes List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Taxes List</li>
        </ol>
    </div>
    



        <div class="x_panel">
        <div class="x_title">
            
            <h2>Taxes List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('tax.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
               
            <div class="clearfix"></div>
                @include('errors')

    </div>
    <div class="row">
    	<div class="table-responsive">
         <table class="table table-bordered table-hover jambo_table bulk_action" id="datatable">
    		<thead>
    			<tr>
    				<th>ID</th>
    				<th>Amount From</th>
    				<th>Amount To</th>
                    <th>Date From</th>
    				<th>Date To</th>
                    <th>Tax Amount</th>
                    <th>Percentage</th>
                    <th class="text-center">Actions</th>
    			</tr>
    		</thead>
    		<tbody>
                @php($i = 1)
    			@foreach($taxes as $tax)
	    			<tr>
	    				<td>{{ $i++ }}</td>
                        <td>{{ number_format($tax->amount_from) }}</td>
                        <td>{{ number_format($tax->amount_to) }}</td>
                        <td>{{ date('m-Y', strtotime($tax->from_date)) }}</td>
                        <td>{{ date('m-Y', strtotime($tax->to_date)) }}</td>
                        <td>{{ number_format($tax->tax_amount) }}</td>
                        <td>{{ $tax->percentage }}</td>
	    				<td class="text-center"><a href="{{ route('tax.edit', $tax->id )}}" class="btn btn-xs btn-info edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['tax.destroy', $tax->id], 'style' => 'display: inline']) !!}
                            <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button>
                            {{-- {!! Form::submit("<p>a</p>", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!} --}}
                        {!! Form::close() !!}
	    				</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
        </div>
    </div>
</div>	
@endsection

@section('javascript')

<script type="text/javascript">

    $(document).ready(function()){
        $('.DeleteBtn').on('click',function(event){
                event.preventDefault();
                var form = $(this).parents('form');
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    form.submit();
                  } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                    // event.preventDefault();
                  }
                });
            });
        });
    </script>
@endsection