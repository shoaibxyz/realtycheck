@extends('admin.layouts.template')
@section('title','Edit Tax')
@section('hr-active','active')
@section('payroll-active','active')
@section('tax-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection
@section('hr-active','active')
@section('tax-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Tax</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Tax</li>
        </ol>
    </div>
    

        <div class="x_panel">
            
            <div class="x_title">
            
            <h2>Edit Tax</h2>
                
            <div class="clearfix"></div>
              

    </div>
        <div class="row">
          @include('errors')

       <div class="col-sm-6 col-sm-offset-3" style="margin: auto; float: none; padding: 20px;">   

        {!! Form::model($tax, ['method' => 'PATCH', 'route' => ['tax.update', $tax->id], 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">From Date </label>
                <div class="col-sm-8">
                {!! Form::text('from_date', $tax->from_date->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'from_date']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">To Date </label>
                <div class="col-sm-8">
                {!! Form::text('to_date', $tax->to_date->format('d/m/Y'), ['class' => 'form-control datepicker', 'id' => 'to_date']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">Amount From </label>
                <div class="col-sm-8">
                {!! Form::text('amount_from', null, ['class' => 'form-control', 'id' => 'amount_from']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">Amount To</label>
                <div class="col-sm-8">
                {!! Form::text('amount_to', null, ['class' => 'form-control', 'id' => 'amount_to']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">Tax Amount </label>
                <div class="col-sm-8">
                {!! Form::text('tax_amount', null, ['class' => 'form-control', 'id' => 'tax_amount']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-4 control-label">Extra Percentage</label>
                <div class="col-sm-8">
                {!! Form::text('percentage', null, ['class' => 'form-control', 'id' => 'percentage']) !!} 
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Define Tax Rate", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/tax') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection

@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script type="text/javascript">
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    </script>
@endsection