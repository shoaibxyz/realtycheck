@extends('admin.layouts.template')
@section('title','Define Tax')
@section('hr-active','active')
@section('payroll-active','active')
@section('tax-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection
@section('hr-active','active')
@section('tax-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Define New Tax</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">>Define New Tax</li>
        </ol>
    </div>

        <div class="x_panel">
            
            <div class="x_title">
            
            <h2>Define New Tax</h2>   
            <div class="clearfix"></div>

    </div>
        <div class="row">
        @include('errors')
       <div class="col-sm-6 col-sm-offset-3" style="margin: auto; float: none; padding: 20px;">  

        {!! Form::open(['method' => 'POST', 'route' => 'tax.store', 'class' => 'form-horizontal']) !!}

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">From Date <span style="color: red">*</span> </label>
                <div class="col-sm-8">
                
                {!! Form::text('from_date', '', ['class' => 'form-control datepicker', 'id' => 'from_date', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">To Date <span style="color: red">*</span> </label>
                <div class="col-sm-8">
                
                {!! Form::text('to_date', '', ['class' => 'form-control datepicker', 'id' => 'to_date', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">Amount From <span style="color: red">*</span> </label>
                <div class="col-sm-8">
                
                {!! Form::text('amount_from', '', ['class' => 'form-control', 'id' => 'amount_from', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">Amount To <span style="color: red">*</span></label>
                <div class="col-sm-8">
                
                {!! Form::text('amount_to', '', ['class' => 'form-control', 'id' => 'amount_to', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">Tax Amount <span style="color: red">*</span> </label>
                <div class="col-sm-8">
                
                {!! Form::text('tax_amount', '', ['class' => 'form-control', 'id' => 'tax_amount', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-4 control-label">Extra Percentage </label>
                <div class="col-sm-8">
                
                {!! Form::text('percentage', '', ['class' => 'form-control', 'id' => 'percentage']) !!} 
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/tax') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
            
        {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script type="text/javascript">
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    </script>
@endsection