@extends('admin.layouts.template')
@section('title','Advance Salary')
@section('hr-active','active')
@section('payroll-active','active')
@section('advance-salary-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        .ui-datepicker-calendar {
        display: none;
        }
    </style>
@endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Advance Salary</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Advance Salary</li>
        </ol>
    </div>


        <div class="x_panel">
            <div class="x_title">
                
                <h2>Advance Salary</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('advance-salary.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
                
                <div class="clearfix"></div>
                @include('errors')
            </div>
        
        {!! Form::open(['method' => 'GET', 'route' => 'advance-salary.checkAdvanceSalary', 'class' => 'form-horizontal']) !!}

        <div class="row">


            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('forMonth', 'For Month') !!}
                    <input name="forMonth" id="forMonth" class="date-picker form-control" required/>
                </div>
            </div>
        
            <div class="col-sm-4">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if(isset($advanceSalary))
                <table class="table table-bordered table-hover jambo_table" id="datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Employee Name</th>
                            <th>Department</th>
                            <th>Amount</th>
                            <th>Deduct Date</th>
                            <th>Request Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($advanceSalary as $salary)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $salary->employee->user->name}}</td>
                            <td>{{ $salary->employee->department->name}}</td>
                            <td>{{ $salary->amount}}</td>
                            <td>{{ $salary->deduct_month . ' '. $salary->deduct_year }}</td>
                            <td>{{ date('d-m-Y', strtotime($salary->created_at))}}</td>
                            <td class="text-center">{!! ($salary->is_approved == 1) ? "<span class='label label-success'>Approved</label>" : "<span class='label label-danger'>Pending</label>" !!}</td>
                            <td class="text-center">
                                {!! Form::open(['method' => 'PATCH', 'route' => ['advance-salary.update', $salary->id, 'style' => 'display: inline']]) !!}
                                    @php( $status = ( $salary->is_approved == 1) ? "Pending" : "Accept" )
                                    {!! Form::hidden('status_val', $salary->is_approved) !!}  
                                    {!! Form::submit($status, ['class' => 'btn btn-xs btn-info btn-xs DeleteBtn']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="modal fade" id="viewsalaryModal">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Paysalary Detail</h4>
              </div>
              <div class="modal-body">


                    <div class="row">
                                                                        
                        <div class="col-sm-12">
                            <p class="pull-right">Code: <span id="code"></span></p> 
                        </div>
                        <div class="col-sm-12">
                            <p class="pull-right">Department: <span id="department"></span></p> 
                        </div>
                        <div class="col-sm-12">
                            <p class="pull-right">Employee: <span id="employeeName"></span></p> 
                        </div>
                        <div class="col-sm-12">
                            <p class="pull-right">Date: <span id="salaryDate"></span></p> 
                        </div>
                        <div class="col-sm-12">
                            <p class="pull-right">Status: <span id="salaryStatus"></span></p> 
                        </div>

                    </div>

                    <hr>

                    <div class="x_panel" style="margin-top: 10px;">
                        <div class="x_title">
                            <h2>Deductions</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div id="Deductions"></div>
                        </div>
                    </div>


                    <div class="x_panel" style="margin-top: 10px;">
                        <div class="x_title">
                            <h2>Allownces</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div id="Allownces"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <h3 class="text-center">Paysalary Summary</h3> 
                            <p class="text-center">Basic Salary: <span id="basic_salary"></span></p> 
                            <p class="text-center">Total Allowance: <span id="total_allowances"></span></p> 
                            <p class="text-center">Total Deductions: <span id="total_deductions"></span></p> 
                            <p class="text-center">Fine: <span id="fine"></span></p> <hr>
                            <p class="text-center">Net Salary: <span id="net_salary"></span></p>
                        </div>                                                                         

                    </div>



              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="printDiv('viewsalaryModal')">Print</button>
              </div>
            </div>
          </div>
        </div>
          </div>
        </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
         $(function() {
    $('.date-picker').datepicker(
    {
        dateFormat: "mm-yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {


            function isDonePressed(){
                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
            }

            if (isDonePressed()){
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                
                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
            }
        },
        beforeShow : function(input, inst) {

            inst.dpDiv.addClass('month_year_datepicker')

            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month-1, 1));
                $(".ui-datepicker-calendar").hide();
            }
        }
        })
    });
        function printDiv(divName) {
             var printContents = document.getElementById(divName).innerHTML;
             var originalContents = document.body.innerHTML;

             document.body.innerHTML = printContents;

             window.print();

             document.body.innerHTML = originalContents;
        }

        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });


        $('.viewsalary').on('click', function(){
              $('#code').html("");
              $('#department').html("");
              $('#employeeName').html("");
              $('#salaryDate').html("");
              $('#salaryStatus').html("");
              $('#employeePicture').html("");
              $('#allowncesDeductions').html("");
              $('#basic_salary').html("");
              $('#net_salary').html("");
              $('#fine').html("");
              $('#fine_reason').html("");
              
              var paysalary_id = $(this).data('paysalary_id');
              $.ajax({
                url: '{{ url('admin/HR/paysalary/') }}/'+paysalary_id,
                type: 'GET'
              })
              .done(function(data) {
                console.log(data);
                $('#code').html(data[0].paysalary.code);
                $('#employeeName').html(data[0].user.name);
                $('#department').html(data[0].paysalary.department.name);
                var salaryDate = data[0].paysalary.created_at.split('-');
                $('#salaryDate').html(salaryDate[1] + ", " + salaryDate[0]);
                $('#salaryStatus').html((data[0].paysalary.status == 1) ? "Paid" : "Unpaid");
                $('#basic_salary').html(data[0].paysalary.basic_salary);
                $('#total_allowances').html(data[0].paysalary.total_allowances);
                $('#total_deductions').html(data[0].paysalary.total_deductions);
                $('#fine').html(data[0].paysalary.fine);
                $('#net_salary').html(data[0].paysalary.net_salary);

                $.each(data[0].allownceDeduction, function(index, val) {
                    if(val.type== 0)
                    {
                     $('#Deductions').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6">'+ val.amount +'</div>');
                    }
                });

                $.each(data[0].allownceDeduction, function(index, val) {
                    if(val.type== 1)
                    {
                     $('#Allownces').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6">'+ val.amount +'</div>');
                    }
                });

                // $.each(data.allownceDeduction, function(index, val) {
                //     if(val.type== 0)
                //     {
                //      $('#Deductions').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6">'+ val.amount +'</div>');
                //     }
                // });

                // $.each(data.allownceDeduction, function(index, val) {
                //     if(val.type== 1)
                //     {
                //      $('#Allownces').append('<div class="col-sm-6">'+ val.label +'</div><div class="col-sm-6">'+ val.amount +'</div>');
                //     }
                // });
                // $.each(data[0].sources, function(index, val) {
                // $('#employeeSources').append(val.name + ", ");
                // });
              })
              .fail(function() {
                console.log("error");
              });
              
            });
    </script>
@endsection