@extends('admin.layouts.template')
@section('title','Advance Salary')
@section('hr-active','active')
@section('payroll-active','active')
@section('advance-salary-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Advance Salary</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Advance Salary</li>
        </ol>
    </div>

        <div class="x_panel">
        <div class="x_title">
            
            <h2>Add Advance Salary</h2>   
                
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'POST', 'route' => 'advance-salary.store', 'class' => 'form-horizontal']) !!}
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">      
            <div class="form-group">    
                <label class="col-sm-3 control-label">Department <span style="color: red">*</span></label>
                <div class="col-sm-9">
                     {!! Form::select('department_id', $departmentsArr, 0 ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Employee <span style="color: red">*</span></label>
                <div class="col-sm-9">
                    {!! Form::select('employee_id', [], 0 ,['class' => 'form-control', 'id' => 'employees', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Amount <span style="color: red">*</span></label>
                <div class="col-sm-9">
                    {!! Form::text('amount', null ,['class' => 'form-control', 'id' => 'amount', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Month <span style="color: red">*</span></label>
                <div class="col-sm-9">
                    @php($monthsArr = [])
                    <?php 
                        $months = [
                            '' => 'Select Month',
                            '1' => 'Jan',
                            '2' => 'Feb',
                            '3' => 'Mar',
                            '5' => 'Apr',
                            '5' => 'May',
                            '6' => 'June',
                            '8' => 'July',
                            '8' => 'Aug',
                            '9' => 'Sep',
                            '10' => 'Oct',
                            '11' => 'Nov',
                            '12' => 'Dec'
                        ]; 
                    ?>
                    @foreach($months as $key => $month)
                        @php($monthsArr[$key] = $month)
                    @endforeach
                    {!! Form::select('deduct_month', $monthsArr, 0, ['class' => 'form-control', 'id' => 'deduct_month', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Deduct Year <span style="color: red">*</span></label>
                <div class="col-sm-9">
                    @php($yearsArr = [])
                    @php($yearsArr[2017] = '2017')
                    @php($yearsArr[2018] = '2018')
                    @php($yearsArr[2019] = '2019')
                    @php($yearsArr[2020] = '2020')
                    @php($yearsArr[2021] = '2021')
                    @php($yearsArr[2022] = '2022')
                    {!! Form::select('deduct_year', $yearsArr, date('Y'), ['class' => 'form-control', 'id' => 'deduct_year', 'required' => 'required']) !!}
                </div>
            </div>
            
            <div class="form-actions">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                    <a href="{{ route('advance-salary.index') }}" class="btn btn-grey">Cancel</a>
            </div>

        </div>
        {!! Form::close() !!}
        
    </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    </script>
@endsection