@extends('admin.layouts.template')
@section('title','Edit Department')
@section('hr-active','active')
@section('payroll-active','active')
@section('advance-salary-active','active')
@section('content')
        
        <h2>Edit Department</h2>    
        <div class="col-sm-6 col-sm-offset-3">      

        {!! Form::model($designation, ['method' => 'PATCH', 'route' => ['designation.update', $designation->id] , 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Department Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) !!} 
            </div>

            <div class="form-group">
                {!! Form::hidden('_method', 'PATCH') !!}
                {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div>

@endsection