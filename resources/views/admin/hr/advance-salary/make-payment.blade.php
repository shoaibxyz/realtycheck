@extends('admin.layouts.template')
@section('title','Make Payment')
@section('hr-active','active')
@section('payroll-active','active')
@section('advance-salary-active','active')
@section('content')
        <div class="x_panel">
        <div class="x_title">
                <h2>Make Payment</h2>   
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'POST', 'route' => 'payslip.store', 'class' => 'form-horizontal']) !!}

        <div class="row">

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, $department->id ,['class' => 'form-control', 'id' => 'department_id']) !!} 
                </div>
            </div>

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('Employee', 'Employee') !!}
                    {!! Form::select('employee_id', $employeeList, $employee->id ,['class' => 'form-control', 'id' => 'employees']) !!} 
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('month', 'Month') !!}
                    @php($monthsArr = [])
                    @foreach(\App\Models\Helpers::months() as $key => $months)
                        @php($monthsArr[$key] = $months)
                    @endforeach
                    {!! Form::select('month', $monthsArr, $month, ['class' => 'form-control', 'id' => 'month']) !!}
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('year', 'Year') !!}
                    @php($yearsArr = [])
                    @php($yearsArr[2017] = '2017')
                    @php($yearsArr[2018] = '2018')
                    @php($yearsArr[2019] = '2019')
                    @php($yearsArr[2020] = '2020')
                    {!! Form::select('year', $yearsArr, $year, ['class' => 'form-control', 'id' => 'year']) !!}
                </div>
            </div>
        
            <div class="col-sm-2">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        
    </div>


    <div class="x_panel">
        <div class="x_title">
                <h2>Allowances & Deductions</h2>   
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                 <div class="x_panel">
                    <div class="x_title">
                        <h2>Allowances</h2>   
                        <div class="clearfix"></div>
                    </div>
                    @php($total_allowances = 0)
                    @foreach($allownceDeduction as $allowance)
                        @if($allowance->type == 1)
                            <div class="col-sm-6">
                                {{ $allowance->label }} 
                            </div>
                            <div class="col-sm-6">
                                {{ $allowance->amount }}
                                @php($total_allowances += $allowance->amount)
                            </div>
                        @endif
                    @endforeach
                </div>    
            </div>

            <div class="col-sm-6">
                 <div class="x_panel">
                    <div class="x_title">
                        <h2>Deductions</h2>   
                        <div class="clearfix"></div>
                    </div>

                     @php($total_deductions = 0)
                    @foreach($allownceDeduction as $deductions)
                        @if($deductions->type == 0)
                            <div class="col-sm-6">
                                {{ $deductions->label }} 
                            </div>
                            <div class="col-sm-6">
                                {{ $deductions->amount }}
                                @php($total_deductions += $allowance->amount)
                            </div>
                        @endif
                    @endforeach
                </div>    
            </div>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
                <h2>Summary</h2>   
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                {!! Form::open(['method' => 'POST', 'route' => 'payslip.store', 'class' => 'form-horizontal']) !!}
                
                <div class="form-group">
                    {!! Form::label('fine', 'Fine', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('fine', null, ['class' => 'form-control', 'id' => 'fine']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Fine Reason', 'Fine Reason', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('fine_reason', null, ['class' => 'form-control']) !!}
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('Basic', 'Basic', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('basic_salary', $employee->salary, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'basic_salary']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('total_deductions', 'Total Deductions', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('total_deductions', $total_deductions, ['class' => 'form-control', 'readonly' => 'readonly' , 'id' => 'total_deductions']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('total_allowances', 'Total Allowances', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('total_allowances', $total_allowances, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'total_allowances']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('net_salary', 'Net Salary', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        {!! Form::text('net_salary', ($employee->salary + $total_allowances) - $total_deductions, ['class' => 'form-control', 'readonly' => 'readonly', 'id' => 'net_salary']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', 'Status', ['class' => 'col-sm-4']) !!}
                    <div class="col-sm-8"> 
                        @php($status = [])
                        @php($status[1] = 'Paid')
                        @php($status[0] = 'Unpaid')
                        {!! Form::select('status', $status, 1, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        {!! Form::hidden('employee_id', $employee->id) !!}
                        {!! Form::hidden('department_id', $department->id) !!}
                        {!! Form::hidden('month', $month) !!}
                        {!! Form::hidden('year', $year) !!}
                        {!! Form::submit('Create Payslip', ['class' => 'btn btn-default']) !!}  
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $('#fine').blur(function(event) {
            var fine = $(this).val();
            if(fine != "")
            {
                var net_salary = $('#net_salary').val();
                var new_net_salary = parseInt(net_salary) - parseInt(fine);
                $('#net_salary').val(new_net_salary);
            }
            else{
                var basic_salary = $('#basic_salary').val();
                var total_allowances = $('#total_allowances').val();
                var total_deductions = $('#total_deductions').val();

                $('#net_salary').val((parseInt(basic_salary) + parseInt(total_allowances)) - parseInt(total_deductions));   
            }
        });
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    </script>
@endsection