@extends('admin.layouts.template')
@section('title','Leave Management')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('hr-active','active')
@section('leave-active','active')
@section('leaves-type-active','active')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">All Leave Requests</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">All Leave Requests</li>
        </ol>
    </div>


        <div class="x_panel">
        <div class="x_title">
            
            <h2>All Leave Requests</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('leave-request.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                </div>
               
            <div class="clearfix"></div>
                @include('errors')

    </div>
    <div class="row">
            {!! Form::open(['method' => 'GET', 'route' => 'leave-request.check', 'class' => 'form-horizontal']) !!}
            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('department', 'Department') !!}
                    {!! Form::select('department_id', $departmentsArr, 0 ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-3">    
                <div class="form-group">
                    {!! Form::label('Employee', 'Employee') !!}
                    {!! Form::select('employee_id', ['Select Department'], 0 ,['class' => 'form-control', 'id' => 'employees', 'required' => 'required']) !!} 
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('from', 'from') !!}
                    {!! Form::text('from', (isset($from)) ? $from : null, ['class' => 'form-control datepicker', 'id' => 'month', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('to', 'to') !!}
                    {!! Form::text('to', (isset($to)) ? $to : null, ['class' => 'form-control datepicker', 'id' => 'to', 'required' => 'required']) !!}
                </div>
            </div>
        
            <div class="col-sm-2">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        
    </div>
    
    <div class="row">
    	<div class="table-responsive">
        <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
    		<thead>
    			<tr>
                    <th>Department</th>
                    <th>Employee</th>
                    <th>Type</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Reason</th>
                    <th class="text-center">Status</th>
    				<th class="text-center">Actions</th>
    			</tr>
    		</thead>
    		<tbody>
                @php($i = 1)
    			@foreach($leaves as $leave)
	    			<tr>
                        <td>{{ $leave->employee->department->name }}</td>
                        <td>{{ \App\User::getUserInfo($leave->employee->person_id)->name }}</td>
                        <td>{{ \App\Helpers::leaveTypes($leave->leave_type_id) }}</td>
                        <td>{{ date('d-m-Y', strtotime($leave->leave_start_date))}}</td>
                        <td>{{ date('d-m-Y', strtotime($leave->leave_end_date))}}</td>
                        <td>{{ $leave->reason }}</td>
                        <?php 
                            if($leave->status == 1)
                                $label = "label-success";
                                $span = "Approved";
                            if($leave->status == 2)
                                $label = "label-warning";
                                $span = "Pending";
                            if($leave->status == 3)
                                $label = "label-danger";
                                $span = "Declined";
                        ?>
                        <td class="text-center"><span class="label {{ $label }}">{{ $span }}</span></td>
                        <td class="text-center">
                            @if($leave->attachment)
                            <a href="{{ env('STORAGE_PATH') .$leave->attachment }}" target="_blank" class="btn btn-xs btn-success">Attachment</a>
                            @endif
                            
                                @if($leave->status == 3 || $leave->status == 2)
                                <a href="{{ route('leave-request.changeStatus', [$leave->id, 1] )}}" class="btn btn-xs btn-success">Approve</a>
                                @elseif($leave->status == 1)
                                <a href="{{ route('leave-request.changeStatus', [$leave->id, 3] )}}" class="btn btn-xs btn-info">Reject</a>
                                @endif                          
                                <a href="{{ route('leave-request.edit', $leave->id )}}" class="btn btn-xs btn-warning edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                            
                            
                            <form method="post" action="{{ route('leave-request.destroy', $leave->id) }}" style="display: inline;">{{ csrf_field() }}<input name='_method' type='hidden' value='DELETE'>  <button type="submit" class="btn btn-danger btn-xs DeleteBtn delete" data-toggle="tooltip" title="Delete"> <i class="fa fa-times"></i></button></form>  
                            

	    				</td>
	    			</tr>
	    		@endforeach
    		</tbody>
    	</table>
        </div>
    </div>
</div>	
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        // $('.datepicker').datepicker({});
        $('.datepicker').datepicker({
              dateFormat: 'dd/mm/yy'
          });
        $('#department_id').change(function(){
            $.ajax({
                url: '{{ route('getEmployeeByDepartment') }}',
                type: 'POST',
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                console.log(data);
                $('#employees').html(data.employees);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    </script>
@endsection