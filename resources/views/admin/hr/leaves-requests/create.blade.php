@extends('admin.layouts.template')
@section('title','Add Leave')
@section('hr-active','active')
@section('leave-active','active')
@section('leaves-type-active','active')
@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Leave</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Leave</li>
        </ol>
    </div>
    
 
        <div class="x_panel">
            
            <div class="x_title">
            
            <h2>Add New Leave</h2> 
               
            <div class="clearfix"></div>
               

    </div>
    <div class="row">
    @include('errors')
        <div class="col-sm-6 col-sm-offset-3" style="margin: auto; float: none; padding: 20px;">       

        {!! Form::open(['method' => 'POST', 'route' => 'leave-request.store', 'class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Leave Type <span style="color: red">*</span>      </label>
                <div class="col-sm-8">
                
                @php($leaveArr = [])
                @foreach($leaveTypes as $key => $leave)
                    @php($leaveArr[$leave->id] = $leave->name)
                @endforeach
                {!! Form::select('leave_type_id', $leaveArr, 0, ['class' => 'form-control', 'id' => 'leave', 'required' => 'required']) !!}
            </div></div>

            
            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Department <span style="color: red">*</span>      </label>
                <div class="col-sm-8">
                
                {!! Form::select('department_id', $departmentsArr, 0 ,['class' => 'form-control', 'id' => 'department_id', 'required' => 'required']) !!} 
            </div></div>

             <div class="form-group">
             <label for="label" class="col-sm-3 control-label">Employee <span style="color: red">*</span>       </label>
                <div class="col-sm-8">
                
                {!! Form::select('employee_id', ['Select Department'], 0 ,['class' => 'form-control', 'id' => 'employees', 'required' => 'required']) !!} 
            </div></div>
            

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Leave Start <span style="color: red">*</span> </label>
                <div class="col-sm-8">
                
                {!! Form::text('leave_start_date', null ,['class' => 'form-control datepicker', 'id' => 'leave_start_date', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Leave End <span style="color: red">*</span>       </label>
                <div class="col-sm-8">
                
                {!! Form::text('leave_end_date', null ,['class' => 'form-control datepicker', 'id' => 'leave_end_date', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Reason <span style="color: red">*</span>      </label>
                <div class="col-sm-8">
               
                {!! Form::textarea('reason', null ,['class' => 'form-control', 'id' => 'reason' , 'size' => '30x5', 'required' => 'required']) !!} 
            </div></div>

            <div class="form-group">
            <label for="label" class="col-sm-3 control-label">Attachment </label>
                <div class="col-sm-8">
                
                {!! Form::file('attachment', ['class' => 'form-control', 'id' => 'attachment']) !!} 
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/leave-request') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    

    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        $(document).ready(function($) {
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
             });
            $('#department_id').change(function(){
                $.ajax({
                    url: '{{ route('getEmployeeByDepartment') }}',
                    type: 'POST',
                    data: {department_id: $(this).val()},
                })
                .done(function(data) {
                    console.log(data);
                    $('#employees').html(data.employees);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        });
       
    </script>
@ENDSECTION