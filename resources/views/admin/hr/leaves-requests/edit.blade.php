@extends('admin.layouts.template')
@section('title','Edit Leave')
@section('hr-active','active')
@section('leave-active','active')
@section('leaves-type-active','active')

@section('style')
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    @endsection

@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Leave Request</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Leave Request</li>
        </ol>
    </div>
    

        <div class="x_panel">
        <div class="x_title">
                  <h2>Edit Leave Request</h2> 
            <div class="clearfix"></div>
                @include('errors')

    </div>
        <div class="col-lg-8" style="margin: auto; float: none; padding: 20px;">  

        {!! Form::model($leave, ['method' => 'PATCH', 'route' => ['leave-request.update', $leave->id] , 'class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Leave Type </label>
                <div class="col-sm-8">
                @php($leaveArr = [])
                @foreach($leaveTypes as $key => $leaves)
                    @php($leaveArr[$leaves->id] = $leaves->name)
                @endforeach
                {!! Form::select('leave_type_id', $leaveArr, null, ['class' => 'form-control', 'id' => 'leave', 'required' => 'required']) !!}
            </div></div>

            <div class="form-group">
                 <label for="label" class="col-sm-3 control-label">Department </label>
                <div class="col-sm-8">
                {!! Form::select('department_id', $departmentsArr, $leave->employee->department->id ,['class' => 'form-control', 'id' => 'department_id']) !!} 
            </div></div>

             <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Employee </label>
                <div class="col-sm-8">
                {!! Form::select('employee_id', $employeesArr, $leave->employee_id, ['class' => 'form-control','id' => 'employees']) !!}
            </div></div>

            <div class="form-group">
                 <label for="label" class="col-sm-3 control-label">Leave Start Date </label>
                <div class="col-sm-8">
                {!! Form::text('leave_start_date', $leave->leave_start_date->format('d/m/Y') ,['class' => 'form-control datepicker', 'id' => 'leave_start_date']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Leave End Date</label>
                <div class="col-sm-8">
                {!! Form::text('leave_end_date', $leave->leave_end_date->format('d/m/Y') ,['class' => 'form-control datepicker', 'id' => 'leave_end_date']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Reason </label>
                <div class="col-sm-8">
                {!! Form::textarea('reason', null ,['class' => 'form-control', 'id' => 'reason' , 'size' => '30x5']) !!} 
            </div></div>

            <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Attachment </label>
                <div class="col-sm-8">
                {!! Form::file('attachment', ['class' => 'form-control', 'id' => 'attachment']) !!} 

                <img src="{{ env('STORAGE_PATH') .$leave->attachment }}" width="100" height="100" class="img-responsive" style="margin-top: 20px;">
            </div></div>
        
            <div class="col-sm-12 form-actions text-right pal">  
        
            <div class="form-group" style="display: inline-block;">
                {!! Form::submit("Submit", ['class' => 'btn btn-warning']) !!}
            </div>
            <a href="{{ url('admin/HR/leave-request') }}" class="btn btn-grey" style="display: inline-block;margin-top:12px;">Cancel</a>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function($) {
            $('.datepicker').datepicker({
                dateFormat: 'dd/mm/yy'
             });
            $('#department_id').change(function(){
                $.ajax({
                    url: '{{ route('getEmployeeByDepartment') }}',
                    type: 'POST',
                    data: {department_id: $(this).val()},
                })
                .done(function(data) {
                    $('#employees').html(data.employees);
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        });
       
    </script>
@endsection