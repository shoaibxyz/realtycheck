@extends('admin.layouts.template')

@section('title','Add Feature Set')

@section('bookings-active','active')
@section('plot-active','active')
@section('features-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add Feature Set</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add Feature Set</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Feature Set</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'route' => 'features.store', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Feature Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('feature', '', ['class' => 'form-control', 'required'=>'required', 'id' => 'feature']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Charges <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('charges', '', ['class' => 'form-control', 'required'=>'required', 'id' => 'charges']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Charges Description <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('charges_description', '', ['class' => 'form-control', 'required'=>'required', 'id' => 'charges_description']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('features.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
