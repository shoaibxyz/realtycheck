@extends('admin.layouts.template')

@section('title','Edit Feature Set')

@section('bookings-active','active')
@section('plot-active','active')
@section('features-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Feature Set</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Feature Set</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($featureset, ['method' => 'PATCH', 'route' => ['features.update', $featureset->id] , 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Feature Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('feature', null , ['class' => 'form-control', 'id' => 'feature', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Charges <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('charges', null, ['class' => 'form-control', 'id' => 'charges', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Charges Description <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('charges_description', null, ['class' => 'form-control', 'id' => 'charges_description', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PATCH') !!}
                        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('features.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
