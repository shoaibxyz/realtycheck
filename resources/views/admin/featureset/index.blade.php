@extends('admin.layouts.template')

@section('title','Features List')

@section('bookings-active','active')
@section('plot-active','active')
@section('features-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Features List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Features List</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Features List</h2>
                <div class="actions" style="float: right; display: inline-block">
                    <a href="{{ route('features.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i>&nbsp;New Feature</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr>
                            <th width="5%">Id#</th>
                            <th width="25%">Name</th>
                            <th width="25%">Charges</th>
                            <th width="25%">Description</th>
                            <th width="10%">Edit</th>
                            <th width="10%">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($featureset as $feature)
                        <tr>
                            <td>{{ $feature->id }}</td>
                            <td>{{ $feature->feature }}</td>
                            <td>{{ $feature->charges }}</td>
                            <td>{{ $feature->charges_description }}</td>
                            <td><a href="{{ route('features.edit', $feature->id ) }}" class="btn btn-info btn-xs" style="{{ $rights->show_edit }}">Edit</a></td>
                            <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['features.destroy', $feature->id], 'class' => 'deleteForm','style' => $rights->show_delete]) !!}
                                {!! Form::submit("Delete", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
    $(document).ready(function(){
        $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });
    });
    </script>
@endsection
