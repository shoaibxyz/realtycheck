@extends('admin.layouts.template')

@section('title','Add Permission')

@section('user-management-active','active')
@section('permissions-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Add New Permission</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Add New Permission</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New Permission</h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6" style="margin: auto; float: none; padding-top: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST', 'route' => 'permissions.store', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name', '', ['class' => 'form-control' ,'required' => 'required', 'id' => 'name', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Display Name</label>
                        <div class="col-sm-8">
                            {!! Form::text('display_name', '', ['class' => 'form-control' ,'required' => 'required', 'id' => 'display_name']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            {!! Form::text('description', '', ['class' => 'form-control' , 'id' => 'description']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Module <span class="required">*</span></label>
                        <div class="col-sm-8">
                            @php($moduleArr[''] = 'Choose one')
                            @foreach($modules as $module)
                                @php ($moduleArr[$module->id] = $module->name)
                            @endforeach
                            {!! Form::select('module_id', $moduleArr, 0, ['class' => 'form-control','required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('permissions.index') }}" class="btn btn-grey">Cancel</a>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
