@extends('admin.layouts.template')

@section('title','Permissions')

@section('user-management-active','active')
@section('permissions-active','active')

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Permissions List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Permissions List</li>
        </ol>
    </div>

    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Permissions List</h2>
                <div class="actions" style="float: right; display: inline-block; {{$rights->show_create}}">
                    <a href="{{ route('permissions.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;New Permission</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover jambo_table" id="datatable">
                    <thead>
                        <tr>
                            <th width="5%">#Id</th>
                            <th width="25%">Name</th>
                            <th width="25%">Display Name</th>
                            <th width="25%">Module</th>
                            <th width="10%"  style="{{$rights->show_edit}}">Edit</th>
                            <th width="10%"  style="{{$rights->show_delete}}">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->id }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->display_name }}</td>
                                <td>{{ $permission->module }}</td>
                                <td style="{{$rights->show_edit}}"><a href="{{ route('permissions.edit', $permission->id )}}" class="btn btn-info btn-xs">Edit</a></td>
                                <td style="{{$rights->show_delete}}">
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id]]) !!}
                                        {!! Form::submit("Delete", ['class' => 'btn btn-danger btn-xs']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
