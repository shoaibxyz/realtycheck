@extends('admin.layouts.template')
@section('title','Receipt Report')

@section('reports-active','active')
@section('booking-report-active','active')
@section('receipt-daily-report-active','active')

<?php
use App\Role;
?>

@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 @endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Receipt Daily Report'])
    <div class="x_panel">
  <div class="x_title">
    <h2>Receipts Detail</h2>
    
    <div class="clearfix"></div>
  </div> 
    
      
      
      {!! Form::open(['route' => 'report.CheckReceiptReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  
  <div class="col-md-5">
    <div class="form-group">
	            {!! Form::label('startdate', 'Start Date') !!}
	            {!! Form::text('startdate', date('d-m-Y',strtotime($startdate)),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
		    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
			{!! Form::label('enddate', 'End Date')!!}
			{!! Form::text('enddate', date('d-m-Y',strtotime($enddate)),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
			</div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>

      <div class="table-responsivee">
        <table class="table table-bordered table-hover jambo_table"  id="receiptReportTbl">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Receipt No</th>
              <th>Receipt Date</th>
              <th>Reg No</th>
              <th>Member Name</th>
              <th>Payment Mode</th>
              <th>Type</th>
              <th>Recv. Amt</th>
              <th>Rebate. Amt</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
          <?php $i = 1; $received_amount=0; $rebate_amount = 0; ?>
            @foreach($report as $key => $rep)
                <?php
                $member = \App\members::join('persons','persons.id','members.person_id')->where('registration_no', $rep->registration_no)->select(DB::Raw('persons.name'))->first()
                ?>
              <tr>
              <td>{{ $i }}</td>
              <td>{{ $rep->receipt_no }}</td>
              <td>{{ date('d-m-Y', strtotime($rep->receiving_date)) }}</td>
              <td>{{ $rep->registration_no }}</td>
              <td>{{ (isset($member)) ? $member->name : "" }}</td>
              <td>{{ $rep->receiving_mode }}</td>
              <td><?php
                    if($rep->receiving_type == 1)
                        echo "Installment";
                    else if($rep->receiving_type == 2)
                        echo "Other";
                    elseif($rep->receiving_type == 3)
                        echo "Both";
                    elseif($rep->receiving_type == 4)
                        echo "Plot Preference";
                    elseif($rep->receiving_type == 5)
                        echo "LPC";
                    elseif($rep->receiving_type == 6)
                        echo "Transfer";
                    ?></td>
              <td>{{ ($rep->received_amount) ? $rep->received_amount :  0 }}</td>
              <td>{{ ($rep->rebate_amount) ? $rep->rebate_amount : 0 }}</td>
             {{--  <td>{{ $rep->reference_no }} </td>
              <td>{{ $rep->instrument_date }}</td>
              <td>{{ $rep->deposit_date }}</td>
              <td>{{ $rep->bank->short_name or "" }}</td>
              <td>{{ $rep->deposit_account_no }}</td>
              <td>{{ $rep->pdc_ack }}</td>
              <td>{{ $rep->plot_name }}</td> --}}
              <td>{{ $rep->receipt_remarks }}</td>
            </tr>
            <?php $i++; $received_amount += $rep->received_amount; ?>
            <?php $rebate_amount += $rep->rebate_amount; ?>
            @endforeach
          </tbody>
          <tr>
            <td colspan="7"  class="text-right"><b>Total Amount:</b></td>
            <td id="received_amount"><b>{{ $received_amount }}</b></td>   
            <td id="rebate_amount" ><b>{{ $rebate_amount }}</b></td>   
            <td></td>
          </tr>

          <tr>
            <td colspan="7"  class="text-right"><b>Grand Total:</b></td>
            <td id="grand_total" ><b>{{ $received_amount + $rebate_amount }}</b></td>   
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>

</div>
@endsection

@section('javascript')
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  
<script type="text/javascript">
  <?php
$rights = Role::getrights('receipts');
  ?>
    if("{{ Auth::user()->Roles->first()->name == 'Super-Admin' }}" || "{{ Auth::user()->Roles->first()->name == 'Admin' }}" || "{{$rights->can_print}}")
      {
          var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];
      }else{
        var buttons = [];
      }

    var datatable = $('#receiptReportTbl').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: buttons
    } );

        jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });

    $("#receiptReportTbl").on('search.dt', function() {
      var receivedAmount = datatable.column( 7, {"filter": "applied"} ).data().sum();
      var rebateAmount = datatable.column( 8, {"filter": "applied"} ).data().sum();
      $('#received_amount').html("<b>"+receivedAmount+"</b>" );
      $('#rebate_amount').html("<b>"+rebateAmount+"</b>" );
      $('#grand_total').html("<b>"+(receivedAmount + rebateAmount)+"</b>" );
  });
  
  $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy'
	    });
    });
    </script>
@endsection