@extends('admin.layouts.template')
@section('title','Activity Logs Report')

@section('reports-active','active')
@section('other-report-active','active')
@section('activity-logs-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<style type="text/css">
    @if(Auth::user()->Roles->first()->name == 'call-center')
      .dt-buttons{
        display: none;
      }
    @endif
    </style>


@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Activity Logs Report'])
@include('errors')

<div class="x_panel">
  <div class="x_title">
    <h2>Activity Logs</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['route' => 'report.activity', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('fromdate', 'From') !!}
      {!! Form::text('fromdate', Request::input('fromdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('todate', 'To') !!}
      {!! Form::text('todate', Request::input('todate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>


<div id="app">
        @include('admin.report.brandin-header', ['reportTitle' => "Activity Logs Report"])

  <div class="table-responsive">
    <table class="table table-bordered table-hover jambo_table" id="activityLogs" style="overflow-x: scroll; width: 100% ">
      <thead>
        <tr>
          <th style="width: 10%;">Log Name</th>
          <th style="width: 400px;">Description</th>
          <th style="width: 10%;">Caused by</th>
          <th style="width: 10%;">Registration No</th>
          <th style="width: 10px;">Event</th>
          <th style="width: 10px;">Date</th>
        </tr>
      </thead>
      <tbody>
        @php($i = 1)
        @if(isset($activities))
          @foreach($activities as $activity)
          <tr>
            <th>{{ $activity->log_name}}</th>
            <th id="description">{!! $activity->description !!}</th>
            <th>{{ ($activity->causer_id != 0) ? App\User::getUserInfo($activity->causer_id)->name : ""  }}</th>
            <th>{{ $activity->getExtraProperty('registration_no') }}</th>
            <th>{{ $activity->getExtraProperty('event') }}</th>
            <th>{{ $activity->created_at->format('d-m-Y h:i:s')}}</th>
          </tr>
          @endforeach
        @endif
      </tbody>
  </table>
</div>



</div>
</div>

@endsection
@section('javascript')
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });
    
    var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];
      
  $('#activityLogs').DataTable({
      responsive: true,
      dom: 'Bfrtip',
      buttons: buttons
    });
    
    jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });
</script>
@endsection