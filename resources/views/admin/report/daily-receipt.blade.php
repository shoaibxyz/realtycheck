@extends('admin.layouts.template')
@section('title','Reports')

@section('reports-active','active')
@section('booking-report-active','active')
@section('receipt-daily-report-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

	@include('errors')
@include('admin/common/breadcrumb',['page'=>'Receipt Daily Report'])
        <div class="x_panel">
        <div class="x_title">
            <h2>Daily Receipt Detail</h2>
            
            <div class="clearfix"></div>
          </div>    
		{!! Form::open(['route' => 'report.CheckReceiptReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
                 
		<div class="col-md-6">
			<div class="form-group">
	            {!! Form::label('startdate', 'Start Date') !!}
	            {!! Form::text('startdate', \Carbon\Carbon::now()->format('d-m-Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
		    </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
			{!! Form::label('enddate', 'End Date')!!}
			{!! Form::text('enddate', \Carbon\Carbon::now()->format('d-m-Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
			</div>
		</div>
<div class="col-md-6">
		<div class="checkbox">
  {{-- <label><input type="checkbox" value="">Full</label> --}}
</div></div>
<div class="col-md-6"></div>

		
	            <div class="form-group">
	                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
	                
	            </div>
	           
	        {!! Form::close() !!}
        </div>
        
@endsection
@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy'
	    });
    });
    </script>
@endsection