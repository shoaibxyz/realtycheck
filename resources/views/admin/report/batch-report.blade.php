@extends('admin.layouts.template')
@section('title','Reports')

@section('reports-active','active')
@section('booking-report-active','active')
@section('batch-files-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css" media="print"> 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
<style type="text/css">
	.bold {
		font-weight: bold;
	}
</style>
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Batch Report'])
@include('errors')

<div class="x_panel">
	<div class="x_title">
		<h2>Batch Report</h2>

		<div class="clearfix"></div>
	</div>
	{!! Form::open(['route' => 'report.check-batch-files', 'class' => 'form-horizontal','method' => 'GET', 'id' => 'batchForm']) !!}


	<div class="col-md-2">
		<div class="form-group">
			{!! Form::label('from_date', 'From') !!}
			{!! Form::text('from_date', Request::input('from_date'),  array( 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			{!! Form::label('to_date',  'To') !!}
			{!! Form::text('to_date',Request::input('to_date'),  array( 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			{!! Form::label('agency_id', 'Agency') !!}
			<?php
			$agencyArr[''] = "";
			foreach ($agencies as $agency) {
				$agencyArr[$agency->id] = $agency->name;
			}
			?>
			{!! Form::select('agency_id', $agencyArr, Request::input('agency_id'), ['class' => 'form-control', 'id' => 'agency'] ) !!}
		</div>
	</div>

	<div class="col-sm-2">
		<div class="form-group">
            {!! Form::label('batch_no', 'Batch') !!}
            <select class="form-control" name="batch_no" id="batches">
            	@if(count($agency_batches) > 0)
            	    <option value="" disabled selected>Select batch</option>
            		@foreach($agency_batches as $batch)
            			@php($selected = ($batch->batch_no == Request::input('batch_no')) ? "selected" : "" )
            			<option value="{{ $batch->batch_no }}" {{ $selected}}> {{ $batch->batch_no }}</option>
            		@endforeach
            	@else
                	<option value="">Select Agency</option>
            	@endif
            </select>
        </div>
	</div>
	<div class="col-sm-2" style="margin-top: 25px;">
		<div class="form-group">
			{!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
			<input type="button" class="btn btn-grey" id="resetBtn" value="Reset">
		</div>
	</div>

	{!! Form::close() !!}

	@if(isset($batches))
	<div id="app">



		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#residential" aria-controls="residential" role="tab" data-toggle="tab">Residential</a>
				</li>
				<li role="presentation">
					<a href="#commercial" aria-controls="commercial" role="tab" data-toggle="tab">Commercial</a>
				</li>

				<li role="presentation">
					<a href="#housing" aria-controls="housing" role="tab" data-toggle="tab">Housing</a>
				</li>
			</ul>
		
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="residential">
					@if(count($filesResidential) > 0)
						@include('admin.report._residential-batch-file-report', ['filesResidential' => $filesResidential, 'nature' => 'Residential'])
					@endif
				</div>
				<div role="tabpanel" class="tab-pane" id="commercial">
					@if(count($filesCommercial) > 0)
						@include('admin.report._commercial-batch-file-report', ['filesCommercial' => $filesCommercial, 'nature' => 'Commercial'])
					@endif
				</div>
				<div role="tabpanel" class="tab-pane" id="housing">
					@if(count($filesHousing) > 0)
						@include('admin.report._housing-batch-file-report', ['filesHousing' => $filesHousing, 'nature' => 'Housing'])
					@endif
				</div>
			</div>
		</div>
		
		</div>
		@endif
		<p><button class="btn btn-success" style="{{ $rights->show_print }}" id="printReport">Print All</button></p>
	</div>

	@endsection
	@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<!--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>-->

<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript">

		$(document).ready(function() {
			$('.datepicker').datepicker({
				dateFormat: 'dd/mm/yy'
			});
		});
		
// 		var datatable = $('.batch_table').DataTable({ 
//           "bSort" : false,
//           "filter" : false,    
//           fixedHeader: true,
//           "paging": false
//         } )

		function CallPrint(strid)
		{
			var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_blank';

                  
                    var WindowObject = window.open('Booking Report', 'PrintWindow' , 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
                    WindowObject.document.writeln('<!DOCTYPE html>');
                    WindowObject.document.writeln('<html><head><title></title>');
                    WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/custom.min.css" rel="stylesheet">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/leadger.css" rel="stylesheet">');
                    WindowObject.document.writeln('<style>.btn-print{display:none;}</style>');
    
                     WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/css/yellow-grey.css" rel="stylesheet">');
                    WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')
        
                    WindowObject.document.writeln($('#'+strid).html());
        
                    WindowObject.document.writeln('</body></html>');
        
                    WindowObject.document.close();
        
                    WindowObject.focus();
                    //WindowObject.close();
                    
                  return true;
		}

		$('#printReport').on('click',function(){
			CallPrint('app');
		})

		$('#printTotal').on('click',function(){
			CallPrint('totalFiles');
		})

		$('#printBooked').on('click',function(){
			CallPrint('bookedFiles');
		})

		$('#printRemaining').on('click',function(){
			CallPrint('remainingFiles');
		})

		$('#CprintTotal').on('click',function(){
			CallPrint('CtotalFiles');
		})

		$('#CprintBooked').on('click',function(){
			CallPrint('CbookedFiles');
		})

		$('#HprintRemaining').on('click',function(){
			CallPrint('HremainingFiles');
		})

		$('#HprintTotal').on('click',function(){
			CallPrint('HtotalFiles');
		})

		$('#HprintBooked').on('click',function(){
			CallPrint('HbookedFiles');
		})

		$('#HprintRemaining').on('click',function(){
			CallPrint('HremainingFiles');
		})




		$('#resetBtn').click(function(){
			window.location.replace("{{ route('report.batch-files') }}");
		});

		$(document).ready(function($) {

			 $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('[name="_token"]').val()
	            }
		    });
					 
			 var url = "{{ route('agency.batch') }}";

	        $('#agency').on('change', function(){
	         	console.log($(this).val());
	            $.ajax({
	                url: url,
	                type: 'POST',
	                data: {'agency_id': $(this).val()},
	                success: function(data){
	                    $('#batches').html(data);
	                },
	                error: function(xhr, status, responseText){
	                    console.log(xhr);
	                }
	            });
	        });
		});

	</script>
	@endsection