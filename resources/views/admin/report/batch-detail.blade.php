@extends('admin.layouts.stemplate')
@section('title','Batch Detail')

@section('content')
        
        @if(!empty($message))
            <p>{{$message}}</p>
        @endif

         <div class="x_panel">
          <div class="x_title">
            <h2>{{ $files['agency']->name }}</h2>
            
            <div class="clearfix"></div>
          </div>    
        <div class="table-responsive">
        <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                     <th>Registration No</th>
                     <th>Member Name</th>
                    <th>Batch No</th>
                    <th>Plot Size</th>
                    <th>Assigned Date</th>
                    <th>Is Booked?</th>
                    
                    
                   
                </tr>
            </thead>

             <tbody>
           
                @foreach($files['assignedFiles'] as $key => $file)
                    {{-- {{ $files['person'][$key]->name }} --}}
                    <tr>
                        <td>
                           {{--  @php($booking_id =  (isset($files['booking'][$key]->id)) ? $files['booking'][$key]->id : "" ) --}}
                            @if($file->is_reserved != 0)
                                <a href="{{ route('booking.ledger', strtolower($file->registration_no)) }}"><u>{{ $file->registration_no }}</u></a>
                            @else
                                {{ $file->registration_no }}
                            @endif
                        </td>
                        <td>
                        @if($file->is_reserved != 0)
                            <u><a href="{{ route('member.inquiry', $file->registration_no ) }}">{{ $files['person'][$key]->name or ""}}</a></u></td> 
                        @endif
                        <td>#{{ $file->batch->batch_no }}</td>
                         <td>{{ $file->plotSize->plot_size }}</td>
                          <td>{{ date('d-m-Y', strtotime($file->batch->batch_assign_date)) }}</td>
                        @php($label = ($file->is_reserved == 1) ? "label-success" : "label-warning")
                        <th><label class="label {{ $label }}">{{ ($file->is_reserved == 1) ? "Yes" : "No" }}</label></th>
                    </tr>
                   
                @endforeach
            </tbody>
            <tbody>
            
                    
                    
            </tbody>
        </table>
        </div>  </div>
@endsection
