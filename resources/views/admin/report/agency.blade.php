@extends('admin.layouts.template')
@section('reports-active','active')
@section('user-report-active','active')
@section('agency-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<style>
    .buttons-print{
      display: none;  
    }
</style>
 
@endsection
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Agency Report</div>
        </div>
        
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Agencies Report</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Agency Report</h2>
                
                <div class="clearfix"></div>
                
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table bulk_action" id="agencytable">
                    
                    <thead>
                        <tr>
                            <th style="width: 5%">#Id</th>
                            <th style="width: 25%">Name</th>
                            <th style="width: 20%">Reg No</th>
                            <th style="width: 20%">Contact No</th>
                            <th class="text-center">Address</th>
                            
                           
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                        @foreach($Agency as $agen)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $agen->name }}</td>
                                <td>{{ $agen->agency_reg_no }}</td>
                                <td>{{ $agen->contact_no }}</td>
                                <td>{{ $agen->address }}</td>
                              
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                    </tbody>
                
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
   <!--<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>-->
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    });
   
       var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];
          
    var datatable = $('#agencytable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: buttons
    } );

    </script>
@endsection