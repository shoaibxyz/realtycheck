@extends('admin.layouts.template')
@section('title','Overdue Installments Detail')

@section('reports-active','active')
@section('installment-report-active','active')
@section('over-due-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection


@section('content')

  @include('errors')
@include('admin/common/breadcrumb',['page'=>'Overdue Installments Report'])
        <div class="x_panel">
          <div class="x_title">
            <h2>Overdue Installments Detail</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.CheckOverDueAmounts', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
                 
          <div class="col-md-4">
            <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-4" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>

            @if(isset($installments))
          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action" id="ReportTbl">
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Inst #</th>
                        <th>Reg # </th>
                        <th>Ref # </th>
                        <th>Member Name</th>
                        <th>Payment Type</th>
                        <th>Due Date</th>
                        <th>Overdue Days</th>
                        <th>Due Amount</th>                        
                    </tr>
                </thead>
                <tbody>
                  @php($i = 1)
                  @php($total = 0)
                  @foreach($installments as $key => $inst)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $inst->installment_no }}</td>
                      <td>{{ $member[$key]->registration_no ?? ""}}</td>
                      <td>{{ $member[$key]->reference_no ?? ""}}</td>
                      <td>{{ $person[$key]->name ?? ""}}</td>
                      <td>{{ $inst->payment_desc }}</td>
                      <td>{{ date('d-m-Y', strtotime($inst->installment_date)) }}</td>
                       <td>
                          <?php 
                            $date1=date_create($inst->installment_date);
                            $date2=date_create(date('Y-m-d'));
                            $diff=date_diff($date2,$date1);
                          ?>
                      {{ $diff->format("%a days") }}</td>
                      <td>{{ ($inst->os_amount > 0 ) ? $inst->os_amount : $inst->installment_amount }}</td>
                    </tr>
                  @php($i++)
                  @php($total += $inst->installment_amount)
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="8" style="text-align:right">Total:</th>
                        <th>{{ number_format($total) }}</th>
                    </tr>
                </tfoot>
            </table>
          </div>
          @endif
      </div>
        
@endsection
@section('javascript')
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    <script type="text/javascript">

      if("{{ Auth::user()->Roles->first()->name }}" == 'Super-Admin' || "{{ Auth::user()->Roles->first()->name }}" == 'ccd-incharge'){
          var buttons = [
        //   {
        //       extend: 'print',
        //       exportOptions: {
        //           columns: [ 2 ]
        //       },
        //       customize: function ( win ) {
        //         $(win.document.body)
        //             .css( 'font-size', '10pt' );

        //         $(win.document.body).find( 'table' )
        //             .addClass( 'compact' )
        //             .css( 'font-size', 'inherit' );
        //       }
        //   },
          {
            extend: 'excel',
            exportOptions: {
                  columns: [ 2 ]
              }
          },
          'colvis'];
      }else{
        var buttons = [];
      }
      
    $('#ReportTbl').dataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    } );

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection