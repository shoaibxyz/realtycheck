        
                  <h3>Commercial - Inventory Report</h3>
                  <button id="commercial_inventory_print" class="btn btn-success inventory_print" style="{{$rights->show_print}}">Print</button>
                  <div class="table-responsive">
                  <table class="table table-hover table-bordered jambo_table bulk_action" id="commercialPrintTable" style="width: 100%; overflow-x: scroll;" >
                    <thead>
                      <tr>
                        <th rowspan="3">Sr. No</th>
                        <th rowspan="3" style="width: 70px;">Estate</th>
                        <th colspan="3">Issued files</th>
                        <th colspan="8">Booked Files</th>
                        <th colspan="3">Bal. Files</th>
                      </tr>
                      <tr>


                        <th>2M</th>
                        <th>4M</th>
                        <th>Total</th>

                        <th colspan="3">2M</th>
                        <th colspan="3">4M</th>
                        <th colspan="2">Total</th>

                        <th>2M</th>
                        <th>4M</th>
                        <th>Total</th>

                      </tr>

                      <tr>
                          
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Open</th>
                        <th>Booked</th>
                        <th>Total</th>
                        
                        <th>Open</th>
                        <th>Booked</th>
                        <th>Total</th>
                        

                        <th>Open</th>
                        <th>Booked</th>
                        

                        <th></th>
                        <th></th>
                        

                      </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                       @php($i = 1)
                      
                       @php($IssuedOldSum = 0)
                       @php($IssuedNewSum = 0)
                       @php($IssuedTotalSum = 0)

                       @php($Booked2MSum = 0)
                       @php($Booked4MSum = 0)
                       @php($BookedOldSum = 0)
                       @php($BookedNewSum = 0)
                       @php($BookedTotalSum = 0)
                       

                       @php($RemainingOldSum = 0)
                       @php($RemainingNewSum = 0)
                       @php($RemainingTotalSum = 0)


                       @foreach($filesArr['Commercial'] as $agency => $inventory)
                          {{-- @php(dd($agencyOldIssuedFiles['Commercial'][$agency])) --}}
                         <td align="center">{{ $i++ }}</td>
                        <td style="width: 70px;">{{ $agency }}</td>
                        {{-- issued Files --}}
                        <td align="center">
                          {{ $IssuedOld = isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M'])  ? array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) : 0}}
                            @php($IssuedOldSum += $IssuedOld )
                        </td>
                        <td align="center">
                          {{ $IssuedNew = isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M'] ) ? array_sum($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) : 0}}
                          @php($IssuedNewSum += $IssuedNew )
                        </td>
                        <td align="center"><strong>
                          @if(isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) && isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']))
                            {{ array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) + array_sum($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) + array_sum($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) )
                          @elseif(isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M']))
                            {{ array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']))
                          @elseif(isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']))
                            {{ array_sum($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) )
                          @else 
                            -
                          @endif
                        </strong></td>

                        {{-- Booked files --}}
                        <td class="text-center">
                          <?php
                            $BookedOld = isset($agencyOldBookedFiles["Commercial"][$agency]['02-M'])  ? array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M']) : 0;
                            $openBooked2M = isset($commercialOpenBookingsArr['Commercial'][$agency]['02-M']) ? $commercialOpenBookingsArr['Commercial'][$agency]['02-M'] : 0;
                            //echo $openBooked2M;
                            echo $IssuedOld - $BookedOld;
                            //$Booked2MSum += $openBooked2M;
                            $Booked2MSum += ($IssuedOld - $BookedOld);
                          ?>

                        </td>
                        <td align="center">
                          {{ $BookedOld }}
                          @php($BookedOldSum += $BookedOld )
                        </td>

                        <td>
                          <b>{{ $BookedOld + ($IssuedOld - $BookedOld) }}</b>
                        </td>

                        <td class="text-center">
                         <?php
                            $BookedNew = isset($agencyOldBookedFiles["Commercial"][$agency]['04-M'] ) ? array_sum($agencyOldBookedFiles["Commercial"][$agency]['04-M']) : 0;
                            $openBooked4M = isset($commercialOpenBookingsArr['Commercial'][$agency]['04-M']) ? $commercialOpenBookingsArr['Commercial'][$agency]['04-M'] : 0;
                            //echo $openBooked4M; 
                            echo $IssuedNew - $BookedNew;
                            //$Booked4MSum += $openBooked4M;
                            $Booked4MSum += ($IssuedNew - $BookedNew);
                          ?>
                        </td>
                        <td align="center">
                          {{ $BookedNew }}
                          @php($BookedNewSum += $BookedNew )
                        </td>

                        <td class="text-center">
                          <b>{{ $BookedNew + ($IssuedNew - $BookedNew) }}</b>
                        </td>

                        <td class="text-center">{{ ($IssuedOld + $IssuedNew) - ($BookedOld + $BookedNew) }}</td>
                        <td align="center"><strong>
                          @if(isset($agencyOldBookedFiles["Commercial"][$agency]['02-M']) && isset($agencyOldBookedFiles["Commercial"][$agency]['04-M']))
                            {{ array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M']) + array_sum($agencyOldBookedFiles["Commercial"][$agency]['04-M']) }}
                            @php($BookedTotalSum += array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M']) + array_sum($agencyOldBookedFiles["Commercial"][$agency]['04-M']) )
                          @elseif(isset($agencyOldBookedFiles["Commercial"][$agency]['02-M']))
                            {{ array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M']) }}
                            @php($BookedTotalSum += array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M'] ))
                          @elseif(isset($agencyOldBookedFiles["Commercial"][$agency]['04-M']))
                            {{ array_sum($agencyOldBookedFiles["Commercial"][$agency]['04-M']) }}
                            @php($BookedTotalSum += array_sum( $agencyOldBookedFiles["Commercial"][$agency]['04-M']) )
                          @else 
                            @php($BookedTotalSum += 0)
                            {{ 0 }}
                          @endif

                        </strong></td>
                        {{-- Remaining Balance --}}
                         <td align="center">
                          {{-- @php($oldRemaining = 0) --}}
                          {{-- @php($oldIssued = 0) --}}
                          {{-- @php($oldIssued = 0) --}}
                          @if(isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) && isset($agencyOldBookedFiles["Commercial"][$agency]['02-M']))
                            {{ $oldRemaining = $IssuedOld - $BookedOld }}
                            @php($RemainingOldSum += ( $IssuedOld - $BookedOld) )
                          @elseif(isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M']))
                            {{-- @php($oldIssued = array_sum($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) ) --}}
                            {{ $oldRemaining = $IssuedOld }}
                            @php($RemainingOldSum += $IssuedOld)
                          @elseif(isset($agencyOldBookedFiles["Commercial"][$agency]['02-M']))
                            {{-- @php($oldBooked = array_sum($agencyOldBookedFiles["Commercial"][$agency]['02-M'] )) --}}
                            <b>{{ $oldRemaining = $BookedOld }}</b>
                            @php($RemainingOldSum += $BookedOld)
                          @else 
                            -
                          @endif
                        </td>

                        @php($newRemaining = 0)
                        @php($newIssued = 0)
                        @php($newBooked = 0)

                        <td align="center">
                          @if(isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']) && isset($agencyOldBookedFiles["Commercial"][$agency]['04-M']))
                            {{ $newRemaining = $IssuedNew - $BookedNew }}
                            @php($RemainingNewSum += ($IssuedNew - $BookedNew))
                          @elseif(isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']))
                            {{ $newRemaining = $IssuedNew }}
                            @php($RemainingNewSum += $IssuedNew)
                          @elseif(isset($agencyOldBookedFiles["Commercial"][$agency]['04-M']))
                            <b>{{ $newRemaining = $BookedNew }}</b>
                            @php($RemainingNewSum += $BookedNew)
                          @else 
                            -
                          @endif
                        </td>

                        <td align="center"><strong>
                          @if(isset($agencyOldIssuedFiles["Commercial"][$agency]['02-M']) && isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']))
                            {{ $oldRemaining + $newRemaining }}
                            @php($RemainingTotalSum += $oldRemaining + $newRemaining )
                          @elseif(isset($agencyOldIssuedFiles["Commercial"][$agency]['04-M']))
                            {{ $newRemaining  }}
                            @php($RemainingTotalSum += $newRemaining )
                          @elseif(isset($agencyOldBookedFiles["Commercial"][$agency]['02-M'])) 
                            {{ $oldRemaining }}
                            @php($RemainingTotalSum += $oldRemaining)
                          @else
                            {{ $oldRemaining }}
                          @endif
                        </strong></td>

                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                       <tr>
                        <th colspan="2">Total</th>

                        <th style="text-align:center">{{ $IssuedOldSum }}</th>
                        <th style="text-align:center">{{ $IssuedNewSum }}</th>
                        <th style="text-align:center">{{ $IssuedOldSum + $IssuedNewSum }}</th>

                        <th style="text-align:center">{{ $Booked2MSum }}</th>
                        <th style="text-align:center">{{ $BookedOldSum }}</th>
                        <th style="text-align:center">{{ $Booked2MSum + $BookedOldSum }}</th>

                        <th style="text-align:center">{{ $Booked4MSum  }}</th>
                        <th style="text-align:center">{{ $BookedNewSum }}</th>
                        <th style="text-align:center">{{ $Booked4MSum + $BookedNewSum }}</th>

                        <th style="text-align:center">{{ $Booked2MSum + $Booked4MSum   }}</th>
                        <th style="text-align:center">{{ $BookedOldSum + $BookedNewSum  }}</th>

                        <th style="text-align:center">{{ $RemainingOldSum }}</th>
                        <th style="text-align:center">{{ $RemainingNewSum }}</th>
                        <th style="text-align:center">{{ $RemainingOldSum + $RemainingNewSum }}</th>

                      </tr>

{{--                       <tr>
                        <td colspan="7">
                          <b>Total Open Files</b>
                        </td>
                        <td>{{ $commercialOpenBookings }}</td>
                        <td colspan="3"></td>
                      </tr>

                      <tr>
                        <td colspan="7">
                          <b>Total Balance</d>
                        </td>
                        <td>{{ ($BookedOldSum + $BookedNewSum ) - $commercialOpenBookings }}</td>
                        <td colspan="3"></td>
                      </tr> --}}
                    </tfoot> 
                  </table>
                </div>