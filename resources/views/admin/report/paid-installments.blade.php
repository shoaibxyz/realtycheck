@extends('admin.layouts.template')
@section('title','Paid Installments Report')

@section('reports-active','active')
@section('installment-report-active','active')
@section('paid-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection



@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'Paid Installments Report'])
<div class="x_panel">
  <div class="x_title">
    <h2>Paid Installments Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['route' => 'report.view-paid-inst', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('startdate', 'From') !!}
      {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('enddate', 'To') !!}
      {!! Form::text('enddate', Request::input('enddate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>

  @if(isset($installments))
  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action" id="ReportTbl">
      <thead>
        <tr>
          <th>Sr #</th>
          <th>Reg #</th>
          <th>Member Name</th>
          <th>Phone</th>
          <th>Payment Type</th>
          <th>Plot Size</th>
          <th>Installment Due</th>
          <th>Amount Paid</th>
          <th>Dated</th>
        </tr>
      </thead>
      <tbody>
       @php($i = 1)
       @php($inst_sum = 0)
       @php($inst_received = 0)
       <?php 
       foreach ($installments as $key => $installment) {
          $receipt = \App\receipts::getReceipt($installment->receipt_no);
        ?>
       <tr>
        <td>{{ $i }}</td>
        <td>{{ $installment->registration_no }}</td>
        <td>{{ $person[$key]->name ?? ""}}</td>
        <td>{{ $person[$key]->phone_mobile ?? ""}}</td>
        <td>{{ $installment->payment_desc }}</td>
        <td>{{ $bookings[$key]->paymentPlan->plotSize->plot_size ?? ""}}</td>
        <td>{{ $installment->installment_amount }}</td>
        <td>{{ $installment->amount_received }}</td>
        <td>{{ date('d-m-Y', strtotime($installment->installment_date)) }}</td>
        @php($i++)
        <?php $inst_sum += $installment->installment_amount; ?>
        <?php $inst_received += ($installment->amount_received -$installment->rebate_amount); ?>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <tr>
        <th colspan="6">Total</th>
        <td id="inst_sum">{{ number_format($inst_sum) }}</td>
        <td id="receiving_amt">{{ number_format($inst_received) }}</td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</div>
@endif


</div>

@endsection
@section('javascript')

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    if("{{ Auth::user()->Roles->first()->name }}" == 'Super-Admin'){
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
          }];
      }else{
        var buttons = [];
      }
      
    var table = $('#ReportTbl').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons:buttons
    } );

          jQuery.fn.DataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
            return (a*1) + (b*1); // cast values in-case they are strings
          });
    });

    $("#ReportTbl").on('search.dt', function() {
      $('#receiving_amt').html(table.column( 7, {"filter": "applied"} ).data().sum() );
      $('#inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
    });

    
    $('.datepicker').datepicker({
      dateFormat: 'dd/mm/yy'
    });
  });
</script>
@endsection