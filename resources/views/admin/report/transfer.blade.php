@extends('admin.layouts.template')
@section('title','Transfer Report')

@section('reports-active','active')
@section('other-report-active','active')
@section('transfer-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection



@section('content')

  @include('errors')
@include('admin/common/breadcrumb',['page'=>'Transfer Report'])
        <div class="x_panel">
          <div class="x_title">
            <h2>Transfer Report</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.transferReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
          <div class="row" style="margin:0;">       
          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('enddate', 'From') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker') )!!}
              </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                    {!! Form::label('startdate', 'To') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker') )!!}
              </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                    {!! Form::label('registration_no', 'Registration No') !!}
                    {!! Form::text('registration_no', Request::input('registration_no'),  array('class'=>'form-control') )!!}
              </div>
          </div>
          
          <div class="col-md-3" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>
        </div>
          @if(isset($transferReport))
          <div id="app">
        @include('admin.report.brandin-header', ['reportTitle' => "Booking Report"])

          <div class="table-responsive">
            <table class="table table-bordered table-hover jambo_table" id="ReportTbl">
                <thead>
                    <tr>
                        <th>Trans. No</th>
                        <th>Reg #</th>
                        <th>Ref #</th>
                        <th>Old Owner</th>
                        <th>New Owner</th>
                        <th>Date</th>
                        <th>Trans. Fee</th>
                        <th>Is Paid</th>
                       
                        <th>NDC Date</th>
                        <th>Completed?</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                      $i = 1;
                      foreach ($transferReport as $key => $report) {?>
                          <tr>
                              <td>{{ $report->transfer_no }}</td>
                              <td>{{ $report->reg_no }}</td>
                              <td>{{ App\members::getMemberDetail($report->current_user_id)->reference_no }}</td>
                              <td>{{ App\members::getMemberDetail($report->current_user_id)->user->name }}</td>
                              <td>{{ App\members::getMemberDetail($report->new_user_id)->user->name }}</td>
                              <td>{{ date('d-m-Y', strtotime($report->transfer_date)) }}</td>
                              <td>{{ $report->tranfer_charges }}</td>
                              <td>{{ ($report->is_paid == 1) ? "Paid" : "Unpaid" }}</td>
                              
                              <td>
                               
                                  {{ date('d-m-Y', strtotime($report->ndc_dl_date))}}
                               
                              </td>
                              <td>{{ $report->is_ok == 1 ? "Yes" : "No" }}</td>
                          </tr>
                  <?php } ?>
                </tbody>
            </table>
            <button id="printreceipt" class="btn btn-success">Print</button>
          </div>
          @endif

</div>
      </div>
        
@endsection
@section('javascript')

  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
      var windowUrl = 'about:blank';
      var uniqueName = new Date();
      var windowName = '_self';

      var printWindow = window.open(window.location.href , windowName);
      printWindow.document.body.innerHTML = printContent.innerHTML;
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
      return false;
    }

    $('#printreceipt').click(function(){
      CallPrint('app');
    });
    

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection