@extends('admin.layouts.template')
@section('title','Due Installments Report')

@section('reports-active','active')
@section('installment-report-active','active')
@section('due-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection

@section('content')
@include('errors')
@include('admin/common/breadcrumb',['page'=>'Due Installments Report'])

        <div class="x_panel">
          <div class="x_title">
            <h2>Due Installments Report</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.CheckdueAmounts', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
                 
          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>


          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('agency', 'Agency') !!}
                    <?php
                    $agencyArr[''] = "";
                    foreach ($agencies as $agency) {
                      $agencyArr[$agency->id] = $agency->name;
                    }
                    ?>
                    {!! Form::select('agency_id', $agencyArr, Request::input('agency_id'), ['class' => 'form-control', 'id' => 'agency'] ) !!}
              </div>
          </div>

          {{-- <div class="col-md-5">
            <div class="form-group">
                    {!! Form::label('startdate', 'To') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
              </div>
          </div> --}}

          <div class="col-md-3" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>

          @if(isset($installments))
          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action table-bordered" id="dueReportTable"> 
                <thead>
                    <tr>
                         <th>Sr</th>
                        <th>Inst </th>
                        <th>Reg</th>
                        <th>Ref </th>
                        <th>Plot</th>
                        <th>Member</th>
                        <th>Phone</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Days</th>
                        <th>Due Amount</th>                        
                        <th>LPC</th>                        
                        <th>Total Amount</th>                            
                    </tr>
                </thead>
                <tbody>
                  @php($i = 1)
                  @php($regArr = [])
                  @php($inst_sum = 0)
                  @php($lpc_sum = 0)
                  @foreach($booking_inst as $key => $inst)
                  
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $inst->installment_no }}</td>
                      <td>{{ $inst->registration_no }}</td>
                      <td>{{ $member[$key]->reference_no}}</td>
                      <td>{{ ($bookings[$key]->paymentPlan and $bookings[$key]->paymentPlan->plotSize) ? $bookings[$key]->paymentPlan->plotSize->plot_size : ""}}</td>
                      <td>{{ $person[$key]->name}}</td>
                      <td>{{ $person[$key]->phone_mobile}}</td>
                      <td>{{ $inst->payment_desc }}</td>
                      <td>{{ date('d-m-Y', strtotime($inst->installment_date)) }}</td>
                      <td>
                        <?php 
                          $date_1=date_create($inst->installment_date->format('Y-m-d'));
                          $date_2=date_create(date('Y-m-d'));
                          $diff2=date_diff($date_1,$date_2);
                          $days = (int)$diff2->format("%R%a");
                          if(($days > 0)) 
                            echo "<span class='text-danger'>".$diff2->format("%R%a")."</span>"; 
                          else 
                            echo $diff2->format("%R%a");
                          $lpc = ($inst->lpc_charges != 0) ? $inst->lpc_charges : $inst->lpc_other_amount;
                        ?>
                      </td>
                      <td>{{ ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount }}</td>
                      <td>{{ number_format($lpc) }}</td>
                      <td>
                        @php($amount = ($inst->os_amount > 0) ? $inst->os_amount : $inst->due_amount - $inst->os_amount)
                        {{ number_format($lpc + $amount)  }}</td>
                    </tr>
                    <?php $inst_sum += $amount; ?>
                    <?php $lpc_sum += $lpc; ?>
                  @php($i++)
                  @endforeach
                </tbody>

                <tfoot>
                  <tr>
                    <th colspan="10">Total</th>
                    <td id="inst_sum">{{ number_format($inst_sum) }}</td>
                    <td id="lpc_sum">{{ number_format($lpc_sum) }}</td>
                    <td id="total">{{ number_format($inst_sum + $lpc_sum) }}</td>
                  </tr>
                </tfoot>
            </table>
          </div>
          @endif


      </div>
        
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    
      if("{{ Auth::user()->Roles->first()->name == 'Super-Admin' }}")
      {
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
          }];
      }else{
        var buttons = [];
      }

      
     jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });

   var table = $('#dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: buttons
    });

    $("#dueReportTable").on('search.dt', function() {
      $('#inst_sum').html(table.column( 10, {"filter": "applied"} ).data().sum() );
      $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#total').html(table.column( 12, {"filter": "applied"} ).data().sum() );
    });

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection