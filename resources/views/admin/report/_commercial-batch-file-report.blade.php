<div id="CtotalFiles">
		<h4>Total Assigned Files {{ $nature }} <small class="pull-right"><button class="btn btn-default" id="CprintTotal">Print</button></small></h4>
		<div class="table-responsive">
			<table class="table table-striped jambo_table bulk_action" id="ReportTbl">
				<thead>
					<tr>
						<th>Sr #</th>
						<th>Batch No</th>
						<th>Agency Name</th>
						<th>Batch Issue Date</th>
						<th>2-M</th>
						<th>3-M</th>
						<th>4-M</th>
						<th>5-M</th>
						<th>06-M</th>
						<th>08-M</th>
						<th>10-M</th>
						<th>12-M</th>
						<th>16-M</th>
						<th>24-M</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				
					
					<?php
					$agency = [];
					$i=1;
					$batchNo = [];

					$twoCMarla       = 0;
					$threeCMarla       = 0;
					$fourCMarla       = 0;
					$fiveCMarla       = 0;
					$sixCMarla        = 0;
					$eightCMarla      = 0;
					$tenCMarla        = 0;
					$twelveCMarla     = 0;
					$sixteenCMarla    = 0;
					$twentyfourCMarla = 0;

					//////////////////////
					$twoRAssigned       = 0;
					$threeRAssigned       = 0;
					$fourRAssigned       = 0;
					$fiveRAssigned       = 0;
					$sixRAssigned        = 0;
					$eightRAssigned      = 0;
					$tenRAssigned        = 0;
					$twelveRAssigned     = 0;
					$sixteenRAssigned    = 0;
					$twentyfourRAssigned = 0;

					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";

					foreach ($filesCommercial as $key => $myfiles) {
						foreach ($myfiles as $key2 => $file) {
							if($file != null)
							{
								if ($file->plotSize->plot_size == '02-M') {
									$batchNo[$key]['TwoMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$batchNo[$key]['ThreeMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$batchNo[$key]['FouCMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$batchNo[$key]['FiveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$batchNo[$key]['SixMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$batchNo[$key]['EightMarla'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$batchNo[$key]['TenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$batchNo[$key]['TwelveMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$batchNo[$key]['SixteenMarla'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$batchNo[$key]['TwentyfouCMarla'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}

						}

						$twoCMarla       = (isset($batchNo[$key]['TwoMarla'])) ? count($batchNo[$key]['TwoMarla']) : 0;
						$threeCMarla       = (isset($batchNo[$key]['ThreeMarla'])) ? count($batchNo[$key]['ThreeMarla']) : 0;
						$fourCMarla       = (isset($batchNo[$key]['FouCMarla'])) ? count($batchNo[$key]['FouCMarla']) : 0;
						$fiveCMarla       = (isset($batchNo[$key]['FiveMarla'])) ? count($batchNo[$key]['FiveMarla']) : 0;
						$sixCMarla        = (isset($batchNo[$key]['SixMarla'])) ? count($batchNo[$key]['SixMarla']) : 0;
						$eightCMarla      = (isset($batchNo[$key]['EightMarla'])) ? count($batchNo[$key]['EightMarla']) : 0;
						$tenCMarla        = (isset($batchNo[$key]['TenMarla'])) ? count($batchNo[$key]['TenMarla']) : 0;
						$twelveCMarla     = (isset($batchNo[$key]['TwelveMarla'])) ? count($batchNo[$key]['TwelveMarla']) : 0;
						$sixteenCMarla    = (isset($batchNo[$key]['SixteenMarla'])) ? count($batchNo[$key]['SixteenMarla']) : 0;
						$twentyfourCMarla = (isset($batchNo[$key]['TwentyfouCMarla'])) ? count($batchNo[$key]['TwentyfouCMarla']) : 0;
						
						?>
						<tr>
							<td>{{ $i }}</td>
							<td>{{ $batch_no }}</td>
							<td><a href="#" target="_blank">{{ $agency_name }}</a></td>
							<td>{{ $batch_assign_date }}</td>
							<td>{{ $twoCMarla }}</td>
							<td>{{ $threeCMarla }}</td>
							<td>{{ $fourCMarla }}</td>
							<td>{{ $fiveCMarla }}</td>
							<td>{{ $sixCMarla }}</td>
							<td>{{ $eightCMarla }}</td>
							<td>{{ $tenCMarla }}</td>
							<td>{{ $twelveCMarla }}</td>
							<td>{{ $sixteenCMarla }}</td>
							<td>{{ $twentyfourCMarla}}</td>
							<td class="bold">{{ $twoCMarla+$threeCMarla+$fourCMarla+$fiveCMarla+$sixCMarla+$eightCMarla+$tenCMarla+$twelveCMarla+$sixteenCMarla+$twentyfourCMarla }}</td>
						</tr>
						<?php
							$twoRAssigned += $twoCMarla;
							$threeRAssigned += $threeCMarla;
							$fourRAssigned += $fourCMarla;
							$fiveRAssigned += $fiveCMarla;
							$sixRAssigned += $sixCMarla;
							$eightRAssigned += $eightCMarla;
							$tenRAssigned += $tenCMarla;
							$twelveRAssigned += $twelveCMarla;
							$sixteenRAssigned += $sixteenCMarla;
							$twentyfourRAssigned += $twentyfourCMarla;
							$i++;
						}   ?>
					</tr>

				</tbody>
				<tfoot>
					<tr class="bold">
						<td colspan="4">Total</td>
						<td>{{ $twoRAssigned }}</td>
						<td>{{ $threeRAssigned }}</td>
						<td>{{ $fourRAssigned }}</td>
						<td>{{ $fiveRAssigned }}</td>
						<td>{{ $sixRAssigned }}</td>
						<td>{{ $eightRAssigned }}</td>
						<td>{{ $tenRAssigned }}</td>
						<td>{{ $twelveRAssigned }}</td>
						<td>{{ $sixteenRAssigned }}</td>
						<td>{{ $twentyfourRAssigned }}</td>
						<td>{{ $twoRAssigned + $threeRAssigned + $fourRAssigned + $fiveRAssigned + $sixRAssigned + $eightRAssigned + $tenRAssigned + $twelveRAssigned + $sixteenRAssigned + $twentyfourRAssigned }}</td>
					</tr>
				</tfoot>
			</table>
		</div>
		</div>

		<div id="CbookedFiles">
		<h4>Booked Files <small class="pull-right"><button class="btn btn-default" id="CprintBooked">Print</button></small></h4>
		<div class="table-responsive">
			<table class="table table-striped jambo_table bulk_action" id="ReportTbl">
				<thead>
					<tr>
						<th>Sr #</th>
						<th>Batch No</th>
						<th>Agency Name</th>
						<th>Batch Issue Date</th>
						<th>2-M</th>
						<th>3-M</th>
						<th>4-M</th>
						<th>5-M</th>
						<th>06-M</th>
						<th>08-M</th>
						<th>10-M</th>
						<th>12-M</th>
						<th>16-M</th>
						<th>24-M</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				
					<?php
					$agency = [];
					$i = 1;
					$batch_no = "";
					$agency_name = "";
					$batch_assign_date = "";
                    $twoRBooked       = 0;
							$threeRBooked       = 0;
							$fourRBooked       = 0;
							$fiveRBooked       = 0;
							$sixRBooked        = 0;
							$eightRBooked      = 0;
							$tenRBooked        = 0;
							$twelveRBooked     = 0;
							$sixteenRBooked    = 0;
							$twentyfourRBooked = 0;


							foreach ($filesCommercial as $key => $myfiles) {
								foreach ($myfiles as $key2 => $file2) {
									if($file != null)
									{

										$batch_no = $file2->batch->batch_no ;
										$agency_name = $file2->batch->agency->name;
										$agency_id = $file2->batch->agency->id;
									}
								}
					
						?>
						<tr>
							<td>{{ $i }}</td>
							<td>{{ $batch_no }}</td>
							<td><a href="#" target="_blank">{{ $agency_name }}</a></td>
							@php
							@endphp
							<?php 

							$booked = [];
							

							$twoRMarla        = 0;
							$threeRMarla        = 0;
							$fourRMarla        = 0;
							$fiveRMarla        = 0;
							$sixRMarla         = 0;
							$eightRMarla       = 0;
							$tenRMarla         = 0;
							$twelveRMarla      = 0;
							$sixteenRMarla     = 0;
							$twentyfourRMarla  = 0;
							//////////////////////
							$booked2Marla  = 0;
							$booked3Marla  = 0;
							$booked4Marla  = 0;
							$booked5Marla  = 0;
							$booked6Marla  = 0;
							$booked8Marla  = 0;
							$booked10Marla = 0;
							$booked12Marla = 0;
							$booked16Marla = 0;
							$booked24Marla = 0;


							
								$bookedFiles = \App\assignFiles::batchBookedFiles($file2->batch_id, Request::input('agency_id'), 'Commercial');
								foreach($bookedFiles as $key2 =>  $file)
								{
									if($file != null)
									{
										$twoMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '02-M');
										
										if ($twoMarlaPlotBookings > 0 ) {
											$booked[$key2]['TwoMarlaBooked'][] = $twoMarlaPlotBookings;
										}

										$threeMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '03-M');

										if ($threeMarlaPlotBookings > 0 ) {
											$booked[$key2]['ThreeMarlaBooked'][] = $threeMarlaPlotBookings;
										}

										$fourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '04-M');
										if ($fourMarlaPlotBookings > 0) {
											$booked[$key2]['FourMarlaBooked'][] = $fourMarlaPlotBookings;
										}

										$fiveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '05-M');

										if ($fiveMarlaPlotBookings > 0) {
											$booked[$key2]['FiveMarlaBooked'][] = $fiveMarlaPlotBookings;
										}

										$sixMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '06-M');


										if ($sixMarlaPlotBookings > 0 ) {
											$booked[$key2]['SixMarlaBooked'][] = $sixMarlaPlotBookings;
										}

										$eightMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '08-M');


										if ($eightMarlaPlotBookings > 0 ) {
											$booked[$key2]['EightMarlaBooked'][] = $eightMarlaPlotBookings;
										}

										$tenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '10-M');

										if ($tenMarlaPlotBookings > 0 ) {
											$booked[$key2]['TenMarlaBooked'][] = $tenMarlaPlotBookings;
										}


										$twelveMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '12-M');


										if ($twelveMarlaPlotBookings > 0) {
											$booked[$key2]['TwelveMarlaBooked'][] = $twelveMarlaPlotBookings;
										}


										$sixteenMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial',  '16-M');


										if ($sixteenMarlaPlotBookings > 0) {
											$booked[$key2]['SixteenMarlaBooked'][] = $sixteenMarlaPlotBookings;
										}

										$twentyfourMarlaPlotBookings = \App\assignFiles::countDatewiseBooking($agency_id, $file->payment_date, $file2->batch_id, 'Commercial', '24-M');

										if ($twentyfourMarlaPlotBookings > 0 ) {
											$booked[$key2]['TwentyfourMarlaBooked'][] = $twentyfourMarlaPlotBookings;
										}
									}


								$booked2Marla  = (isset($booked[$key2]['TwoMarlaBooked'])) ? $booked[$key2]['TwoMarlaBooked'][0] : 0;
								$booked3Marla  = (isset($booked[$key2]['ThreeMarlaBooked'])) ? $booked[$key2]['ThreeMarlaBooked'][0] : 0;
								$booked4Marla  = (isset($booked[$key2]['FourMarlaBooked'])) ? $booked[$key2]['FourMarlaBooked'][0] : 0;
								$booked5Marla  = (isset($booked[$key2]['FiveMarlaBooked'])) ? $booked[$key2]['FiveMarlaBooked'][0] : 0;
								$booked6Marla  = (isset($booked[$key2]['SixMarlaBooked'])) ? $booked[$key2]['SixMarlaBooked'][0] : 0;
								$booked8Marla  = (isset($booked[$key2]['EightMarlaBooked'])) ? $booked[$key2]['EightMarlaBooked'][0] : 0;
								$booked10Marla = (isset($booked[$key2]['TenMarlaBooked'])) ? $booked[$key2]['TenMarlaBooked'][0] : 0;
								$booked12Marla = (isset($booked[$key2]['TwelveMarlaBooked'])) ? $booked[$key2]['TwelveMarlaBooked'][0] : 0;
								$booked16Marla = (isset($booked[$key2]['SixteenMarlaBooked'])) ? $booked[$key2]['SixteenMarlaBooked'][0] : 0;
								$booked24Marla = (isset($booked[$key2]['TwentyfourMarlaBooked'])) ? $booked[$key2]['TwentyfourMarlaBooked'][0] : 0;
								?>
							<tr>
								<td colspan="3"></td>
								<td>{{ date('d-m-Y', strtotime($file->payment_date)) }}</td>
								<td>{{ $booked2Marla }}</td>
								<td>{{ $booked3Marla }}</td>
								<td>{{ $booked4Marla }}</td>
								<td>{{ $booked5Marla }}</td>
								<td>{{ $booked6Marla }}</td>
								<td>{{ $booked8Marla }}</td>
								<td>{{ $booked10Marla }}</td>
								<td>{{ $booked12Marla }}</td>
								<td>{{ $booked16Marla }}</td>
								<td>{{ $booked24Marla }}</td>
								<td class="bold">{{ $booked2Marla + $booked3Marla + $booked4Marla+$booked5Marla+$booked6Marla+$booked8Marla+$booked10Marla+$booked12Marla+$booked16Marla+$booked24Marla }}</td>
							</tr>
						</tr>
						@php($i++)
						<?php
						$twoRBooked += $booked2Marla;
						$threeRBooked += $booked3Marla;
						$fourRBooked += $booked4Marla;
						$fiveRBooked += $booked5Marla;
						$sixRBooked += $booked6Marla;
						$eightRBooked += $booked8Marla;
						$tenRBooked += $booked10Marla;
						$twelveRBooked += $booked12Marla;
						$sixteenRBooked += $booked16Marla;
						$twentyfourRBooked += $booked24Marla;
					}}
					?>
					
					<tr class="bold">
						<td colspan="4">Total</td>
						<td>{{ $twoRBooked }}</td>
						<td>{{ $threeRBooked }}</td>
						<td>{{ $fourRBooked }}</td>
						<td>{{ $fiveRBooked }}</td>
						<td>{{ $sixRBooked }}</td>
						<td>{{ $eightRBooked }}</td>
						<td>{{ $tenRBooked }}</td>
						<td>{{ $twelveRBooked }}</td>
						<td>{{ $sixteenRBooked }}</td>
						<td>{{ $twentyfourRBooked }}</td>
						<td>{{ $twoRBooked + $threeRBooked + $fourRBooked + $fiveRBooked + $sixRBooked + $eightRBooked + $tenRBooked + $twelveRBooked + $sixteenRBooked + $twentyfourRBooked }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>


		<div id="CremainingFiles">
		<h4>Remaining Files <small class="pull-right"><button class="btn btn-default" id="CprintRemaining">Print</button></small></h4>
		<div class="table-responsive">
			<table class="table table-striped jambo_table bulk_action" id="ReportTbl">
				<thead>
					<tr>
						<th>Sr #</th>
						<th>Batch No</th>
						<th>Agency Name</th>
						<th>Batch Issue Date</th>
						<th>2-M</th>
						<th>3-M</th>
						<th>4-M</th>
						<th>5-M</th>
						<th>06-M</th>
						<th>08-M</th>
						<th>10-M</th>
						<th>12-M</th>
						<th>16-M</th>
						<th>24-M</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@php($agency = [])
					@php($i = 1)
					<?php
					$remaining = [];
					$twoCRemaining       = 0;
					$threeCRemaining       = 0;
					$fourCRemaining       = 0;
					$fiveCRemaining       = 0;
					$sixCRemaining        = 0;
					$eightCRemaining      = 0;
					$tenCRemaining        = 0;
					$twelveCRemaining     = 0;
					$sixteenCRemaining    = 0;
					$twentyfourCRemaining = 0;
					
					$twoCBooked       = 0;
					$threeCBooked       = 0;
					$fourCBooked       = 0;
					$fiveCBooked       = 0;
					$sixCBooked        = 0;
					$eightCBooked      = 0;
					$tenCBooked        = 0;
					$twelveCBooked     = 0;
					$sixteenCBooked    = 0;
					$twentyfourCBooked = 0;
					
					$twoCMarla        = 0;
					$threeCMarla        = 0;
					$fourCMarla        = 0;
					$fiveCMarla        = 0;
					$sixCMarla         = 0;
					$eightCMarla       = 0;
					$tenCMarla         = 0;
					$twelveCMarla      = 0;
					$sixteenCMarla     = 0;
					$twentyfourCMarla  = 0;
					//////////////////////
					$booked2Marla  = 0;
					$booked3Marla  = 0;
					$booked4Marla  = 0;
					$booked5Marla  = 0;
					$booked6Marla  = 0;
					$booked8Marla  = 0;
					$booked10Marla = 0;
					$booked12Marla = 0;
					$booked16Marla = 0;
					$booked24Marla = 0;

					$agency_name = "";
					$batch_no = "";
					$batch_assign_date = "";
					foreach ($filesCommercial as $key => $myfiles) {
						foreach ($myfiles as $key2 => $file) {

							if($file != null)
							{

								if ($file->plotSize->plot_size == '02-M') {
									$remaining[$key]['TwoMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['TwoMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '03-M') {
									$remaining[$key]['ThreeMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['ThreeMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '04-M') {
									$remaining[$key]['FouCMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['FouCMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '05-M') {
									$remaining[$key]['FiveMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['FiveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '06-M') {
									$remaining[$key]['SixMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['SixMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '08-M') {
									$remaining[$key]['EightMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['EightMarlaBooked'][] = 1;
								}
								if ($file->plotSize->plot_size == '10-M') {
									$remaining[$key]['TenMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['TenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '12-M') {
									$remaining[$key]['TwelveMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['TwelveMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '16-M') {
									$remaining[$key]['SixteenMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['SixteenMarlaBooked'][] = 1;
								}

								if ($file->plotSize->plot_size == '24-M') {
									$remaining[$key]['TwentyfouCMarla'][] = 1;
									if($file->is_reserved == 1 and $file->booking->is_cancelled==0)
										$remaining[$key]['TwentyfouCMarlaBooked'][] = 1;
								}

								$batch_no = $file->batch->batch_no ;
								$agency_name = $file->batch->agency->name;
								$batch_assign_date = date('d-m-Y', strtotime($file->batch->batch_assign_date));
							}
						}

						$twoCMarla  = (isset($remaining[$key]['TwoMarla'])) ? count($remaining[$key]['TwoMarla']) : 0;
						$threeCMarla  = (isset($remaining[$key]['ThreeMarla'])) ? count($remaining[$key]['ThreeMarla']) : 0;
						$fourCMarla  = (isset($remaining[$key]['FouCMarla'])) ? count($remaining[$key]['FouCMarla']) : 0;
						$fiveCMarla  = (isset($remaining[$key]['FiveMarla'])) ? count($remaining[$key]['FiveMarla']) : 0;
						$sixCMarla  = (isset($remaining[$key]['SixMarla'])) ? count($remaining[$key]['SixMarla']) : 0;
						$eightCMarla  = (isset($remaining[$key]['EightMarla'])) ? count($remaining[$key]['EightMarla']) : 0;
						$tenCMarla = (isset($remaining[$key]['TenMarla'])) ? count($remaining[$key]['TenMarla']) : 0;
						$twelveCMarla = (isset($remaining[$key]['TwelveMarla'])) ? count($remaining[$key]['TwelveMarla']) : 0;
						$sixteenCMarla = (isset($remaining[$key]['SixteenMarla'])) ? count($remaining[$key]['SixteenMarla']) : 0;
						$twentyfourCMarla = (isset($remaining[$key]['TwentyfouCMarla'])) ? count($remaining[$key]['TwentyfouCMarla']) : 0;

						$booked2Marla  = (isset($remaining[$key]['TwoMarlaBooked'])) ? count($remaining[$key]['TwoMarlaBooked']) : 0;
						$booked3Marla  = (isset($remaining[$key]['ThreeMarlaBooked'])) ? count($remaining[$key]['ThreeMarlaBooked']) : 0;
						$booked4Marla  = (isset($remaining[$key]['FouCMarlaBooked'])) ? count($remaining[$key]['FouCMarlaBooked']) : 0;
						$booked5Marla  = (isset($remaining[$key]['FiveMarlaBooked'])) ? count($remaining[$key]['FiveMarlaBooked']) : 0;
						$booked6Marla  = (isset($remaining[$key]['SixMarlaBooked'])) ? count($remaining[$key]['SixMarlaBooked']) : 0;
						$booked8Marla  = (isset($remaining[$key]['EightMarlaBooked'])) ? count($remaining[$key]['EightMarlaBooked']) : 0;
						$booked10Marla = (isset($remaining[$key]['TenMarlaBooked'])) ? count($remaining[$key]['TenMarlaBooked']) : 0;
						$booked12Marla = (isset($remaining[$key]['TwelveMarlaBooked'])) ? count($remaining[$key]['TwelveMarlaBooked']) : 0;
						$booked16Marla = (isset($remaining[$key]['SixteenMarlaBooked'])) ? count($remaining[$key]['SixteenMarlaBooked']) : 0;
						$booked24Marla = (isset($remaining[$key]['TwentyfouCMarlaBooked'])) ? count($remaining[$key]['TwentyfouCMarlaBooked']) : 0;
						
						?>
						<tr>
							<td>{{ $i }}</td>
							<td>{{ $batch_no }}</td>
							<td><a href="#" target="_blank">{{ $agency_name }}</a></td>
							<td>{{ $batch_assign_date }}</td>
							<td>{{ $twoCMarla - $booked2Marla }}</td>
							<td>{{ $threeCMarla - $booked3Marla }}</td>
							<td>{{ $fourCMarla - $booked4Marla }}</td>
							<td>{{ $fiveCMarla - $booked5Marla }}</td>
							<td>{{ $sixCMarla - $booked6Marla }}</td>
							<td>{{ $eightCMarla - $booked8Marla }}</td>
							<td>{{ $tenCMarla - $booked10Marla }}</td>
							<td>{{ $twelveCMarla - $booked12Marla }}</td>
							<td>{{ $sixteenCMarla - $booked16Marla }}</td>
							<td>{{ $twentyfourCMarla - $booked24Marla }}</td>
							<td class="bold">
								{{
									($twoCMarla - $booked2Marla) + ($threeCMarla - $booked3Marla) +($fourCMarla - $booked4Marla) + ($fiveCMarla - $booked5Marla) + ($sixCMarla - $booked6Marla) + ($eightCMarla - $booked8Marla) + ($tenCMarla - $booked10Marla) + ($twelveCMarla - $booked12Marla) + ($sixteenCMarla - $booked16Marla) + ($twentyfourCMarla - $booked24Marla)

								}}</td>
							</tr>
							@php($i++)
							<?php
							$twoCRemaining += ($twoCMarla - $booked2Marla);
							$threeCRemaining += ($threeCMarla - $booked3Marla);
							$fourCRemaining += ($fourCMarla - $booked4Marla);
							$fiveCRemaining += ($fiveCMarla - $booked5Marla);
							$sixCRemaining += ($sixCMarla - $booked6Marla);
							$eightCRemaining += ($eightCMarla - $booked8Marla);
							$tenCRemaining += ($tenCMarla - $booked10Marla);
							$twelveCRemaining += ($twelveCMarla - $booked12Marla);
							$sixteenCRemaining += ($sixteenCMarla - $booked16Marla);
							$twentyfourCRemaining += ($twentyfourCMarla - $booked24Marla);
						}
						?>
						<tr class="bold">
							<td colspan="4">Total</td>
							<td>{{ $twoCRemaining }}</td>
							<td>{{ $threeCRemaining }}</td>
							<td>{{ $fourCRemaining }}</td>
							<td>{{ $fiveCRemaining }}</td>
							<td>{{ $sixCRemaining }}</td>
							<td>{{ $eightCRemaining }}</td>
							<td>{{ $tenCRemaining }}</td>
							<td>{{ $twelveCRemaining }}</td>
							<td>{{ $sixteenCRemaining }}</td>
							<td>{{ $twentyfourCRemaining }}</td>
							<td>{{ $twoCRemaining + $threeCRemaining + $fourCRemaining + $fiveCRemaining + $sixCRemaining + $eightCRemaining + $tenCRemaining + $twelveCRemaining + $sixteenCRemaining + $twentyfourCRemaining }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			</div>