@extends('admin.layouts.template')
@section('title','Cancel Report')
@section('reports-active','active')
@section('booking-report-active','active')
@section('cancel_report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

 
@endsection

@section('content')
    <div class="content">
        @include('admin/common/breadcrumb',['page'=>'Cancel Report'])
        <div class="x_panel">
          <div class="x_title">
            <h2>Cancel Report</h2>
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.Checkcancelreport', 'class' => 'form-horizontal','method' => 'GET']) !!}
            <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                        {!! Form::label('startdate', 'From') !!}
                        {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
                  </div>
              </div>
                <div class="col-md-3">
                <div class="form-group">
                        {!! Form::label('enddate', 'To') !!}
                        {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
                  </div>
              </div> 
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', 'Status') !!}
                       
                        @php($statusArr = [])
                        @foreach(\App\Helpers::booking_status() as $key => $status)
                            @php($statusArr[$key] = $status)
                        @endforeach
                        {!! Form::select('status', $statusArr, Request::input('status'), ['class' => 'form-control', 'id' => 'status' , 'required' => 'required']) !!}
                     
                    </div>
                </div>
                <div class="col-md-3" style="margin-top: 25px; ">
                <div class="form-group">
                    {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
                </div>
                {!! Form::close() !!}
              </div>
            </div>
            @if(isset($bookings))
            <div class="table-responsive">
                @if($st == "3")
                <table class="table table-striped jambo_table bulk_action table-bordered"> 
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Agency</th>
                        <th>Registration #</th>
                        <th>Date</th>
                        <th>Merged Amount</th>
                        <th>Merged Into</th>
                        <th>Deducted Amount</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i = 1)
                    @foreach($bookings as $key => $booking)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $booking->agency->name }}</td>
                        <td>{{ $booking->registration_no }}</td>
                        <td>{{ date('d-m-Y', strtotime($booking->cancel_date)) }}</td>
                        <td>{{$booking->merge_amount}}</td>
                        <td></td>
                        <td>{{ $booking->deduct_amount }}</td>
                        <td>{{ $booking->cancel_remarks }}</td>
                    </tr>
                    <?php $res = \App\Payments::getmergebook($booking->id); ?>
                    @foreach($res as $key => $mergebook) 
                    
                    <tr>
                        <td colspan="4"></td>
                        <td>{{ $mergebook->amount}}</td>
                        <td>{{ $mergebook->merged_into }}</td>
                        <td colspan="2"></td>
                    </tr>
                    @endforeach
                    @php($i++)
                  
                  @endforeach
                </tbody>
                </table>
                @else
                    <table class="table table-striped jambo_table bulk_action table-bordered" id="dueReportTable"> 
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Agency</th>
                        <th>Registration #</th>
                        <th>Date</th>
                        <th>{{($st == "3") ? "Merged" : "Refunded" }} Amount</th>
                        @if($st == "3")
                        <th>Merged Into</th>
                        @endif
                        <th>Deducted Amount</th>
                        <th>Remarks</th>
                        <!--<th>By</th>-->
                    </tr>
                </thead>
                <tbody>
                  @php($i = 1)
                  @foreach($bookings as $key => $inst)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $inst->agency->name }}</td>
                      <td>{{ $inst->registration_no }}</td>
                     <td>{{ date('d-m-Y', strtotime($inst->cancel_date)) }}</td>
                     <td>{{ ($st == "3") ? $inst->merge_amount : $inst->refund_amount }}</td>
                     @if($st == "3")
                        <td>{{ $inst->merge_into }}</td>
                    @endif
                     <td>{{ ($inst->deduct_amount) ? $inst->deduct_amount:"" }}</td>
                    <td>{{ ($inst->cancel_remarks) ? $inst->cancel_remarks:"" }}</td>
                      <!--<td>{{ ($inst->cancel_by) ? \App\User::getUserInfo($inst->cancel_by)->name : "" }}</td>-->
                    </tr>
                  @php($i++)
                  @endforeach
                </tbody>
                </table>    
                @endif
            </div>
            @endif
        </div>
    </div>      
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    });
   
       var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];
      
    var datatable = $('#dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: buttons
    } );

    </script>
@endsection