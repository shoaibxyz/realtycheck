@extends('admin.layouts.template')
@section('title','Recovery Report')

@section('reports-active','active')
@section('installment-report-active','active')
@section('recovery-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('public/')}}/css/leadger.css" media="print">

<style type="text/css">
    @if(Auth::user()->Roles->first()->name == 'call-center')
      .dt-buttons{
        display: none;
      } 
    @endif
    </style>


@endsection

@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'Recovery Report'])
<div class="x_panel">
  <div class="x_title">
    <h2>Recovery Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['route' => 'report.recovery-report', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('fromdate', 'From') !!}
      {!! Form::text('fromdate', Request::input('fromdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>

  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('todate', 'To') !!}
      {!! Form::text('todate', Request::input('todate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>

  <div class="col-md-3">
      <div class="form-group">
            {!! Form::label('agency', 'Agency') !!}
            <?php
            $agencyArr[''] = "";
            foreach ($agencies as $agency) {
              $agencyArr[$agency->id] = $agency->name;
            }
            ?>
            {!! Form::select('agency_id', $agencyArr, Request::input('agency_id'), ['class' => 'form-control', 'id' => 'agency'] ) !!}
      </div>
  </div>
  
  <div class="col-md-3" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
      <input type="reset" value="Reset" class="btn btn-grey">
    </div>
    {!! Form::close() !!} 
  </div>


<div id="app">
    
    @include('admin.report.brandin-header', ['reportTitle' => "recovery Report"])

    <div role="tabpanel">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#residential" aria-controls="residential" role="tab" data-toggle="tab">Residential</a>
              </li>
              <li role="presentation">
                <a href="#commercial" aria-controls="residential" role="tab" data-toggle="tab">Commercial</a>
              </li>
              <li role="presentation">
                <a href="#housing" aria-controls="housing" role="tab" data-toggle="tab">Housing</a>
              </li>
          </ul>
        
          <!-- Tab panes -->
          <div class="tab-content">

            <div role="tabpanel" class="tab-pane active" id="residential">
                <h3>Month Wise</h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-hover jambo_table" >
                    <thead>
                      <tr>
                        <th>S#</th>
                        <th>Month</th>
                        <th>Due Amount</th>
                        <th>Received Amount</th>
                        <th>Remaining Amount</th>
                        <th>Recovery</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        $start    = new DateTime($from);
                        $start->modify('first day of this month');
                        $end      = new DateTime($to);
                        $end->modify('first day of next month');
                        $interval = DateInterval::createFromDateString('1 month');
                        $period   = new DatePeriod($start, $interval, $end);
                        
                        $tdue = 0;
                        $trec = 0;
                        $tremain = 0;
                        $tpercent = 0;
                        
                        foreach ($period as $dt) 
                        {
                            //echo $dt->format("Y-m-".$start->modify('first day of this month')) . PHP_EOL;
                            $dateObj   = DateTime::createFromFormat('!m', $dt->format("m"));
                            $monthName = $dateObj->format('F');
                            $dateToTest = $dt->format("Y-m-d");
                            //echo date('t',strtotime($dateToTest));
                            $from1 = $dt->format("Y-m") . '-01';
                            $to1 = $dt->format("Y-m") . '-' . date('t',strtotime($dateToTest));
                            $due = \App\Payments::instDue($from1, $to1, $agency_id, 'Residential','month');
                            $tdue += $due;
                            $received = \App\Payments::instRec($from1, $to1, $agency_id, 'Residential','month');
                            //dd($received);
                            $trec += $received;
                            $remaining = $due - $received;
                            $tremain = $tdue - $trec;
                            if(isset($received) and !empty($received) and ($remaining + $received) > 0)
                                $percentage = round($received / ($remaining + $received) * 100, 2) . ' %';
                            else
                                $percentage = "0 %";
                            if($tremain + $trec > 0)
                                $tpercent = round($trec / ($tremain + $trec) * 100, 2) . ' %';  
                            else
                                $tpercent = "0 %";
                        ?>
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $monthName }}</td>
                          <td>{{ number_format($due) }}</td>
                          <td>{{ number_format($received) }}</td>
                          <td>{{ number_format($remaining) }}</td>
                          <td>{{ $percentage }}</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2"><b>Total</b></td>
                        <td><b>{{ number_format($tdue) }}</b></td>
                        <td><b>{{ number_format($trec) }}</b></td>
                        <td><b>{{ number_format($tremain) }}</b></td>
                        <td><b>{{ $tpercent }}</b></td>
                      </tr>
                    </tfoot>
                </table>
              </div>
                <div style="height:20px;"></div>
                <h3>Agency Wise Report</h3>
                <div class="table-responsive">
                    
                </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="commercial">
                <h3>Month Wise Report</h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-hover jambo_table" >
                    <thead>
                      <tr>
                        <th>S#</th>
                        <th>Month</th>
                        <th>Due Amount</th>
                        <th>Received Amount</th>
                        <th>Remaining Amount</th>
                        <th>Recovery</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        //dd($from);
                        $start    = new DateTime($from);
                        $start->modify('first day of this month');
                        $end      = new DateTime($to);
                        $end->modify('first day of next month');
                        $interval = DateInterval::createFromDateString('1 month');
                        $period   = new DatePeriod($start, $interval, $end);
                        
                        $tdue = 0;
                        $trec = 0;
                        $tremain = 0;
                        $tpercent = 0;
                        
                        foreach ($period as $dt) 
                        {
                            //echo $dt->format("Y-m-".$start->modify('first day of this month')) . PHP_EOL;
                            $dateObj   = DateTime::createFromFormat('!m', $dt->format("m"));
                            $monthName = $dateObj->format('F');
                            $dateToTest = $dt->format("Y-m-d");
                            //echo date('t',strtotime($dateToTest));
                            $from2 = $dt->format("Y-m") . '-01';
                            $to2 = $dt->format("Y-m") . '-' . date('t',strtotime($dateToTest));
                            $inst_due = 0;
                            $inst_rec = 0;
                            $due = \App\Payments::instDue($from2, $to2, $agency_id, 'Commercial','month');
                            if($due)
                               $inst_due = $due;
                            $tdue += $inst_due;
                            $received = \App\Payments::instRec($from2, $to2, $agency_id, 'Commercial','month');
                            if($received)
                                $inst_rec = $received;
                            $trec += $inst_rec;
                            $remaining = $inst_due - $inst_rec;
                            $tremain = $tdue - $trec;
                            if(isset($inst_rec) and !empty($inst_rec) and ($remaining + $inst_rec) > 0)
                                $percentage = round($inst_rec / ($remaining + $inst_rec) * 100, 2) . ' %';
                            else
                                $percentage = "0 %";
                            if(($tremain + $trec) > 0)
                                $tpercent = round($trec / ($tremain + $trec) * 100, 2) . ' %';
                            else
                                $tpercent = "0 %";
                        ?>
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $monthName }}</td>
                          <td>{{ number_format($due) }}</td>
                          <td>{{ number_format($received) }}</td>
                          <td>{{ number_format($remaining) }}</td>
                          <td>{{ $percentage }}</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2"><b>Total</b></td>
                        <td><b>{{ number_format($tdue) }}</b></td>
                        <td><b>{{ number_format($trec) }}</b></td>
                        <td><b>{{ number_format($tremain) }}</b></td>
                        <td><b>{{ $tpercent }}</b></td>
                      </tr>
                    </tfoot>
                </table>
              </div>
                <div style="height:20px;"></div>
                <h3>Agency Wise Report</h3>
                <div class="table-responsive">
                  
                </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="housing">
              
                <h3>Month Wise Report</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover jambo_table" >
                    <thead>
                      <tr>
                        <th>S#</th>
                        <th>Month</th>
                        <th>Due Amount</th>
                        <th>Received Amount</th>
                        <th>Remaining Amount</th>
                        <th>Recovery</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        $start    = new DateTime($from);
                        $start->modify('first day of this month');
                        $end      = new DateTime($to);
                        $end->modify('first day of next month');
                        $interval = DateInterval::createFromDateString('1 month');
                        $period   = new DatePeriod($start, $interval, $end);
                        //dd($start);
                        $tdue = 0;
                        $trec = 0;
                        $tremain = 0;
                        $tpercent = 0;
                        
                        foreach ($period as $dt) 
                        {
                            //echo $dt->format("Y-m-".$start->modify('first day of this month')) . PHP_EOL;
                            $dateObj   = DateTime::createFromFormat('!m', $dt->format("m"));
                            $monthName = $dateObj->format('F');
                            $dateToTest = $dt->format("Y-m-d");
                            //echo date('t',strtotime($dateToTest));
                            $from3 = $dt->format("Y-m") . '-01';
                            $to3 = $dt->format("Y-m") . '-' . date('t',strtotime($dateToTest));
                            $due = \App\Payments::instDue($from3, $to3, $agency_id, 'Housing','month');
                            $tdue += $due;
                            $received = \App\Payments::instRec($from3, $to3, $agency_id, 'Housing','month');
                            $trec += $received;
                            $remaining = $due - $received;
                            $tremain = $tdue - $trec;
                            if(isset($received) and !empty($received) and ($remaining + $received) > 0)
                                $percentage = round($received / ($remaining + $received) * 100, 2) . ' %';
                            else
                                $percentage = "0 %";
                            
                            if(($tremain + $trec) > 0)
                                $tpercent = round($trec / ($tremain + $trec) * 100, 2) . ' %';
                            else
                                $tpercent = "0 %";
                        ?>
                        <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $monthName }}</td>
                          <td>{{ number_format($due) }}</td>
                          <td>{{ number_format($received) }}</td>
                          <td>{{ number_format($remaining) }}</td>
                          <td>{{ $percentage }}</td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2"><b>Total</b></td>
                        <td><b>{{ number_format($tdue) }}</b></td>
                        <td><b>{{ number_format($trec) }}</b></td>
                        <td><b>{{ number_format($tremain) }}</b></td>
                        <td><b>{{ $tpercent }}</b></td>
                      </tr>
                    </tfoot>
                </table>
                </div>
        
                <div style="height:20px;"></div>
                <h3>Agency Wise Report</h3>
                <div class="table-responsive">
                  
                </div>
                
            </div>
                
        </div>
    </div>
</div>
</div>

@endsection
@section('javascript')

   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    var columns = [
            { data: 'id'},
            { data: 'registration_no', name: 'registration_no' },
            { data: 'member_name', name: 'member_name' },
            { data: 'phone', name: 'phone' },
            { data: 'payment_desc', name: 'payment_desc' },
            { data: 'plot_size', name: 'plot_size' },
            { data: 'installment_amount', name: 'installment_amount' },
            { data: 'amount_received', name: 'amount_received' },
            { data: 'payment_date', name: 'payment_date' },
        ];

        var to = "{{ Request::input('startdate') }}";
        var from = "{{ Request::input('enddate') }}";

       var table = $('#paidInstTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "info": true,
            "bSort" : false,
            buttons: [
                 'excel', 'print'
            ],
            "ajax": {
                "url": "{{ URL::to('admin/report/getPaidInst/')}}/"+from+"/"+to, 
            },
            "columns": columns
        });


    // jQuery.fn.DataTable.Api.register( 'sum()', function () {
    //   return this.flatten().reduce( function ( a, b ) {
    //         return (a*1) + (b*1); // cast values in-case they are strings
    //       });
    // });

   function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_blank';

                  
                    var WindowObject = window.open('Booking Report', 'PrintWindow' , 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
                    WindowObject.document.writeln('<!DOCTYPE html>');
                    WindowObject.document.writeln('<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title></title>');
                    WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/custom.min.css" rel="stylesheet">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/leadger.css" rel="stylesheet">');
                    WindowObject.document.writeln('<style>.btn-success{display:none;}</style>');
    
                     WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/css/yellow-grey.css" rel="stylesheet">');
                    WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')
        
                    WindowObject.document.writeln($('#'+strid).html());
        
                    WindowObject.document.writeln('</body></html>');
        
                    WindowObject.document.close();
        
                    WindowObject.focus();
                    //WindowObject.close();
                    
                  return true;
    }

    $('#printreceipt_Commercial').click(function(){
      CallPrint('app');
    });

    $('#printreceipt_Housing').click(function(){
      CallPrint('app');
    });

    $('#printreceipt_Residential').click(function(){
      CallPrint('app');
    });

    $('#printreceipt_Commercial-detail').click(function(){
      CallPrint('app');
    });

    $('#printreceipt_Housing-detail').click(function(){
      CallPrint('app');
    });

    $('#printreceipt_Residential-detail').click(function(){
      CallPrint('app');
    });

    
    $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });
  });
</script>
@endsection