@extends('admin.layouts.template')
@section('title','Sale Summary Report')

@section('reports-active','active')
@section('sales-report-active','active')
@section('sale-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection

@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'Sale Summary Report'])
<div class="x_panel">
  <div class="x_title">
    <h2>Sale Summary Report</h2>
    
    <div class="clearfix"></div>
  </div>    
{{--   {!! Form::open(['route' => 'report.saleSummaryReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('fromdate', 'From') !!}
      {!! Form::text('fromdate', Request::input('fromdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('todate', 'To') !!}
      {!! Form::text('todate', Request::input('todate'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
    </div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-default' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>
 --}}

<div id="app">
  {{-- @include('admin.report.brandin-header', ['reportTitle' => "Sale Summary Report"]) --}}

  <div class="table-responsive">
    <table class="table table-bordered table-hover jambo_table" id="saleSummaryTable">
      <thead>
        <tr>
          <th>Sr #</th>
          <th>Agency</th>
          <th>Total Files</th>
          <th>Net Price</th>
          <th>Booking Recev</th>
          <th>Inst Recev</th>
          <th>Balloting Recev</th>
          <th>Total Recev</th>
          <th>Receivable</th>
        </tr>
      </thead>
      <tbody>
        @php($i = 1)
        @php($files = 0)
        @php($totalAmount = 0)
        @php($downPayment = 0)
        @php($installmentReceived = 0)
        @php($ballotingReceived = 0)
        @php($totalReceived = 0)
        @php($currReceievable = 0)
        @if(isset($agencyData))
          @foreach($agencyData as $key => $agency)
          <tr>
            <th>{{ $i++ }}</th>
            <th>{{ $agency['agency'] }}</th>
            <th>{{ $agency['fileQty'] }}</th>
            <th>{{ number_format($agency['totalAmount']) }}</th>
            <th>{{ number_format($agency['downPayment']) }}</th>
            <th>{{ number_format($agency['installmentReceived']) }}</th>
            <th>{{ number_format($agency['ballotingReceived']) }}</th>
            <th>{{ number_format($agency['totalReceived']) }}</th>
            <th>{{ number_format($agency['currReceievable']) }}</th>
          </tr>
           <?php 
              $files += $agency['fileQty'];
              $totalAmount += $agency['totalAmount'];
              $downPayment += $agency['downPayment'];
              $installmentReceived += $agency['installmentReceived'];
              $ballotingReceived += $agency['ballotingReceived'];
              $totalReceived += $agency['totalReceived'];
              $currReceievable += $agency['currReceievable'];
           ?>
          @endforeach
        @endif
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2"  class="text-right"><b>Total:</b></td>
          <td id="files"><b>{{ number_format($files) }}</b></td>   
          <td id="totalAmount"><b>{{ number_format($totalAmount) }}</b></td>   
          <td id="downPayment"><b>{{ number_format($downPayment) }}</b></td>   
          <td id="installmentReceived"><b>{{ number_format($installmentReceived) }}</b></td>   
          <td id="ballotingReceived"><b>{{ number_format($ballotingReceived) }}</b></td>   
          <td id="totalReceived"><b>{{ number_format($totalReceived) }}</b></td>   
          <td id="currReceievable"><b>{{ number_format($currReceievable) }}</b></td>   
        </tr>
      </tfoot>
  </table>
  {{-- <button id="printreceipt" class="btn btn-success btn-xs">Print</button> --}}
</div>
{{-- @include('admin.report.brandin-footer')   --}}


</div>
</div>

@endsection
@section('javascript')

   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    var currentDate = new Date();
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();

    var d = day + "-" + month + "-" + year;

    if("{{ Auth::user()->Roles->first()->name == 'Super-Admin' }}")
      {
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
          }];
      }else{
        var buttons = [];
      }
      
    var datatable = $('#saleSummaryTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
        } );

        jQuery.fn.dataTable.Api.register( 'sum()', function () {
          return this.flatten().reduce( function ( a, b ) {
              return (a*1) + (b*1); // cast values in-case they are strings
          });
      });

        $("#saleSummaryTable").on('search.dt', function() {
          $('#files').html("<b>"+datatable.column( 2, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#totalAmount').html("<b>"+datatable.column( 3, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#downPayment').html("<b>"+datatable.column( 4, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#installmentReceived').html("<b>"+datatable.column( 5, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#ballotingReceived').html("<b>"+datatable.column( 6, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#totalReceived').html("<b>"+datatable.column( 7, {"filter": "applied"} ).data().sum()+"</b>" );
          $('#currReceievable').html("<b>"+datatable.column( 8, {"filter": "applied"} ).data().sum()+"</b>" );
      });

   function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
      var windowUrl = 'about:blank';
      var uniqueName = new Date();
      var windowName = '_self';

      var printWindow = window.open(window.location.href , windowName);
      printWindow.document.body.innerHTML = printContent.innerHTML;
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
      return false;
    }

    $('#printreceipt').click(function(){
      CallPrint('app');
    });

    // var table = $('.jambo_table').DataTable({ 
    //   "bSort" : false,
    //   dom: 'Bfrtip',
    //   buttons: [
    //          'excel',
    //     ],
    // });
      
    // $("#paidInstTable").on('search.dt', function() {
    //   $('#receiving_amt').html(table.column( 6, {"filter": "applied"} ).data().sum() );
    //   $('#inst_sum').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    // });

    
    $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });
  });
</script>
@endsection