@extends('admin.layouts.template')
@section('title','Customer Feedback Report')

@section('reports-active','active')
@section('user-report-active','active')
@section('c-h-customers-report-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <style>
        div .dt-buttons{
            {{$rights->show_print}};
        }
    </style>
@endsection

@section('content')
@include('errors')
@include('admin/common/breadcrumb',['page'=>'Customer Feedback Report'])
        <div class="x_panel">
          <div class="x_title">
            <h2>Customer Feedback Report</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.CustomerFeedbackReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
            {{-- <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div> --}}


          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('agency', 'Agency') !!}
                    <?php
                    $agencyArr[''] = "";
                    foreach ($agencies as $agency) {
                      $agencyArr[$agency->id] = $agency->name;
                    }
                    ?>
                    {!! Form::select('agency_id', $agencyArr, Request::input('agency_id'), ['class' => 'form-control', 'id' => 'agency'] ) !!}
              </div>
          </div>

          {{-- <div class="col-md-5">
            <div class="form-group">
                    {!! Form::label('startdate', 'To') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
              </div>
          </div> --}}

          <div class="col-md-3" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>

          @if(isset($dueAmounts))

          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#Commercial" aria-controls="Commercial" role="tab" data-toggle="tab">Commercial</a>
              </li>
              <li role="presentation">
                <a href="#Housing" aria-controls="Commercial" role="tab" data-toggle="tab">Housing</a>
              </li>
            </ul>
          
            <!-- Tab panes -->
            <div class="tab-content">
              @php($i = 0)
              @foreach($dueAmounts as $key => $plotType)
              <div role="tabpanel" class="tab-pane {{ $i == 0 ? "active" : "" }}" id="{{$key}}">
                <h3>{{ $key}} Files</h3>
                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action table-bordered" id="{{$key}}_dueReportTable"> 
                      <thead>
                          <tr>
                              <th>Sr</th>
                              <th>Reg</th>
                              <th>Agency</th>
                              <th>Due Inst</th>
                              <th>Recv. Inst</th>
                              <th>Due Amount</th>                               
                              <th>Recv. Amount</th>                            
                              <th>Follow up</th>                            
                          </tr>
                      </thead>
                      <tbody>
                        @php($i = 1)
                        @php($regArr = [])
                        @php($due_inst = 0)
                        @php($paid_inst = 0)
                        @php($inst_sum = 0)
                        @php($lpc_sum = 0)
                        @php($paid_sum = 0)
                        @foreach($plotType as $key => $inst)
                          
                             <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $key }}</td>
                            <td>{{ isset($plotType[$key]['bookings']) ? $plotType[$key]['bookings']->agency->name  : "" }}</td>    

                            
                           <td>{{ ($plotType[$key]['inst']==NULL)? '':count($plotType[$key]['inst']) }}</td>
                           
        <td>{{ ($plotType[$key]['paid_inst'] && $plotType[$key]['partial'])?(count($plotType[$key]['paid_inst']) - count($plotType[$key]['partial'])):'' }}</td>
          
          <td>{{ ($plotType[$key]['sum'])?(number_format($plotType[$key]['sum'])):'' }}</td> 
          <td>{{ ($plotType[$key]['paid'])?(number_format($plotType[$key]['paid'])):'' }}</td> 
          <td>{{ ($plotType[$key]['followup'])?($plotType[$key]['followup']):'' }}</td> 
                           
                           
                            
                            
                          </tr>
                          <?php 
                           if($plotType[$key]['inst'])
                           $due_inst += count($plotType[$key]['inst']); 
                           
                           if($plotType[$key]['paid_inst'])
                           $paid_inst += count($plotType[$key]['paid_inst']); 
                           
                           if($plotType[$key]['sum'])
                           $inst_sum += $plotType[$key]['sum']; 
                           
                           if($plotType[$key]['paid'])
                           $paid_sum += $plotType[$key]['paid'];
                               
                           
                          ?>
                          
                        @php($i++)
                        @endforeach
                      </tbody>

                      <tfoot>
                        <tr>
                          <th colspan="3">Total</th>
                          <td id="due_inst">{{ number_format($due_inst) }}</td>
                          <td id="paid_inst">{{ number_format($paid_inst) }}</td>
                          <td id="inst_sum">{{ number_format($inst_sum) }}</td>
                          <td id="paid_sum">{{ number_format($paid_sum) }}</td>
                          <td id="total">{{ number_format($inst_sum + $lpc_sum) }}</td>
                        </tr>
                      </tfoot>
                  </table>
                </div>
              </div>
              @endforeach
            </div>
          </div>

          @endif


      </div>
        
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
      
      var buttons = [];
      // if("{{ Auth::user()->roles[0]->name == 'Super-Admin' }}")
      // {
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
            // exportOptions: {
            //       columns: [1,4]
            //   }
          }];
      // }
      
      if("{{ Auth::user()->roles[0]->name == 'ccd-incharge' }}")
      {
          var buttons = [
          {
            extend: 'excel',
            exportOptions: {
                  columns: [1,4]
              }
          }];
      }

      
     jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });

   var table = $('#Residential_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

    $("#Residential_dueReportTable").on('search.dt', function() {
      $('#inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      $('#paid_sum').html(table.column( 7, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#total').html(table.column( 8, {"filter": "applied"} ).data().sum() );
    });

    var table = $('#Commercial_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

    $("#Commercial_dueReportTable").on('search.dt', function() {
      $('#inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#total').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    });

    var table = $('#Housing_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

    $("#Housing_dueReportTable").on('search.dt', function() {
      $('#inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#total').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    });

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection