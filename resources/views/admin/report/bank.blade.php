@extends('admin.layouts.template')
@section('title','Reports')

@section('reports-active','active')
@section('other-report-active','active')
@section('bank-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Bank Report'])
	@include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Bank Report</h2>
            
            <div class="clearfix"></div>
          </div>    
		
           {!! Form::open(['route' => 'report.bank_report', 'method' => 'GET', 'class' => 'form-horizontal']) !!}      
                 
		<div class="col-md-6">
			<div class="form-group">
	            {!! Form::label('startdate', 'Start Date') !!}
	            {!! Form::text('startdate', \Carbon\Carbon::now()->format('d-m-Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
		    </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
			{!! Form::label('enddate', 'End Date')!!}
			{!! Form::text('enddate', \Carbon\Carbon::now()->format('d-m-Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
			</div>
		</div>
<div class="col-md-6">
		<div class="checkbox">
  {{-- <label><input type="checkbox" value="" id="full">Full</label> --}}
</div></div>
<div class="col-md-6"></div>

		
	            <div class="form-group">
	                {!! Form::submit('Ok', ['class' => 'btn btn-success' , 'id' => 'ok']) !!}
	                {!! Form::submit('Exit', ['class' => 'btn btn-grey' , 'id' => 'exit']) !!}
	            </div>
	           
	        {!! Form::close() !!}
        </div>
        
@endsection
@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'yy/mm/dd'
	    });
    });
    </script>
@endsection