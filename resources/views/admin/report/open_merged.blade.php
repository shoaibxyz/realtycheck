@extends('admin.layouts.template')
@section('title','Open File Report')

@section('reports-active','active')
@section('booking-report-active','active')
@section('open-file-cancel-report-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">


@endsection

@section('content')

  @include('errors')

    <div class="x_panel">
      <div class="x_title">
        <h2>Open File Merged Report</h2>
        <div class="clearfix"></div>
      </div>    
      {!! Form::open(['route' => 'report.Checkmergedreport', 'class' => 'form-horizontal' , 'method' => 'GET']) !!}
        <div class="row">
          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div> 
          
          <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', 'Status') !!}
                       
                        @php($statusArr = [])
                        @foreach(\App\Helpers::booking_status() as $key => $status)
                            @php($statusArr[$key] = $status)
                        @endforeach
                        {!! Form::select('status', $statusArr, Request::input('status'), ['class' => 'form-control', 'id' => 'status' , 'required' => 'required']) !!}
                     
                    </div>
                </div>
          <div class="col-md-3" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-default' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>
        </div>
    @if(isset($files))
      <div id="app">
        @include('admin.report.brandin-header', ['reportTitle' => "Open File Cancel Report"])
      <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action table-bordered" id="dueReportTable"> 
            <thead>
                <tr>
                    <th>Sr #</th>
                    <th>Registration #</th>
                    <th>Date</th>
                    <th>Received Amount</th>
                    <th>Deducted Amount</th>
                    <th>Merged Amount</th>
                    <th>Remarks</th>
                    
                </tr>
            </thead>
            <tbody>
              @php($i = 1)
              @foreach($files as $key => $inst)
                <tr>
                  <td>{{ $i }}</td>
                  <td>{{ $inst->registration_no }}</td>
                 <td>{{ date('d-m-Y', strtotime($inst->cancel_date)) }}</td>
                 <td>{{ ($inst->received_amount) ? $inst->received_amount:"" }}</td>
                 <td>{{ ($inst->deduct_amount) ? $inst->deduct_amount:"" }}</td>
                 <td>{{ ($inst->merged_amount) ? $inst->merged_amount:"" }}</td>
                <td>{{ ($inst->remarks) ? $inst->remarks:"" }}</td>
                  
                </tr>
              @php($i++)
              @endforeach
            </tbody>
            </table>
      </div>
       @include('admin.report.brandin-footer')
      @endif
    </div>
  </div>
        
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection