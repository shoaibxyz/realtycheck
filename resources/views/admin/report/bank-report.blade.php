@extends('admin.layouts.template')
@section('title','Bank Report')
@section('reports-active','active')
@section('sales-report-active','active')
@section('bank-report-active','active')
@section('content')
@include('admin/common/breadcrumb',['page'=>'Bank Report'])
            

        @if(!empty($message))
            <p>{{$message}}</p>
        @endif

        <div class="x_panel">
        <div class="x_title">
            
            <h2>Bank Wise Report</h2>
            <div class="clearfix"></div>
          </div>    
        <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action" id="receiptReportTbl">
            <thead>
                <tr>
                    {{-- <th>Picture</th> --}}
                    <th>Deposit In Bank </th>
                    <th>Deposit In A/C</th>
                    <th>Cash</th>
                    <th>Cheque</th>
                    <th>Online</th>
                    <th>Grand Total</th>
                    
                </tr>
            </thead>
            <tbody>
                  <tr>
                    <td></td>
                    <td></td>               
                    <td>{{ $cash }}</td>
                     <td></td>
                     <td></td>
                     <td class="price1">{{ $cash }}</td>
                  </tr>
                  <tr>
                        <td><strong>Total</strong></td>
                        <td ></td>
                        <td >{{ $cash }}</td>
                        <td></td>
                        <td></td>
                        <td class="total"></td>
                 </tr>

                  <?php $hbl_total = 0; $bankArr = []; $i= 0; $arr = []; ?>

              
                  @foreach($banks as $key => $bank)
                
                    <?php $totalChequeAmt = 0;  ?>

                    @foreach($cheque as $cheq)                        
                      <?php 
                          if($cheq->receiving_mode == "cheque" && $bank->id == $cheq->bank_id)
                          {
                            $totalChequeAmt += $cheq->received_amount; 

                          }
                      ?>
                    @endforeach

                    <?php
                      if($totalChequeAmt > 0)
                      {
                          $arr[$i][] = $totalChequeAmt;
                          $arr[$i][] = $bank->bank_name;  
                          $arr[$i][] = $bank->account;  
                          $i++;
                      }
                    ?>
                  @endforeach

                  @php($bankTotalSum = 0)

                  @foreach($arr as $array)
                    @php($bankTotalSum+=$array[0])
                  @endforeach

                  @foreach($arr as $array)
                    <tr>
                      <td>{{ $array[1] }}</td>
                      <td>{{ $array[2] }}</td>
                      <td></td>
                      <td>{{ $array[0] }}</td>
                      <td></td>
                      <td>{{ $bankTotalSum }}</td>
                    </tr>
                  @endforeach
            </tbody>
        </table>
        </div>
        </div>    
@endsection
@section('javascript')
<script>
   $(document).ready(function () {
    //iterate through each row in the table
    loadgrandtotal();
    });
   function loadgrandtotal() {
    var sum = 0;
    $('.price1').each(function () {
        var prodprice = Number($(this).text());
        sum = sum + prodprice;
    });
    $(".total").text(sum);
}
</script>

<script type="text/javascript">
    $('#receiptReportTbl').dataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: [
      {
        text: 'Export to PDF',
        extend: 'pdfHtml5',
        message: '',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible',
          header: true,
          footer: true,
        },
    },
    {
        extend: 'print',
        customize: function ( win ) {
          $(win.document.body)
              .css( 'font-size', '10pt' );

          $(win.document.body).find( 'table' )
              .addClass( 'compact' )
              .css( 'font-size', 'inherit' );
        }
      },
]
    } );
    </script>

@endsection