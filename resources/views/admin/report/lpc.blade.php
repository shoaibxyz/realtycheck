@extends('admin.layouts.template')
@section('title','LPC Report')

@section('reports-active','active')
@section('other-report-active','active')
@section('lpc-report-active','active')
@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection

@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'LPC Report'])
<div class="x_panel">
  <div class="x_title">
    <h2>LPC Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['class' => 'form-horizontal','id' => 'lpcReportFilters']) !!}
  
  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('registration_no', 'Reg #') !!}
      {!! Form::text('registration_no', null,  array('class'=>'form-control', 'id' => 'registration_no') )!!}
    </div>
  </div>

  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>


<div id="app">
  <div style="overflow-x: auto">
      
    <table class="table table-bordered table-hover jambo_table" style="overflow-x: scroll; width: 100%" id="lpcTable">
      <thead>
        <tr>
          <th>Reg #</th>
          <th>Install #</th>
          <th>Calculation Date</th>
          <th>Days</th>
          <th>Lpc Amount</th>
          <th>Waive Amount</th>
          <th>Amount Received</th>
          <th>OS Amt</th>
          <th>Remarks</th>
        </tr>
      </thead>
      
  </table>
</div>



</div>
</div>

@endsection

@section('javascript')
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    
          var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          }];
     

  $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });

  var columns = [
        { data: 'registration_no', name: 'registration_no' },
        { data: 'installment_no', name: 'installment_no'},
        { data: 'calculation_date', name: 'calculation_date'},
        { data: 'days', name: 'days'},
        { data: 'lpc_amount', name: 'lpc_amount'},
        { data: 'waive_amount', name: 'waive_amount'},
        { data: 'amount_received', name: 'amount_received'},
        { data: 'os_amount', name: 'os_amount'},
        { data: 'waive_remarks', name: 'waive_remarks'},
    ];
    $('#lpcReportFilters').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

    $.fn.dataTable.ext.buttons.export =
    {
        className: 'buttons-alert',
        id: 'ExportButton',
        text: "Export All Test III",
        action: function (e, dt, node, config)
        {
            var SearchData = dt.rows({ filter: 'applied' }).data();
            var SearchData1 = dt.search();
            console.log(SearchData);
            var OrderData = dt.order();
            console.log(SearchData1);
            var NumCol = SearchData[0].length;
            var NumRow = SearchData.length;
            var SearchData2 = [];
            for (j = 0; j < NumRow; j++)
            {
                var NewSearchData = SearchData[j];
                for (i = 0; i < NewSearchData.length; i++)
                {
                    NewSearchData[i] = NewSearchData[i].replace("<div class='Scrollable'>", "");
                    NewSearchData[i] = NewSearchData[i].replace("</div>", "");
                }
                SearchData2.push([NewSearchData]);
            }

            for (i = 0; i < SearchData2.length; i++)
            {
                for (j = 0; j < SearchData2[i].length; j++)
                {
                    SearchData2[i][j] = SearchData2[i][j].join('::');
                }
            }
            SearchData2 = SearchData2.join("%%");
            window.location.href = './ServerSide.php?ExportToExcel=Yes';
        }
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
   var table = $('#lpcTable').DataTable({
        "processing": true,
        "bDestroy":true,
        dom: 'Bfrtip',
        "serverSide": true,
         bFilter: false,
         bSort: true,
        fixedHeader: true,
        "bPaginate": false,
        "info": true,
        "order": [0, "asc" ],
        ajax: {
            method : "post",
            url: '{!! route('report.lpcReport') !!}',
            data: function (d) {
                d.registration_no = $('#registration_no').val();                
            }
        },
        "columns": columns,
        "buttons": [
        {
            extend: 'collection',
            text: 'Export',
            buttons: ['export', 'excel', 'csv', 'pdf', { extend: 'excel',
                text: 'Export Current Page',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                },
                customize: function (xlsx)
                {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row:first c', sheet).attr('s', '7');
                }
            }]
        }
        ]
    });
  </script>
@endsection