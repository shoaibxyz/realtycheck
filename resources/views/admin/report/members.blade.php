@extends('admin.layouts.template')
@section('title','Members Report')

@section('reports-active','active')
@section('user-report-active','active')
@section('members-report-active','active')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<style>
    div .dt-buttons{
        {{$rights->show_print}};
    }
</style>
@endsection

@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'Members Report'])

<div class="x_panel">
  <div class="x_title">
    <h2>Members Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['class' => 'form-horizontal','id' => 'memberReportFilters']) !!}
  
  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('member_name', 'Member Name') !!}
      {!! Form::text('member_name', null,  array('class'=>'form-control', 'id' => 'member_name') )!!}
    </div>
  </div>

  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('plot_size', 'Plot Size') !!}
      @php($plotSizeArr = [])
      @php($plotSizeArr['']  = 'Choose One')
      @foreach($plotSize as $plot_size)
        @php($plotSizeArr[$plot_size->id] = $plot_size->plot_size)
      @endforeach
      {!! Form::select('plot_size', $plotSizeArr, 0,  array('class'=>'form-control', 'id' => 'plot_size') )!!}
    </div>
  </div>

  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('plot_type', 'Plot Type') !!}
      {!! Form::select('plot_type', ['' => 'Choose One' , 'Residential' => 'Residential' , 'Commercial' => 'Commercial', 'Housing' => 'Housing'], 0,  array('class'=>'form-control', 'id' => 'plot_type') )!!}
    </div>
  </div>


  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('agency', 'Agency') !!}
      @php($agencyArr = [])
      @php($agencyArr['']  = 'Choose One')
      @foreach($agencies as $agency)
        @php($agencyArr[$agency->id] = $agency->name)
      @endforeach
      {!! Form::select('agency', $agencyArr, 0,  array('class'=>'form-control', 'id' => 'agency') )!!}
    </div>
  </div>

  <div class="col-md-3">
    <div class="form-group">
      {!! Form::label('payment_plan', 'Payment Plan') !!}
       @php($paymentPlanArr = [])
      @php($paymentPlanArr['']  = 'Choose One')
      @foreach($paymentPlan as $plan)
        @php($paymentPlanArr[$plan->id] = $plan->payment_code)
      @endforeach
      {!! Form::select('payment_plan', $paymentPlanArr, 0,  array('class'=>'form-control', 'id' => 'payment_plan') )!!}
    </div>
  </div>

  <!--<div class="col-md-3">-->
  <!--  <div class="form-group">-->
  <!--    {!! Form::label('due', 'Due') !!}-->
  <!--    {!! Form::checkbox('due',1, false, ['class' => 'form-control','id' => 'due']) !!}-->
  <!--  </div>-->
  <!--</div>-->

  <!--<div class="col-md-3">-->
  <!--  <div class="form-group">-->
  <!--    {!! Form::label('overdue', 'Overdue') !!}-->
  <!--    {!! Form::checkbox('overdue',1, false, ['class' => 'form-control', 'id' => 'overdue']) !!}-->
  <!--  </div>-->
  <!--</div>-->

  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>


<div id="app">
  <div style="overflow-x: auto">
    <table class="table table-bordered table-hover jambo_table" style="overflow-x: scroll; width: 100%" id="membersTable">
      <thead>
        <tr>
          <th style="width: 10%;">Reg #</th>
          <th style="width: 400px;">Name</th>
          <th style="width: 10%;">Plan</th>
          <th style="width: 10%;">Block #</th>
          <th style="width: 10px;">Street #</th>
          <th style="width: 10px;">Plot #</th>
          <th style="width: 10px;">T. Price</th>
          <th style="width: 10px;">Rebate Amt</th>
          <th style="width: 10px;">T. Price net</th>
          <th style="width: 10px;">T. Recev.</th>
          <th style="width: 10px;">T. OS</th>
          <th style="width: 10px;">T. overdue amt</th>
          <th style="width: 10px;">Booking Price</th>
          <th style="width: 10px;">Inst. Due amt</th>
          <th style="width: 10px;">Inst. Rebate</th>
          <th style="width: 10px;">Inst. Recev</th>
          <th style="width: 10px;">Inst. OS amt</th>
          <th style="width: 10px;">Oth. due amt</th>
          <th style="width: 10px;">Oth. Rebate</th>
          <th style="width: 10px;">Oth. recev amt</th>
          <th style="width: 10px;">Oth. OS amt</th>
      <th style="width: 10px;">LPC due amt</th>
      <th style="width: 10px;">LPC waive</th>
      <th style="width: 10px;">LPC recev</th>
      <th style="width: 10px;">LPC os</th>
          <th style="width: 10px;">T. Inst</th>
          <th style="width: 10px;">T. OS</th>
      <th style="width: 10px;">Inst. Od</th>
          <th style="width: 10px;">Agency</th>
          <th style="width: 10px;">Plot Size</th>
          <th style="width: 10px;">Phone R.</th>
          <th style="width: 10px;">Phone O.</th>
          <th style="width: 10px;">Mobile</th>
          <th style="width: 10px;">Booking Date</th>
          <th style="width: 10px;">CNIC</th>
          <th style="width: 10px;">Curr Mon Inst</th>
          <th style="width: 10px;">Status</th>
        </tr>
      </thead>
      
  </table>
</div>


</div>
</div>

@endsection

@section('javascript')
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    if("{{ Auth::user()->Roles->first()->name }}" == 'Super-Admin' || "{{ Auth::user()->Roles->first()->name }}" == 'Admin'){
          var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          }];
      }else{
        var buttons = [];
      }

  $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });

  var columns = [
        { data: 'registration_no', name: 'registration_no' },
        { data: 'name', name: 'name'},
        { data: 'payment_plan', name: 'payment_plan'},
        { data: 'block_no', name: 'block_no'},
        { data: 'street_no', name: 'street_no'},
        { data: 'plot_no', name: 'plot_no'},
        { data: 'total_price', name: 'total_price'},
        { data: 'rebate_amount', name: 'rebate_amount'},
        { data: 'total_net_price', name: 'total_net_price'},
        { data: 'total_received', name: 'total_received'},
        { data: 'total_os_amount', name: 'total_os_amount'},
        { data: 'total_od', name: 'total_od'},
        { data: 'booking_price', name: 'booking_price'},
        { data: 'installment_due_amount', name: 'installment_due_amount'},
        { data: 'installment_rebate', name: 'installment_rebate'},
        { data: 'total_installment_paid', name: 'total_installment_paid'},
        { data: 'total_installment_os', name: 'total_installment_os'},
        { data: 'other_due_amount', name: 'other_due_amount'},
        { data: 'other_rebate_amount', name: 'other_rebate_amount'},
        { data: 'other_recev_amount', name: 'other_recev_amount'},
        { data: 'other_os_amount', name: 'other_os_amount'},
        { data: 'lpc_due', name: 'lpc_due'},
        { data: 'lpc_waive', name: 'lpc_waive'},
        { data: 'lpc_receive', name: 'lpc_receive'},
        { data: 'lpc_os', name: 'lpc_os'},
        { data: 'total_installments', name: 'total_installments'},
        { data: 'unpaid_inst_count', name: 'unpaid_inst_count'},
        { data: 'installments_od', name: 'installments_od'},
        { data: 'agency_name', name: 'agency_name'},
        { data: 'plot_size', name: 'plot_size'},
        { data: 'phone_res', name: 'phone_res'},
        { data: 'phone_office', name: 'phone_office'},
        { data: 'phone_mobile', name: 'phone_mobile'},
        { data: 'payment_date', name: 'payment_date'},
        { data: 'cnic', name: 'cnic'},
        { data: 'curr_inst', name: 'curr_inst'},
        { data: 'registration_status', name: 'registration_status'}
    ];
    $('#memberReportFilters').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

    $.fn.dataTable.ext.buttons.export =
    {
        className: 'buttons-alert',
        id: 'ExportButton',
        text: "Export All Test III",
        action: function (e, dt, node, config)
        {
            var SearchData = dt.rows({ filter: 'applied' }).data();
            var SearchData1 = dt.search();
            console.log(SearchData);
            var OrderData = dt.order();
            console.log(SearchData1);
            var NumCol = SearchData[0].length;
            var NumRow = SearchData.length;
            var SearchData2 = [];
            for (j = 0; j < NumRow; j++)
            {
                var NewSearchData = SearchData[j];
                for (i = 0; i < NewSearchData.length; i++)
                {
                    NewSearchData[i] = NewSearchData[i].replace("<div class='Scrollable'>", "");
                    NewSearchData[i] = NewSearchData[i].replace("</div>", "");
                }
                SearchData2.push([NewSearchData]);
            }

            for (i = 0; i < SearchData2.length; i++)
            {
                for (j = 0; j < SearchData2[i].length; j++)
                {
                    SearchData2[i][j] = SearchData2[i][j].join('::');
                }
            }
            SearchData2 = SearchData2.join("%%");
            window.location.href = './ServerSide.php?ExportToExcel=Yes';
        }
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
   var table = $('#membersTable').DataTable({
        "processing": true,
        dom: 'Bfrtip',
        "serverSide": true,
         bFilter: false,
         bSort: false,
        "pageLength": 20,
        "bPaginate": false,
        fixedHeader: true,
        "info": true,
        ajax: {
            method : "post",
            url: '{!! route('report.getMembers') !!}',
            data: function (d) {
                d.member_name = $('#member_name').val();
                d.plot_size = $('#plot_size').val();
                d.plot_type = $('#plot_type').val();
                d.agency = $('#agency').val();
                d.payment_plan = $('#payment_plan').val();
                d.due = $('#due').val();
                d.overdue = $('#overdue').val();
            }
        },
        "columns": columns,
        "buttons": [
        {
            extend: 'collection',
            text: 'Export',
            buttons: ['export', 'excel', 'csv', 'pdf', { extend: 'excel',
                text: 'Export Current Page',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                },
                customize: function (xlsx)
                {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row:first c', sheet).attr('s', '7');
                }
            }]
        }
        ]
    });
  </script>
@endsection