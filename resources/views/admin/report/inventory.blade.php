@extends('admin.layouts.template')
@section('title','Inventory Report')

@section('reports-active','active')
@section('booking-report-active','active')
@section('inventory-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <style>
      @media print {

        #inventory_print{display:none;}
tr{
    background-color: #1a4567 !important;
    -webkit-print-color-adjust: exact; 
}


}



    </style>
   
@endsection



@section('content')
@include('errors')
@include('admin/common/breadcrumb',['page'=>'Inventory Report'])
        <div class="x_panel">
          <div class="x_title">
            <h2>Inventory Report</h2>
            
            <div class="clearfix"></div>
          </div>    

          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#Residential" aria-controls="Residential" role="tab" data-toggle="tab">Residential</a>
              </li>
              <li role="presentation">
                <a href="#Commercial" aria-controls="Commercial" role="tab" data-toggle="tab">Commercial</a>
              </li>
              <li role="presentation">
                <a href="#Housing" aria-controls="Housing" role="tab" data-toggle="tab">Housing</a>
              </li>
            </ul>

            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="Residential">

                  @include('admin.report._residential-inventory', [
                    'filesArr' => $filesArr, 
                    'nature' => 'Residential', 
                    'agencyOldIssuedFiles' => $agencyOldIssuedFiles,
                    'agencyNewIssuedFiles' => $agencyNewIssuedFiles,
                    'agencyOldBookedFiles' => $agencyOldBookedFiles,
                    'agencyNewBookedFiles' => $agencyNewBookedFiles,
                    'residentialOpenBookings' => $residentialOpenBookings
                  ])

              </div>
              <div role="tabpanel" class="tab-pane" id="Commercial">
                  @include('admin.report._commercial-inventory', [
                    'filesArr' => $filesArr, 
                    'nature' => 'Commercial', 
                    'agencyOldIssuedFiles' => $agencyOldIssuedFiles,
                    'agencyNewIssuedFiles' => $agencyNewIssuedFiles,
                    'agencyOldBookedFiles' => $agencyOldBookedFiles,
                    'agencyNewBookedFiles' => $agencyNewBookedFiles,
                    'agencyRemainingFiles' => $agencyRemainingFiles,
                    'commercialOpenBookings' => $commercialOpenBookings,
                    'commercialOpenBookingsArr' => $commercialOpenBookingsArr

                  ])
              </div>
              <div role="tabpanel" class="tab-pane" id="Housing">
                  @include('admin.report._housing-inventory', [
                    'filesArr' => $filesArr, 
                    'nature' => 'Housing', 
                    'agencyOldIssuedFiles' => $agencyOldIssuedFiles,
                    'agencyNewIssuedFiles' => $agencyNewIssuedFiles,
                    'agencyOldBookedFiles' => $agencyOldBookedFiles,
                    'agencyNewBookedFiles' => $agencyNewBookedFiles,
                    'agencyRemainingFiles' => $agencyRemainingFiles,
                   
                    'housingOpenBookingsArr' => $housingOpenBookingsArr

                  ])
              </div>
            </div>


           


      </div>
        
@endsection
@section('javascript')

  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

   <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">

      if("{{ Auth::user()->Roles->first()->name == 'Super-Admin' }}")
      {
          var buttons = [
          // {
          //     text: 'Export to PDF',
          //     extend: 'pdfHtml5',
          //     message: '',
          //     // orientation: 'landscape',
          //     exportOptions: {
          //       columns: ':visible',
          //       header: true,
          //       footer: true,
          //     },
          //   },
          {
              extend: 'print',
               header: true,
               footer: true,
              // customize: function ( win ) {
              //   $(win.document.body)
              //       .css( 'font-size', '10pt' );

              //   $(win.document.body).find( 'table' )
              //       .addClass( 'compact' )
              //       .css( 'font-size', 'inherit' );
              // }
          },
          ];
      }else{
        var buttons = [];
      }

    $('#ReportTbl').dataTable({ 
      dom: 'Bfrtip',
      bSort: false,
      "bPaginate": false,
      buttons: buttons
    } );

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });

    function printData(divId)
    {
      var printContent = document.getElementById(divId);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_blank';

                  
                    var WindowObject = window.open('Inventory Report', 'PrintWindow' , 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
                    WindowObject.document.writeln('<!DOCTYPE html>');
                    WindowObject.document.writeln('<html><head><title></title>');
                    WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/custom.min.css" rel="stylesheet">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/leadger.css" rel="stylesheet">');
                    WindowObject.document.writeln('<style>.inventory_print{display:none;}</style>');
    
                     WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/css/yellow-grey.css" rel="stylesheet">');
                    WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')
        
                    WindowObject.document.writeln($('#'+divId).html());
        
                    WindowObject.document.writeln('</body></html>');
        
                    WindowObject.document.close();
        
                    WindowObject.focus();
                    //WindowObject.close();
                    
                  return true;
    }

    // $('button').on('click',function(){
    //   printData();
    // });

    $('#commercial_inventory_print').on('click',function(){
      printData('Commercial');
    })

    $('#residential_inventory_print').on('click',function(){
      printData('Residential');
    })

    $('#housing_inventory_print').on('click',function(){
      printData('Housing');
    })
    </script>
@endsection