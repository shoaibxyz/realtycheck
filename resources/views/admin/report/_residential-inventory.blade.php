
              @php($i=1)        
                  <h3>Residential - Inventory Report</h3>
                  <button id="residential_inventory_print" class="btn btn-success inventory_print" style="{{$rights->show_print}}">Print</button>
                  <div class="table-responsive">
                  <table class="table table-hover table-bordered jambo_table bulk_action" id="residentialPrintTable" style="width: 100%; overflow-x: scroll;" >
                    <thead>
                      <tr>
                        <th rowspan="2">Sr. No</th>
                        <th rowspan="2" style="width: 70px;">Estate</th>
                        <th colspan="3">Batch 1</th>
                        <th colspan="3">Batch 2</th>
                        <th colspan="3">Batch 3</th>
                        <!--<th colspan="3">Batch 4</th>-->
                        <!--<th colspan="3">Batch 5</th>-->
                        <th colspan="3">Extended Batch</th>
                        <th colspan="2">LOB</th>
                        <th colspan="3">Issued files</th>
                        <th colspan="3">Booked Files</th>
                        <th colspan="3">Bal. Files</th>
                      </tr>
                      <tr>
                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>


                        <th>N</th>
                        <th>T</th>

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                        <!--<th>O</th>-->
                        <!--<th>N</th>-->
                        <!--<th>T</th>-->

                        <!--<th>O</th>-->
                        <!--<th>N</th>-->
                        <!--<th>T</th>-->

                        <th>O</th>
                        <th>N</th>
                        <th>T</th>

                      </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                       @php($i = 1)
                       @php($batch1OldSum = 0)
                       @php($batch1NewSum = 0)
                       @php($batch1TotalSum = 0)

                       @php($batch2OldSum = 0)
                       @php($batch2NewSum = 0)
                       @php($batch2TotalSum = 0)

                       @php($batch3OldSum = 0)
                       @php($batch3NewSum = 0)
                       @php($batch3TotalSum = 0)

                       @php($batch4OldSum = 0)
                       @php($batch4NewSum = 0)
                       @php($batch4TotalSum = 0)

                       @php($batch5OldSum = 0)
                       @php($batch5NewSum = 0)
                       @php($batch5TotalSum = 0)

                       @php($batchEBOldSum = 0)
                       @php($batchEBNewSum = 0)
                       @php($batchEBTotalSum = 0)

                       @php($batchLOBOldSum = 0)
                       @php($batchLOBNewSum = 0)
                       @php($batchLOBTotalSum = 0)


                       @php($IssuedOldSum = 0)
                       @php($IssuedNewSum = 0)
                       @php($IssuedTotalSum = 0)

                       @php($BookedOldSum = 0)
                       @php($BookedNewSum = 0)
                       @php($BookedTotalSum = 0)
                       

                       @php($RemainingOldSum = 0)
                       @php($RemainingNewSum = 0)
                       @php($RemainingTotalSum = 0)


                       @foreach($filesArr['Residential'] as $agency => $inventory)
                          
                      <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td style="width: 70px;">{{ $agency }}</td>

                        {{-- Batch 1 --}}
                        <td align="center">
                          @if(isset($inventory[1]) && isset($inventory[1]['old']))
                            @php($batch1Old = 0)
                            @foreach($inventory[1]['old'] as $files)
                              @php($batch1Old+=$files)
                            @endforeach
                            @php($batch1OldSum += $batch1Old)
                            {{ $batch1Old }}
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center">
                          @if(isset($inventory[1]) && isset($inventory[1]['new']))
                            @php($batch1New = 0)
                            @foreach($inventory[1]['new'] as $files)
                              @php($batch1New+=$files)
                            @endforeach
                            @php($batch1NewSum += $batch1New)
                            {{ $batch1New }}
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center"><strong>
                          @if(isset($inventory[1]) && isset($inventory[1]['old']) && isset($inventory[1]) && isset($inventory[1]['new']))
                            {{ $batch1Old + $batch1New }}
                            @php($batch1TotalSum += $batch1Old + $batch1New)
                          @elseif(isset($inventory[1]) && isset($inventory[1]['old']))
                            {{ $batch1Old }}
                            @php($batch1TotalSum += $batch1Old)
                          @elseif(isset($inventory[1]) && isset($inventory[1]['new']))
                            {{ $batch1New }}
                            @php($batch1TotalSum += $batch1New)
                          @else 
                            -
                          @endif
                        </strong></td>

                        {{-- Batch 2 --}}
                        <td align="center">
                          @if(isset($inventory[2]) && isset($inventory[2]['old']))
                            @php($batch2Old = 0)
                            @foreach($inventory[2]['old'] as $files)
                              @php($batch2Old+=$files)
                            @endforeach
                            {{ $batch2Old}}
                            @php($batch2OldSum += $batch2Old)
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center">
                          @if(isset($inventory[2]) && isset($inventory[2]['new']))
                            @php($batch2New = 0)
                            @foreach($inventory[2]['new'] as $files)
                              @php($batch2New+=$files)
                            @endforeach
                            {{ $batch2New }}
                            @php($batch2NewSum += $batch2New)
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center"><strong>
                          @if(isset($inventory[2]) && isset($inventory[2]['old']) && isset($inventory[2]) && isset($inventory[2]['new']))
                            {{ $batch2Old + $batch2New }}
                            @php($batch2TotalSum += $batch2New + $batch2Old)
                          @elseif(isset($inventory[2]) && isset($inventory[2]['old']))
                            {{ $batch2Old }}
                            @php($batch2TotalSum += $batch2Old)
                          @elseif(isset($inventory[2]) && isset($inventory[2]['new']))
                            {{ $batch2New }}
                            @php($batch2TotalSum += $batch2New)
                          @else
                            -
                          @endif
                        </strong></td>

                        {{-- Batch 3 --}}
                        <td align="center">
                          @if(isset($inventory[3]) && isset($inventory[3]['old']))
                            @php($batch3Old = 0)
                            @foreach($inventory[3]['old'] as $files)
                              @php($batch3Old+=$files)
                            @endforeach
                            {{ $batch3Old}}
                            @php($batch3OldSum += $batch3Old)
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center">
                          @if(isset($inventory[3]) && isset($inventory[3]['new']))
                            @php($batch3New = 0)
                            @foreach($inventory[3]['new'] as $files)
                              @php($batch3New+=$files)
                            @endforeach
                            {{ $batch3New }}
                            @php($batch3NewSum += $batch3New)
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center"><strong>
                          @if(isset($inventory[3]) && isset($inventory[3]['old']) && isset($inventory[3]) && isset($inventory[3]['new']))
                            {{ $batch3Old + $batch3New }}
                            @php($batch3TotalSum += $batch3New + $batch3Old)
                          @elseif(isset($inventory[3]) && isset($inventory[3]['old']))
                            {{ $batch3Old }}
                            @php($batch3TotalSum += $batch3Old)
                          @elseif(isset($inventory[3]) && isset($inventory[3]['new']))
                            {{ $batch3New }}
                            @php($batch3TotalSum += $batch3New)
                          @else 
                            -
                          @endif
                        </strong></td>

                        {{-- Batch 4 --}}
                        <!--<td align="center">-->
                        <!--  @if(isset($inventory[4]) && isset($inventory[4]['old']))-->
                        <!--    @php($batch4Old = 0)-->
                        <!--    @foreach($inventory[4]['old'] as $files)-->
                        <!--      @php($batch4Old+=$files)-->
                        <!--    @endforeach-->
                        <!--    {{ $batch4Old}}-->
                        <!--    @php($batch4OldSum += $batch4Old)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</td>-->
                        <!--<td align="center">-->
                        <!--  @if(isset($inventory[4]) && isset($inventory[4]['new']))-->
                        <!--    @php($batch4New = 0)-->
                        <!--    @foreach($inventory[4]['new'] as $files)-->
                        <!--      @php($batch4New+=$files)-->
                        <!--    @endforeach-->
                        <!--    {{ $batch4New }}-->
                        <!--    @php($batch4NewSum += $batch4New)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</td>-->
                        <!--<td align="center"><strong>-->
                        <!--  @if(isset($inventory[4]) && isset($inventory[4]['old']) && isset($inventory[4]) && isset($inventory[4]['new']))-->
                        <!--    {{ $batch4Old + $batch4New }}-->
                        <!--    @php($batch4TotalSum += $batch4Old + $batch4New)-->
                        <!--  @elseif(isset($inventory[4]) && isset($inventory[4]['old']))-->
                        <!--    {{ $batch4Old }}-->
                        <!--    @php($batch4TotalSum += $batch4Old)-->
                        <!--  @elseif(isset($inventory[4]) && isset($inventory[4]['new']))-->
                        <!--    {{ $batch4New }}-->
                        <!--    @php($batch4TotalSum += $batch4New)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</strong></td>-->

                        {{-- Batch 5 --}}
                        <!--<td align="center">-->
                        <!--  @if(isset($inventory[5]) && isset($inventory[5]['old']))-->
                        <!--    @php($batch5Old = 0)-->
                        <!--    @foreach($inventory[5]['old'] as $files)-->
                        <!--      @php($batch5Old+=$files)-->
                        <!--    @endforeach-->
                        <!--    {{ $batch5Old}}-->
                        <!--    @php($batch5OldSum += $batch5Old)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</td>-->
                        <!--<td align="center">-->
                        <!--  @if(isset($inventory[5]) && isset($inventory[5]['new']))-->
                        <!--    @php($batch5New = 0)-->
                        <!--    @foreach($inventory[5]['new'] as $files)-->
                        <!--      @php($batch5New+=$files)-->
                        <!--    @endforeach-->
                        <!--    {{ $batch5New }}-->
                        <!--    @php($batch5NewSum += $batch5New)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</td>-->
                        <!--<td align="center"><strong>-->
                        <!--  @if(isset($inventory[5]) && isset($inventory[5]['old']) && isset($inventory[5]) && isset($inventory[5]['new']))-->
                        <!--    {{ $batch5Old + $batch5New }}-->
                        <!--    @php($batch5TotalSum += $batch5New + $batch5Old)-->
                        <!--  @elseif(isset($inventory[5]) && isset($inventory[5]['old']))-->
                        <!--    {{ $batch5Old }}-->
                        <!--    @php($batch5TotalSum += $batch5Old)-->
                        <!--  @elseif(isset($inventory[5]) && isset($inventory[5]['new']))-->
                        <!--    {{ $batch5New }}-->
                        <!--    @php($batch5TotalSum += $batch5New)-->
                        <!--  @else -->
                        <!--    --->
                        <!--  @endif-->
                        <!--</strong></td>-->

                         {{-- Batch Extended --}}
                        <td align="center">
                          @if(isset($inventory['EB']) && isset($inventory['EB']['old']))
                             @php($EBOld = 0)
                            @foreach($inventory['EB']['old'] as $files)
                              @php($EBOld+=$files)
                            @endforeach
                            {{ $EBOld}}
                            @php($batchEBOldSum += $EBOld)
                          @else 
                            -
                          @endif
                        </td>

                        <td align="center">
                          @if(isset($inventory['EB']) && isset($inventory['EB']['new']))
                             @php($EBnew = 0)
                            @foreach($inventory['EB']['new'] as $files)
                              @php($EBnew+=$files)
                            @endforeach
                            {{ $EBnew}}
                            @php($batchEBNewSum += $EBnew)
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center"><strong>
                          @if(isset($inventory['EB']) && isset($inventory['EB']['old']) && isset($inventory['EB']) && isset($inventory['EB']['new']))
                            {{ $EBOld + $EBnew }}
                            @php($batchEBTotalSum += $EBOld + $EBnew)
                          @elseif(isset($inventory['EB']) && isset($inventory['EB']['old']))
                            {{ $EBOld }}
                            @php($batchEBTotalSum += $EBOld)
                          @elseif(isset($inventory['EB']) && isset($inventory['EB']['new']))
                            {{ $EBnew }}
                            @php($batchEBTotalSum += $EBnew)
                          @else 
                            -
                          @endif
                        </strong></td>


                        {{-- Batch LOB --}}
                        <td align="center">
                          @php($LOB = 0)
                          @if(isset($inventory['LOB']) && isset($inventory['LOB']['new']))
                              @foreach($inventory['LOB']['new'] as $files)
                                @php($LOB+=$files)
                              @endforeach
                              {{ $LOB}}
                              @php($batchLOBNewSum += $LOB)
                          @else 
                            -
                          @endif
                        </td>

                      {{--   <td align="center">
                          @php($LOB2 = 0)
                          @if(isset($inventory['LOB-2']) && isset($inventory['LOB-2']['new']))
                              @foreach($inventory['LOB-2']['new'] as $files)
                                @php($LOB2+=$files)
                              @endforeach
                              {{ $LOB2}}
                          @else 
                            -
                          @endif
                        </td>
                        <td align="center">
                          @php($LOB3 = 0)
                          @if(isset($inventory['LOB-3']) && isset($inventory['LOB-3']['new']))
                              @foreach($inventory['LOB-3']['new'] as $files)
                                @php($LOB3+=$files)
                              @endforeach
                              {{ $LOB3}}
                          @else 
                            -
                          @endif
                        </td>

                        <td align="center">
                          @php($LOB4 = 0)
                          @if(isset($inventory['LOB-4']) && isset($inventory['LOB-4']['new']))
                              @foreach($inventory['LOB-4']['new'] as $files)
                                @php($LOB4+=$files)
                              @endforeach
                              {{ $LOB4}}
                          @else 
                            -
                          @endif
                        </td>

                        <td align="center">
                          @php($LOB5 = 0)
                          @if(isset($inventory['LOB-5']) && isset($inventory['LOB-5']['new']))
                              @foreach($inventory['LOB-5']['new'] as $files)
                                @php($LOB5+=$files)
                              @endforeach
                              {{ $LOB5}}
                          @else 
                            -
                          @endif --}}
                        </td>

                        <td align="center"><strong>
                            {{ $LOB  }}
                            @php($batchLOBTotalSum += $LOB)
                        </strong></td>

                        {{-- issued Files --}}
                        <td align="center">
                          {{ $IssuedOld = isset($agencyOldIssuedFiles["Residential"][$agency]['old'])  ? $agencyOldIssuedFiles["Residential"][$agency]['old'] : 0}}
                            @php($IssuedOldSum += $IssuedOld )
                        </td>
                        <td align="center">
                          {{ $IssuedNew = isset($agencyNewIssuedFiles["Residential"][$agency]['new'] ) ? $agencyNewIssuedFiles["Residential"][$agency]['new'] : 0}}
                          @php($IssuedNewSum += $IssuedNew )
                        </td>
                        <td align="center"><strong>
                          @if(isset($agencyOldIssuedFiles["Residential"][$agency]['old']) && isset($agencyNewIssuedFiles["Residential"][$agency]['new']))
                            {{ $agencyOldIssuedFiles["Residential"][$agency]['old'] + $agencyNewIssuedFiles["Residential"][$agency]['new']}}
                            @php($IssuedTotalSum += $agencyOldIssuedFiles["Residential"][$agency]['old'] + $agencyNewIssuedFiles["Residential"][$agency]['new'] )
                          @elseif(isset($agencyOldIssuedFiles["Residential"][$agency]['old']))
                            {{ $agencyOldIssuedFiles["Residential"][$agency]['old'] }}
                            @php($IssuedTotalSum += $agencyOldIssuedFiles["Residential"][$agency]['old'])
                          @elseif(isset($agencyNewIssuedFiles["Residential"][$agency]['new']))
                            {{ $agencyNewIssuedFiles["Residential"][$agency]['new'] }}
                            @php($IssuedTotalSum += $agencyNewIssuedFiles["Residential"][$agency]['new'])
                          @else 
                            -
                          @endif
                        </strong></td>


                        {{-- Booked files --}}
                        <td align="center">
                          {{ $BookedOld = isset($agencyOldBookedFiles["Residential"][$agency]['old'])  ? $agencyOldBookedFiles["Residential"][$agency]['old'] : 0}}
                          @php($BookedOldSum += $BookedOld )
                        </td>
                        <td align="center">
                          {{ $BookedNew = isset($agencyNewBookedFiles["Residential"][$agency]['new'] ) ? $agencyNewBookedFiles["Residential"][$agency]['new'] : 0}}
                          @php($BookedNewSum += $BookedNew )
                        </td>
                        <td align="center"><strong>
                          @if(isset($agencyOldBookedFiles["Residential"][$agency]['old']) && isset($agencyNewBookedFiles["Residential"][$agency]['new']))
                            {{ $agencyOldBookedFiles["Residential"][$agency]['old'] + $agencyNewBookedFiles["Residential"][$agency]['new']}}
                            @php($BookedTotalSum += $agencyOldBookedFiles["Residential"][$agency]['old'] + $agencyNewBookedFiles["Residential"][$agency]['new'])
                          @elseif(isset($agencyOldBookedFiles["Residential"][$agency]['old']))
                            {{ $agencyOldBookedFiles["Residential"][$agency]['old'] }}
                            @php($BookedTotalSum += $agencyOldBookedFiles["Residential"][$agency]['old'])
                          @elseif(isset($agencyNewBookedFiles["Residential"][$agency]['new']))
                            {{ $agencyNewBookedFiles["Residential"][$agency]['new'] }}
                            @php($BookedTotalSum += $agencyNewBookedFiles["Residential"][$agency]['new'])
                          @else 
                            -
                          @endif
                        </strong></td>

                        {{-- Remaining Balance --}}
                         <td align="center">
                          @php($oldRemaining = 0)
                          @php($oldIssued = 0)
                          @php($oldIssued = 0)
                          @if(isset($agencyOldIssuedFiles["Residential"][$agency]['old']) && isset($agencyOldBookedFiles["Residential"][$agency]['old']))
                            @php($oldRemaining = $agencyOldIssuedFiles["Residential"][$agency]['old'] - $agencyOldBookedFiles["Residential"][$agency]['old'])
                            {{ $oldRemaining }}
                            @php($RemainingOldSum += $oldRemaining)
                          @elseif(isset($agencyOldIssuedFiles["Residential"][$agency]['old']))
                            @php($oldIssued = $agencyOldIssuedFiles["Residential"][$agency]['old'])
                            {{ $oldIssued }}
                            @php($RemainingOldSum += $oldIssued)
                          @elseif(isset($agencyOldBookedFiles["Residential"][$agency]['old']))
                            @php($oldBooked = $agencyOldBookedFiles["Residential"][$agency]['old'])
                            {{ $oldBooked }}
                            @php($RemainingOldSum += $oldBooked)
                          @else 
                            -
                          @endif
                        </td>

                        @php($newRemaining = 0)
                        @php($newIssued = 0)
                        @php($newBooked = 0)
                        <td align="center">
                          @if(isset($agencyNewIssuedFiles["Residential"][$agency]['new']) && isset($agencyNewBookedFiles["Residential"][$agency]['new']))
                            @php($newRemaining = $agencyNewIssuedFiles["Residential"][$agency]['new'] - $agencyNewBookedFiles["Residential"][$agency]['new'])
                            {{ $newRemaining }}
                            @php($RemainingNewSum += $newRemaining)
                          @elseif(isset($agencyNewIssuedFiles["Residential"][$agency]['new']))
                            @php($newIssued = $agencyNewIssuedFiles["Residential"][$agency]['new'])
                            {{ $newIssued }}
                            @php($RemainingNewSum += $newIssued)
                          @elseif(isset($agencyNewBookedFiles["Residential"][$agency]['new']))
                            @php($newBooked = $agencyNewBookedFiles["Residential"][$agency]['new'])
                            {{ $newBooked }}
                            @php($RemainingNewSum += $newBooked)
                          @else 
                            -
                          @endif
                        </td>

                        <td align="center"><strong>
                          @if($oldRemaining && $newRemaining)
                            {{ $oldRemaining + $newRemaining }}
                            @php($RemainingTotalSum += $oldRemaining + $newRemaining )
                          @elseif($oldRemaining)
                            {{ $oldRemaining }}
                            @php($RemainingTotalSum += $oldRemaining)
                          @elseif($newRemaining)
                            {{ $newRemaining }}
                            @php($RemainingTotalSum += $newRemaining)
                          @else 
                            -
                          @endif
                        </strong></td>

 
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                       <tr>
                        <th></th>
                        <th>Total</th>

                        <th style="text-align:center">{{ $batch1OldSum }}</th>
                        <th style="text-align:center">{{ $batch1NewSum }}</th>
                        <th style="text-align:center">{{ $batch1TotalSum }}</th>

                        <th style="text-align:center">{{ $batch2OldSum }}</th>
                        <th style="text-align:center">{{ $batch2NewSum }}</th>
                        <th style="text-align:center">{{ $batch2TotalSum }}</th>

                        <th style="text-align:center">{{ $batch3OldSum }}</th>
                        <th style="text-align:center">{{ $batch3NewSum }}</th>
                        <th style="text-align:center">{{ $batch3TotalSum }}</th>

                        <!--<th style="text-align:center">{{ $batch4OldSum }}</th>-->
                        <!--<th style="text-align:center">{{ $batch4NewSum }}</th>-->
                        <!--<th style="text-align:center">{{ $batch4TotalSum }}</th>-->

                        <!--<th style="text-align:center">{{ $batch5OldSum }}</th>-->
                        <!--<th style="text-align:center">{{ $batch5NewSum }}</th>-->
                        <!--<th style="text-align:center">{{ $batch5TotalSum }}</th>-->

                        <th style="text-align:center">{{ $batchEBOldSum }}</th>
                        <th style="text-align:center">{{ $batchEBNewSum }}</th>
                        <th style="text-align:center">{{ $batchEBTotalSum }}</th>
                        <th style="text-align:center">{{ $batchLOBNewSum }}</th>
                        <th style="text-align:center">{{ $batchLOBTotalSum }}</th>

                        <th style="text-align:center">{{ $IssuedOldSum }}</th>
                        <th style="text-align:center">{{ $IssuedNewSum }}</th>
                        <th style="text-align:center">{{ $IssuedOldSum + $IssuedNewSum }}</th>

                        <th style="text-align:center">{{ $BookedOldSum }}</th>
                        <th style="text-align:center">{{ $BookedNewSum }}</th>
                        <th style="text-align:center">{{ $BookedOldSum + $BookedNewSum  }}</th>

                        <th style="text-align:center">{{ $RemainingOldSum }}</th>
                        <th style="text-align:center">{{ $RemainingNewSum }}</th>
                        <th style="text-align:center">{{ $RemainingOldSum + $RemainingNewSum }}</th>

                      </tr>
                      
                      <tr>
                        <td colspan="21">
                          <b>Total Open Files</b>
                        </td>
                        <td>{{ $residentialOpenBookings }}</td>
                        <td colspan="3"></td>
                      </tr>

                      <tr>
                        <td colspan="21">
                          <b>Total Balance</d>
                        </td>
                        <td>{{ ($BookedOldSum + $BookedNewSum ) - $residentialOpenBookings }}</td>
                        <td colspan="3"></td>
                      </tr>
                      
                    </tfoot> 
                  </table>
                </div>