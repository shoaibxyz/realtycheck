@extends('admin.layouts.template')
@section('title','Bookings Report ('. Request::input('enddate') . '-' . Request::input('startdate'). ')')

@section('reports-active','active')
@section('booking-report-active','active')
@section('booking-reports-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <style>
      @media print {

        #inventory_print{display:none;}
        tr{
            background-color: #1a4567 !important;
            -webkit-print-color-adjust: exact; 
        }
        
        }

    </style>
@endsection



@section('content')
@include('admin/common/breadcrumb',['page'=>'Bookings Report'])
  @include('errors')

        <div class="x_panel">
          <div class="x_title">
            <h2>Bookings Report</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.bookingsReport', 'class' => 'form-horizontal','method' => 'GET']) !!}
          <div class="row">       
          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker', 'autocomplete' => 'off') )!!}
              </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker', 'autocomplete' => 'off' ))!!}
              </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
                @php($agencyArr[''] = '')
                @foreach($agencies as $agency)
                  @php($agencyArr[$agency->id] = $agency->name)
                @endforeach    
                {!! Form::label('agency', 'Agency') !!}
                {!! Form::select('agency_id', $agencyArr, Request::input('agency_id'),  array('class'=>'form-control') )!!}
              </div>
          </div>

          <div class="col-md-2" style="margin-top: 25px; ">
            <div class="form-group">
                {{-- {!! Form::reset('Reset', ['class' => 'btn btn-primary' , 'id' => 'ok2']) !!} --}}
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>
          </div>
            <br><br>
          @if(isset($bookingsArr))

          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              @if(isset($bookingsArr['Residential']))
              <li role="presentation" class="active">
                <a href="#Residential" aria-controls="Residential" role="tab" data-toggle="tab">Residential</a>
              </li>
              @endif
              @if(isset($bookingsArr['Commercial']))
              <li role="presentation">
                <a href="#Commercial" aria-controls="Commercial" role="tab" data-toggle="tab">Commercial</a>
              </li>
              @endif
              @if(isset($bookingsArr['Housing']))
              <li role="presentation">
                <a href="#Housing" aria-controls="Housing" role="tab" data-toggle="tab">Housing</a>
              </li>
              @endif
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              @php($k = 1)
              @foreach($bookingsArr as $plotType => $bookingArr)
              <div role="tabpanel" class="tab-pane {{ $k == 1 ? "active" : "" }}" id="{{ $plotType }}">
                <h3>{{ $plotType . " - Booking Report" }}</h3>
                @php($k++)
               <div id="bookingtable_{{ $plotType }}">
               <button class="btn btn-success btn-print" id="printTotal_{{ $plotType }}">Print</button>
                <div class="table-responsive">
                  <table class="table table-hover table-bordered jambo_table bulk_action" id="ReportTbl" style="width: 100%; overflow-x: scroll;">
                      <thead>
                          <tr>
                              <th rowspan="2" style="vertical-align: middle;">Agency</th>
                              
                              @foreach ($plotsArr[$plotType] as $key => $plot)
                                <th colspan="3">{{ $key }}</th>    
                              @endforeach
                            <th rowspan="2" style="vertical-align: middle;">Total Bookings</th>
                          </tr>

                          <tr>
                            @for($i = 1; $i <= count($plotsArr[$plotType]); $i++)
                              <th>O</th>
                              <th>N</th>
                              <th>T</th>
                            @endfor
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $sum = 0;
                          $sumArr = [];
                          $sum1=0;
                            foreach ($bookingArr as $key => $booking) {?>
                                @php($rowCount = 0)
                                @php($grandTotal = 0)
                                <tr>
                                    <td>{{ $key }}</td>
                                    @php($sumPlot = 0)
                                      @foreach($plotsArr[$plotType] as $plotsKey => $plots)

                                        {{-- @if(isset($booking[$plotsKey])) --}}

                                          {{-- @foreach($booking as $rateType => $book) --}}
                                            @php($sum = 0)
                                            @php($oldSum = 0)
                                            @php($newSum = 0)

                                            @if(isset($booking[$plotsKey]['old']))
                                              <td>{{ $oldSum += count($booking[$plotsKey]['old']) }}</td>
                                            @else
                                              <td>0</td>
                                            @endif

                                            @if(isset($booking[$plotsKey]['new']))
                                              <td>{{ $newSum += count($booking[$plotsKey]['new']) }}</td>
                                            @else
                                              <td>0</td>
                                            @endif

                                            <td><strong>
                                              {{ $oldSum + $newSum }}
                                              @php($grandTotal += ( $oldSum + $newSum) )
                                            </strong></td>

                                            @if(isset($booking[$plotsKey]['old']))
                                              @php($sumArr[$plotsKey]['old'][] = count($booking[$plotsKey]['old']))
                                            @endif
                                            
                                            @if(isset($booking[$plotsKey]['new']))
                                              @php($sumArr[$plotsKey]['new'][] = count($booking[$plotsKey]['new']))
                                            @endif
                                          
                                          @endforeach

                                        {{-- @else
                                          <td>0</td>
                                        @endif   --}}      
                                        @php($rowCount += isset($booking[$plotsKey]) ? $grandTotal : 0)
                                    {{-- @endforeach --}}
                                    <td><strong>{{ $rowCount }}</strong></td>
                                </tr>
                        <?php }   ?>

                        <tfoot>
                          <tr>
                            <td>Total</td>
                              @php($grandTotal = 0)
                             @foreach($sumArr as $value)

                             @php($total = 0)
                              @php($oldCount=0)
                              @php($newCount=0)
                              
                              @if(isset($value['old']) )
                                @foreach($value['old'] as $val)
                                  @php($oldCount+=$val)
                                @endforeach
                              @endif
                              <td><strong>{{ $oldCount }}</strong></td>

                              @if(isset($value['new']))
                                @foreach($value['new'] as $val)
                                  @php($newCount+=$val)
                                @endforeach
                              @endif
                              <td><strong>{{ $newCount }}</strong></td>

                              @php($total += ($oldCount + $newCount) )
                              <td><strong>{{ $total }}</strong></td>
                              @php($grandTotal += $total )
                             @endforeach

                             <td><strong>{{ $grandTotal }}</strong></td>
                          </tr>
                        </tfoot>
                      </tbody>
                  </table>
                </div>
                </div>
                </div>
                

                @endforeach
                </div>
            </div>
          </div>

          @endif


      </div>
        
@endsection
@section('javascript')

  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

   <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">

$(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy'
	    });
    
    function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
                  var windowUrl = 'about:blank';
                  var uniqueName = new Date();
                  var windowName = '_blank';

                  
                    var WindowObject = window.open('Booking Report', 'PrintWindow' , 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
                    WindowObject.document.writeln('<!DOCTYPE html>');
                    WindowObject.document.writeln('<html><head><title></title>');
                    WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/custom.min.css" rel="stylesheet">');
                    WindowObject.document.writeln('<link href="https://realtycheck2.lahorecapitalcity.com/public/css/leadger.css" rel="stylesheet">');
                    WindowObject.document.writeln('<style>.btn-print{display:none;}</style>');
    
                     WindowObject.document.writeln('<link href="http://realtycheck2.lahorecapitalcity.com/public/css/yellow-grey.css" rel="stylesheet">');
                    WindowObject.document.writeln('</head><body style="background: #fff;" onload="window.print()">')
        
                    WindowObject.document.writeln($('#'+strid).html());
        
                    WindowObject.document.writeln('</body></html>');
        
                    WindowObject.document.close();
        
                    WindowObject.focus();
                    //WindowObject.close();
                    
                  return true;
    }

    $('#printTotal_Residential').on('click',function(){
      CallPrint('bookingtable_Residential');
    })

    $('#printTotal_Commercial').on('click',function(){
      CallPrint('bookingtable_Commercial');
    })

    $('#printTotal_Housing').on('click',function(){
      CallPrint('bookingtable_Housing');
    })
    
    
});

    </script>
@endsection
