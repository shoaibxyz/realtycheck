@extends('admin.layouts.template')
@section('title','Salary Report')

@section('reports-active','active')
@section('user-report-active','active')
@section('salary-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    
    <style>
        .ui-datepicker-calendar {display: none;}

    </style>
  
@endsection

@section('content')
<h2>Salary Report</h2>   
@include('admin/common/breadcrumb',['page'=>'Salary Report'])
        <div class="x_panel">
        <div class="x_title">
                
            <div class="clearfix"></div>
                @include('errors')

    </div>
        
        {!! Form::open(['method' => 'GET', 'route' => 'getsalaryreport', 'class' => 'form-horizontal']) !!}

        <div class="row">

            
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('forMonth', 'For Month') !!}
                    <input name="forMonth" id="forMonth" class="date-picker form-control" value="" required/>
                </div>
            </div>
        
            <div class="col-sm-3">
                <div class="form-group" style="margin-top: 25px;">
                    {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
                </div>
            </div>
            
          
        {!! Form::close() !!}
        
    </div>

   
</div>

@if(isset($payslip))
<div id="app">
      
  <div class="table-responsive" style="overflow-x: auto">
    <table class="table table-bordered table-hover jambo_table" id="datatable">
      <thead>
        <tr>
          <th>Sr #</th>
          <th>Name</th>
          <th>Designation</th>
          <th>Basic Salary</th>
          <th>Working Days</th>
          <th>Salary Days</th>
          <th>Total Allowances</th>
          <th>Total Deductions</th>
          <th>Total Fine</th>
          <th>Total Tax</th>
          <th>Total Advance Paid</th>
          <th>Total Salary</th>
          <th>Remarks</th>
        </tr>
      </thead>
      <tbody>
        @php($i = 1)
          @foreach($payslip as $slip)
          <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $slip->username }}</td>
            <td>{{ $slip->designation_name }}</td>
            <td>{{ $slip->basic_salary }}</td>
            <td>{{ \App\LeavesRequests::getleaves($slip->employee_id,$slip->month,$slip->year) }}</td>
            <td>{{ \App\LeavesRequests::getleaves($slip->employee_id,$slip->month,$slip->year) }}</td>
            <td>{{ $slip->total_allowances }}</td>
            <td>{{ $slip->total_deductions }}</td>
            <td>{{ $slip->fine }}</td>
            <td>{{ $slip->tax_amount }}</td>
            <td>{{ $slip->advance_salary }}</td>
            <td>{{ $slip->net_salary }}</td>
            <td></td>
          </tr>
          @endforeach
      </tbody>
  </table>
  
</div>
</div>
@endif

@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    
 
    $(function() {
             
    $('.date-picker').datepicker(
    {
        dateFormat: "mm-yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {


            function isDonePressed(){
                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > 1);
            }

            if (isDonePressed()){
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                
                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
            }
        },
        beforeShow : function(input, inst) {

            inst.dpDiv.addClass('month_year_datepicker')

            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
                $(".ui-datepicker-calendar").hide();
            }
        }
        })
    });
    
    
    
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
          }];
      var table = $('#datatable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      fixedHeader: true,
      buttons: buttons
    });
    

    </script>
@endsection