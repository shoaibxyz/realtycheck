@extends('admin.layouts.template')
@section('title','Due Installments Report')

@section('reports-active','active')
@section('installment-report-active','active')
@section('accumulated-due-report-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Accumulated Due Report'])
  @include('errors')

        <div class="x_panel">
          <div class="x_title">
            <h2>Accumulated Due Report</h2>
            
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.CheckdueAccumulatedAmounts', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
         {{--    <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('startdate', 'From') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>
 --}}

          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('enddate', 'To') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          <div class="col-md-3">
              <div class="form-group">
                    {!! Form::label('agency', 'Agency') !!}
                    <?php
                    $agencyArr[''] = "";
                    foreach ($agencies as $agency) {
                      $agencyArr[$agency->id] = $agency->name;
                    }
                    ?>
                    {!! Form::select('agency_id', $agencyArr, Request::input('agency_id'), ['class' => 'form-control', 'id' => 'agency'] ) !!}
              </div>
          </div>

          {{-- <div class="col-md-5">
            <div class="form-group">
                    {!! Form::label('startdate', 'To') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
              </div>
          </div> --}}

          <div class="col-md-3" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>

          @if(isset($dueAmounts))

          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#Residential" aria-controls="Residential" role="tab" data-toggle="tab">Residential</a>
              </li>
              <li role="presentation">
                <a href="#Commercial" aria-controls="Commercial" role="tab" data-toggle="tab">Commercial</a>
              </li>
              <li role="presentation">
                <a href="#Housing" aria-controls="Commercial" role="tab" data-toggle="tab">Housing</a>
              </li>
            </ul>
          
            <!-- Tab panes -->
            <div class="tab-content">
              @php($i = 0)
              @foreach($dueAmounts as $key => $plotType)
              <div role="tabpanel" class="tab-pane {{ $i == 0 ? "active" : "" }}" id="{{$key}}">
                <h3>{{ $key}} - Due Amount Accumulated</h3>
                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action table-bordered" id="{{$key}}_dueReportTable"> 
                      <thead>
                          <tr>
                              <th>Sr</th>
                              <th>Reg</th>
                              <th>Ref </th>
                              <th>Plot</th>
                              <th>Member</th>
                              <th>Phone</th>
                              <th>Total Amt</th>
                              <th>Remain Amt</th>
                              <th>O.Due Inst</th>
                              <th>O.Due Amt</th>
                              <th>Curr Mon Inst</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php($i = 1)
                        @php($regArr = [])
                        @php($inst_sum = 0)
                        @php($total_sum = 0)
                        @php($os_sum = 0)
                        @php($lpc_sum = 0)
                        @foreach($plotType as $key => $inst)
                          <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $key }}</td>
                            <td>{{ $plotType[$key]['member']->reference_no}}</td>
                            <?php
                            
                            $plotsize =  App\plotSize::join('payment_schedule','payment_schedule.plot_size_id','plot_size.id')->
                            where('payment_schedule.id',$plotType[$key]['bookings']->payment_schedule_id)->select(DB::Raw('plot_size.plot_size'))->first();
                            $plot_size = '';
                             if($plotsize){
                                $plot_size = $plotsize->plot_size;
                             }
                            ?>
                            <td>{{ $plot_size }}</td>
                            <td>{{ $plotType[$key]['person']->name}}</td>
                            <td>{{ $plotType[$key]['person']->phone_mobile}}</td>
                            <td>{{ number_format($plotType[$key]['bookings']->paymentPlan->total_price)  }}</td>
                            <td>{{ number_format($plotType[$key]['bookings']->paymentPlan->total_price - $plotType[$key]['recSum']) }}</td>
                            <td>{{ count($plotType[$key]['inst']) }}</td>
                            <td>{{ number_format($plotType[$key]['sum']) }}</td>
                            <td>
                                <?php
                                $insts = \DB::select('select installment_amount from member_installments where month(installment_date) = '. date('m') .' and year(installment_date) = '. date('Y') .' and registration_no="'. $key .'"');
                                foreach($insts as $inst)
                                    echo(number_format($inst->installment_amount));
                                ?>
                                
                            </td>
                          </tr>
                          <?php $inst_sum += $plotType[$key]['sum']; 
                            $total_sum += $plotType[$key]['bookings']->paymentPlan->total_price; 
                            $os_sum += $plotType[$key]['bookings']->paymentPlan->total_price - $plotType[$key]['recSum']; 
                          ?>
                        @php($i++)
                        @endforeach
                      </tbody>

                      <tfoot>
                        <tr>
                          <th colspan="6">Total</th>
                          <td id="{{$key}}_inst_sum">{{ number_format($total_sum) }}</td>
                          <td id="{{$key}}_inst_sum">{{ number_format($os_sum) }}</td>
                          <td></td>
                          <td id="{{$key}}_total">{{ number_format($inst_sum + $lpc_sum) }}</td>
                        </tr>
                      </tfoot>
                  </table>
                </div>
              </div>
              @endforeach
            </div>
          </div>

          @endif


      </div>
        
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
      
      var buttons = [];
      if("{{ Auth::user()->roles[0]->name == 'Super-Admin' }}")
      {
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
            // exportOptions: {
            //       columns: [1,4]
            //   }
          }];
      }
      
      /*if("{{ Auth::user()->roles[0]->name == 'ccd-incharge' }}")
      {*/
          var buttons = [
          {
            extend: 'excel',
            exportOptions: {
                  columns: [1,4,6,7,8,9,10]
              }
          }];
      //}

      
     jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });

   var table = $('#Residential_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

     $("#Residential_dueReportTable").on('search.dt', function() {
      $('#Residential_inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#Residential_total').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    });

    var table = $('#Commercial_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

    $("#Commercial_dueReportTable").on('search.dt', function() {
      $('#Commercial_inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#Commercial_total').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    });

    var table = $('#Housing_dueReportTable').DataTable({ 
      "bSort" : true,
      dom: 'Bfrtip',
      buttons: buttons
    });

    $("#Housing_dueReportTable").on('search.dt', function() {
      $('#Housing_inst_sum').html(table.column( 6, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      $('#Housing_total').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    });

    $(document).ready(function() {
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection