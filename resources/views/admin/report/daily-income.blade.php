@extends('admin.layouts.template')
@section('title','Reports')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

	@include('errors')

        <div class="x_panel">
        <div class="x_title">
            <h2>Daily Receipt Detail</h2>
            
            <div class="clearfix"></div>
          </div>    
		{!! Form::open(['route' => 'report.CheckDailyIncome', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
    	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
		            {!! Form::label('payment_date', 'Payment Date') !!}
		            {!! Form::text('payment_date', \Carbon\Carbon::now()->format('d/m/Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
			    </div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
		            {!! Form::label('monthly', 'Payment Date') !!}
		            {!! Form::text('monthly', \Carbon\Carbon::now()->format('d/m/Y'),  array('required' => 'required'  , 'class'=>'form-control datepicker') )!!}
			    </div>
			</div>

			<div class="col-md-4">		
	            <div class="form-group">
	                {!! Form::submit('Search', ['class' => 'btn btn-default' , 'id' => 'ok', 'style' => 'margin-top: 25px;']) !!}
	            </div>
	        </div>
	    </div>
	     {!! Form::close() !!}

	    @if(isset($BookingDetail))
          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action" id="ReportTbl">
                <thead>
                    <tr>
                        <th>Reg #</th>
                        <th>Ref #</th>
                        <th>Plan Code</th>
                        <th>Plot Size</th>
                        <th>Total Price</th>
                        <th>Amount Paid</th>
                        <th>Dated</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                      foreach ($BookingDetail['booking'] as $bookings => $booking) {?>
                          <tr>
                              {{-- <th>{{ $booking->id }}</th> --}}
                              <td>{{ $booking->registration_no }}</td>
                              <td>{{ $BookingDetail['member'][$bookings]->reference_no or ""}}</td>
                              {{-- <td>{{ $BookingDetail['person'][$bookings]->gurdian_name}}</td> --}}
                              <td>{{ $BookingDetail['paymentPlan'][$bookings]->payment_code}}</td>
                              <td>{{ $BookingDetail['PlotSize'][$bookings]->plot_size}}</td>
                              <td>{{ $BookingDetail['paymentPlan'][$bookings]->total_price}}</td>
                              <td>{{ number_format($BookingDetail['inst'][$bookings]) }}</td>
                              <td>{{ $booking->created_at->format('d-m-Y')}}</td>
                          </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>
          @endif
        
@endsection
@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'dd/mm/yy'
	    });
    });
    </script>
@endsection