@extends('admin.layouts.template')
@section('title','Summary Report')

@section('reports-active','active')
@section('report-active','active')
@section('summary-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    
@endsection
@section('content')
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Summary Report</div>
        </div>
        
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Summary Report</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel">
            <div class="x_title">
                <h2>Summary Report</h2>
                <div class="clearfix"></div>
                    @include('errors')
    
            </div>
             
            <div style="overflow-x: auto">
                <table class="table table-bordered" style="overflow-x: scroll; width: 100%" id="summaryreport">
    <button id="btnExport" onclick="javascript:xport.toCSV('summaryreport');"> Excel</button> 
    <thead>
      <tr>
        <?php
        $j = 1;
        ?>
        
        
        <th>Month</th>
        <th>D/P</th>
        @foreach($plotsize as $Plotsize)
         <th>{{ $Plotsize->plot_size }}</th>
        <?php $j++; ?>
        @endforeach
        
        @for ($x = 0; $x <= $diff; $x++)
         <th>{{ $x }}</th>
        @endfor
        
      </tr>
    </thead>
    <tbody>
        
            <?php
                $date = $first;
                $end = $last;
                
                $start    = new DateTime($first);
                $start->modify('first day of this month');
                $end      = new DateTime($last);
                $end->modify('first day of next month');
                $interval = DateInterval::createFromDateString('1 month');
                $period   = new DatePeriod($start, $interval, $end);
                $a = 0;
                $i = 1;
                foreach ($period as $dt) {
                    
                    echo "<tr><td>" .$dt->format("M-y"). "</td>";
                    
                    $mon = $dt->format("m");
                    $year = $dt->format("Y");
                    
                    $booking = \App\memberInstallments::
                                join('members', 'members.registration_no', 'member_installments.registration_no')
                                ->join('bookings', 'bookings.registration_no', 'members.registration_no')
                                
                                ->where('member_installments.payment_desc', 'Down Payment')
                                ->where('bookings.status',1)
                                ->whereNull('bookings.deleted_at')
                                ->where('bookings.is_cancelled',0)
                                ->where('member_installments.is_active_user', 1)
                                ->where('member_installments.is_cancelled', 0)
                                ->where('member_installments.is_other_payment', 1)
                                ->where('members.is_current_owner',1)
                                ->where('member_installments.installment_no', 1)
                                ->whereNull('member_installments.deleted_at')
                                ->whereNull('members.deleted_at')
                                ->where(DB::Raw('MONTH(member_installments.installment_date)') , $mon)
                                ->where(DB::Raw('YEAR(member_installments.installment_date)') , $year)
                                ->count();
                    
                    echo "<td>" .$booking. "</td>";
                    
                    foreach($plotsize as $Plotsize){
                
            $monthwise = \App\plotSize::join('payment_schedule','payment_schedule.plot_size_id','plot_size.id')
                            ->join('bookings', 'bookings.payment_schedule_id', 'payment_schedule.id')
                            ->join('members', 'members.registration_no', 'bookings.registration_no')
                            ->where('plot_size.id', $Plotsize->id)
                            ->where(DB::Raw('MONTH(bookings.created_at)') , $mon)
                            ->where(DB::Raw('YEAR(bookings.created_at)') , $year)
                            ->where('bookings.status',1)
                            ->where('bookings.is_cancelled',0)
                            ->where('members.is_current_owner',1)
                            ->whereNull('bookings.deleted_at')
                            ->where('bookings.is_cancelled',0)
                            ->groupBy('plot_size.id')
                            ->orderBy('plot_size.plot_size', 'ASC')->count();
            
            echo "<td>" .$monthwise. "</td>";
            // $a = $a+$monthwise;
            
            }
            
            for ($x = 0; $x <= $diff; $x++){
             
            
                if($x == 0){
            
                $count = \App\memberInstallments::where('is_paid', 1)
                    ->join('bookings', 'bookings.registration_no', 'member_installments.registration_no')
                    ->where('bookings.status',1)
                    ->where('bookings.is_cancelled',0)
                    ->where('member_installments.payment_desc', 'Down Payment')
                    ->where('member_installments.is_active_user', 1)
                    ->where('member_installments.is_cancelled', 0)
                    ->where('member_installments.is_other_payment', 1)
                    ->where('member_installments.installment_no', 1)
                    ->whereNull('member_installments.deleted_at')
                    ->where(DB::Raw('MONTH(member_installments.installment_date)') , $mon)
                    ->where(DB::Raw('YEAR(member_installments.installment_date)') , $year)
                    ->count();
            }else{
                $count = \App\memberInstallments::where('is_paid', 1)
                ->join('bookings', 'bookings.registration_no', 'member_installments.registration_no')
                ->where('bookings.status',1)
                ->where('bookings.is_cancelled',0)
                ->where('member_installments.is_active_user', 1)
                ->where('member_installments.is_cancelled', 0)
                ->where('member_installments.is_other_payment', 0)
                ->where('member_installments.installment_no', $x)
                ->where(DB::Raw('MONTH(member_installments.installment_date)') , $mon)
                ->where(DB::Raw('YEAR(member_installments.installment_date)') , $year)
                ->count();    
            }
            
            
            // dd($count);
            echo "<td>".$count."</td>";
            
            }
            
            
            
                    echo "</tr>";
                     
                //  echo "<tr><td>" .$a. "</td></tr>";   
                
                $i++;
                }
                
                    echo "<tfoot> <tr> <td id='total'>Total</td>";
                for ($x = 1; $x <= $j; $x++){
                    echo "<td></td>";
                }
                
                for ($x = 1; $x <= $i; $x++){
                    echo "<td></td>";
                }
                
            echo "<tr>";
        
            ?>        
        </tfoot>
    </tbody>
  </table>
            </div>
        </div>
        
    </div>
@endsection
@section('javascript')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">
var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

    $(document).ready(function() {
    	$('.datepicker').datepicker({
	        dateFormat: 'yy/mm/dd'
	    });
    });
    

        $(document).ready(function() {
            $('table thead th').each(function(i) {
                calculateColumn(i);
            });
        });

        function calculateColumn(index) {
            var total = 0;
            
            $('table tr').each(function() {
                if(index == 0){
                    total = "Total";
                }else{
                    var value = parseInt($('td', this).eq(index).text());
                    
                    if (!isNaN(value)) {
                        total += value;
                    }
                }
            });
            
            $('table tfoot td').eq(index).text(total);
        }

    // $('#total').html('Total');
</script>
    <!--<style>-->
    <!--    #total::after {-->
    <!--      content: "Total";-->
    <!--    }-->
    <!--    #total::first-letter {-->
    <!--      font-size: 0%;-->
         
    <!--    }-->
    <!--</style>-->
@endsection