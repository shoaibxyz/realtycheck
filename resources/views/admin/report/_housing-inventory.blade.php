        
                  <h3>Housing - Inventory Report</h3>
                  <button id="housing_inventory_print" class="btn btn-success inventory_print" style="{{$rights->show_print}}">Print</button>
                  <div class="table-responsive">
                  <table class="table table-hover table-bordered jambo_table bulk_action" id="housingPrintTable" style="width: 100%; overflow-x: scroll;" >
                    <thead>
                      <tr>
                        <th rowspan="3">Sr. No</th>
                        <th rowspan="3" style="width: 70px;">Estate</th>
                        <th colspan="4">Issued files</th>
                        <th colspan="11">Booked Files</th>
                        <th colspan="4">Bal. Files</th>
                      </tr>
                      <tr>
                        <th>5M</th>
                        <th>8M</th>
                        <th>10M</th>
                        <th>Total</th>

                        <th colspan="3">5M</th>
                        <th colspan="3">8M</th>
                        <th colspan="3">10M</th>
                        <th colspan="2">Total</th>

                        <th>5M</th>
                        <th>8M</th>
                        <th>10M</th>
                        <th>Total</th>

                      </tr>

                      <tr>
                          
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Open</th>
                        <th>Booked</th>
                        <th>Total</th>
                        
                        <th>Open</th>
                        <th>Booked</th>
                        <th>Total</th>
                        
                        <th>Open</th>
                        <th>Booked</th>
                        <th>Total</th>
                        
                        <th>Open</th>
                        <th>Booked</th>
                        
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>

                      </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                       @php($i = 1)
                      
                       @php($IssuedOldSum = 0)
                       @php($IssuedNewSum = 0)
                       @php($Issued10MSum = 0)
                       @php($IssuedTotalSum = 0)

                       @php($BookedOldSum = 0)
                       @php($BookedNewSum = 0)
                       @php($Booked10MSum = 0)
                       @php($BookedTotalSum = 0)
                       
                       @php($OpenSum5M = 0)
                       @php($OpenSum8M = 0)
                       @php($OpenSum10M = 0)
                       
                       @php($RemainingOldSum = 0)
                       @php($RemainingNewSum = 0)
                       @php($Remaining10MSum = 0)
                       @php($RemainingTotalSum = 0)

                       @foreach($filesArr['Housing'] as $agency => $inventory)

                       @php($Booked5MSumH = 0)
                       @php($Booked8MSumH = 0)
                          {{-- @php(dd($agencyOldIssuedFiles['Commercial'][$agency])) --}}
                         <td align="center">{{ $i++ }}</td>
                        <td style="width: 70px;">{{ $agency }}</td>
                        {{-- issued Files --}}
                        <td align="center">
                          {{ $IssuedOld = isset($agencyOldIssuedFiles["Housing"][$agency]['05-M'])  ? array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']) : 0}}
                            @php($IssuedOldSum += $IssuedOld )
                        </td>
                        <td align="center">
                          {{ $IssuedNew = isset($agencyOldIssuedFiles["Housing"][$agency]['08-M'] ) ? array_sum($agencyOldIssuedFiles["Housing"][$agency]['08-M']) : 0}}
                          @php($IssuedNewSum += $IssuedNew )
                        </td>
                        <td align="center">
                          {{ $Issued10M = isset($agencyOldIssuedFiles["Housing"][$agency]['10-M'] ) ? array_sum($agencyOldIssuedFiles["Housing"][$agency]['10-M']) : 0}}
                          @php($Issued10MSum += $Issued10M)
                        </td>
                        <td align="center"><strong>
                            {{ $IssuedOld + $IssuedNew + $Issued10M }}
                          <!--@if(isset($agencyOldIssuedFiles["Housing"][$agency]['05-M']) && isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']))
                            {{ array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']) + array_sum($agencyOldIssuedFiles["Housing"][$agency]['08-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']) + array_sum($agencyOldIssuedFiles["Housing"][$agency]['08-M']) )
                          @elseif(isset($agencyOldIssuedFiles["Housing"][$agency]['05-M']))
                            {{ array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']))
                          @elseif(isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']))
                            {{ array_sum($agencyOldIssuedFiles["Housing"][$agency]['08-M']) }}
                            @php($IssuedTotalSum += array_sum($agencyOldIssuedFiles["Housing"][$agency]['08-M']) )
                          @else 
                            -
                          @endif-->
                        </strong></td>

                        {{-- Booked files --}}
                        <td align="center">
                          <?php
                            $BookedOld = isset($agencyOldBookedFiles["Housing"][$agency]['05-M'])  ? array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M']) : 0;
                            $openBooked5MH = isset($housingOpenBookingsArr['Housing'][$agency]['05-M']) ? $housingOpenBookingsArr['Housing'][$agency]['05-M'] : 0;
                            //echo $openBooked5MH;
                            echo $IssuedOld - $BookedOld;
                            $OpenSum5M += ($IssuedOld - $BookedOld);
                          ?>
                        </td>

                        <td align="center">
                          {{ $BookedOld }}
                          @php($BookedOldSum += $BookedOld )
                        </td>

                        <td align="center">
                            <b>{{ $BookedOld + ($IssuedOld - $BookedOld) }}</b>
                        </td>

                        <td align="center">
                          <?php
                            $BookedNew = isset($agencyOldBookedFiles["Housing"][$agency]['08-M'] ) ? array_sum($agencyOldBookedFiles["Housing"][$agency]['08-M']) : 0;
                            $openBooked8MH = isset($housingOpenBookingsArr['Housing'][$agency]['08-M']) ? $housingOpenBookingsArr['Housing'][$agency]['08-M'] : 0;
                            //echo $openBooked8MH;
                            echo $IssuedNew - $BookedNew;
                            //$OpenSum8M += $openBooked8MH;
                            $OpenSum8M += ($IssuedNew - $BookedNew);
                          ?>

                        </td>

                        <td align="center">
                          {{ $BookedNew }}
                          @php($BookedNewSum += $BookedNew)
                        </td>

                        <td align="center">
                            <b>{{ $BookedNew + ($IssuedNew - $BookedNew) }}</b>
                        </td>
                        
                        <td align="center">
                          <?php
                            $Booked10M = isset($agencyOldBookedFiles["Housing"][$agency]['10-M'] ) ? array_sum($agencyOldBookedFiles["Housing"][$agency]['10-M']) : 0;
                            //$openBooked8MH = isset($housingOpenBookingsArr['Housing'][$agency]['08-M']) ? $housingOpenBookingsArr['Housing'][$agency]['08-M'] : 0;
                            //echo $openBooked8MH;
                            echo $Issued10M - $Booked10M;
                            //$OpenSum8M += $openBooked8MH;
                            $OpenSum10M += ($Issued10M - $Booked10M);
                          ?>

                        </td>

                        <td align="center">
                          {{ $Booked10M }}
                          @php($Booked10MSum += $Booked10M)
                        </td>

                        <td align="center">
                            <b>{{ $Booked10M + ($Issued10M - $Booked10M) }}</b>
                        </td>
                        
                        <td align="center">{{ ($IssuedOld + $IssuedNew + $Issued10M) - ($BookedOld + $BookedNew + $Booked10M) }}</td>
                        <td align="center"><strong>
                          {{ ($BookedOld + $BookedNew + $Booked10M) }}
                          <!--@if(isset($agencyOldBookedFiles["Housing"][$agency]['05-M']) && isset($agencyOldBookedFiles["Housing"][$agency]['08-M']))-->
                          <!--  {{ array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M']) + array_sum($agencyOldBookedFiles["Housing"][$agency]['08-M']) }}-->
                          <!--  @php($BookedTotalSum += array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M']) + array_sum($agencyOldBookedFiles["Housing"][$agency]['08-M']) )-->
                          <!--@elseif(isset($agencyOldBookedFiles["Housing"][$agency]['05-M']))-->
                          <!--  {{ array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M']) }}-->
                          <!--  @php($BookedTotalSum += array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M'] ))-->
                          <!--@elseif(isset($agencyOldBookedFiles["Housing"][$agency]['08-M']))-->
                          <!--  {{ array_sum($agencyOldBookedFiles["Housing"][$agency]['08-M']) }}-->
                          <!--  @php($BookedTotalSum += array_sum( $agencyOldBookedFiles["Housing"][$agency]['08-M']) )-->
                          <!--@else -->
                          <!--  @php($BookedTotalSum += 0)-->
                          <!--  {{ 0 }}-->
                          <!--@endif-->
                        </strong>
                      </td>

                        {{-- Remaining Balance --}}
                         <td align="center">
                          <!--@if(isset($agencyOldIssuedFiles["Housing"][$agency]['05-M']) && isset($agencyOldBookedFiles["Housing"][$agency]['05-M']))
                            {{ $oldRemaining = $IssuedOld - $BookedOld }}
                            @php($RemainingOldSum += ( $IssuedOld - $BookedOld) )
                          @elseif(isset($agencyOldIssuedFiles["Housing"][$agency]['05-M']))
                            {{-- @php($oldIssued = array_sum($agencyOldIssuedFiles["Housing"][$agency]['05-M']) ) --}}
                            {{ $oldRemaining = $IssuedOld }}
                            @php($RemainingOldSum += $IssuedOld)
                          @elseif(isset($agencyOldBookedFiles["Housing"][$agency]['05-M']))
                            {{-- @php($oldBooked = array_sum($agencyOldBookedFiles["Housing"][$agency]['05-M'] )) --}}
                            <b>{{ $oldRemaining = $BookedOld }}</b>
                            
                          @else 
                            -
                          @endif-->
                            {{ $IssuedOld - $BookedOld }}
                            @php($RemainingOldSum += ($IssuedOld - $BookedOld))
                        </td>

                        @php($newRemaining = 0)
                        @php($newIssued = 0)
                        @php($newBooked = 0)

                        <td align="center">
                          <!--@if(isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']) && isset($agencyOldBookedFiles["Housing"][$agency]['08-M']))
                            {{ $newRemaining = $IssuedNew - $BookedNew }}
                            @php($RemainingNewSum += ($IssuedNew - $BookedNew))
                          @elseif(isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']))
                            {{ $newRemaining = $IssuedNew }}
                            @php($RemainingNewSum += $IssuedNew)
                          @elseif(isset($agencyOldBookedFiles["Housing"][$agency]['08-M']))
                            <b>{{ $newRemaining = $BookedNew }}</b>
                            @php($RemainingNewSum += $BookedNew)
                          @else 
                            -
                          @endif-->
                            {{ $IssuedNew - $BookedNew }}
                            @php($RemainingNewSum += ($IssuedNew - $BookedNew))
                        </td>
                        
                        <td align="center">
                            {{ $Issued10M - $Booked10M }}
                            @php($Remaining10MSum += ($Issued10M - $Booked10M))
                        </td>

                        <td align="center"><strong>
                          <!--@if(isset($agencyOldIssuedFiles["Housing"][$agency]['05-M']) && isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']))
                            {{ $oldRemaining + $newRemaining }}
                            @php($RemainingTotalSum += $oldRemaining + $newRemaining )
                          @elseif(isset($agencyOldIssuedFiles["Housing"][$agency]['08-M']))
                            {{ $newRemaining  }}
                            @php($RemainingTotalSum += $newRemaining )
                          @elseif(isset($agencyOldBookedFiles["Housing"][$agency]['05-M'])) 
                            {{ $oldRemaining }}
                            @php($RemainingTotalSum += $oldRemaining)
                          @else
                            {{ $oldRemaining }}
                          @endif-->
                          {{ ($IssuedOld + $IssuedNew + $Issued10M) - ($BookedOld + $BookedNew + $Booked10M) }}
                          @php($RemainingTotalSum += (($IssuedOld + $IssuedNew + $Issued10M) - ($BookedOld + $BookedNew + $Booked10M)))
                        </strong></td>

                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                       <tr>
                        <th colspan="2">Total</th>

                        <th style="text-align:center">{{ $IssuedOldSum }}</th>
                        <th style="text-align:center">{{ $IssuedNewSum }}</th>
                        <th style="text-align:center">{{ $Issued10MSum }}</th>
                        <th style="text-align:center">{{ $IssuedOldSum + $IssuedNewSum + $Issued10MSum }}</th>

                        <th style="text-align:center">{{ $OpenSum5M }}</th>
                        <th style="text-align:center">{{ $BookedOldSum }}</th>
                        <th style="text-align:center">{{ $OpenSum5M + $BookedOldSum }}</th>

                        <th style="text-align:center">{{ $OpenSum8M }}</th>
                        <th style="text-align:center">{{ $BookedNewSum }}</th>
                        <th style="text-align:center">{{ $OpenSum8M + $BookedNewSum }}</th>
                        
                        <th style="text-align:center">{{ $OpenSum10M }}</th>
                        <th style="text-align:center">{{ $Booked10MSum }}</th>
                        <th style="text-align:center">{{ $OpenSum10M + $Booked10MSum }}</th>

                        <th style="text-align:center">{{ $OpenSum5M + $OpenSum8M + $OpenSum10M  }}</th>
                        <th style="text-align:center">{{ $BookedOldSum + $BookedNewSum + $Booked10MSum  }}</th>

                        <th style="text-align:center">{{ $OpenSum5M }}</th>
                        <th style="text-align:center">{{ $OpenSum8M }}</th>
                        <th style="text-align:center">{{ $OpenSum10M }}</th>
                        <th style="text-align:center">{{ $OpenSum5M + $OpenSum8M + $OpenSum10M }}</th>

                      </tr>

                {{--       <tr>
                        <td colspan="7">
                          <b>Total Open Files</b>
                        </td>
                        <td align="center"></td>
                      <td align="center">{{ $housingOpenBookings }}</td>
                        <td colspan="3"></td>
                      </tr> --}}

                    {{--   <tr>
                        <td colspan="7">
                          <b>Total Balance</d>
                        </td>
                        <td align="center">{{ ($BookedOldSum + $BookedNewSum ) }}</td>
                        <td colspan="3"></td>
                      </tr>
 --}}
                    </tfoot> 
                  </table>
                </div>