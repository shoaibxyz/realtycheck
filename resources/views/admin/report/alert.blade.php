@extends('admin.layouts.template')
@section('title','Alert Report')

@section('reports-active','active')
@section('marketing-report-active','active')
@section('alert-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<style type="text/css">
    @if(Auth::user()->Roles->first()->name == 'call-center')
      .dt-buttons{
        display: none;
      }
    @endif
    </style>


@endsection

@section('content')
@include('admin/common/breadcrumb',['page'=>'Alert Report'])
@include('errors')

<div class="x_panel">
  <div class="x_title">
    <h2>Alert Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['route' => 'report.alert-report', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('fromdate', 'From') !!}
      {!! Form::text('fromdate', Request::input('fromdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      {!! Form::label('todate', 'To') !!}
      {!! Form::text('todate', Request::input('todate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
    </div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>


<div id="app">
        @include('admin.report.brandin-header', ['reportTitle' => "Alert Report"])

  <div class="table-responsive">
    <table class="table table-bordered table-hover jambo_table" >
      <thead>
        <tr>
          <th>Sr #</th>
          <th>Reg #</th>
          <th>Message</th>
          <th>Created By</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        @php($i = 1)
        @if(isset($alerts))
          @foreach($alerts as $alert)
          <tr>
            <th>{{ $i++ }}</th>
            <th>{{ $alert->registration_no}}</th>
            <th>{{ $alert->message }}</th>
            <th>{{ App\User::getUserInfo($alert->created_by)->name }}</th>
            <th>{{ $alert->created_at->format('d-m-Y')}}</th>
          </tr>
          @endforeach
        @endif
      </tbody>
  </table>
  <button id="printreceipt" class="btn btn-success">Print</button>
</div>


</div>
</div>

@endsection
@section('javascript')

   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    var columns = [
            { data: 'id'},
            { data: 'registration_no', name: 'registration_no' },
            { data: 'member_name', name: 'member_name' },
            { data: 'phone', name: 'phone' },
            { data: 'payment_desc', name: 'payment_desc' },
            { data: 'plot_size', name: 'plot_size' },
            { data: 'installment_amount', name: 'installment_amount' },
            { data: 'amount_received', name: 'amount_received' },
            { data: 'payment_date', name: 'payment_date' },
        ];

        var to = "{{ Request::input('startdate') }}";
        var from = "{{ Request::input('enddate') }}";

       var table = $('#paidInstTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "info": true,
            "bSort" : false,
            buttons: [
                 'excel', 'print'
            ],
            "ajax": {
                "url": "{{ URL::to('admin/report/getPaidInst/')}}/"+from+"/"+to, 
            },
            "columns": columns
        });


    // jQuery.fn.DataTable.Api.register( 'sum()', function () {
    //   return this.flatten().reduce( function ( a, b ) {
    //         return (a*1) + (b*1); // cast values in-case they are strings
    //       });
    // });

   function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
      var windowUrl = 'about:blank';
      var uniqueName = new Date();
      var windowName = '_self';

      var printWindow = window.open(window.location.href , windowName);
      printWindow.document.body.innerHTML = printContent.innerHTML;
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
      return false;
    }

    $('#printreceipt').click(function(){
      CallPrint('app');
    });

    // var table = $('.jambo_table').DataTable({ 
    //   "bSort" : false,
    //   dom: 'Bfrtip',
    //   buttons: [
    //          'excel',
    //     ],
    // });
      
    // $("#paidInstTable").on('search.dt', function() {
    //   $('#receiving_amt').html(table.column( 6, {"filter": "applied"} ).data().sum() );
    //   $('#inst_sum').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    // });

    
    $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });
  });
</script>
@endsection