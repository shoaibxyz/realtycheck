@extends('admin.layouts.template')
@section('title','SMS Report')

@section('reports-active','active')
@section('marketing-report-active','active')
@section('sms-report-active','active')
@section('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<style type="text/css">
@if(Auth::user()->Roles->first()->name == 'call-center')
      .dt-buttons{
        display: none;
      }
    @endif
    </style>


@endsection

@section('content')

@include('errors')
@include('admin/common/breadcrumb',['page'=>'SMS Report'])
<div class="x_panel">
  <div class="x_title">
    <h2>SMS Report</h2>
    
    <div class="clearfix"></div>
  </div>    
  {!! Form::open(['route' => 'report.getSms', 'class' => 'form-horizontal','method' => 'GET']) !!}
  
  <div class="col-md-2">
    <div class="form-group">
      {!! Form::label('fromdate', 'From') !!}
      {!! Form::text('from_date', Request::input('from_date'),  array('class'=>'form-control datepicker', 'autocomplete' => 'off') )!!}
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      {!! Form::label('todate', 'To') !!}
      {!! Form::text('to_date', Request::input('to_date'),  array('class'=>'form-control datepicker', 'autocomplete' => 'off') )!!}
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      {!! Form::label('mobile_no', 'Mobile') !!}
      {!! Form::text('mobile_no', Request::input('mobile_no'),  array('class'=>'form-control', 'autocomplete' => 'off') )!!}
    </div>
  </div>
  
  <div class="col-md-2">
    <div class="form-group">
      {!! Form::label('reg_no', 'Reg#') !!}
      {!! Form::text('reg_no', Request::input('reg_no'),  array('class'=>'form-control', 'autocomplete' => 'off') )!!}
    </div>
  </div>
  
  <div class="col-md-2" style="margin-top: 25px; ">
    <div class="form-group">
      {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
    </div>
    {!! Form::close() !!}
  </div>


@if(isset($smss))
<div id="app">
        @include('admin.report.brandin-header', ['reportTitle' => "Alert Report"])

  <div class="table-responsive">
      
    <table class="table table-bordered table-hover jambo_table" id="datatable">
      <thead>
        <tr>
          <th>Sr #</th>
          <th>Mobile</th>
          <th>Event</th>
          <th>Message</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        @php($i = 1)
          @foreach($smss as $key => $val)
          <tr>
            <th>{{ $i++ }}</th>
            <th>{{ $val->sent_to }}</th>
            <th>{{ $val->event }}</th>
            <th>{{ $val->message }}</th>
            <th>{{ $val->created_at }}</th>
          </tr>
          @endforeach
      </tbody>
  </table>
  <button id="printreceipt" class="btn btn-success">Print</button>
</div>
@endif



</div>
</div>

@endsection
@section('javascript')

   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    var columns = [
            { data: 'id'},
            { data: 'registration_no', name: 'registration_no' },
            { data: 'member_name', name: 'member_name' },
            { data: 'phone', name: 'phone' },
            { data: 'payment_desc', name: 'payment_desc' },
            { data: 'plot_size', name: 'plot_size' },
            { data: 'installment_amount', name: 'installment_amount' },
            { data: 'amount_received', name: 'amount_received' },
            { data: 'payment_date', name: 'payment_date' },
        ];

        var to = "{{ Request::input('startdate') }}";
        var from = "{{ Request::input('enddate') }}";

       var table = $('#paidInstTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "info": true,
            "bSort" : false,
            buttons: [
                 'excel', 'print'
            ],
            "ajax": {
                "url": "{{ URL::to('admin/report/getPaidInst/')}}/"+from+"/"+to, 
            },
            "columns": columns
        });


    // jQuery.fn.DataTable.Api.register( 'sum()', function () {
    //   return this.flatten().reduce( function ( a, b ) {
    //         return (a*1) + (b*1); // cast values in-case they are strings
    //       });
    // });

   function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
      var windowUrl = 'about:blank';
      var uniqueName = new Date();
      var windowName = '_self';

      var printWindow = window.open(window.location.href , windowName);
      printWindow.document.body.innerHTML = printContent.innerHTML;
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
      return false;
    }

    $('#printreceipt').click(function(){
      CallPrint('app');
    });

    // var table = $('.jambo_table').DataTable({ 
    //   "bSort" : false,
    //   dom: 'Bfrtip',
    //   buttons: [
    //          'excel',
    //     ],
    // });
      
    // $("#paidInstTable").on('search.dt', function() {
    //   $('#receiving_amt').html(table.column( 6, {"filter": "applied"} ).data().sum() );
    //   $('#inst_sum').html(table.column( 7, {"filter": "applied"} ).data().sum() );
    // });

    
    $('.datepicker').datepicker({
      dateFormat: 'dd-mm-yy'
    });
  });
</script>
@endsection