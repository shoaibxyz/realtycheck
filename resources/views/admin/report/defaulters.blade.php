@extends('admin.layouts.template')
@section('title','Defaulters Report')

@section('reports-active','active')
@section('installment-report-active','active')
@section('defaulters-report-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

  
     <style type="text/css">
    @if(Auth::user()->Roles->first()->name == 'call-center' && Auth::user()->employee->is_senior)
      .dt-buttons{
        display: none;
      }
    @else
      .dt-buttons{
        display: none;
      }
    @endif
    </style>
@endsection

@section('content')
@include('errors')
@include('admin/common/breadcrumb',['page'=>'Defaulters Report'])

        <div class="x_panel">
          <div class="x_title">
            <h2>Defaulters Report</h2>
            <div class="clearfix"></div>
          </div>    
          {!! Form::open(['route' => 'report.defaulters', 'class' => 'form-horizontal','method' => 'GET']) !!}
                 
                 
          <div class="col-md-5">
              <div class="form-group">
                    {!! Form::label('enddate', 'From') !!}
                    {!! Form::text('enddate', Request::input('enddate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div>

          {{-- <div class="col-md-5">
            <div class="form-group">
                    {!! Form::label('startdate', 'To') !!}
                    {!! Form::text('startdate', Request::input('startdate'),  array('required' => 'required'  , 'class'=>'form-control datepicker','autocomplete'=>'off') )!!}
              </div>
          </div> --}}

          <div class="col-md-2" style="margin-top: 25px; ">
            <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-warning' , 'id' => 'ok']) !!}
            </div>
            {!! Form::close() !!}
          </div>





       
       
        @if(isset($installments))
          <div id="app">
            @include('admin.report.brandin-header', ['reportTitle' => "Due Installaments"])
          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action table-bordered" id="dueReportTable"> 
                <thead>
                    <tr>
                        <th>Sr #</th>
                        <th>Reg</th>
                        <th>Reference</th>
                        <th>Plot</th>
                        <th>Member</th>
                        <th>Date</th>
                        <th>Days</th>
                        <th>Phone</th>
                        <th>Amount</th>                        
                        {{-- <th>LPC</th>                         --}}
                        {{-- <th>Total Amount</th>                         --}}
                    </tr>
                </thead>
                <tbody>
                  @php($i = 1)
                  @php($regArr = [])
                  @php($inst_sum = 0)
                  @php($lpc_sum = 0)
                  @foreach($installments as $key => $inst)
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $inst->registration_no }}</td>
                      <td>{{ $member[$key]->reference_no ?? ""}}</td>
                      
                      <td>{{ $bookings[$key]->paymentPlan->plotSize->plot_size ?? ""}}</td>
                      <td>{{ $person[$key]->name ?? ""}}</td>
                      <td>{{ date('d-m-Y', strtotime($inst->installment_date)) }}</td>
                      <td>
                        <?php 
                          $date_1=date_create($inst->installment_date->format('Y-m-d'));
                          $date_2=date_create(date('Y-m-d'));
                          $diff2=date_diff($date_1,$date_2);
                          $days = (int)$diff2->format("%R%a");
                          if(($days > 0)) 
                            echo "<span class='text-danger'>".$diff2->format("%R%a")."</span>"; 
                          else 
                          echo $diff2->format("%R%a");
                        ?>
                      </td>
                      <td>{{ $person[$key]->phone_mobile ?? ""}}</td>
                      <td id="inst_amount">{{ $inst->sum }}</td>
                    </tr>
                    <?php $inst_sum += $inst->sum; ?>
                  @php($i++)
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="8">Total</th>
                    <td id="inst_sum">{{ number_format($inst_sum) }}</td>
                  </tr>
                </tfoot>
            </table>    
            
            
            
            @if(Auth::user()->Roles->first()->name == 'Super-Admin')
              <button id="printreceipt" class="btn btn-success">Print</button>
            @endif
          </div>
           
          @endif

</div>
      </div>
@endsection
@section('javascript')
   <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    // jQuery.fn.DataTable.Api.register( 'sum()', function () {
    //   return this.flatten().reduce( function ( a, b ) {
    //         return (a*1) + (b*1); // cast values in-case they are strings
    //       });
    // });
    
    function CallPrint(strid)
    {
      var printContent = document.getElementById(strid);
      var windowUrl = 'about:blank';
      var uniqueName = new Date();
      var windowName = '_self';

      var printWindow = window.open(window.location.href , windowName);
      printWindow.document.body.innerHTML = printContent.innerHTML;
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
      return false;
    }

    if("{{ Auth::user()->Roles->first()->name == 'Super-Admin' }}")
      {
          var buttons = [{
              text: 'Export to PDF',
              extend: 'pdfHtml5',
              message: '',
              orientation: 'landscape',
              exportOptions: {
                columns: ':visible',
                header: true,
                footer: true,
              },
          },
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
              }
          },
          {
            extend: 'excel'
          }];
      }else{
        var buttons = [];
      }

    $('#printreceipt').click(function(){
      CallPrint('app');
    });

    jQuery.fn.dataTable.Api.register( 'sum()', function () {
      return this.flatten().reduce( function ( a, b ) {
          return (a*1) + (b*1); // cast values in-case they are strings
      });
  });


       var table = $('#dueReportTable').DataTable({ 
        "bSort" : false,
        // dom: 'Bfrtip',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: buttons
      });

    $("#dueReportTable").on('search.dt', function() {
      $('#inst_sum').html(table.column( 7, {"filter": "applied"} ).data().sum() );
      // $('#lpc_sum').html(table.column( 11, {"filter": "applied"} ).data().sum() );
      // $('#total').html(table.column( 12, {"filter": "applied"} ).data().sum() );
    });

    function numberFormat(n) {
        return n.toFixed(0).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }
    
      $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
      });
    });
    </script>
@endsection