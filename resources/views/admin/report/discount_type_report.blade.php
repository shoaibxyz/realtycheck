@extends('admin.layouts.template')
@section('title', 'Discount Type Report')

@section('reports-active','active')
@section('discount-report-active','active')
@section('management-discount-active','active')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
         
         <?php if( Session::has('message')) : 
            $alertType = ( Session('status') == 1 ) ? "alert-success" : "alert-danger";
         ?>
            <div class="alert {{ $alertType }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session('message') }}
            </div>
        <?php endif; ?>


               <div class="x_panel">
        <div class="x_title">
           <h2>{{ $rebate_type }} Receipts</h2>
            
            <div class="clearfix"></div>
          </div>   
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action" id="receiptTable">
                <thead>
                    <tr>
                        <th>Reg #</th>
                        <th>Receipt #</th>
                        <th>Receipt Amt</th>
                        <th>Rebate</th>
                        <th>Discount Type</th>
                        <th>Recev Mode</th>
                        <th>Recev Date</th>
                        <th>Recev Type</th>
@if(Auth::user()->Roles->first()->name == 'Super-Admin' || Auth::user()->Roles->first()->name == 'Admin')
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
                </table>
            </div>
        </div>
@endsection
 {{-- <tbody>
    @foreach($receipts as $receipt)
        <tr>
            <td>{{ $receipt->registration_no }}</td>
            <td>{{ $receipt->receipt_no }}</td>
            <td>{{ $receipt->received_amount }}</td>
            <td>{{ $receipt->receiving_mode }}</td>
            <td>{{ date('d-m-Y', strtotime($receipt->receiving_date)) }}</td>
            <td>{{ ($receipt->receiving_type == 1) ? "Inst" : "Other" }}</td>
@if(Auth::user()->Roles->first()->name == 'Super-Admin' || Auth::user()->Roles->first()->name == 'Admin')
            <td>
                <a href="{{ route('receipt.edit', $receipt->id) }}" class="btn btn-xs btn-warning">Edit</a>
                {!! Form::open(['method' => 'DELETE', 'route' => ['receipt.destroy', $receipt->id], 'style' => 'display: inline;']) !!}
                    <input type='submit' class='btn btn-danger btn-xs DeleteBtn' value='Delete'>
                {!! Form::close() !!}
           </td>
           @endif
        </tr>
    @endforeach
    </tbody>


 --}}
@section('javascript')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

        var columns = [
            { data: 'registration_no', name: 'registration_no' },
            { data: 'receipt_no', name: 'receipt_no' },
            { data: 'received_amount', name: 'received_amount' },
            { data: 'rebate_amount', name: 'rebate_amount' },
            { data: 'discount_type', name: 'discount_type' },
            { data: 'receiving_mode', name: 'receiving_mode' },
            { data: 'receiving_date', name: 'receiving_date' },
            { data: 'receiving_type', name: 'receiving_type' },
        ];
        if("{{ Auth::user()->Roles->first()->name }}" == 'Super-Admin' || "{{ Auth::user()->Roles->first()->name }}" == 'Admin'){
          columns.push({ data: 'actions', name: 'actions'})
        }

        $('#receiptTable').DataTable({
            "processing": true,
            "serverSide": true,
            "bSort": false,
            "ajax": '{!! route('receipt.getReceipts') !!}' + '/{{ $rebate_type }}',
            "columns": columns
        });


     $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
    });

      $(document).on('click','.DeleteBtn',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this receipt!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your receipt file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your receipt file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });

    $('#payment_schedule').change(function(event) {
        var planId = $(this).val();
        $.ajax({
            url: '{{ URL::to('/PlanInfo') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {'payment_schedule_id': planId},
            success: function(result){
                $('#Booking_Price').val(result.booking_price);
                $('#booking_percentage').val(result.booking_percentage);
            },
            error: function(xhr, status,response){
                $('#data').html(xhr.responseText);
            }
        }); 
    });

    var inputs = $('#data input, #data select').attr('disabled','disabled');
    $('#paymentForm').submit(function(event){
        event.preventDefault();
        var url = '{{ URL::to('/memberInfo') }}';
        var cnic = $('#cnic').val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {'cnic': cnic},
            success: function(result){
                var $inputs = $('#data input');
                var $persons = $('#persons input');

                $('#data input, #data select').removeAttr('disabled','disabled');
                $('#data input#cnic').attr('disabled','disabled');
                $('#data input#person').attr('disabled','disabled');
                $('#data input#person').val(result.person.name);
                $('#cnic_field').val($('#cnic').val());
                $('#img-preview').attr('src','../public/images/'+result.person.picture);

                $.each(result.member, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

                $.each(result.person, function(key, value) {
                  $inputs.filter(function() {
                    return key == this.name;
                  }).val(value);
                });

            },
            error: function(xhr, status, response){
                console.log(response);
            }
        });
    });
        
    </script>
@endsection

