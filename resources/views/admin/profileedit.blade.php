@extends('admin.layouts.template')

@section('title','Profile')

@section('profile-active','active')

@section('content')

@include('admin/common/breadcrumb',['page'=>'Edit Profile'])
<div class="" role="main">
         
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                 <h2>Edit Member Information</h2> 
            </div>
            </div>
        <div class="row">
            
            <div class="x_panel">
                  <div class="x_title">
                    <h2>User Information </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
        {!! Form::model($profile,['route' => ['memberstore', $profile->id] , 'method' => 'POST' , 'files'=>true]) !!}
                <div class="form-group">
                        {!! Form::label('picture', 'Picture')!!}
                        {!! Form::file('picture', null, ['class' => 'form-control']) !!}
                       
                        <img id="img-preview" src="{{ env('STORAGE_PATH').$profile->picture }}" name="picture"  style="margin: 10px auto;" alt="Picture" height="100" width="200" />
                        
                  </div>


     
   
      <div class="form-group">      
                <input type="hidden" name="_method" value="POST">
                {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
           
    </div>

        {!! Form::close() !!} 

              </div>
              
              </div>
              </div>
           
        </div>
@endsection