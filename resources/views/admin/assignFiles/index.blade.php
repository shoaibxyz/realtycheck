@extends('admin.layouts.template')

@section('title','Assigned Files')

@section('bookings-active','active')
@section('agencies-active','active')
@section('assign_file-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

 
@endsection

@section('content')
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Assigned Files</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Assigned Files</li>
        </ol>
    </div>
    <div class="content">
        
        <div class="x_panel">
            <div class="x_title">
                <h2>Assigned Files</h2>
                <div class="actions" style="float: right; display: inline-block; {{$rights->show_create}} ">
                    <a href="{{ route('assign-file.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <div class="note note-success" style="{{ (Session('status') == 'success') ? 'display:block' : 'display:none' }}">
                    <h4 class="box-heading">Success</h4>
                    <p>{{ Session::get('message') }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 align-right">
                    <form method="POST" id="search-form" class="form-horizontal" role="form">
                        <div class="col-md-3">
                  <div class="form-group">
                        {!! Form::label('startdate', 'From') !!}
                        {!! Form::text('startdate', Request::input('startdate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
                  </div>
              </div>
                <div class="col-md-3">
                <div class="form-group">
                        {!! Form::label('enddate', 'To') !!}
                        {!! Form::text('enddate', Request::input('enddate'),  array('class'=>'form-control datepicker','autocomplete'=>'off') )!!}
                  </div>
              </div> 
                        
                        <div class="col-md-2">
                            <div class="form-group" style="margin-top:23px;">
                            
                            <input type="submit" name="search" id="search" class="form-control ddsearch btn btn-primary">
                        </div></div>
                        
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table" id="assignfileTable" width="100%">
                    <thead>
                        <tr>
                            <th>#Id</th>
                            <th>Plot</th>
                            <th>Type</th>
                            <th>Reg No</th>
                            <th>Agency</th>
                            <th>Batch No</th>
                            <th>Batch Date</th>
                            <th>Is Booked?</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    </div>
  
@endsection
@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript">
        // var index = iDisplayIndex +1;
        // $('td:eq(0)',nRow).html(index);
        // return nRow;

    $(document).ready(function() {
        $('.datepicker').datepicker({
          dateFormat: 'dd/mm/yy'
        });
    });
       
        $('.DeleteBtn').on('click',function(event){
            event.preventDefault();
            var form = $(this).parents('form');
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                form.submit();
              } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                // event.preventDefault();
              }
            });
        });

        var columns = [
            { data: 'id', name: 'id' },
            { data: 'plot_size', name: 'plot_size' },
            { data: 'plot_type', name: 'plot_type' },
            { data: 'registration_no', name: 'registration_no' },
            { data: 'agency', name: 'agency' },
            { data: 'batch_no', name: 'batch_no' },
            { data: 'batch_assign_date', name: 'batch_assign_date' },
            { data: 'is_reserved', name: 'is_reserved' },
            { data: 'block', name: 'block' },
            { data: 'actions', name: 'actions' },
        ];
        
        var buttons = [
          {
              extend: 'print',
              customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                    
                $(win.document.body).find( 'table tr , table td' )
                    .addClass( 'compact' )
                    .css( 'border', '1px solid' );
              }
          },
          {
              extend: 'excel'
          }];


        var table = $('#assignfileTable').DataTable({
             dom: 'Bfrtip',
            bFilter: true,
            processing: true,
            serverSide: true,
            "stateSave":true,
            "bDestroy":true,
            bSort: false,
            "bPaginate": false,
            ajax: {
                url: '{!! route('assign-file.getFiles') !!}',
                data: function (d) {
                    d.startdate = $('#startdate').val(),
                    d.enddate = $('#enddate').val()
                }
            },
            columns: columns,
            buttons: buttons
        });

         $('#search').on('click', function(e) {
            table.draw();
            e.preventDefault();
        });
    </script>
@endsection

