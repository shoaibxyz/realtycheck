@extends('admin.layouts.template')

@section('title','Edit Assigned File')

@section('bookings-active','active')
@section('agencies-active','active')
@section('assign_file-active','active')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Edit Assigned File</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ url("admin/home") }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Edit Assigned File</li>
        </ol>
    </div>
    <div class="content">
        <div class="x_panel" style="padding-top: 50px;">
            <div class="col-sm-6 col-sm-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($assignFiles, ['method' => 'POST', 'route' => ['assign-file.update', $assignFiles->id], 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Registration No <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('registration_no', null, ['class' => 'form-control', 'id' => 'registration_no','required' =>'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Plot Size <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <?php $plotSizeArr[''] = 'choose one'; ?>
                            @foreach($plotSize as $plot)
                                <?php $plotSizeArr[$plot->id] = $plot->plot_size; ?>
                            @endforeach

                            {!! Form::select('plot_size_id', $plotSizeArr, old('plot_size_id'),  array('class'=>'form-control','required' =>'required') )!!}
                        </div>
                    </div>

                     <div class="form-group">
                        <?php
                        $plotNatureArr[""] = "";
                        $plotNatureArr["Residential"] = 'Residential';
                        $plotNatureArr["Commercial"] = 'Commercial';
                        $plotNatureArr["Housing"] = 'Housing';
                        ?>
                        <label for="inputEmail3" class="col-sm-3 control-label">Plot Nature <span class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::select('plot_type', $plotNatureArr, old('plot_type'), ['required' =>'required', 'class' => 'form-control has-feedback-right', 'id' => 'plot_type']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Agency <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <?php $agenciesArr[''] = 'choose one'; ?>
                            @foreach($agencies as $agency)
                                <?php $agenciesArr[$agency->id] = $agency->name; ?>
                            @endforeach
                            {!! Form::select('agency_id', $agenciesArr, old('agency_id'),  array('class'=>'form-control',  'id' => 'agency','required' =>'required') )!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Batch <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <?php $batchArr[''] = 'choose one'; ?>
                            @foreach($agencyBatch as $batch)
                                <?php $batchArr[$batch->id] = $batch->batch_no; ?>
                            @endforeach
                            {!! Form::select('batch_id', $batchArr, old('batch_id'),  array('class'=>'form-control', 'id' => 'batches','required' =>'required') )!!}
                        </div>
                    </div>

                    <div class="form-actions text-right pal">
                        {!! Form::hidden('_method', 'PUT') !!}
                        {!! Form::submit("Update Assigned File", ['class' => 'btn btn-success']) !!}&nbsp;
                        <a href="{{ route('assign-file.index') }}" class="btn btn-grey">Cancel</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {
        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy'
        });

        var url = "{{ route('agency.batch') }}";
        $('#agency').on('change', function(){
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    'agency_id': $(this).val(),
                    'id': 1
                },
                success: function(data){
                    $('#batches').html(data);
                },
                error: function(xhr, status, responseText){
                    console.log(xhr);
                }
            });
        });
    });
    </script>
@endsection
