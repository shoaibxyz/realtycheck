@extends('admin.layouts.template')
@section('title','Add Transfer Charges')

@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer_charges-active','active')
@section('content')

@include('admin/common/breadcrumb',['page'=>'Add Transfer Charges'])
       <div class="x_panel">
		    <div class="x_title">
			    <h2>Add New</h2>
			    <div class="clearfix"></div>
			</div>  
        <div class="col-sm-6">    
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif  

        {!! Form::open(['method' => 'POST', 'route' => 'transfer_charges.store', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('charges', 'Charges') !!}
                {!! Form::text('charges', '', ['class' => 'form-control', 'id' => 'name']) !!} 
            </div>

           	<div class="form-group">
                <h4>Plot Size</h4>
                <select class="select2_multiple form-control"  name="plot_size_id">
                    <option value="">Choose option</option>
                    @foreach($paymentSchedule as $schedule)
                        <option value="{{ $schedule->id }}"> {{ $schedule->plot_size }}</option>
                    @endforeach
                </select>
        	</div>

            <div class="form-group">
                {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
        </div></div>
@endsection