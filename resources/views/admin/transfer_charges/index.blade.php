@extends('admin.layouts.template')
@section('title','Transfer Charges')

@section('bookings-active','active')
@section('transfers-active','active')
@section('transfer_charges-active','active')

@section('content')
@include('admin/common/breadcrumb',['page'=>'Transfer Charges'])
       
        @if(!empty($message))
            <p>{{$message}}</p>
        @endif

        <div class="x_panel">
        <div class="x_title">
            
             <h2>All Transfer Charges</h2>
             <small class="pull-right"><a href="{{ route('transfer_charges.create')}}" class="btn btn-primary" style="{{ $rights->show_create }}">Add New</a></small>
            <div class="clearfix"></div>
          </div>    
        
        <div class="table-responsive">
        <table class="table table-striped table-bordered jambo_table bulk_action" id="datatable">
            <thead>
                <tr>
                    <th>Plot Size</th>
                    <th>Charges</th>
                    <th style="{{ $rights->show_edit }}">Edit</th> 
                    <th style="{{ $rights->show_delete }}">Delete</th> 
                </tr>
            </thead>
            <tbody>
                
                @foreach($TransferCharges as $transfer)
                <tr>
                    <td>{{ ($transfer->plotSize) ? $transfer->plotSize->plot_size : '' }}</td>
                    <td>{{ $transfer->charges}}</td>
                    <td style="{{ $rights->show_edit }}">
                        <a class="btn btn-warning btn-xs" href="{{ route('transfer_charges.edit', $transfer->id) }}">Edit</a> 
                    </td>
                    <td  style="{{ $rights->show_delete }}">
                       {!! Form::open(['method' => 'DELETE', 'route' => ['transfer_charges.destroy', $transfer->id]]) !!}
                            {!! Form::submit("Delete", ['class' => 'btn btn-danger btn-xs DeleteBtn']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
        </div>  </div>
@endsection
