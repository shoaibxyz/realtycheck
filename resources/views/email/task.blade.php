@component('mail::message')
# Hi

<h2>{{ $content['title'] }} </h2> has been added in your system.



Thanks,<br>
{{ config('app.name') }}
@endcomponent