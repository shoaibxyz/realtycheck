<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>REALTY CHECK</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="{{ asset('public/')}}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/animate.css">
		<link rel="stylesheet" href="{{ asset('public/')}}/css/form-elements.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/style.css">
        <link rel="stylesheet" href="{{ asset('public/')}}/css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{ asset('public/')}}/ico/rcfavicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('public/')}}{{ asset('public/')}}/ico/rcfavicon.png">

    </head>

    <body>

        <!-- Loader -->
    	<div class="loader">
    		<div class="loader-img"></div>
    	</div>

		<!-- Top menu -->
		<nav class="navbar navbar-inverse navbar-fixed-top navbar-no-bg" role="navigation">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{ URL::to('/') }}">
                        <img class="img-responsive" src="{{ asset('public/')}}/img/logo.png"/>
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a class="scroll-link" href="#top-content">HOME</a></li>
						<li><a class="scroll-link" href="#features">FEATURES</a></li>
						<li><a class="scroll-link" href="#screenshots">SCREENSHOTS</a></li>
						<li><a class="scroll-link" href="#testimonials">TESTIMONIALS</a></li>

						<li><a class="scroll-link" href="#about-us">CONTACT US</a></li>
						<li><a class="scroll-link" href="">|</a></li>
						<li><a class="scroll-link" ><img src="{{ asset('public/')}}/img/A-product-of-.png" alt=""></a></li>
					</ul>
				</div>
			</div>
		</nav>

        <!-- Top content -->
        <div class="top-content" >

            <div class="inner-bg">


                    <div class="row">


                        <div class="col-md-7 great-support-box wow fadeInUp">
	                    <img src="{{ asset('public/')}}/img/computers.png" alt="" class="img-responsive" id="s-img">
	                </div>
	            	<div class="col-md-5 great-support-box wow fadeInLeft">
	                    <div class="great-support-box-text great-support-box-text-left">
	                    	<h2 style="color:white;">Make your Real Estate Business easy with our <strong>REALTY CHECK</strong></h2>
	                    	<p class="medium-paragraph" style="color:white;">
	                    		A solution to help your real estate needs and make life easier. Get all your real estate data online through Realty Check
	                    	</p>

	                    </div>
	                    <div class="top-big-link wow fadeInUp">
                            	<a class="btn btn-link-1" href="{{ route('login') }}" target="blank">Login</a>

                            </div>
	                </div>


                        </div>

                    </div>

                </div>
            </div>

        </div>

        <!-- Features -->
        <div class="features-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 features section-description wow fadeIn">
	                    <h2><strong>FEATURES</strong> </h2>
	                    <p>We've got a lot of amazing and cool features.</p>

	                </div>
	            </div>
	            <div class="row">
                	<div class="col-sm-4 features-box wow fadeInUp">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/reporting.png">
	                	</div>
	                    <h3>REPORTING</h3>
	                    <p>Quickly create easy to read reports using built-in standard reports for property management and financial data</p>
                    </div>
                    <div class="col-sm-4 features-box wow fadeInDown">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/support.png">
	                	</div>
	                    <h3>SUPPORT</h3>
	                    <p>Urgent question or technical problem? Customer Care is available online or on the phone Monday through Friday</p>
                    </div>
                    <div class="col-sm-4 features-box wow fadeInUp">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/feature-rich.png">
	                	</div>
	                    <h3>FEATURE RICH</h3>
	                    <p>Our customers say that the best part of this is how feature rich it is that makes our work much more easier.</p>
                    </div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-4 features-box wow fadeInUp">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/user-friendly.png">
	                	</div>
	                    <h3>USER FRIENDLY</h3>
	                    <p>We've created user friendly interface for our customers due to which they can easily communicate with us through this portal</p>
                    </div>
                    <div class="col-sm-4 features-box wow fadeInUp">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/data-security.png">
	                	</div>
	                    <h3>DATA SECURITY</h3>
	                    <p>More than 12,000 property managers trust Buildium with their data, because we take industry-leading security measures.</p>
                    </div>
                    <div class="col-sm-4 features-box wow fadeInUp">
	                	<div class="features-box-icon">
	                		<img src="{{ asset('public/')}}/img/icons/admin-portal.png">
	                	</div>
	                    <h3>ADMIN PORTAL</h3>
	                    <p>Provide easy access to financial reporting to owners via a desktop or mobile portal.</p>
                    </div>


	            </div>

	        </div>
        </div>

		<!--  screenshots -->
        <div class="screenshots-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 features section-description wow fadeIn">
	                    <h2><strong>SCREENSHOTS</strong> </h2>
	                    <p>Our awsome app screenshots's gallery here.</p>

	                </div>
	            </div>
	            <div class="row">

                    <div class="col-sm-12 great-support-box wow fadeInUp">
	                    <img src="{{ asset('public/')}}/img/devices/screenshot.png" alt="">
	                </div>

	            </div>

	        </div>
        </div>

        <!-- Great support -->


        <!-- How it works -->


        <!-- Testimonials -->
        <div class="testimonials-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 testimonials section-description wow fadeIn">
	                    <h2><strong style="color:white;">TESTIMONIALS</strong> </h2>

	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-10 col-sm-offset-1 testimonial-list wow fadeInUp">
	                	<div role="tabpanel">
	                		<!-- Tab panes -->
	                		<div class="tab-content">
	                			<div role="tabpanel" class="tab-pane fade in active" id="tab1">

	                				<div class="testimonial-text">
		                                <p>
		                                	" I will highly recommend your company to all my friends who own rental properties (and there are quite a few). I can't say enough about the service from your company and look forward to dealing with you again in the near future. You're the best."<br>
		                                	<a href="#">Ahmad Azam</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab2">

	                				<div class="testimonial-text">
		                                <p>
		                                	"Thank you once again for the fantastic job of getting my property rent again this year. I wanted to let you know that your agents were fantastic and were willing to take my call and return my calls every time. Thanks again."<br>
		                                	<a href="#">Rizwan Saleem</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab3">

	                				<div class="testimonial-text">
		                                <p>
		                                	"Your staff here is very proactive and there has definitely been more traffic thanks to you guys! I really appreciate all of your efforts and hard work! So, please keep them coming and I'll keep you guys posted on all the developments!"<br>
		                                	<a href="#">Faizan Ali</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab4">

	                				<div class="testimonial-text">
		                                <p>
		                                	"Xeffectstudio provide excellent service to me beyond my expectations. I would be happy to recommend them in my circle, thanks!"<br>
		                                	<a href="#">Tehreem Atique</a>
		                                </p>
	                                </div>
	                			</div>
	                		</div>
	                		<!-- Nav tabs -->
	                		<ul class="nav nav-tabs" role="tablist">
	                			<li role="presentation" class="active">
	                				<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"></a>
	                			</li>
	                		</ul>
	                	</div>
	                </div>
	            </div>

	        </div>
        </div>

        <!-- Subcribe -->
        <div class="subscribe-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 features section-description wow fadeIn">
	                    <h2><strong>SUBSCRIBE TO STAY UPDATE</strong> </h2>


	                    <div class="row">

                    <div class="col-sm-12 great-support-box wow fadeInUp">
                    <form>
                    <div class="col-md-4"></div>
                    	<div class="col-md-4"><input type="text" placeholder="Enter Email"  id="email1" class="form-control"></br></div>
                    	<div class="col-md-4"></div>

                    	<div class="col-md-12"><input type="submit" class="btn btn-link-2" value="Subscribe" ></div>

                    </form>
                    </div></div>
	                </div>
	            </div>


	        </div>
        </div>

        <!-- Get In touch -->
        <div class="about-us-container section-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 about-us section-description wow fadeIn">
	                    <h2><strong style="color:white;">GET IN TOUCH</strong> </h2>
	                    <p>If you have any question , get in touch here.</p>

	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-8 about-us-box wow fadeInUp">

	                    <form>
	                    <div class="row">
	                    <div class="col-md-4">
                    	<input type="text" placeholder="Name " class="form-control"></div>
                    	<div class="col-md-4"><input type="text" placeholder="Email" class="form-control"></div>
                    	<div class="col-md-4"><input type="text" placeholder="Subject" class="form-control"></div></div>
</br>

                    	<div class="row"><div class="col-md-12">
                    	<textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10" placeholder="Message"></textarea>
</div></div>
</br>
<input type="submit" class="btn btn-link-2" value="Send Message">

                    </form>

	                </div>
	                <div class="col-md-4 about-us-box wow fadeInDown">
		                <ul class="list-unstyled">
                                <li><i class="fa fa-building"></i> 6-Commercial Area Cavalary Ground Lahore Pakistan </li>
                                <li><i class="fa fa-envelope"></i> waqqas.qadir@gmail.com </li>
                                <li><i class="fa fa-phone"></i> 0312-3541880 </li>

                              </ul>
	                    <div class="col-sm-12 footer-social">
                    	<a href="#"><i class="fa fa-facebook"></i></a>

                    	<a href="#"><i class="fa fa-twitter"></i></a>
                    	<a href="#"><i class="fa fa-instagram"></i></a>


	            </div>
	                </div>



	            </div>
	        </div>
        </div>

        <!-- Footer -->
        <footer>
	        <div class="container">

	            <div class="row">
                    <div class="col-sm-12 footer-copyright">
                    	&copy; 2017- <a href="">XeffectStudio</a>All Rights Reserved.
                    </div>
                </div>
	        </div>
        </footer>


        <!-- Javascript -->
        <script src="{{ asset('public/')}}/js/jquery-1.11.1.min.js"></script>
        <script src="{{ asset('public/')}}/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{ asset('public/')}}/js/jquery.backstretch.min.js"></script>
        <script src="{{ asset('public/')}}/js/wow.min.js"></script>
        <script src="{{ asset('public/')}}/js/retina-1.1.0.min.js"></script>
        <script src="{{ asset('public/')}}/js/waypoints.min.js"></script>
        <script src="{{ asset('public/')}}/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
