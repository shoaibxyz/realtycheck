@if (count($errors) > 0)
    <div class="note note-danger">
        <button type="button" class="close" data-dismiss="note" aria-hidden="true">&times;</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <?php if( Session::has('message')) :
    $alertType = ( Session('status') == 1 ) ? "note-success" : "note-danger";
 ?>

    <div class="note {{ $alertType }}">
        <button type="button" class="close" data-dismiss="note" aria-hidden="true">&times;</button>
        {{ Session('message') }}
    </div>
<?php endif; ?>
