@extends('layouts.app')

@section('content')
<div class="container1" >
    <div class="row">
        <div class="col-md-8 ">
            <div class="panel panel-default" >
                <div class="panel-heading" style="color:#f9a533">Login/Register</div>
                <div class="panel-body">
                    @include('errors')
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <input id="username" type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" required  autocomplete="" >
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" autocomplete=""  class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                           <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-primary btn-md">Sign In</button>
                                </div>
                                <div class="col-md-5">
                                    <a class="btn btn-link" href="{{ route('register') }}" style="color:black;">Create Account</a>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                         <div class="panel-footer">
                            <div class="form-group">
                                <center style="margin-top: 6px;">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <a href=""><img src="{{ asset('public/') }}/images/Vector.png"  class="l-img"></a>
            </div>
            <div class="col-md-6" style="padding-right: 0; text-align: right">
                <a href=""><img src="{{ asset('public/') }}/images/A-product-of-.png" class="l-img" style="display: inline-block; margin: 0;">
            </div>
        </div>
    </div>
</div>
@endsection
