@extends('layouts.app2')

@section('content')
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">


<form class="login100-form validate-form" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
<div class="input-group" style="margin-bottom:50px;">

<img src="{{ asset('public/') }}/img/logo1.png" class="navbar-brand" style="margin-bottom: 15px;">
</div>
<div class="input-group{{ $errors->has('username') ? ' has-error' : '' }}">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
     <input id="username" type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" required  autocomplete="" >
  </div>
  
  
  <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input id="password" type="password" autocomplete=""  class="form-control" name="password" placeholder="Password" required>
  </div>
  
  <div class="form-group">

      <button type="submit" class="btn btn-default">Login</button>

  </div>
</form>

				<div class="login100-more" style="background-image: url('http://realtycheck.lahorecapitalcity.com/public/img/left.png'); width: 50%;">
				</div>
			</div>
		</div>
	</div>
	
@endsection