<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <!-- Styles -->

   	<link rel="stylesheet" type="text/css" href="{{ asset('public/')}}/css/main.css">
   	<link rel="stylesheet" type="text/css" href="{{ asset('public/')}}/css/util.css">
   	<link href="{{ asset('public/vendor/')}}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
   	 <link href="{{ asset('public/vendor/')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   	
   <style>
.glyphicon{
    padding: 4px;
}
.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle{
    height: 58px;
    font-family: Montserrat-Regular;font-size:17px;
    border-radius: 1px;
    -webkit-box-shadow: none;
    box-shadow: none;
    border:none;
}
.input-group {
    margin-bottom: 11px;
    width: 67%;font-family: Montserrat-Regular;font-size:17px;
}
.input-group-addon{
    background-color: #e1e1e1;
    border-radius: 0px;border: none;
}
.form-control{
    background-color: #f3f3f3;
}
.navbar-brand {
    float: left;
    height: 127px;
    padding: 16px 0px;
    font-size: 18px;
    line-height: 20px;
}
.login100-form{
    background-color: #fff;
    padding: 303px 56px 40px 56px;
}
.btn-default {
    color: #fff;
    background-color: #2e3192;
    border-color: #ccc;
padding: 6px 18px;font-family: Montserrat-SemiBold;font-size:17px;border-radius: 1px;
}
.checkbox{
    font-family: Montserrat;
    font-size: 15px;
}
</style>
</head>
<body style="background-color: #666666;">
    

    <!-- Scripts -->
  @yield('content')
    <script src="{{ asset('public/')}}/js/main1.js"></script>
    <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
          crossorigin="anonymous"></script>
           <script src="{{ asset('public/vendor/')}}/bootstrap/dist/js/bootstrap.min.js"></script>

    @yield('footer')
</body>
</html>
