<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public/')}}/ico/rcfavicon.png">
    <!-- Styles -->
   <link rel="stylesheet" href="{{ asset('public/')}}/css/app.css">
   
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <strong style="color:#f58220;font-size:28px;">REALTY </strong><strong style="color:#2e3192;font-size:28px;">CHECK</strong>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('public/')}}/js/app.js">

    @yield('footer')
</body>
</html>
